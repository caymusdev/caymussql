SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[batches] (
		[batch_id]             [int] IDENTITY(1, 1) NOT NULL,
		[batch_type]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[batch_status]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[file_name]            [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[file_create_date]     [datetime] NULL,
		[comments]             [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[deleted_flag]         [bit] NOT NULL,
		[create_date]          [datetime] NOT NULL,
		[processed_date]       [datetime] NULL,
		[change_date]          [datetime] NULL,
		[change_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[status_message]       [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[batch_date]           [datetime] NULL,
		[settle_date]          [datetime] NULL,
		[manual_flag]          [bit] NOT NULL,
		[batch_amount]         [money] NULL,
		CONSTRAINT [pk_batches_id]
		PRIMARY KEY
		CLUSTERED
		([batch_id] DESC)
)
GO
ALTER TABLE [dbo].[batches]
	ADD
	CONSTRAINT [DF__batches__manual___5A4F643B]
	DEFAULT ((0)) FOR [manual_flag]
GO
ALTER TABLE [dbo].[batches]
	ADD
	CONSTRAINT [DF__batches__deleted__5B438874]
	DEFAULT ((0)) FOR [deleted_flag]
GO
ALTER TABLE [dbo].[batches]
	ADD
	CONSTRAINT [DF__batches__create___5C37ACAD]
	DEFAULT (getutcdate()) FOR [create_date]
GO
CREATE NONCLUSTERED INDEX [idx_batches_batch_status]
	ON [dbo].[batches] ([batch_status])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_batches_batch_type]
	ON [dbo].[batches] ([batch_type])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[batches] SET (LOCK_ESCALATION = TABLE)
GO
