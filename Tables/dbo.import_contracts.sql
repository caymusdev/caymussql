SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[import_contracts] (
		[affiliate_id]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[affiliate_name]       [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[merchant_id]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[merchant_name]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contract_id]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contract_number]      [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[gross_due]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[import_id]            [int] IDENTITY(1, 1) NOT NULL,
		[create_date]          [datetime] NOT NULL,
		[change_date]          [datetime] NOT NULL,
		[change_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[processed_date]       [datetime] NULL,
		[bad_record]           [bit] NULL,
		[fees_outstanding]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[original_rtr]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[pp_outstanding]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[writeoff_date]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[collect_status]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_import_contracts_id]
		PRIMARY KEY
		CLUSTERED
		([import_id])
)
GO
ALTER TABLE [dbo].[import_contracts]
	ADD
	CONSTRAINT [DF__import_co__creat__3DD3211E]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[import_contracts]
	ADD
	CONSTRAINT [DF__import_co__chang__3EC74557]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[import_contracts]
	ADD
	CONSTRAINT [DF__import_co__chang__3FBB6990]
	DEFAULT ('SYSTEM') FOR [change_user]
GO
ALTER TABLE [dbo].[import_contracts]
	ADD
	CONSTRAINT [DF__import_co__creat__40AF8DC9]
	DEFAULT ('SYSTEM') FOR [create_user]
GO
CREATE NONCLUSTERED INDEX [idx_import_contracts_affiliate_id]
	ON [dbo].[import_contracts] ([affiliate_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_import_contracts_merchant_id]
	ON [dbo].[import_contracts] ([merchant_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_import_contracts_contract_id]
	ON [dbo].[import_contracts] ([contract_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[import_contracts] SET (LOCK_ESCALATION = TABLE)
GO
