SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[import_logs] (
		[import_log_id]     [int] IDENTITY(1, 1) NOT NULL,
		[create_date]       [datetime] NOT NULL,
		[message]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[user_id]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[module]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[file_name]         [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]       [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_date]       [datetime] NULL,
		[change_user]       [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_import_log]
		PRIMARY KEY
		CLUSTERED
		([import_log_id] DESC)
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[import_logs]
	ADD
	CONSTRAINT [DF__import_lo__creat__0169315C]
	DEFAULT (getutcdate()) FOR [create_date]
GO
CREATE NONCLUSTERED INDEX [idx_import_log_create_date]
	ON [dbo].[import_logs] ([create_date] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[import_logs] SET (LOCK_ESCALATION = TABLE)
GO
