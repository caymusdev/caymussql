SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user_statuses] (
		[user_statuses_id]     [int] IDENTITY(1, 1) NOT NULL,
		[user_id]              [int] NOT NULL,
		[status_id]            [int] NOT NULL,
		[start_date]           [datetime] NOT NULL,
		[end_date]             [datetime] NULL,
		[create_date]          [datetime] NOT NULL,
		[change_date]          [datetime] NOT NULL,
		[change_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_user_statuses_id]
		PRIMARY KEY
		CLUSTERED
		([user_statuses_id])
)
GO
ALTER TABLE [dbo].[user_statuses]
	ADD
	CONSTRAINT [DF__user_stat__creat__002AF460]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[user_statuses]
	ADD
	CONSTRAINT [DF__user_stat__chang__011F1899]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[user_statuses]
	ADD
	CONSTRAINT [DF__user_stat__start__7F36D027]
	DEFAULT (getutcdate()) FOR [start_date]
GO
ALTER TABLE [dbo].[user_statuses]
	WITH CHECK
	ADD CONSTRAINT [fk_user_statuses_users_user_id]
	FOREIGN KEY ([user_id]) REFERENCES [dbo].[users] ([user_id])
ALTER TABLE [dbo].[user_statuses]
	CHECK CONSTRAINT [fk_user_statuses_users_user_id]

GO
ALTER TABLE [dbo].[user_statuses] SET (LOCK_ESCALATION = TABLE)
GO
