SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[emails] (
		[email_id]         [int] IDENTITY(1, 1) NOT NULL,
		[affiliate_id]     [bigint] NULL,
		[merchant_id]      [bigint] NULL,
		[contract_id]      [bigint] NULL,
		[subject]          [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[body]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[sender_id]        [int] NULL,
		[sent]             [datetime] NULL,
		[received]         [datetime] NULL,
		[create_date]      [datetime] NOT NULL,
		[change_date]      [datetime] NOT NULL,
		[change_user]      [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]      [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[message_id]       [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[unique_id]        [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[incoming]         [bit] NULL,
		[from_address]     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_emails_id]
		PRIMARY KEY
		CLUSTERED
		([email_id] DESC)
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[emails]
	ADD
	CONSTRAINT [DF__emails__create_d__186C9245]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[emails]
	ADD
	CONSTRAINT [DF__emails__change_d__1960B67E]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[emails]
	WITH CHECK
	ADD CONSTRAINT [fk_emails_affiliates]
	FOREIGN KEY ([affiliate_id]) REFERENCES [dbo].[affiliates] ([affiliate_id])
ALTER TABLE [dbo].[emails]
	CHECK CONSTRAINT [fk_emails_affiliates]

GO
ALTER TABLE [dbo].[emails]
	WITH CHECK
	ADD CONSTRAINT [fk_emails_contracts]
	FOREIGN KEY ([contract_id]) REFERENCES [dbo].[contracts] ([contract_id])
ALTER TABLE [dbo].[emails]
	CHECK CONSTRAINT [fk_emails_contracts]

GO
ALTER TABLE [dbo].[emails]
	WITH CHECK
	ADD CONSTRAINT [fk_emails_merchants]
	FOREIGN KEY ([merchant_id]) REFERENCES [dbo].[merchants] ([merchant_id])
ALTER TABLE [dbo].[emails]
	CHECK CONSTRAINT [fk_emails_merchants]

GO
ALTER TABLE [dbo].[emails]
	WITH CHECK
	ADD CONSTRAINT [fk_emails_users]
	FOREIGN KEY ([sender_id]) REFERENCES [dbo].[users] ([user_id])
ALTER TABLE [dbo].[emails]
	CHECK CONSTRAINT [fk_emails_users]

GO
ALTER TABLE [dbo].[emails] SET (LOCK_ESCALATION = TABLE)
GO
