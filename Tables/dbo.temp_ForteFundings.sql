SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_ForteFundings] (
		[Client_Name]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Consumer_ID]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Frequency]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Next_weekly_payment]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[notes]                   [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[account_number]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[routing_number]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[bank_name]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Payment]                 [numeric](10, 2) NULL,
		[FirstPaymentDate]        [date] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[temp_ForteFundings] SET (LOCK_ESCALATION = TABLE)
GO
