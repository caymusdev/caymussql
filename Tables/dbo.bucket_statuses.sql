SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[bucket_statuses] (
		[bucket_status_id]     [int] IDENTITY(1, 1) NOT NULL,
		[status]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[status_desc]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[parent_status_id]     [int] NULL,
		[create_date]          [datetime] NOT NULL,
		[change_date]          [datetime] NOT NULL,
		[change_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [UQ__bucket_s__A858923C342D422A]
		UNIQUE
		NONCLUSTERED
		([status]),
		CONSTRAINT [pk_bucket_statuses_id]
		PRIMARY KEY
		CLUSTERED
		([bucket_status_id])
)
GO
ALTER TABLE [dbo].[bucket_statuses]
	ADD
	CONSTRAINT [DF__bucket_st__creat__629A9179]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[bucket_statuses]
	ADD
	CONSTRAINT [DF__bucket_st__chang__638EB5B2]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[bucket_statuses]
	WITH CHECK
	ADD CONSTRAINT [fk_bucket_status_bucket_status]
	FOREIGN KEY ([parent_status_id]) REFERENCES [dbo].[bucket_statuses] ([bucket_status_id])
ALTER TABLE [dbo].[bucket_statuses]
	CHECK CONSTRAINT [fk_bucket_status_bucket_status]

GO
ALTER TABLE [dbo].[bucket_statuses] SET (LOCK_ESCALATION = TABLE)
GO
