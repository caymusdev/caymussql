SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transactions] (
		[trans_id]                  [int] IDENTITY(1, 1) NOT NULL,
		[trans_date]                [datetime] NULL,
		[funding_id]                [bigint] NULL,
		[trans_amount]              [money] NULL,
		[comments]                  [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[batch_id]                  [int] NULL,
		[trans_type_id]             [int] NULL,
		[record_id]                 [int] NULL,
		[previous_id]               [int] NULL,
		[redistributed]             [bit] NULL,
		[settle_date]               [datetime] NULL,
		[create_date]               [datetime] NULL,
		[record_type]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[transaction_id]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_id]                [int] NULL,
		[sent_to_acct]              [datetime] NULL,
		[merchant_audit_id]         [bigint] NULL,
		[commission_id]             [int] NULL,
		[redirect_approval_id]      [int] NULL,
		[redirect_from_id]          [int] NULL,
		[calculated]                [bit] NULL,
		[from_access]               [bit] NULL,
		[change_date]               [datetime] NULL,
		[change_user]               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[sent_to_collect]           [datetime] NULL,
		[settle_code]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[sent_to_acct_2]            [datetime] NULL,
		[create_user]               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[source_transaction_id]     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[source_contract_id]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[transaction_type]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[source_merchant_id]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[merchant_id]               [int] NULL,
		[portfolio]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[spv_main_id]               [int] NULL,
		CONSTRAINT [pk_transactions_id]
		PRIMARY KEY
		CLUSTERED
		([trans_id])
	WITH FILLFACTOR=80
)
GO
ALTER TABLE [dbo].[transactions]
	ADD
	CONSTRAINT [DF__transacti__creat__297722B6]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[transactions]
	ADD
	CONSTRAINT [DF__transacti__calcu__2A6B46EF]
	DEFAULT ((0)) FOR [calculated]
GO
ALTER TABLE [dbo].[transactions]
	ADD
	CONSTRAINT [DF__transacti__from___2B5F6B28]
	DEFAULT ((0)) FOR [from_access]
GO
ALTER TABLE [dbo].[transactions]
	ADD
	CONSTRAINT [DF_transactions_change_date]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[transactions]
	WITH CHECK
	ADD CONSTRAINT [fk_transactions_batches]
	FOREIGN KEY ([batch_id]) REFERENCES [dbo].[batches] ([batch_id])
ALTER TABLE [dbo].[transactions]
	CHECK CONSTRAINT [fk_transactions_batches]

GO
ALTER TABLE [dbo].[transactions]
	WITH CHECK
	ADD CONSTRAINT [fk_transactions_trans_type]
	FOREIGN KEY ([trans_type_id]) REFERENCES [dbo].[transaction_types] ([trans_type_id])
ALTER TABLE [dbo].[transactions]
	CHECK CONSTRAINT [fk_transactions_trans_type]

GO
CREATE NONCLUSTERED INDEX [idx_transactions_cv_model_trans]
	ON [dbo].[transactions] ([funding_id], [trans_id], [trans_type_id])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_transactions_funding_id]
	ON [dbo].[transactions] ([funding_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_transactions_merchant_audit]
	ON [dbo].[transactions] ([trans_type_id], [redistributed])
	INCLUDE ([funding_id], [trans_amount])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_transactions_payment_id]
	ON [dbo].[transactions] ([payment_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_transactions_trans_date]
	ON [dbo].[transactions] ([trans_date] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_transactions_transaction_id]
	ON [dbo].[transactions] ([transaction_id])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_transactions_trans_type_id]
	ON [dbo].[transactions] ([trans_type_id])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [nci_wi_transactions_5C67AE2BED2C506924D7D256418AA81B]
	ON [dbo].[transactions] ([batch_id], [redistributed])
	INCLUDE ([funding_id], [trans_amount], [trans_type_id])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[transactions] SET (LOCK_ESCALATION = TABLE)
GO
