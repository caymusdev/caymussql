SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[payment_plan_statuses] (
		[id]                           [int] IDENTITY(1, 1) NOT NULL,
		[payment_plan_id]              [int] NULL,
		[payment_plan_status_code]     [int] NULL,
		[start_date]                   [datetime] NULL,
		[end_date]                     [datetime] NULL,
		[create_date]                  [datetime] NULL,
		[change_date]                  [datetime] NULL,
		[change_user]                  [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]                  [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_payment_plan_statuses_id]
		PRIMARY KEY
		CLUSTERED
		([id])
)
GO
ALTER TABLE [dbo].[payment_plan_statuses]
	ADD
	CONSTRAINT [DF__payment_p__creat__0CDAE408]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[payment_plan_statuses]
	WITH CHECK
	ADD CONSTRAINT [fk_pay_statuses_payment_plan]
	FOREIGN KEY ([payment_plan_id]) REFERENCES [dbo].[payment_plans] ([payment_plan_id])
	ON DELETE CASCADE
ALTER TABLE [dbo].[payment_plan_statuses]
	CHECK CONSTRAINT [fk_pay_statuses_payment_plan]

GO
ALTER TABLE [dbo].[payment_plan_statuses]
	WITH CHECK
	ADD CONSTRAINT [fk_pay_statuses_payment_sched_id]
	FOREIGN KEY ([payment_plan_status_code]) REFERENCES [dbo].[payment_plan_status_codes] ([id])
ALTER TABLE [dbo].[payment_plan_statuses]
	CHECK CONSTRAINT [fk_pay_statuses_payment_sched_id]

GO
CREATE NONCLUSTERED INDEX [idx_pps_dates]
	ON [dbo].[payment_plan_statuses] ([start_date] DESC, [end_date] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_pps_payment_plan_id]
	ON [dbo].[payment_plan_statuses] ([payment_plan_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[payment_plan_statuses] SET (LOCK_ESCALATION = TABLE)
GO
