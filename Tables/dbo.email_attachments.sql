SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[email_attachments] (
		[email_attachment_id]     [int] IDENTITY(1, 1) NOT NULL,
		[email_id]                [int] NULL,
		[attachment]              [varbinary](max) NULL,
		[create_date]             [datetime] NOT NULL,
		[change_date]             [datetime] NOT NULL,
		[change_user]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[attachment_name]         [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_email_attachments_id]
		PRIMARY KEY
		CLUSTERED
		([email_attachment_id] DESC)
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[email_attachments]
	ADD
	CONSTRAINT [DF__email_att__creat__2C738AF2]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[email_attachments]
	ADD
	CONSTRAINT [DF__email_att__chang__2D67AF2B]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[email_attachments]
	WITH CHECK
	ADD CONSTRAINT [fk_email_attachments_emails]
	FOREIGN KEY ([email_id]) REFERENCES [dbo].[emails] ([email_id])
ALTER TABLE [dbo].[email_attachments]
	CHECK CONSTRAINT [fk_email_attachments_emails]

GO
ALTER TABLE [dbo].[email_attachments] SET (LOCK_ESCALATION = TABLE)
GO
