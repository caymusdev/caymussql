SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_funding_info] (
		[Contract_Number]                           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Funded_Date]                               [date] NOT NULL,
		[Legal_Name]                                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Industry]                                  [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Incorporated_State]                        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Time_in_Business__Years_]                  [int] NOT NULL,
		[Merchant_Gross_monthly_Average_Volume]     [money] NOT NULL,
		[FICO]                                      [int] NOT NULL,
		[Payment]                                   [money] NOT NULL,
		[frequency]                                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_temp_funding_info]
		PRIMARY KEY
		CLUSTERED
		([Contract_Number])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[temp_funding_info] SET (LOCK_ESCALATION = TABLE)
GO
