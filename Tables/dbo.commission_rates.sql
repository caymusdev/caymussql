SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[commission_rates] (
		[commission_rate_id]         [int] IDENTITY(1, 1) NOT NULL,
		[collection_type]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[start_date]                 [datetime] NULL,
		[end_date]                   [datetime] NULL,
		[rate]                       [numeric](12, 6) NULL,
		[collector]                  [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[funding_id]                 [bigint] NULL,
		[merchant_id]                [bigint] NULL,
		[affiliate_id]               [bigint] NULL,
		[create_date]                [datetime] NULL,
		[collection_level]           [int] NULL,
		[target_percent]             [numeric](10, 4) NULL,
		[target_commission_rate]     [numeric](10, 4) NULL,
		[change_date]                [datetime] NULL,
		[change_user]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[include_in_stats]           [bit] NULL,
		CONSTRAINT [pk_commission_rates_id]
		PRIMARY KEY
		CLUSTERED
		([commission_rate_id])
)
GO
ALTER TABLE [dbo].[commission_rates]
	ADD
	CONSTRAINT [DF__commissio__creat__5F141958]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[commission_rates]
	ADD
	CONSTRAINT [DF__commissio__inclu__60083D91]
	DEFAULT ((1)) FOR [include_in_stats]
GO
ALTER TABLE [dbo].[commission_rates]
	WITH CHECK
	ADD CONSTRAINT [fk_comm_rates_affiliate_id]
	FOREIGN KEY ([affiliate_id]) REFERENCES [dbo].[affiliates] ([affiliate_id])
ALTER TABLE [dbo].[commission_rates]
	CHECK CONSTRAINT [fk_comm_rates_affiliate_id]

GO
ALTER TABLE [dbo].[commission_rates]
	WITH CHECK
	ADD CONSTRAINT [fk_comm_rates_funding_id]
	FOREIGN KEY ([funding_id]) REFERENCES [dbo].[fundings] ([id])
ALTER TABLE [dbo].[commission_rates]
	CHECK CONSTRAINT [fk_comm_rates_funding_id]

GO
ALTER TABLE [dbo].[commission_rates]
	WITH CHECK
	ADD CONSTRAINT [fk_comm_rates_merchant_id]
	FOREIGN KEY ([merchant_id]) REFERENCES [dbo].[merchants] ([merchant_id])
ALTER TABLE [dbo].[commission_rates]
	CHECK CONSTRAINT [fk_comm_rates_merchant_id]

GO
CREATE NONCLUSTERED INDEX [idx_comm_rates_affiliate_id]
	ON [dbo].[commission_rates] ([affiliate_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_comm_rates_dates]
	ON [dbo].[commission_rates] ([start_date] DESC, [end_date] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_comm_rates_funding_id]
	ON [dbo].[commission_rates] ([funding_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_comm_rates_merchant_id]
	ON [dbo].[commission_rates] ([merchant_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[commission_rates] SET (LOCK_ESCALATION = TABLE)
GO
