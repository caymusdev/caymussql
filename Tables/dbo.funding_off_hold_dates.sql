SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[funding_off_hold_dates] (
		[id]                 [int] IDENTITY(1, 1) NOT NULL,
		[funding_id]         [bigint] NOT NULL,
		[off_hold_date]      [date] NOT NULL,
		[create_date]        [datetime] NULL,
		[change_date]        [datetime] NULL,
		[change_user]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[processed_date]     [datetime] NULL,
		CONSTRAINT [pk_funding_off_hold_dates_id]
		PRIMARY KEY
		CLUSTERED
		([id] DESC)
	ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_funding_off_hold_dates_fundind_ig]
	ON [dbo].[funding_off_hold_dates] ([funding_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[funding_off_hold_dates] SET (LOCK_ESCALATION = TABLE)
GO
