SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[phone_numbers] (
		[phone_number_id]       [int] IDENTITY(1, 1) NOT NULL,
		[phone_type_id]         [int] NULL,
		[phone_number]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contact_id]            [int] NULL,
		[create_date]           [datetime] NOT NULL,
		[change_date]           [datetime] NOT NULL,
		[change_user]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[de_cosigner_rowid]     [int] NULL,
		CONSTRAINT [pk_phone_numbers_id]
		PRIMARY KEY
		CLUSTERED
		([phone_number_id] DESC)
)
GO
ALTER TABLE [dbo].[phone_numbers]
	ADD
	CONSTRAINT [DF__phone_num__creat__36BC0F3B]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[phone_numbers]
	ADD
	CONSTRAINT [DF__phone_num__chang__37B03374]
	DEFAULT (getutcdate()) FOR [change_date]
GO
CREATE NONCLUSTERED INDEX [idx_phone_numbers_contact_type]
	ON [dbo].[phone_numbers] ([phone_number] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[phone_numbers] SET (LOCK_ESCALATION = TABLE)
GO
