SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[contracts] (
		[contract_id]             [bigint] IDENTITY(1, 1) NOT NULL,
		[merchant_id]             [bigint] NULL,
		[source_contract_id]      [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contract_status_id]      [int] NULL,
		[gross_due]               [money] NULL,
		[last_payment_date]       [datetime] NULL,
		[last_payment_amount]     [money] NULL,
		[create_date]             [datetime] NOT NULL,
		[change_date]             [datetime] NOT NULL,
		[change_user]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contract_number]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[fees_outstanding]        [money] NULL,
		[original_rtr]            [money] NULL,
		[pp_outstanding]          [money] NULL,
		[writeoff_date]           [datetime] NULL,
		[collect_status]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_contracts_id]
		PRIMARY KEY
		CLUSTERED
		([contract_id] DESC)
)
GO
ALTER TABLE [dbo].[contracts]
	ADD
	CONSTRAINT [DF__contracts__creat__353DDB1D]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[contracts]
	ADD
	CONSTRAINT [DF__contracts__chang__3631FF56]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[contracts]
	WITH CHECK
	ADD CONSTRAINT [fk_contracts_contract_statuses_status]
	FOREIGN KEY ([contract_status_id]) REFERENCES [dbo].[contract_statuses] ([contract_status_id])
ALTER TABLE [dbo].[contracts]
	CHECK CONSTRAINT [fk_contracts_contract_statuses_status]

GO
ALTER TABLE [dbo].[contracts] SET (LOCK_ESCALATION = TABLE)
GO
