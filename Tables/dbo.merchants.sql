SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[merchants] (
		[merchant_id]            [bigint] NOT NULL,
		[merchant_name]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[affiliate_id]           [bigint] NOT NULL,
		[MID]                    [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[status]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]            [datetime] NULL,
		[temp_id]                [bigint] IDENTITY(1, 1) NOT NULL,
		[change_date]            [datetime] NULL,
		[change_user]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[source_merchant_id]     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_merchants_id]
		PRIMARY KEY
		CLUSTERED
		([merchant_id])
)
GO
ALTER TABLE [dbo].[merchants]
	ADD
	CONSTRAINT [DF__merchants__creat__062DE679]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[merchants]
	WITH CHECK
	ADD CONSTRAINT [fk_merchants_affiliate]
	FOREIGN KEY ([affiliate_id]) REFERENCES [dbo].[affiliates] ([affiliate_id])
ALTER TABLE [dbo].[merchants]
	CHECK CONSTRAINT [fk_merchants_affiliate]

GO
CREATE NONCLUSTERED INDEX [IX_merchants_MID]
	ON [dbo].[merchants] ([MID])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[merchants] SET (LOCK_ESCALATION = TABLE)
GO
