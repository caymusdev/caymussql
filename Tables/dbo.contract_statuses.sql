SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[contract_statuses] (
		[contract_status_id]       [int] IDENTITY(1, 1) NOT NULL,
		[contract_status]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[status_desc]              [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]              [datetime] NOT NULL,
		[change_date]              [datetime] NULL,
		[change_user]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contract_status_desc]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_contract_status_id]
		PRIMARY KEY
		CLUSTERED
		([contract_status_id] DESC)
)
GO
ALTER TABLE [dbo].[contract_statuses]
	ADD
	CONSTRAINT [DF__contract___creat__62E4AA3C]
	DEFAULT (getutcdate()) FOR [create_date]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_contract_status_contract_status]
	ON [dbo].[contract_statuses] ([contract_status])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[contract_statuses] SET (LOCK_ESCALATION = TABLE)
GO
