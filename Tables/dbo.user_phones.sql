SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user_phones] (
		[user_phone_id]     [int] IDENTITY(1, 1) NOT NULL,
		[user_id]           [int] NOT NULL,
		[phone_number]      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[phone_type]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[is_primary]        [bit] NOT NULL,
		[create_date]       [datetime] NOT NULL,
		[change_date]       [datetime] NOT NULL,
		[change_user]       [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]       [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_user_phone_id_id]
		PRIMARY KEY
		CLUSTERED
		([user_phone_id])
)
GO
ALTER TABLE [dbo].[user_phones]
	ADD
	CONSTRAINT [DF__user_phon__is_pr__4F87BD05]
	DEFAULT ((0)) FOR [is_primary]
GO
ALTER TABLE [dbo].[user_phones]
	ADD
	CONSTRAINT [DF__user_phon__creat__507BE13E]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[user_phones]
	ADD
	CONSTRAINT [DF__user_phon__chang__51700577]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[user_phones]
	WITH CHECK
	ADD CONSTRAINT [fk_user_phone_users]
	FOREIGN KEY ([user_id]) REFERENCES [dbo].[users] ([user_id])
ALTER TABLE [dbo].[user_phones]
	CHECK CONSTRAINT [fk_user_phone_users]

GO
ALTER TABLE [dbo].[user_phones] SET (LOCK_ESCALATION = TABLE)
GO
