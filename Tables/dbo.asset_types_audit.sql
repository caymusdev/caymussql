SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[asset_types_audit] (
		[audit_id]           [int] IDENTITY(1, 1) NOT NULL,
		[asset_types_id]     [int] NULL,
		[change_date]        [datetime] NOT NULL,
		[change_user]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]        [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[create_user]        [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_asset_types_audit_audit_id]
		PRIMARY KEY
		CLUSTERED
		([audit_id])
)
GO
ALTER TABLE [dbo].[asset_types_audit]
	ADD
	CONSTRAINT [DF__asset_typ__chang__7C8F6DA6]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[asset_types_audit]
	ADD
	CONSTRAINT [DF__asset_typ__creat__7D8391DF]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[asset_types_audit] SET (LOCK_ESCALATION = TABLE)
GO
