SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[forte_fundings] (
		[forte_funding_id]      [int] IDENTITY(1, 1) NOT NULL,
		[funding_id]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[status]                [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[effective_date]        [datetime] NULL,
		[origination_date]      [datetime] NULL,
		[net_amount]            [money] NULL,
		[routing_number]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[entry_description]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[batch_id]              [int] NULL,
		[create_date]           [datetime] NULL,
		[change_date]           [datetime] NULL,
		[change_user]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_forte_fundings_id]
		PRIMARY KEY
		CLUSTERED
		([forte_funding_id] DESC)
)
GO
ALTER TABLE [dbo].[forte_fundings]
	ADD
	CONSTRAINT [DF__forte_fun__creat__6D6238AF]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[forte_fundings]
	WITH CHECK
	ADD CONSTRAINT [fk_forte_fundings_batches]
	FOREIGN KEY ([batch_id]) REFERENCES [dbo].[batches] ([batch_id])
	ON DELETE CASCADE
ALTER TABLE [dbo].[forte_fundings]
	CHECK CONSTRAINT [fk_forte_fundings_batches]

GO
CREATE NONCLUSTERED INDEX [ixd_forte_fundings_batch_id]
	ON [dbo].[forte_fundings] ([batch_id])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[forte_fundings] SET (LOCK_ESCALATION = TABLE)
GO
