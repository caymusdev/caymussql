SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[collector_cases] (
		[collector_case_id]     [int] IDENTITY(1, 1) NOT NULL,
		[collector_user_id]     [int] NULL,
		[case_id]               [int] NULL,
		[start_date]            [datetime] NULL,
		[end_date]              [datetime] NULL,
		[create_date]           [datetime] NOT NULL,
		[change_date]           [datetime] NOT NULL,
		[change_user]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_collector_cases_id]
		PRIMARY KEY
		CLUSTERED
		([collector_case_id] DESC)
)
GO
ALTER TABLE [dbo].[collector_cases]
	ADD
	CONSTRAINT [DF__collector__creat__0DEF03D2]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[collector_cases]
	ADD
	CONSTRAINT [DF__collector__chang__0EE3280B]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[collector_cases]
	WITH CHECK
	ADD CONSTRAINT [fk_collector_cases_cases]
	FOREIGN KEY ([case_id]) REFERENCES [dbo].[cases] ([case_id])
ALTER TABLE [dbo].[collector_cases]
	CHECK CONSTRAINT [fk_collector_cases_cases]

GO
ALTER TABLE [dbo].[collector_cases]
	WITH CHECK
	ADD CONSTRAINT [fk_collector_cases_users]
	FOREIGN KEY ([collector_user_id]) REFERENCES [dbo].[users] ([user_id])
ALTER TABLE [dbo].[collector_cases]
	CHECK CONSTRAINT [fk_collector_cases_users]

GO
CREATE NONCLUSTERED INDEX [idx_collector_cases]
	ON [dbo].[collector_cases] ([collector_user_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_collector_cases_case]
	ON [dbo].[collector_cases] ([case_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[collector_cases] SET (LOCK_ESCALATION = TABLE)
GO
