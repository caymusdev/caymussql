SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user_departments] (
		[user_department_id]     [int] IDENTITY(1, 1) NOT NULL,
		[user_id]                [int] NOT NULL,
		[department_id]          [int] NOT NULL,
		[create_date]            [datetime] NOT NULL,
		[change_date]            [datetime] NOT NULL,
		[change_user]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_user_departments_id]
		PRIMARY KEY
		CLUSTERED
		([user_department_id] DESC)
)
GO
ALTER TABLE [dbo].[user_departments]
	ADD
	CONSTRAINT [DF__user_depa__creat__6F357288]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[user_departments]
	ADD
	CONSTRAINT [DF__user_depa__chang__702996C1]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[user_departments]
	WITH CHECK
	ADD CONSTRAINT [fk_user_departments_department]
	FOREIGN KEY ([department_id]) REFERENCES [dbo].[departments] ([department_id])
ALTER TABLE [dbo].[user_departments]
	CHECK CONSTRAINT [fk_user_departments_department]

GO
ALTER TABLE [dbo].[user_departments]
	WITH CHECK
	ADD CONSTRAINT [fk_user_departments_users]
	FOREIGN KEY ([user_id]) REFERENCES [dbo].[users] ([user_id])
ALTER TABLE [dbo].[user_departments]
	CHECK CONSTRAINT [fk_user_departments_users]

GO
CREATE NONCLUSTERED INDEX [idx_user_departments_user]
	ON [dbo].[user_departments] ([user_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_user_departments_dept]
	ON [dbo].[user_departments] ([department_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[user_departments] SET (LOCK_ESCALATION = TABLE)
GO
