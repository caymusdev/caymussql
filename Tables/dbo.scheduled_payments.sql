SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[scheduled_payments] (
		[scheduled_payment_id]            [int] IDENTITY(1, 1) NOT NULL,
		[source_scheduled_payment_id]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_method]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_date]                    [date] NULL,
		[payment_amount]                  [money] NULL,
		[source_contract_id]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contract_id]                     [bigint] NULL,
		[source_merchant_id]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[merchant_id]                     [bigint] NULL,
		[source_affiliate_id]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[affiliate_id]                    [bigint] NULL,
		[is_debit]                        [bit] NOT NULL,
		[bank_name]                       [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[routing_number]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[account_number]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[changed]                         [bit] NULL,
		[exported]                        [datetime] NULL,
		[create_date]                     [datetime] NOT NULL,
		[change_date]                     [datetime] NOT NULL,
		[change_user]                     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]                     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_plan_id]                 [int] NULL,
		[approved_date]                   [datetime] NULL,
		[approved_user]                   [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[comments]                        [nvarchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[deleted]                         [bit] NOT NULL,
		[change_reason_id]                [int] NULL,
		[rejected_user]                   [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[rejected_date]                   [datetime] NULL,
		[funding_payment_id]              [int] NULL,
		[merchant_bank_account_id]        [int] NULL,
		[reject_reason]                   [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_scheduled_payment_id_id]
		PRIMARY KEY
		CLUSTERED
		([scheduled_payment_id])
)
GO
ALTER TABLE [dbo].[scheduled_payments]
	ADD
	CONSTRAINT [DF__scheduled__is_de__3E5D3103]
	DEFAULT ((1)) FOR [is_debit]
GO
ALTER TABLE [dbo].[scheduled_payments]
	ADD
	CONSTRAINT [DF__scheduled__creat__3F51553C]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[scheduled_payments]
	ADD
	CONSTRAINT [DF__scheduled__chang__40457975]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[scheduled_payments]
	ADD
	CONSTRAINT [DF__scheduled__delet__644DCFC1]
	DEFAULT ((0)) FOR [deleted]
GO
ALTER TABLE [dbo].[scheduled_payments]
	WITH CHECK
	ADD CONSTRAINT [fk_scheduled_affiliates]
	FOREIGN KEY ([affiliate_id]) REFERENCES [dbo].[affiliates] ([affiliate_id])
ALTER TABLE [dbo].[scheduled_payments]
	CHECK CONSTRAINT [fk_scheduled_affiliates]

GO
ALTER TABLE [dbo].[scheduled_payments]
	WITH CHECK
	ADD CONSTRAINT [fk_scheduled_contracts]
	FOREIGN KEY ([contract_id]) REFERENCES [dbo].[contracts] ([contract_id])
ALTER TABLE [dbo].[scheduled_payments]
	CHECK CONSTRAINT [fk_scheduled_contracts]

GO
ALTER TABLE [dbo].[scheduled_payments]
	WITH CHECK
	ADD CONSTRAINT [fk_scheduled_payments_merchants]
	FOREIGN KEY ([merchant_id]) REFERENCES [dbo].[merchants] ([merchant_id])
ALTER TABLE [dbo].[scheduled_payments]
	CHECK CONSTRAINT [fk_scheduled_payments_merchants]

GO
ALTER TABLE [dbo].[scheduled_payments]
	WITH CHECK
	ADD CONSTRAINT [fk_scheduled_payments_paymet_plan_id]
	FOREIGN KEY ([payment_plan_id]) REFERENCES [dbo].[payment_plans] ([payment_plan_id])
ALTER TABLE [dbo].[scheduled_payments]
	CHECK CONSTRAINT [fk_scheduled_payments_paymet_plan_id]

GO
CREATE NONCLUSTERED INDEX [idx_source_payment_id]
	ON [dbo].[scheduled_payments] ([source_scheduled_payment_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_source_funding_id]
	ON [dbo].[scheduled_payments] ([source_contract_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_funding_id]
	ON [dbo].[scheduled_payments] ([contract_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[scheduled_payments] SET (LOCK_ESCALATION = TABLE)
GO
