SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[contact_types] (
		[contact_type_id]       [int] IDENTITY(1, 1) NOT NULL,
		[contact_type]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[contact_type_desc]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]           [datetime] NOT NULL,
		[change_date]           [datetime] NOT NULL,
		[change_user]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_contact_types_id]
		PRIMARY KEY
		CLUSTERED
		([contact_type_id])
)
GO
ALTER TABLE [dbo].[contact_types]
	ADD
	CONSTRAINT [DF__contact_t__creat__15C52FC4]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[contact_types]
	ADD
	CONSTRAINT [DF__contact_t__chang__16B953FD]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[contact_types] SET (LOCK_ESCALATION = TABLE)
GO
