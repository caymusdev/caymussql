SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[partial_writeoffs] (
		[partial_writeoff_id]      [int] IDENTITY(1, 1) NOT NULL,
		[funding_id]               [bigint] NOT NULL,
		[trans_date]               [date] NOT NULL,
		[writeoff_amount]          [money] NOT NULL,
		[margin_adjust_amount]     [money] NOT NULL,
		[processed]                [datetime] NULL,
		[create_date]              [datetime] NOT NULL,
		[change_date]              [datetime] NOT NULL,
		[change_user]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_partial_writeoffs_id]
		PRIMARY KEY
		CLUSTERED
		([partial_writeoff_id])
	WITH FILLFACTOR=80
)
GO
ALTER TABLE [dbo].[partial_writeoffs]
	ADD
	CONSTRAINT [DF__partial_w__creat__2BBF5DDB]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[partial_writeoffs]
	ADD
	CONSTRAINT [DF__partial_w__chang__2CB38214]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[partial_writeoffs]
	WITH CHECK
	ADD CONSTRAINT [fk_writeoffs_fundings]
	FOREIGN KEY ([funding_id]) REFERENCES [dbo].[fundings] ([id])
ALTER TABLE [dbo].[partial_writeoffs]
	CHECK CONSTRAINT [fk_writeoffs_fundings]

GO
CREATE NONCLUSTERED INDEX [idx_trans_date]
	ON [dbo].[partial_writeoffs] ([trans_date])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_funding_id]
	ON [dbo].[partial_writeoffs] ([funding_id])
	ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_unique_funding_id]
	ON [dbo].[partial_writeoffs] ([funding_id], [trans_date])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[partial_writeoffs] SET (LOCK_ESCALATION = TABLE)
GO
