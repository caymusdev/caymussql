SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[merchants_lookup] (
		[solo_id]           [bigint] NULL,
		[merchant_name]     [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[affiliate_id]      [bigint] NULL,
		[portfolio_id]      [int] NULL,
		[createDate]        [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[merchants_lookup] SET (LOCK_ESCALATION = TABLE)
GO
