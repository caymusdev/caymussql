SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[processed_imports] (
		[processed_import_id]               [int] IDENTITY(1, 1) NOT NULL,
		[affiliates_added]                  [int] NULL,
		[merchants_added]                   [int] NULL,
		[contracts_added]                   [int] NULL,
		[bad_contract_records]              [int] NULL,
		[transactions_added]                [int] NULL,
		[bad_trans_records]                 [int] NULL,
		[contacts_added]                    [int] NULL,
		[contact_addresses_added]           [int] NULL,
		[contact_phones_added]              [int] NULL,
		[bad_contact_records]               [int] NULL,
		[bad_contact_phones_records]        [int] NULL,
		[bad_contact_addresses_records]     [int] NULL,
		[scheduled_payments_added]          [int] NULL,
		[bad_scheduled_payment_records]     [int] NULL,
		[bank_accounts_added]               [int] NULL,
		[bad_bank_account_records]          [int] NULL,
		[create_date]                       [datetime] NOT NULL,
		[change_date]                       [datetime] NOT NULL,
		[change_user]                       [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]                       [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_processed_imports_id]
		PRIMARY KEY
		CLUSTERED
		([processed_import_id] DESC)
)
GO
ALTER TABLE [dbo].[processed_imports]
	ADD
	CONSTRAINT [DF__processed__creat__3B4BBA2E]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[processed_imports]
	ADD
	CONSTRAINT [DF__processed__chang__3C3FDE67]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[processed_imports] SET (LOCK_ESCALATION = TABLE)
GO
