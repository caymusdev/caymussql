SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[email_addresses] (
		[email_address_id]     [int] IDENTITY(1, 1) NOT NULL,
		[email_id]             [int] NULL,
		[email_address]        [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[address_type]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]          [datetime] NOT NULL,
		[change_date]          [datetime] NOT NULL,
		[change_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_email_addresses_id]
		PRIMARY KEY
		CLUSTERED
		([email_address_id] DESC)
)
GO
ALTER TABLE [dbo].[email_addresses]
	ADD
	CONSTRAINT [DF__email_add__creat__23DE44F1]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[email_addresses]
	ADD
	CONSTRAINT [DF__email_add__chang__24D2692A]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[email_addresses]
	WITH CHECK
	ADD CONSTRAINT [fk_email_addresses_emails]
	FOREIGN KEY ([email_id]) REFERENCES [dbo].[emails] ([email_id])
ALTER TABLE [dbo].[email_addresses]
	CHECK CONSTRAINT [fk_email_addresses_emails]

GO
CREATE NONCLUSTERED INDEX [idx_email_addresses_email_address]
	ON [dbo].[email_addresses] ([email_address])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[email_addresses] SET (LOCK_ESCALATION = TABLE)
GO
