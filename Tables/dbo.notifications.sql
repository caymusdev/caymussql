SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[notifications] (
		[notification_id]          [int] IDENTITY(1, 1) NOT NULL,
		[user_id]                  [int] NULL,
		[affiliate_id]             [bigint] NULL,
		[merchant_id]              [bigint] NULL,
		[contract_id]              [bigint] NULL,
		[notification_message]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[notification_type_id]     [int] NULL,
		[displayed_time]           [datetime] NULL,
		[acted_on_time]            [datetime] NULL,
		[create_date]              [datetime] NOT NULL,
		[change_date]              [datetime] NOT NULL,
		[change_user]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[record_id]                [int] NULL,
		CONSTRAINT [pk_notifications_id]
		PRIMARY KEY
		CLUSTERED
		([notification_id] DESC)
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[notifications]
	ADD
	CONSTRAINT [DF__notificat__creat__5B2E79DB]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[notifications]
	ADD
	CONSTRAINT [DF__notificat__chang__5C229E14]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[notifications]
	WITH CHECK
	ADD CONSTRAINT [fk_notifications_affiliates]
	FOREIGN KEY ([affiliate_id]) REFERENCES [dbo].[affiliates] ([affiliate_id])
ALTER TABLE [dbo].[notifications]
	CHECK CONSTRAINT [fk_notifications_affiliates]

GO
ALTER TABLE [dbo].[notifications]
	WITH CHECK
	ADD CONSTRAINT [fk_notifications_contracts]
	FOREIGN KEY ([contract_id]) REFERENCES [dbo].[contracts] ([contract_id])
ALTER TABLE [dbo].[notifications]
	CHECK CONSTRAINT [fk_notifications_contracts]

GO
ALTER TABLE [dbo].[notifications]
	WITH CHECK
	ADD CONSTRAINT [fk_notifications_merchants]
	FOREIGN KEY ([merchant_id]) REFERENCES [dbo].[merchants] ([merchant_id])
ALTER TABLE [dbo].[notifications]
	CHECK CONSTRAINT [fk_notifications_merchants]

GO
ALTER TABLE [dbo].[notifications]
	WITH CHECK
	ADD CONSTRAINT [fk_notifications_users]
	FOREIGN KEY ([user_id]) REFERENCES [dbo].[users] ([user_id])
ALTER TABLE [dbo].[notifications]
	CHECK CONSTRAINT [fk_notifications_users]

GO
ALTER TABLE [dbo].[notifications]
	WITH CHECK
	ADD CONSTRAINT [fk_notifications_notifiction_types]
	FOREIGN KEY ([notification_type_id]) REFERENCES [dbo].[notification_types] ([notification_type_id])
ALTER TABLE [dbo].[notifications]
	CHECK CONSTRAINT [fk_notifications_notifiction_types]

GO
CREATE NONCLUSTERED INDEX [idx_notifications_user]
	ON [dbo].[notifications] ([user_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_notifications_affiliate]
	ON [dbo].[notifications] ([affiliate_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[notifications] SET (LOCK_ESCALATION = TABLE)
GO
