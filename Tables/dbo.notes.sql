SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[notes] (
		[notes_id]            [int] IDENTITY(1, 1) NOT NULL,
		[affiliate_id]        [bigint] NULL,
		[merchant_id]         [bigint] NULL,
		[contract_id]         [bigint] NULL,
		[notes_text]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[important_notes]     [bit] NOT NULL,
		[change_date]         [datetime] NOT NULL,
		[change_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]         [datetime] NOT NULL,
		[create_user]         [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_notes_notes_id]
		PRIMARY KEY
		CLUSTERED
		([notes_id])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[notes]
	ADD
	CONSTRAINT [DF__notes__important__1837881B]
	DEFAULT ((0)) FOR [important_notes]
GO
ALTER TABLE [dbo].[notes]
	ADD
	CONSTRAINT [DF__notes__change_da__192BAC54]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[notes]
	ADD
	CONSTRAINT [DF__notes__create_da__1A1FD08D]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[notes]
	WITH CHECK
	ADD CONSTRAINT [fk_notes_affiliate_id]
	FOREIGN KEY ([affiliate_id]) REFERENCES [dbo].[affiliates] ([affiliate_id])
ALTER TABLE [dbo].[notes]
	CHECK CONSTRAINT [fk_notes_affiliate_id]

GO
ALTER TABLE [dbo].[notes]
	WITH CHECK
	ADD CONSTRAINT [fk_notes_merchant_id]
	FOREIGN KEY ([merchant_id]) REFERENCES [dbo].[merchants] ([merchant_id])
ALTER TABLE [dbo].[notes]
	CHECK CONSTRAINT [fk_notes_merchant_id]

GO
ALTER TABLE [dbo].[notes]
	WITH CHECK
	ADD CONSTRAINT [fk_notes_contract_id]
	FOREIGN KEY ([contract_id]) REFERENCES [dbo].[contracts] ([contract_id])
ALTER TABLE [dbo].[notes]
	CHECK CONSTRAINT [fk_notes_contract_id]

GO
ALTER TABLE [dbo].[notes] SET (LOCK_ESCALATION = TABLE)
GO
