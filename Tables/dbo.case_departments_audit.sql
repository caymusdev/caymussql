SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[case_departments_audit] (
		[audit_id]               [int] IDENTITY(1, 1) NOT NULL,
		[case_department_id]     [int] NULL,
		[change_date]            [datetime] NOT NULL,
		[change_user]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_category]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[field]                  [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[previous_value]         [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[changed_to_value]       [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_case_departments_audit_id]
		PRIMARY KEY
		CLUSTERED
		([audit_id] DESC)
)
GO
ALTER TABLE [dbo].[case_departments_audit]
	ADD
	CONSTRAINT [DF__case_depa__chang__70E8B0D0]
	DEFAULT (getutcdate()) FOR [change_date]
GO
CREATE NONCLUSTERED INDEX [idx_case_departments_audit_change_date]
	ON [dbo].[case_departments_audit] ([change_date] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[case_departments_audit] SET (LOCK_ESCALATION = TABLE)
GO
