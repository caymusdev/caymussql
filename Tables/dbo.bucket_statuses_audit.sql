SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[bucket_statuses_audit] (
		[audit_id]             [int] IDENTITY(1, 1) NOT NULL,
		[bucket_status_id]     [int] NULL,
		[change_date]          [datetime] NOT NULL,
		[change_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_category]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[field]                [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[previous_value]       [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[changed_to_value]     [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_bucket_statuses_audit_id]
		PRIMARY KEY
		CLUSTERED
		([audit_id])
)
GO
ALTER TABLE [dbo].[bucket_statuses_audit]
	ADD
	CONSTRAINT [DF__bucket_st__chang__675F4696]
	DEFAULT (getutcdate()) FOR [change_date]
GO
CREATE NONCLUSTERED INDEX [idx_bucket_statuses_audit_change_date]
	ON [dbo].[bucket_statuses_audit] ([change_date] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[bucket_statuses_audit] SET (LOCK_ESCALATION = TABLE)
GO
