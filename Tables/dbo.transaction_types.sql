SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transaction_types] (
		[trans_type_id]         [int] NOT NULL,
		[trans_type]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[trans_description]     [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]           [datetime] NOT NULL,
		[change_date]           [datetime] NULL,
		[change_user]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[debit_account]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[credit_account]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[main_flag]             [bit] NULL,
		[detail_flag]           [bit] NULL,
		CONSTRAINT [pk_transaction_type_id]
		PRIMARY KEY
		CLUSTERED
		([trans_type_id])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[transaction_types]
	ADD
	CONSTRAINT [DF__transacti__creat__1F63A897]
	DEFAULT (getutcdate()) FOR [create_date]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_transactions_trans_type]
	ON [dbo].[transaction_types] ([trans_type])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[transaction_types] SET (LOCK_ESCALATION = TABLE)
GO
