SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[boa_staging] (
		[boa_staging_id]     [int] IDENTITY(1, 1) NOT NULL,
		[Date]               [datetime] NULL,
		[Description]        [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Amount]             [money] NULL,
		[Running Bal]        [money] NULL,
		[create_date]        [datetime] NULL,
		[BatchID]            [int] NULL,
		CONSTRAINT [pk_boa_staging_id]
		PRIMARY KEY
		CLUSTERED
		([boa_staging_id] DESC)
)
GO
ALTER TABLE [dbo].[boa_staging]
	ADD
	CONSTRAINT [DF__boa_stagi__creat__5E1FF51F]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[boa_staging]
	WITH NOCHECK
	ADD CONSTRAINT [fk_boa_staging_batches]
	FOREIGN KEY ([BatchID]) REFERENCES [dbo].[batches] ([batch_id])
	ON DELETE CASCADE
ALTER TABLE [dbo].[boa_staging]
	CHECK CONSTRAINT [fk_boa_staging_batches]

GO
ALTER TABLE [dbo].[boa_staging] SET (LOCK_ESCALATION = TABLE)
GO
