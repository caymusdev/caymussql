SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[collect_blobs] (
		[cm_sync_db_id]         [int] NOT NULL,
		[rb_created_date]       [datetime] NOT NULL,
		[rb_changed_date]       [datetime] NOT NULL,
		[bl_rowid]              [bigint] NOT NULL,
		[bl_type]               [int] NULL,
		[bl_blob]               [varbinary](max) NULL,
		[bl_date]               [date] NULL,
		[bl_time]               [time](7) NULL,
		[bl_status]             [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[bl_opid]               [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[bl_text]               [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[bl_sticky]             [tinyint] NULL,
		[bl_rowid_debtor]       [bigint] NULL,
		[bl_rowid_a_debtor]     [bigint] NULL,
		[bl_rowid_client]       [bigint] NULL,
		[bl_rowid_a_client]     [bigint] NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[collect_blobs] SET (LOCK_ESCALATION = TABLE)
GO
