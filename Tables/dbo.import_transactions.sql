SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[import_transactions] (
		[transaction_id]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[transaction_type]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[transaction_date]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[transaction_amount]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_id]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contract_id]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[merchant_id]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[import_id]              [int] IDENTITY(1, 1) NOT NULL,
		[create_date]            [datetime] NOT NULL,
		[change_date]            [datetime] NOT NULL,
		[change_user]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[processed_date]         [datetime] NULL,
		[bad_record]             [bit] NULL,
		[settle_code]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_import_transasctions_id]
		PRIMARY KEY
		CLUSTERED
		([import_id])
)
GO
ALTER TABLE [dbo].[import_transactions]
	ADD
	CONSTRAINT [DF__import_tr__creat__438BFA74]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[import_transactions]
	ADD
	CONSTRAINT [DF__import_tr__chang__44801EAD]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[import_transactions]
	ADD
	CONSTRAINT [DF__import_tr__chang__457442E6]
	DEFAULT ('SYSTEM') FOR [change_user]
GO
ALTER TABLE [dbo].[import_transactions]
	ADD
	CONSTRAINT [DF__import_tr__creat__4668671F]
	DEFAULT ('SYSTEM') FOR [create_user]
GO
CREATE NONCLUSTERED INDEX [idx_import_transactions_transaction_id]
	ON [dbo].[import_transactions] ([contract_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[import_transactions] SET (LOCK_ESCALATION = TABLE)
GO
