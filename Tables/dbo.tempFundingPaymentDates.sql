SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tempFundingPaymentDates] (
		[id]                             [bigint] IDENTITY(1, 1) NOT NULL,
		[funding_id]                     [bigint] NOT NULL,
		[payment_date]                   [date] NOT NULL,
		[amount]                         [money] NOT NULL,
		[rec_amount]                     [money] NULL,
		[payment_schedule_id]            [int] NULL,
		[create_date]                    [datetime] NULL,
		[active]                         [bit] NULL,
		[original]                       [bit] NULL,
		[chargeback_amount]              [money] NULL,
		[merchant_bank_account_id]       [int] NULL,
		[payment_method]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_processor]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_schedule_change_id]     [int] NULL,
		[comments]                       [nvarchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_reason_id]               [int] NULL,
		[change_date]                    [datetime] NULL,
		[change_user]                    [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_plan_id]                [int] NULL,
		[missed]                         [bit] NULL,
		[rec_trans_id]                   [int] NULL,
		[chargeback_trans_id]            [int] NULL,
		[reject_trans_id]                [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tempFundingPaymentDates] SET (LOCK_ESCALATION = TABLE)
GO
