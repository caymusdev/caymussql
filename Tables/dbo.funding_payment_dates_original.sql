SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[funding_payment_dates_original] (
		[id]               [bigint] IDENTITY(1, 1) NOT NULL,
		[funding_id]       [bigint] NOT NULL,
		[payment_date]     [date] NOT NULL,
		[amount]           [money] NOT NULL,
		[rec_amount]       [money] NOT NULL,
		[create_date]      [datetime] NULL,
		CONSTRAINT [pk_funding_payment_dates_original]
		PRIMARY KEY
		CLUSTERED
		([id])
)
GO
ALTER TABLE [dbo].[funding_payment_dates_original]
	ADD
	CONSTRAINT [DF__funding_p__creat__79C80F94]
	DEFAULT (getutcdate()) FOR [create_date]
GO
CREATE NONCLUSTERED INDEX [idx_funding_payment_dates_original_main]
	ON [dbo].[funding_payment_dates_original] ([funding_id], [payment_date] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[funding_payment_dates_original] SET (LOCK_ESCALATION = TABLE)
GO
