SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fundings] (
		[id]                                        [bigint] NOT NULL,
		[funding_id]                                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[affiliate_id]                              [bigint] NULL,
		[merchant_id]                               [bigint] NULL,
		[contract_number]                           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[legal_name]                                [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contract_type]                             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[funded_date]                               [datetime] NULL,
		[purchase_price]                            [money] NULL,
		[portfolio_value]                           [money] NULL,
		[unearned_income]                           [money] NULL,
		[pricing_ratio]                             [numeric](10, 4) NULL,
		[commission_percent]                        [numeric](10, 4) NULL,
		[commission_amount]                         [money] NULL,
		[estimated_turn]                            [numeric](10, 2) NULL,
		[upfront_total_commission]                  [money] NULL,
		[upfront_commission_percent]                [numeric](10, 2) NULL,
		[calc_turn]                                 [numeric](10, 2) NULL,
		[contract_status]                           [int] NULL,
		[payoff_amount]                             [money] NULL,
		[net_to_merchant]                           [money] NULL,
		[apply_to_contract]                         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[apply_to_date]                             [datetime] NULL,
		[frequency]                                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[weekday_payment]                           [money] NULL,
		[competitor_payoff]                         [money] NULL,
		[receivables_bank_name]                     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[receivables_account_nbr]                   [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[receivables_aba]                           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[weekly_payment]                            [money] NULL,
		[create_date]                               [datetime] NOT NULL,
		[temp_id]                                   [int] IDENTITY(1, 1) NOT NULL,
		[processor]                                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[paid_off_amount]                           [money] NULL,
		[actual_turn]                               [numeric](10, 2) NULL,
		[last_payment_date]                         [datetime] NULL,
		[final_payment_date]                        [datetime] NULL,
		[rtr_out]                                   [money] NULL,
		[pp_out]                                    [money] NULL,
		[margin_out]                                [money] NULL,
		[fees_out]                                  [money] NULL,
		[percent_paid]                              [numeric](10, 2) NULL,
		[percent_performance]                       [numeric](10, 2) NULL,
		[change_date]                               [datetime] NULL,
		[change_user]                               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[on_hold]                                   [bit] NULL,
		[collection_type]                           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[writeoff_date]                             [datetime] NULL,
		[orig_fee_percent]                          [numeric](16, 2) NULL,
		[orig_fee]                                  [money] NULL,
		[float_amount]                              [money] NULL,
		[commission_iso]                            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[completed_date]                            [datetime] NULL,
		[wire_bank_name]                            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[wire_account_nbr]                          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[wire_aba]                                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payments_stopped_date]                     [datetime] NULL,
		[first_payment_date]                        [datetime] NULL,
		[project_name]                              [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[referral_iso]                              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[referral_commission]                       [money] NULL,
		[wiring_instructions]                       [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[est_months_left]                           [numeric](10, 2) NULL,
		[paid_calc_turn]                            [numeric](10, 2) NULL,
		[performance_status_id]                     [int] NULL,
		[contract_substatus_id]                     [int] NULL,
		[industry]                                  [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[incorp_state]                              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[years_in_business]                         [numeric](18, 2) NULL,
		[monthly_avg_vol]                           [money] NULL,
		[fico]                                      [int] NULL,
		[eligible]                                  [bit] NULL,
		[hard_offer_type]                           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_split]                                  [numeric](12, 6) NULL,
		[net_est_yield]                             [numeric](12, 5) NULL,
		[discount]                                  [money] NULL,
		[previous_funding_id]                       [bigint] NULL,
		[never_redistribute]                        [bit] NULL,
		[refund_out]                                [money] NULL,
		[sent_to_acct]                              [datetime] NULL,
		[process_fees]                              [bit] NULL,
		[sent_to_collect]                           [datetime] NULL,
		[closed_in_collections]                     [datetime] NULL,
		[do_retries]                                [bit] NULL,
		[cash_to_pp]                                [money] NULL,
		[cash_to_margin]                            [money] NULL,
		[cash_fees]                                 [money] NULL,
		[counter_deposit_revenue]                   [money] NULL,
		[ach_received]                              [money] NULL,
		[cc_revenue]                                [money] NULL,
		[wires_revenue]                             [money] NULL,
		[wires_revenue_no_renewal]                  [money] NULL,
		[discount_received]                         [money] NULL,
		[fees_applied]                              [money] NULL,
		[settlement_received]                       [money] NULL,
		[writeoff_received]                         [money] NULL,
		[cash_to_chargeback]                        [money] NULL,
		[cash_to_bad_debt]                          [money] NULL,
		[cash_to_reapplication]                     [money] NULL,
		[writeoff_adjustment_received]              [money] NULL,
		[ach_draft]                                 [money] NULL,
		[checks_revenue]                            [money] NULL,
		[ach_reject]                                [money] NULL,
		[refund]                                    [money] NULL,
		[refund_processed]                          [money] NULL,
		[refund_reapplied]                          [money] NULL,
		[refund_returned]                           [money] NULL,
		[continuous_pull]                           [money] NULL,
		[cash_to_rtr]                               [money] NULL,
		[rtr_adjustment]                            [money] NULL,
		[rtr_applied]                               [money] NULL,
		[pp_adjustment]                             [money] NULL,
		[pp_applied]                                [money] NULL,
		[margin_adjustment]                         [money] NULL,
		[margin_applied]                            [money] NULL,
		[gross_revenue]                             [money] NULL,
		[applied_amount]                            [money] NULL,
		[gross_received]                            [money] NULL,
		[net_other_income]                          [money] NULL,
		[net_revenue]                               [money] NULL,
		[ach_revenue]                               [money] NULL,
		[total_fees_collected]                      [money] NULL,
		[gross_dollars_rec]                         [money] NULL,
		[last_trans_amount]                         [money] NULL,
		[total_portfolio_adjustment]                [money] NULL,
		[first_received_date]                       [datetime] NULL,
		[cash_from_reapplication]                   [money] NULL,
		[workdays_since_funded]                     [int] NULL,
		[last_draft_date]                           [datetime] NULL,
		[first_draft_date]                          [datetime] NULL,
		[estimated_final_payment_date]              [date] NULL,
		[anticipated_final_payment_date]            [date] NULL,
		[percent_complete]                          [numeric](10, 2) NULL,
		[variance_amount]                           [money] NULL,
		[pp_settlement]                             [money] NULL,
		[margin_settlement]                         [money] NULL,
		[auto_payment_dates]                        [bit] NOT NULL,
		[on_hold_date]                              [datetime] NULL,
		[scorecard_value]                           [decimal](16, 4) NULL,
		[sent_to_acct_2]                            [datetime] NULL,
		[one_month_prepay_amount]                   [money] NULL,
		[two_month_prepay_amount]                   [money] NULL,
		[three_month_prepay_amount]                 [money] NULL,
		[include_prepay_discounts]                  [bit] NULL,
		[prepay_discounts_too_low]                  [bit] NULL,
		[prepaid_month]                             [int] NULL,
		[early_payoff_discount]                     [money] NULL,
		[early_payoff_commission]                   [money] NULL,
		[early_payoff_referral_commission]          [money] NULL,
		[commission_on_funding_or_net_merchant]     [bit] NULL,
		[referral_commission_percent]               [numeric](10, 4) NULL,
		[bonus_commission]                          [money] NULL,
		[bonus_commission_percent]                  [numeric](12, 4) NULL,
		[funding_bonus_id]                          [int] NULL,
		[iso_bonus_id]                              [int] NULL,
		[first_position]                            [bit] NOT NULL,
		[ineligible_date]                           [date] NULL,
		[portfolio]                                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK__fundings__3213E83F93A34213]
		PRIMARY KEY
		CLUSTERED
		([id] DESC)
)
GO
ALTER TABLE [dbo].[fundings]
	ADD
	CONSTRAINT [DF__fundings__first___0876219E]
	DEFAULT ((1)) FOR [first_position]
GO
ALTER TABLE [dbo].[fundings]
	ADD
	CONSTRAINT [DF__fundings__create__7CA47C3F]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[fundings]
	ADD
	CONSTRAINT [DF__fundings__auto_p__7D98A078]
	DEFAULT ((1)) FOR [auto_payment_dates]
GO
ALTER TABLE [dbo].[fundings]
	ADD
	CONSTRAINT [DF__fundings__do_ret__7E8CC4B1]
	DEFAULT ((1)) FOR [do_retries]
GO
ALTER TABLE [dbo].[fundings]
	ADD
	CONSTRAINT [DF__fundings__proces__7F80E8EA]
	DEFAULT ((1)) FOR [process_fees]
GO
ALTER TABLE [dbo].[fundings]
	WITH CHECK
	ADD CONSTRAINT [fk_fundings_affiliate]
	FOREIGN KEY ([affiliate_id]) REFERENCES [dbo].[affiliates] ([affiliate_id])
ALTER TABLE [dbo].[fundings]
	CHECK CONSTRAINT [fk_fundings_affiliate]

GO
ALTER TABLE [dbo].[fundings]
	WITH CHECK
	ADD CONSTRAINT [fk_fundings_contract_status]
	FOREIGN KEY ([contract_status]) REFERENCES [dbo].[contract_statuses] ([contract_status_id])
ALTER TABLE [dbo].[fundings]
	CHECK CONSTRAINT [fk_fundings_contract_status]

GO
ALTER TABLE [dbo].[fundings]
	WITH CHECK
	ADD CONSTRAINT [fk_fundings_contract_substatuses]
	FOREIGN KEY ([contract_substatus_id]) REFERENCES [dbo].[contract_substatuses] ([substatus_id])
ALTER TABLE [dbo].[fundings]
	CHECK CONSTRAINT [fk_fundings_contract_substatuses]

GO
ALTER TABLE [dbo].[fundings]
	WITH CHECK
	ADD CONSTRAINT [fk_fundings_merchant]
	FOREIGN KEY ([merchant_id]) REFERENCES [dbo].[merchants] ([merchant_id])
ALTER TABLE [dbo].[fundings]
	CHECK CONSTRAINT [fk_fundings_merchant]

GO
ALTER TABLE [dbo].[fundings]
	WITH CHECK
	ADD CONSTRAINT [fk_fundings_performance_statuses]
	FOREIGN KEY ([performance_status_id]) REFERENCES [dbo].[performance_statuses] ([status_id])
ALTER TABLE [dbo].[fundings]
	CHECK CONSTRAINT [fk_fundings_performance_statuses]

GO
CREATE NONCLUSTERED INDEX [idx_fundings_affiliate_id]
	ON [dbo].[fundings] ([affiliate_id])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_fundings_contract_number]
	ON [dbo].[fundings] ([contract_number])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_fundings_contract_status]
	ON [dbo].[fundings] ([contract_status])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_fundings_merchant_id]
	ON [dbo].[fundings] ([merchant_id])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_fundings_on_hold]
	ON [dbo].[fundings] ([on_hold])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_fundings_legal_name]
	ON [dbo].[fundings] ([legal_name])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[fundings] SET (LOCK_ESCALATION = TABLE)
GO
