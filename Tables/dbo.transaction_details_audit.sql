SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transaction_details_audit] (
		[audit_id]             [int] IDENTITY(1, 1) NOT NULL,
		[trans_detail_id]      [int] NULL,
		[change_date]          [datetime] NOT NULL,
		[change_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_category]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[field]                [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[previous_value]       [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[changed_to_value]     [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_transaction_details_audit_id]
		PRIMARY KEY
		CLUSTERED
		([audit_id])
)
GO
ALTER TABLE [dbo].[transaction_details_audit]
	ADD
	CONSTRAINT [DF__transacti__chang__269AB60B]
	DEFAULT (getutcdate()) FOR [change_date]
GO
CREATE NONCLUSTERED INDEX [idx_transaction_details_audit_change_date]
	ON [dbo].[transaction_details_audit] ([change_date] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[transaction_details_audit] SET (LOCK_ESCALATION = TABLE)
GO
