SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[payment_schedule_status] (
		[id]                     [int] IDENTITY(1, 1) NOT NULL,
		[performance_status]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[parent_id]              [int] NULL,
		[create_date]            [datetime] NULL,
		[code]                   [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_ps_status_id]
		PRIMARY KEY
		CLUSTERED
		([id])
)
GO
ALTER TABLE [dbo].[payment_schedule_status]
	ADD
	CONSTRAINT [DF__payment_s__creat__147C05D0]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[payment_schedule_status] SET (LOCK_ESCALATION = TABLE)
GO
