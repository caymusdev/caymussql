SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[funding_payment_dates_work] (
		[id]                             [int] IDENTITY(1, 1) NOT NULL,
		[session_id]                     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[userid]                         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[funding_payment_id]             [int] NULL,
		[payment_date]                   [date] NOT NULL,
		[amount]                         [money] NOT NULL,
		[create_date]                    [datetime] NULL,
		[active]                         [bit] NULL,
		[merchant_bank_account_id]       [int] NULL,
		[payment_method]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_processor]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_schedule_change_id]     [int] NULL,
		[comments]                       [nvarchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_reason_id]               [int] NULL,
		[change_date]                    [datetime] NULL,
		[change_user]                    [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[approved]                       [bit] NULL,
		[funding_id]                     [int] NULL,
		[changed]                        [bit] NULL,
		[approver]                       [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[approved_date]                  [datetime] NULL,
		[ready_for_approval]             [bit] NULL,
		[revised_original]               [bit] NULL,
		[payment_plan_id]                [int] NULL,
		[rejector]                       [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[rejected_date]                  [datetime] NULL,
		[reject_reason]                  [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[missed]                         [bit] NULL,
		[affiliate_id]                   [int] NULL,
		CONSTRAINT [pk_funding_payment_dates_work_id]
		PRIMARY KEY
		CLUSTERED
		([id] DESC)
)
GO
CREATE NONCLUSTERED INDEX [idx_funding_payment_dates_work_sessionid]
	ON [dbo].[funding_payment_dates_work] ([session_id])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[funding_payment_dates_work] SET (LOCK_ESCALATION = TABLE)
GO
