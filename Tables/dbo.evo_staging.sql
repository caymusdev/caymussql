SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[evo_staging] (
		[evo_staging_id]     [int] IDENTITY(1, 1) NOT NULL,
		[Date]               [date] NULL,
		[MID]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[A360 MID]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DBA]                [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Total Batch]        [money] NULL,
		[Split Amount]       [money] NULL,
		[Split Rate]         [numeric](10, 2) NULL,
		[create_date]        [datetime] NOT NULL,
		[BatchID]            [int] NULL,
		[bank_batch_id]      [int] NULL,
		CONSTRAINT [evo_staging_id]
		PRIMARY KEY
		CLUSTERED
		([evo_staging_id] DESC)
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[evo_staging]
	ADD
	CONSTRAINT [DF_evotaging_createdDate]
	DEFAULT (getutcdate()) FOR [create_date]
GO
CREATE NONCLUSTERED INDEX [IX_evo_staging_MID]
	ON [dbo].[evo_staging] ([MID])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_evo_staging_Date]
	ON [dbo].[evo_staging] ([Date] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[evo_staging] SET (LOCK_ESCALATION = TABLE)
GO
