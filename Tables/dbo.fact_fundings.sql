SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fact_fundings] (
		[fact_funding_id]                    [bigint] IDENTITY(1, 1) NOT NULL,
		[funding_id]                         [bigint] NOT NULL,
		[contract_status]                    [int] NULL,
		[payoff_amount]                      [money] NULL,
		[frequency]                          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[weekday_payment]                    [money] NULL,
		[weekly_payment]                     [money] NULL,
		[processor]                          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[paid_off_amount]                    [money] NULL,
		[last_payment_date]                  [datetime] NULL,
		[rtr_out]                            [money] NULL,
		[pp_out]                             [money] NULL,
		[margin_out]                         [money] NULL,
		[fees_out]                           [money] NULL,
		[percent_paid]                       [numeric](10, 2) NULL,
		[percent_performance]                [numeric](10, 2) NULL,
		[on_hold]                            [bit] NULL,
		[collection_type]                    [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[margin_adjustment]                  [money] NULL,
		[start_date]                         [datetime] NOT NULL,
		[end_date]                           [datetime] NOT NULL,
		[float_amount]                       [money] NULL,
		[payments_stopped_date]              [datetime] NULL,
		[est_months_left]                    [numeric](10, 2) NULL,
		[performance_status_id]              [int] NULL,
		[contract_substatus_id]              [int] NULL,
		[ach_draft]                          [money] NULL,
		[refund_out]                         [money] NULL,
		[create_date]                        [datetime] NULL,
		[cash_to_pp]                         [money] NULL,
		[cash_to_margin]                     [money] NULL,
		[cash_fees]                          [money] NULL,
		[counter_deposit_revenue]            [money] NULL,
		[ach_received]                       [money] NULL,
		[cc_revenue]                         [money] NULL,
		[wires_revenue]                      [money] NULL,
		[wires_revenue_no_renewal]           [money] NULL,
		[discount_received]                  [money] NULL,
		[fees_applied]                       [money] NULL,
		[settlement_received]                [money] NULL,
		[writeoff_received]                  [money] NULL,
		[cash_to_chargeback]                 [money] NULL,
		[cash_to_bad_debt]                   [money] NULL,
		[cash_to_reapplication]              [money] NULL,
		[cash_from_reapplication]            [money] NULL,
		[writeoff_adjustment_received]       [money] NULL,
		[checks_revenue]                     [money] NULL,
		[ach_reject]                         [money] NULL,
		[refund]                             [money] NULL,
		[refund_processed]                   [money] NULL,
		[refund_reapplied]                   [money] NULL,
		[refund_returned]                    [money] NULL,
		[continuous_pull]                    [money] NULL,
		[cash_to_rtr]                        [money] NULL,
		[rtr_adjustment]                     [money] NULL,
		[rtr_applied]                        [money] NULL,
		[pp_adjustment]                      [money] NULL,
		[pp_applied]                         [money] NULL,
		[margin_applied]                     [money] NULL,
		[gross_revenue]                      [money] NULL,
		[applied_amount]                     [money] NULL,
		[gross_received]                     [money] NULL,
		[net_other_income]                   [money] NULL,
		[net_revenue]                        [money] NULL,
		[ach_revenue]                        [money] NULL,
		[total_fees_collected]               [money] NULL,
		[gross_dollars_rec]                  [money] NULL,
		[last_trans_amount]                  [money] NULL,
		[total_portfolio_adjustment]         [money] NULL,
		[estimated_final_payment_date]       [date] NULL,
		[anticipated_final_payment_date]     [date] NULL,
		[last_draft_date]                    [datetime] NULL,
		[end_of_month]                       [bit] NULL,
		[actual_turn]                        [numeric](10, 2) NULL,
		[variance_amount]                    [money] NULL,
		[pp_settlement]                      [money] NULL,
		[margin_settlement]                  [money] NULL,
		[percent_complete]                   [numeric](10, 2) NULL,
		CONSTRAINT [pk_fact_fundings_id]
		PRIMARY KEY
		CLUSTERED
		([fact_funding_id])
)
GO
ALTER TABLE [dbo].[fact_fundings]
	ADD
	CONSTRAINT [DF__fact_fund__end_o__6991A7CB]
	DEFAULT ((0)) FOR [end_of_month]
GO
ALTER TABLE [dbo].[fact_fundings]
	ADD
	CONSTRAINT [DF__fact_fund__creat__6A85CC04]
	DEFAULT (getutcdate()) FOR [create_date]
GO
CREATE NONCLUSTERED INDEX [idx_fact_fundings_funding_id]
	ON [dbo].[fact_fundings] ([funding_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_fact_fundings_start_date]
	ON [dbo].[fact_fundings] ([start_date] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[fact_fundings] SET (LOCK_ESCALATION = TABLE)
GO
