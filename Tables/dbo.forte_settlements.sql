SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[forte_settlements] (
		[forte_settle_id]          [int] IDENTITY(1, 1) NOT NULL,
		[settle_id]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[customer_token]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[transaction_id]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[customer_id]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[funding_id]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[settle_batch_id]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[settle_date]              [datetime] NULL,
		[settle_type]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[settle_response_code]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[settle_amount]            [money] NOT NULL,
		[method]                   [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[forte_funding_id]         [int] NULL,
		[batch_id]                 [int] NULL,
		[create_date]              [datetime] NULL,
		[change_date]              [datetime] NULL,
		[change_user]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_forte_settlements_id]
		PRIMARY KEY
		CLUSTERED
		([forte_settle_id] DESC)
)
GO
ALTER TABLE [dbo].[forte_settlements]
	ADD
	CONSTRAINT [DF__forte_set__creat__6E565CE8]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[forte_settlements]
	WITH CHECK
	ADD CONSTRAINT [fk_forte_settlements_forte_fundings]
	FOREIGN KEY ([forte_funding_id]) REFERENCES [dbo].[forte_fundings] ([forte_funding_id])
	ON DELETE CASCADE
ALTER TABLE [dbo].[forte_settlements]
	CHECK CONSTRAINT [fk_forte_settlements_forte_fundings]

GO
CREATE NONCLUSTERED INDEX [IX_forte_settlements_trans_id]
	ON [dbo].[forte_settlements] ([transaction_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_forte_settlements_trans_id_settle_response_code]
	ON [dbo].[forte_settlements] ([transaction_id] DESC, [settle_response_code])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [nci_wi_forte_settlements_CF4E0B2C86B54015588F0A7BDA20D086]
	ON [dbo].[forte_settlements] ([settle_id])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[forte_settlements] SET (LOCK_ESCALATION = TABLE)
GO
