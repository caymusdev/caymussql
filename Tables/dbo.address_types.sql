SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[address_types] (
		[address_type_id]       [int] IDENTITY(1, 1) NOT NULL,
		[address_type]          [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[address_type_desc]     [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]           [datetime] NOT NULL,
		[change_date]           [datetime] NOT NULL,
		[change_user]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_address_type_id]
		PRIMARY KEY
		CLUSTERED
		([address_type_id])
)
GO
ALTER TABLE [dbo].[address_types]
	ADD
	CONSTRAINT [DF__address_t__creat__246854D6]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[address_types]
	ADD
	CONSTRAINT [DF__address_t__chang__255C790F]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[address_types] SET (LOCK_ESCALATION = TABLE)
GO
