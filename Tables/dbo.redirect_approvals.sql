SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[redirect_approvals] (
		[redirect_approval_id]       [int] IDENTITY(1, 1) NOT NULL,
		[from_funding_id]            [bigint] NOT NULL,
		[to_funding_id]              [bigint] NOT NULL,
		[approved_by]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[approved_date]              [datetime] NULL,
		[anticipated_start_date]     [date] NULL,
		[actual_start_date]          [date] NULL,
		[payment_schedule_id]        [int] NULL,
		[end_date]                   [date] NULL,
		[create_date]                [datetime] NULL,
		[redirect_reason]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[rejected_by]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[rejected_date]              [datetime] NULL,
		[change_date]                [datetime] NULL,
		[change_user]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[on_hold]                    [bit] NULL,
		CONSTRAINT [pk_redirect_approvals_id]
		PRIMARY KEY
		CLUSTERED
		([redirect_approval_id] DESC)
)
GO
ALTER TABLE [dbo].[redirect_approvals]
	ADD
	CONSTRAINT [DF__redirect___creat__1FEDB87C]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[redirect_approvals]
	WITH CHECK
	ADD CONSTRAINT [fk_red_app_from_funding_id]
	FOREIGN KEY ([from_funding_id]) REFERENCES [dbo].[fundings] ([id])
ALTER TABLE [dbo].[redirect_approvals]
	CHECK CONSTRAINT [fk_red_app_from_funding_id]

GO
ALTER TABLE [dbo].[redirect_approvals]
	WITH CHECK
	ADD CONSTRAINT [fk_red_app_to_funding_id]
	FOREIGN KEY ([to_funding_id]) REFERENCES [dbo].[fundings] ([id])
ALTER TABLE [dbo].[redirect_approvals]
	CHECK CONSTRAINT [fk_red_app_to_funding_id]

GO
CREATE NONCLUSTERED INDEX [idx_redirect_approvals_funding_ids]
	ON [dbo].[redirect_approvals] ([from_funding_id] DESC, [to_funding_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[redirect_approvals] SET (LOCK_ESCALATION = TABLE)
GO
