SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[contact_log] (
		[contact_log_id]        [int] IDENTITY(1, 1) NOT NULL,
		[merchant_id]           [bigint] NULL,
		[affiliate_id]          [bigint] NULL,
		[contract_id]           [bigint] NULL,
		[contact_type]          [int] NULL,
		[contact_date]          [datetime] NULL,
		[contact_id]            [int] NULL,
		[contact_outcome]       [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[reminder_id]           [int] NULL,
		[notes]                 [nvarchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]           [datetime] NOT NULL,
		[change_date]           [datetime] NOT NULL,
		[change_user]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contact_status_id]     [int] NULL,
		[record_id]             [int] NULL,
		CONSTRAINT [pk_contact_log_id]
		PRIMARY KEY
		CLUSTERED
		([contact_log_id] DESC)
)
GO
ALTER TABLE [dbo].[contact_log]
	ADD
	CONSTRAINT [DF__contact_l__creat__01892CED]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[contact_log]
	ADD
	CONSTRAINT [DF__contact_l__chang__027D5126]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[contact_log]
	WITH CHECK
	ADD CONSTRAINT [fk_contact_log_contacts_contact_id]
	FOREIGN KEY ([contact_id]) REFERENCES [dbo].[contacts] ([contact_id])
ALTER TABLE [dbo].[contact_log]
	CHECK CONSTRAINT [fk_contact_log_contacts_contact_id]

GO
ALTER TABLE [dbo].[contact_log]
	WITH CHECK
	ADD CONSTRAINT [fk_contact_log_merchants]
	FOREIGN KEY ([merchant_id]) REFERENCES [dbo].[merchants] ([merchant_id])
ALTER TABLE [dbo].[contact_log]
	CHECK CONSTRAINT [fk_contact_log_merchants]

GO
ALTER TABLE [dbo].[contact_log]
	WITH CHECK
	ADD CONSTRAINT [fk_contact_log_affiliates]
	FOREIGN KEY ([affiliate_id]) REFERENCES [dbo].[affiliates] ([affiliate_id])
ALTER TABLE [dbo].[contact_log]
	CHECK CONSTRAINT [fk_contact_log_affiliates]

GO
ALTER TABLE [dbo].[contact_log]
	WITH CHECK
	ADD CONSTRAINT [fk_contact_contact_types]
	FOREIGN KEY ([contact_type]) REFERENCES [dbo].[contact_types] ([contact_type_id])
ALTER TABLE [dbo].[contact_log]
	CHECK CONSTRAINT [fk_contact_contact_types]

GO
ALTER TABLE [dbo].[contact_log]
	WITH CHECK
	ADD CONSTRAINT [fk_contact_contacts]
	FOREIGN KEY ([contact_id]) REFERENCES [dbo].[contacts] ([contact_id])
ALTER TABLE [dbo].[contact_log]
	CHECK CONSTRAINT [fk_contact_contacts]

GO
ALTER TABLE [dbo].[contact_log]
	WITH CHECK
	ADD CONSTRAINT [fk_contact_log_contact_status]
	FOREIGN KEY ([contact_status_id]) REFERENCES [dbo].[contact_statuses] ([contact_status_id])
ALTER TABLE [dbo].[contact_log]
	CHECK CONSTRAINT [fk_contact_log_contact_status]

GO
ALTER TABLE [dbo].[contact_log] SET (LOCK_ESCALATION = TABLE)
GO
