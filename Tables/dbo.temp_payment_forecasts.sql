SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[temp_payment_forecasts] (
		[funding_id]              [bigint] NOT NULL,
		[payment_date]            [date] NOT NULL,
		[amount]                  [money] NOT NULL,
		[rec_amount]              [money] NULL,
		[payment_schedule_id]     [int] NULL,
		[id]                      [int] IDENTITY(1, 1) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[temp_payment_forecasts] SET (LOCK_ESCALATION = TABLE)
GO
