SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[iso_bonuses] (
		[iso_bonus_id]            [int] IDENTITY(1, 1) NOT NULL,
		[iso_name]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[iso_id]                  [bigint] NOT NULL,
		[bonus_amount]            [money] NULL,
		[pay_date]                [datetime] NULL,
		[create_date]             [datetime] NOT NULL,
		[change_date]             [datetime] NOT NULL,
		[change_user]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[approved_by]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[approved_date]           [datetime] NULL,
		[sent_to_acct]            [datetime] NULL,
		[bonus_month]             [int] NULL,
		[bonus_year]              [int] NULL,
		[funding_bonus_id]        [int] NULL,
		[bonus_percent_level]     [numeric](16, 4) NULL,
		[bonus_amount_level]      [numeric](16, 4) NULL,
		[total_amount]            [numeric](16, 4) NULL,
		[total_volume]            [numeric](16, 4) NULL,
		CONSTRAINT [pk_iso_bonus_id]
		PRIMARY KEY
		CLUSTERED
		([iso_bonus_id] DESC)
)
GO
ALTER TABLE [dbo].[iso_bonuses]
	ADD
	CONSTRAINT [DF__iso_bonus__creat__430CD787]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[iso_bonuses]
	ADD
	CONSTRAINT [DF__iso_bonus__chang__4400FBC0]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[iso_bonuses] SET (LOCK_ESCALATION = TABLE)
GO
