SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[nacha_codes] (
		[nachca_code_id]     [int] IDENTITY(1, 1) NOT NULL,
		[code]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[description]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[comments]           [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [nacha_id]
		PRIMARY KEY
		CLUSTERED
		([nachca_code_id])
	ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_nacha_codes_code]
	ON [dbo].[nacha_codes] ([code])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[nacha_codes] SET (LOCK_ESCALATION = TABLE)
GO
