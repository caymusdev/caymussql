SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[merchant_audit] (
		[merchant_audit_id]           [bigint] IDENTITY(1, 1) NOT NULL,
		[funding_id]                  [bigint] NULL,
		[trans_id]                    [bigint] NULL,
		[trans_date]                  [datetime] NULL,
		[trans_amount]                [money] NULL,
		[trans_type]                  [int] NULL,
		[payment_id]                  [bigint] NULL,
		[is_fee]                      [bit] NULL,
		[orig_payment_id]             [bigint] NULL,
		[previous_payment_id]         [bigint] NULL,
		[balance]                     [money] NULL,
		[portfolio_value]             [money] NULL,
		[transaction_id]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[orig_transaction_id]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[previous_transaction_id]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[retry_level]                 [int] NULL,
		[retry_sublevel]              [int] NULL,
		[retry_level_text]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[response_code]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_path]                [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[trans_path]                  [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[result_desc]                 [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[trans_comments]              [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]                 [datetime] NULL,
		CONSTRAINT [pk_merchant_audit_id]
		PRIMARY KEY
		CLUSTERED
		([merchant_audit_id])
)
GO
ALTER TABLE [dbo].[merchant_audit]
	ADD
	CONSTRAINT [DF__merchant___creat__035179CE]
	DEFAULT (getutcdate()) FOR [create_date]
GO
CREATE NONCLUSTERED INDEX [idx_merchant_audit_funding_id]
	ON [dbo].[merchant_audit] ([funding_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_merchant_audit_resp_code]
	ON [dbo].[merchant_audit] ([response_code])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_merchant_audit_trans_date]
	ON [dbo].[merchant_audit] ([trans_date] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_merchant_audit_trans_type]
	ON [dbo].[merchant_audit] ([trans_type])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[merchant_audit] SET (LOCK_ESCALATION = TABLE)
GO
