SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[approval_users] (
		[approval_user_id]     [int] IDENTITY(1, 1) NOT NULL,
		[approval_type_id]     [int] NULL,
		[user_id]              [int] NULL,
		[create_date]          [datetime] NOT NULL,
		[change_date]          [datetime] NOT NULL,
		[change_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_approval_users_id]
		PRIMARY KEY
		CLUSTERED
		([approval_user_id] DESC)
)
GO
ALTER TABLE [dbo].[approval_users]
	ADD
	CONSTRAINT [DF__approval___creat__42ECDBF6]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[approval_users]
	ADD
	CONSTRAINT [DF__approval___chang__43E1002F]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[approval_users]
	WITH CHECK
	ADD CONSTRAINT [fk_approval_users_approval_types]
	FOREIGN KEY ([approval_type_id]) REFERENCES [dbo].[approval_types] ([approval_type_id])
ALTER TABLE [dbo].[approval_users]
	CHECK CONSTRAINT [fk_approval_users_approval_types]

GO
ALTER TABLE [dbo].[approval_users]
	WITH CHECK
	ADD CONSTRAINT [fk_approval_users_users]
	FOREIGN KEY ([user_id]) REFERENCES [dbo].[users] ([user_id])
ALTER TABLE [dbo].[approval_users]
	CHECK CONSTRAINT [fk_approval_users_users]

GO
CREATE NONCLUSTERED INDEX [idx_approval_users]
	ON [dbo].[approval_users] ([user_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[approval_users] SET (LOCK_ESCALATION = TABLE)
GO
