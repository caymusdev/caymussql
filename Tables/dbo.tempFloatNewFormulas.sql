SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tempFloatNewFormulas] (
		[funding_id]       [bigint] NOT NULL,
		[start_date]       [date] NULL,
		[float_amount]     [money] NULL,
		[create_date]      [datetime] NULL
)
GO
ALTER TABLE [dbo].[tempFloatNewFormulas]
	ADD
	CONSTRAINT [DF__tempFloat__creat__24B26D99]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[tempFloatNewFormulas] SET (LOCK_ESCALATION = TABLE)
GO
