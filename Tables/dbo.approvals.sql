SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[approvals] (
		[approval_id]          [int] IDENTITY(1, 1) NOT NULL,
		[approval_type_id]     [int] NOT NULL,
		[description]          [nvarchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[comments]             [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[record_id]            [int] NULL,
		[new_value]            [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]          [datetime] NOT NULL,
		[change_date]          [datetime] NOT NULL,
		[change_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[affiliate_id]         [bigint] NULL,
		[approved_by]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[approved_date]        [datetime] NULL,
		[rejected_by]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[rejected_date]        [datetime] NULL,
		CONSTRAINT [pk_approval_id]
		PRIMARY KEY
		CLUSTERED
		([approval_id])
)
GO
ALTER TABLE [dbo].[approvals]
	ADD
	CONSTRAINT [DF__approvals__creat__1BD30ED5]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[approvals]
	ADD
	CONSTRAINT [DF__approvals__chang__1CC7330E]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[approvals]
	WITH CHECK
	ADD CONSTRAINT [fk_approvals_approval_type_id]
	FOREIGN KEY ([approval_type_id]) REFERENCES [dbo].[approval_types] ([approval_type_id])
ALTER TABLE [dbo].[approvals]
	CHECK CONSTRAINT [fk_approvals_approval_type_id]

GO
ALTER TABLE [dbo].[approvals] SET (LOCK_ESCALATION = TABLE)
GO
