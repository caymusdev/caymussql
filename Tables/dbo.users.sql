SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[users] (
		[user_id]             [int] IDENTITY(1, 1) NOT NULL,
		[email]               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[hash_password]       [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[salt]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[timezone]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]         [datetime] NOT NULL,
		[change_date]         [datetime] NOT NULL,
		[change_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[presence_status]     [int] NULL,
		[active]              [bit] NOT NULL,
		CONSTRAINT [pk_users_id]
		PRIMARY KEY
		CLUSTERED
		([user_id])
)
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__create_da__4A6E022D]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__change_da__4B622666]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[users]
	ADD
	CONSTRAINT [DF__users__active__5F891AA4]
	DEFAULT ((1)) FOR [active]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_users_email]
	ON [dbo].[users] ([email])
	ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_user_status_id]
	ON [dbo].[users] ([user_id])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[users] SET (LOCK_ESCALATION = TABLE)
GO
