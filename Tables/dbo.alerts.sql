SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[alerts] (
		[alert_id]          [int] IDENTITY(1, 1) NOT NULL,
		[alert_type]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[record_id]         [bigint] NULL,
		[alert_message]     [nvarchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]       [datetime] NOT NULL,
		[deleted]           [bit] NULL,
		[alert_type_2]      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_alerts_id]
		PRIMARY KEY
		CLUSTERED
		([alert_id])
)
GO
ALTER TABLE [dbo].[alerts]
	ADD
	CONSTRAINT [DF__alerts__create_d__5772F790]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[alerts] SET (LOCK_ESCALATION = TABLE)
GO
