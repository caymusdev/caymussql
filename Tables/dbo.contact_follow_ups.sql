SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[contact_follow_ups] (
		[contact_follow_up_id]       [int] IDENTITY(1, 1) NOT NULL,
		[contact_log_id]             [int] NOT NULL,
		[contact_status_id]          [int] NOT NULL,
		[contact_outcome]            [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[follow_up_date]             [datetime] NULL,
		[follow_up_note]             [nvarchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[follow_up_contact_type]     [int] NULL,
		[affiliate_note]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[affiliate_id]               [bigint] NULL,
		[contact_first_name]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contact_last_name]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contact_contact_type]       [int] NULL,
		[create_date]                [datetime] NOT NULL,
		[change_date]                [datetime] NOT NULL,
		[change_user]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_contact_follow_ups_id]
		PRIMARY KEY
		CLUSTERED
		([contact_follow_up_id] DESC)
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[contact_follow_ups]
	ADD
	CONSTRAINT [DF__contact_f__creat__57E7F8DC]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[contact_follow_ups]
	ADD
	CONSTRAINT [DF__contact_f__chang__58DC1D15]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[contact_follow_ups]
	WITH CHECK
	ADD CONSTRAINT [fk_contact_followup_contact_status]
	FOREIGN KEY ([contact_status_id]) REFERENCES [dbo].[contact_statuses] ([contact_status_id])
ALTER TABLE [dbo].[contact_follow_ups]
	CHECK CONSTRAINT [fk_contact_followup_contact_status]

GO
ALTER TABLE [dbo].[contact_follow_ups]
	WITH CHECK
	ADD CONSTRAINT [fk_contact_followup_contact_log]
	FOREIGN KEY ([contact_log_id]) REFERENCES [dbo].[contact_log] ([contact_log_id])
ALTER TABLE [dbo].[contact_follow_ups]
	CHECK CONSTRAINT [fk_contact_followup_contact_log]

GO
CREATE NONCLUSTERED INDEX [idx_contact_follow_ups]
	ON [dbo].[contact_follow_ups] ([contact_log_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[contact_follow_ups] SET (LOCK_ESCALATION = TABLE)
GO
