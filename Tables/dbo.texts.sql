SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[texts] (
		[text_id]             [int] IDENTITY(1, 1) NOT NULL,
		[user_id]             [int] NULL,
		[to]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[from]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[affiliate_id]        [bigint] NULL,
		[merchant_id]         [bigint] NULL,
		[contract_id]         [bigint] NULL,
		[sent]                [datetime] NULL,
		[received]            [datetime] NULL,
		[text_message]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]         [datetime] NOT NULL,
		[change_date]         [datetime] NOT NULL,
		[change_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[message_id]          [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[text_type]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[text_data]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[concat]              [bit] NULL,
		[contact_id]          [int] NULL,
		[sent_from_bossk]     [bit] NULL,
		CONSTRAINT [pk_texts_id]
		PRIMARY KEY
		CLUSTERED
		([text_id] DESC)
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[texts]
	ADD
	CONSTRAINT [DF__texts__create_da__444B1483]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[texts]
	ADD
	CONSTRAINT [DF__texts__change_da__453F38BC]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[texts]
	WITH CHECK
	ADD CONSTRAINT [fk_texts_affiliates]
	FOREIGN KEY ([affiliate_id]) REFERENCES [dbo].[affiliates] ([affiliate_id])
ALTER TABLE [dbo].[texts]
	CHECK CONSTRAINT [fk_texts_affiliates]

GO
ALTER TABLE [dbo].[texts]
	WITH CHECK
	ADD CONSTRAINT [fk_texts_contracts]
	FOREIGN KEY ([contract_id]) REFERENCES [dbo].[contracts] ([contract_id])
ALTER TABLE [dbo].[texts]
	CHECK CONSTRAINT [fk_texts_contracts]

GO
ALTER TABLE [dbo].[texts]
	WITH CHECK
	ADD CONSTRAINT [fk_texts_merchants]
	FOREIGN KEY ([merchant_id]) REFERENCES [dbo].[merchants] ([merchant_id])
ALTER TABLE [dbo].[texts]
	CHECK CONSTRAINT [fk_texts_merchants]

GO
ALTER TABLE [dbo].[texts]
	WITH CHECK
	ADD CONSTRAINT [fk_texts_users]
	FOREIGN KEY ([user_id]) REFERENCES [dbo].[users] ([user_id])
ALTER TABLE [dbo].[texts]
	CHECK CONSTRAINT [fk_texts_users]

GO
CREATE NONCLUSTERED INDEX [idx_texts_to]
	ON [dbo].[texts] ([to] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_texts_from]
	ON [dbo].[texts] ([from] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[texts] SET (LOCK_ESCALATION = TABLE)
GO
