SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[contact_statuses] (
		[contact_status_id]       [int] IDENTITY(1, 1) NOT NULL,
		[contact_status]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[contact_status_desc]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contact_type_id]         [int] NULL,
		[create_date]             [datetime] NOT NULL,
		[change_date]             [datetime] NOT NULL,
		[change_user]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_contact_statuses_id]
		PRIMARY KEY
		CLUSTERED
		([contact_status_id])
)
GO
ALTER TABLE [dbo].[contact_statuses]
	ADD
	CONSTRAINT [DF__contact_s__creat__1D66518C]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[contact_statuses]
	ADD
	CONSTRAINT [DF__contact_s__chang__1E5A75C5]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[contact_statuses]
	WITH CHECK
	ADD CONSTRAINT [fk_contact_status_contract_type]
	FOREIGN KEY ([contact_type_id]) REFERENCES [dbo].[contact_types] ([contact_type_id])
ALTER TABLE [dbo].[contact_statuses]
	CHECK CONSTRAINT [fk_contact_status_contract_type]

GO
ALTER TABLE [dbo].[contact_statuses] SET (LOCK_ESCALATION = TABLE)
GO
