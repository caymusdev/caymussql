SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[payment_schedules] (
		[payment_schedule_id]            [int] IDENTITY(1, 1) NOT NULL,
		[payment_method]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[frequency]                      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[amount]                         [money] NULL,
		[start_date]                     [date] NULL,
		[cc_percentage]                  [numeric](10, 2) NULL,
		[bank_name]                      [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[routing_number]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[account_number]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_processor]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]                    [datetime] NULL,
		[change_date]                    [datetime] NULL,
		[change_user]                    [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[end_date]                       [datetime] NULL,
		[active]                         [bit] NOT NULL,
		[merchant_bank_account_id]       [int] NULL,
		[comments]                       [nvarchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_schedule_change_id]     [int] NULL,
		[change_reason_id]               [int] NULL,
		[performance_status_id]          [int] NULL,
		[payment_plan_id]                [int] NULL,
		[current_status]                 [int] NULL,
		[broken_payment_date]            [datetime] NULL,
		CONSTRAINT [payment_schedules_id]
		PRIMARY KEY
		CLUSTERED
		([payment_schedule_id])
)
GO
ALTER TABLE [dbo].[payment_schedules]
	ADD
	CONSTRAINT [DF__payment_s__activ__15702A09]
	DEFAULT ((1)) FOR [active]
GO
ALTER TABLE [dbo].[payment_schedules]
	ADD
	CONSTRAINT [DF_payment_schedules_create_date]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[payment_schedules] SET (LOCK_ESCALATION = TABLE)
GO
