SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[departments] (
		[department_id]          [int] IDENTITY(1, 1) NOT NULL,
		[department]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[department_desc]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]            [datetime] NOT NULL,
		[change_date]            [datetime] NOT NULL,
		[change_user]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[days_before_moving]     [int] NULL,
		[workflow_order]         [int] NULL,
		CONSTRAINT [pk_departments_id]
		PRIMARY KEY
		CLUSTERED
		([department_id] DESC)
)
GO
ALTER TABLE [dbo].[departments]
	ADD
	CONSTRAINT [DF__departmen__creat__679450C0]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[departments]
	ADD
	CONSTRAINT [DF__departmen__chang__688874F9]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[departments] SET (LOCK_ESCALATION = TABLE)
GO
