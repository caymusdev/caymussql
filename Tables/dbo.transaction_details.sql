SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transaction_details] (
		[trans_detail_id]     [int] IDENTITY(1, 1) NOT NULL,
		[trans_id]            [int] NOT NULL,
		[trans_type_id]       [int] NULL,
		[trans_amount]        [money] NULL,
		[comments]            [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]         [datetime] NULL,
		[change_date]         [datetime] NULL,
		[change_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_transaction_details_id]
		PRIMARY KEY
		CLUSTERED
		([trans_detail_id])
)
GO
ALTER TABLE [dbo].[transaction_details]
	ADD
	CONSTRAINT [DF__transacti__creat__25A691D2]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[transaction_details]
	WITH CHECK
	ADD CONSTRAINT [fk_transaction_details_trans_type]
	FOREIGN KEY ([trans_type_id]) REFERENCES [dbo].[transaction_types] ([trans_type_id])
ALTER TABLE [dbo].[transaction_details]
	CHECK CONSTRAINT [fk_transaction_details_trans_type]

GO
ALTER TABLE [dbo].[transaction_details]
	WITH CHECK
	ADD CONSTRAINT [fk_transaction_details_transactions]
	FOREIGN KEY ([trans_id]) REFERENCES [dbo].[transactions] ([trans_id])
	ON DELETE CASCADE
ALTER TABLE [dbo].[transaction_details]
	CHECK CONSTRAINT [fk_transaction_details_transactions]

GO
CREATE NONCLUSTERED INDEX [idx_transaction_details_trans_id]
	ON [dbo].[transaction_details] ([trans_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_transaction_details_trans_type_id]
	ON [dbo].[transaction_details] ([trans_type_id])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[transaction_details] SET (LOCK_ESCALATION = TABLE)
GO
