SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[case_statuses] (
		[case_status_id]     [int] IDENTITY(1, 1) NOT NULL,
		[status]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[status_desc]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]        [datetime] NOT NULL,
		[change_date]        [datetime] NOT NULL,
		[change_user]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [UQ__case_sta__A858923CAE735749]
		UNIQUE
		NONCLUSTERED
		([status]),
		CONSTRAINT [pk_statuses_id]
		PRIMARY KEY
		CLUSTERED
		([case_status_id])
)
GO
ALTER TABLE [dbo].[case_statuses]
	ADD
	CONSTRAINT [DF__case_stat__creat__5A054B78]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[case_statuses]
	ADD
	CONSTRAINT [DF__case_stat__chang__5AF96FB1]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[case_statuses] SET (LOCK_ESCALATION = TABLE)
GO
