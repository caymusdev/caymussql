SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_ActualTurnData] (
		[Contract_Number]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Funded_Date]         [datetime2](7) NOT NULL,
		[Legal_Name]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Actual_Est_Turn]     [float] NOT NULL,
		[Database_turn]       [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[temp_ActualTurnData] SET (LOCK_ESCALATION = TABLE)
GO
