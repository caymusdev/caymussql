SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[fundings_lookup] (
		[solo_id]            [bigint] NULL,
		[purchase_price]     [money] NULL,
		[pricing_ratio]      [numeric](10, 4) NULL,
		[portfolio_id]       [int] NULL,
		[createDate]         [datetime] NULL,
		[merchant_id]        [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[fundings_lookup] SET (LOCK_ESCALATION = TABLE)
GO
