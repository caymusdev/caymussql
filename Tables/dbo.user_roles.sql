SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user_roles] (
		[user_role_id]     [int] IDENTITY(1, 1) NOT NULL,
		[user_id]          [int] NOT NULL,
		[role_id]          [int] NOT NULL,
		[create_date]      [datetime] NOT NULL,
		[change_date]      [datetime] NOT NULL,
		[change_user]      [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]      [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_user_roles_id]
		PRIMARY KEY
		CLUSTERED
		([user_role_id])
)
GO
ALTER TABLE [dbo].[user_roles]
	ADD
	CONSTRAINT [DF__user_role__creat__02B25B50]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[user_roles]
	ADD
	CONSTRAINT [DF__user_role__chang__03A67F89]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[user_roles]
	WITH CHECK
	ADD CONSTRAINT [fk_user_roles_users_user_id]
	FOREIGN KEY ([user_id]) REFERENCES [dbo].[users] ([user_id])
ALTER TABLE [dbo].[user_roles]
	CHECK CONSTRAINT [fk_user_roles_users_user_id]

GO
ALTER TABLE [dbo].[user_roles]
	WITH CHECK
	ADD CONSTRAINT [fk_user_roles_roles_role_id]
	FOREIGN KEY ([role_id]) REFERENCES [dbo].[roles] ([role_id])
ALTER TABLE [dbo].[user_roles]
	CHECK CONSTRAINT [fk_user_roles_roles_role_id]

GO
ALTER TABLE [dbo].[user_roles] SET (LOCK_ESCALATION = TABLE)
GO
