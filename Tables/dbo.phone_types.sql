SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[phone_types] (
		[phone_type_id]       [int] IDENTITY(1, 1) NOT NULL,
		[phone_type]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[phone_type_desc]     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]         [datetime] NOT NULL,
		[change_date]         [datetime] NOT NULL,
		[change_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_phone_types_id]
		PRIMARY KEY
		CLUSTERED
		([phone_type_id] DESC)
)
GO
ALTER TABLE [dbo].[phone_types]
	ADD
	CONSTRAINT [DF__phone_typ__creat__62108194]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[phone_types]
	ADD
	CONSTRAINT [DF__phone_typ__chang__6304A5CD]
	DEFAULT (getutcdate()) FOR [change_date]
GO
CREATE NONCLUSTERED INDEX [idx_phone_types_contact_type]
	ON [dbo].[phone_types] ([phone_type] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[phone_types] SET (LOCK_ESCALATION = TABLE)
GO
