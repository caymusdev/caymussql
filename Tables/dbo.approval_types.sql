SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[approval_types] (
		[approval_type_id]     [int] IDENTITY(1, 1) NOT NULL,
		[approval_type]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[description]          [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]          [datetime] NOT NULL,
		[change_date]          [datetime] NOT NULL,
		[change_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_approval_type_id]
		PRIMARY KEY
		CLUSTERED
		([approval_type_id])
)
GO
ALTER TABLE [dbo].[approval_types]
	ADD
	CONSTRAINT [DF__approval___creat__1249A49B]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[approval_types]
	ADD
	CONSTRAINT [DF__approval___chang__133DC8D4]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[approval_types]
	ADD
	CONSTRAINT [DF__approval___chang__1431ED0D]
	DEFAULT ('SYSTEM') FOR [change_user]
GO
ALTER TABLE [dbo].[approval_types]
	ADD
	CONSTRAINT [DF__approval___creat__15261146]
	DEFAULT ('SYSTEM') FOR [create_user]
GO
ALTER TABLE [dbo].[approval_types] SET (LOCK_ESCALATION = TABLE)
GO
