SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[portfolio_eligibility] (
		[portfolio_eligibility_id]     [int] IDENTITY(1, 1) NOT NULL,
		[incorp_state_percent]         [numeric](10, 2) NULL,
		[incorp_state]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[incorp_state_percent_max]     [numeric](10, 2) NULL,
		[incorp_state_eligible]        [bit] NULL,
		[industry_percent]             [numeric](10, 2) NULL,
		[industry]                     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[industry_percent_max]         [numeric](10, 2) NULL,
		[industry_eligible]            [bit] NULL,
		[avg_balance]                  [money] NULL,
		[avg_balance_max]              [money] NULL,
		[avg_balance_eligible]         [bit] NULL,
		[remaining_term]               [numeric](10, 2) NULL,
		[remaining_term_max]           [numeric](10, 2) NULL,
		[remaining_term_eligible]      [bit] NULL,
		[portfolio_date]               [date] NULL,
		[create_date]                  [datetime] NOT NULL,
		[change_date]                  [datetime] NOT NULL,
		[change_user]                  [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]                  [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_portfolio_eligibility_id]
		PRIMARY KEY
		CLUSTERED
		([portfolio_eligibility_id])
	WITH FILLFACTOR=80
)
GO
ALTER TABLE [dbo].[portfolio_eligibility]
	ADD
	CONSTRAINT [DF__portfolio__chang__009FF5AC]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[portfolio_eligibility]
	ADD
	CONSTRAINT [DF__portfolio__creat__7FABD173]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[portfolio_eligibility] SET (LOCK_ESCALATION = TABLE)
GO
