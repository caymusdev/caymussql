SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[funding_bonuses] (
		[funding_bonus_id]          [bigint] NOT NULL,
		[funding_id]                [bigint] NULL,
		[bonus_percent_level_1]     [numeric](16, 4) NULL,
		[bonus_percent_level_2]     [numeric](16, 4) NULL,
		[bonus_percent_level_3]     [numeric](16, 4) NULL,
		[bonus_percent_level_4]     [numeric](16, 4) NULL,
		[bonus_percent_level_5]     [numeric](16, 4) NULL,
		[bonus_amount_level_1]      [numeric](16, 4) NULL,
		[bonus_amount_level_2]      [numeric](16, 4) NULL,
		[bonus_amount_level_3]      [numeric](16, 4) NULL,
		[bonus_amount_level_4]      [numeric](16, 4) NULL,
		[bonus_amount_level_5]      [numeric](16, 4) NULL,
		[iso_name]                  [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[iso_id]                    [bigint] NULL,
		[bonus_template_id]         [bigint] NULL,
		[iso_bonus_id]              [bigint] NULL,
		[bonus_metric]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[bonus_filter]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]               [datetime] NOT NULL,
		[change_date]               [datetime] NOT NULL,
		[change_user]               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_funding_bonus_id]
		PRIMARY KEY
		CLUSTERED
		([funding_bonus_id] DESC)
)
GO
ALTER TABLE [dbo].[funding_bonuses]
	ADD
	CONSTRAINT [DF__funding_b__creat__3F3C46A3]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[funding_bonuses]
	ADD
	CONSTRAINT [DF__funding_b__chang__40306ADC]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[funding_bonuses] SET (LOCK_ESCALATION = TABLE)
GO
