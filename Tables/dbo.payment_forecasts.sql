SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[payment_forecasts] (
		[funding_id]                  [bigint] NOT NULL,
		[payment_date]                [date] NOT NULL,
		[amount]                      [money] NOT NULL,
		[rec_amount]                  [money] NULL,
		[payment_schedule_id]         [int] NULL,
		[id]                          [int] IDENTITY(1, 1) NOT NULL,
		[create_date]                 [datetime] NULL,
		[funding_payment_date_id]     [int] NULL,
		[change_date]                 [datetime] NULL,
		[change_user]                 [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT ['pk_payment_forecasts_id']
		PRIMARY KEY
		CLUSTERED
		([id])
)
GO
ALTER TABLE [dbo].[payment_forecasts]
	ADD
	CONSTRAINT [DF__payment_f__creat__0AF29B96]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[payment_forecasts] SET (LOCK_ESCALATION = TABLE)
GO
