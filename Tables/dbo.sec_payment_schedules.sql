SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sec_payment_schedules] (
		[sec_payment_sched_id]     [int] IDENTITY(1, 1) NOT NULL,
		[submitter_user_id]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[approvers]                [nvarchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_sec_payment_schedules_id]
		PRIMARY KEY
		CLUSTERED
		([sec_payment_sched_id] DESC)
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sec_payment_schedules] SET (LOCK_ESCALATION = TABLE)
GO
