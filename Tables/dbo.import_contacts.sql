SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[import_contacts] (
		[contact_id]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[affiliate_id]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[merchant_id]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contract_id]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contact_person_type]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[active]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contact_first_name]      [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contact_middle_name]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contact_last_name]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[business_name]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[note]                    [nvarchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[email]                   [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[import_id]               [int] IDENTITY(1, 1) NOT NULL,
		[create_date]             [datetime] NOT NULL,
		[change_date]             [datetime] NOT NULL,
		[change_user]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[processed_date]          [datetime] NULL,
		[bad_record]              [bit] NULL,
		CONSTRAINT [pk_import_contacts_id]
		PRIMARY KEY
		CLUSTERED
		([import_id])
)
GO
ALTER TABLE [dbo].[import_contacts]
	ADD
	CONSTRAINT [DF__import_co__creat__4944D3CA]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[import_contacts]
	ADD
	CONSTRAINT [DF__import_co__chang__4A38F803]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[import_contacts]
	ADD
	CONSTRAINT [DF__import_co__chang__4B2D1C3C]
	DEFAULT ('SYSTEM') FOR [change_user]
GO
ALTER TABLE [dbo].[import_contacts]
	ADD
	CONSTRAINT [DF__import_co__creat__4C214075]
	DEFAULT ('SYSTEM') FOR [create_user]
GO
CREATE NONCLUSTERED INDEX [idx_import_contacts_affiliate_id]
	ON [dbo].[import_contacts] ([affiliate_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_import_contacts_merchant_id]
	ON [dbo].[import_contacts] ([merchant_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_import_contacts_contract_id]
	ON [dbo].[import_contacts] ([contract_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[import_contacts] SET (LOCK_ESCALATION = TABLE)
GO
