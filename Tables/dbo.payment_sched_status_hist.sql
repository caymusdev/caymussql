SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[payment_sched_status_hist] (
		[id]                            [int] IDENTITY(1, 1) NOT NULL,
		[payment_sched_id]              [int] NULL,
		[payment_sched_status_code]     [int] NULL,
		[start_date]                    [datetime] NULL,
		[end_date]                      [datetime] NULL,
		[create_date]                   [datetime] NULL,
		[change_date]                   [datetime] NULL,
		[change_user]                   [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_payment_sched_statuses_id]
		PRIMARY KEY
		CLUSTERED
		([id])
)
GO
ALTER TABLE [dbo].[payment_sched_status_hist]
	ADD
	CONSTRAINT [DF__payment_s__creat__0FB750B3]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[payment_sched_status_hist]
	WITH CHECK
	ADD CONSTRAINT [fk_pay_sched_status__id]
	FOREIGN KEY ([payment_sched_status_code]) REFERENCES [dbo].[payment_schedule_status] ([id])
ALTER TABLE [dbo].[payment_sched_status_hist]
	CHECK CONSTRAINT [fk_pay_sched_status__id]

GO
ALTER TABLE [dbo].[payment_sched_status_hist]
	WITH CHECK
	ADD CONSTRAINT [fk_pay_sched_status_pay_sched]
	FOREIGN KEY ([payment_sched_id]) REFERENCES [dbo].[payment_schedules] ([payment_schedule_id])
ALTER TABLE [dbo].[payment_sched_status_hist]
	CHECK CONSTRAINT [fk_pay_sched_status_pay_sched]

GO
CREATE NONCLUSTERED INDEX [idx_pps__sched_dates]
	ON [dbo].[payment_sched_status_hist] ([start_date] DESC, [end_date] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_pps_payment_sched_id]
	ON [dbo].[payment_sched_status_hist] ([payment_sched_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[payment_sched_status_hist] SET (LOCK_ESCALATION = TABLE)
GO
