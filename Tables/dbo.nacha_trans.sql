SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[nacha_trans] (
		[id]               [int] IDENTITY(1, 1) NOT NULL,
		[batch_id]         [int] NULL,
		[funding_id]       [bigint] NULL,
		[payment_id]       [bigint] NULL,
		[reason_code]      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[settle_type]      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[trans_amount]     [money] NULL,
		[company_name]     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[settle_date]      [datetime] NULL,
		[file_name]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]      [datetime] NOT NULL,
		CONSTRAINT [pk_nacha_trans_id]
		PRIMARY KEY
		CLUSTERED
		([id])
)
GO
ALTER TABLE [dbo].[nacha_trans]
	ADD
	CONSTRAINT [DF__nacha_tra__creat__09FE775D]
	DEFAULT (getutcdate()) FOR [create_date]
GO
CREATE NONCLUSTERED INDEX [idx_nacha_trans_batch_id]
	ON [dbo].[nacha_trans] ([batch_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_nacha_trans_funding_id]
	ON [dbo].[nacha_trans] ([funding_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_nacha_trans_payment_id]
	ON [dbo].[nacha_trans] ([payment_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[nacha_trans] SET (LOCK_ESCALATION = TABLE)
GO
