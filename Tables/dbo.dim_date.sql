SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dim_date] (
		[DateKey]                     [int] NOT NULL,
		[Date]                        [datetime] NULL,
		[FullDateUK]                  [char](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FullDateUSA]                 [char](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DayOfMonth]                  [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DaySuffix]                   [varchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DayName]                     [varchar](9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DayOfWeekUSA]                [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DayOfWeekUK]                 [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DayOfWeekInMonth]            [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DayOfWeekInYear]             [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DayOfQuarter]                [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DayOfYear]                   [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[WeekOfMonth]                 [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[WeekOfQuarter]               [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[WeekOfYear]                  [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Month]                       [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MonthName]                   [varchar](9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MonthOfQuarter]              [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Quarter]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[QuarterName]                 [varchar](9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Year]                        [char](4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[YearName]                    [char](7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MonthYear]                   [char](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MMYYYY]                      [char](6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FirstDayOfMonth]             [date] NULL,
		[LastDayOfMonth]              [date] NULL,
		[FirstDayOfQuarter]           [date] NULL,
		[LastDayOfQuarter]            [date] NULL,
		[FirstDayOfYear]              [date] NULL,
		[LastDayOfYear]               [date] NULL,
		[IsHolidayUSA]                [bit] NULL,
		[IsWeekday]                   [bit] NULL,
		[HolidayUSA]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsHolidayUK]                 [bit] NULL,
		[HolidayUK]                   [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FiscalDayOfYear]             [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FiscalWeekOfYear]            [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FiscalMonth]                 [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FiscalQuarter]               [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FiscalQuarterName]           [varchar](9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FiscalYear]                  [char](4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FiscalYearName]              [char](7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FiscalMonthYear]             [char](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FiscalMMYYYY]                [char](6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FiscalFirstDayOfMonth]       [date] NULL,
		[FiscalLastDayOfMonth]        [date] NULL,
		[FiscalFirstDayOfQuarter]     [date] NULL,
		[FiscalLastDayOfQuarter]      [date] NULL,
		[FiscalFirstDayOfYear]        [date] NULL,
		[FiscalLastDayOfYear]         [date] NULL,
		[create_date]                 [datetime] NULL,
		CONSTRAINT [PK__dim_date__40DF45E320AE9C09]
		PRIMARY KEY
		CLUSTERED
		([DateKey])
)
GO
ALTER TABLE [dbo].[dim_date]
	ADD
	CONSTRAINT [DF__dim_date__create__65C116E7]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[dim_date] SET (LOCK_ESCALATION = TABLE)
GO
