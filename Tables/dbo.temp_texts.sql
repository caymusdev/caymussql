SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_texts] (
		[temp_text_id]     [int] IDENTITY(1, 1) NOT NULL,
		[concat_ref]       [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[concat_total]     [int] NULL,
		[concat_part]      [int] NULL,
		[text_id]          [int] NULL,
		[to]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[from]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[received]         [datetime] NULL,
		[text_message]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[text_type]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[text_data]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[message_id]       [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]      [datetime] NOT NULL,
		[change_date]      [datetime] NOT NULL,
		[change_user]      [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]      [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_temp_texts_id]
		PRIMARY KEY
		CLUSTERED
		([temp_text_id] DESC)
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[temp_texts]
	ADD
	CONSTRAINT [DF__temp_text__creat__4FBCC72F]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[temp_texts]
	ADD
	CONSTRAINT [DF__temp_text__chang__50B0EB68]
	DEFAULT (getutcdate()) FOR [change_date]
GO
CREATE NONCLUSTERED INDEX [idx_temp_texts_ref]
	ON [dbo].[temp_texts] ([concat_ref] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[temp_texts] SET (LOCK_ESCALATION = TABLE)
GO
