SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sms_logs] (
		[sms_log_id]         [int] IDENTITY(1, 1) NOT NULL,
		[message_id]         [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[number_to_text]     [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[text_to_send]       [nvarchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_user]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[success]            [bit] NULL,
		[status_message]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]        [datetime] NOT NULL,
		CONSTRAINT [pk_sms_logs_sms_log_id]
		PRIMARY KEY
		CLUSTERED
		([sms_log_id] DESC)
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[sms_logs]
	ADD
	CONSTRAINT [DF__sms_logs__create__23BE4960]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[sms_logs] SET (LOCK_ESCALATION = TABLE)
GO
