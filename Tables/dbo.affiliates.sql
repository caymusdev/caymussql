SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[affiliates] (
		[affiliate_id]              [bigint] NOT NULL,
		[affiliate_name]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[status]                    [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[sent_to_collect]           [datetime] NULL,
		[closed_in_collections]     [datetime] NULL,
		[collection_level]          [int] NULL,
		[collect_status]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]               [datetime] NULL,
		[temp_id]                   [bigint] IDENTITY(1, 1) NOT NULL,
		[change_date]               [datetime] NULL,
		[change_user]               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[source_affiliate_id]       [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[gross_due]                 [money] NULL,
		[create_user]               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_affiliates_id]
		PRIMARY KEY
		CLUSTERED
		([affiliate_id])
)
GO
ALTER TABLE [dbo].[affiliates]
	ADD
	CONSTRAINT [DF__affiliate__creat__558AAF1E]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[affiliates] SET (LOCK_ESCALATION = TABLE)
GO
