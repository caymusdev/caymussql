SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lexis_nexis] (
		[lexis_nexis_id]     [int] IDENTITY(1, 1) NOT NULL,
		[merchant_id]        [bigint] NULL,
		[request_type]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[raw_request]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[raw_response]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]        [datetime] NOT NULL,
		[change_date]        [datetime] NOT NULL,
		[change_user]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_merchant_type_id]
		PRIMARY KEY
		CLUSTERED
		([lexis_nexis_id] DESC)
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[lexis_nexis]
	ADD
	CONSTRAINT [DF__lexis_nex__chang__004AEFF1]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[lexis_nexis]
	ADD
	CONSTRAINT [DF__lexis_nex__creat__7F56CBB8]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[lexis_nexis] SET (LOCK_ESCALATION = TABLE)
GO
