SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[funding_collections] (
		[funding_collection_id]     [bigint] IDENTITY(1, 1) NOT NULL,
		[funding_id]                [bigint] NOT NULL,
		[collection_type]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[start_date]                [datetime] NULL,
		[end_date]                  [datetime] NULL,
		[assigned_collector]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]               [datetime] NULL,
		[change_date]               [datetime] NULL,
		[change_user]               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_funding_collection_id]
		PRIMARY KEY
		CLUSTERED
		([funding_collection_id])
)
GO
ALTER TABLE [dbo].[funding_collections]
	ADD
	CONSTRAINT [DF__funding_c__creat__7132C993]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[funding_collections]
	ADD
	CONSTRAINT [DF_funding_collections_change_date]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[funding_collections]
	WITH CHECK
	ADD CONSTRAINT [fk_fundings_collect_funding_id]
	FOREIGN KEY ([funding_id]) REFERENCES [dbo].[fundings] ([id])
ALTER TABLE [dbo].[funding_collections]
	CHECK CONSTRAINT [fk_fundings_collect_funding_id]

GO
CREATE NONCLUSTERED INDEX [idx_fund_collect_dates]
	ON [dbo].[funding_collections] ([start_date] DESC, [end_date] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_fund_collect_funding_id]
	ON [dbo].[funding_collections] ([funding_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[funding_collections] SET (LOCK_ESCALATION = TABLE)
GO
