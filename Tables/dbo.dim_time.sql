SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dim_time] (
		[TimeKey]                   [int] NOT NULL,
		[TimeAltKey]                [int] NOT NULL,
		[Time30]                    [varchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Hour30]                    [tinyint] NOT NULL,
		[MinuteNumber]              [tinyint] NOT NULL,
		[SecondNumber]              [tinyint] NOT NULL,
		[TimeInSecond]              [int] NOT NULL,
		[HourlyBucket]              [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DayTimeBucketGroupKey]     [int] NOT NULL,
		[DayTimeBucket]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[create_date]               [datetime] NULL,
		CONSTRAINT [PK_dim_tme]
		PRIMARY KEY
		CLUSTERED
		([TimeKey])
)
GO
ALTER TABLE [dbo].[dim_time]
	ADD
	CONSTRAINT [DF__dim_time__create__66B53B20]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[dim_time] SET (LOCK_ESCALATION = TABLE)
GO
