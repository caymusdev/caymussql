SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[funding_payment_schedules] (
		[id]                      [int] IDENTITY(1, 1) NOT NULL,
		[funding_id]              [bigint] NOT NULL,
		[payment_schedule_id]     [int] NOT NULL,
		[change_date]             [datetime] NULL,
		[change_user]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [funding_payment_schedules_id]
		PRIMARY KEY
		CLUSTERED
		([id])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[funding_payment_schedules]
	WITH CHECK
	ADD CONSTRAINT [fk_funding_payment_schedule]
	FOREIGN KEY ([payment_schedule_id]) REFERENCES [dbo].[payment_schedules] ([payment_schedule_id])
ALTER TABLE [dbo].[funding_payment_schedules]
	CHECK CONSTRAINT [fk_funding_payment_schedule]

GO
ALTER TABLE [dbo].[funding_payment_schedules]
	WITH CHECK
	ADD CONSTRAINT [fk_funding_payment_funding]
	FOREIGN KEY ([funding_id]) REFERENCES [dbo].[fundings] ([id])
ALTER TABLE [dbo].[funding_payment_schedules]
	CHECK CONSTRAINT [fk_funding_payment_funding]

GO
CREATE NONCLUSTERED INDEX [idx_fps_funding_id]
	ON [dbo].[funding_payment_schedules] ([funding_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_fps_ps_id]
	ON [dbo].[funding_payment_schedules] ([payment_schedule_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[funding_payment_schedules] SET (LOCK_ESCALATION = TABLE)
GO
