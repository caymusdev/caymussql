SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[states] (
		[state_id]        [int] IDENTITY(1, 1) NOT NULL,
		[state_code]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[state_name]      [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[country_iso]     [char](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [states_iso]
		PRIMARY KEY
		CLUSTERED
		([state_id])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[states]
	WITH CHECK
	ADD CONSTRAINT [fk_states_countries]
	FOREIGN KEY ([country_iso]) REFERENCES [dbo].[countries] ([iso])
	ON DELETE CASCADE
ALTER TABLE [dbo].[states]
	CHECK CONSTRAINT [fk_states_countries]

GO
ALTER TABLE [dbo].[states] SET (LOCK_ESCALATION = TABLE)
GO
