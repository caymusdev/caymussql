SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[merchant_bank_accounts] (
		[merchant_bank_account_id]     [int] IDENTITY(1, 1) NOT NULL,
		[merchant_id]                  [bigint] NOT NULL,
		[bank_name]                    [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[routing_number]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[account_number]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[account_status]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]                  [datetime] NOT NULL,
		[change_date]                  [datetime] NOT NULL,
		[change_user]                  [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[nickname]                     [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[affiliate_id]                 [bigint] NULL,
		CONSTRAINT [pk_merchant_bank_accounts_id]
		PRIMARY KEY
		CLUSTERED
		([merchant_bank_account_id])
)
GO
ALTER TABLE [dbo].[merchant_bank_accounts]
	ADD
	CONSTRAINT [DF__merchant___creat__04459E07]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[merchant_bank_accounts]
	WITH CHECK
	ADD CONSTRAINT [fk_merchants_merchant_bank_accounts]
	FOREIGN KEY ([merchant_id]) REFERENCES [dbo].[merchants] ([merchant_id])
	ON DELETE CASCADE
ALTER TABLE [dbo].[merchant_bank_accounts]
	CHECK CONSTRAINT [fk_merchants_merchant_bank_accounts]

GO
CREATE NONCLUSTERED INDEX [idx_merchant_bank_accounts_merchant_id]
	ON [dbo].[merchant_bank_accounts] ([merchant_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[merchant_bank_accounts] SET (LOCK_ESCALATION = TABLE)
GO
