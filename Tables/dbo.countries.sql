SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[countries] (
		[iso]              [char](2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[name]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[display_name]     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[iso3]             [char](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[numcode]          [smallint] NULL,
		CONSTRAINT [countries_iso]
		PRIMARY KEY
		CLUSTERED
		([iso])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[countries] SET (LOCK_ESCALATION = TABLE)
GO
