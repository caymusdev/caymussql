SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fundings_audit] (
		[audit_id]             [int] IDENTITY(1, 1) NOT NULL,
		[id]                   [bigint] NULL,
		[funding_id]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_date]          [datetime] NOT NULL,
		[change_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_category]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[field]                [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[previous_value]       [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[changed_to_value]     [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_fundings_audit_id]
		PRIMARY KEY
		CLUSTERED
		([audit_id])
)
GO
ALTER TABLE [dbo].[fundings_audit]
	ADD
	CONSTRAINT [DF__fundings___chang__00750D23]
	DEFAULT (getutcdate()) FOR [change_date]
GO
CREATE NONCLUSTERED INDEX [idx_fundings_audit_change_date]
	ON [dbo].[fundings_audit] ([change_date] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_fundings_audit_field_value]
	ON [dbo].[fundings_audit] ([id] DESC, [field], [changed_to_value])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_fundings_audit_funding_id]
	ON [dbo].[fundings_audit] ([id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_fundings_audit_field]
	ON [dbo].[fundings_audit] ([field])
	INCLUDE ([id], [change_date], [changed_to_value])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[fundings_audit] SET (LOCK_ESCALATION = TABLE)
GO
