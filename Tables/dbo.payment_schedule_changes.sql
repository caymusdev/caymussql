SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[payment_schedule_changes] (
		[id]                      [int] IDENTITY(1, 1) NOT NULL,
		[change_type]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[payment_schedule_id]     [int] NULL,
		[frequency]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[amount]                  [money] NOT NULL,
		[start_date]              [date] NOT NULL,
		[end_date]                [date] NULL,
		[active]                  [bit] NOT NULL,
		[bank_name]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[routing_number]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[account_number]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[userid]                  [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[funding_id]              [bigint] NULL,
		[approved_by]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[approved_date]           [datetime] NULL,
		[comments]                [nvarchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_method]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_processor]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]             [datetime] NULL,
		[change_reason_id]        [int] NULL,
		[part_of_plan]            [bit] NULL,
		[change_date]             [datetime] NULL,
		[change_user]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_payment_schedule_changes_id]
		PRIMARY KEY
		CLUSTERED
		([id])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[payment_schedule_changes]
	ADD
	CONSTRAINT [DF_payment_schedule_changes_create_date]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[payment_schedule_changes] SET (LOCK_ESCALATION = TABLE)
GO
