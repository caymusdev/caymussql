SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[import_contact_addresses] (
		[contact_id]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[address_number]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[street_name]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[suffix]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[postdirectional]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[city]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[state]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[postal_code]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[import_id]           [int] IDENTITY(1, 1) NOT NULL,
		[create_date]         [datetime] NOT NULL,
		[change_date]         [datetime] NOT NULL,
		[change_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[processed_date]      [datetime] NULL,
		[bad_record]          [bit] NULL,
		CONSTRAINT [pk_import_contact_addresses_id]
		PRIMARY KEY
		CLUSTERED
		([import_id])
)
GO
ALTER TABLE [dbo].[import_contact_addresses]
	ADD
	CONSTRAINT [DF__import_co__creat__54B68676]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[import_contact_addresses]
	ADD
	CONSTRAINT [DF__import_co__chang__55AAAAAF]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[import_contact_addresses]
	ADD
	CONSTRAINT [DF__import_co__chang__569ECEE8]
	DEFAULT ('SYSTEM') FOR [change_user]
GO
ALTER TABLE [dbo].[import_contact_addresses]
	ADD
	CONSTRAINT [DF__import_co__creat__5792F321]
	DEFAULT ('SYSTEM') FOR [create_user]
GO
ALTER TABLE [dbo].[import_contact_addresses] SET (LOCK_ESCALATION = TABLE)
GO
