SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[recordings] (
		[recordings_id]         [int] IDENTITY(1, 1) NOT NULL,
		[CallSid]               [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RecordingUrl]          [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RecordingSid]          [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RecordingDuration]     [int] NULL,
		[Caller]                [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Called]                [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]           [datetime] NOT NULL,
		[change_date]           [datetime] NOT NULL,
		[change_user]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_recordings_id]
		PRIMARY KEY
		CLUSTERED
		([recordings_id])
)
GO
ALTER TABLE [dbo].[recordings]
	ADD
	CONSTRAINT [DF__recording__creat__2C09769E]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[recordings]
	ADD
	CONSTRAINT [DF__recording__chang__2CFD9AD7]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[recordings] SET (LOCK_ESCALATION = TABLE)
GO
