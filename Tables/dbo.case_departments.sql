SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[case_departments] (
		[case_department_id]     [int] IDENTITY(1, 1) NOT NULL,
		[case_id]                [int] NULL,
		[department_id]          [int] NULL,
		[start_date]             [datetime] NULL,
		[end_date]               [datetime] NULL,
		[do_not_move]            [datetime] NULL,
		[do_not_move_by]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[next_move_on]           [datetime] NULL,
		[next_move_to]           [int] NULL,
		[create_date]            [datetime] NOT NULL,
		[change_date]            [datetime] NOT NULL,
		[change_user]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[do_not_move_until]      [datetime] NULL,
		CONSTRAINT [pk_case_departments_id]
		PRIMARY KEY
		CLUSTERED
		([case_department_id] DESC)
)
GO
ALTER TABLE [dbo].[case_departments]
	ADD
	CONSTRAINT [DF__case_depa__creat__6B2FD77A]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[case_departments]
	ADD
	CONSTRAINT [DF__case_depa__chang__6C23FBB3]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[case_departments]
	WITH CHECK
	ADD CONSTRAINT [fk_case_departments_cases]
	FOREIGN KEY ([case_id]) REFERENCES [dbo].[cases] ([case_id])
ALTER TABLE [dbo].[case_departments]
	CHECK CONSTRAINT [fk_case_departments_cases]

GO
ALTER TABLE [dbo].[case_departments]
	WITH CHECK
	ADD CONSTRAINT [fk_case_departments_departments]
	FOREIGN KEY ([department_id]) REFERENCES [dbo].[departments] ([department_id])
ALTER TABLE [dbo].[case_departments]
	CHECK CONSTRAINT [fk_case_departments_departments]

GO
CREATE NONCLUSTERED INDEX [idx_case_departments]
	ON [dbo].[case_departments] ([case_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[case_departments] SET (LOCK_ESCALATION = TABLE)
GO
