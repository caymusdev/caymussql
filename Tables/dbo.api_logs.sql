SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[api_logs] (
		[api_log_id]       [int] IDENTITY(1, 1) NOT NULL,
		[api_call]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[api_raw_call]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[api_response]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]      [datetime] NOT NULL,
		[create_user]      [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_date]      [datetime] NULL,
		[change_user]      [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_api_log]
		PRIMARY KEY
		CLUSTERED
		([api_log_id] DESC)
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[api_logs]
	ADD
	CONSTRAINT [DF__api_logs__create__58671BC9]
	DEFAULT (getutcdate()) FOR [create_date]
GO
CREATE NONCLUSTERED INDEX [idx_api_log_create_date]
	ON [dbo].[api_logs] ([create_date] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_api_logs_api_call]
	ON [dbo].[api_logs] ([api_call])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[api_logs] SET (LOCK_ESCALATION = TABLE)
GO
