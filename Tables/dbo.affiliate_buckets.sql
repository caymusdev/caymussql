SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[affiliate_buckets] (
		[affiliate_bucket_id]     [int] IDENTITY(1, 1) NOT NULL,
		[affiliate_id]            [bigint] NOT NULL,
		[create_date]             [datetime] NOT NULL,
		[change_date]             [datetime] NULL,
		[change_user]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[collection_level]        [int] NOT NULL,
		[start_date]              [datetime] NOT NULL,
		[end_date]                [datetime] NULL,
		[change_reason]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_plan_date]       [date] NULL,
		CONSTRAINT [pk_affiliate_buckets_id]
		PRIMARY KEY
		CLUSTERED
		([affiliate_bucket_id])
)
GO
ALTER TABLE [dbo].[affiliate_buckets]
	ADD
	CONSTRAINT [DF__affiliate__creat__52AE4273]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[affiliate_buckets]
	ADD
	CONSTRAINT [DF_affiliate_buckets_change_date]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[affiliate_buckets]
	WITH CHECK
	ADD CONSTRAINT [fk_affiliate_buckets_affiliate_id]
	FOREIGN KEY ([affiliate_id]) REFERENCES [dbo].[affiliates] ([affiliate_id])
ALTER TABLE [dbo].[affiliate_buckets]
	CHECK CONSTRAINT [fk_affiliate_buckets_affiliate_id]

GO
CREATE NONCLUSTERED INDEX [idx_affiliate_buckets_affiliate_id]
	ON [dbo].[affiliate_buckets] ([affiliate_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[affiliate_buckets] SET (LOCK_ESCALATION = TABLE)
GO
