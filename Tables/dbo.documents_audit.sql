SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[documents_audit] (
		[audit_id]         [int] IDENTITY(1, 1) NOT NULL,
		[documents_id]     [int] NULL,
		[change_date]      [datetime] NOT NULL,
		[change_user]      [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]      [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[create_user]      [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_documents_audit_audit_id]
		PRIMARY KEY
		CLUSTERED
		([audit_id])
)
GO
ALTER TABLE [dbo].[documents_audit]
	ADD
	CONSTRAINT [DF__documents__chang__1372D2FE]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[documents_audit]
	ADD
	CONSTRAINT [DF__documents__creat__1466F737]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[documents_audit] SET (LOCK_ESCALATION = TABLE)
GO
