SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ips_staging] (
		[ips_staging_id]         [int] IDENTITY(1, 1) NOT NULL,
		[report_name]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cash_company]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[settle_date]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[merch_footer]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[merchant_total_vol]     [money] NULL,
		[merch_total_hold]       [money] NULL,
		[file_date]              [date] NULL,
		[merch_no]               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[merch_name]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[total_vol]              [money] NULL,
		[hold_amt]               [money] NULL,
		[hold_rate]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[sourcefile]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]            [datetime] NULL,
		[BatchID]                [int] NULL,
		CONSTRAINT [pk_ips_staging_id]
		PRIMARY KEY
		CLUSTERED
		([ips_staging_id] DESC)
)
GO
ALTER TABLE [dbo].[ips_staging]
	ADD
	CONSTRAINT [DF__ips_stagi__creat__025D5595]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[ips_staging]
	WITH NOCHECK
	ADD CONSTRAINT [fk_ips_staging_batches]
	FOREIGN KEY ([BatchID]) REFERENCES [dbo].[batches] ([batch_id])
	ON DELETE CASCADE
ALTER TABLE [dbo].[ips_staging]
	CHECK CONSTRAINT [fk_ips_staging_batches]

GO
CREATE NONCLUSTERED INDEX [IX_ips_staging_file_date]
	ON [dbo].[ips_staging] ([file_date] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ips_staging_merchno]
	ON [dbo].[ips_staging] ([merch_no])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[ips_staging] SET (LOCK_ESCALATION = TABLE)
GO
