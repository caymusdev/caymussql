SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[exception_logs] (
		[exception_log_id]     [int] IDENTITY(1, 1) NOT NULL,
		[create_date]          [datetime] NOT NULL,
		[exception]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[user_id]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[stack_trace]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[method_name]          [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[file_path]            [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[line_number]          [int] NULL,
		[comments]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_date]          [datetime] NULL,
		[change_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_exception_log]
		PRIMARY KEY
		CLUSTERED
		([exception_log_id] DESC)
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[exception_logs]
	ADD
	CONSTRAINT [DF__exception__creat__689D8392]
	DEFAULT (getutcdate()) FOR [create_date]
GO
CREATE NONCLUSTERED INDEX [idx_exception_log_create_date]
	ON [dbo].[exception_logs] ([create_date] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[exception_logs] SET (LOCK_ESCALATION = TABLE)
GO
