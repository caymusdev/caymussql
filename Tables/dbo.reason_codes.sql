SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[reason_codes] (
		[reason_code_id]         [int] IDENTITY(1, 1) NOT NULL,
		[reason_type]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[reason_text]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[reason_description]     [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]            [datetime] NOT NULL,
		[change_date]            [datetime] NULL,
		[change_user]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_reason_code_id]
		PRIMARY KEY
		CLUSTERED
		([reason_code_id])
)
GO
ALTER TABLE [dbo].[reason_codes]
	ADD
	CONSTRAINT [DF__reason_co__creat__1E05700A]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[reason_codes] SET (LOCK_ESCALATION = TABLE)
GO
