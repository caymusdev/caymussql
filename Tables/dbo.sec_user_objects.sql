SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sec_user_objects] (
		[id]                [int] IDENTITY(1, 1) NOT NULL,
		[sec_object_id]     [int] NOT NULL,
		[userid]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[read_only]         [bit] NOT NULL,
		CONSTRAINT [pk_sec_user_objects_id]
		PRIMARY KEY
		CLUSTERED
		([id])
)
GO
ALTER TABLE [dbo].[sec_user_objects]
	ADD
	CONSTRAINT [DF__sec_user___read___21D600EE]
	DEFAULT ((0)) FOR [read_only]
GO
ALTER TABLE [dbo].[sec_user_objects]
	WITH CHECK
	ADD CONSTRAINT [fk_sec_objects_sec_user_objects]
	FOREIGN KEY ([sec_object_id]) REFERENCES [dbo].[sec_objects] ([sec_object_id])
ALTER TABLE [dbo].[sec_user_objects]
	CHECK CONSTRAINT [fk_sec_objects_sec_user_objects]

GO
CREATE NONCLUSTERED INDEX [idx_sec_user_objects]
	ON [dbo].[sec_user_objects] ([sec_object_id], [userid])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[sec_user_objects] SET (LOCK_ESCALATION = TABLE)
GO
