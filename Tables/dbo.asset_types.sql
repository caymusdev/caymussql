SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[asset_types] (
		[asset_types_id]             [int] IDENTITY(1, 1) NOT NULL,
		[asset_type_name]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[asset_type_description]     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_date]                [datetime] NOT NULL,
		[change_user]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]                [datetime] NOT NULL,
		[create_user]                [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_asset_types_asset_type_id]
		PRIMARY KEY
		CLUSTERED
		([asset_types_id])
)
GO
ALTER TABLE [dbo].[asset_types]
	ADD
	CONSTRAINT [DF__asset_typ__chang__78BEDCC2]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[asset_types]
	ADD
	CONSTRAINT [DF__asset_typ__creat__79B300FB]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[asset_types] SET (LOCK_ESCALATION = TABLE)
GO
