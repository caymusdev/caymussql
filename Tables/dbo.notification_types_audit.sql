SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[notification_types_audit] (
		[audit_id]                 [int] IDENTITY(1, 1) NOT NULL,
		[notification_type_id]     [int] NULL,
		[change_date]              [datetime] NOT NULL,
		[change_user]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_category]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[field]                    [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[previous_value]           [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[changed_to_value]         [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_notification_types_audit_id]
		PRIMARY KEY
		CLUSTERED
		([audit_id] DESC)
)
GO
ALTER TABLE [dbo].[notification_types_audit]
	ADD
	CONSTRAINT [DF__notificat__chang__575DE8F7]
	DEFAULT (getutcdate()) FOR [change_date]
GO
CREATE NONCLUSTERED INDEX [idx_notification_types_audit_change_date]
	ON [dbo].[notification_types_audit] ([change_date] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[notification_types_audit] SET (LOCK_ESCALATION = TABLE)
GO
