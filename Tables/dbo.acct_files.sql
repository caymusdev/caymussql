SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[acct_files] (
		[acct_file_id]        [int] IDENTITY(1, 1) NOT NULL,
		[acct_file_name]      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[create_date]         [datetime] NOT NULL,
		[userid]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[row_count]           [int] NULL,
		[batch_ids]           [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[downloaded_last]     [datetime] NULL,
		[acct_folder]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_date]         [datetime] NULL,
		[change_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_acct_files_id]
		PRIMARY KEY
		CLUSTERED
		([acct_file_id] DESC)
)
GO
ALTER TABLE [dbo].[acct_files]
	ADD
	CONSTRAINT [DF__acct_file__creat__4FD1D5C8]
	DEFAULT (getutcdate()) FOR [create_date]
GO
CREATE NONCLUSTERED INDEX [idx_acct_files_create_date]
	ON [dbo].[acct_files] ([create_date] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[acct_files] SET (LOCK_ESCALATION = TABLE)
GO
