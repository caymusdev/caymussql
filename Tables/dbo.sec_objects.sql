SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sec_objects] (
		[sec_object_id]       [int] IDENTITY(1, 1) NOT NULL,
		[sec_object_name]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[sec_object_type]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [pk_sec_objects_id]
		PRIMARY KEY
		CLUSTERED
		([sec_object_id])
	ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_sec_objects]
	ON [dbo].[sec_objects] ([sec_object_name], [sec_object_type])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[sec_objects] SET (LOCK_ESCALATION = TABLE)
GO
