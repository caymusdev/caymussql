SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[payment_schedule_change_reasons] (
		[id]                   [int] IDENTITY(1, 1) NOT NULL,
		[reason]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[reason_parent_id]     [int] NULL,
		[create_date]          [datetime] NULL,
		CONSTRAINT [pk_ps_change_reasons_id]
		PRIMARY KEY
		CLUSTERED
		([id])
)
GO
ALTER TABLE [dbo].[payment_schedule_change_reasons]
	ADD
	CONSTRAINT [DF__payment_s__creat__10AB74EC]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[payment_schedule_change_reasons] SET (LOCK_ESCALATION = TABLE)
GO
