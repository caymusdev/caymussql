SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[roles] (
		[role_id]         [int] IDENTITY(1, 1) NOT NULL,
		[role_name]       [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[create_date]     [datetime] NOT NULL,
		[change_date]     [datetime] NOT NULL,
		[change_user]     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[role_desc]       [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[system_role]     [bit] NULL,
		CONSTRAINT [pk_role_id]
		PRIMARY KEY
		CLUSTERED
		([role_id])
)
GO
ALTER TABLE [dbo].[roles]
	ADD
	CONSTRAINT [DF__roles__create_da__520F23F5]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[roles]
	ADD
	CONSTRAINT [DF__roles__change_da__5303482E]
	DEFAULT (getutcdate()) FOR [change_date]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_roles_role_name]
	ON [dbo].[roles] ([role_name])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[roles] SET (LOCK_ESCALATION = TABLE)
GO
