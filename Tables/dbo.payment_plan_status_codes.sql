SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[payment_plan_status_codes] (
		[id]                     [int] IDENTITY(1, 1) NOT NULL,
		[performance_status]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[parent_id]              [int] NULL,
		[code]                   [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]            [datetime] NULL,
		CONSTRAINT [pk_ppsc_status_id]
		PRIMARY KEY
		CLUSTERED
		([id])
)
GO
ALTER TABLE [dbo].[payment_plan_status_codes]
	ADD
	CONSTRAINT [DF__payment_p__creat__0BE6BFCF]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[payment_plan_status_codes] SET (LOCK_ESCALATION = TABLE)
GO
