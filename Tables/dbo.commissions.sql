SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[commissions] (
		[commission_id]               [int] IDENTITY(1, 1) NOT NULL,
		[payment_id]                  [int] NULL,
		[trans_id]                    [int] NULL,
		[trans_date]                  [date] NULL,
		[trans_amount]                [money] NULL,
		[commission_rate]             [numeric](12, 6) NULL,
		[commission_rate_id]          [int] NULL,
		[commission_amount]           [money] NULL,
		[collection_type]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[collector]                   [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[funding_id]                  [bigint] NULL,
		[create_date]                 [datetime] NULL,
		[draft_date]                  [date] NULL,
		[total_assigned]              [int] NULL,
		[num_with_plans]              [int] NULL,
		[collection_start_date]       [datetime] NULL,
		[collection_end_date]         [datetime] NULL,
		[payment_plan_created]        [datetime] NULL,
		[payment_plan_status]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[age_in_collections]          [int] NULL,
		[payment_schedule_id]         [int] NULL,
		[affiliate_bucket]            [int] NULL,
		[target_rate_met]             [bit] NULL,
		[funding_payment_date_id]     [int] NULL,
		[change_date]                 [datetime] NULL,
		[change_user]                 [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_commissions_id]
		PRIMARY KEY
		CLUSTERED
		([commission_id])
)
GO
ALTER TABLE [dbo].[commissions]
	ADD
	CONSTRAINT [DF__commissio__creat__60FC61CA]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[commissions]
	WITH CHECK
	ADD CONSTRAINT [fk_commissions_comm_rates_id]
	FOREIGN KEY ([commission_rate_id]) REFERENCES [dbo].[commission_rates] ([commission_rate_id])
ALTER TABLE [dbo].[commissions]
	CHECK CONSTRAINT [fk_commissions_comm_rates_id]

GO
ALTER TABLE [dbo].[commissions]
	WITH CHECK
	ADD CONSTRAINT [fk_commissions_funding_id]
	FOREIGN KEY ([funding_id]) REFERENCES [dbo].[fundings] ([id])
ALTER TABLE [dbo].[commissions]
	CHECK CONSTRAINT [fk_commissions_funding_id]

GO
ALTER TABLE [dbo].[commissions]
	WITH CHECK
	ADD CONSTRAINT [fk_commissions_payment_id]
	FOREIGN KEY ([payment_id]) REFERENCES [dbo].[payments] ([payment_id])
ALTER TABLE [dbo].[commissions]
	CHECK CONSTRAINT [fk_commissions_payment_id]

GO
ALTER TABLE [dbo].[commissions]
	WITH CHECK
	ADD CONSTRAINT [fk_commissions_trans_id]
	FOREIGN KEY ([trans_id]) REFERENCES [dbo].[transactions] ([trans_id])
ALTER TABLE [dbo].[commissions]
	CHECK CONSTRAINT [fk_commissions_trans_id]

GO
CREATE NONCLUSTERED INDEX [idx_commission_dates]
	ON [dbo].[commissions] ([trans_date] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_commissions_collector]
	ON [dbo].[commissions] ([collector] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_commissions_funding_id]
	ON [dbo].[commissions] ([funding_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[commissions] SET (LOCK_ESCALATION = TABLE)
GO
