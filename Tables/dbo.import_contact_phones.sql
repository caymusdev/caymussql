SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[import_contact_phones] (
		[contact_id]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[phone_type]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[phone_number]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[import_id]          [int] IDENTITY(1, 1) NOT NULL,
		[create_date]        [datetime] NOT NULL,
		[change_date]        [datetime] NOT NULL,
		[change_user]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[processed_date]     [datetime] NULL,
		[bad_record]         [bit] NULL,
		CONSTRAINT [pk_import_contact_phones_id]
		PRIMARY KEY
		CLUSTERED
		([import_id])
)
GO
ALTER TABLE [dbo].[import_contact_phones]
	ADD
	CONSTRAINT [DF__import_co__creat__4EFDAD20]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[import_contact_phones]
	ADD
	CONSTRAINT [DF__import_co__chang__4FF1D159]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[import_contact_phones]
	ADD
	CONSTRAINT [DF__import_co__chang__50E5F592]
	DEFAULT ('SYSTEM') FOR [change_user]
GO
ALTER TABLE [dbo].[import_contact_phones]
	ADD
	CONSTRAINT [DF__import_co__creat__51DA19CB]
	DEFAULT ('SYSTEM') FOR [create_user]
GO
ALTER TABLE [dbo].[import_contact_phones] SET (LOCK_ESCALATION = TABLE)
GO
