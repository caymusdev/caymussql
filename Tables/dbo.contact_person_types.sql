SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[contact_person_types] (
		[contact_person_type_id]     [int] IDENTITY(1, 1) NOT NULL,
		[contact_type]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[contact_type_desc]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]                [datetime] NOT NULL,
		[change_date]                [datetime] NOT NULL,
		[change_user]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_contact_person_types_id]
		PRIMARY KEY
		CLUSTERED
		([contact_person_type_id] DESC)
)
GO
ALTER TABLE [dbo].[contact_person_types]
	ADD
	CONSTRAINT [DF__contact_p__creat__5A6F5FCC]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[contact_person_types]
	ADD
	CONSTRAINT [DF__contact_p__chang__5B638405]
	DEFAULT (getutcdate()) FOR [change_date]
GO
CREATE NONCLUSTERED INDEX [idx_contact_person_types_contact_type]
	ON [dbo].[contact_person_types] ([contact_type] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[contact_person_types] SET (LOCK_ESCALATION = TABLE)
GO
