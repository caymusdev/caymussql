SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[notification_types] (
		[notification_type_id]       [int] IDENTITY(1, 1) NOT NULL,
		[notification_type]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[notification_type_desc]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]                [datetime] NOT NULL,
		[change_date]                [datetime] NOT NULL,
		[change_user]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_notification_types_id]
		PRIMARY KEY
		CLUSTERED
		([notification_type_id] DESC)
)
GO
ALTER TABLE [dbo].[notification_types]
	ADD
	CONSTRAINT [DF__notificat__creat__538D5813]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[notification_types]
	ADD
	CONSTRAINT [DF__notificat__chang__54817C4C]
	DEFAULT (getutcdate()) FOR [change_date]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_notification_types_user]
	ON [dbo].[notification_types] ([notification_type] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[notification_types] SET (LOCK_ESCALATION = TABLE)
GO
