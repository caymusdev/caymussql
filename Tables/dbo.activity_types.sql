SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[activity_types] (
		[activity_type_id]       [int] NOT NULL,
		[activity_type]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[activity_type_desc]     [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]            [datetime] NOT NULL,
		[change_date]            [datetime] NOT NULL,
		[change_user]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_activity_type_id]
		PRIMARY KEY
		CLUSTERED
		([activity_type_id])
)
GO
ALTER TABLE [dbo].[activity_types]
	ADD
	CONSTRAINT [DF__activity___creat__61516785]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[activity_types]
	ADD
	CONSTRAINT [DF__activity___chang__62458BBE]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[activity_types] SET (LOCK_ESCALATION = TABLE)
GO
