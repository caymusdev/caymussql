SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[nacha_ack] (
		[id]                [int] IDENTITY(1, 1) NOT NULL,
		[customer_id]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ref_number]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[debit_amount]      [money] NULL,
		[credit_amount]     [money] NULL,
		[process_date]      [datetime] NULL,
		[date_provided]     [datetime] NULL,
		[file_name]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]       [datetime] NOT NULL,
		CONSTRAINT [pk_nacha_ack_id]
		PRIMARY KEY
		CLUSTERED
		([id])
)
GO
ALTER TABLE [dbo].[nacha_ack]
	ADD
	CONSTRAINT [DF__nacha_ack__creat__08162EEB]
	DEFAULT (getutcdate()) FOR [create_date]
GO
CREATE NONCLUSTERED INDEX [idx_nacha_ack_process_date]
	ON [dbo].[nacha_ack] ([process_date] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[nacha_ack] SET (LOCK_ESCALATION = TABLE)
GO
