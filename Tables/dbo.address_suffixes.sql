SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[address_suffixes] (
		[address_suffix_id]     [int] IDENTITY(1, 1) NOT NULL,
		[suffix]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[suffix_desc]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_date]           [datetime] NOT NULL,
		[change_user]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]           [datetime] NOT NULL,
		[create_user]           [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_address_suffixes_id]
		PRIMARY KEY
		CLUSTERED
		([address_suffix_id] DESC)
)
GO
ALTER TABLE [dbo].[address_suffixes]
	ADD
	CONSTRAINT [DF__address_s__chang__23A93AC7]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[address_suffixes]
	ADD
	CONSTRAINT [DF__address_s__creat__249D5F00]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[address_suffixes] SET (LOCK_ESCALATION = TABLE)
GO
