SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[addresses] (
		[address_id]          [int] IDENTITY(1, 1) NOT NULL,
		[affiliate_id]        [bigint] NULL,
		[merchant_id]         [bigint] NULL,
		[contract_id]         [bigint] NULL,
		[address_number]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[street_name]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[suffix_id]           [int] NULL,
		[postdirectional]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[city]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[state]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[postal_code]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_date]         [datetime] NOT NULL,
		[change_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]         [datetime] NOT NULL,
		[create_user]         [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contact_id]          [int] NULL,
		[address_type_id]     [int] NULL,
		[address_1]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[address_2]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[state_abbrev]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_addresses_id]
		PRIMARY KEY
		CLUSTERED
		([address_id] DESC)
)
GO
ALTER TABLE [dbo].[addresses]
	ADD
	CONSTRAINT [DF__addresses__chang__2B4A5C8F]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[addresses]
	ADD
	CONSTRAINT [DF__addresses__creat__2C3E80C8]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[addresses]
	WITH CHECK
	ADD CONSTRAINT [fk_addresses_merchants]
	FOREIGN KEY ([merchant_id]) REFERENCES [dbo].[merchants] ([merchant_id])
ALTER TABLE [dbo].[addresses]
	CHECK CONSTRAINT [fk_addresses_merchants]

GO
ALTER TABLE [dbo].[addresses]
	WITH CHECK
	ADD CONSTRAINT [fk_addresses_affiliates]
	FOREIGN KEY ([affiliate_id]) REFERENCES [dbo].[affiliates] ([affiliate_id])
ALTER TABLE [dbo].[addresses]
	CHECK CONSTRAINT [fk_addresses_affiliates]

GO
ALTER TABLE [dbo].[addresses]
	WITH CHECK
	ADD CONSTRAINT [fk_addresses_contracts]
	FOREIGN KEY ([contract_id]) REFERENCES [dbo].[contracts] ([contract_id])
ALTER TABLE [dbo].[addresses]
	CHECK CONSTRAINT [fk_addresses_contracts]

GO
ALTER TABLE [dbo].[addresses]
	WITH CHECK
	ADD CONSTRAINT [fk_addresses_suffix]
	FOREIGN KEY ([suffix_id]) REFERENCES [dbo].[address_suffixes] ([address_suffix_id])
ALTER TABLE [dbo].[addresses]
	CHECK CONSTRAINT [fk_addresses_suffix]

GO
ALTER TABLE [dbo].[addresses] SET (LOCK_ESCALATION = TABLE)
GO
