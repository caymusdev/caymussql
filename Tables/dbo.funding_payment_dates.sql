SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[funding_payment_dates] (
		[id]                             [bigint] IDENTITY(1, 1) NOT NULL,
		[funding_id]                     [bigint] NOT NULL,
		[payment_date]                   [date] NOT NULL,
		[amount]                         [money] NOT NULL,
		[rec_amount]                     [money] NULL,
		[payment_schedule_id]            [int] NULL,
		[create_date]                    [datetime] NULL,
		[active]                         [bit] NULL,
		[original]                       [bit] NULL,
		[chargeback_amount]              [money] NULL,
		[merchant_bank_account_id]       [int] NULL,
		[payment_method]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_processor]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_schedule_change_id]     [int] NULL,
		[comments]                       [nvarchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_reason_id]               [int] NULL,
		[change_date]                    [datetime] NULL,
		[change_user]                    [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_plan_id]                [int] NULL,
		[missed]                         [bit] NULL,
		[rec_trans_id]                   [int] NULL,
		[chargeback_trans_id]            [int] NULL,
		[reject_trans_id]                [int] NULL,
		[affiliate_id]                   [int] NULL,
		[scheduled_payment_id]           [int] NULL,
		CONSTRAINT [pk_funding_payment_dates]
		PRIMARY KEY
		CLUSTERED
		([id])
)
GO
ALTER TABLE [dbo].[funding_payment_dates]
	ADD
	CONSTRAINT [DF__funding_p__creat__76EBA2E9]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[funding_payment_dates]
	ADD
	CONSTRAINT [DF__funding_p__activ__77DFC722]
	DEFAULT ((1)) FOR [active]
GO
CREATE NONCLUSTERED INDEX [idx_funding_payment_dates_2]
	ON [dbo].[funding_payment_dates] ([payment_method], [rec_amount], [rec_trans_id])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_funding_payment_dates_3]
	ON [dbo].[funding_payment_dates] ([payment_method], [chargeback_amount])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_funding_payment_dates_main]
	ON [dbo].[funding_payment_dates] ([funding_id], [payment_date] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_funding_payment_dates_payment_method]
	ON [dbo].[funding_payment_dates] ([payment_method])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[funding_payment_dates] SET (LOCK_ESCALATION = TABLE)
GO
