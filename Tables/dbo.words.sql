SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[words] (
		[word_id]             [int] IDENTITY(1, 1) NOT NULL,
		[word]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[word_desc]           [nvarchar](400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[translated_word]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]         [datetime] NOT NULL,
		[change_date]         [datetime] NOT NULL,
		[change_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_word_id]
		PRIMARY KEY
		CLUSTERED
		([word_id])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[words]
	ADD
	CONSTRAINT [DF__words__create_da__7187CF4E]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[words]
	ADD
	CONSTRAINT [DF__words__change_da__727BF387]
	DEFAULT (getutcdate()) FOR [change_date]
GO
CREATE NONCLUSTERED INDEX [idx_word]
	ON [dbo].[words] ([word])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[words] SET (LOCK_ESCALATION = TABLE)
GO
