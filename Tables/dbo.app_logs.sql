SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[app_logs] (
		[app_log_id]      [int] IDENTITY(1, 1) NOT NULL,
		[create_date]     [datetime] NOT NULL,
		[message]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[user_id]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[module]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_app_log]
		PRIMARY KEY
		CLUSTERED
		([app_log_id] DESC)
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[app_logs]
	ADD
	CONSTRAINT [DF__app_logs__create__595B4002]
	DEFAULT (getutcdate()) FOR [create_date]
GO
CREATE NONCLUSTERED INDEX [idx_app_log_create_date]
	ON [dbo].[app_logs] ([create_date] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_app_logs_module]
	ON [dbo].[app_logs] ([module])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[app_logs] SET (LOCK_ESCALATION = TABLE)
GO
