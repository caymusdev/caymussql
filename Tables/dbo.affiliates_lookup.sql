SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[affiliates_lookup] (
		[solo_id]            [bigint] NULL,
		[affiliate_name]     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[portfolio_id]       [int] NULL,
		[createDate]         [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[affiliates_lookup] SET (LOCK_ESCALATION = TABLE)
GO
