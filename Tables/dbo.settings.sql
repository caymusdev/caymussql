SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[settings] (
		[setting_id]        [int] IDENTITY(1, 1) NOT NULL,
		[setting]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[value]             [nvarchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[setting_desc]      [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[setting_group]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[encrypt_value]     [varbinary](max) NULL,
		[change_date]       [datetime] NULL,
		[change_user]       [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]       [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]       [datetime] NULL,
		CONSTRAINT [pk_settings_id]
		PRIMARY KEY
		CLUSTERED
		([setting_id])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[settings]
	ADD
	CONSTRAINT [DF__settings__create__45A94D10]
	DEFAULT (getutcdate()) FOR [create_date]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_settings_setting]
	ON [dbo].[settings] ([setting] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[settings] SET (LOCK_ESCALATION = TABLE)
GO
