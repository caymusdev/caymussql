SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[phone_logs] (
		[phone_log_id]         [int] IDENTITY(1, 1) NOT NULL,
		[call_to]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[call_from]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[call_id]              [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[conversation_id]      [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[status]               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[direction]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[start_time]           [datetime] NULL,
		[end_time]             [datetime] NULL,
		[duration]             [int] NULL,
		[rate]                 [numeric](16, 10) NULL,
		[price]                [numeric](16, 10) NULL,
		[raw_response]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[event_type]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]          [datetime] NOT NULL,
		[change_date]          [datetime] NOT NULL,
		[change_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[affiliate_id]         [int] NULL,
		[users_email]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[step]                 [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[source]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[dial_call_status]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_phone_logs_id]
		PRIMARY KEY
		CLUSTERED
		([phone_log_id] DESC)
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[phone_logs]
	ADD
	CONSTRAINT [DF__phone_log__creat__407A839F]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[phone_logs]
	ADD
	CONSTRAINT [DF__phone_log__chang__416EA7D8]
	DEFAULT (getutcdate()) FOR [change_date]
GO
CREATE NONCLUSTERED INDEX [idx_phone_logs_to]
	ON [dbo].[phone_logs] ([call_to] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_phone_logs_from]
	ON [dbo].[phone_logs] ([call_from] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_phone_logs_call_id]
	ON [dbo].[phone_logs] ([call_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_phone_logs_conversation_id]
	ON [dbo].[phone_logs] ([conversation_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[phone_logs] SET (LOCK_ESCALATION = TABLE)
GO
