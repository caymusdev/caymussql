SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[contacts] (
		[contact_id]              [int] IDENTITY(1, 1) NOT NULL,
		[merchant_id]             [bigint] NULL,
		[affiliate_id]            [bigint] NULL,
		[contract_id]             [bigint] NULL,
		[contact_person_type]     [int] NULL,
		[active]                  [bit] NULL,
		[best_contact]            [bit] NULL,
		[contact_first_name]      [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contact_middle_name]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contact_last_name]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[business_name]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[note]                    [nvarchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[email]                   [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[success_rate]            [numeric](16, 4) NULL,
		[create_date]             [datetime] NOT NULL,
		[change_date]             [datetime] NOT NULL,
		[change_user]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[from_bossk]              [bit] NULL,
		[exported_time]           [datetime] NULL,
		[source_contact_id]       [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[de_cosigner_rowid]       [int] NULL,
		CONSTRAINT [pk_contacts_id]
		PRIMARY KEY
		CLUSTERED
		([contact_id] DESC)
)
GO
ALTER TABLE [dbo].[contacts]
	ADD
	CONSTRAINT [DF__contacts__create__69B1A35C]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[contacts]
	ADD
	CONSTRAINT [DF__contacts__change__6AA5C795]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[contacts]
	WITH CHECK
	ADD CONSTRAINT [fk_contacts_contact_person_types]
	FOREIGN KEY ([contact_person_type]) REFERENCES [dbo].[contact_person_types] ([contact_person_type_id])
ALTER TABLE [dbo].[contacts]
	CHECK CONSTRAINT [fk_contacts_contact_person_types]

GO
ALTER TABLE [dbo].[contacts]
	WITH CHECK
	ADD CONSTRAINT [fk_contacts_merchants]
	FOREIGN KEY ([merchant_id]) REFERENCES [dbo].[merchants] ([merchant_id])
ALTER TABLE [dbo].[contacts]
	CHECK CONSTRAINT [fk_contacts_merchants]

GO
ALTER TABLE [dbo].[contacts]
	WITH CHECK
	ADD CONSTRAINT [fk_contacts_affiliates]
	FOREIGN KEY ([affiliate_id]) REFERENCES [dbo].[affiliates] ([affiliate_id])
ALTER TABLE [dbo].[contacts]
	CHECK CONSTRAINT [fk_contacts_affiliates]

GO
ALTER TABLE [dbo].[contacts]
	WITH CHECK
	ADD CONSTRAINT [fk_contacts_contracts]
	FOREIGN KEY ([contract_id]) REFERENCES [dbo].[contracts] ([contract_id])
ALTER TABLE [dbo].[contacts]
	CHECK CONSTRAINT [fk_contacts_contracts]

GO
ALTER TABLE [dbo].[contacts] SET (LOCK_ESCALATION = TABLE)
GO
