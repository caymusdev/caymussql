SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[import_scheduled_payments] (
		[scheduled_payment_id]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_method]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_date]             [date] NULL,
		[payment_amount]           [money] NULL,
		[contract_id]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[merchant_id]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[affiliate_id]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[is_debit]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[bank_name]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[routing_number]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[account_number]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[import_id]                [int] IDENTITY(1, 1) NOT NULL,
		[create_date]              [datetime] NOT NULL,
		[change_date]              [datetime] NOT NULL,
		[change_user]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[processed_date]           [datetime] NULL,
		[bad_record]               [bit] NULL,
		CONSTRAINT [pk_import_scheduledpayments_id]
		PRIMARY KEY
		CLUSTERED
		([import_id])
)
GO
ALTER TABLE [dbo].[import_scheduled_payments]
	ADD
	CONSTRAINT [DF__import_sc__creat__49CEE3AF]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[import_scheduled_payments]
	ADD
	CONSTRAINT [DF__import_sc__chang__4AC307E8]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[import_scheduled_payments]
	ADD
	CONSTRAINT [DF__import_sc__chang__4BB72C21]
	DEFAULT ('SYSTEM') FOR [change_user]
GO
ALTER TABLE [dbo].[import_scheduled_payments]
	ADD
	CONSTRAINT [DF__import_sc__creat__4CAB505A]
	DEFAULT ('SYSTEM') FOR [create_user]
GO
CREATE NONCLUSTERED INDEX [idx_import_scheduled_payments_contract_id]
	ON [dbo].[import_scheduled_payments] ([contract_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[import_scheduled_payments] SET (LOCK_ESCALATION = TABLE)
GO
