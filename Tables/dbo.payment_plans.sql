SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[payment_plans] (
		[payment_plan_id]         [int] IDENTITY(1, 1) NOT NULL,
		[merchant_id]             [bigint] NULL,
		[affiliate_id]            [bigint] NULL,
		[collector]               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]             [datetime] NULL,
		[current_status]          [int] NULL,
		[broken_payment_date]     [datetime] NULL,
		[change_date]             [datetime] NULL,
		[change_user]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_plan_type]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[frequency]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[on_monday]               [bit] NULL,
		[on_tuesday]              [bit] NULL,
		[on_wednesday]            [bit] NULL,
		[on_thursday]             [bit] NULL,
		[on_friday]               [bit] NULL,
		[start_date]              [date] NULL,
		[how_long]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[num_periods]             [int] NULL,
		[payment_method]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_amount]          [money] NULL,
		[bank_account_id]         [int] NULL,
		[deleted]                 [bit] NOT NULL,
		[status_id]               [int] NULL,
		[create_user]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payment_plan_name]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_payment_plans_id]
		PRIMARY KEY
		CLUSTERED
		([payment_plan_id])
)
GO
ALTER TABLE [dbo].[payment_plans]
	ADD
	CONSTRAINT [DF__payment_p__creat__0DCF0841]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[payment_plans]
	ADD
	CONSTRAINT [DF__payment_p__delet__44B528D7]
	DEFAULT ((0)) FOR [deleted]
GO
CREATE NONCLUSTERED INDEX [idx_pay_plan_affiliate_id]
	ON [dbo].[payment_plans] ([affiliate_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_pay_plan_merchant_id]
	ON [dbo].[payment_plans] ([merchant_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[payment_plans] SET (LOCK_ESCALATION = TABLE)
GO
