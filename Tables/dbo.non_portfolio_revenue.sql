SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[non_portfolio_revenue] (
		[id]                 [int] IDENTITY(1, 1) NOT NULL,
		[revenue_date]       [date] NULL,
		[revenue_amount]     [money] NULL,
		CONSTRAINT [pk_non_portfolio_revenue_id]
		PRIMARY KEY
		CLUSTERED
		([id] DESC)
)
GO
ALTER TABLE [dbo].[non_portfolio_revenue] SET (LOCK_ESCALATION = TABLE)
GO
