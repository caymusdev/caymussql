SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[funding_isos] (
		[funding_iso_id]          [int] IDENTITY(1, 1) NOT NULL,
		[funding_id]              [bigint] NOT NULL,
		[iso_id]                  [bigint] NOT NULL,
		[is_referring]            [bit] NOT NULL,
		[legal_name]              [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[dba_name]                [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[name_on_check]           [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[address_1]               [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[address_2]               [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[city]                    [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[state]                   [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[postal_code]             [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[bank_name]               [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[account_number]          [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[routing_number]          [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[wire_routing_number]     [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[wiring_instructions]     [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]             [datetime] NOT NULL,
		[change_date]             [datetime] NOT NULL,
		[change_user]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_funding_isos_id]
		PRIMARY KEY
		CLUSTERED
		([funding_iso_id])
)
GO
ALTER TABLE [dbo].[funding_isos]
	ADD
	CONSTRAINT [DF__funding_i__creat__6DEC4894]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[funding_isos]
	ADD
	CONSTRAINT [DF__funding_i__chang__6EE06CCD]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[funding_isos]
	WITH CHECK
	ADD CONSTRAINT [fk_funding_iso_funding_id]
	FOREIGN KEY ([funding_id]) REFERENCES [dbo].[fundings] ([id])
ALTER TABLE [dbo].[funding_isos]
	CHECK CONSTRAINT [fk_funding_iso_funding_id]

GO
CREATE NONCLUSTERED INDEX [idx_funding_isos_id]
	ON [dbo].[funding_isos] ([funding_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[funding_isos] SET (LOCK_ESCALATION = TABLE)
GO
