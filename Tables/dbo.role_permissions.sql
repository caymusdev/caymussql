SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[role_permissions] (
		[role_permission_id]     [int] IDENTITY(1, 1) NOT NULL,
		[role_id]                [int] NOT NULL,
		[permission_id]          [int] NOT NULL,
		[create_date]            [datetime] NOT NULL,
		[change_date]            [datetime] NOT NULL,
		[change_user]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[read_only]              [bit] NULL,
		CONSTRAINT [pk_role_permissions_id]
		PRIMARY KEY
		CLUSTERED
		([role_permission_id])
)
GO
ALTER TABLE [dbo].[role_permissions]
	ADD
	CONSTRAINT [DF__role_perm__creat__0C3BC58A]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[role_permissions]
	ADD
	CONSTRAINT [DF__role_perm__chang__0D2FE9C3]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[role_permissions]
	WITH CHECK
	ADD CONSTRAINT [fk_role_permissions_permissions_permission_id]
	FOREIGN KEY ([permission_id]) REFERENCES [dbo].[permissions] ([permission_id])
ALTER TABLE [dbo].[role_permissions]
	CHECK CONSTRAINT [fk_role_permissions_permissions_permission_id]

GO
ALTER TABLE [dbo].[role_permissions]
	WITH CHECK
	ADD CONSTRAINT [fk_role_permissions_roles_role_id]
	FOREIGN KEY ([role_id]) REFERENCES [dbo].[roles] ([role_id])
ALTER TABLE [dbo].[role_permissions]
	CHECK CONSTRAINT [fk_role_permissions_roles_role_id]

GO
ALTER TABLE [dbo].[role_permissions] SET (LOCK_ESCALATION = TABLE)
GO
