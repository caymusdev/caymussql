SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[performance_statuses] (
		[status_id]       [int] NOT NULL,
		[status_text]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [pk_performance_statuses_id]
		PRIMARY KEY
		CLUSTERED
		([status_id])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[performance_statuses] SET (LOCK_ESCALATION = TABLE)
GO
