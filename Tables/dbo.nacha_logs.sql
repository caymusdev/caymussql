SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[nacha_logs] (
		[nacha_log_id]        [int] IDENTITY(1, 1) NOT NULL,
		[batch_id]            [int] NULL,
		[nacha_file_name]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[file_data]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[processed_date]      [datetime] NULL,
		[proc_status]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]         [datetime] NOT NULL,
		[change_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_nacha_log_nacha_log_id]
		PRIMARY KEY
		CLUSTERED
		([nacha_log_id] DESC)
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[nacha_logs]
	ADD
	CONSTRAINT [DF__nacha_log__creat__090A5324]
	DEFAULT (getutcdate()) FOR [create_date]
GO
CREATE NONCLUSTERED INDEX [idx_nacha_log_batch_id]
	ON [dbo].[nacha_logs] ([batch_id] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[nacha_logs] SET (LOCK_ESCALATION = TABLE)
GO
