SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[caymus_phone_numbers] (
		[caymus_phone_number_id]     [int] IDENTITY(1, 1) NOT NULL,
		[area_code]                  [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[phone_number]               [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[state]                      [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]                [datetime] NOT NULL,
		[change_date]                [datetime] NOT NULL,
		[change_user]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[state_abbrev]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_caymus_phone_number_id]
		PRIMARY KEY
		CLUSTERED
		([caymus_phone_number_id])
)
GO
ALTER TABLE [dbo].[caymus_phone_numbers]
	ADD
	CONSTRAINT [DF__caymus_ph__creat__33AA9866]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[caymus_phone_numbers]
	ADD
	CONSTRAINT [DF__caymus_ph__chang__349EBC9F]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[caymus_phone_numbers] SET (LOCK_ESCALATION = TABLE)
GO
