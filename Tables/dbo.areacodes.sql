SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[areacodes] (
		[areacode_id]      [int] IDENTITY(1, 1) NOT NULL,
		[areacode]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[state]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[state_abbrev]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]      [datetime] NOT NULL,
		[change_date]      [datetime] NOT NULL,
		[change_user]      [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]      [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_areacodes_id]
		PRIMARY KEY
		CLUSTERED
		([areacode_id])
)
GO
ALTER TABLE [dbo].[areacodes]
	ADD
	CONSTRAINT [DF__areacodes__creat__3F1C4B12]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[areacodes]
	ADD
	CONSTRAINT [DF__areacodes__chang__40106F4B]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[areacodes] SET (LOCK_ESCALATION = TABLE)
GO
