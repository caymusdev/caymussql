SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[documents] (
		[documents_id]      [int] IDENTITY(1, 1) NOT NULL,
		[affiliate_id]      [bigint] NULL,
		[merchant_id]       [bigint] NULL,
		[contract_id]       [bigint] NULL,
		[document_name]     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[document_type]     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[document_url]      [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[change_date]       [datetime] NOT NULL,
		[change_user]       [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]       [datetime] NOT NULL,
		[create_user]       [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_documents_documents_id]
		PRIMARY KEY
		CLUSTERED
		([documents_id])
)
GO
ALTER TABLE [dbo].[documents]
	ADD
	CONSTRAINT [DF__documents__chang__0CC5D56F]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[documents]
	ADD
	CONSTRAINT [DF__documents__creat__0DB9F9A8]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[documents]
	WITH CHECK
	ADD CONSTRAINT [fk_documents_affiliate_id]
	FOREIGN KEY ([affiliate_id]) REFERENCES [dbo].[affiliates] ([affiliate_id])
ALTER TABLE [dbo].[documents]
	CHECK CONSTRAINT [fk_documents_affiliate_id]

GO
ALTER TABLE [dbo].[documents]
	WITH CHECK
	ADD CONSTRAINT [fk_documents_merchant_id]
	FOREIGN KEY ([merchant_id]) REFERENCES [dbo].[merchants] ([merchant_id])
ALTER TABLE [dbo].[documents]
	CHECK CONSTRAINT [fk_documents_merchant_id]

GO
ALTER TABLE [dbo].[documents]
	WITH CHECK
	ADD CONSTRAINT [fk_documents_contract_id]
	FOREIGN KEY ([contract_id]) REFERENCES [dbo].[contracts] ([contract_id])
ALTER TABLE [dbo].[documents]
	CHECK CONSTRAINT [fk_documents_contract_id]

GO
ALTER TABLE [dbo].[documents] SET (LOCK_ESCALATION = TABLE)
GO
