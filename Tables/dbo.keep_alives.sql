SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[keep_alives] (
		[keep_alive_id]     [int] IDENTITY(1, 1) NOT NULL,
		[user_email]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]       [datetime] NOT NULL,
		CONSTRAINT [pk_keep_alives_id]
		PRIMARY KEY
		CLUSTERED
		([keep_alive_id] DESC)
)
GO
ALTER TABLE [dbo].[keep_alives]
	ADD
	CONSTRAINT [DF__keep_aliv__creat__4C764630]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[keep_alives] SET (LOCK_ESCALATION = TABLE)
GO
