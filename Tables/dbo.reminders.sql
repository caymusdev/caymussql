SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[reminders] (
		[reminder_id]         [int] IDENTITY(1, 1) NOT NULL,
		[merchant_id]         [bigint] NULL,
		[affiliate_id]        [bigint] NULL,
		[contract_id]         [bigint] NULL,
		[contact_type_id]     [int] NULL,
		[reminder_date]       [datetime] NULL,
		[contact_id]          [int] NULL,
		[user_id]             [int] NULL,
		[notes]               [nvarchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[completed]           [datetime] NULL,
		[create_date]         [datetime] NOT NULL,
		[change_date]         [datetime] NOT NULL,
		[change_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[subject]             [nvarchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_reminders_id]
		PRIMARY KEY
		CLUSTERED
		([reminder_id] DESC)
)
GO
ALTER TABLE [dbo].[reminders]
	ADD
	CONSTRAINT [DF__reminders__creat__75235608]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[reminders]
	ADD
	CONSTRAINT [DF__reminders__chang__76177A41]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[reminders]
	WITH CHECK
	ADD CONSTRAINT [fk_reminders_contacts_contact_id]
	FOREIGN KEY ([contact_id]) REFERENCES [dbo].[contacts] ([contact_id])
ALTER TABLE [dbo].[reminders]
	CHECK CONSTRAINT [fk_reminders_contacts_contact_id]

GO
ALTER TABLE [dbo].[reminders]
	WITH CHECK
	ADD CONSTRAINT [fk_reminders_merchants]
	FOREIGN KEY ([merchant_id]) REFERENCES [dbo].[merchants] ([merchant_id])
ALTER TABLE [dbo].[reminders]
	CHECK CONSTRAINT [fk_reminders_merchants]

GO
ALTER TABLE [dbo].[reminders]
	WITH CHECK
	ADD CONSTRAINT [fk_reminders_affiliates]
	FOREIGN KEY ([affiliate_id]) REFERENCES [dbo].[affiliates] ([affiliate_id])
ALTER TABLE [dbo].[reminders]
	CHECK CONSTRAINT [fk_reminders_affiliates]

GO
ALTER TABLE [dbo].[reminders]
	WITH CHECK
	ADD CONSTRAINT [fk_reminders_contact_types]
	FOREIGN KEY ([contact_type_id]) REFERENCES [dbo].[contact_types] ([contact_type_id])
ALTER TABLE [dbo].[reminders]
	CHECK CONSTRAINT [fk_reminders_contact_types]

GO
ALTER TABLE [dbo].[reminders]
	WITH CHECK
	ADD CONSTRAINT [fk_reminders_users]
	FOREIGN KEY ([user_id]) REFERENCES [dbo].[users] ([user_id])
ALTER TABLE [dbo].[reminders]
	CHECK CONSTRAINT [fk_reminders_users]

GO
ALTER TABLE [dbo].[reminders] SET (LOCK_ESCALATION = TABLE)
GO
