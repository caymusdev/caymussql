SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[payments] (
		[payment_id]                  [int] IDENTITY(1, 1) NOT NULL,
		[payment_schedule_id]         [int] NULL,
		[amount]                      [money] NOT NULL,
		[trans_date]                  [date] NULL,
		[transaction_id]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[approved_flag]               [bit] NULL,
		[processed_date]              [datetime] NULL,
		[change_date]                 [datetime] NULL,
		[change_user]                 [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[previous_transaction_id]     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[orig_transaction_id]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[is_fee]                      [bit] NULL,
		[retry_level]                 [int] NULL,
		[retry_sublevel]              [int] NULL,
		[funding_id]                  [bigint] NULL,
		[complete]                    [bit] NULL,
		[batch_id]                    [int] NULL,
		[retry_complete]              [bit] NULL,
		[processor]                   [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[is_refund]                   [bit] NULL,
		[is_debit]                    [bit] NULL,
		[deleted]                     [bit] NULL,
		[waived]                      [bit] NULL,
		[redirect_funding_id]         [int] NULL,
		[redirect_approval_id]        [int] NULL,
		[funding_payment_date_id]     [int] NULL,
		[create_date]                 [datetime] NULL,
		[iso_id]                      [int] NULL,
		[iso_bonus_id]                [int] NULL,
		[portfolio]                   [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [payments_id]
		PRIMARY KEY
		CLUSTERED
		([payment_id])
)
GO
ALTER TABLE [dbo].[payments]
	ADD
	CONSTRAINT [DF__payments__is_ref__184C96B4]
	DEFAULT ((0)) FOR [is_refund]
GO
ALTER TABLE [dbo].[payments]
	ADD
	CONSTRAINT [DF__payments__delete__1940BAED]
	DEFAULT ((0)) FOR [deleted]
GO
ALTER TABLE [dbo].[payments]
	ADD
	CONSTRAINT [DF__payments__waived__1A34DF26]
	DEFAULT ((0)) FOR [waived]
GO
ALTER TABLE [dbo].[payments]
	ADD
	CONSTRAINT [DF__payments__is_deb__1B29035F]
	DEFAULT ((1)) FOR [is_debit]
GO
ALTER TABLE [dbo].[payments]
	ADD
	CONSTRAINT [DF__payments__create__1C1D2798]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[payments]
	WITH CHECK
	ADD CONSTRAINT [fk_payments_payment_schedule]
	FOREIGN KEY ([payment_schedule_id]) REFERENCES [dbo].[payment_schedules] ([payment_schedule_id])
ALTER TABLE [dbo].[payments]
	CHECK CONSTRAINT [fk_payments_payment_schedule]

GO
CREATE NONCLUSTERED INDEX [idx_payments_orig_trans_id]
	ON [dbo].[payments] ([orig_transaction_id])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_payments_prev_trans_id]
	ON [dbo].[payments] ([previous_transaction_id])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_payments_transaction_id]
	ON [dbo].[payments] ([transaction_id])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[payments] SET (LOCK_ESCALATION = TABLE)
GO
