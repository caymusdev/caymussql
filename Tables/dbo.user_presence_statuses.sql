SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user_presence_statuses] (
		[user_presence_status_id]     [int] IDENTITY(1, 1) NOT NULL,
		[status_name]                 [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[status_description]          [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]                 [datetime] NOT NULL,
		[change_date]                 [datetime] NOT NULL,
		[change_user]                 [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]                 [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_user_presence_statuses_id]
		PRIMARY KEY
		CLUSTERED
		([user_presence_status_id])
)
GO
ALTER TABLE [dbo].[user_presence_statuses]
	ADD
	CONSTRAINT [DF__user_pres__creat__08C03A61]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[user_presence_statuses]
	ADD
	CONSTRAINT [DF__user_pres__chang__09B45E9A]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[user_presence_statuses]
	ADD
	CONSTRAINT [DF__user_pres__chang__0AA882D3]
	DEFAULT ('SYSTEM') FOR [change_user]
GO
ALTER TABLE [dbo].[user_presence_statuses]
	ADD
	CONSTRAINT [DF__user_pres__creat__0B9CA70C]
	DEFAULT ('SYSTEM') FOR [create_user]
GO
ALTER TABLE [dbo].[user_presence_statuses] SET (LOCK_ESCALATION = TABLE)
GO
