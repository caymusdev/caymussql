SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[funding_distributions] (
		[id]                     [int] IDENTITY(1, 1) NOT NULL,
		[funding_id]             [bigint] NOT NULL,
		[recipient]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[address_1]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[address_2]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[city]                   [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[state_code]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[zip_code]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[amount]                 [money] NULL,
		[bank_name]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[account_number]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[routing_number]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[comments]               [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[wire_date]              [datetime] NULL,
		[discounted_balance]     [money] NULL,
		[create_date]            [datetime] NULL,
		[change_date]            [datetime] NULL,
		[change_user]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[payoff]                 [bit] NOT NULL,
		CONSTRAINT [funding_distributions_id]
		PRIMARY KEY
		CLUSTERED
		([id])
)
GO
ALTER TABLE [dbo].[funding_distributions]
	ADD
	CONSTRAINT [DF__funding_d__payof__0781FD65]
	DEFAULT ((1)) FOR [payoff]
GO
ALTER TABLE [dbo].[funding_distributions]
	ADD
	CONSTRAINT [DF__funding_d__creat__740F363E]
	DEFAULT (getutcdate()) FOR [create_date]
GO
CREATE NONCLUSTERED INDEX [idx_funding_dist_funding_id]
	ON [dbo].[funding_distributions] ([funding_id])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[funding_distributions] SET (LOCK_ESCALATION = TABLE)
GO
