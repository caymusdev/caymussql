SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[phone_calls] (
		[phone_call_id]       [int] IDENTITY(1, 1) NOT NULL,
		[user_id]             [int] NULL,
		[call_to]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[call_from]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[affiliate_id]        [bigint] NULL,
		[merchant_id]         [bigint] NULL,
		[contract_id]         [bigint] NULL,
		[call_duration]       [int] NULL,
		[start_time]          [datetime] NULL,
		[end_time]            [datetime] NULL,
		[disposition]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]         [datetime] NOT NULL,
		[change_date]         [datetime] NOT NULL,
		[change_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[incoming_call]       [bit] NULL,
		[call_id]             [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[conversation_id]     [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_phone_calls_id]
		PRIMARY KEY
		CLUSTERED
		([phone_call_id] DESC)
)
GO
ALTER TABLE [dbo].[phone_calls]
	ADD
	CONSTRAINT [DF__phone_cal__creat__3508D0F3]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[phone_calls]
	ADD
	CONSTRAINT [DF__phone_cal__chang__35FCF52C]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[phone_calls]
	WITH CHECK
	ADD CONSTRAINT [fk_phone_calls_affiliates]
	FOREIGN KEY ([affiliate_id]) REFERENCES [dbo].[affiliates] ([affiliate_id])
ALTER TABLE [dbo].[phone_calls]
	CHECK CONSTRAINT [fk_phone_calls_affiliates]

GO
ALTER TABLE [dbo].[phone_calls]
	WITH CHECK
	ADD CONSTRAINT [fk_phone_calls_contracts]
	FOREIGN KEY ([contract_id]) REFERENCES [dbo].[contracts] ([contract_id])
ALTER TABLE [dbo].[phone_calls]
	CHECK CONSTRAINT [fk_phone_calls_contracts]

GO
ALTER TABLE [dbo].[phone_calls]
	WITH CHECK
	ADD CONSTRAINT [fk_phone_calls_merchants]
	FOREIGN KEY ([merchant_id]) REFERENCES [dbo].[merchants] ([merchant_id])
ALTER TABLE [dbo].[phone_calls]
	CHECK CONSTRAINT [fk_phone_calls_merchants]

GO
ALTER TABLE [dbo].[phone_calls]
	WITH CHECK
	ADD CONSTRAINT [fk_phone_calls_users]
	FOREIGN KEY ([user_id]) REFERENCES [dbo].[users] ([user_id])
ALTER TABLE [dbo].[phone_calls]
	CHECK CONSTRAINT [fk_phone_calls_users]

GO
CREATE NONCLUSTERED INDEX [idx_phone_calls_to]
	ON [dbo].[phone_calls] ([call_to] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_phone_calls_from]
	ON [dbo].[phone_calls] ([call_from] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[phone_calls] SET (LOCK_ESCALATION = TABLE)
GO
