SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[portfolio_eligibility_audit] (
		[audit_id]                     [int] IDENTITY(1, 1) NOT NULL,
		[portfolio_eligibility_id]     [int] NULL,
		[change_date]                  [datetime] NOT NULL,
		[change_user]                  [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_category]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[field]                        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[previous_value]               [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[changed_to_value]             [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_portfolio_eligibility_audit_id]
		PRIMARY KEY
		CLUSTERED
		([audit_id])
	WITH FILLFACTOR=80
)
GO
ALTER TABLE [dbo].[portfolio_eligibility_audit]
	ADD
	CONSTRAINT [DF__portfolio__chang__037C6257]
	DEFAULT (getutcdate()) FOR [change_date]
GO
CREATE NONCLUSTERED INDEX [idx_portfolio_eligibility_audit_change_date]
	ON [dbo].[portfolio_eligibility_audit] ([change_date] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[portfolio_eligibility_audit] SET (LOCK_ESCALATION = TABLE)
GO
