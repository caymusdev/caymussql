SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[forte_trans] (
		[forte_trans_id]           [int] IDENTITY(1, 1) NOT NULL,
		[customer_token]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[transaction_id]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[customer_id]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[status]                   [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[action]                   [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[authorization_amount]     [money] NOT NULL,
		[authorization_code]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[entered_by]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[received_date]            [datetime] NULL,
		[origination_date]         [datetime] NULL,
		[company_name]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[response_code]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]              [datetime] NULL,
		[change_date]              [datetime] NULL,
		[change_user]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[batch_id]                 [int] NULL,
		[processed_date]           [datetime] NULL,
		CONSTRAINT [pk_forte_trans_id]
		PRIMARY KEY
		CLUSTERED
		([forte_trans_id] DESC)
)
GO
ALTER TABLE [dbo].[forte_trans]
	ADD
	CONSTRAINT [DF__forte_tra__creat__703EA55A]
	DEFAULT (getutcdate()) FOR [create_date]
GO
CREATE NONCLUSTERED INDEX [IX_forte_trans_cust_id_orig_date]
	ON [dbo].[forte_trans] ([customer_id] DESC, [origination_date] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_forte_trans_trans_id]
	ON [dbo].[forte_trans] ([transaction_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_forte_trans_trans_id_status]
	ON [dbo].[forte_trans] ([transaction_id] DESC, [status])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[forte_trans] SET (LOCK_ESCALATION = TABLE)
GO
