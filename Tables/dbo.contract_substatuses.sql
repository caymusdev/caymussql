SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[contract_substatuses] (
		[substatus_id]           [int] IDENTITY(1, 1) NOT NULL,
		[substatus_text]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[contract_status_id]     [int] NOT NULL,
		[substatus_desc]         [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]            [datetime] NULL,
		CONSTRAINT [pk_contract_substatuses_id]
		PRIMARY KEY
		CLUSTERED
		([substatus_id])
)
GO
ALTER TABLE [dbo].[contract_substatuses]
	ADD
	CONSTRAINT [DF__contract___creat__64CCF2AE]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[contract_substatuses]
	WITH CHECK
	ADD CONSTRAINT [fk_contract_substatuses_contract_statues]
	FOREIGN KEY ([contract_status_id]) REFERENCES [dbo].[contract_statuses] ([contract_status_id])
ALTER TABLE [dbo].[contract_substatuses]
	CHECK CONSTRAINT [fk_contract_substatuses_contract_statues]

GO
ALTER TABLE [dbo].[contract_substatuses] SET (LOCK_ESCALATION = TABLE)
GO
