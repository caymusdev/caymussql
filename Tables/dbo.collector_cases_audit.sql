SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[collector_cases_audit] (
		[audit_id]              [int] IDENTITY(1, 1) NOT NULL,
		[collector_case_id]     [int] NULL,
		[change_date]           [datetime] NOT NULL,
		[change_user]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_category]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[field]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[previous_value]        [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[changed_to_value]      [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_collector_cases_audit_id]
		PRIMARY KEY
		CLUSTERED
		([audit_id] DESC)
)
GO
ALTER TABLE [dbo].[collector_cases_audit]
	ADD
	CONSTRAINT [DF__collector__chang__13A7DD28]
	DEFAULT (getutcdate()) FOR [change_date]
GO
CREATE NONCLUSTERED INDEX [idx_collector_cases_audit_change_date]
	ON [dbo].[collector_cases_audit] ([change_date] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[collector_cases_audit] SET (LOCK_ESCALATION = TABLE)
GO
