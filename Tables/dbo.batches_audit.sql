SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[batches_audit] (
		[audit_id]             [int] IDENTITY(1, 1) NOT NULL,
		[batch_id]             [int] NULL,
		[batch_type]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_date]          [datetime] NOT NULL,
		[change_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_category]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[field]                [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[previous_value]       [nvarchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[changed_to_value]     [nvarchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_batches_audit_id]
		PRIMARY KEY
		CLUSTERED
		([audit_id])
)
GO
ALTER TABLE [dbo].[batches_audit]
	ADD
	CONSTRAINT [DF__batches_a__chang__5D2BD0E6]
	DEFAULT (getutcdate()) FOR [change_date]
GO
CREATE NONCLUSTERED INDEX [idx_batches_audit_change_date]
	ON [dbo].[batches_audit] ([change_date] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[batches_audit] SET (LOCK_ESCALATION = TABLE)
GO
