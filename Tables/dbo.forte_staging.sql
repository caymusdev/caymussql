SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[forte_staging] (
		[forte_staging_id]          [int] IDENTITY(1, 1) NOT NULL,
		[BillTo Company Name]       [nvarchar](55) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Total Amount]              [money] NULL,
		[Created Date]              [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Origination Date]          [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Settle Date]               [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Response Description]      [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Transaction ID]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Status]                    [nvarchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Settled Response Code]     [nvarchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Consumer ID]               [nvarchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Merchant Data 9]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BillTo FirstName]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Entered By]                [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDate]               [datetime] NULL,
		[BatchID]                   [int] NULL,
		CONSTRAINT [pkforte_staging_id]
		PRIMARY KEY
		CLUSTERED
		([forte_staging_id])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[forte_staging]
	ADD
	CONSTRAINT [DF_fortestaging_createdDate]
	DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[forte_staging] SET (LOCK_ESCALATION = TABLE)
GO
