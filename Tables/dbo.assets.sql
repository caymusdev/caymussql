SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[assets] (
		[asset_id]              [int] IDENTITY(1, 1) NOT NULL,
		[asset_types_id]        [int] NULL,
		[affiliate_id]          [bigint] NULL,
		[merchant_id]           [bigint] NULL,
		[contract_id]           [bigint] NULL,
		[asset_name]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[asset_price]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[asset_description]     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[asset_owner]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[change_date]           [datetime] NOT NULL,
		[change_user]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]           [datetime] NOT NULL,
		[create_user]           [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[asset_located]         [bit] NULL,
		CONSTRAINT [pk_assets_asset_id]
		PRIMARY KEY
		CLUSTERED
		([asset_id])
)
GO
ALTER TABLE [dbo].[assets]
	ADD
	CONSTRAINT [DF__assets__change_d__015422C3]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[assets]
	ADD
	CONSTRAINT [DF__assets__create_d__024846FC]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[assets]
	WITH CHECK
	ADD CONSTRAINT [fk_assets_asset_types_id]
	FOREIGN KEY ([asset_types_id]) REFERENCES [dbo].[asset_types] ([asset_types_id])
ALTER TABLE [dbo].[assets]
	CHECK CONSTRAINT [fk_assets_asset_types_id]

GO
ALTER TABLE [dbo].[assets]
	WITH CHECK
	ADD CONSTRAINT [fk_assets_affiliate_id]
	FOREIGN KEY ([affiliate_id]) REFERENCES [dbo].[affiliates] ([affiliate_id])
ALTER TABLE [dbo].[assets]
	CHECK CONSTRAINT [fk_assets_affiliate_id]

GO
ALTER TABLE [dbo].[assets]
	WITH CHECK
	ADD CONSTRAINT [fk_assets_merchant_id]
	FOREIGN KEY ([merchant_id]) REFERENCES [dbo].[merchants] ([merchant_id])
ALTER TABLE [dbo].[assets]
	CHECK CONSTRAINT [fk_assets_merchant_id]

GO
ALTER TABLE [dbo].[assets]
	WITH CHECK
	ADD CONSTRAINT [fk_assets_contract_id]
	FOREIGN KEY ([contract_id]) REFERENCES [dbo].[contracts] ([contract_id])
ALTER TABLE [dbo].[assets]
	CHECK CONSTRAINT [fk_assets_contract_id]

GO
ALTER TABLE [dbo].[assets] SET (LOCK_ESCALATION = TABLE)
GO
