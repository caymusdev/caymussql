SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[permissions] (
		[permission_id]       [int] IDENTITY(1, 1) NOT NULL,
		[permission_name]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[permission_desc]     [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]         [datetime] NOT NULL,
		[change_date]         [datetime] NOT NULL,
		[change_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[permission_type]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[is_page]             [bit] NULL,
		[is_menu_item]        [bit] NULL,
		CONSTRAINT [pk_permission_id]
		PRIMARY KEY
		CLUSTERED
		([permission_id])
)
GO
ALTER TABLE [dbo].[permissions]
	ADD
	CONSTRAINT [DF__permissio__creat__59B045BD]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[permissions]
	ADD
	CONSTRAINT [DF__permissio__chang__5AA469F6]
	DEFAULT (getutcdate()) FOR [change_date]
GO
CREATE NONCLUSTERED INDEX [idx_permissions_permission_name]
	ON [dbo].[permissions] ([permission_name])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[permissions] SET (LOCK_ESCALATION = TABLE)
GO
