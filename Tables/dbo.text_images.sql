SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[text_images] (
		[text_image_id]     [int] IDENTITY(1, 1) NOT NULL,
		[text_id]           [int] NOT NULL,
		[image_url]         [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]       [datetime] NOT NULL,
		[change_date]       [datetime] NOT NULL,
		[change_user]       [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]       [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_text_image_id]
		PRIMARY KEY
		CLUSTERED
		([text_image_id])
)
GO
ALTER TABLE [dbo].[text_images]
	ADD
	CONSTRAINT [DF__text_imag__creat__4F52B2DB]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[text_images]
	ADD
	CONSTRAINT [DF__text_imag__chang__5046D714]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[text_images]
	WITH CHECK
	ADD CONSTRAINT [fk_text_images_text_id]
	FOREIGN KEY ([text_id]) REFERENCES [dbo].[texts] ([text_id])
ALTER TABLE [dbo].[text_images]
	CHECK CONSTRAINT [fk_text_images_text_id]

GO
ALTER TABLE [dbo].[text_images] SET (LOCK_ESCALATION = TABLE)
GO
