SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[payment_schedule_payments] (
		[id]                      [int] IDENTITY(1, 1) NOT NULL,
		[payment_schedule_id]     [int] NULL,
		[payment_date]            [datetime] NULL,
		[expected_amount]         [money] NULL,
		[rec_amount]              [money] NULL,
		[payment_id]              [int] NULL,
		[trans_id]                [int] NULL,
		[create_date]             [datetime] NULL,
		[chargeback_amount]       [money] NULL,
		[chargeback_date]         [datetime] NULL,
		[change_date]             [datetime] NULL,
		[change_user]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_ps_sched_pay_id]
		PRIMARY KEY
		CLUSTERED
		([id])
)
GO
ALTER TABLE [dbo].[payment_schedule_payments]
	ADD
	CONSTRAINT [DF__payment_s__creat__1387E197]
	DEFAULT (getutcdate()) FOR [create_date]
GO
CREATE NONCLUSTERED INDEX [idx_ps_payments_rec_amount]
	ON [dbo].[payment_schedule_payments] ([rec_amount])
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_ps_sched_pay_payment_id]
	ON [dbo].[payment_schedule_payments] ([payment_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_ps_sched_pay_ps_id]
	ON [dbo].[payment_schedule_payments] ([payment_schedule_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_ps_sched_pay_trans_id]
	ON [dbo].[payment_schedule_payments] ([trans_id] DESC)
	ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_psp_ps_id_date]
	ON [dbo].[payment_schedule_payments] ([payment_schedule_id] DESC, [payment_date] DESC)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[payment_schedule_payments] SET (LOCK_ESCALATION = TABLE)
GO
