SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[activity_log] (
		[activity_log_id]      [int] IDENTITY(1, 1) NOT NULL,
		[activity_type_id]     [int] NOT NULL,
		[user_id]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[comments]             [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[affiliate_id]         [bigint] NULL,
		[create_date]          [datetime] NOT NULL,
		[change_date]          [datetime] NOT NULL,
		[change_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_activity_log_id]
		PRIMARY KEY
		CLUSTERED
		([activity_log_id])
)
GO
ALTER TABLE [dbo].[activity_log]
	ADD
	CONSTRAINT [DF__activity___creat__68F2894D]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[activity_log]
	ADD
	CONSTRAINT [DF__activity___chang__69E6AD86]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[activity_log]
	WITH CHECK
	ADD CONSTRAINT [fk_activity_log_activity_type]
	FOREIGN KEY ([activity_type_id]) REFERENCES [dbo].[activity_types] ([activity_type_id])
ALTER TABLE [dbo].[activity_log]
	CHECK CONSTRAINT [fk_activity_log_activity_type]

GO
CREATE NONCLUSTERED INDEX [idx_activity_log_type_id]
	ON [dbo].[activity_log] ([activity_type_id])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[activity_log] SET (LOCK_ESCALATION = TABLE)
GO
