SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[funding_eligibility] (
		[funding_eligibility_id]         [int] IDENTITY(1, 1) NOT NULL,
		[funding_id]                     [bigint] NULL,
		[application_id]                 [bigint] NULL,
		[ineligible_date]                [date] NULL,
		[estimated_turn]                 [numeric](10, 2) NULL,
		[estimated_turn_max]             [numeric](10, 2) NULL,
		[estimated_turn_eligible]        [bit] NULL,
		[purchase_price]                 [money] NULL,
		[purchase_price_max]             [money] NULL,
		[purchase_price_eligible]        [bit] NULL,
		[first_position]                 [bit] NULL,
		[first_position_requirement]     [bit] NULL,
		[first_position_eligible]        [bit] NULL,
		[pricing_ratio]                  [numeric](10, 2) NULL,
		[pricing_ratio_min]              [numeric](10, 2) NULL,
		[pricing_ratio_eligible]         [bit] NULL,
		[frequency]                      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[frequency_requirement]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[frequency_eligible]             [bit] NULL,
		[first_payment]                  [datetime] NULL,
		[first_payment_requirement]      [datetime] NULL,
		[first_payment_eligible]         [bit] NULL,
		[last_paymemt]                   [datetime] NULL,
		[last_paymemt_requirement]       [datetime] NULL,
		[last_paymemt_eligible]          [bit] NULL,
		[collections]                    [bit] NULL,
		[collections_requirement]        [bit] NULL,
		[collections_eligible]           [bit] NULL,
		[percent_collected]              [numeric](10, 2) NULL,
		[percent_collected_minimum]      [numeric](10, 2) NULL,
		[percent_collected_eligible]     [bit] NULL,
		[create_date]                    [datetime] NOT NULL,
		[change_date]                    [datetime] NOT NULL,
		[change_user]                    [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]                    [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [pk_funding_eligibility_id]
		PRIMARY KEY
		CLUSTERED
		([funding_eligibility_id])
	WITH FILLFACTOR=80
)
GO
ALTER TABLE [dbo].[funding_eligibility]
	ADD
	CONSTRAINT [DF__funding_e__creat__77168B72]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[funding_eligibility]
	ADD
	CONSTRAINT [DF__funding_e__chang__780AAFAB]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[funding_eligibility]
	WITH CHECK
	ADD CONSTRAINT [fk_funding_eligibility_fundingss]
	FOREIGN KEY ([funding_id]) REFERENCES [dbo].[fundings] ([id])
ALTER TABLE [dbo].[funding_eligibility]
	CHECK CONSTRAINT [fk_funding_eligibility_fundingss]

GO
ALTER TABLE [dbo].[funding_eligibility] SET (LOCK_ESCALATION = TABLE)
GO
