SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cases] (
		[case_id]                              [int] IDENTITY(1, 1) NOT NULL,
		[affiliate_id]                         [bigint] NULL,
		[last_contact]                         [datetime] NULL,
		[last_attempted_contact]               [datetime] NULL,
		[last_contact_type]                    [int] NULL,
		[last_attempted_contact_type]          [int] NULL,
		[last_contact_status_id]               [int] NULL,
		[last_attempted_contact_status_id]     [int] NULL,
		[next_follow_up_date]                  [datetime] NULL,
		[initial_gross_due]                    [money] NULL,
		[collection_age]                       [int] NULL,
		[attempted_calls]                      [int] NULL,
		[contacts_made]                        [int] NULL,
		[create_date]                          [datetime] NOT NULL,
		[change_date]                          [datetime] NOT NULL,
		[change_user]                          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_user]                          [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[active]                               [bit] NOT NULL,
		[payment_plan_id]                      [int] NULL,
		[new_case]                             [bit] NULL,
		[missed_payment]                       [bit] NULL,
		[new_communication]                    [bit] NULL,
		[collection_date]                      [datetime] NULL,
		[days_assigned_to_collector]           [int] NULL,
		[case_status_id]                       [int] NULL,
		[department_id]                        [int] NULL,
		[bucket_status_id]                     [int] NULL,
		[last_payment_amount]                  [money] NULL,
		[last_payment_date]                    [datetime] NULL,
		CONSTRAINT [pk_cases_id]
		PRIMARY KEY
		CLUSTERED
		([case_id] DESC)
)
GO
ALTER TABLE [dbo].[cases]
	ADD
	CONSTRAINT [DF__cases__create_da__26EFBBC6]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[cases]
	ADD
	CONSTRAINT [DF__cases__change_da__27E3DFFF]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[cases]
	ADD
	CONSTRAINT [DF__cases__active__607D3EDD]
	DEFAULT ((0)) FOR [active]
GO
ALTER TABLE [dbo].[cases]
	WITH CHECK
	ADD CONSTRAINT [fk_cases_affiliates_affiliate_id]
	FOREIGN KEY ([affiliate_id]) REFERENCES [dbo].[affiliates] ([affiliate_id])
ALTER TABLE [dbo].[cases]
	CHECK CONSTRAINT [fk_cases_affiliates_affiliate_id]

GO
ALTER TABLE [dbo].[cases]
	WITH CHECK
	ADD CONSTRAINT [fk_cases_last_contact_type]
	FOREIGN KEY ([last_contact_type]) REFERENCES [dbo].[contact_types] ([contact_type_id])
ALTER TABLE [dbo].[cases]
	CHECK CONSTRAINT [fk_cases_last_contact_type]

GO
ALTER TABLE [dbo].[cases]
	WITH CHECK
	ADD CONSTRAINT [fk_cases_attempted_contact_type]
	FOREIGN KEY ([last_attempted_contact_type]) REFERENCES [dbo].[contact_types] ([contact_type_id])
ALTER TABLE [dbo].[cases]
	CHECK CONSTRAINT [fk_cases_attempted_contact_type]

GO
ALTER TABLE [dbo].[cases]
	WITH CHECK
	ADD CONSTRAINT [fk_cases_last_contact_status]
	FOREIGN KEY ([last_contact_status_id]) REFERENCES [dbo].[contact_statuses] ([contact_status_id])
ALTER TABLE [dbo].[cases]
	CHECK CONSTRAINT [fk_cases_last_contact_status]

GO
ALTER TABLE [dbo].[cases]
	WITH CHECK
	ADD CONSTRAINT [fk_cases_attempted_contact_status]
	FOREIGN KEY ([last_attempted_contact_status_id]) REFERENCES [dbo].[contact_statuses] ([contact_status_id])
ALTER TABLE [dbo].[cases]
	CHECK CONSTRAINT [fk_cases_attempted_contact_status]

GO
ALTER TABLE [dbo].[cases]
	WITH CHECK
	ADD CONSTRAINT [fk_payment_plans_cases]
	FOREIGN KEY ([payment_plan_id]) REFERENCES [dbo].[payment_plans] ([payment_plan_id])
ALTER TABLE [dbo].[cases]
	CHECK CONSTRAINT [fk_payment_plans_cases]

GO
ALTER TABLE [dbo].[cases]
	WITH CHECK
	ADD CONSTRAINT [fk_cases_status]
	FOREIGN KEY ([case_status_id]) REFERENCES [dbo].[case_statuses] ([case_status_id])
ALTER TABLE [dbo].[cases]
	CHECK CONSTRAINT [fk_cases_status]

GO
ALTER TABLE [dbo].[cases]
	WITH CHECK
	ADD CONSTRAINT [fk_cases_department]
	FOREIGN KEY ([department_id]) REFERENCES [dbo].[departments] ([department_id])
ALTER TABLE [dbo].[cases]
	CHECK CONSTRAINT [fk_cases_department]

GO
ALTER TABLE [dbo].[cases]
	WITH CHECK
	ADD CONSTRAINT [fk_cases_bucketstatus]
	FOREIGN KEY ([bucket_status_id]) REFERENCES [dbo].[bucket_statuses] ([bucket_status_id])
ALTER TABLE [dbo].[cases]
	CHECK CONSTRAINT [fk_cases_bucketstatus]

GO
ALTER TABLE [dbo].[cases] SET (LOCK_ESCALATION = TABLE)
GO
