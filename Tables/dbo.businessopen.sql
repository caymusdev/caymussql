SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[businessopen] (
		[businessopen_id]     [int] IDENTITY(1, 1) NOT NULL,
		[affiliate_id]        [bigint] NULL,
		[merchant_id]         [bigint] NULL,
		[contract_id]         [bigint] NULL,
		[open_closed]         [bit] NOT NULL,
		[contact_type]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[change_date]         [datetime] NOT NULL,
		[change_user]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[create_date]         [datetime] NOT NULL,
		[create_user]         [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[verified_date]       [date] NULL,
		CONSTRAINT [pk_businessopen_businessopen_id]
		PRIMARY KEY
		CLUSTERED
		([businessopen_id])
)
GO
ALTER TABLE [dbo].[businessopen]
	ADD
	CONSTRAINT [DF__businesso__chang__74B941B4]
	DEFAULT (getutcdate()) FOR [change_date]
GO
ALTER TABLE [dbo].[businessopen]
	ADD
	CONSTRAINT [DF__businesso__creat__75AD65ED]
	DEFAULT (getutcdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[businessopen]
	WITH CHECK
	ADD CONSTRAINT [fk_businessopen_affiliate_id]
	FOREIGN KEY ([affiliate_id]) REFERENCES [dbo].[affiliates] ([affiliate_id])
ALTER TABLE [dbo].[businessopen]
	CHECK CONSTRAINT [fk_businessopen_affiliate_id]

GO
ALTER TABLE [dbo].[businessopen]
	WITH CHECK
	ADD CONSTRAINT [fk_businessopen_merchant_id]
	FOREIGN KEY ([merchant_id]) REFERENCES [dbo].[merchants] ([merchant_id])
ALTER TABLE [dbo].[businessopen]
	CHECK CONSTRAINT [fk_businessopen_merchant_id]

GO
ALTER TABLE [dbo].[businessopen]
	WITH CHECK
	ADD CONSTRAINT [fk_businessopen_contract_id]
	FOREIGN KEY ([contract_id]) REFERENCES [dbo].[contracts] ([contract_id])
ALTER TABLE [dbo].[businessopen]
	CHECK CONSTRAINT [fk_businessopen_contract_id]

GO
ALTER TABLE [dbo].[businessopen] SET (LOCK_ESCALATION = TABLE)
GO
