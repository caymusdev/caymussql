SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-12-07
-- Description:	Contains the logic for retrying payments
-- =============================================
CREATE PROCEDURE [dbo].[CSP_CreateRetryPayments]
(
	@transaction_id			NVARCHAR (100), 
	@funding_id				BIGINT, 
	@payment_amount			MONEY
)
AS
BEGIN
SET NOCOUNT ON;

-- UPDATE - RKB: 2019-01-15 -If payment schedule has changed dollar amount recently, we don't want to have retries created.  I think just checking payment_schedule.active will work

DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
BEGIN TRANSACTION

-- given a transaction id, check to see if we are already on a retry level
DECLARE @retry_level INT, @trans_amount MONEY, @p1 MONEY, @p2 MONEY, @previous_trans_id NVARCHAR (100),
	@orig_transaction_id NVARCHAR (100), @is_fee BIT, @retry_sublevel INT, @orig_trans_amount MONEY, @on_hold BIT, @funding_payment_date_id INT, @active BIT, @do_retries BIT
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SELECT @tomorrow = DATEADD(DD, CASE DATEPART(WEEKDAY, @today) WHEN 6 THEN 3 WHEN 7 THEN 2 ELSE 1 END, @today)

SELECT @on_hold = COALESCE(f.on_hold, 0), @do_retries = COALESCE(f.do_retries, 1) FROM fundings f WHERE f.id = @funding_id
-- if the funding is on hold, we won't create any new payments or if it has do_retries set to false
IF @on_hold <> 1 AND @do_retries = 1
	BEGIN
		SELECT @retry_level = p.retry_level, @trans_amount = p.amount, @previous_trans_id = p.previous_transaction_id, 
			@orig_transaction_id = p.orig_transaction_id, @is_fee = p.is_fee, @retry_sublevel = p.retry_sublevel, @funding_payment_date_id = p.funding_payment_date_id
		FROM payments p 
		WHERE p.transaction_id = @transaction_id

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR

-- 		SELECT @active = active FROM payment_schedules WHERE payment_schedule_id = @payment_schedule_id
		SELECT @active = active FROM funding_payment_dates WHERE id = @funding_payment_date_id
		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR
		
		IF @trans_amount IS NULL SET @trans_amount = @payment_amount
		IF @orig_transaction_id IS NULL SET @orig_transaction_id = @transaction_id
		IF @is_fee IS NULL SET @is_fee = 0

		SET @retry_level = COALESCE(@retry_level, 0); SET @retry_sublevel = COALESCE(@retry_sublevel, 0)

		IF @is_fee = 0 AND COALESCE(@active, 1) = 1  -- only create the retry if the payment schedule is still active.  Assumption is if they changed the schedule, we have new numbers
													-- and don't want to retry previously failed ones.
			BEGIN
				IF @retry_level = 0
					BEGIN
						-- insert 2 payments and set retry_level @retry_level + 1
						-- 60% and 40%
						SET @p1 = ROUND(0.60 * @trans_amount, 2)
						SET @p2 = @trans_amount - @p1

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
						IF @errorNum != 0 GOTO ERROR
	
						INSERT INTO payments(payment_schedule_id, amount, trans_date, transaction_id, approved_flag, processed_date, change_date, change_user, previous_transaction_id,
							complete, retry_level, is_fee, funding_id, orig_transaction_id, retry_sublevel)
						VALUES (NULL, @p1, @tomorrow, NULL, 1, NULL, GETUTCDATE(), 'SYSTEM', @transaction_id, 0, @retry_level + 1, 0, @funding_id, @orig_transaction_id, 0)

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
						IF @errorNum != 0 GOTO ERROR

						INSERT INTO payments(payment_schedule_id, amount, trans_date, transaction_id, approved_flag, processed_date, change_date, change_user, previous_transaction_id,
							complete, retry_level, is_fee, funding_id, orig_transaction_id, retry_sublevel)
						VALUES (NULL, @p2, @tomorrow, NULL, 1, NULL, GETUTCDATE(), 'SYSTEM', @transaction_id, 0, @retry_level + 1, 0, @funding_id, @orig_transaction_id, 1)

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
						IF @errorNum != 0 GOTO ERROR
					END -- @retry_level = 0
				ELSE IF @retry_level = 1
					BEGIN
						-- insert 2 payments depending on retry_sublevel and set retry_level = @retry_level + 1
						-- 35% and 25% (which is still 60% or original) then 24% and 16% (40% of original)
						SELECT @orig_trans_amount = p.amount FROM payments p WHERE transaction_id = @orig_transaction_id
				
						-- just in case we don't have the original transaction, we don't have to abort everything
						IF @orig_trans_amount IS NOT NULL
							BEGIN
								IF @retry_sublevel = 0
									BEGIN
										SET @p1 = ROUND(0.35 * @orig_trans_amount, 2)
										SET @p2 = @trans_amount - @p1
									END
								ELSE 
									BEGIN
										SET @p1 = ROUND(0.24 * @orig_trans_amount, 2)
										SET @p2 = @trans_amount - @p1
									END

								SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
								IF @errorNum != 0 GOTO ERROR

								INSERT INTO payments(payment_schedule_id, amount, trans_date, transaction_id, approved_flag, processed_date, change_date, change_user, previous_transaction_id,
										complete, retry_level, is_fee, funding_id, orig_transaction_id, retry_sublevel)
								VALUES (NULL, @p1, @tomorrow, NULL, 1, NULL, GETUTCDATE(), 'SYSTEM', @transaction_id, 0, @retry_level + 1, 0, @funding_id, @orig_transaction_id, 0)

								SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
								IF @errorNum != 0 GOTO ERROR

								INSERT INTO payments(payment_schedule_id, amount, trans_date, transaction_id, approved_flag, processed_date, change_date, change_user, previous_transaction_id,
									complete, retry_level, is_fee, funding_id, orig_transaction_id, retry_sublevel)
								VALUES (NULL, @p2, @tomorrow, NULL, 1, NULL, GETUTCDATE(), 'SYSTEM', @transaction_id, 0, @retry_level + 1, 0, @funding_id, @orig_transaction_id, 1)

								SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
								IF @errorNum != 0 GOTO ERROR
							END
					END -- @retry_level = 1
				ELSE  -- retry level = 2
					-- insert 2 payments (high and low from previous 4)	
					BEGIN
						PRINT 'Handled in separate proces.  Have to wait for all 4 at this level to be processed before knowing what to do.'
					END
			END -- @is_fee = 0 AND COALESCE(@active, 1) = 1 
		ELSE IF @is_fee = 1
			BEGIN   -- @is_fee = 1
				-- stop at 2 retries (just because) account likely will be on hold before ever getting here.
				IF @retry_level < 0  -- don't do retries on fees anymore.  A fee would have been assessed and will get pulled at some point but no need to retry fees immediately.
					BEGIN
						INSERT INTO payments(payment_schedule_id, amount, trans_date, transaction_id, approved_flag, processed_date, change_date, change_user, previous_transaction_id,
								complete, retry_level, is_fee, funding_id, orig_transaction_id, retry_sublevel)
						VALUES (NULL, @trans_amount, @tomorrow, NULL, 1, NULL, GETUTCDATE(), 'SYSTEM', @transaction_id, 0, @retry_level + 1, 1, @funding_id, @orig_transaction_id, 0)

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
						IF @errorNum != 0 GOTO ERROR
					END
			END
	END -- @on_hold <> 1 AND @do_retries = 1
COMMIT TRANSACTION
GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)	

Done:

END
GO
