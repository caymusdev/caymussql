SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-06-25
-- Description:	Inserts a merchant from SOLO if it does not exist
-- =============================================
CREATE PROCEDURE [dbo].[CSP_SyncMerchant]
(
	@merchant_id			BIGINT,
	@merchant_name			NVARCHAR (100),
	@affiliate_id			BIGINT
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @minTempID BIGINT; SET @minTempID = 100000000
DECLARE @maxID BIGINT

IF @merchant_id IS NULL
	BEGIN
		SELECT @maxID = MAX(merchant_id) FROM merchants WITH (NOLOCK)

		IF @maxID < @minTempID
			BEGIN
				SET @merchant_id = @minTempID
			END
		ELSE
			BEGIN
				SET @merchant_id = @maxID + 1
			END
		-- in theory, this code will only run during BCP which will hopefully never happen.  If it does, only one worksheet at a time will be uploaded so
		-- the associated affiliate id should be the max in affiliates that would have just been added before calling this SP
		SELECT @affiliate_id = MAX(affiliate_id) FROM affiliates WITH (NOLOCK)
	END


IF NOT EXISTS(SELECT 1 FROM merchants WHERE merchant_id = @merchant_id)
	BEGIN
		INSERT INTO merchants(merchant_id, merchant_name, affiliate_id, change_date, change_user)
		VALUES (@merchant_id, @merchant_name, @affiliate_id, GETUTCDATE(), 'SYSTEM')
	END
END
GO
