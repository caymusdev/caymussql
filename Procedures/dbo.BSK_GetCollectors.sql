SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2021-03-15
-- Description: Gets all collectors in the system
-- =============================================
CREATE PROCEDURE dbo.BSK_GetCollectors
AS
BEGIN
SET NOCOUNT ON


SELECT u.user_id, u.email, r.role_name
FROM users u
INNER JOIN user_roles ur ON u.user_id = ur.user_id
INNER JOIN roles r ON ur.role_id = r.role_id
WHERE u.active = 1 AND r.role_name IN ('Collector', 'CustomerService')
ORDER BY u.email


END
GO
