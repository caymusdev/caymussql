SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-08-30
-- Description:	Retrieves all settings related to sending an email
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetEmailSettings]
	
AS
BEGIN
SET NOCOUNT ON;

SELECT dbo.CF_GetSetting('EmailFrom') AS EmailFrom, dbo.CF_GetSetting('EmailSMTPUserID') AS EmailSMTPUserID, dbo.CF_GetSetting('EmailSMTPPassword') AS EmailSMTPPassword, 
	dbo.CF_GetSetting('EmailSMTPHost') AS EmailSMTPHost, dbo.CF_GetSetting('EmailSMTPort') AS EmailSMTPort
	
END
GO
