SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2020-10-02
-- Description: Redoes future payment dates to make sure all active fundings have the proper dates
-- =============================================
CREATE PROCEDURE [dbo].[CSP_DoFuturePaymentDates]

AS
BEGIN
SET NOCOUNT ON

-- This will redo all future payment dates for each active contract
-- start at the last currently scheduled payment.  sum up all between now and then.  if needed, change last payment and then start 
-- from there or don't change last payment and then start from there
-- this wil run right before payments are generated so we can get rid of CSP_GenerateMissingFundingPaymentDates which currently runs right before
-- generating payments which would only create funding payment dates for tomorrow.  Now, we'll just create all future ones.


DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SELECT @tomorrow = DATEADD(DD, CASE DATEPART(WEEKDAY, @today) WHEN 6 THEN 3 WHEN 7 THEN 2 ELSE 1 END, @today)


--IF OBJECT_ID('tempFundingPaymentDates') IS NOT NULL DROP TABLE tempFundingPaymentDates
--SELECT * 
--INTO tempFundingPaymentDates
--FROM funding_payment_dates
--WHERE 1 = 2

IF OBJECT_ID('tempdb..#tempFundings') IS NOT NULL DROP TABLE #tempFundings
CREATE TABLE #tempFundings (id INT IDENTITY(1, 1), funding_id BIGINT, payment_amount MONEY, frequency VARCHAR (50), payoff_amount MONEY, merchant_bank_account_id INT, 
	first_payment_date DATE)

-- first get all fundings that are active and do not have enough scheduled payments to finish paying it off
INSERT INTO #tempFundings (funding_id, payment_amount, frequency, payoff_amount, merchant_bank_account_id, first_payment_date)
SELECT id, CASE f.frequency WHEN 'Daily' THEN f.weekday_payment ELSE f.weekly_payment END AS payment, f.frequency, f.payoff_amount, ba.merchant_bank_account_id, 
	COALESCE(f.first_payment_date, f.funded_date)
FROM fundings f WITH (NOLOCK)
LEFT JOIN merchant_bank_accounts ba WITH (NOLOCK) ON f.receivables_account_nbr = ba.account_number AND f.receivables_aba = ba.routing_number AND ba.account_status = 'Active'
	AND f.merchant_id = ba.merchant_id
WHERE f.contract_status = 1 AND COALESCE(f.on_hold, 0) = 0 AND f.payoff_amount > 0
	AND f.payoff_amount > COALESCE((SELECT SUM(amount) FROM funding_payment_dates pd WHERE pd.funding_id = f.id AND pd.payment_date >= @tomorrow AND pd.active = 1), 0)
	AND f.hard_offer_type = 'ACH' AND f.processor IS NOT NULL
	AND (f.auto_payment_dates = 1 OR (f.rtr_out = 0 AND COALESCE(f.fees_out, 0) > 0))-- regardless of auto payment dates setting, if funding is paid off except fees we want it to happen
ORDER BY f.id

DECLARE @maxID INT, @tempID INT, @tempFutureAmounts MONEY, @tempPayoffAmount MONEY, @tempDate DATE, @tempFundingID INT, @tempOriginal BIT, @last_date DATE
DECLARE @original_payment MONEY, @original_frequency VARCHAR (50), @lastPaymentAmount MONEY, @last_payment_id INT, @remaining_amount MONEY, @temp_bank_id INT
DECLARE @first_payment_date DATE, @tempDate2 DATE

SELECT @tempID = 1, @maxID = MAX(id) FROM #tempFundings

WHILE @tempID <= @maxID
	BEGIN
		-- find the last funding payment date for each funding above.
		-- if it has changed (approved) then take remaining balance and start creating regular payments after it
		-- if it has not changed (original) update it if it is smaller than regular payment and then continue making regular payments based on remaining balance.
		SELECT @tempPayoffAmount = payoff_amount, @tempFundingID = funding_id, @original_payment = payment_amount, @original_frequency = frequency, @temp_bank_id = merchant_bank_account_id,
			@first_payment_date = first_payment_date
		FROM #tempFundings 
		WHERE id = @tempID

		SELECT @last_date = MAX(pd.payment_date), @tempFutureAmounts = SUM(pd.amount)
		FROM funding_payment_dates pd WITH (NOLOCK) 
		WHERE pd.funding_id = @tempFundingID AND pd.active = 1 AND pd.payment_date >= @tomorrow
		SET @last_date = COALESCE(@last_date, @today)

		SELECT @tempOriginal = original, @lastPaymentAmount = amount, @last_payment_id = id
		FROM funding_payment_dates WITH (NOLOCK) WHERE funding_id = @tempFundingID AND active = 1 AND payment_date = @last_date

		IF COALESCE(@tempOriginal, 0) = 1
			BEGIN
				IF @lastPaymentAmount + (@tempPayoffAmount - @tempFutureAmounts) <= @original_payment
					-- this is the last payment and it is an original payment and what they will owe on that day is less than the original payment
					-- so this is the last one and needs to be the correct amount
					BEGIN
						UPDATE funding_payment_dates
						SET amount = @lastPaymentAmount + (@tempPayoffAmount - @tempFutureAmounts), change_date = GETUTCDATE(), change_user = 'FuturePayments'
						WHERE id = @last_payment_id AND amount <> @lastPaymentAmount + (@tempPayoffAmount - @tempFutureAmounts)
					END
				ELSE
					-- We are on the last original payment and what they will owe is more than the original payment
					-- so need to make sure this last payment equals the original and then we can move forward with new payemnts
					BEGIN
						UPDATE funding_payment_dates
						SET amount = @original_payment, change_date = GETUTCDATE(), change_user = 'FuturePayments'
						WHERE id = @last_payment_id AND amount <> @original_payment
					END
			END

		-- Now that the very last payment has either been adjusted (if original) or left alone (if not original) we need to get the total 
		-- future payments amount again and then possibly start creating payment dates

		SELECT @tempFutureAmounts = SUM(pd.amount)
		FROM funding_payment_dates pd WITH (NOLOCK) 
		WHERE pd.funding_id = @tempFundingID AND pd.active = 1 AND pd.payment_date >= @tomorrow

		SET @remaining_amount = (@tempPayoffAmount - COALESCE(@tempFutureAmounts, 0))
		SET @tempDate = DATEADD(dd, 1, @last_date)  -- start with the payments last date's tomorrow

		WHILE @remaining_amount > 0
			BEGIN
				IF @original_frequency = 'Daily'
					BEGIN
						IF DATEPART(dw, @tempDate) IN (2,3,4,5,6) -- only do Monday Through Friday even though CC might have weekend amounts
							BEGIN
								INSERT INTO funding_payment_dates (funding_id, payment_date, amount, rec_amount, payment_schedule_id, create_date, active, original, chargeback_amount, 
									merchant_bank_account_id, payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, change_date, change_user, 
									payment_plan_id)
								VALUES (@tempFundingID, @tempDate, CASE WHEN @remaining_amount < @original_payment THEN @remaining_amount ELSE @original_payment END, 
									NULL, NULL, GETUTCDATE(), 1, 0, NULL, @temp_bank_id, 'ACH', 'Forte', NULL, 'System Generated', NULL, GETUTCDATE(), 'FuturePayments', NULL)

								SET @remaining_amount = @remaining_amount - @original_payment
							END
					END
				ELSE IF @original_frequency = 'Weekly'
					BEGIN
						IF DATEPART(dw, @tempDate) = DATEPART(dw, @first_payment_date)
							BEGIN
								-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
								INSERT INTO funding_payment_dates (funding_id, payment_date, amount, rec_amount, payment_schedule_id, create_date, active, original, chargeback_amount, 
									merchant_bank_account_id, payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, change_date, change_user, 
									payment_plan_id)
								VALUES (@tempFundingID, @tempDate, CASE WHEN @remaining_amount < @original_payment THEN @remaining_amount ELSE @original_payment END, 
									NULL, NULL, GETUTCDATE(), 1, 0, NULL, @temp_bank_id, 'ACH', 'Forte', NULL, 'System Generated', NULL, GETUTCDATE(), 'FuturePayments', NULL)

								SET @remaining_amount = @remaining_amount - @original_payment
							END
					END
				ELSE IF @original_frequency = 'Monthly'
					BEGIN
						IF DAY(@tempDate) = DAY(@first_payment_date)
							BEGIN
								-- this means we are on the same day of the week as the first payment so we need to insert as a future payment

								SET @tempDate2 = @tempDate
								-- if it is weekend, bring it back to friday
								SET @tempDate2 = DATEADD(DD, CASE DATEPART(dw, @tempDate) WHEN 1 THEN -2 WHEN 7 THEN -1 ELSE 0 END, @tempDate) 		
										
								INSERT INTO funding_payment_dates (funding_id, payment_date, amount, rec_amount, payment_schedule_id, create_date, active, original, chargeback_amount, 
									merchant_bank_account_id, payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, change_date, change_user, 
									payment_plan_id)
								VALUES (@tempFundingID, @tempDate2, CASE WHEN @remaining_amount < @original_payment THEN @remaining_amount ELSE @original_payment END, 
									NULL, NULL, GETUTCDATE(), 1, 0, NULL, @temp_bank_id, 'ACH', 'Forte', NULL, 'System Generated', NULL, GETUTCDATE(), 'FuturePayments', NULL)		
									
								SET @remaining_amount = @remaining_amount - @original_payment
							END
					END
				ELSE
					BEGIN
						SET @remaining_amount = 0 --- in case frequency is not one of the ones it should be this will get us out of the loop
					END
				SET @tempDate = DATEADD(dd, 1, @tempDate)
			END  -- @remaining_amount > 0
		SET @tempID = @tempID + 1
	END


END
GO
