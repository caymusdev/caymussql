SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2021-03-09
-- Description: Approves a scheduled payment plan.  Goes through each item in the payment plan and calls approve on it separately.
-- =============================================
CREATE PROCEDURE dbo.[BSK_ApproveSchedPaymentPlan]
(
	@payment_plan_id				INT,
	@users_email					NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON

DECLARE @maxID INT, @currentID INT, @temp_payment_id INT

-- need a temp table to store all the active fundings for this affiliate and how much of the payment each should get applied.
IF OBJECT_ID('tempdb..#tempDataPlan') IS NOT NULL DROP TABLE #tempDataPlan
CREATE TABLE #tempDataPlan (id INT IDENTITY (1, 1), scheduled_payment_id INT)

INSERT INTO #tempDataPlan (scheduled_payment_id)
SELECT p.scheduled_payment_id
FROM scheduled_payments p
WHERE payment_plan_id = @payment_plan_id AND approved_date IS NULL AND approved_user IS NULL AND rejected_user IS NULL AND rejected_date IS NULL
ORDER BY p.payment_date

SELECT @maxID = MAX(id), @currentID = 1
FROM #tempDataPlan

WHILE @currentID <= @maxID
	BEGIN
		SELECT @temp_payment_id = scheduled_payment_id FROM #tempDataPlan WHERE id = @currentID
				
		EXEC dbo.BSK_ApproveSchedPayment @temp_payment_id, @users_email

		SET @currentID = @currentID + 1
	END
END



GO
