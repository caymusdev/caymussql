SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--==========================================
-- Author:		Ryan Brown
-- Create date: 2019-11-22
-- Description:	Checks for transactions where the amount does not total the detail
-- =============================================
CREATE PROCEDURE [dbo].[CSP_CheckForUnbalancedTransactions]

AS
BEGIN
SET NOCOUNT ON;

SELECT t.trans_id, t.trans_date, t.funding_id, t.trans_amount, 
	COALESCE((SELECT SUM(d.trans_amount) FROM transaction_details d WHERE d.trans_id = t.trans_id), 0) AS detail_amount,
	t.trans_amount - COALESCE((SELECT SUM(d.trans_amount) FROM transaction_details d WHERE d.trans_id = t.trans_id), 0) AS diff
FROM transactions t WITH (NOLOCK)
WHERE COALESCE((SELECT SUM(d.trans_amount) FROM transaction_details d WHERE d.trans_id = t.trans_id), 0) <> t.trans_amount
	AND t.redistributed = 0
	AND t.trans_date >= '2019-01-01'
ORDER BY t.trans_date, t.funding_id, t.trans_id


END
GO
