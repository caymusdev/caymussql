SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-28
-- Description:	updates case statistics
-- =============================================
CREATE PROCEDURE [dbo].[BSK_UpdCaseStats]
	
AS
BEGIN
SET NOCOUNT ON;

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @yesterday DATE; SELECT @yesterday = dbo.BF_GetYesterday(@today)

UPDATE c
SET last_contact = x.last_contact, last_attempted_contact = x.last_attempted_contact, last_contact_type = x.last_contact_type, last_attempted_contact_type = x.last_attempted_contact_type, 
	last_contact_status_id = x.last_contact_status_id, last_attempted_contact_status_id = x.last_attempted_contact_status_id, next_follow_up_date = x.next_follow_up_date,
	collection_age = x.collection_age, attempted_calls = x.attempted_calls, contacts_made = x.contacts_made, change_date = GETUTCDATE(), change_user = 'System', 
	days_assigned_to_collector = x.days_assigned_to_collector, last_payment_date = x.last_payment_date, last_payment_amount = x.last_payment_amount
FROM cases c
INNER JOIN (
	SELECT c.case_id, cl.contact_date AS last_contact, cl.contact_date AS last_attempted_contact, 
		cl.contact_type AS last_contact_type, cl.contact_type AS last_attempted_contact_type, cl.contact_status_id AS last_contact_status_id, 
		cl.contact_status_id AS last_attempted_contact_status_id, y.next_follow_up_date, 
		DATEDIFF(DD, COALESCE(c.collection_date, GETUTCDATE()), GETUTCDATE()) AS collection_age, 
		COALESCE((SELECT COUNT(*) 
		 FROM contact_log cl WITH (NOLOCK)
		 INNER JOIN contact_types ct WITH (NOLOCK) ON cl.contact_type = ct.contact_type_id
		 WHERE cl.affiliate_id = c.affiliate_id AND ct.contact_type = 'phone'), 0) AS attempted_calls, 
		 COALESCE(
			(SELECT COUNT(*) 
			FROM contact_log cl WITH (NOLOCK)
			INNER JOIN contact_types ct WITH (NOLOCK) ON cl.contact_type = ct.contact_type_id
			LEFT JOIN emails e ON cl.record_id = e.email_id AND cl.contact_type = (SELECT contact_type_id FROM contact_types WHERE contact_type = 'email')
			LEFT JOIN texts t ON cl.record_id = t.text_id AND cl.contact_type = (SELECT contact_type_id FROM contact_types WHERE contact_type = 'text') 
			WHERE cl.affiliate_id = c.affiliate_id 
			AND (
				ct.contact_type IN ('phone')
				OR (ct.contact_type IN ('email') AND e.incoming = 1)
				OR (ct.contact_type IN ('text') AND COALESCE(t.sent_from_bossk, 0) = 0)
				)
			), 0
		) AS contacts_made, 
		 COALESCE(DATEDIFF(DD, cc.start_date, @today), 0) AS days_assigned_to_collector, last_payment.trans_date AS last_payment_date, 
		 last_payment.trans_amount AS last_payment_amount 
	FROM cases c WITH (NOLOCK)
	LEFT JOIN (
		SELECT affiliate_id, MAX(contact_log_id) AS last_contact_id
		FROM contact_log WITH (NOLOCK)
		GROUP BY affiliate_id
	) x ON c.affiliate_id = x.affiliate_id
	LEFT JOIN contact_log cl WITH (NOLOCK) ON x.last_contact_id = cl.contact_log_id
	LEFT JOIN (
		SELECT r.affiliate_id, MIN(r.reminder_date) AS next_follow_up_date
		FROM reminders r WITH  (NOLOCK)
		WHERE r.reminder_date >= @today	
		GROUP BY r.affiliate_id	
	) y ON c.affiliate_id = y.affiliate_id
	LEFT JOIN collector_cases cc ON c.case_id = cc.case_id AND cc.start_date <= @today AND COALESCE(cc.end_date, '2050-01-01') >= @today
	LEFT JOIN (	
		SELECT TOP 1 WITH TIES f.affiliate_id, t.trans_date, t.trans_amount
		FROM transactions t
		INNER JOIN fundings f ON t.funding_id = f.id
		WHERE t.trans_amount > 0 AND t.trans_type_id IN (2, 3, 4, 5, 19)
		ORDER BY ROW_NUMBER() OVER (PARTITION BY f.affiliate_id ORDER BY t.trans_date DESC, t.trans_amount DESC)
	) last_payment ON c.affiliate_id = last_payment.affiliate_id
) x ON c.case_id = x.case_id
WHERE (COALESCE(c.last_contact, '1900-01-01') != COALESCE(x.last_contact, '1900-01-01') OR COALESCE(c.last_attempted_contact, '1900-01-01') != COALESCE(x.last_attempted_contact, '1900-01-01') OR 
	COALESCE(c.last_contact_type, -1) != COALESCE(x.last_contact_type, -1) OR COALESCE(c.last_attempted_contact_type, -1) != COALESCE(x.last_attempted_contact_type, -1) OR
	COALESCE(c.last_contact_status_id, -1) != COALESCE(x.last_contact_status_id, -1) OR COALESCE(c.last_attempted_contact_status_id, -1) != COALESCE(x.last_attempted_contact_status_id, -1) OR
	COALESCE(c.next_follow_up_date, '1900-01-01') != COALESCE(x.next_follow_up_date, '1900-01-01') OR COALESCE(c.collection_age, -1) != x.collection_age OR 
	COALESCE(c.attempted_calls, -1) != x.attempted_calls OR COALESCE(c.contacts_made, -1) != x.contacts_made OR 
	COALESCE(c.days_assigned_to_collector, 0) != COALESCE(x.days_assigned_to_collector, 0) OR
	COALESCE(c.last_payment_amount, 0) != COALESCE(x.last_payment_amount, 0) OR
	COALESCE(c.last_payment_date, '1900-01-01') != COALESCE(x.last_payment_date, '1900-01-01')
	)


	
UPDATE c
SET last_payment_date = x.trans_date, last_payment_amount = x.trans_amount, change_date = GETUTCDATE(), change_user = 'System'
FROM contracts c
INNER JOIN (
	SELECT TOP 1 WITH TIES t.funding_id, t.trans_date, t.trans_amount
	FROM transactions t
	ORDER BY ROW_NUMBER() OVER (PARTITION BY t.funding_id ORDER BY t.trans_amount DESC)
) x ON c.contract_id = x.funding_id
WHERE (COALESCE(c.last_payment_date, '1900-01-01') != COALESCE(x.trans_date, '1900-01-01') OR COALESCE(c.last_payment_amount, 0) != COALESCE(x.trans_amount, 0))


--****************
DECLARE @active_id INT, @inactive_id INT
SELECT @active_id = cs.case_status_id FROM case_statuses cs WITH (NOLOCK) WHERE cs.status = 'Active'
SELECT @inactive_id = cs.case_status_id FROM case_statuses cs WITH (NOLOCK) WHERE cs.status = 'Inactive'

-- if a case has any payments in the future on fundings not on hold, set case_status_id to inactive if it is active
UPDATE c
SET case_status_id = CASE WHEN x.affiliate_id IS NULL OR y.affiliate_id IS NOT NULL THEN @active_id ELSE @inactive_id END, change_date = GETUTCDATE(), change_user = 'System'
FROM cases c
LEFT JOIN (  -- fundings that are not on hold and have future payments and the payment plan (if any) is not broken
	SELECT MIN(payment_date) AS next_payment, f.affiliate_id
	FROM funding_payment_dates fpd
	INNER JOIN fundings f ON fpd.funding_id = f.id
	LEFT JOIN payment_plans pp ON fpd.payment_plan_id = pp.payment_plan_id
	LEFT JOIN payment_plan_status_codes sc ON pp.current_status = sc.id
	WHERE fpd.payment_date > @today AND COALESCE(f.on_hold, 0) = 0 AND COALESCE(sc.code, 'InProgress') IN ('Pending', 'InProgress')
	GROUP BY f.affiliate_id
) x ON c.affiliate_id = x.affiliate_id  --- next future payment date by affiliate
LEFT JOIN(
	SELECT DISTINCT f.affiliate_id
	FROM transactions t
	INNER JOIN fundings f ON t.funding_id = f.id
	LEFT JOIN batches b ON t.batch_id = b.batch_id
	WHERE t.trans_date >= @yesterday AND trans_type_id IN (14, 21)  -- see if they rejected anything today or yesterday, if so, they need to go active again
		AND t.redistributed = 0  AND COALESCE(b.batch_status, '') NOT IN ('Duplicate', 'Duplicate-old', 'Deleted')	
) y ON c.affiliate_id = y.affiliate_id
--WHERE c.case_status_id = (SELECT case_status_id FROM case_statuses WHERE status = 'Active')
WHERE c.case_status_id  != CASE WHEN x.affiliate_id IS NULL THEN @active_id ELSE @inactive_id END


-- if they have an active payment plan, need to clear out next_move_on
UPDATE cd
SET next_move_on = NULL, next_move_to = NULL, change_date = GETUTCDATE(), change_user = 'System', do_not_move = NULL, do_not_move_by = NULL, 
	do_not_move_until = NULL
FROM case_departments cd
INNER JOIN cases c ON cd.case_id = c.case_id
INNER JOIN case_statuses cs ON c.case_status_id = cs.case_status_id
WHERE next_move_on IS NOT NULL
	-- make sure there is a resolution (active or pending payment plan)
	AND cs.status IN ('Inactive', 'ClosedUncollectable')



-- Store in temp table a list of cases that will be moved.  Need to update cases table AND insert AND update into case_departments so need a temp table to 
-- keep track of the specific cases involved.
IF OBJECT_ID('tempdb..#tempMoveCases') IS NOT NULL DROP TABLE #tempMoveCases
CREATE TABLE #tempMoveCases (id INT IDENTITY(1, 1), case_id INT, current_department_id INT, next_department_id INT)

INSERT INTO #tempMoveCases (case_id, current_department_id, next_department_id)
SELECT c.case_id, c.department_id, cd.next_move_to
FROM cases c
INNER JOIN case_departments cd ON c.case_id = cd.case_id AND cd.start_date <= @today AND COALESCE(cd.end_date, '2050-01-01') >= @today
WHERE COALESCE(cd.next_move_on, '2050-01-01') <= @today AND cd.next_move_to != c.department_id AND cd.do_not_move IS NULL AND cd.do_not_move_by IS NULL
	AND COALESCE(cd.do_not_move_until, '2000-01-01') <= @today
ORDER BY cd.next_move_on

-- move cases first - change their department_id
UPDATE c
SET department_id = t.next_department_id, change_user = 'System', change_date = GETUTCDATE()
FROM cases c
INNER JOIN #tempMoveCases t ON c.case_id = t.case_id

-- update previous case_departments records' end dates
SELECT @yesterday = DATEADD(DD, -1, @today)
UPDATE cd
SET end_date = @yesterday, change_user = 'SystemUpdCaseStats', change_date = GETUTCDATE()
FROM case_departments cd
INNER JOIN #tempMoveCases t ON cd.case_id = t.case_id AND cd.department_id = t.current_department_id AND cd.end_date IS NULL


-- add records in case_departments for the new department
INSERT INTO case_departments (case_id, department_id, start_date, end_date, create_date, change_date, change_user, create_user, do_not_move, do_not_move_by, next_move_on, next_move_to)
SELECT t.case_id, t.next_department_id, @today, NULL, GETUTCDATE(), GETUTCDATE(), 'SystemUpdCaseStats', 'SystemUpdCaseStats', NULL, NULL, NULL, NULL
FROM #tempMoveCases t


-- NOW ASSIGN THE NEW CASES TO A COLLECTOR
DECLARE @CaseIDs IDTableType
INSERT INTO @CaseIDs(record_id)
SELECT case_id
FROM #tempMoveCases
--  SELECT * FROM @CaseIDs

EXEC dbo.BSK_AssignCases @CaseIDs


-- now update the next move to based on the changes above
UPDATE cd
-- get the larger date of "do not move's until's tomorrow" or the regular calculation
SET next_move_on = dbo.BF_GetLargerDate(dbo.BF_GetTomorrow(COALESCE(cd.do_not_move_until, '2000-01-01')), 
	DATEADD(day, (d.days_before_moving % 5) +   
		CASE WHEN ((@@DATEFIRST + DATEPART(weekday, dbo.BF_GetLargerDate(cd.start_date, broken_plans.most_recent_break_date))) % 7 
			+ (d.days_before_moving % 5)) > 6 THEN 2 ELSE 0 END, DATEADD(week, (d.days_before_moving / 5), dbo.BF_GetLargerDate(cd.start_date, broken_plans.most_recent_break_date)))), 
	next_move_to = d2.department_id, change_date = GETUTCDATE(), change_user = 'System'
FROM case_departments cd
INNER JOIN departments d ON cd.department_id = d.department_id
INNER JOIN departments d2 ON (d.department = 'CustomerService' AND d2.department = 'Collections1')
	OR (d.department = 'Collections1' AND d2.department = 'Collections2')
	OR (d.department = 'Collections2' AND d2.department = 'Collections3PreLegal')
INNER JOIN cases c ON cd.case_id = c.case_id
INNER JOIN case_statuses cs ON c.case_status_id = cs.case_status_id
LEFT JOIN (
	SELECT pp.affiliate_id, MAX(pps.start_date) AS most_recent_break_date
	FROM payment_plan_statuses pps
	INNER JOIN payment_plan_status_codes ppc ON pps.payment_plan_status_code = ppc.id
	INNER JOIN payment_plans pp ON pps.payment_plan_id = pp.payment_plan_id
	WHERE ppc.code IN ('Broken', 'BrokenPayment') AND pps.end_date IS NULL
	GROUP BY pp.affiliate_id
) broken_plans ON c.affiliate_id = broken_plans.affiliate_id
WHERE cd.do_not_move IS NULL AND cd.do_not_move_by IS NULL AND d.days_before_moving IS NOT NULL
	AND COALESCE(next_move_on, '2000-01-01') != dbo.BF_GetLargerDate(dbo.BF_GetTomorrow(COALESCE(cd.do_not_move_until, '2000-01-01')), 
	DATEADD(day, (d.days_before_moving % 5) +   
		CASE WHEN ((@@DATEFIRST + DATEPART(weekday, dbo.BF_GetLargerDate(cd.start_date, broken_plans.most_recent_break_date))) % 7 
			+ (d.days_before_moving % 5)) > 6 THEN 2 ELSE 0 END, DATEADD(week, (d.days_before_moving / 5), dbo.BF_GetLargerDate(cd.start_date, broken_plans.most_recent_break_date))))
	-- make sure they are active cases
	AND cs.status = 'Active'
	AND c.active = 1
	AND COALESCE(cd.do_not_move_until, '2000-01-01') <= @today


END

GO
