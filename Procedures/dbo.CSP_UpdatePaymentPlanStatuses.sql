SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-07-254
-- Description:	Updates the status of payment plans.  
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdatePaymentPlanStatuses]
AS
BEGIN

SET NOCOUNT ON;
-- THIS NOW NEEDS TO UPDATE BOSSK TABLES

-- VARS
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @yesterday DATE; SELECT @yesterday = DATEADD(DD, CASE DATEPART(WEEKDAY, @today) WHEN 1 THEN -2 WHEN 2 THEN -3 ELSE -1 END, @today)
DECLARE @pending_id INT; SELECT @pending_id = id FROM payment_plan_status_codes WHERE code = 'Pending'
DECLARE @complete_id INT; SELECT @complete_id = id FROM payment_plan_status_codes WHERE code = 'Complete'
DECLARE @in_progress_id INT; SELECT @in_progress_id = id FROM payment_plan_status_codes WHERE code = 'InProgress'
DECLARE @broken_payment_id INT; SELECT @broken_payment_id = id FROM payment_plan_status_codes WHERE code = 'BrokenPayment'
DECLARE @broken_id INT; SELECT @broken_id = id FROM payment_plan_status_codes WHERE code = 'Broken'
DECLARE @bufferDays INT; SET @bufferDays = 5

-- END VARS

-- temp table / work table
IF OBJECT_ID('tempdb..#tempIDs') IS NOT NULL DROP TABLE #tempIDs
CREATE TABLE #tempIDs (id int not null identity, payment_plan_id INT)

IF OBJECT_ID('tempdb..#tempPlans') IS NOT NULL DROP TABLE #tempPlans
CREATE TABLE #tempPlans (id int not null identity, payment_plan_id INT, plan_status INT NULL, orderby INT NULL, original BIT NULL)
-- end temp table

-- put into temp table all payment plans that DO NOT HAVE any status records.  Shouldn't be any, but just in case.
INSERT INTO #tempIDs (payment_plan_id)
SELECT pp.payment_plan_id
FROM payment_plans pp WITH (NOLOCK)
WHERE NOT EXISTS (SELECT 1 FROM payment_plan_statuses s WHERE s.payment_plan_id = pp.payment_plan_id AND s.payment_plan_status_code = @pending_id)

-- First make sure there is a Pending record for each payment_plan
INSERT INTO payment_plan_statuses (payment_plan_id, payment_plan_status_code, start_date, end_date, change_date, change_user)
SELECT pp.payment_plan_id, @pending_id, CONVERT(DATE, pp.create_date), NULL, GETUTCDATE(), 'SYSTEM'
FROM payment_plans pp WITH (NOLOCK)
INNER JOIN #tempIDs t ON pp.payment_plan_id = t.payment_plan_id

-- make sure current status id is now set to pending
UPDATE pp
SET current_status = @pending_id, change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM payment_plans pp WITH (NOLOCK)
INNER JOIN #tempIDs t ON pp.payment_plan_id = t.payment_plan_id
WHERE pp.current_status IS NULL

-- clear out work table
TRUNCATE TABLE #tempIDs


-- Secondly, set to Complete all those that have a complete funding now
INSERT INTO #tempIDs (payment_plan_id)
SELECT DISTINCT pp.payment_plan_id
FROM payment_plans pp WITH (NOLOCK)
INNER JOIN funding_payment_dates pd WITH (NOLOCK) ON pp.payment_plan_id = pd.payment_plan_id
INNER JOIN fundings f ON pd.funding_id = f.id
WHERE pp.current_status <> @complete_id AND (f.contract_status = 2 OR (f.contract_status = 4 AND f.contract_substatus_id = 2))

INSERT INTO payment_plan_statuses (payment_plan_id, payment_plan_status_code, start_date, end_date, change_date, change_user)
SELECT t.payment_plan_id, @complete_id, @today, NULL, GETUTCDATE(), 'SYSTEM'
FROM #tempIDs t

-- now update payment plans current status
UPDATE payment_plans 
SET current_status = @complete_id, change_user = 'SYSTEM', change_date = GETUTCDATE()
WHERE payment_plan_id IN (SELECT payment_plan_id FROM #tempIDs)

-- now set end date to yesterday on all history ones that have no end date
UPDATE payment_plan_statuses
SET end_date = @yesterday, change_date = GETUTCDATE(), change_user = 'SYSTEM'
WHERE payment_plan_id IN (SELECT payment_plan_id FROM #tempIDs) AND end_date IS NULL AND payment_plan_status_code <> @complete_id

-- clear out work table
TRUNCATE TABLE #tempIDs



----**********************************************************--------------------------
----**********************************************************--------------------------
----**********************************************************--------------------------
-- Now, load up temp table with original values.  Will be used at the end to compare to and update statuses
INSERT INTO #tempPlans(payment_plan_id, plan_status, orderby, original)
SELECT pp.payment_plan_id, pp.current_status, 0, 1
FROM payment_plans pp WITH (NOLOCK)
WHERE pp.current_status NOT IN (@complete_id)

-- want to keep a separate list of just the ids we are dealing with
INSERT INTO #tempIDs (payment_plan_id)
SELECT DISTINCT payment_plan_id
FROM #tempPlans



-- now insert in progress status for those that have any payment
INSERT INTO #tempPlans(payment_plan_id, plan_status, orderby, original)
SELECT DISTINCT i.payment_plan_id, @in_progress_id, 1, 0
FROM #tempIDs i
INNER JOIN funding_payment_dates pd WITH (NOLOCK) ON i.payment_plan_id = pd.payment_plan_id AND COALESCE(pd.rec_amount, 0) > 0

-- now check for any missed payments
INSERT INTO #tempPlans(payment_plan_id, plan_status, orderby, original)
SELECT DISTINCT i.payment_plan_id, @broken_payment_id, 2, 0
FROM #tempIDs i
INNER JOIN funding_payment_dates pd WITH (NOLOCK) ON i.payment_plan_id = pd.payment_plan_id
	AND (
		(pd.rec_amount = 0 OR (pd.rec_amount IS NULL AND DATEDIFF(DD, pd.payment_date, @today) > @bufferDays))
		OR 
		COALESCE(pd.chargeback_amount, 0) <> 0
	)


-- now check for broken plan
INSERT INTO #tempPlans(payment_plan_id, plan_status, orderby, original)
SELECT DISTINCT i.payment_plan_id, @broken_id, 3, 0
FROM #tempIDs i
INNER JOIN funding_payment_dates pd WITH (NOLOCK) ON i.payment_plan_id = pd.payment_plan_id
	AND (
		(pd.rec_amount = 0 OR (pd.rec_amount IS NULL AND DATEDIFF(DD, pd.payment_date, @today) > @bufferDays))
		OR 
		COALESCE(pd.chargeback_amount, 0) <> 0
	)
INNER JOIN payment_plans pp ON i.payment_plan_id = pp.payment_plan_id
WHERE DATEDIFF(DD, COALESCE(pp.broken_payment_date, pd.payment_date), pd.payment_date) > 7


-- now check for inprogress if everything all adds up (received enough money)
INSERT INTO #tempPlans(payment_plan_id, plan_status, orderby, original)
SELECT DISTINCT x.payment_plan_id, @in_progress_id, 4, 0
FROM (
	SELECT x.payment_plan_id, x.total_expected, SUM(t.trans_amount) AS rec_amount
	FROM (
			SELECT DISTINCT i.payment_plan_id, pd.funding_id, SUM(pd.amount) AS total_expected, MIN(pd.payment_date) AS start_date,
				MAX(pd.payment_date) AS end_date
			FROM #tempIDs i
			INNER JOIN funding_payment_dates pd ON i.payment_plan_id = pd.payment_plan_id
			GROUP BY i.payment_plan_id, pd.funding_id
	) x
	INNER JOIN transactions t ON x.funding_id = t.funding_id AND t.trans_date BETWEEN x.start_date AND x.end_date
	GROUP BY x.payment_plan_id, x.total_expected
) x
WHERE COALESCE(x.rec_amount, 0) >= COALESCE(x.total_expected, 0)

--************************************************************ Find last and set to new status

-- first set end date to yesterday on all history ones that have no end date because we're about to enter some with no end date.  Do this first.
UPDATE pps
SET end_date = @yesterday, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM payment_plan_statuses pps
INNER JOIN #tempPlans t ON pps.payment_plan_id = t.payment_plan_id
INNER JOIN (
	SELECT ROW_NUMBER() OVER (PARTITION BY t.payment_plan_id ORDER BY t.orderby DESC) AS RowNumber, t.id, t.payment_plan_id, t.plan_status, t.original
	FROM #tempPlans t
	WHERE t.original = 0
) x  ON t.payment_plan_id = x.payment_plan_id AND x.RowNumber = 1
WHERE t.original = 1 AND t.plan_status <> x.plan_status AND pps.end_date IS NULL 



-- now get the last one, if there is one (ordered by orderby DESC) and that will be the new status
INSERT INTO payment_plan_statuses (payment_plan_id, payment_plan_status_code, start_date, end_date, change_date, change_user)
SELECT t.payment_plan_id, x.plan_status, @today, NULL, GETUTCDATE(), 'SYSTEM'
FROM #tempPlans t
INNER JOIN (
	SELECT ROW_NUMBER() OVER (PARTITION BY t.payment_plan_id ORDER BY t.orderby DESC) AS RowNumber, t.id, t.payment_plan_id, t.plan_status, t.original
	FROM #tempPlans t
	WHERE t.original = 0
) x  ON t.payment_plan_id = x.payment_plan_id AND x.RowNumber = 1
WHERE t.original = 1 AND t.plan_status <> x.plan_status


-- now update payment plans current status
UPDATE pp 
SET current_status = x.plan_status, change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM payment_plans pp
INNER JOIN #tempPlans t ON pp.payment_plan_id = t.payment_plan_id
INNER JOIN (
	SELECT ROW_NUMBER() OVER (PARTITION BY t.payment_plan_id ORDER BY t.orderby DESC) AS RowNumber, t.id, t.payment_plan_id, t.plan_status, t.original
	FROM #tempPlans t
	WHERE t.original = 0
) x  ON t.payment_plan_id = x.payment_plan_id AND x.RowNumber = 1
WHERE t.original = 1 AND t.plan_status <> x.plan_status


-- change Active to Inactive if 'Complete', 'Pending', 'InProgress'
UPDATE c
SET case_status_id = (SELECT case_status_id FROM case_statuses WHERE status = 'Inactive'), change_date = GETUTCDATE(), change_user = 'PPStatus'
FROM cases c
INNER JOIN case_statuses cs ON c.case_status_id = cs.case_status_id
INNER JOIN (
	SELECT MAX(pp.payment_plan_id) AS payment_plan_id, pp.affiliate_id
	FROM payment_plans pp
	GROUP BY pp.affiliate_id
) x ON c.affiliate_id = x.affiliate_id
INNER JOIN payment_plans pp ON x.payment_plan_id = pp.payment_plan_id
INNER JOIN payment_plan_status_codes pps ON pp.current_status = pps.id
WHERE c.active = 1 AND cs.status = 'Active' AND pps.code IN ('Complete', 'Pending', 'InProgress')

-- change Inactive to Active if 'Broken', 'BrokenPayment'
UPDATE c
SET case_status_id = (SELECT case_status_id FROM case_statuses WHERE status = 'Active'), change_date = GETUTCDATE(), change_user = 'PPStatus'
FROM cases c
INNER JOIN case_statuses cs ON c.case_status_id = cs.case_status_id
INNER JOIN (
	SELECT MAX(pp.payment_plan_id) AS payment_plan_id, pp.affiliate_id
	FROM payment_plans pp
	GROUP BY pp.affiliate_id
) x ON c.affiliate_id = x.affiliate_id
INNER JOIN payment_plans pp ON x.payment_plan_id = pp.payment_plan_id
INNER JOIN payment_plan_status_codes pps ON pp.current_status = pps.id
WHERE c.active = 1 AND cs.status = 'Inactive' AND pps.code IN ('Broken', 'BrokenPayment')



EXEC dbo.CSP_UpdatePaymentScheduleStatuses

END

GO
