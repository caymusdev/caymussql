SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-07-18
-- Description:	Gets a list of fundings that have changed recently to be sent to SOLO
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetFundingsToSendToSolo]
(
	@change_date		DATETIME
)
AS
BEGIN
SET NOCOUNT ON;

SELECT f.id, f.funded_date, f.payoff_amount, f.percent_paid, COALESCE(f.calc_turn, 0) AS calc_turn, 
	COALESCE(f.percent_performance, 0) AS percent_performance, f.last_payment_date, 
	COALESCE(f.actual_turn, 0) AS actual_turn, 
	CASE f.contract_status WHEN 1 THEN 'Active' WHEN 2 THEN 'Complete' WHEN 3 THEN 'NonPerforming' WHEN 4 THEN 'Write_Off' END as contract_status
FROM fundings f
WHERE --f.change_date >= @change_date 
	f.id < 10000000  -- this means it is not in SOLO
	AND f.payoff_amount <> 0
END
GO
