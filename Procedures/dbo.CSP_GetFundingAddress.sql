SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2022-12-05
-- Description: Gets the physical address for a funding.  New for use with Solo.Net and Unity.
-- =============================================
CREATE PROCEDURE CSP_GetFundingAddress
(
	@funding_id			INT
)
AS
BEGIN    
SET NOCOUNT ON


SELECT a.address_id, a.address_1, a.address_2, a.city, a.state_abbrev, a.postal_code,
	a.address_1 + ' ' + a.address_2 + ' ' + a.city + ', ' + a.state_abbrev + ' ' + a.postal_code AS address_string
FROM addresses a
INNER JOIN address_types t ON a.address_type_id = t.address_type_id AND t.address_type = 'Physical'
INNER JOIN merchants m ON a.merchant_id = m.merchant_id
INNER JOIN fundings f ON m.merchant_id = f.merchant_id
WHERE f.id = @funding_id


END
GO
