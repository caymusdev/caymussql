SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-05-24
-- Description:	Gets a batch summary "report"
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetBatchReportSummary]
(
	@batch_id		INT
)
AS
BEGIN
SET NOCOUNT ON;

--SELECT ROW_NUMBER() OVER (ORDER BY x.funding_id, x.IsMain DESC, x.trans_type) AS RowNumber, 
SELECT x.trans_type, SUM(x.trans_amount) AS trans_amount
FROM (
	SELECT tt.trans_type, t.trans_amount, 1 AS IsMain
	FROM batches b 
	INNER JOIN transactions t ON b.batch_id = t.batch_id	
	LEFT JOIN transaction_types tt ON t.trans_type_id = tt.trans_type_id
	WHERE b.batch_id = @batch_id AND t.redistributed = 0
	UNION ALL
	SELECT tt.trans_type, d.trans_amount, 0 AS IsMain
	FROM batches b
	INNER JOIN transactions t ON b.batch_id = t.batch_id	
	INNER JOIN transaction_details d ON t.trans_id = d.trans_id
	LEFT JOIN transaction_types tt ON d.trans_type_id = tt.trans_type_id
	WHERE b.batch_id = @batch_id AND t.redistributed = 0
		AND d.trans_type_id NOT IN (8, 9, 11) -- exclude those that are also in main, to avoid duplicates
) x
GROUP BY x.trans_type, x.IsMain
ORDER BY x.IsMain DESC, x.trans_type

END
GO
