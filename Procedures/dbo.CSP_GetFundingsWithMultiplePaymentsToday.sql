SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-01-04
-- Description:	Checks for any contracts that have more than 4 payments to go out today.  Sent to users to determine if they want that or not.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetFundingsWithMultiplePaymentsToday]
	
AS
BEGIN	
IF OBJECT_ID('tempdb..#tempFundings') IS NOT NULL DROP TABLE #tempFundings
CREATE TABLE #tempFundings (payment_id INT NOT NULL, amount MONEY, trans_date DATE, bank_name NVARCHAR (100), account_number VARCHAR (50), routing_number VARCHAR (50), legal_name NVARCHAR (100), 
	contract_number VARCHAR(50), funding_id BIGINT, payoff_amount MONEY, float_amount MONEY, processor VARCHAR (50), retry_level INT, retry_sublevel INT, is_fee BIT, is_refund BIT,
	is_debit BIT, redirect_contract_number NVARCHAR (50), portfolio VARCHAR (50))

INSERT INTO #tempFundings
EXEC CSP_GetPaymentsToSendACH NULL

SELECT f.legal_name, f.contract_number, y.total_amount, y.total_count
FROM fundings f
INNER JOIN (
	-- get a list of fundings that have a retry payment
	SELECT t.funding_id
	FROM #tempFundings t
	WHERE COALESCE(t.retry_level, 0) <> 0 AND t.is_refund = 0
	UNION 
	-- get fundings that have more than one payment in it
	SELECT t.funding_id
	FROM #tempFundings t
	WHERE t.is_refund = 0
	GROUP BY t.funding_id 
	HAVING COUNT(*) >= 2
) x ON f.id = x.funding_id  -- this narrows it down to the ones we care about
INNER JOIN (
	SELECT t.funding_id, SUM(t.amount) AS total_amount, COUNT(*) AS total_count
	FROM #tempFundings t
	WHERE t.is_refund = 0
	GROUP BY t.funding_id
) y ON f.id = y.funding_id  -- this has the total amount for each funding
ORDER BY f.legal_name

IF OBJECT_ID('tempdb..#tempFundings') IS NOT NULL DROP TABLE #tempFundings

END
GO
