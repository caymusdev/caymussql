SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-12-09
-- Description:	scheduled to run once a day to process the partial writeoffs table and create partial writeoffs from schedule
-- =============================================
CREATE PROCEDURE [dbo].[CSP_CheckPartialWriteoffs]

AS
BEGIN
SET NOCOUNT ON;

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())

IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData
CREATE TABLE #tempData (id INT IDENTITY(1, 1), writeoff_id INT)

INSERT INTO #tempData (writeoff_id)
SELECT partial_writeoff_id
FROM partial_writeoffs
WHERE processed IS NULL AND trans_date = @today

DECLARE @cnt INT, @max INT

SELECT @cnt = 1, @max = MAX(id) 
FROM #tempData

IF @max IS NULL SET @max = 0


WHILE @cnt <= @max
	BEGIN
		DECLARE @funding_id INT, @trans_date DATE, @writeoff_amount MONEY, @margin_adjust_amount MONEY, @writeoff_id INT

		SELECT @writeoff_id = writeoff_id
		FROM #tempData 
		WHERE id = @cnt

		SELECT @funding_id = funding_id, @trans_date = trans_date, @writeoff_amount = writeoff_amount, @margin_adjust_amount = margin_adjust_amount
		FROM partial_writeoffs
		WHERE partial_writeoff_id = @writeoff_id

		EXEC dbo.CSP_PartialWriteoffFunding @funding_id, @trans_date, @writeoff_amount, @margin_adjust_amount

		UPDATE partial_writeoffs
		SET processed = GETUTCDATE(), change_user = 'scheduler', change_date = GETUTCDATE()
		WHERE partial_writeoff_id = @writeoff_id
	
		SET @cnt = @cnt + 1
	END

IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData
	
END
GO
