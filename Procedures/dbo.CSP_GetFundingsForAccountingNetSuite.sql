SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:     Ryan Brown
-- Create Date: 2021-03-31
-- Description: Gets Journal Entries for NetSuite Accounting for fundings
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetFundingsForAccountingNetSuite]

AS

SET NOCOUNT ON



IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData
CREATE TABLE #tempData (id int not null identity, funding_id INT)

IF OBJECT_ID('tempdb..#tempAllData') IS NOT NULL DROP TABLE #tempAllData
CREATE TABLE #tempAllData (id int not null identity, trans_id VARCHAR (50), trans_date VARCHAR (50), subsidiary VARCHAR (100), currency VARCHAR (50), postingperiod VARCHAR (50), 
	journalItemLine_memo NVARCHAR (1000), journalItemLine_account VARCHAR (100), journalItemLine_debitAmount MONEY, journalItemLine_creditAmount MONEY, row_id INT)

INSERT INTO #tempData (funding_id)
SELECT f.id
FROM fundings f
WHERE COALESCE(f.funded_date, '2000-01-01') >= '2018-07-01'	AND f.sent_to_acct_2 IS NULL
ORDER BY f.funded_date, f.id

DECLARE @current_id INT, @max_id INT
SELECT @current_id = MIN(id), @max_id = MAX(id) FROM #tempData

WHILE @current_id <= @max_id
	BEGIN
		DECLARE @new_funding_id INT
		SELECT @new_funding_id = funding_id FROM #tempData WHERE id = @current_id
	
		-- rtr
		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT f.contract_number, FORMAT(f.funded_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(f.funded_date, 'MMM yyyy'),
			f.legal_name + ' - Right to Receive', '1101 Right to Receive (RTR)', f.portfolio_value, NULL, 1
		FROM fundings f
		WHERE f.id = @new_funding_id

		-- deferred margin
		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT f.contract_number, FORMAT(f.funded_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(f.funded_date, 'MMM yyyy'),
			f.legal_name + ' - Deferred Margin', '2011 Deferred Revenue', NULL, f.unearned_income, 2
		FROM fundings f
		WHERE f.id = @new_funding_id
		
		-- bad debt calc
		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT f.contract_number, FORMAT(f.funded_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(f.funded_date, 'MMM yyyy'),
			f.legal_name + ' - Bad Debt Expense', '5020 Provision for Doubtful Accounts', ROUND(0.085 * f.purchase_price, 2), NULL, 3
		FROM fundings f
		WHERE f.id = @new_funding_id

		-- bad debt calc
		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT f.contract_number, FORMAT(f.funded_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(f.funded_date, 'MMM yyyy'),
			f.legal_name + ' - Bad Debt Reserve', '1200 RTR Allowance for Bad Debts', NULL, ROUND(0.085 * f.purchase_price, 2),  4
		FROM fundings f
		WHERE f.id = @new_funding_id

		-- net to merchant
		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT f.contract_number, FORMAT(f.funded_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(f.funded_date, 'MMM yyyy'),
			f.legal_name + ' - To Merchant', '1008 Cash - Regions Bank - 1484 - (Funding)', NULL, f.net_to_merchant, 5
		FROM fundings f
		WHERE f.id = @new_funding_id

		-- competitor payoffs
		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT f.contract_number, FORMAT(f.funded_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(f.funded_date, 'MMM yyyy'), 
			f.legal_name + ' - Payoff To ' + d.recipient, '1008 Cash - Regions Bank - 1484 - (Funding)', NULL, 
			CASE WHEN COALESCE(d.discounted_balance, 0) <> 0 THEN d.discounted_balance ELSE d.amount END, 6
		FROM fundings f
		INNER JOIN funding_distributions d ON f.id = d.funding_id AND d.payoff = 1
		WHERE f.id = @new_funding_id
		ORDER BY CASE WHEN d.recipient LIKE '%Caymus%' THEN 1 ELSE 0 END  -- put Caymus at the bottom of the list

		-- commissions
		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT f.contract_number, FORMAT(f.funded_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(f.funded_date, 'MMM yyyy'), 
			f.legal_name + ' - Commission To ' + f.commission_iso, '1008 Cash - Regions Bank - 1484 - (Funding)', NULL, f.commission_amount, 7
		FROM fundings f		
		WHERE f.id = @new_funding_id AND COALESCE(f.commission_iso, '') <> ''

		-- commissions
		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT f.contract_number, FORMAT(f.funded_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(f.funded_date, 'MMM yyyy'), 
			f.legal_name + ' - Commission To ' + f.commission_iso, '5025 Commissions - Upfront', f.commission_amount, NULL, 8
		FROM fundings f		
		WHERE f.id = @new_funding_id AND COALESCE(f.commission_iso, '') <> ''

		-- referral commissions
		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT f.contract_number, FORMAT(f.funded_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(f.funded_date, 'MMM yyyy'), 
			f.legal_name + ' - Commission To ' + f.referral_iso, '1008 Cash - Regions Bank - 1484 - (Funding)', NULL, f.referral_commission, 9
		FROM fundings f		
		WHERE f.id = @new_funding_id AND COALESCE(f.referral_iso, '') <> ''

		-- referral commissions
		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT f.contract_number, FORMAT(f.funded_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(f.funded_date, 'MMM yyyy'), 
			f.legal_name + ' - Commission To ' + f.referral_iso, '5025 Commissions - Upfront', f.referral_commission, NULL, 10
		FROM fundings f		
		WHERE f.id = @new_funding_id AND COALESCE(f.referral_iso, '') <> ''

		-- orig fee to caymus
		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT f.contract_number, FORMAT(f.funded_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(f.funded_date, 'MMM yyyy'),  
			f.legal_name + ' - To Caymus Fnding - Origination Fee', '1008 Cash - Regions Bank - 1484 - (Funding)', NULL, f.orig_fee, 11
		FROM fundings f		
		WHERE f.id = @new_funding_id AND COALESCE(f.orig_fee, 0) <> 0
		/*
		-- orig fee due to caymus
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id)
		SELECT '', 'Fundings', '', NULL, f.contract_number, f.legal_name, '1102', f.legal_name + ' - Origination Fee due to Caymus', NULL, f.orig_fee, NULL, f.id
		FROM fundings f		
		WHERE f.id = @new_funding_id AND COALESCE(f.orig_fee, 0) <> 0
		*/
		-- orig fee income
		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT f.contract_number, FORMAT(f.funded_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(f.funded_date, 'MMM yyyy'),  
			f.legal_name + ' - Origination Fee Income', '4004 Origination Fee Income', NULL, f.orig_fee, 12
		FROM fundings f		
		WHERE f.id = @new_funding_id AND COALESCE(f.orig_fee, 0) <> 0

		-- orig fee due to caymus
		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT f.contract_number, FORMAT(f.funded_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(f.funded_date, 'MMM yyyy'),  
			f.legal_name + ' - Origination Fee Received', '1007 Cash - Regions Business Checking - 1476', f.orig_fee, NULL, 13
		FROM fundings f		
		WHERE f.id = @new_funding_id AND COALESCE(f.orig_fee, 0) <> 0

		-- renewal payoff
		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT f.contract_number, FORMAT(f.funded_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(f.funded_date, 'MMM yyyy'),  
			f.legal_name + ' - Payoff from Renewal Contract' + COALESCE(' - ' + f2.contract_number, ''), '1101 Right to Receive (RTR)', NULL, 
			CASE WHEN COALESCE(d.discounted_balance, 0) <> 0 THEN d.discounted_balance ELSE d.amount END, 14
		FROM fundings f
		INNER JOIN funding_distributions d ON f.id = d.funding_id AND d.payoff = 1
		LEFT JOIN fundings f2 ON f.previous_funding_id = f2.id
		WHERE f.id = @new_funding_id AND d.recipient LIKE '%Caymus%'	
		
		
		-- renewal margin on original
		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT f.contract_number, FORMAT(f.funded_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(f.funded_date, 'MMM yyyy'), 
		f.legal_name + ' - Payoff from Renewal Contract - ' + f2.contract_number, '2011 Deferred Revenue', td.trans_amount, NULL, 17
		FROM fundings f
		INNER JOIN funding_distributions d ON f.id = d.funding_id AND d.payoff = 1
		INNER JOIN fundings f2 ON f.previous_funding_id = f2.id
		INNER JOIN transactions t ON t.funding_id = f2.id AND t.trans_type_id = 5 AND t.comments LIKE '%renewal%'
		INNER JOIN transaction_details td ON t.trans_id = td.trans_id AND td.trans_type_id = 6
		WHERE f.id = @new_funding_id AND d.recipient LIKE '%Caymus%'
		
		-- revenue margin
		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT f.contract_number, FORMAT(f.funded_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(f.funded_date, 'MMM yyyy'), 
			f.legal_name + ' - Revenue on Original Contract - ' + f2.contract_number, '4003 Revenue (Margin)', NULL, td.trans_amount, 18
		FROM fundings f
		INNER JOIN funding_distributions d ON f.id = d.funding_id AND d.payoff = 1
		INNER JOIN fundings f2 ON f.previous_funding_id = f2.id
		INNER JOIN transactions t ON t.funding_id = f2.id AND t.trans_type_id = 5 AND t.comments LIKE '%renewal%'
		INNER JOIN transaction_details td ON t.trans_id = td.trans_id AND td.trans_type_id = 6
		WHERE f.id = @new_funding_id AND d.recipient LIKE '%Caymus%'

		-- in case renewal paid off fees  - one as credit
		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT f.contract_number, FORMAT(f.funded_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(f.funded_date, 'MMM yyyy'), 
			f.legal_name + ' - Fees Paid from Renewal - ' + f2.contract_number, '4001 Fee Income', NULL, td.trans_amount, 19
		FROM fundings f
		INNER JOIN funding_distributions d ON f.id = d.funding_id AND d.payoff = 1
		INNER JOIN fundings f2 ON f.previous_funding_id = f2.id
		INNER JOIN transactions t ON t.funding_id = f2.id AND t.trans_type_id = 5 AND t.comments LIKE '%renewal%'
		INNER JOIN transaction_details td ON t.trans_id = td.trans_id AND td.trans_type_id = 1
		WHERE f.id = @new_funding_id AND d.recipient LIKE '%Caymus%'

		-- in case renewal paid off fees -- one as debit
		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT f.contract_number, FORMAT(f.funded_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(f.funded_date, 'MMM yyyy'),
		f.legal_name + ' - Fees Paid from Renewal - ' + f2.contract_number, '4001 Fee Income', td.trans_amount, NULL, 20
		FROM fundings f
		INNER JOIN funding_distributions d ON f.id = d.funding_id AND d.payoff = 1
		INNER JOIN fundings f2 ON f.previous_funding_id = f2.id
		INNER JOIN transactions t ON t.funding_id = f2.id AND t.trans_type_id = 5 AND t.comments LIKE '%renewal%'
		INNER JOIN transaction_details td ON t.trans_id = td.trans_id AND td.trans_type_id = 1
		WHERE f.id = @new_funding_id AND d.recipient LIKE '%Caymus%'
		
		-- prior contract payoff
		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT f.contract_number, FORMAT(f.funded_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(f.funded_date, 'MMM yyyy'),
			f.legal_name + ' - Prior Contract Payoff to Caymus Funding', '1007 Cash - Regions Business Checking - 1476', 
			CASE WHEN COALESCE(d.discounted_balance, 0) <> 0 THEN d.discounted_balance ELSE d.amount END, NULL, 21
		FROM fundings f
		INNER JOIN funding_distributions d ON f.id = d.funding_id AND d.payoff = 1
		WHERE f.id = @new_funding_id AND d.recipient LIKE '%Caymus%'


		-- 2021-04-22 - For NetSuite, Dartanya asked that discount be back into a single JE with the other funding stuff
		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT f.contract_number, FORMAT(f.funded_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(f.funded_date, 'MMM yyyy'), 
			f2.legal_name + ' - Discount given as part of Renewal Contract - ' + f.contract_number, '1101 Right to Receive (RTR)', NULL, f.discount, 22
		FROM fundings f
		INNER JOIN fundings f2 ON f.previous_funding_id = f2.id
		WHERE f.id = @new_funding_id AND COALESCE(f.discount, 0) <> 0

		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT f.contract_number, FORMAT(f.funded_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(f.funded_date, 'MMM yyyy'), 
			f2.legal_name + ' - Discount given as part of Renewal Contract - ' + f.contract_number, '2011 Deferred Revenue', f.discount, NULL, 23
		FROM fundings f
		INNER JOIN fundings f2 ON f.previous_funding_id = f2.id
		WHERE f.id = @new_funding_id AND COALESCE(f.discount, 0) <> 0

		/*
		-- RKB 2020-01-17 - Dartanya says discounts need to be their own journal entry
		-- renewal discount
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT 'JE', 'Deposits', '', NULL, f2.contract_number, f2.legal_name, '1101', f2.legal_name + ' - Discount given as part of Renewal Contract - ' + f.contract_number, 
			NULL, NULL, f.discount, f.id, 21
		FROM fundings f
		INNER JOIN fundings f2 ON f.previous_funding_id = f2.id
		WHERE f.id = @new_funding_id AND COALESCE(f.discount, 0) <> 0

		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT '', 'Deposits', '', NULL, f2.contract_number, f2.legal_name, '2011', f2.legal_name + ' - Discount given as part of Renewal Contract - ' + f.contract_number, 
			NULL, f.discount, NULL, f.id, 22
		FROM fundings f
		INNER JOIN fundings f2 ON f.previous_funding_id = f2.id
		WHERE f.id = @new_funding_id AND COALESCE(f.discount, 0) <> 0
		*/
	
		SET @current_id = @current_id + 1
	END

	
UPDATE f
SET sent_to_acct_2 = GETUTCDATE(), change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM fundings f 
INNER JOIN #tempAllData t ON f.contract_number = t.trans_id


--  SELECT * FROM #tempAllData

SELECT COALESCE(t.trans_id, '') AS [tranId], COALESCE(CONVERT(VARCHAR (50), t.trans_date), '') AS trandate, COALESCE(t.subsidiary, '') AS [subsidiary], 
	COALESCE(t.currency, '') AS [currency], t.postingperiod, t.journalItemLine_memo, t.journalItemLine_account, 
	COALESCE(CONVERT(VARCHAR (50), t.journalItemLine_debitAmount), '') AS [journalItemLine_debitAmount], 
	COALESCE(CONVERT(VARCHAR (50), t.journalItemLine_creditAmount), '') AS [journalItemLine_creditAmount]
FROM #tempAllData t
ORDER BY t.id

-- get any fundings that do not balance
SELECT t.trans_id, COALESCE(SUM(t.journalItemLine_debitAmount), 0) AS debits, COALESCE(SUM(t.journalItemLine_creditAmount), 0) AS credits, 
	COALESCE(SUM(t.journalItemLine_debitAmount), 0) - COALESCE(SUM(t.journalItemLine_creditAmount), 0) AS diff
FROM #tempAllData t
GROUP BY t.trans_id
HAVING COALESCE(SUM(t.journalItemLine_creditAmount), 0) <> COALESCE(SUM(t.journalItemLine_debitAmount), 0)


IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData
IF OBJECT_ID('tempdb..#tempAllData') IS NOT NULL DROP TABLE #tempAllData

GO
