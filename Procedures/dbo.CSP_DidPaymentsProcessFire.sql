SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-12-11
-- Description:	checks to see if sending payments process started
-- used by an alert in case for some reason the process to send payments does not fire.
-- only will run a few times after payments were supposed to have been sent.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_DidPaymentsProcessFire]
	@processor			VARCHAR (50)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @lastRunTime DATETIME
SELECT @lastRunTime = COALESCE(MAX(create_date), '2020-01-01') -- don't want this to fail the first time.  allow it to record once and then it won't ever be null again
FROM app_logs WITH (NOLOCK)
WHERE module = CASE LOWER(@processor) WHEN 'forte' THEN 'FortePayments' WHEN '53' THEN '53Payments' ELSE 'RegionsPayments' END

IF DATEDIFF(MINUTE, @lastRunTime, GETUTCDATE()) >  120 -- this means it has not run in the last 2 hours
	BEGIN
		SELECT @lastRunTime AS LastRunTime, 0 AS HasRunToday
	END
ELSE
	BEGIN
		SELECT @lastRunTime AS LastRunTime, 1 AS HasRunToday
	END

END

GO
