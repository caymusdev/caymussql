SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-06-28
-- Description:	Updates the payoff amount in the fundings table based on all the transactions.
-- only updates fundings that are in active or writeoff status.
--  NOTE--- *********************** CHANGES HERE MAY ALSO NEED TO BE MADE IN CSP_ApplyPPAndMargin ****************************------------------------
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdatePayoffAmounts]
(
	@change_user			NVARCHAR (100) = 'SYSTEM'
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE @today DATE; SET @today = CONVERT(DATE, DATEADD(HH, -5, GETUTCDATE()))

UPDATE f
SET paid_off_amount = COALESCE(x.paid_off, 0), payoff_amount = f.portfolio_value - COALESCE(x.paid_off, 0), percent_paid = COALESCE(x.paid_off, 0) / f.portfolio_value * 100.00,
	change_date = GETUTCDATE(), change_user = @change_user
FROM fundings f
LEFT JOIN (
	SELECT f.id, SUM(CASE WHEN d.trans_type_id IN (1, 6, 7, 15, 8, 14, 10) THEN d.trans_amount -- 11, 8  -- RKB - 2020-03-02 added settlement to reduce payoff amount
						WHEN d.trans_type_id IN (9) THEN -1 * d.trans_amount 
					  ELSE 0 END) AS paid_off
	FROM fundings f WITH (NOLOCK)
	INNER JOIN transactions t WITH (NOLOCK) ON f.id = t.funding_id
	INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0
		AND f.contract_status IN (1,3,4) AND COALESCE(f.contract_substatus_id, -1) <> 2
		AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 
	GROUP BY f.id
) x ON f.id = x.id
WHERE (COALESCE(paid_off_amount, 0) <> COALESCE(x.paid_off, 0) 
	OR COALESCE(payoff_amount, 0) <> f.portfolio_value - COALESCE(x.paid_off, 0)
	OR COALESCE(percent_paid, 0) <> COALESCE(x.paid_off, 0) / f.portfolio_value * 100.00
	)
	AND f.contract_status IN (1,3,4) AND COALESCE(f.contract_substatus_id, -1) <> 2


UPDATE f
SET f.rtr_out = f.purchase_price - COALESCE(principal.principal_paid, 0) - COALESCE(writeoff.writeoff, 0) + f.portfolio_value - f.purchase_price - COALESCE(margin.margin_paid, 0) 
		- COALESCE(discount.discount, 0) - COALESCE(margin_adjustment.margin_adjustment, 0), -- rtr_out is the same as pp_out + margin_out
	f.pp_out = f.purchase_price - COALESCE(principal.principal_paid, 0) - COALESCE(writeoff.writeoff, 0), 
	f.margin_out = f.portfolio_value - f.purchase_price - COALESCE(margin.margin_paid, 0) - COALESCE(discount.discount, 0) - COALESCE(margin_adjustment.margin_adjustment, 0), 
	f.fees_out = COALESCE(fees.fees_out, 0), change_date = GETUTCDATE(), change_user = @change_user, refund_out = COALESCE(refund.refund_out, 0)
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT t.funding_id, SUM(d.trans_amount) AS margin_paid
	FROM transactions t WITH (NOLOCK)
	INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE d.trans_type_id = 6 AND t.redistributed = 0
		AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 
	GROUP BY t.funding_id
) margin ON f.id = margin.funding_id
LEFT JOIN (
	SELECT t.funding_id, SUM(d.trans_amount) AS principal_paid
	FROM transactions t WITH (NOLOCK)
	INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE d.trans_type_id = 7 AND t.redistributed = 0
		AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 
	GROUP BY t.funding_id
) principal ON f.id = principal.funding_id
LEFT JOIN (
	SELECT t.funding_id, SUM(CASE d.trans_type_id WHEN 9 THEN d.trans_amount ELSE -1 * d.trans_amount END) AS fees_out
	FROM transactions t WITH (NOLOCK)
	INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE d.trans_type_id IN (1,9) AND t.redistributed = 0
		AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 
	GROUP BY t.funding_id
) fees ON f.id = fees.funding_id
LEFT JOIN (
	SELECT t.funding_id, SUM(d.trans_amount) AS writeoff
	FROM transactions t WITH (NOLOCK)
	INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id		
	WHERE d.trans_type_id IN (11) AND t.redistributed = 0
		AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 
	GROUP BY t.funding_id
) writeoff ON f.id = writeoff.funding_id
LEFT JOIN (
	SELECT t.funding_id, SUM(d.trans_amount) AS discount
	FROM transactions t WITH (NOLOCK)
	INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id		
	WHERE d.trans_type_id IN (8) AND t.redistributed = 0
		AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 
	GROUP BY t.funding_id
) discount ON f.id = discount.funding_id
LEFT JOIN (
	SELECT t.funding_id, SUM(d.trans_amount) AS margin_adjustment
	FROM transactions t WITH (NOLOCK)
	INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id		
	WHERE d.trans_type_id IN (17) AND t.redistributed = 0
		AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 
	GROUP BY t.funding_id
) margin_adjustment ON f.id = margin_adjustment.funding_id
LEFT JOIN (
	SELECT t.funding_id, SUM(t.trans_amount) AS refund_out
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id		
	WHERE t.trans_type_id IN (22,23, 25) AND t.redistributed = 0  -- 24 is not included because 23 is money leaving the account, 24 is money going on to an account
		AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 
	GROUP BY t.funding_id
) refund ON f.id = refund.funding_id
WHERE f.contract_status IN (1,3,4) AND COALESCE(f.contract_substatus_id, -1) <> 2
	AND (
		COALESCE(f.rtr_out, -999) <> f.purchase_price - COALESCE(principal.principal_paid, 0) - COALESCE(writeoff.writeoff, 0) + f.portfolio_value - f.purchase_price - COALESCE(margin.margin_paid, 0) 
		- COALESCE(discount.discount, 0)- COALESCE(margin_adjustment.margin_adjustment, 0)
	OR COALESCE(f.pp_out,-999) <> f.purchase_price - COALESCE(principal.principal_paid, 0) - COALESCE(writeoff.writeoff, 0)
	OR COALESCE(f.margin_out, -999) <> f.portfolio_value - f.purchase_price - COALESCE(margin.margin_paid, 0) - COALESCE(discount.discount, 0) - COALESCE(margin_adjustment.margin_adjustment, 0)
	OR COALESCE(f.fees_out, -999) <> COALESCE(fees.fees_out, 999)
	OR COALESCE(f.refund_out, -999) <> COALESCE(refund.refund_out, 999)
	)


-- RKB  2019-10-17
-- completed contracts may still have an outstanding balance and refund_out needs to be updated 
UPDATE f
SET change_date = GETUTCDATE(), change_user = @change_user, refund_out = COALESCE(refund.refund_out, 0)
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT t.funding_id, SUM(t.trans_amount) AS refund_out
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id		
	WHERE t.trans_type_id IN (22,23, 25) AND t.redistributed = 0  -- 24 is not included because 23 is money leaving the account, 24 is money going on to an account
		AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 
	GROUP BY t.funding_id
) refund ON f.id = refund.funding_id
WHERE f.contract_status IN (2) 
	AND COALESCE(f.refund_out, -999) <> COALESCE(refund.refund_out, 999)
	AND COALESCE(f.refund_out, 0) <> 0	




-- calc turn for active contracts
UPDATE fundings
SET --calc_turn = (f.payoff_amount / CASE WHEN f.frequency = 'Weekly' THEN f.weekly_payment / 5 ELSE f.weekday_payment END) / 21.0,
	--est_months_left = ((f.payoff_amount - COALESCE(f.float_amount, 0)) / CASE WHEN f.frequency = 'Weekly' THEN f.weekly_payment / 5 ELSE f.weekday_payment END) / 21.0,
	est_months_left = DATEDIFF(D, @today, x.final_date)/30.4,
	--calc_turn = (((f.payoff_amount - COALESCE(f.float_amount, 0)) / CASE WHEN f.frequency = 'Weekly' THEN f.weekly_payment / 5 ELSE f.weekday_payment END) / 21.0) 
--		+ (DATEDIFF(D, funded_date, @today) /30.4),
	calc_turn = DATEDIFF(D, funded_date, x.final_date)/30.4,
	change_date = GETUTCDATE(), change_user = @change_user
FROM fundings f WITH (NOLOCK)
INNER JOIN (
	SELECT MAX(payment_date) AS final_date, funding_id
	FROM payment_forecasts WITH (NOLOCK)
	GROUP BY funding_id
) x ON f.id = x.funding_id
WHERE --COALESCE(f.frequency, '') <> '' AND 
	--f.contract_status IN (1,3,4) AND COALESCE(f.contract_substatus_id, -1) <> 2
	f.contract_status IN (1) AND COALESCE(f.contract_substatus_id, -1) <> 2
	AND (
		--COALESCE(calc_turn, -999) <> (((f.payoff_amount - COALESCE(f.float_amount, 0)) / CASE WHEN f.frequency = 'Weekly' THEN f.weekly_payment / 5 ELSE f.weekday_payment END) / 21.0) 
	--		+ (DATEDIFF(D, funded_date, @today) /30.4)
	--	OR COALESCE(est_months_left, -999) <> ((f.payoff_amount - COALESCE(f.float_amount, 0)) / CASE WHEN f.frequency = 'Weekly' THEN f.weekly_payment / 5 ELSE f.weekday_payment END) / 21.0 
		COALESCE(est_months_left, -9999) <> DATEDIFF(D, @today, x.final_date)/30.4
		OR
		COALESCE(calc_turn, -999) <> DATEDIFF(D, funded_date, x.final_date)/30.4
	)

	-- calc turn for non active contracts
UPDATE fundings
SET est_months_left = (f.payoff_amount / CASE WHEN f.frequency = 'Weekly' THEN f.weekly_payment / 5 ELSE f.weekday_payment END) / 21.0,
	--est_months_left = DATEDIFF(D, @today, x.final_date)/30.4,
	calc_turn = ((f.payoff_amount / CASE WHEN f.frequency = 'Weekly' THEN f.weekly_payment / 5 ELSE f.weekday_payment END) / 21.0) 
		+ (DATEDIFF(D, funded_date, @today) /30.4),
	--calc_turn = DATEDIFF(D, funded_date, x.final_date)/30.4,
	change_date = GETUTCDATE(), change_user = @change_user
FROM fundings f WITH (NOLOCK)
INNER JOIN (
	SELECT MAX(payment_date) AS final_date, funding_id
	FROM payment_forecasts WITH (NOLOCK)
	GROUP BY funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.frequency, '') <> '' AND 
	f.contract_status IN (3,4) AND COALESCE(f.contract_substatus_id, -1) <> 2
	AND (
		COALESCE(calc_turn, -999) <> ((f.payoff_amount / CASE WHEN f.frequency = 'Weekly' THEN f.weekly_payment / 5 ELSE f.weekday_payment END) / 21.0) 
			+ (DATEDIFF(D, funded_date, @today) /30.4)
		OR COALESCE(est_months_left, -999) <> (f.payoff_amount / CASE WHEN f.frequency = 'Weekly' THEN f.weekly_payment / 5 ELSE f.weekday_payment END) / 21.0 		
	)

---- now need to update the paid_calc_turn which is based on how much they have paid off so far, not what their payment plan is.
--UPDATE fundings
--SET	paid_calc_turn = CASE 
--	WHEN (f.portfolio_value * DATEDIFF(D, funded_date, @today))/(f.paid_off_amount+0.001) / 30.4 > 60 THEN 60
--		ELSE (f.portfolio_value * DATEDIFF(D, funded_date, @today))/(f.paid_off_amount+0.001) / 30.4 END,
--	change_date = GETUTCDATE(), change_user = @change_user
--FROM fundings f WITH (NOLOCK)
--WHERE f.contract_status IN (1,3,4) AND COALESCE(f.contract_substatus_id, -1) <> 2
--	AND (
--		COALESCE(paid_calc_turn, -999) <> CASE 
--	WHEN (f.portfolio_value * DATEDIFF(D, funded_date, @today))/(f.paid_off_amount+0.001) / 30.4 > 60 THEN 60
--		ELSE (f.portfolio_value * DATEDIFF(D, funded_date, @today))/(f.paid_off_amount+0.001) / 30.4 END 
--	)


UPDATE fundings
SET last_payment_date = x.last_payment_date, change_date = GETUTCDATE(), change_user = @change_user
FROM fundings f WITH (NOLOCK)
INNER JOIN (
	SELECT t.funding_id, MAX(t.trans_date) AS last_payment_date
	FROM transactions t	 WITH (NOLOCK)	
	WHERE t.trans_type_id IN (2, 3, 4, 5, 19) AND t.trans_amount <> 0
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE f.contract_status IN (1,3,4) AND COALESCE(f.contract_substatus_id, -1) <> 2
	AND COALESCE(f.last_payment_date, '2000-01-01') != COALESCE(x.last_payment_date, '2000-01-01')

UPDATE fundings
SET actual_turn = DATEDIFF(D, funded_date, COALESCE(completed_date, last_payment_date)) /30.4, change_date = GETUTCDATE(), change_user = @change_user
WHERE (contract_status = 2 
		OR (contract_status = 4 AND COALESCE(contract_substatus_id, -1) = 2)
		)
	AND actual_turn IS NULL
	AND COALESCE(actual_turn, -9999999) != DATEDIFF(D, funded_date, COALESCE(completed_date, last_payment_date)) /30.4

UPDATE fundings
SET percent_performance = (estimated_turn - calc_turn) / estimated_turn * 100.00, change_date = GETUTCDATE(), change_user = @change_user 
WHERE calc_turn IS NOT NULL AND estimated_turn IS NOT NULL
	AND COALESCE(percent_performance, -9999999) != (estimated_turn - calc_turn) / estimated_turn


-- now do the float
UPDATE f 
SET float_amount = COALESCE(x.total_float, 0) + COALESCE(y.total_float, 0)
FROM fundings f WITH (NOLOCK)
-- get float from batches
LEFT JOIN (
	SELECT f2.id, SUM(CASE WHEN t.trans_type_id IN (9, 14, 17) THEN -1* t.trans_amount ELSE t.trans_amount END) AS total_float
	FROM fundings f2 WITH (NOLOCK)
	JOIN transactions t WITH (NOLOCK) ON f2.id = t.funding_id
	INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE b.batch_status IN ('Processed') AND b.batch_type NOT IN ('ForteAPI') -- if it IS ForteAPI, we've already distributed the money so it is not in float.
	GROUP BY f2.id
) x ON f.id = x.id
LEFT JOIN (
-- now get float from forte trans
	SELECT SUM(t.authorization_amount) AS total_float, f.id
	FROM forte_trans t WITH (NOLOCK)
	INNER JOIN fundings f WITH (NOLOCK) ON t.customer_id = f.contract_number
	LEFT JOIN forte_trans t2 WITH (NOLOCK) ON t.transaction_id = t2.transaction_id AND t2.status <> 'settling'
	WHERE t.status = 'settling' AND t2.forte_trans_id IS NULL -- this makes sure it does not have a matching record (which would mean there is an update to the settling)
	GROUP BY f.id
) y ON f.id = y.id
WHERE (f.float_amount <> (COALESCE(x.total_float, 0) + COALESCE(y.total_float, 0))) 
	OR f.float_amount IS NULL



END
GO
