SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-11-11
-- Description:	Saves a long text that is sent in pieces into a temp table
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsTempText]
(
	@concat_ref					NVARCHAR (200),
	@concat_total				INT, 
	@concat_part				INT,
	@to							VARCHAR (50),
	@from						VARCHAR (50), 
	@received					DATETIME,
	@text_message				NVARCHAR (MAX),
	@message_id					NVARCHAR (200),
	@text_type					VARCHAR (50),
	@text_data					NVARCHAR(MAX)
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @newTextID INT
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())


-- if there is a problem in receiving they might get sent several times so just make sure NOT EXISTS
INSERT INTO temp_texts(concat_ref, concat_total, concat_part, text_id, [to], [from], received, text_message, text_type, text_data, message_id, create_date, change_date, change_user, create_user)
SELECT @concat_ref, @concat_total, @concat_part, NULL, @to, @from, @received, @text_message, @text_type, @text_data, @message_id, GETUTCDATE(), GETUTCDATE(), 'System', 'System'
WHERE NOT EXISTS (SELECT 1 FROM temp_texts WHERE concat_ref = @concat_ref AND concat_part = @concat_part)

-- now, if all parts have been received insert into the actual text table
-- i don't care if we have the proper individual numbers.  if vonage says there are 10 pieces I don't care that I get 1, 2, 3, .. 10, I just care that I get 10 total
IF (SELECT COUNT(*) FROM temp_texts WHERE concat_ref = @concat_ref) = @concat_total
	BEGIN
		DECLARE @affiliate_id INT
		SELECT @affiliate_id = affiliate_id FROM dbo.[BF_GetAffiliateIDFromSMSNumber](@from)

		-- from BSK_GetAssignedCollectorForAffiliate
		DECLARE @user_email NVARCHAR (100)
		SELECT @user_email = u.email
		FROM collector_cases cc
		INNER JOIN cases c ON cc.case_id = c.case_id AND c.active = 1
		INNER JOIN users u ON cc.collector_user_id = u.user_id
		WHERE cc.start_date <= @today AND COALESCE(cc.end_date, '2050-05-09') >= @today AND c.affiliate_id = @affiliate_id

		-- need to get all of the combined text_message and text_datas
		DECLARE @combined_text_message NVARCHAR (MAX), @combined_text_data NVARCHAR (MAX)

		SELECT @combined_text_message = COALESCE(@combined_text_message + ' ', ' ') + x.text_message
		FROM (
			SELECT TOP 100 PERCENT text_message
			FROM temp_texts t
			WHERE t.concat_ref = @concat_ref
			ORDER BY t.concat_part
		) x

		SELECT @combined_text_data = COALESCE(@combined_text_data + ' ', ' ') + x.text_data
		FROM (
			SELECT TOP 100 PERCENT text_data
			FROM temp_texts t
			WHERE t.concat_ref = @concat_ref
			ORDER BY t.concat_part
		) x

		EXEC dbo.BSK_InsText @affiliate_id, NULL, NULL, @to, @from, @user_email, NULL, @received, @combined_text_message, @message_id, @text_type, 
			@combined_text_data, 1, @new_text_id = @newTextID OUTPUT

		IF @newTextID IS NOT NULL
			BEGIN
				UPDATE temp_texts SET text_id = @newTextID WHERE concat_ref = @concat_ref AND text_id IS NULL
			END
	END
END

GO
