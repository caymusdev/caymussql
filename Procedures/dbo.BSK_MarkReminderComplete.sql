SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:     Ryan Brown
-- Create Date: 2021-04-19
-- Description: Marks a reminder as complete
-- =============================================
CREATE PROCEDURE [dbo].[BSK_MarkReminderComplete]
(
	@alert_id				VARCHAR (50),
	@users_email			NVARCHAR (100)
)
AS

SET NOCOUNT ON;
DECLARE @id INT; SET @id = CONVERT(INT, REPLACE(REPLACE(@alert_id, 'N', ''), 'R', ''))

-- right now, only does reminders.
IF LEFT(@alert_id, 1) = 'R'
	BEGIN
		UPDATE reminders 
		SET completed = GETUTCDATE(), change_date = GETUTCDATE(), change_user = @users_email
		WHERE reminder_id = @id
	END

GO
