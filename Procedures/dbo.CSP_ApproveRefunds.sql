SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-04-20
-- Description:	Accepts a delimited list of funding ids to approve the refunds to be sent back
-- =============================================
CREATE PROCEDURE [dbo].[CSP_ApproveRefunds]
(	
	@funding_ids		NVARCHAR (4000), -- could be a delimited list of funding ids
	@user_id			NVARCHAR (100)
)
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SELECT @tomorrow = DATEADD(DD, CASE DATEPART(WEEKDAY, @today) WHEN 6 THEN 3 WHEN 7 THEN 2 ELSE 1 END, @today)

IF OBJECT_ID('tempdb..#tempIDs') IS NOT NULL DROP TABLE #tempIDs
CREATE TABLE #tempIDs (id int not null identity, funding_id BIGINT)

INSERT INTO #tempIDs(funding_id)
SELECT a.value
FROM string_split(@funding_ids, '|') a


-- create credit payment for each funding
INSERT INTO payments (payment_schedule_id, amount, trans_date, transaction_id, approved_flag, processed_date, change_date, change_user, previous_transaction_id, orig_transaction_id, 
	is_fee, retry_level, retry_sublevel, funding_id, complete, batch_id, retry_complete, processor, is_debit, is_refund, funding_payment_date_id)
SELECT NULL, ABS(f.refund_out), @tomorrow, NULL, 1, NULL, GETUTCDATE(), @user_id, NULL, NULL, 0, 0, 0, f.id, NULL, NULL, NULL, NULL, 0, 1, x.funding_payment_date_id
FROM #tempIDs t
INNER JOIN fundings f ON t.funding_id = f.id
--OUTER APPLY (
--	SELECT TOP 1 fps.funding_id, fps.payment_schedule_id
--	FROM funding_payment_schedules fps
--	INNER JOIN payment_schedules ps ON fps.payment_schedule_id = ps.payment_schedule_id
--	WHERE ps.active = 1 AND f.id = fps.funding_id
--	ORDER BY COALESCE(ps.end_date, '2028-12-31') DESC
--) x
OUTER APPLY (
	SELECT TOP 1 d.funding_id, d.id AS funding_payment_date_id
	FROM funding_payment_dates d
	WHERE (d.active = 1 OR d.original = 1)  -- if none are active we can get one of the originals
		AND f.id = d.funding_id AND d.merchant_bank_account_id IS NOT NULL
	ORDER BY d.payment_date DESC
) x
--LEFT JOIN payment_schedules ps ON x.payment_schedule_id = ps.payment_schedule_id

IF OBJECT_ID('tempdb..#tempIDs') IS NOT NULL DROP TABLE #tempIDs

END
GO
