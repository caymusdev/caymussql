SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-05-31
-- Description:	Returns the forte batches that are in Staging status.  Then c# loops through results and processes
-- into the transactions tables.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetForteStagingBatches]

AS
BEGIN
SET NOCOUNT ON;

-- first look for duplicates
UPDATE batches
SET batch_status = 'Duplicate', change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM batches b
WHERE b.batch_status = 'Staging' AND b.batch_type IN ('Forte', 'ForteAPI')
	AND EXISTS (SELECT 1 FROM batches b2 WHERE CONVERT(DATE, b.batch_date) = CONVERT(DATE, b2.batch_date) AND b.batch_id <> b2.batch_id AND b2.batch_type LIKE 'Forte%'
				AND b2.batch_status IN ('Loaded', 'Processing', 'Processed', 'Closed', 'Staging', 'Settled')
			)
	

SELECT b.batch_id
FROM batches b
WHERE b.batch_status = 'Staging' AND b.batch_type IN ('Forte', 'ForteAPI')
ORDER BY b.batch_id

END
GO
