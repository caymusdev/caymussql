SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-02-07
-- Description:	Takes an id for the worker table and then creates the changes in the actual table
-- =============================================
CREATE PROCEDURE [dbo].[CSP_RejectPaymentDateChange]
(
	@worker_id			INT,
	@approver			NVARCHAR (100),
	@reject_reason		NVARCHAR (500)
)
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SET @tomorrow = dbo.CF_GetTomorrow(@today)
DECLARE @yesterday DATE; SELECT @yesterday = DATEADD(DD, CASE DATEPART(WEEKDAY, @today) WHEN 1 THEN -2 WHEN 2 THEN -3 ELSE -1 END, @today)

DECLARE @original_user NVARCHAR (100), @funding_id BIGINT

-- first, just make sure it hasn't already been approved or rejected
----
--IF (SELECT approved FROM funding_payment_dates_work WHERE id = @worker_id) = 1
--	BEGIN
--		RAISERROR ('Already approved.', 16, 1)	
--	END
--ELSE IF (SELECT rejected_date FROM funding_payment_dates_work WHERE id = @worker_id) IS NOT NULL
--	BEGIN
--		RAISERROR ('Already rejected.', 16, 1)	
--	END
--ELSE 
--	BEGIN
		SELECT @original_user = w.userid, @funding_id = w.funding_id
		FROM funding_payment_dates_work w
		WHERE w.id = @worker_id

		SELECT f.legal_name, f.contract_number, w.payment_date, 
			CASE WHEN w.revised_original = 1 THEN 'Revise Original' ELSE 
				CASE WHEN w.funding_payment_id IS NULL THEN 'New' WHEN w.active = 0 THEN 'Delete' ELSE 'Change' END END AS change_type, 
			w.userid
		FROM funding_payment_dates_work w
		INNER JOIN fundings f ON w.funding_id = f.id
		WHERE w.id = @worker_id


		-- need to check if this is revising an original payment schedule
		IF (SELECT revised_original FROM funding_payment_dates_work WHERE id = @worker_id) = 1
			BEGIN
				UPDATE funding_payment_dates_work 
				SET approved = 0, rejector = @approver, rejected_date = GETUTCDATE(), change_user = @approver, change_date = GETUTCDATE(), ready_for_approval = 0, 
					active = 0, reject_reason = @reject_reason
				WHERE funding_id = @funding_id AND userid = @original_user AND revised_original = 1 AND approved_date IS NULL AND rejected_date IS NULL
			END
		ELSE
			BEGIN
				UPDATE funding_payment_dates_work 
				SET approved = 0, rejector = @approver, rejected_date = GETUTCDATE(), change_user = @approver, change_date = GETUTCDATE(), ready_for_approval = 0, 
					active = 0, reject_reason = @reject_reason
				WHERE id = @worker_id
			END
--	END

END
GO
