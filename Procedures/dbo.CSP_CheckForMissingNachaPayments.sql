SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-02-28
-- Description:	Checks for missing payments for a nacha processor.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_CheckForMissingNachaPayments] 
(		
	@processor				VARCHAR (50)
)
AS
BEGIN
SET NOCOUNT ON;

SELECT p.payment_id
FROM payments p
INNER JOIN transactions t ON CONVERT(VARCHAR(50), p.payment_id) = t.transaction_id AND t.trans_type_id = 18 AND t.record_type = 'payments'
LEFT JOIN transactions t2 ON CONVERT(VARCHAR(50), p.payment_id) = t2.transaction_id AND t2.trans_type_id IN (3, 14, 21) AND t2.record_type = 'nacha'
WHERE t2.trans_id IS NULL AND p.processor = @processor

END
GO
