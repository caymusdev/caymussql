SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-05-23
-- Description:	Gets a batch "report"
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetBatchReport]
(
	@batch_id		INT
) 
AS
BEGIN
SET NOCOUNT ON;

--SELECT ROW_NUMBER() OVER (ORDER BY x.funding_id, x.IsMain DESC, x.trans_type) AS RowNumber, 
SELECT x.batch_id, x.batch_type, x.batch_status, x.funding_id, x.legal_name, x.trans_date, x.trans_type, x.trans_amount, x.id, 
	COALESCE(p.trans_date, ft.origination_date, x.trans_date) AS origination_date,
	x.contract_number--, p.trans_date, ft.origination_date
FROM (
	SELECT b.batch_id, b.batch_type, b.batch_status, f.funding_id, f.legal_name, t.trans_date, tt.trans_type, t.trans_amount, 1 AS IsMain,
		f.id, t.record_type, t.record_id, f.contract_number
	FROM batches b 
	INNER JOIN transactions t ON b.batch_id = t.batch_id
	INNER JOIN fundings f ON t.funding_id = f.id
	LEFT JOIN transaction_types tt ON t.trans_type_id = tt.trans_type_id AND t.redistributed = 0
	WHERE b.batch_id = @batch_id
	UNION ALl
	SELECT b.batch_id, b.batch_type, b.batch_status, f.funding_id, f.legal_name, t.trans_date, tt.trans_type, d.trans_amount, 0 AS IsMain,
		f.id, t.record_type, t.record_id, f.contract_number
	FROM batches b
	INNER JOIN transactions t ON b.batch_id = t.batch_id
	INNER JOIN fundings f ON t.funding_id = f.id
	INNER JOIN transaction_details d ON t.trans_id = d.trans_id
	LEFT JOIN transaction_types tt ON d.trans_type_id = tt.trans_type_id
	WHERE b.batch_id = @batch_id AND t.redistributed = 0
		AND d.trans_type_id NOT IN (8, 9, 11, 17) -- exclude those that are also in main, to avoid duplicates
) x
LEFT JOIN forte_settlements fs ON x.record_type = 'forte_settlements' AND x.record_id = fs.forte_settle_id
LEFT JOIN forte_trans ft ON fs.transaction_id = ft.transaction_id AND ft.status = 'settling'
LEFT JOIN payments p ON ft.transaction_id = p.transaction_id
ORDER BY x.legal_name, x.IsMain DESC, x.trans_type, x.trans_amount

END
GO
