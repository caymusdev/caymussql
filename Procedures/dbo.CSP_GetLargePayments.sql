SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2022-04-07
-- Description: Gets a list of large payments for someone to review
-- =============================================
CREATE PROCEDURE dbo.CSP_GetLargePayments

AS
BEGIN
SET NOCOUNT ON

SELECT t.trans_id, t.trans_date, f.contract_number, f.legal_name, t.trans_amount, 
	CASE f.frequency WHEN 'Daily' THEN f.weekday_payment ELSE f.weekly_payment END AS payment, 
	CONVERT(DECIMAL(10,2), (t.trans_amount / (CASE f.frequency WHEN 'Daily' THEN f.weekday_payment ELSE f.weekly_payment END)) * 100.00) AS percent_higher,
	tt.trans_type
FROM transactions t
INNER JOIN fundings f ON t.funding_id = f.id
INNER JOIN transaction_types tt ON t.trans_type_id = tt.trans_type_id
WHERE DATEDIFF(DD, t.trans_date, GETUTCDATE()) < 10
	AND t.trans_type_id IN (2, 3, 4, 5, 19)
	AND t.trans_amount >= (CASE f.frequency WHEN 'Daily' THEN f.weekday_payment ELSE f.weekly_payment END * 5)
	AND t.comments NOT LIKE 'Wire payoff due to renewal of%'
ORDER BY t.trans_date DESC, f.legal_name



END
GO
