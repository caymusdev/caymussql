SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-11-05
-- Description:	Returns the assigned collector for a given affiliate.  Used in notifications when needing to see who to notify
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetAssignedCollectorForAffiliate]
(
	@affiliate_id				INT
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())

SELECT u.user_id, u.email
FROM collector_cases cc
INNER JOIN cases c ON cc.case_id = c.case_id AND c.active = 1
INNER JOIN users u ON cc.collector_user_id = u.user_id
WHERE cc.start_date <= @today AND COALESCE(cc.end_date, '2050-05-09') >= @today
	AND c.affiliate_id = @affiliate_id
	
END

GO
