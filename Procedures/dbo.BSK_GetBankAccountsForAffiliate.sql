SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:     Ryan Brown
-- Create Date: 2020-12-11
-- Description: Gets all bank accounts for an affiliate
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetBankAccountsForAffiliate]
(
	@affiliate_id				INT	
)
AS

SET NOCOUNT ON

SELECT DISTINCT ba.bank_name, ba.routing_number, ba.account_number, ba.account_status, ba.merchant_bank_account_id, 
	COALESCE(ba.nickname, COALESCE(ba.bank_name, '') + ': ' + COALESCE(ba.routing_number, '') + ' / ' + COALESCE(ba.account_number, '')) 
		+ CASE WHEN ba.account_number = f.receivables_account_nbr AND ba.routing_number = f.receivables_aba THEN ' (Main)' ELSE '' END AS nickname,
		CASE WHEN ba.account_number = f.receivables_account_nbr AND ba.routing_number = f.receivables_aba THEN 1 ELSE 0 END AS is_main_account
FROM merchant_bank_accounts ba
INNER JOIN merchants m ON ba.merchant_id = m.merchant_id OR (ba.merchant_id = 1 AND ba.affiliate_id = @affiliate_id)
INNER JOIN fundings f ON m.merchant_id = f.merchant_id
WHERE f.affiliate_id = @affiliate_id
ORDER BY nickname, ba.merchant_bank_account_id

GO
