SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-04-23
-- Description: If user HAS an entry it means that is ALL they have access to.  No defined permissions means you have access to everything
-- =============================================
CREATE PROCEDURE [dbo].[CSP_DoesUserHaveAccess]
(	
	@sec_object_name		VARCHAR (50),
	@sec_object_type		VARCHAR (50),
	@userid					NVARCHAR (100)
)
AS
BEGIN	
SET NOCOUNT ON;

--user either needs to have the specific permission or NO permission at all specified.
-- RKB - 2019-12-11 -- adding flag for read-only

IF EXISTS(
	SELECT 1
	FROM sec_user_objects u
	INNER JOIN sec_objects o ON u.sec_object_id = o.sec_object_id
	WHERE u.userid = @userid AND o.sec_object_name = @sec_object_name AND o.sec_object_type = @sec_object_type
)
	BEGIN
		-- there is a security record for this page and user
		SELECT u.read_only AS [ReadOnly], CONVERT(BIT, 1) AS HasPermission
		FROM sec_user_objects u
		INNER JOIN sec_objects o ON u.sec_object_id = o.sec_object_id
		WHERE u.userid = @userid AND o.sec_object_name = @sec_object_name AND o.sec_object_type = @sec_object_type
	END
ELSE IF NOT EXISTS (SELECT 1 FROM sec_user_objects WHERE userid = @userid)
	BEGIN 
		SELECT CONVERT(BIT, 0) AS [ReadOnly], CONVERT(BIT, 1) AS HasPermission
	END
ELSE
	BEGIN
		SELECT CONVERT(BIT, 1) AS [ReadOnly], CONVERT(BIT, 0) AS HasPermission
	END
END
GO
