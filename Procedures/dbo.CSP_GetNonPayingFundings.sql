SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-10-22
-- Description:	Gets a list of Forte customers that have not had a payment recently.  Probably still need to tweak this logic
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetNonPayingFundings]
AS
BEGIN


SET NOCOUNT ON;
/*
SELECT last_payment_date, f.funded_date, frequency, float_amount, first_payment_date,  
	DATEADD(D, CASE f.frequency WHEN 'Daily' THEN 1 ELSE 7 END, COALESCE(last_payment_date, funded_date)) AS expected_next_payment,
	f.id, f.contract_number, f.legal_name, f.payoff_amount, f.contract_status
FROM fundings f
WHERE processor = 'Forte'
	AND COALESCE(last_payment_date, funded_date)< DATEADD(D, CASE frequency WHEN 'Daily' THEN -10 ELSE -16 END, GETUTCDATE())
	AND DATEADD(D, 10, COALESCE(f.first_payment_date, DATEADD(D, 1, f.funded_date))) < GETUTCDATE() -- add 10 days to first payment date to account for initial float of forte	
	AND f.contract_status = 1
	AND COALESCE(f.on_hold, 0) = 0
ORDER BY f.last_payment_date
*/
-- RKB - 2018-11-26 Simplifying the logic to just look at received amounts
SELECT last_payment_date, f.funded_date, frequency, float_amount, first_payment_date,  
	DATEADD(D, CASE f.frequency WHEN 'Daily' THEN 1 ELSE 7 END, COALESCE(last_payment_date, funded_date)) AS expected_next_payment,
	f.id, f.contract_number, f.legal_name, f.payoff_amount, f.contract_status, f.processor
FROM fundings f
WHERE f.processor IS NOT NULL -- = 'Forte'
	AND 
	DATEADD(D, 10, COALESCE(f.first_payment_date, DATEADD(D, 1, f.funded_date))) < GETUTCDATE() -- add 10 days to first payment date to account for initial float of forte	
	AND f.contract_status = 1
	AND COALESCE(f.on_hold, 0) = 0
	AND COALESCE((SELECT MAX(trans_date) FROM transactions t WHERE t.funding_id = f.id AND t.trans_type_id IN (2,3,4,5)), '2000-01-01') <
		DATEADD(D, CASE frequency WHEN 'Daily' THEN -7 ELSE -11 END, GETUTCDATE())
	AND COALESCE(f.float_amount, 0) = 0
ORDER BY f.legal_name 
END
GO
