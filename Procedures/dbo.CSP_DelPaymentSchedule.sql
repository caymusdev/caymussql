SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-10-12
-- Description:	Updates a payment schedule
-- =============================================
CREATE PROCEDURE [dbo].[CSP_DelPaymentSchedule]
(
	@payment_schedule_id	INT,
	@userid					NVARCHAR (100),
	@approved_by			NVARCHAR (100) = NULL,
	@change_id				INT = NULL
)
AS
BEGIN	
SET NOCOUNT ON;

-- First need to check and see if this user needs approval for editing/creating payment schedules
DECLARE @needs_approval BIT; SET @needs_approval = 0

-- VARS
DECLARE @frequency VARCHAR (50), @amount MONEY, @start_date DATE, @end_date DATE, @active BIT, @bank_name NVARCHAR (50), @routing_number VARCHAR (50)
DECLARE @account_number	VARCHAR (50), @funding_id BIGINT, @cc_percentage NUMERIC (10,4), @payment_method VARCHAR (50), @payment_processor VARCHAR (50), @comments NVARCHAR (4000)
SELECT @cc_percentage = NULL
-- end VARS

SELECT @payment_schedule_id = c.payment_schedule_id, @frequency = c.frequency, @amount = c.amount, @start_date = c.start_date, @end_date = c.end_date, 
	@active = c.active, @bank_name = c.bank_name, @routing_number = c.routing_number, @account_number = c.account_number, 
	@payment_method = c.payment_method, @payment_processor = c.payment_processor, @comments = c.comments
FROM payment_schedules c
WHERE c.payment_schedule_id = @payment_schedule_id

IF @approved_by IS NULL AND EXISTS (SELECT 1 FROM sec_payment_schedules s WHERE s.submitter_user_id = @userid)
	BEGIN
		SET @needs_approval = 1

		SELECT @funding_id = funding_id FROM funding_payment_schedules WHERE payment_schedule_id = @payment_schedule_id


		-- if there is already an unapproved queued record then assume update otherwise insert
		IF EXISTS(SELECT 1 FROM payment_schedule_changes WHERE payment_schedule_id = @payment_schedule_id AND approved_by IS NULL AND change_type = 'Delete')
			-- do an update
			BEGIN
				-- do nothing.
				PRINT 'already has been queued for deletion.'
			END
		ELSE 
			BEGIN
				INSERT INTO payment_schedule_changes (change_type, payment_schedule_id, frequency, amount, start_date, end_date, active, bank_name, routing_number, account_number, userid,
					funding_id, approved_by, comments, payment_method, payment_processor)
				VALUES ('Delete', @payment_schedule_id, @frequency, @amount, @start_date, @end_date, @active, @bank_name, @routing_number, @account_number, @userid, @funding_id, @approved_by,
					@comments,@payment_method, @payment_processor)
			END

		SELECT 1 AS NeedsApproval
	END
ELSE IF @approved_by IS NULL
	BEGIN
		-- so, there is NOT a security record which means they have no need to get approved separaretly.
		SET @approved_by = @userid
	END


IF @needs_approval = 0
	BEGIN
		-- if this schedule has any payments, then they cannot delete it.  We'll mark it inactive
		IF EXISTS (SELECT 1 FROM payments p WHERE p.payment_schedule_id = @payment_schedule_id AND approved_flag = 1 AND processed_date IS NOT NULL AND deleted = 0 AND waived = 0)
			BEGIN
				-- get last payment date for this schedule so we can set it to be the end date
				DECLARE @lastDate DATETIME
				SELECT @lastDate = MAX(trans_date) FROM payments WHERE payment_schedule_id = @payment_schedule_id AND approved_flag = 1
				
				-- mark as inactive and set end date
				UPDATE payment_schedules SET active = 0, end_date = @lastDate, change_user = @userid, change_date = GETUTCDATE() WHERE payment_schedule_id = @payment_schedule_id
			END
		ELSE
			-- no approved payments on this schedule
			BEGIN
				DELETE FROM payments WHERE payment_schedule_id = @payment_schedule_id AND processed_date IS NULL
				-- DELETE FROM funding_payment_schedules WHERE payment_schedule_id = @payment_schedule_id	
				--DELETE FROM payment_schedules WHERE payment_schedule_id = @payment_schedule_id				
				UPDATE payment_schedules SET active = 0, end_date = @lastDate, change_user = @userid, change_date = GETUTCDATE() WHERE payment_schedule_id = @payment_schedule_id
			END

		-- delete any payments that have not been sent
		DELETE FROM payments WHERE payment_schedule_id = @payment_schedule_id AND processed_date IS NULL

		SELECT 0 AS NeedsApproval	
	END
END
GO
