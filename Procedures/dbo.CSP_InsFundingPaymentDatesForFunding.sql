SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-01-27
-- Description:	Inserts the original funding payment dates that will be used for the initial load of payments.  The table will then be editable by the users 
-- when they need to change payments, instead of using payment schedules
-- =============================================
CREATE PROCEDURE [dbo].[CSP_InsFundingPaymentDatesForFunding]
(
	@funding_id			BIGINT, 
	@userid				NVARCHAR (100)
)
AS
BEGIN	
SET NOCOUNT ON;


DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
DECLARE @today DATE; SET @today = DATEADD(HH, -5, GETUTCDATE())
BEGIN TRANSACTION

DECLARE @tempDate DATE;
DECLARE @frequency VARCHAR (50), @payment MONEY, @first_payment_date DATE
DECLARE @remaining_amount MONEY, @tempAmount MONEY, @orig_payment MONEY
DECLARE @rtr MONEY,  @schedule_start_date DATE

SELECT @rtr = f.portfolio_value, @frequency = f.frequency, @payment = CASE f.frequency WHEN 'Daily' THEN f.weekday_payment ELSE f.weekly_payment END, 
	@first_payment_date = f.first_payment_date
FROM fundings f 
WHERE id = @funding_id

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

SET @tempDate = @first_payment_date
-- don't want to do this again if there are any fundig_payment_dates already
IF NOT EXISTS (SELECT 1 FROM funding_payment_dates WHERE funding_id = @funding_id)
	BEGIN
		-- the merchant bank account is likely not there yet.  Create in case.
		INSERT INTO merchant_bank_accounts (merchant_id, bank_name, routing_number, account_number, account_status, create_date, change_date, change_user, nickname)
		SELECT f.merchant_id, f.receivables_bank_name, f.receivables_aba, f.receivables_account_nbr, 'Active', GETUTCDATE(), GETUTCDATE(),  @userid,
			COALESCE(f.receivables_bank_name, '') + ': ' + COALESCE(f.receivables_aba, '') + ' / ' + COALESCE(f.receivables_account_nbr, '')			
		FROM fundings f
		WHERE f.id = @funding_id
			AND NOT EXISTS (SELECT 1 FROM merchant_bank_accounts ba 
							WHERE ba.merchant_id = f.merchant_id AND ba.bank_name = f.receivables_bank_name AND ba.routing_number = f.receivables_aba 
								AND ba.account_number = f.receivables_account_nbr)

		IF @payment IS NOT NULL AND @frequency IS NOT NULL
			BEGIN
				SET @remaining_amount = @rtr 
				SET @orig_payment = @payment -- @payment can be changed

				WHILE @remaining_amount > 0 
					BEGIN
						SET @tempAmount = @remaining_amount - @payment
						IF @tempAmount < 0
							BEGIN
								SET @payment = @remaining_amount
							END
						IF @frequency = 'Daily'
							BEGIN
								IF DATEPART(dw, @tempDate) IN (2,3,4,5,6) -- only do Monday Through Friday even though CC might have weekend amounts
									BEGIN
										INSERT INTO funding_payment_dates (funding_id, payment_date, amount, rec_amount, payment_schedule_id, create_date, active, original, chargeback_amount, 
											merchant_bank_account_id, payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, change_date, change_user,
											payment_plan_id)								
										SELECT @funding_id, @tempDate, @payment, NULL, NULL, GETUTCDATE(), 1, 1, NULL, ba.merchant_bank_account_id, 
											CASE f.hard_offer_type WHEN 'Split' THEN 'CC' ELSE f.hard_offer_type END, f.processor, NULL, NULL, NULL, GETUTCDATE(), @userid, 
											NULL AS payment_plan_id
										FROM fundings f
										LEFT JOIN merchant_bank_accounts ba ON f.merchant_id = ba.merchant_id AND f.receivables_bank_name = ba.bank_name
											AND f.receivables_aba = ba.routing_number AND f.receivables_account_nbr  = ba.account_number
										WHERE f.id = @funding_id

										SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

										SET @remaining_amount = @remaining_amount - @payment
									END
							END
						ELSE IF @frequency = 'Weekly'
							BEGIN
								IF DATEPART(dw, @tempDate) = DATEPART(dw, @first_payment_date)
									BEGIN
										-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
										INSERT INTO funding_payment_dates (funding_id, payment_date, amount, rec_amount, payment_schedule_id, create_date, active, original, chargeback_amount, 
											merchant_bank_account_id, payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, change_date, change_user,
											payment_plan_id)								
										SELECT @funding_id, @tempDate, @payment, NULL, NULL, GETUTCDATE(), 1, 1, NULL, ba.merchant_bank_account_id, 
											CASE f.hard_offer_type WHEN 'Split' THEN 'CC' ELSE f.hard_offer_type END, f.processor, NULL, NULL, NULL, GETUTCDATE(), @userid, 
											NULL AS payment_plan_id
										FROM fundings f
										LEFT JOIN merchant_bank_accounts ba ON f.merchant_id = ba.merchant_id AND f.receivables_bank_name = ba.bank_name
											AND f.receivables_aba = ba.routing_number AND f.receivables_account_nbr  = ba.account_number
										WHERE f.id = @funding_id

										SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

										SET @remaining_amount = @remaining_amount - @payment
									END
							END
						ELSE IF @frequency = 'Monthly'
							BEGIN
								IF DAY(@tempDate) = DAY(@first_payment_date)
									BEGIN
										-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
										INSERT INTO funding_payment_dates (funding_id, payment_date, amount, rec_amount, payment_schedule_id, create_date, active, original, chargeback_amount, 
											merchant_bank_account_id, payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, change_date, change_user,
											payment_plan_id)								
										SELECT @funding_id, @tempDate, @payment, NULL, NULL, GETUTCDATE(), 1, 1, NULL, ba.merchant_bank_account_id, 
											CASE f.hard_offer_type WHEN 'Split' THEN 'CC' ELSE f.hard_offer_type END, f.processor, NULL, NULL, NULL, GETUTCDATE(), @userid, 
											NULL AS payment_plan_id
										FROM fundings f
										LEFT JOIN merchant_bank_accounts ba ON f.merchant_id = ba.merchant_id AND f.receivables_bank_name = ba.bank_name
											AND f.receivables_aba = ba.routing_number AND f.receivables_account_nbr  = ba.account_number
										WHERE f.id = @funding_id

										SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

										SET @remaining_amount = @remaining_amount - @payment
									END
							END
						-- increment @tempDate
						SET @tempDate = DATEADD(D, 1, @tempDate)		
						-- reset @payment variable
						SET @payment = @orig_payment						
					END	
			END
	END

EXEC dbo.CSP_GeneratePaymentsForFunding @funding_id, @userid

COMMIT TRANSACTION
GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)

Done:


END
GO
