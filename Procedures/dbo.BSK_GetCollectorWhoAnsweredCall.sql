SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:     Ryan Brown
-- Create Date: 2021-03-23
-- Description: Returns the collector_email of who answered the call.  Used for sending the affiliate popup and contact follow up popup
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetCollectorWhoAnsweredCall]
(
	@call_id		NVARCHAR (300)
)
AS

SET NOCOUNT ON

SELECT u.email
FROM phone_logs l
LEFT JOIN user_phones p ON l.call_to = p.phone_number
LEFT JOIN users u ON p.user_id = u.user_id
WHERE (l.call_id = @call_id OR l.conversation_id = @call_id)
	AND l.status = 'in-progress' AND l.source = 'TwilioEvents'
	AND l.raw_response LIKE '%ForwardedFrom%'
UNION
SELECT l.users_email
FROM phone_logs l
LEFT JOIN user_phones p ON l.call_to = p.phone_number
LEFT JOIN users u ON p.user_id = u.user_id
WHERE (l.call_id = @call_id OR l.conversation_id = @call_id)
	AND l.source = 'MakeCall' 


GO
