SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-04-25
-- Description:	Inserts a new record into the acct_files table
-- =============================================
CREATE PROCEDURE [dbo].[CSP_InsAcctFile]
(
	@acct_file_name				[VARCHAR] (100),	
	@row_count					[INT],
	@batch_ids					[VARCHAR](MAX),
	@acct_folder				[VARCHAR] (100)
)
AS
BEGIN
SET NOCOUNT ON;

INSERT INTO acct_files (acct_file_name, userid, row_count, batch_ids, downloaded_last, acct_folder, change_date, change_user)
VALUES (@acct_file_name, NULL, @row_count, LEFT(@batch_ids, 2000), NULL, @acct_folder, GETUTCDATE(), 'SYSTEM')


END
GO
