SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-08-13
-- Description:	Marks Complete fundings that have been paid off
-- =============================================
CREATE PROCEDURE [dbo].[CSP_MarkCompleteFundings] 

AS
BEGIN
SET NOCOUNT ON;

-- make sure to get the correct "today's" date since this runs in UTC
DECLARE @today DATE; SELECT @today = CONVERT(DATE, DATEADD(HOUR, -5, GETUTCDATE()))
DECLARE @tomorrow DATE; SELECT @tomorrow = dbo.CF_GetTomorrow(@today)

/*
UPDATE f
SET contract_status = 2, final_payment_date = x.last_payment_date, completed_date = @today, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM fundings f
INNER JOIN (
	SELECT t.funding_id, MAX(t.trans_date) AS last_payment_date
	FROM transactions t		
	WHERE t.trans_type_id IN (2, 3, 4, 5, 19)
	GROUP BY t.funding_id
) x ON f.id = x.funding_id 
WHERE f.payoff_amount = 0 AND COALESCE(f.float_amount, 0) = 0 AND f.contract_status NOT IN (2,4) AND f.completed_date IS NULL
*/


-- we need to also make sure that any payment schedules are set to inactive so we need to put all affected
-- fundings into a temp table first
IF OBJECT_ID('tempdb..#tempIDs') IS NOT NULL DROP TABLE #tempIDs
CREATE TABLE #tempIDs (funding_id bigint not null, final_payment_date DATETIME)

INSERT INTO #tempIDs (funding_id, final_payment_date)
SELECT f.id, x.last_payment_date
FROM fundings f
INNER JOIN (
	SELECT t.funding_id, MAX(t.trans_date) AS last_payment_date
	FROM transactions t		
	WHERE t.trans_type_id IN (2, 3, 4, 5, 19)
	GROUP BY t.funding_id
) x ON f.id = x.funding_id 
WHERE f.payoff_amount = 0 AND COALESCE(f.float_amount, 0) = 0 AND f.contract_status NOT IN (2,4) AND f.completed_date IS NULL

UPDATE f 
SET contract_status = 2, final_payment_date = t.final_payment_date, completed_date = @today, change_date = GETUTCDATE(), change_user = 'SYSTEM', on_hold = 1, 
	est_months_left = 0
FROM fundings f
INNER JOIN #tempIDs t ON f.id = t.funding_id

-- deactivate all future funding payment dates
UPDATE pd
SET active = 0, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM funding_payment_dates pd 
INNER JOIN #tempIDs t ON pd.funding_id = t.funding_id
WHERE pd.active = 1 AND pd.payment_date > @tomorrow

/*
UPDATE ps
SET end_date = t.final_payment_date, active = 0, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM payment_schedules ps
INNER JOIN funding_payment_schedules fps ON ps.payment_schedule_id = fps.payment_schedule_id
INNER JOIN #tempIDs t ON fps.funding_id = t.funding_id
WHERE ps.active = 1
*/


--- **********************************************

TRUNCATE TABLE #tempIDs
/*
UPDATE f
SET final_payment_date = x.last_payment_date, completed_date = @today, change_date = GETUTCDATE(), change_user = 'SYSTEM',
	contract_substatus_id = 2 -- writeoff, complete
FROM fundings f
INNER JOIN (
	SELECT t.funding_id, MAX(t.trans_date) AS last_payment_date
	FROM transactions t		
	WHERE t.trans_type_id IN (2, 3, 4, 5, 19)
	GROUP BY t.funding_id
) x ON f.id = x.funding_id 
WHERE f.payoff_amount = 0 AND COALESCE(f.float_amount, 0) = 0 AND f.contract_status = 4 AND f.completed_date IS NULL
*/


INSERT INTO #tempIDs(funding_id, final_payment_date)
SELECT f.id, x.last_payment_date
FROM fundings f
INNER JOIN (
	SELECT t.funding_id, MAX(t.trans_date) AS last_payment_date
	FROM transactions t		
	WHERE t.trans_type_id IN (2, 3, 4, 5, 19)
	GROUP BY t.funding_id
) x ON f.id = x.funding_id 
WHERE f.payoff_amount = 0 AND COALESCE(f.float_amount, 0) = 0 AND f.contract_status = 4 AND f.completed_date IS NULL


UPDATE f 
SET final_payment_date = t.final_payment_date, completed_date = @today, change_date = GETUTCDATE(), change_user = 'SYSTEM', on_hold = 1, 
	est_months_left = 0
FROM fundings f
INNER JOIN #tempIDs t ON f.id = t.funding_id


/*
UPDATE ps
SET end_date = t.final_payment_date, active = 0, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM payment_schedules ps
INNER JOIN funding_payment_schedules fps ON ps.payment_schedule_id = fps.payment_schedule_id
INNER JOIN #tempIDs t ON fps.funding_id = t.funding_id
WHERE ps.active = 1
*/

IF OBJECT_ID('tempdb..#tempIDs') IS NOT NULL DROP TABLE #tempIDs


-- Update any redirect approvals where the funding id is now complete.  Set end_date  
UPDATE ra
SET change_date = GETUTCDATE(), change_user = 'SYSTEM', end_date = GETUTCDATE()
FROM redirect_approvals ra WITH (NOLOCK)
INNER JOIN fundings f2 WITH (NOLOCK) ON ra.to_funding_id = f2.id
WHERE ra.end_date IS NULL 
	AND (f2.contract_status = 2 OR (f2.contract_status = 4 AND f2.contract_substatus_id = 2))


END
GO
