SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-11-19
-- Description:	Creates phone_calls records using the phone_logs tables
-- =============================================
CREATE PROCEDURE [dbo].[BSK_CreatePhoneCallsFromLogs]

AS
BEGIN
SET NOCOUNT ON;

DECLARE @affiliate_id INT, @user_email NVARCHAR (100)
DECLARE @call_to VARCHAR (50), @call_from VARCHAR (50), @call_duration INT, @start_time DATETIME, @end_time DATETIME
DECLARE @first_call_id NVARCHAR (200), @temp_call_id NVARCHAR (200), @incoming BIT

IF OBJECT_ID('tempdb..#tempCalls') IS NOT NULL DROP TABLE #tempCalls
CREATE TABLE #tempCalls (id INT IDENTITY(1, 1), call_id NVARCHAR (200))

INSERT INTO #tempCalls (call_id)
SELECT DISTINCT l.call_id
FROM phone_logs l
WHERE ((status = 'completed' AND direction = 'outbound-api')
	OR (status = 'completed' AND direction = 'inbound')
	)
	AND NOT EXISTS (SELECT 1 FROM phone_calls pc WHERE l.call_id = pc.call_id)


DECLARE @max_id INT, @current_id INT
SELECT @max_id = MAX(id), @current_id = 1
FROM #tempCalls 

WHILE @current_id <= COALESCE(@max_id, 0) 
	BEGIN
		SELECT @temp_call_id = call_id FROM #tempCalls WHERE id = @current_id

		SELECT @call_to = l.call_to, @call_from = l.call_from, @first_call_id = l.call_id, @affiliate_id = l.affiliate_id, @user_email = l.users_email			
		FROM phone_logs l
		WHERE l.call_id = @temp_call_id AND event_type IN ('MakeCall', 'IncomingCall')

		SELECT @call_duration = l.duration, @start_time = l.start_time, @end_time = l.start_time, @incoming = CASE WHEN l.direction = 'inbound' THEN 1 ELSE 0 END
		FROM phone_logs l
		WHERE l.phone_log_id = (SELECT MAX(phone_log_id) FROM phone_logs l2 WHERE l2.call_id = @temp_call_id AND status = 'completed')


		EXEC dbo.BSK_InsPhoneCall  @affiliate_id, null, null, @user_email, @call_duration, @start_time, @end_time, null, @first_call_id, @temp_call_id, @incoming

		SET @current_id = @current_id + 1
	END

END

GO
