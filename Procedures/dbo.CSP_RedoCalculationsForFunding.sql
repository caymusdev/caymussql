SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-04-22
-- Description:	Redoes the calculations for a specific funding
-- =============================================
CREATE PROCEDURE [dbo].[CSP_RedoCalculationsForFunding]
(
	@funding_id			BIGINT,
	@userid				NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON;

UPDATE transactions 
SET calculated = NULL 
WHERE funding_id = @funding_id


UPDATE fundings SET ach_draft = NULL, ach_received = NULL, ach_reject = NULL, ach_revenue = NULL, applied_amount = NULL, cash_fees = NULL, cash_from_reapplication = NULL, cash_to_bad_debt = NULL, 
	cash_to_chargeback = NULL, cash_to_margin = NULL, cash_to_pp = NULL, cash_to_reapplication = NULL, cash_to_rtr = NULL, cc_revenue = NULL, checks_revenue = NULL, continuous_pull = NULL, 
	counter_deposit_revenue = NULL, discount_received = NULL, fees_applied = NULL, fees_out = NULL, gross_dollars_rec = NULL, gross_received = NULL, gross_revenue = NULL, margin_adjustment = NULL, 
	margin_applied = NULL, margin_out = NULL, net_other_income = NULL, net_revenue = NULL, paid_off_amount = NULL, pp_adjustment = NULL, pp_applied = NULL, pp_out = NULL, 
	refund = NULL, refund_processed = NULL, refund_reapplied = NULL, refund_returned = NULL, rtr_adjustment = NULL, rtr_applied = NULL, rtr_out = NULL, settlement_received = NULL, 
	total_fees_collected = NULL, total_portfolio_adjustment = NULL, wires_revenue = NULL, wires_revenue_no_renewal = NULL, writeoff_adjustment_received = NULL, writeoff_received = NULL,
	payoff_amount = portfolio_value, pp_settlement = NULL, margin_settlement = NULL, first_draft_date = NULL, first_received_date = NULL
WHERE id = @funding_id


EXEC dbo.CSP_UpdCalculationsForFunding @funding_id, @userid


END
GO
