SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[BSK_InsException]
(
		@Exception		NVARCHAR (MAX),
		@StackTrace		NVARCHAR (MAX) = null,
		@UserID			NVARCHAR (50) = null,
		@MethodName		NVARCHAR (500) = null,
		@FilePath		NVARCHAR (500) = null,
		@LineNumber		INT = null,
		@Comments		NVARCHAR (MAX) = null	
)
AS
BEGIN
SET NOCOUNT ON;

    INSERT INTO exception_logs 
			(Exception, stack_trace, create_date, user_id, method_name, file_path, line_number, comments)
    VALUES	
			(@Exception, @StackTrace, GETUTCDATE(), @UserID, @MethodName, @FilePath, @LineNumber, @Comments)			
END

GO
