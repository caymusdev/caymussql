SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-04-24
-- Description:	This will get a new batch # which will be used during the importing of files
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetNewBatchNumber]
(
	@FileType			NVARCHAR (50),
	@change_user		NVARCHAR (50) = 'SYSTEM',
	@batch_type			NVARCHAR (50),
	@file_name			NVARCHAR (500),
	@batch_date			DATETIME,
	@manual_flag		BIT = 0
)
AS
BEGIN	
SET NOCOUNT ON;

INSERT INTO Batches(batch_type, batch_status, file_name, batch_date, comments, deleted_flag, create_date, processed_date, change_date, change_user, manual_flag)
VALUES (@batch_type, 'New', @file_name, @batch_date, 'New batch ' + @FileType, 0, GETUTCDATE(), NULL, GETUTCDATE(), @change_user, @manual_flag)

DECLARE @batch_id INT
SELECT @batch_id = SCOPE_IDENTITY() 
SELECT @batch_id AS batch_id
RETURN @batch_id

END
GO
