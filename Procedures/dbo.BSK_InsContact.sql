SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Rick Pina
-- Create Date: 2020-12-22
-- Description: 
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsContact]
(
	@affiliate_id				INT,
	@contact_person_type		INT,
	@active						BIT,
	--@best_contact				BIT,
	@contact_first_name			NVARCHAR (100),
	@contact_last_name			NVARCHAR (100),
	@business_name				NVARCHAR (100),
	@note						NVARCHAR (4000),
	@email						NVARCHAR (100),
	@create_user				NVARCHAR (100),
	@from_bossk					BIT 
)
AS
	DECLARE @contact_id AS NVARCHAR (100)

SET NOCOUNT ON
	INSERT INTO contacts(affiliate_id, contact_person_type, active, best_contact, contact_first_name, contact_last_name, business_name, note, email, create_date, change_date, 
		change_user, create_user, from_bossk, exported_time)
	Values (@affiliate_id, @contact_person_type, @active, NULL, @contact_first_name, @contact_last_name, @business_name, @note, @email, GETUTCDATE(), GETUTCDATE(), 
		@create_user, @create_user, @from_bossk, NULL)
	

	Select scope_identity() as contact_id

GO
