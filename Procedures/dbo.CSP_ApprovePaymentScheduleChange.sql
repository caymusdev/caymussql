SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-05-14
-- Description:	Approves a payment schedule change that has been queued
-- =============================================
CREATE PROCEDURE [dbo].[CSP_ApprovePaymentScheduleChange]
(	
	@id							INT,
	@userid						NVARCHAR (100),  -- this is the approver
	@payment_method				VARCHAR (50),
	@payment_processor			VARCHAR (50)
)
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @change_type VARCHAR (50)
SELECT @change_type = change_type FROM payment_schedule_changes c WHERE c.id = @id

-- VARS
DECLARE @payment_schedule_id INT, @frequency VARCHAR (50), @amount MONEY, @start_date DATE, @end_date DATE, @active BIT, @bank_name NVARCHAR (50), @routing_number VARCHAR (50)
DECLARE @account_number	VARCHAR (50), @funding_id BIGINT, @approved_by NVARCHAR (100), @cc_percentage NUMERIC (10,4), @change_reason_id INT
DECLARE @comments NVARCHAR (4000), @part_of_plan BIT, @orig_user NVARCHAR (100)
SELECT @cc_percentage = NULL
-- end VARS

SELECT @payment_schedule_id = c.payment_schedule_id, @frequency = c.frequency, @amount = c.amount, @start_date = c.start_date, @end_date = c.end_date, 
	@active = c.active, @bank_name = c.bank_name, @routing_number = c.routing_number, @account_number = c.account_number, @funding_id = c.funding_id, 
	@approved_by = @userid, @payment_method = c.payment_method, @comments = c.comments, @change_reason_id = c.change_reason_id, @part_of_plan = c.part_of_plan,
	@orig_user = c.userid
FROM payment_schedule_changes c
WHERE c.id = @id

IF @change_type = 'Insert'
	BEGIN
		-- make sure funding is not on hold
		UPDATE fundings SET on_hold = 0 WHERE id = @funding_id AND on_hold = 1

		EXEC dbo.CSP_InsPaymentSchedule @payment_method, @frequency, @amount, @start_date, @cc_percentage, @bank_name, @routing_number, @account_number, @payment_processor, 
			@funding_id, @orig_user, @end_date, @active, @approved_by, @id, @comments, @change_reason_id, @part_of_plan
	END
ELSE IF @change_type = 'Update'
	BEGIN
		-- make sure funding is not on hold
		UPDATE fundings SET on_hold = 0 WHERE id = @funding_id AND on_hold = 1

		EXEC dbo.CSP_UpdPaymentSchedule @payment_schedule_id, @frequency, @amount, @start_date, @end_date, @active, @bank_name, @routing_number, @account_number, @orig_user, 
			@funding_id, @approved_by, @id, @comments, @payment_processor, @change_reason_id, @part_of_plan
	END
ELSE IF @change_type = 'Delete'
	BEGIN
		EXEC dbo.CSP_DelPaymentSchedule @payment_schedule_id, @orig_user, @approved_by, @id
	END


-- update this schedule to mark it as approved
UPDATE payment_schedule_changes 
SET approved_by = @userid, approved_date = GETUTCDATE(), change_date = GETUTCDATE(), change_user = @userid
WHERE id = @id

END
GO
