SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-03-24
-- Description:	Used from the distributions page to mark an undistributed transaction as a non-portfolio transaction.
-- Ex: one that is imported from bank statements
-- =============================================
CREATE PROCEDURE dbo.CSP_InsNonPortfolioTrans
(
	@trans_id		INT, 
	@userid			NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON;

UPDATE transactions 
SET trans_type_id = 9999, change_date = GETUTCDATE(), change_user = @userid, redistributed = 1
WHERE trans_id = @trans_id
	-- double check this has not been redistributed already
	AND redistributed = 0
	AND NOT EXISTS (SELECT 1 FROM transaction_details WHERE trans_id = @trans_id)


END
GO
