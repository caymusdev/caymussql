SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-06-25
-- Description:	Inserts an affiliate from SOLO if it does not exist
-- =============================================
CREATE PROCEDURE [dbo].[CSP_SyncAffiliate]
(
	@affiliate_id			BIGINT,
	@affiliate_name			NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @minTempID BIGINT; SET @minTempID = 1000000
DECLARE @maxID BIGINT

IF (@affiliate_id IS NULL)
	BEGIN
		SELECT @maxID = MAX(affiliate_id) FROM affiliates WITH (NOLOCK)

		IF @maxID < @minTempID
			BEGIN
				SET @affiliate_id = @minTempID
			END
		ELSE
			BEGIN
				SET @affiliate_id = @maxID + 1
			END
	END

IF NOT EXISTS(SELECT 1 FROM affiliates WHERE affiliate_id = @affiliate_id)
	BEGIN
		INSERT INTO affiliates(affiliate_id, affiliate_name, change_user, change_date)
		VALUES (@affiliate_id, @affiliate_name, 'SYSTEM', GETUTCDATE())
	END
END
GO
