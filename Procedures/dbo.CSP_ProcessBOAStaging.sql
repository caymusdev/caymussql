SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-06-18
-- Description:	Takes a boa_staging id and will create transactions for it
-- =============================================
CREATE PROCEDURE [dbo].[CSP_ProcessBOAStaging]
(
	@boa_staging_id			INT
)

AS
BEGIN
SET NOCOUNT ON;

DECLARE @funding_id INT, @settle_type VARCHAR (50), @trans_type_id INT, @description NVARCHAR (1000)
SELECT @settle_type = 'WIRE', @trans_type_id = 5

-- first need to get the funding for the settlement
DECLARE @mid NVARCHAR (100), @merchant_name NVARCHAR (200), @trans_date DATE, @trans_amount MONEY, @batch_id INT, @record_id INT
SELECT @trans_date = s.[Date], @trans_amount = s.Amount, @batch_id = s.BatchID, @record_id = s.boa_staging_id, 
	@merchant_name = CASE CHARINDEX('ORIG:', s.Description) WHEN 0 THEN ''
		ELSE SUBSTRING(s.Description, CHARINDEX('ORIG:', s.Description)+5, CHARINDEX(' ID:', s.Description) - 5 - CHARINDEX('ORIG:', s.Description)) END,
	@description = s.Description
FROM boa_staging s
WHERE s.boa_staging_id = @boa_staging_id

-- WIRES don't have good information right now so they'll just go into a queue to be manually associated with a fund

EXEC @funding_id = dbo.CSP_GetFundingIDForTransaction @trans_date, NULL, NULL, @merchant_name
IF @funding_id = -1 SET @funding_id = NULL


--EXEC dbo.CSP_DistributeTransaction @trans_date, @funding_id, @trans_amount, @merchant_name, @batch_id, @record_id, NULL, 'WIRE'
--EXEC dbo.CSP_DistributeTransaction_v2 @funding_id, @trans_amount, NULL, @trans_date, @merchant_name, @batch_id, @record_id, 'boa_staging', 
	-- NULL, @trans_date, 'WIRE', NULL, 5

-- RKB 2019-07-31- New logic to bring in everything in BOA since some things are missed and not manually entered by Accounting
IF @description LIKE '%WIRE TYPE:%' AND @description NOT LIKE '%MPCF II LLC%'
	BEGIN
		SELECT @settle_type = 'WIRE', @trans_type_id = 5
	END
ELSE IF LOWER(@description) = 'counter credit'
	BEGIN
		SELECT @settle_type = 'CounterCredit', @trans_type_id = 2
	END
-- all the others I guess will go in as wires.  Until we learn other patterns.  


EXEC dbo.CSP_DistributeTransaction_v2 @funding_id, @trans_amount, NULL, @trans_date, @merchant_name, @batch_id, @record_id, 'boa_staging', 
	NULL, @trans_date, @settle_type, NULL, @trans_type_id


END
GO
