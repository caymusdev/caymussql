SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-01-15
-- Description:	Take a table of case ids and assigns them and creates the new case notification.  case_departments should and department_Id on case should already be handled.
--  This is just the assigning portion
-- =============================================
CREATE PROCEDURE [dbo].[BSK_AssignCases]
(
	@Case_IDs		IDTableType READONLY
)
AS
BEGIN
SET NOCOUNT ON;
DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE());
DECLARE @yesterday DATE; SELECT @yesterday = DATEADD(DD, -1, @today)

BEGIN TRANSACTION

IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL DROP TABLE #tempUsers
CREATE TABLE #tempUsers (id INT IDENTITY (1, 1), user_id INT, department NVARCHAR (50), department_id INT, num_cases INT)

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


-- get a count of assigned cases for each collector that is a Customer Service
INSERT INTO #tempUsers (user_id, department, department_id, num_cases)
SELECT ud.user_id, d.department, d.department_id, SUM(CASE WHEN c.case_id IS NULL THEN 0 ELSE 1 END) AS num_cases
FROM user_departments ud
INNER JOIN departments d ON ud.department_id = d.department_id
LEFT JOIN collector_cases cc ON ud.user_id = cc.collector_user_id AND cc.start_date <= @today AND COALESCE(cc.end_date, '2050-05-09') >= @today
LEFT JOIN cases c ON cc.case_id = c.case_id AND c.active = 1 
GROUP BY ud.user_id, d.department_id, d.department
ORDER BY d.department_id, d.department, num_cases 

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


DECLARE @maxID INT, @currentID INT, @tempUserID INT, @CurrentCaseID INT
SELECT @maxID = MAX(id), @currentID = 1 FROM @Case_IDs

DECLARE @department_id INT, @current_assigned_collector_id INT

WHILE @currentID <= @maxID
	BEGIN
		-- get the current case id
		SELECT @CurrentCaseID = record_id FROM @Case_IDs WHERE id = @currentID

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		-- get case's current department id
		SELECT @department_id = department_id FROM cases WHERE case_id = @CurrentCaseID

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


		-- This runs AFTER a cases department id has been updated so now we need to find a user id on the new department

		SELECT TOP 1 @tempUserID = user_id
		FROM #tempUsers
		WHERE department_id = @department_id
		ORDER BY num_cases

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


		-- need to get the id of the currently assigned collector singe we are changing the assignment, we'll need to decrement their count
		SELECT @current_assigned_collector_id = collector_user_id
		FROM collector_cases
		WHERE case_id = @CurrentCaseID AND end_date IS NULL

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


		-- update current record to end yesterday since the new one starts today
		UPDATE collector_cases
		SET end_date = @yesterday, change_date = GETUTCDATE(), change_user = 'System'
		WHERE case_id = @CurrentCaseID AND end_date IS NULL

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


		INSERT INTO collector_cases (collector_user_id, case_id, start_date, end_date, create_date, change_date, change_user, create_user)
		VALUES (@tempUserID, @CurrentCaseID, @today, NULL, GETUTCDATE(), GETUTCDATE(), 'System', 'System')

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


		-- create a notification for the new case
		DECLARE @affiliate_id INT, @affiliate_name NVARCHAR (100)
		SELECT @affiliate_id = affiliate_id FROM cases WHERE case_id = @CurrentCaseID
		SELECT @affiliate_name = 'New Case for ' + affiliate_name FROM affiliates WHERE affiliate_id = @affiliate_id

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


		EXEC dbo.BSK_InsNotification @tempUserID, NULL, @affiliate_id, NULL, NULL, @affiliate_name, 'NewCase', 'System', @CurrentCaseID

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		-- mark the case as new
		UPDATE cases SET new_case = 1, change_date = GETUTCDATE(), change_user ='SYSTEM' WHERE case_id = @CurrentCaseID
		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


		-- update the count of cases for this user
		UPDATE #tempUsers SET num_cases = num_cases + 1 WHERE user_id = @tempUserID AND department_id = @department_id
		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		-- update the count for the one who just lost a case
		UPDATE #tempUsers SET num_cases = num_cases - 1 WHERE user_id =  @current_assigned_collector_id AND department_id = @department_id
		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


		SET @currentID = @currentID + 1
	END

COMMIT TRANSACTION
GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)

Done:


END


GO
