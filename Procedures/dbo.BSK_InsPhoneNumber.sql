SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Rick Pina
-- Create Date: 2020-12-29
-- Description: 
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsPhoneNumber]
(
	@phone_type_id				INT,
	@phone_number				VARCHAR  (50),
	@create_user				NVARCHAR (100),
	@contact_id					INT 
)
AS

SET NOCOUNT ON

	INSERT INTO phone_numbers(phone_type_id, phone_number, contact_id, create_date, change_date, change_user, create_user)
	Values (@phone_type_id, @phone_number, @contact_id, GETUTCDATE(), GETUTCDATE(), @create_user, @create_user)

GO
