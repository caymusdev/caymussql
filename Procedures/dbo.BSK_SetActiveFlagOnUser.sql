SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2020-09-18
-- Description: Either sets or clears active flag on user account
-- =============================================
CREATE PROCEDURE [dbo].[BSK_SetActiveFlagOnUser]
(
	@user_id			INT,
	@active				BIT,
	@change_user		NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON

UPDATE users
SET active = @active, change_user = @change_user, change_date = GETUTCDATE()
WHERE user_id = @user_id

END
GO
