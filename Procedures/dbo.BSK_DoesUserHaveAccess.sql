SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-06
-- Description: 
-- =============================================
CREATE PROCEDURE [dbo].[BSK_DoesUserHaveAccess]
(	
	@permission_name		VARCHAR (50),	
	@email					NVARCHAR (100)
)
AS
BEGIN	
SET NOCOUNT ON;
-- first check if they are admin, if so they get access to everything
IF EXISTS (SELECT 1
			FROM users u WITH (NOLOCK)
			INNER JOIN user_roles ur WITH (NOLOCK) ON u.user_id = ur.user_id						
			INNER JOIN roles r WITH (NOLOCK) ON ur.role_id = r.role_id
			WHERE u.email = @email AND r.role_name = 'Administrator' AND u.active = 1
			)
	BEGIN
		-- has admin
		SELECT CONVERT(BIT, 0) AS [ReadOnly], CONVERT(BIT, 1) AS HasPermission
	END
-- check for NOT read only
ELSE IF EXISTS (SELECT 1
				FROM users u WITH (NOLOCK)
				INNER JOIN user_roles ur WITH (NOLOCK) ON u.user_id = ur.user_id
				INNER JOIN role_permissions  rp WITH (NOLOCK) ON ur.role_id = rp.role_id
				INNER JOIN permissions p WITH (NOLOCK) ON rp.permission_id = p.permission_id
				WHERE u.email = @email AND p.permission_name = @permission_name AND COALESCE(rp.read_only, 0) = 0 AND u.active = 1
			)
	BEGIN
		SELECT CONVERT(BIT, 0) AS [ReadOnly], CONVERT(BIT, 1) AS HasPermission
	END	
-- check for read only
ELSE IF EXISTS (SELECT 1
				FROM users u WITH (NOLOCK)
				INNER JOIN user_roles ur WITH (NOLOCK) ON u.user_id = ur.user_id
				INNER JOIN role_permissions  rp WITH (NOLOCK) ON ur.role_id = rp.role_id
				INNER JOIN permissions p WITH (NOLOCK) ON rp.permission_id = p.permission_id
				WHERE u.email = @email AND p.permission_name = @permission_name AND COALESCE(rp.read_only, 0) = 1 AND u.active = 1
			)
	BEGIN
		SELECT CONVERT(BIT, 1) AS [ReadOnly], CONVERT(BIT, 1) AS HasPermission
	END	
ELSE
	-- not admin and does not have the permission at all
	BEGIN
		SELECT CONVERT(BIT, 1) AS [ReadOnly], CONVERT(BIT, 0) AS HasPermission
	END

END

GO
