SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-06-27
-- Description:	Used to insert a setting
-- =============================================
CREATE PROCEDURE [dbo].[CSP_InsSetting]
(
	@setting			NVARCHAR (50),
	@value				NVARCHAR (500),
	@setting_desc		NVARCHAR (1000),
	@setting_group		NVARCHAR (50),
	@encrypt			BIT
)
AS
BEGIN	
SET NOCOUNT ON;

INSERT INTO settings(setting, value, setting_desc, setting_group, encrypt_value, change_date, change_user)
SELECT @setting, CASE @encrypt WHEN 1 THEN NULL ELSE @value END, @setting_desc, @setting_group, CASE @encrypt WHEN 1 THEN ENCRYPTBYPASSPHRASE('corellia', @value) ELSE NULL END, 
	GETUTCDATE(), 'SYSTEM'
END
GO
