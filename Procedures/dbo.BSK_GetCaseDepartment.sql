SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2021-01-18
-- Description: Gets the department for a specific case 
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetCaseDepartment]
(
	@case_id			INT
)
AS
BEGIN
SET NOCOUNT ON

SELECT c.department_id, d.department_desc, d.department
FROM cases c
INNER JOIN departments d ON c.department_id = d.department_id
WHERE c.case_id = @case_id

END
GO
