SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-01-21
-- Description:	Saves various settings for the management main tab
-- =============================================
CREATE PROCEDURE [dbo].[BSK_UpdManagementSettings]
(
	@LeftVMFollowup				INT,
	@CustomerServiceDays		INT,
	@Collections1Days			INT,
	@Collections2Days			INT,
	@users_email				NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON;

UPDATE settings SET value = @LeftVMFollowup, change_date = GETUTCDATE(), change_user = @users_email
WHERE setting = 'LeftVMFollowup' AND value != @LeftVMFollowup

UPDATE departments 
SET days_before_moving = @CustomerServiceDays, change_date = GETUTCDATE(), change_user = @users_email
WHERE department = 'CustomerService' AND days_before_moving != @CustomerServiceDays

UPDATE departments 
SET days_before_moving = @Collections1Days, change_date = GETUTCDATE(), change_user = @users_email
WHERE department = 'Collections1' AND days_before_moving != @Collections1Days

UPDATE departments 
SET days_before_moving = @Collections2Days, change_date = GETUTCDATE(), change_user = @users_email
WHERE department = 'Collections2' AND days_before_moving != @Collections2Days

END

GO
