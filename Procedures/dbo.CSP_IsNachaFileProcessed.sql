SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-01-11
-- Description:	Gets a Nacha log to see if a file has already been downloaded
-- =============================================
CREATE PROCEDURE CSP_IsNachaFileProcessed
(
	@nacha_file_name			VARCHAR (100)
)
AS
BEGIN

SET NOCOUNT ON;

IF EXISTS(
	SELECT 1
	FROM nacha_logs l
	WHERE l.nacha_file_name = @nacha_file_name
		AND processed_date IS NOT NULL
		AND proc_status IS NOT NULL
)
	BEGIN
		SELECT 1 AS file_exists
	END
ELSE
	BEGIN
		SELECT 0 AS file_exists
	END


END
GO
