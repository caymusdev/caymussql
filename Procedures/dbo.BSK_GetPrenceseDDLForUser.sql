SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:     Ryan Brown
-- Create Date: 2021-03-18
-- Description: Gets the dropdown options for presence including the current status of the user
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetPrenceseDDLForUser]
(
	@users_email		NVARCHAR (100)
)
AS

SET NOCOUNT ON

DECLARE @current_status INT
SELECT @current_status = presence_status
FROM users 
WHERE email = @users_email

IF @current_status IS NULL
	BEGIN
		DECLARE @avail_status INT
		SELECT @avail_status = user_presence_status_id
		FROM user_presence_statuses 
		WHERE status_name = 'Available'

		EXEC dbo.BSK_InsUserStatus @users_email, @avail_status
	END

SELECT s.user_presence_status_id, s.status_name, s.status_description, 
	CONVERT(BIT, CASE WHEN s.user_presence_status_id = u.presence_status THEN 1 ELSE 0 END) AS CurrentStatus
FROM user_presence_statuses s
LEFT JOIN users u ON s.user_presence_status_id = u.presence_status AND u.email = @users_email
ORDER BY s.status_description

GO
