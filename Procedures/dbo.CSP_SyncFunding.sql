SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-06-25
-- Description:	Inserts a funding/contract from SOLO if it does not exist
-- =============================================
CREATE PROCEDURE [dbo].[CSP_SyncFunding]
(
	@solo_id					BIGINT,
	@affiliate_id				BIGINT,
	@merchant_id				BIGINT,
	@purchase_price				MONEY,
	@pricing_ratio				NUMERIC(10,4),
	@rtr						MONEY,
	@commission_percent			NUMERIC(10,4),
	@commission_amount			MONEY,
	@estimated_turn				NUMERIC(10,2),
	@net_to_merchant			MONEY,
	@frequency					NVARCHAR(50) = '',
	@weekday_payment			MONEY,
	@competitor_payoff			MONEY,
	@receivables_bank_name		NVARCHAR (100),
	@receivables_account_nbr	NVARCHAR (100),
	@receivables_aba			NVARCHAR (100),
	@weekly_payment				MONEY,
	@unearned_income			MONEY = NULL,
	@collection_type			VARCHAR (50),
	@orig_fee					MONEY,
	@commission_iso				NVARCHAR (100),
	@project_name				NVARCHAR (150) = NULL,
	@referral_iso				NVARCHAR (50) = NULL,
	@referral_commission		MONEY = NULL,
	@wire_aba					VARCHAR(50) = NULL,
	@wiring_instructions		NVARCHAR (1000) = NULL,
	@industry					NVARCHAR (100) = NULL,
	@incorp_state				VARCHAR (50) = NULL,
	@years_in_business			INT = NULL,
	@monthly_avg_vol			MONEY = NULL,
	@fico						INT = NULL,
	@contract_type				VARCHAR (50) = NULL,
	@first_payment_date			DATETIME = NULL,
	@hard_offer_type			VARCHAR (50) = NULL,
	@cc_split					NUMERIC(12,6) = NULL,
	@discount					MONEY = NULL,
	@scorecard_value			DECIMAL(16, 4) = NULL,
	@referral_commission_percent NUMERIC(10,4) = NULL,
	@one_month_prepay_amount	MONEY = NULL,
	@two_month_prepay_amount	MONEY = NULL,
	@three_month_prepay_amount	MONEY = NULL,
	@include_prepay_discounts	BIT = NULL,
	@prepay_discounts_too_low	BIT = NULL,
	@commission_on_funding_or_net_merchant BIT = NULL,
	@eligible					BIT = NULL
)
AS
BEGIN
SET NOCOUNT ON;

-- 2018-07-17 - Funded date has to be set by the user in the fundings queue
--DECLARE @funded_date DATETIME -- for now, funded date will be set to the date we import from SOLO
-- this runs in UTC time so convert back to eastern so reporting can get the day right
--SET @funded_date = CONVERT(DATE, DATEADD(HH, -5, GETUTCDATE())) 
DECLARE @contract_number NVARCHAR (50) -- contract number will be today's date (yyyyMMdd) + the solo contract id
SELECT @contract_number = CONVERT(VARCHAR(10), CONVERT(DATE, DATEADD(HH, -5, GETUTCDATE())) , 112) + CONVERT(VARCHAR(50), @solo_id)

DECLARE @recordCount INT; SET @recordCount = 0
DECLARE @added BIT; SET @added = 0

-- RKB 2020-04-27 - some monies come in as more than 2 decimals
SELECT @purchase_price = CONVERT(DECIMAL (16, 2), @purchase_price), @rtr = CONVERT(DECIMAL (16, 2), @rtr), @commission_amount = CONVERT(DECIMAL (16, 2), @commission_amount), 
	@net_to_merchant = CONVERT(DECIMAL (16, 2), @net_to_merchant), @weekday_payment = CONVERT(DECIMAL (16, 2), @weekday_payment), 
	@competitor_payoff = CONVERT(DECIMAL (16, 2), @competitor_payoff),
	@weekly_payment = CONVERT(DECIMAL (16, 2), @weekly_payment), @unearned_income = CONVERT(DECIMAL (16, 2), @unearned_income), @orig_fee = CONVERT(DECIMAL (16, 2), @orig_fee),
	@referral_commission = CONVERT(DECIMAL (16, 2), @referral_commission), @monthly_avg_vol = CONVERT(DECIMAL (16, 2), @monthly_avg_vol), @discount = CONVERT(DECIMAL (16, 2), @discount),
	@one_month_prepay_amount = CONVERT(DECIMAL (16, 2), @one_month_prepay_amount), @two_month_prepay_amount = CONVERT(DECIMAL (16, 2), @two_month_prepay_amount),
	@three_month_prepay_amount = CONVERT(DECIMAL (16, 2), @three_month_prepay_amount)
-- end cleanup on monies


IF @merchant_id IS NULL
	BEGIN
		-- in theory, this code will only run during BCP which will hopefully never happen.  If it does, only one worksheet at a time will be uploaded so
		-- the associated affiliate id should be the max in affiliates that would have just been added before calling this SP
		SELECT @merchant_id = MAX(merchant_id) FROM merchants WITH (NOLOCK)
		SELECT @affiliate_id = MAX(affiliate_id) FROM affiliates WITH (NOLOCK)
	END

IF NOT EXISTS(SELECT 1 FROM fundings WHERE id = @solo_id)
	BEGIN
		DECLARE @legal_name NVARCHAR (100)
		SELECT @legal_name = merchant_name FROM merchants WHERE merchant_id = @merchant_id

		IF COALESCE(@unearned_income, 0) = 0
			BEGIN
				SET @unearned_income = @rtr - @purchase_price
			END

		DECLARE @net_est_yield NUMERIC(12, 5), @orig_fee_percent NUMERIC (16,2)

		SET @orig_fee_percent = @orig_fee / @purchase_price * 100.00
		SET @net_est_yield = ((@pricing_ratio - (@commission_percent / 100.00) + (COALESCE(@orig_fee_percent, 0) / 100.00)) -1)*(12 / (@estimated_turn + 0.000000001))*100.00		

		INSERT INTO fundings(id, affiliate_id, merchant_id, purchase_price, portfolio_value, pricing_ratio, commission_percent, commission_amount, estimated_turn, contract_status, net_to_merchant,
			frequency, weekday_payment, competitor_payoff, receivables_bank_name, receivables_account_nbr, receivables_aba, weekly_payment, legal_name, payoff_amount, paid_off_amount,
			funded_date, unearned_income, contract_number, collection_type, orig_fee, commission_iso, project_name, referral_iso, referral_commission, wire_aba, wiring_instructions,
			industry, incorp_state, years_in_business, monthly_avg_vol, fico, contract_type, first_payment_date, hard_offer_type, cc_split, orig_fee_percent, net_est_yield,
			discount, previous_funding_id, never_redistribute, change_user, change_date, scorecard_value, referral_commission_percent, one_month_prepay_amount, 
			two_month_prepay_amount, three_month_prepay_amount, include_prepay_discounts, prepay_discounts_too_low, commission_on_funding_or_net_merchant, eligible, 
			portfolio)
		VALUES (@solo_id, @affiliate_id, @merchant_id, @purchase_price, @rtr, @pricing_ratio, @commission_percent, @commission_amount, @estimated_turn, 1, @net_to_merchant,
			@frequency, @weekday_payment, @competitor_payoff, @receivables_bank_name, @receivables_account_nbr, @receivables_aba, @weekly_payment, @legal_name, @rtr, 0, NULL, 
			@unearned_income, CONVERT(NVARCHAR (50), @contract_number), @collection_type, @orig_fee, @commission_iso, @project_name, @referral_iso, @referral_commission, @wire_aba, 
			@wiring_instructions, @industry, @incorp_state, @years_in_business, @monthly_avg_vol, @fico, @contract_type, @first_payment_date, @hard_offer_type, @cc_split, @orig_fee_percent, @net_est_yield,
			@discount, NULL, 0, 'SYSTEM', GETUTCDATE(), @scorecard_value, @referral_commission_percent, @one_month_prepay_amount, @two_month_prepay_amount, @three_month_prepay_amount, 
			@include_prepay_discounts, @prepay_discounts_too_low, @commission_on_funding_or_net_merchant, --@eligible, CASE WHEN @eligible = 1 THEN 'spv' ELSE 'caymus' END)
			0, 'caymus')

		SET @recordCount = @@ROWCOUNT
		SET @added = 1
	END
ELSE
	BEGIN
		-- some frequencies were not set originally in SOLO
		UPDATE fundings
		SET frequency = @frequency, project_name = @project_name, change_user = 'SYSTEM', change_date = GETUTCDATE()
		WHERE id = @solo_id AND COALESCE(frequency, '') = ''
	END


SELECT @recordCount AS recordCount, @added AS added
END

GO
