SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-02-26
-- Description:	Checks for fundings with no future payment dates.  
-- Likely shouldn't happen since we have CSP_GenerateMissingFundingPaymentDates
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetFundingsWithNoFuturePaymentDates]	
AS
BEGIN
SET NOCOUNT ON;

DECLARE @goLiveDate DATETIME; SELECT @goLiveDate = CONVERT(DATETIME, dbo.CF_GetSetting('GoLiveDate'))
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SELECT @tomorrow = DATEADD(DD, CASE DATEPART(WEEKDAY, @today) WHEN 6 THEN 3 WHEN 7 THEN 2 ELSE 1 END, @today)


SELECT f.id, f.legal_name
FROM fundings f
WHERE COALESCE(f.funded_date, GETDATE()) >= @goLiveDate
	AND f.processor IS NOT NULL	AND f.contract_status IN (1) AND COALESCE(f.on_hold, 0) = 0
	AND NOT EXISTS (SELECT 1 FROM funding_payment_dates WHERE funding_id = f.id AND payment_date >= @tomorrow AND active = 1)
	AND f.hard_offer_type = 'ACH'
	AND COALESCE(f.payoff_amount, f.portfolio_value) > 0
ORDER BY f.legal_name

END
GO
