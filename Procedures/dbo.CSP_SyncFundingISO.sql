SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-02-04
-- Description:	Inserts a funding/contract distribution (payoff) from SOLO if it does not exist
-- =============================================
CREATE PROCEDURE [dbo].[CSP_SyncFundingISO]
(
	@funding_id				[BIGINT],
	@iso_id					[BIGINT],
	@legal_name				[NVARCHAR] (200),
	@dba_name				[NVARCHAR] (200),
	@name_on_check			[NVARCHAR] (200),
	@address_1				[NVARCHAR] (200),
	@address_2				[NVARCHAR] (200),
	@city					[NVARCHAR] (200),
	@state					[NVARCHAR] (200),
	@postal_code			[NVARCHAR] (200),
	@bank_name				[NVARCHAR] (200),
	@account_number			[NVARCHAR] (200),
	@routing_number			[NVARCHAR] (200),
	@wire_routing_number	[NVARCHAR] (200),
	@wiring_instructions	[NVARCHAR] (1000),
	@is_referring			BIT
)
AS
BEGIN
SET NOCOUNT ON;


IF NOT EXISTS(SELECT 1 FROM funding_isos WHERE funding_id = @funding_id AND iso_id = @iso_id)
	BEGIN
		INSERT INTO funding_isos (funding_id, iso_id, is_referring, legal_name, dba_name, name_on_check, address_1, address_2, city, state, postal_code, bank_name, account_number, 
			routing_number, wire_routing_number, wiring_instructions, create_date, change_date, change_user, create_user)
		VALUES (@funding_id, @iso_id, @is_referring, @legal_name, @dba_name, @name_on_check, @address_1, @address_2, @city, @state, @postal_code, @bank_name, @account_number,
			@routing_number, @wire_routing_number, @wiring_instructions, GETUTCDATE(), GETUTCDATE(), 'SYSTEM', 'SYSTEM')
	END
/*  Not doing now.  Should never be an update.   Once it is funded in SOLO this data should never change so once we have it in Portfolio, it stays that way.
ELSE
	BEGIN

		
	END
*/

END


GO
