SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Rick Pina
-- Create date: 2021-02-15
-- Description:	Updates Contact
-- =============================================
CREATE PROCEDURE [dbo].[BSK_UpdContactLog]
(
	@contact_log_id						INT,	
	@contact_outcome					NVARCHAR (1000),
	@notes								NVARCHAR (MAX),
	@reminder_id				INT,
	@change_user				NVARCHAR(100)

)
AS
BEGIN
SET NOCOUNT ON;

UPDATE contact_log
SET contact_outcome = @contact_outcome,	notes = @notes, reminder_id = @reminder_id,
change_user = @change_user, change_date = GETUTCDATE()
WHERE contact_log_id = @contact_log_id

END

GO
