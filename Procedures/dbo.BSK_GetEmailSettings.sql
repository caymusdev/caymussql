SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-08-30
-- Description:	Retrieves all settings related to sending an email
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetEmailSettings]
	
AS
BEGIN
SET NOCOUNT ON;

SELECT dbo.BF_GetSetting('EmailFrom') AS EmailFrom, dbo.BF_GetSetting('EmailSMTPUserID') AS EmailSMTPUserID, dbo.BF_GetSetting('EmailSMTPPassword') AS EmailSMTPPassword, 
	dbo.BF_GetSetting('EmailSMTPHost') AS EmailSMTPHost, dbo.BF_GetSetting('EmailSMTPort') AS EmailSMTPort
	
END
GO
