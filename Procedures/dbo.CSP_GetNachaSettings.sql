SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-01-11
-- Description:	Gets NACHA settings for a given vendor
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetNachaSettings]
(
	@vendor			VARCHAR (50)
)
AS
BEGIN	
SET NOCOUNT ON;


IF @vendor = '53'
	BEGIN
		SELECT dbo.CF_GetSetting('ImmediateDestination') AS ImmediateDestination, dbo.CF_GetSetting('ImmediateOrigin') AS ImmediateOrigin, dbo.CF_GetSetting('ImmediateDestName') AS ImmediateDestName, 
			dbo.CF_GetSetting('ImmediateOrigName') AS ImmediateOrigName, dbo.CF_GetSetting('CallbackNumber') AS CallbackNumber, dbo.CF_GetSetting('CompanyEntryDescription') AS CompanyEntryDescription,
			dbo.CF_GetSetting('CompanyIDIncoming') AS CompanyIDIncoming, dbo.CF_GetSetting('CompanyIDOutgoing') AS CompanyIDOutgoing
	END
ELSE IF @vendor = 'Regions'
	BEGIN
		SELECT dbo.CF_GetSetting('RegionsImmediateDestination') AS ImmediateDestination, dbo.CF_GetSetting('RegionsImmediateOrigin') AS ImmediateOrigin, 
			dbo.CF_GetSetting('RegionsImmediateDestName') AS ImmediateDestName, dbo.CF_GetSetting('RegionsImmediateOrigName') AS ImmediateOrigName, 
			'' AS CallbackNumber, dbo.CF_GetSetting('RegionsCompanyEntryDescription') AS CompanyEntryDescription,
			dbo.CF_GetSetting('RegionsCompanyIDIncoming') AS CompanyIDIncoming, dbo.CF_GetSetting('RegionsCompanyIDOutgoing') AS CompanyIDOutgoing
	END
ELSE IF @vendor = 'SPV'
	BEGIN
		SELECT dbo.CF_GetSetting('RegionsImmediateDestinationSPV') AS ImmediateDestination, dbo.CF_GetSetting('RegionsImmediateOriginSPV') AS ImmediateOrigin, 
			dbo.CF_GetSetting('RegionsImmediateDestNameSPV') AS ImmediateDestName, dbo.CF_GetSetting('RegionsImmediateOrigNameSPV') AS ImmediateOrigName, 
			'' AS CallbackNumber, dbo.CF_GetSetting('RegionsCompanyEntryDescriptionSPV') AS CompanyEntryDescription,
			dbo.CF_GetSetting('RegionsCompanyIDIncomingSPV') AS CompanyIDIncoming, dbo.CF_GetSetting('RegionsCompanyIDOutgoingSPV') AS CompanyIDOutgoing
	END


END


GO
