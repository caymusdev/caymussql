SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-05-21
-- Description:	Processes the Forte staging tables into the regular forte tables
-- =============================================
CREATE PROCEDURE [dbo].[CSP_ProcessForteStagingTables_old]
	
AS
BEGIN	
SET NOCOUNT ON;
DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''

BEGIN TRANSACTION

-- forte transactions
INSERT INTO forte_trans(customer_token, transaction_id, customer_id, status, action, authorization_amount, authorization_code, entered_by, received_date, origination_date,
	company_name, response_code, create_date, change_date, change_user)
SELECT s.customer_token, s.transaction_id, s.customer_id, s.status, s.action, s.authorization_amount, s.authorization_code, s.entered_by, s.received_date, s.origination_date,
	s.company_name, s.response_code, GETUTCDATE(), GETUTCDATE(), 'SYSTEM'
FROM forte_trans_staging s
WHERE NOT EXISTS (SELECT 1 FROM forte_trans t WHERE s.transaction_id = t.transaction_id AND s.status = t.status AND s.response_code = t.response_code)

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR

-- forte fundings
INSERT INTO forte_fundings(funding_id, status, effective_date, origination_date, net_amount, routing_number, entry_description, batch_id, create_date, change_date, change_user)
SELECT s.funding_id, s.status, s.effective_date, s.origination_date, s.net_amount, s.routing_number, s.entry_description, NULL, GETUTCDATE(), GETUTCDATE(), 'SYSTEM'
FROM forte_fundings_staging s
WHERE NOT EXISTS (SELECT 1 FROM forte_fundings f WHERE s.funding_id = f.funding_id)

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR

-- forte settlements
INSERT INTO forte_settlements (settle_id, customer_token, transaction_id, customer_id, funding_id, settle_batch_id, settle_date, settle_type, settle_response_code, settle_amount, 
	method, forte_funding_id, create_date, change_date, change_user)
SELECT s.settle_id, s.customer_token, s.transaction_id, s.customer_id, s.funding_id, s.settle_batch_id, s.settle_date, s.settle_type, s.settle_response_code, s.settle_amount,
	s.method, f.forte_funding_id, GETUTCDATE(), GETUTCDATE(), 'SYSTEM'
FROM forte_settlements_staging s
LEFT JOIN forte_fundings f ON s.funding_id = f.funding_id

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR

-- now truncate the tables
TRUNCATE TABLE forte_trans
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR

TRUNCATE TABLE forte_fundings
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR

TRUNCATE TABLE forte_settlements
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR


COMMIT TRANSACTION
GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)	

Done:

END
GO
