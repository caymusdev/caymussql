SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-04-25
-- Description:	Updates a record into the acct_files table
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdAcctFile]
(
	@acct_file_id				[INT],
	@userid						[VARCHAR] (100)
)
AS
BEGIN
SET NOCOUNT ON;

UPDATE acct_files
SET downloaded_last = GETUTCDATE(), userid = @userid, change_user = @userid, change_date = GETUTCDATE()
WHERE acct_file_id = @acct_file_id

END
GO
