SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-11-14
-- Description:	Since some of the formulas have changed and since there are a ton new ones, this sp will wipe out the facts tables and reinsert
-- =============================================
CREATE PROCEDURE [dbo].[CSP_RedoFactTables]

AS
BEGIN

SET NOCOUNT ON;

DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
BEGIN TRANSACTION

DECLARE @today DATE; SET @today = CONVERT(DATE, DATEADD(HH, -5, GETUTCDATE()))
DECLARE @timeStamp DATETIME; SET @timeStamp = GETUTCDATE()
DECLARE @endDate DATE; set @endDate = '9999-12-31'



IF NOT EXISTS(SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tempFloatNewFormulas]') AND type in (N'U'))
	BEGIN
		CREATE TABLE tempFloatNewFormulas([funding_id] [bigint] NOT NULL, [start_date] DATE, float_amount MONEY, create_date DATETIME DEFAULT GETUTCDATE())

		-- don't have a good way to recreate float so we'll take the existing values and then update the db at the end after we truncated the tables
		INSERT INTO tempFloatNewFormulas (funding_id, start_date, float_amount)
		SELECT funding_id, start_date, float_amount
		FROM fact_fundings ff ORDER BY ff.funding_id, ff.start_date
	END





TRUNCATE TABLE fact_fundings
TRUNCATE TABLE fact_fundings_portfolio


-- temp table to store all transactions that have not been used in calculations yet
IF OBJECT_ID('tempdb..#tempTrans') IS NOT NULL DROP TABLE #tempTrans
CREATE TABLE #tempTrans (id int not null identity, funding_id BIGINT, trans_type_id INT, trans_amount MONEY, used BIT, trans_id INT, batch_id INT, trans_date DATE)

-- this temp table will store transactions grouped by funding and transaction type.  Some of the formulas are simple sums which will use this table.
IF OBJECT_ID('tempdb..#tempTransGrouped') IS NOT NULL DROP TABLE #tempTransGrouped
CREATE TABLE #tempTransGrouped (id int not null identity, funding_id BIGINT, trans_type_id INT, trans_amount MONEY)

-- keep one record per funding with all the fields so that at the end, we can do one update on the fundings table of all these calculated fields.
IF OBJECT_ID('tempdb..#tempFundings') IS NOT NULL DROP TABLE #tempFundings
CREATE TABLE #tempFundings (id int not null identity, funding_id BIGINT, cash_fees MONEY, counter_deposit_revenue MONEY, ach_received MONEY, cc_revenue MONEY, 
	wires_revenue MONEY, fees_applied MONEY, settlement_received MONEY, writeoff_received MONEY, cash_to_chargeback MONEY, cash_to_bad_debt MONEY,
	cash_to_reapplication MONEY, cash_from_reapplication MONEY, writeoff_adjustment_received MONEY, ach_draft MONEY, checks_revenue MONEY, ach_reject MONEY,
	refund MONEY, refund_processed MONEY, refund_reapplied MONEY, refund_returned MONEY, continuous_pull MONEY, cash_to_pp MONEY, cash_to_margin MONEY, 
	wires_revenue_no_renewal MONEY, ach_revenue MONEY, pp_adjustment MONEY, margin_adjustment MONEY, pp_applied MONEY, margin_applied MONEY, cash_to_rtr MONEY, rtr_adjustment MONEY, 
	discount_received MONEY, gross_revenue MONEY, applied_amount MONEY, gross_received MONEY, gross_dollars_rec MONEY, rtr_applied MONEY, net_other_income MONEY, total_portfolio_adjustment MONEY, 
	percent_paid NUMERIC (10, 2), rtr_out MONEY, pp_out MONEY, margin_out MONEY, fees_out MONEY, net_revenue MONEY, workdays_since_funded INT, funded_date DATE, payoff_amount MONEY, 
	total_fees_collected MONEY, orig_fee MONEY, paid_off_amount MONEY, portfolio_value MONEY, purchase_price MONEY, unearned_income MONEY, pp_settlement MONEY, margin_settlement MONEY,
	percent_complete NUMERIC (10,2), variance_amount MONEY)
	
SELECT *
INTO #tempFundings_original
FROM fundings 
ORDER BY id

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

UPDATE #tempFundings_original SET ach_draft = NULL, ach_received = NULL, ach_reject = NULL, ach_revenue = NULL, applied_amount = NULL, cash_fees = NULL, cash_from_reapplication = NULL, cash_to_bad_debt = NULL, 
	cash_to_chargeback = NULL, cash_to_margin = NULL, cash_to_pp = NULL, cash_to_reapplication = NULL, cash_to_rtr = NULL, cc_revenue = NULL, checks_revenue = NULL, continuous_pull = NULL, 
	counter_deposit_revenue = NULL, discount_received = NULL, fees_applied = NULL, fees_out = NULL, gross_dollars_rec = NULL, gross_received = NULL, gross_revenue = NULL, margin_adjustment = NULL, 
	margin_applied = NULL, margin_out = NULL, net_other_income = NULL, net_revenue = NULL, paid_off_amount = NULL, pp_adjustment = NULL, pp_applied = NULL, pp_out = NULL, 
	refund = NULL, refund_processed = NULL, refund_reapplied = NULL, refund_returned = NULL, rtr_adjustment = NULL, rtr_applied = NULL, rtr_out = NULL, settlement_received = NULL, 
	total_fees_collected = NULL, total_portfolio_adjustment = NULL, wires_revenue = NULL, wires_revenue_no_renewal = NULL, writeoff_adjustment_received = NULL, writeoff_received = NULL,
	payoff_amount = portfolio_value, estimated_final_payment_date = NULL, pp_settlement = NULL, margin_settlement = NULL

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
-- one record per funding that is on the transactions we are dealing with

INSERT INTO #tempFundings (funding_id)
SELECT id
FROM fundings WITH (NOLOCK)


SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

-- Update a few fields. Pull in a few static fields from the funding that is used in calculations.
UPDATE t
SET funded_date = f.funded_date, orig_fee = f.orig_fee, portfolio_value = f.portfolio_value, purchase_price = f.purchase_price, unearned_income = f.unearned_income
FROM #tempFundings t
INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id


IF OBJECT_ID('tempdb..#tempFacts') IS NOT NULL DROP TABLE #tempFacts
CREATE TABLE #tempFacts ([funding_id] [bigint] NOT NULL, [contract_status] [int] NULL, [payoff_amount] [money] NULL, [frequency] [nvarchar](50) NULL, [weekday_payment] [money] NULL,
	[weekly_payment] [money] NULL, [processor] [varchar](50) NULL, [paid_off_amount] [money] NULL, [last_payment_date] [datetime] NULL, [rtr_out] [money] NULL, [pp_out] [money] NULL,
	[margin_out] [money] NULL, [fees_out] [money] NULL, [percent_paid] [numeric](10, 2) NULL, [percent_performance] [numeric](10, 2) NULL, [on_hold] [bit] NULL, 
	[collection_type] [varchar](50) NULL, [margin_adjustment] [money] NULL, [start_date] [datetime] NOT NULL, [end_date] [datetime] NOT NULL, [float_amount] [money] NULL, 
	[payments_stopped_date] [datetime] NULL, [est_months_left] [numeric](10, 2) NULL, [performance_status_id] [int] NULL, [contract_substatus_id] [int] NULL, [ach_draft] [money] NULL,
	[refund_out] [money] NULL, [create_date] [datetime] NULL, [cash_to_pp] [money] NULL, [cash_to_margin] [money] NULL, [cash_fees] [money] NULL, [counter_deposit_revenue] [money] NULL,
	[ach_received] [money] NULL, [cc_revenue] [money] NULL, [wires_revenue] [money] NULL, [wires_revenue_no_renewal] [money] NULL, [discount_received] [money] NULL, [fees_applied] [money] NULL,
	[settlement_received] [money] NULL, [writeoff_received] [money] NULL, [cash_to_chargeback] [money] NULL, [cash_to_bad_debt] [money] NULL, [cash_to_reapplication] [money] NULL, 
	[cash_from_reapplication] [money] NULL, [writeoff_adjustment_received] [money] NULL, [checks_revenue] [money] NULL, [ach_reject] [money] NULL, [refund] [money] NULL, 
	[refund_processed] [money] NULL, [refund_reapplied] [money] NULL, [refund_returned] [money] NULL, [continuous_pull] [money] NULL, [cash_to_rtr] [money] NULL, [rtr_adjustment] [money] NULL,
	[rtr_applied] [money] NULL, [pp_adjustment] [money] NULL, [pp_applied] [money] NULL, [margin_applied] [money] NULL, [gross_revenue] [money] NULL, [applied_amount] [money] NULL,
	[gross_received] [money] NULL, [net_other_income] [money] NULL, [net_revenue] [money] NULL, [ach_revenue] [money] NULL, [total_fees_collected] [money] NULL, [gross_dollars_rec] [money] NULL,
	[last_trans_amount] [money] NULL, [total_portfolio_adjustment] [money] NULL, [estimated_final_payment_date] [date] NULL, [anticipated_final_payment_date] [date] NULL, 
	[last_draft_date] [datetime] NULL, [end_of_month] BIT NULL, pp_settlement MONEY, margin_settlement MONEY, actual_turn NUMERIC (10, 2), [percent_complete] [numeric](10, 2), variance_amount MONEY)


DECLARE @tempDate DATE, @tomorrow DATETIME
SELECT @tempDate = MIN(funded_date) FROM fundings WITH (NOLOCK) --WHERE id = 710
SELECT @tomorrow = dbo.CF_GetTomorrow(@today)

-- ******************* START LOOP HERE  -----------***************
WHILE @tempDate < @tomorrow
	BEGIN
		TRUNCATE TABLE #tempFacts
		TRUNCATE TABLE #tempTrans
		TRUNCATE TABLE #tempTransGrouped

		-- HAVE TO LOOP THROUGH EACH DAY OF TRANSACTIONS AND FIGURE OUT WHAT IT WAS UP TILL THAT DAY
		-- first insert into the temp table all transactions that are part of settled/closed batches but have not been used in calculations yet
		INSERT INTO #tempTrans (funding_id, trans_type_id, trans_amount, used, trans_id, batch_id, trans_date)
		SELECT t.funding_id, t.trans_type_id, t.trans_amount, 0, t.trans_id, b.batch_id, t.trans_date
		FROM transactions t WITH (NOLOCK)
		LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id		
		WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 
			AND t.trans_date <= @tempDate
			-- DO ALL TRANSACTIONS
			--AND t.funding_id = 710
		-- ORDER BY t.trans_date

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		-- store transactions also grouped by funding and trans type.  some formulas are simpler and can use this data.
		INSERT INTO #tempTransGrouped (funding_id, trans_type_id, trans_amount)
		SELECT t.funding_id, t.trans_type_id, SUM(t.trans_amount)
		FROM #tempTrans t
		GROUP BY t.funding_id, t.trans_type_id
		ORDER BY t.funding_id, t.trans_type_id

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		-- First, do the easy ones that can be pulled from the group by table.  Because they are MAIN transaction types.  Should be one record per funding/trans type combo
		UPDATE f
		SET counter_deposit_revenue = x.counter_deposit_revenue, ach_received = x.ach_received, cc_revenue = x.cc_revenue, wires_revenue = x.wires_revenue, fees_applied = x.fees_applied,
			settlement_received = x.settlement_received, writeoff_received = x.writeoff_received, cash_to_chargeback = ABS(x.cash_to_chargeback), writeoff_adjustment_received = x.writeoff_adjustment_received,
			ach_draft = x.ach_draft, checks_revenue = x.checks_revenue, ach_reject = ABS(x.ach_reject), refund = ABS(x.refund), refund_processed = x.refund_processed, refund_reapplied = x.refund_reapplied,
			refund_returned = x.refund_returned, continuous_pull = x.continuous_pull, discount_received = x.discount_received 
		FROM #tempFundings f WITH (NOLOCK)
		INNER JOIN (
			SELECT DISTINCT funding_id,  
				(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 2 AND funding_id = t.funding_id) AS counter_deposit_revenue,
				(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 3 AND funding_id = t.funding_id) AS ach_received,
				(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 4 AND funding_id = t.funding_id) AS cc_revenue,
				(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 5 AND funding_id = t.funding_id) AS wires_revenue,
				(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 8 AND funding_id = t.funding_id) AS discount_received,
				(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 9 AND funding_id = t.funding_id) AS fees_applied,
				(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 10 AND funding_id = t.funding_id) AS settlement_received,
				(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 11 AND funding_id = t.funding_id) AS writeoff_received,
				(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 14 AND funding_id = t.funding_id) AS cash_to_chargeback,
				(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 17 AND funding_id = t.funding_id) AS writeoff_adjustment_received,
				(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 18 AND funding_id = t.funding_id) AS ach_draft,
				(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 19 AND funding_id = t.funding_id) AS checks_revenue,
				(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 21 AND funding_id = t.funding_id) AS ach_reject,
				(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 22 AND funding_id = t.funding_id) AS refund,
				(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 23 AND funding_id = t.funding_id) AS refund_processed,
				(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 24 AND funding_id = t.funding_id) AS refund_reapplied,
				(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 25 AND funding_id = t.funding_id) AS refund_returned,
				(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 26 AND funding_id = t.funding_id) AS continuous_pull
			FROM #tempTransGrouped t	
		)  x ON f.funding_id = x.funding_id


		UPDATE f
		SET cash_to_pp =  COALESCE(cash_to_pp.cash_to_pp, 0), 
			cash_to_margin = COALESCE(cash_to_margin.cash_to_margin, 0),
			wires_revenue_no_renewal = COALESCE(wires_no_renewal.wires_no_renewal, 0),
			cash_to_bad_debt = COALESCE(cash_to_bad_debt.cash_to_bad_debt, 0),
			cash_to_reapplication = ABS(COALESCE(cash_to_reapplication.cash_to_reapplication, 0)),
			cash_from_reapplication = COALESCE(cash_from_reapplication.cash_from_reapplication, 0),
			ach_revenue = COALESCE(ach_revenue.ach_revenue, 0), 
			pp_settlement = COALESCE(pp_settlement.pp_settlement, 0),
			margin_settlement = COALESCE(margin_settlement.margin_settlement, 0),
			cash_fees = COALESCE(cash_fees.cash_fees, 0)
		FROM #tempFundings f WITH (NOLOCK)
		LEFT JOIN (
			SELECT tt.funding_id, SUM(d.trans_amount) AS cash_to_pp
			FROM #tempTrans tt WITH (NOLOCK)
			INNER JOIN transaction_details d WITH (NOLOCK) ON tt.trans_id = d.trans_id	
			WHERE tt.trans_type_id IN (1, 2, 3, 4, 5, 14, 16, 18, 19, 21, 22, 24) -- actually received (cash to pp)  -- 1 was used to fix some trans so need to put it here
				AND d.trans_type_id = 7
			GROUP BY tt.funding_id
		) cash_to_pp ON f.funding_id = cash_to_pp.funding_id
		LEFT JOIN (
			SELECT tt.funding_id, SUM(d.trans_amount) AS cash_to_margin
			FROM #tempTrans tt WITH (NOLOCK)
			INNER JOIN transaction_details d WITH (NOLOCK) ON tt.trans_id = d.trans_id	
			WHERE tt.trans_type_id IN (1, 2, 3, 4, 5, 14, 16, 18, 19, 21, 22, 24) -- actually received (cash to margin) -- 1 was used to fix some trans so need to put it here
				AND d.trans_type_id = 6
			GROUP BY tt.funding_id
		) cash_to_margin ON f.funding_id = cash_to_margin.funding_id
		LEFT JOIN (
			SELECT tt.funding_id, SUM(tt.trans_amount) AS wires_no_renewal
			FROM #tempTrans tt WITH (NOLOCK)
			LEFT JOIN batches b WITH (NOLOCK) ON tt.batch_id = b.batch_id
			WHERE tt.trans_type_id = 5 AND COALESCE(b.batch_type, '') <> 'ContractRenewal'
			GROUP BY tt.funding_id
		) wires_no_renewal ON f.funding_id = wires_no_renewal.funding_id
		LEFT JOIN (
			SELECT tt.funding_id, SUM(d.trans_amount) AS cash_to_bad_debt
			FROM #tempTrans tt WITH (NOLOCK)
			INNER JOIN transaction_details d WITH (NOLOCK) ON tt.trans_id = d.trans_id
			WHERE d.trans_type_id = 15 
			GROUP BY tt.funding_id
		) cash_to_bad_debt ON f.funding_id = cash_to_bad_debt.funding_id
		LEFT JOIN (
			SELECT tt.funding_id, SUM(tt.trans_amount) AS cash_to_reapplication
			FROM #tempTrans tt WITH (NOLOCK)	
			WHERE tt.trans_type_id = 16 AND tt.trans_amount < 0
			GROUP BY tt.funding_id
		) cash_to_reapplication ON f.funding_id = cash_to_reapplication.funding_id
		LEFT JOIN (
			SELECT tt.funding_id, SUM(tt.trans_amount) AS cash_from_reapplication
			FROM #tempTrans tt WITH (NOLOCK)	
			WHERE tt.trans_type_id = 16 AND tt.trans_amount > 0
			GROUP BY tt.funding_id
		) cash_from_reapplication ON f.funding_id = cash_from_reapplication.funding_id
		LEFT JOIN (
			SELECT tt.funding_id, SUM(tt.trans_amount) AS ach_revenue
			FROM #tempTrans tt WITH (NOLOCK)	
			WHERE ((tt.trans_type_id = 3 AND tt.trans_date < '2019-01-01')
				OR (tt.trans_type_id = 18 AND tt.trans_date >= '2019-01-01'))
			GROUP BY tt.funding_id
		) ach_revenue ON f.funding_id = ach_revenue.funding_id
		LEFT JOIN (
			SELECT tt.funding_id, SUM(d.trans_amount) AS pp_settlement
			FROM #tempTrans tt WITH (NOLOCK)
			INNER JOIN transaction_details d WITH (NOLOCK) ON tt.trans_id = d.trans_id	
			WHERE tt.trans_type_id IN (10) -- actually received (cash to pp) -- 1 was used to fix some trans so need to put it here
				AND d.trans_type_id = 7
			GROUP BY tt.funding_id
		) pp_settlement ON f.funding_id = pp_settlement.funding_id
		LEFT JOIN (
			SELECT tt.funding_id, SUM(d.trans_amount) AS margin_settlement
			FROM #tempTrans tt WITH (NOLOCK)
			INNER JOIN transaction_details d WITH (NOLOCK) ON tt.trans_id = d.trans_id	
			WHERE tt.trans_type_id IN (10) -- actually received (cash to pp) -- 1 was used to fix some trans so need to put it here
				AND d.trans_type_id = 6
			GROUP BY tt.funding_id
		) margin_settlement ON f.funding_id = margin_settlement.funding_id
		LEFT JOIN (
			SELECT tt.funding_id, SUM(d.trans_amount) AS cash_fees
			FROM #tempTrans tt WITH (NOLOCK)
			INNER JOIN transaction_details d WITH (NOLOCK) ON tt.trans_id = d.trans_id	
			WHERE d.trans_type_id = 1
			GROUP BY tt.funding_id
		) cash_fees ON f.funding_id = cash_fees.funding_id


		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		
		UPDATE #tempFundings
		SET pp_adjustment = COALESCE(writeoff_received, 0) + COALESCE(pp_settlement, 0), 
			margin_adjustment = COALESCE(discount_received, 0) + COALESCE(margin_settlement, 0) + COALESCE(writeoff_adjustment_received, 0)


		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR



		UPDATE #tempFundings
		SET pp_applied = COALESCE(cash_to_pp, 0) + COALESCE(pp_adjustment, 0), margin_applied = COALESCE(cash_to_margin, 0) + COALESCE(margin_adjustment, 0), 
			cash_to_rtr = COALESCE(cash_to_pp, 0) + COALESCE(cash_to_margin, 0), rtr_adjustment = COALESCE(discount_received, 0) + COALESCE(writeoff_received, 0) + COALESCE(settlement_received, 0) +
			COALESCE(writeoff_adjustment_received, 0),
			gross_revenue = COALESCE(orig_fee, 0) + COALESCE(cash_fees, 0)  + COALESCE(cash_to_margin, 0) + COALESCE(cash_to_pp, 0)  + COALESCE(cash_to_bad_debt, 0), 
			gross_received = COALESCE(counter_deposit_revenue, 0) + COALESCE(ach_revenue, 0) + COALESCE(cc_revenue, 0) + COALESCE(wires_revenue, 0) + COALESCE(checks_revenue, 0),
			gross_dollars_rec = COALESCE(counter_deposit_revenue, 0) + COALESCE(ach_revenue, 0) + COALESCE(cc_revenue, 0) + COALESCE(wires_revenue_no_renewal, 0) + COALESCE(checks_revenue, 0)

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR



		UPDATE #tempFundings
		SET rtr_applied = COALESCE(cash_to_rtr, 0) + COALESCE(rtr_adjustment, 0), net_other_income = COALESCE(gross_dollars_rec, 0) - COALESCE(cash_to_rtr, 0), 
			total_portfolio_adjustment = COALESCE(margin_adjustment, 0) + COALESCE(pp_adjustment, 0),
			percent_paid = COALESCE(cash_to_rtr, 0) / portfolio_value * 100.00,
			applied_amount = COALESCE(cash_fees, 0) + COALESCE(margin_applied, 0) + COALESCE(pp_applied, 0) + COALESCE(cash_to_bad_debt, 0) + COALESCE(discount_received, 0) 
				- COALESCE(cash_to_chargeback, 0)

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
		
		UPDATE f
		SET percent_complete = COALESCE(x.paid_off, 0) / f.portfolio_value * 100.00
		FROM #tempFundings f
		LEFT JOIN (
			SELECT f.id AS funding_id, SUM(CASE WHEN d.trans_type_id IN (1, 6, 7, 15, 8, 14) THEN d.trans_amount -- 11, 8
								WHEN d.trans_type_id IN (9) THEN -1 * d.trans_amount 
							  ELSE 0 END) AS paid_off
			FROM fundings f WITH (NOLOCK)
			INNER JOIN #tempTrans t WITH (NOLOCK) ON f.id = t.funding_id
			INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id
			GROUP BY f.id
		) x ON f.funding_id = x.funding_id
		WHERE COALESCE(percent_complete, 0) <> COALESCE(x.paid_off, 0) / f.portfolio_value * 100.00

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


		UPDATE #tempFundings
		SET rtr_out = COALESCE(portfolio_value, 0) - COALESCE(rtr_applied, 0), pp_out = COALESCE(purchase_price, 0) - COALESCE(pp_applied, 0),
			margin_out = COALESCE(unearned_income, 0) - COALESCE(margin_applied, 0), fees_out = COALESCE(fees_applied, 0) - COALESCE(cash_fees, 0), 
			net_revenue = COALESCE(cash_to_margin, 0) + COALESCE(net_other_income, 0)
		-- FROM #tempFundings f WITH (NOLOCK)


		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


		UPDATE #tempFundings
		SET workdays_since_funded = DATEDIFF(DAY, funded_date, @today) + 1 - (DATEDIFF(WEEK, funded_date, @today) * 2)
			+ CASE WHEN DATEPART(WEEKDAY, DATEADD(DAY, @@datefirst, funded_date)) = 1 THEN -1 ELSE 0 END
			+ CASE WHEN DATEPART(WEEKDAY, DATEADD(DAY, @@datefirst, GETDATE())) = 7 THEN -1 ELSE 0 END,
			payoff_amount = COALESCE(rtr_out, 0) + COALESCE(fees_out, 0),
			total_fees_collected = COALESCE(orig_fee, 0) + COALESCE(cash_fees, 0), paid_off_amount = portfolio_value - COALESCE(rtr_out, 0)	


		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		-- update past due amount
		UPDATE f
		SET variance_amount = COALESCE(x.total_expected, 0) - COALESCE(f.cash_to_rtr, 0)
		FROM #tempFundings f WITH (NOLOCK)
		INNER JOIN (
			SELECT d.funding_id, SUM(d.amount) AS total_expected
			FROM funding_payment_dates_original d WITH (NOLOCK)
			WHERE d.payment_date <= @tempDate
			GROUP BY d.funding_id
		) x ON f.funding_id = x.funding_id
		WHERE COALESCE(variance_amount, 0) <> COALESCE(x.total_expected, 0) - COALESCE(f.cash_to_rtr, 0)

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR




	--	SELECT * FROM #tempFundings


		-- this might be where we need to update fundings table with all temp values from #tempFundings
		UPDATE f
		SET ach_draft = COALESCE(t.ach_draft, 0), ach_received = COALESCE(t.ach_received, 0), 
			ach_reject = COALESCE(t.ach_reject, 0), ach_revenue = COALESCE(t.ach_revenue, 0), 
			applied_amount = COALESCE(t.applied_amount, 0), cash_fees = COALESCE(t.cash_fees, 0), 
			cash_from_reapplication = COALESCE(t.cash_from_reapplication, 0), cash_to_bad_debt = COALESCE(t.cash_to_bad_debt, 0), 
			cash_to_chargeback = COALESCE(t.cash_to_chargeback, 0), workdays_since_funded = COALESCE(t.workdays_since_funded, 0), 
			cash_to_margin = COALESCE(t.cash_to_margin, 0), cash_to_pp = COALESCE(t.cash_to_pp, 0), 
			cash_to_reapplication = COALESCE(t.cash_to_reapplication, 0), cash_to_rtr = COALESCE(t.cash_to_rtr, 0) , 
			cc_revenue = COALESCE(t.cc_revenue, 0), checks_revenue = COALESCE(t.checks_revenue, 0), 
			continuous_pull = COALESCE(t.continuous_pull, 0), counter_deposit_revenue = COALESCE(t.counter_deposit_revenue, 0), 
			discount_received = COALESCE(t.discount_received, 0), fees_applied = COALESCE(t.fees_applied, 0), 
			fees_out = COALESCE(t.fees_out, 0), gross_dollars_rec = COALESCE(t.gross_dollars_rec, 0), 
			gross_received = COALESCE(t.gross_received, 0), gross_revenue = COALESCE(t.gross_revenue, 0),
			margin_adjustment = COALESCE(t.margin_adjustment, 0), margin_applied = COALESCE(t.margin_applied, 0), 
			margin_out = COALESCE(t.margin_out, 0), net_other_income = COALESCE(t.net_other_income, 0), 
			net_revenue = COALESCE(t.net_revenue, 0), paid_off_amount = COALESCE(t.paid_off_amount, 0), 
			percent_paid = COALESCE(t.percent_paid, 0),
			percent_complete = COALESCE(t.percent_complete, 0),
			pp_adjustment = COALESCE(t.pp_adjustment, 0), pp_applied = COALESCE(t.pp_applied, 0), 
			pp_out = COALESCE(t.pp_out, 0), refund = COALESCE(t.refund, 0), 
			refund_processed = COALESCE(t.refund_processed, 0), refund_reapplied = COALESCE(t.refund_reapplied, 0), 
			refund_returned = COALESCE(t.refund_returned, 0), rtr_adjustment = COALESCE(t.rtr_adjustment, 0), 
			rtr_applied = COALESCE(t.rtr_applied, 0), rtr_out = COALESCE(t.rtr_out, 0), 
			settlement_received = COALESCE(t.settlement_received, 0), total_fees_collected = COALESCE(t.total_fees_collected, 0), 
			total_portfolio_adjustment = COALESCE(t.total_portfolio_adjustment, 0), wires_revenue = COALESCE(t.wires_revenue, 0), 
			wires_revenue_no_renewal = COALESCE(t.wires_revenue_no_renewal, 0), 
			writeoff_adjustment_received = COALESCE(t.writeoff_adjustment_received, 0), 
			writeoff_received = COALESCE(t.writeoff_received, 0),
			pp_settlement = COALESCE(t.pp_settlement, 0),
			margin_settlement = COALESCE(t.margin_settlement, 0), 
			variance_amount = COALESCE(t.variance_amount, 0)
		FROM #tempFundings_original f 
		INNER JOIN #tempFundings t ON f.id = t.funding_id

		UPDATE #tempFundings_original SET payoff_amount = COALESCE(rtr_out, 0) + COALESCE(fees_out, 0)
		WHERE COALESCE(payoff_amount, 0) <> COALESCE(rtr_out, 0) + COALESCE(fees_out, 0)

	--	SELECT rtr_out, fees_out, payoff_amount, rtr_applied, cash_to_rtr, rtr_adjustment, * FROM #tempFundings WHERE funding_id = 183
		--SELECT rtr_out, fees_out, payoff_amount, rtr_applied, cash_to_rtr, rtr_adjustment, * FROM #tempFundings_original WHERE funding_id = 183


		-- actual turn is always changing so update separately each time
		UPDATE #tempFundings_original
		SET actual_turn = CASE WHEN 
							ABS(CASE WHEN DATEDIFF(DD, funded_date, @tempDate) <= 3  AND frequency = 'Daily' THEN estimated_turn
								WHEN DATEDIFF(DD, funded_date, @tempDate) <= 10 AND frequency = 'Weekly' THEN estimated_turn
								ELSE (portfolio_value / ((COALESCE(cash_to_rtr, 0) / COALESCE(workdays_since_funded, 1)) + 0.0000001)) / 21
							END) > 60  -- shouldn't have negative cash_to_rtr but it has happened (I think it may have been miscalculations)
							THEN 60
							ELSE 
								CASE WHEN DATEDIFF(DD, funded_date, @tempDate) <= 3  AND frequency = 'Daily' THEN estimated_turn
									WHEN DATEDIFF(DD, funded_date, @tempDate) <= 10 AND frequency = 'Weekly' THEN estimated_turn
									ELSE (portfolio_value / ((COALESCE(cash_to_rtr, 0) / COALESCE(workdays_since_funded, 1)) + 0.0000001)) / 21
								END
							END
		WHERE funded_date IS NOT NULL AND COALESCE(completed_date, '2050-01-01') > @tempDate
			AND COALESCE(actual_turn, -1) <> (
						  CASE WHEN 
							ABS(CASE WHEN DATEDIFF(DD, funded_date, @tempDate) <= 3  AND frequency = 'Daily' THEN estimated_turn
								WHEN DATEDIFF(DD, funded_date, @tempDate) <= 10 AND frequency = 'Weekly' THEN estimated_turn
								ELSE (portfolio_value / ((COALESCE(cash_to_rtr, 0) / COALESCE(workdays_since_funded, 1)) + 0.0000001)) / 21
							END) > 60
							THEN 60
							ELSE 
								CASE WHEN DATEDIFF(DD, funded_date, @tempDate) <= 3  AND frequency = 'Daily' THEN estimated_turn
									WHEN DATEDIFF(DD, funded_date, @tempDate) <= 10 AND frequency = 'Weekly' THEN estimated_turn
									ELSE (portfolio_value / ((COALESCE(cash_to_rtr, 0) / COALESCE(workdays_since_funded, 1)) + 0.0000001)) / 21
								END
							END
			)


		UPDATE #tempFundings_original
		SET percent_performance = (estimated_turn - actual_turn) / estimated_turn * 100.00
		WHERE actual_turn IS NOT NULL
			AND (
				COALESCE(percent_performance, 0) <> ((estimated_turn - actual_turn) / estimated_turn * 100.00)	
			)

		UPDATE #tempFundings_original
		SET anticipated_final_payment_date = DATEADD(DD, actual_turn * 30.4, funded_date)
		WHERE actual_turn IS NOT NULL
			AND funded_date IS NOT NULL
			AND COALESCE(anticipated_final_payment_date, '2000-01-01') <> DATEADD(DD, actual_turn * 30.4, funded_date)


		UPDATE #tempFundings_original
		SET est_months_left = DATEDIFF(DD, @tempDate, anticipated_final_payment_date)/30.4
		WHERE COALESCE(completed_date, '2050-01-01') > @tempDate
			AND anticipated_final_payment_date IS NOT NULL
			AND COALESCE(est_months_left, 0) <> DATEDIFF(DD, @today, anticipated_final_payment_date)/30.4

		UPDATE #tempFundings_original
		SET last_payment_date = x.last_payment_date
		FROM #tempFundings_original f WITH (NOLOCK)
		INNER JOIN (
			SELECT t.funding_id, MAX(t.trans_date) AS last_payment_date
			FROM #tempTrans t	 WITH (NOLOCK)
			WHERE t.trans_type_id IN (2, 3, 4, 5, 19) AND t.trans_amount <> 0 		
			GROUP BY t.funding_id
		) x ON f.id = x.funding_id
		WHERE COALESCE(f.last_payment_date, '2000-01-01') != COALESCE(x.last_payment_date, '2000-01-01')


		UPDATE #tempFundings_original
		SET last_trans_amount = t.trans_amount
		FROM #tempFundings_original f WITH (NOLOCK)
		INNER JOIN #tempTrans t WITH (NOLOCK) ON f.id = t.funding_id AND t.trans_type_id IN (2, 3, 4, 5, 19) AND t.trans_amount <> 0 
			AND t.trans_date = f.last_payment_date
		WHERE COALESCE(f.last_trans_amount, -1) != COALESCE(t.trans_amount, -1)


		UPDATE #tempFundings_original
		SET first_received_date = x.first_date
		FROM #tempFundings_original f WITH (NOLOCK)
		INNER JOIN (
			SELECT t.funding_id, MIN(t.trans_date) AS first_date
			FROM #tempTrans t	 WITH (NOLOCK)	
			WHERE t.trans_type_id IN (2, 3, 4, 5, 19) AND t.trans_amount <> 0
			GROUP BY t.funding_id
		) x ON f.id = x.funding_id
		WHERE first_received_date <> x.first_date



		UPDATE #tempFundings_original
		SET last_draft_date = x.last_payment_date
		FROM #tempFundings_original f WITH (NOLOCK)
		INNER JOIN (
			SELECT t.funding_id, MAX(t.trans_date) AS last_payment_date
			FROM #tempTrans t	 WITH (NOLOCK)	
			WHERE t.trans_type_id IN (2, 18, 4, 5, 19) AND t.trans_amount <> 0
			GROUP BY t.funding_id
		) x ON f.id = x.funding_id
		WHERE COALESCE(f.last_draft_date, '2000-01-01') != COALESCE(x.last_payment_date, '2000-01-01')



		UPDATE #tempFundings_original
		SET first_draft_date = x.first_date
		FROM #tempFundings_original f WITH (NOLOCK)
		INNER JOIN (
			SELECT t.funding_id, MIN(t.trans_date) AS first_date
			FROM #tempTrans t	 WITH (NOLOCK)	
			WHERE t.trans_type_id IN (2, 18, 4, 5, 19) AND t.trans_amount <> 0
			GROUP BY t.funding_id
		) x ON f.id = x.funding_id
		WHERE first_draft_date <> x.first_date

		/*
		-- now do the float
		UPDATE f 
		SET float_amount = COALESCE(x.total_float, 0) + COALESCE(y.total_float, 0)
		FROM fundings f WITH (NOLOCK)
		-- get float from batches
		LEFT JOIN (
			SELECT f2.id, SUM(CASE WHEN t.trans_type_id IN (9, 14, 17) THEN -1* t.trans_amount ELSE t.trans_amount END) AS total_float
			FROM fundings f2 WITH (NOLOCK)
			JOIN transactions t WITH (NOLOCK) ON f2.id = t.funding_id
			INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
			WHERE b.batch_status IN ('Processed') AND b.batch_type NOT IN ('ForteAPI') -- if it IS ForteAPI, we've already distributed the money so it is not in float.
			GROUP BY f2.id
		) x ON f.id = x.id
		LEFT JOIN (
		-- now get float from forte trans
			SELECT SUM(t.authorization_amount) AS total_float, f.id
			FROM forte_trans t WITH (NOLOCK)
			INNER JOIN fundings f WITH (NOLOCK) ON t.customer_id = f.contract_number
			LEFT JOIN forte_trans t2 WITH (NOLOCK) ON t.transaction_id = t2.transaction_id AND t2.status <> 'settling'
			WHERE t.status = 'settling' AND t2.forte_trans_id IS NULL -- this makes sure it does not have a matching record (which would mean there is an update to the settling)
			GROUP BY f.id
		) y ON f.id = y.id
		WHERE ((f.float_amount <> COALESCE(x.total_float, 0) + COALESCE(y.total_float, 0)) 
			OR f.float_amount IS NULL)
			AND f.contract_status = 2
*/

		-- RKB  2019-10-17
		-- completed contracts may still have an outstanding balance and refund_out needs to be updated 
		UPDATE f
		SET refund_out = COALESCE(refund.refund_out, 0)
		FROM #tempFundings_original f WITH (NOLOCK)
		LEFT JOIN (
			SELECT t.funding_id, SUM(t.trans_amount) AS refund_out
			FROM #tempTrans t WITH (NOLOCK)		
			WHERE t.trans_type_id IN (22,23, 25) -- 24 is not included because 23 is money leaving the account, 24 is money going on to an account
			GROUP BY t.funding_id
		) refund ON f.id = refund.funding_id
		WHERE COALESCE(f.refund_out, -999) <> COALESCE(refund.refund_out, 999)
			AND COALESCE(f.refund_out, 0) <> 0	
	
	/*
		-- Estimated final payment date
		UPDATE f
		SET estimated_final_payment_date = x.final_date
		FROM #tempFundings_original f WITH (NOLOCK)
		INNER JOIN (
			SELECT MAX(payment_date) AS final_date, funding_id
			FROM payment_forecasts WITH (NOLOCK)
			GROUP BY funding_id
		) x ON f.id = x.funding_id
		WHERE f.contract_status IN (2) 
			AND COALESCE(estimated_final_payment_date, '2001-01-01') <> x.final_date
			*/


		--insert into the temp table data up till today
		INSERT INTO #tempFacts (funding_id, contract_status, payoff_amount, frequency, weekday_payment, weekly_payment, processor, paid_off_amount, last_payment_date, rtr_out, pp_out, margin_out, 
			fees_out, percent_paid, percent_performance, on_hold, collection_type, margin_adjustment, start_date, end_date, float_amount, payments_stopped_date, est_months_left, performance_status_id, 
			contract_substatus_id, ach_draft, refund_out, create_date, cash_to_pp, cash_to_margin, cash_fees, counter_deposit_revenue, ach_received, cc_revenue, wires_revenue, wires_revenue_no_renewal,
			discount_received, fees_applied, settlement_received, writeoff_received, cash_to_chargeback, cash_to_bad_debt, cash_to_reapplication, cash_from_reapplication, writeoff_adjustment_received, 
			checks_revenue, ach_reject, refund, refund_processed, refund_reapplied, refund_returned, continuous_pull, cash_to_rtr, rtr_adjustment, rtr_applied, pp_adjustment, pp_applied, 
			margin_applied, gross_revenue, applied_amount, gross_received, net_other_income, net_revenue, ach_revenue, total_fees_collected, gross_dollars_rec, last_trans_amount, 
			total_portfolio_adjustment, estimated_final_payment_date, anticipated_final_payment_date, last_draft_date, end_of_month, pp_settlement, margin_settlement, actual_turn, percent_complete, 
			variance_amount)
		SELECT id, contract_status, payoff_amount, frequency, weekday_payment, weekly_payment, processor, paid_off_amount, last_payment_date, rtr_out, pp_out, margin_out, 
			fees_out, percent_paid, percent_performance, on_hold, collection_type, margin_adjustment, @tempDate, @endDate, float_amount, payments_stopped_date, est_months_left, performance_status_id, 
			contract_substatus_id, ach_draft, refund_out, GETUTCDATE(), cash_to_pp, cash_to_margin, cash_fees, counter_deposit_revenue, ach_received, cc_revenue, wires_revenue, wires_revenue_no_renewal,
			discount_received, fees_applied, settlement_received, writeoff_received, cash_to_chargeback, cash_to_bad_debt, cash_to_reapplication, cash_from_reapplication, writeoff_adjustment_received, 
			checks_revenue, ach_reject, refund, refund_processed, refund_reapplied, refund_returned, continuous_pull, cash_to_rtr, rtr_adjustment, rtr_applied, pp_adjustment, pp_applied, 
			margin_applied, gross_revenue, applied_amount, gross_received, net_other_income, net_revenue, ach_revenue, total_fees_collected, gross_dollars_rec, last_trans_amount, 
			total_portfolio_adjustment, estimated_final_payment_date, anticipated_final_payment_date, last_draft_date, 
			IIF(@tempDate = EOMONTH(@tempDate), 1, 0), pp_settlement, margin_settlement, actual_turn, percent_complete, variance_amount
		FROM #tempFundings_original f WITH (NOLOCK)
		WHERE f.funded_date <= @tempDate

		
		-- now the temp table stores all data to date.  need to compare each funding to the last entry in the fact_fundings and if any field is different
		-- we need to insert a new record
		INSERT INTO fact_fundings(funding_id, contract_status, payoff_amount, frequency, weekday_payment, weekly_payment, processor, paid_off_amount, last_payment_date, rtr_out, pp_out, margin_out, 
			fees_out, percent_paid, percent_performance, on_hold, collection_type, margin_adjustment, start_date, end_date, float_amount, payments_stopped_date, est_months_left, performance_status_id, 
			contract_substatus_id, ach_draft, refund_out, create_date, cash_to_pp, cash_to_margin, cash_fees, counter_deposit_revenue, ach_received, cc_revenue, wires_revenue, wires_revenue_no_renewal,
			discount_received, fees_applied, settlement_received, writeoff_received, cash_to_chargeback, cash_to_bad_debt, cash_to_reapplication, cash_from_reapplication, writeoff_adjustment_received, 
			checks_revenue, ach_reject, refund, refund_processed, refund_reapplied, refund_returned, continuous_pull, cash_to_rtr, rtr_adjustment, rtr_applied, pp_adjustment, pp_applied, 
			margin_applied, gross_revenue, applied_amount, gross_received, net_other_income, net_revenue, ach_revenue, total_fees_collected, gross_dollars_rec, last_trans_amount, 
			total_portfolio_adjustment, estimated_final_payment_date, anticipated_final_payment_date, last_draft_date, end_of_month, pp_settlement, margin_settlement, actual_turn, percent_complete, 
			variance_amount) 
		SELECT t.funding_id, t.contract_status, t.payoff_amount, t.frequency, t.weekday_payment, t.weekly_payment, t.processor, t.paid_off_amount, t.last_payment_date, t.rtr_out, t.pp_out, t.margin_out, 
			t.fees_out, t.percent_paid, t.percent_performance, t.on_hold, t.collection_type, t.margin_adjustment, @tempDate, @endDate, t.float_amount, t.payments_stopped_date, t.est_months_left, 
			t.performance_status_id, t.contract_substatus_id, t.ach_draft, t.refund_out, t.create_date, t.cash_to_pp, t.cash_to_margin, t.cash_fees, t.counter_deposit_revenue, t.ach_received, 
			t.cc_revenue, t.wires_revenue, t.wires_revenue_no_renewal, t.discount_received, t.fees_applied, t.settlement_received, t.writeoff_received, t.cash_to_chargeback, t.cash_to_bad_debt, 
			t.cash_to_reapplication, t.cash_from_reapplication, t.writeoff_adjustment_received, t.checks_revenue, t.ach_reject, t.refund, t.refund_processed, t.refund_reapplied, t.refund_returned, 
			t.continuous_pull, t.cash_to_rtr, t.rtr_adjustment, t.rtr_applied, t.pp_adjustment, t.pp_applied, t.margin_applied, t.gross_revenue, t.applied_amount, t.gross_received, t.net_other_income, 
			t.net_revenue, t.ach_revenue, t.total_fees_collected, t.gross_dollars_rec, t.last_trans_amount, t.total_portfolio_adjustment, t.estimated_final_payment_date, 
			t.anticipated_final_payment_date, t.last_draft_date, t.end_of_month, t.pp_settlement, t.margin_settlement, t.actual_turn, t.percent_complete, t.variance_amount
		FROM #tempFacts t
		LEFT JOIN fact_fundings f ON t.funding_id = f.funding_id AND f.end_date = @endDate
		WHERE f.funding_id IS NULL
			OR (
				t.funding_id = f.funding_id 
				AND (			   
					   t.contract_status <> f.contract_status
					OR COALESCE(t.payoff_amount, 0) <> COALESCE(f.payoff_amount, 0)
					OR t.frequency <> f.frequency
					OR COALESCE(t.weekday_payment, 0) <> COALESCE(f.weekday_payment, 0)
					OR COALESCE(t.weekly_payment, 0) <> COALESCE(f.weekly_payment, 0)
					OR COALESCE(t.processor, '') <> COALESCE( f.processor, '')
					OR COALESCE(t.paid_off_amount, 0) <> COALESCE(f.paid_off_amount, 0)
					OR COALESCE(t.last_payment_date, '1900-01-01') <> COALESCE(f.last_payment_date, '1900-01-01')
					OR COALESCE(t.rtr_out, 0) <> COALESCE(f.rtr_out, 0)
					OR COALESCE(t.pp_out, 0) <> COALESCE(f.pp_out, 0)
					OR COALESCE(t.margin_out, 0) <> COALESCE(f.margin_out, 0)
					OR COALESCE(t.fees_out, 0) <> COALESCE(f.fees_out, 0)
					OR COALESCE(t.percent_paid, 0) <> COALESCE(f.percent_paid, 0)
					OR COALESCE(t.percent_complete, 0) <> COALESCE(f.percent_complete, 0)
					OR COALESCE(t.percent_performance, 0) <> COALESCE(f.percent_performance, 0)
					OR COALESCE(t.on_hold, 0) <> COALESCE(f.on_hold, 0)
					OR COALESCE(t.collection_type, '') <> COALESCE(f.collection_type, '')
					OR COALESCE(t.margin_adjustment, 0) <> COALESCE(f.margin_adjustment, 0)
					OR COALESCE(t.float_amount, 0) <> COALESCE(f.float_amount, 0)
					OR COALESCE(t.payments_stopped_date, '1900-01-01') <> COALESCE(f.payments_stopped_date, '1900-01-01')
					OR COALESCE(t.est_months_left, 0) <> COALESCE(f.est_months_left, 0)			
					OR COALESCE(t.performance_status_id, -1) <> COALESCE(f.performance_status_id, -1)
					OR COALESCE(t.contract_substatus_id, -1) <> COALESCE(f.contract_substatus_id, -1)
					OR COALESCE(t.ach_draft, 0) <> COALESCE(f.ach_draft, 0)						
					OR COALESCE(t.cash_to_pp, 0) <> COALESCE(f.cash_to_pp, 0)
					OR COALESCE(t.cash_to_margin, 0) <> COALESCE(f.cash_to_margin, 0)
					OR COALESCE(t.cash_fees, 0) <> COALESCE(f.cash_fees, 0)
					OR COALESCE(t.counter_deposit_revenue, 0) <> COALESCE(f.counter_deposit_revenue, 0)
					OR COALESCE(t.ach_received, 0) <> COALESCE(f.ach_received, 0)
					OR COALESCE(t.cc_revenue, 0) <> COALESCE(f.cc_revenue, 0)
					OR COALESCE(t.wires_revenue, 0) <> COALESCE(f.wires_revenue, 0)
					OR COALESCE(t.wires_revenue_no_renewal, 0) <> COALESCE(f.wires_revenue_no_renewal, 0)
					OR COALESCE(t.discount_received, 0) <> COALESCE(f.discount_received, 0)
					OR COALESCE(t.fees_applied, 0) <> COALESCE(f.fees_applied, 0)
					OR COALESCE(t.settlement_received, 0) <> COALESCE(f.settlement_received, 0)
					OR COALESCE(t.writeoff_received, 0) <> COALESCE(f.writeoff_received, 0)
					OR COALESCE(t.cash_to_chargeback, 0) <> COALESCE(f.cash_to_chargeback, 0)
					OR COALESCE(t.cash_to_bad_debt, 0) <> COALESCE(f.cash_to_bad_debt, 0)
					OR COALESCE(t.cash_to_reapplication, 0) <> COALESCE(f.cash_to_reapplication, 0)
					OR COALESCE(t.cash_from_reapplication, 0) <> COALESCE(f.cash_from_reapplication, 0)
					OR COALESCE(t.writeoff_adjustment_received, 0) <> COALESCE(f.writeoff_adjustment_received, 0)
					OR COALESCE(t.checks_revenue, 0) <> COALESCE(f.checks_revenue, 0)
					OR COALESCE(t.ach_reject, 0) <> COALESCE(f.ach_reject, 0)
					OR COALESCE(t.refund, 0) <> COALESCE(f.refund, 0)
					OR COALESCE(t.refund_processed, 0) <> COALESCE(f.refund_processed, 0)
					OR COALESCE(t.refund_reapplied, 0) <> COALESCE(f.refund_reapplied, 0)
					OR COALESCE(t.refund_returned, 0) <> COALESCE(f.refund_returned, 0)
					OR COALESCE(t.continuous_pull, 0) <> COALESCE(f.continuous_pull, 0)
					OR COALESCE(t.cash_to_rtr, 0) <> COALESCE(f.cash_to_rtr, 0)
					OR COALESCE(t.rtr_adjustment, 0) <> COALESCE(f.rtr_adjustment, 0)
					OR COALESCE(t.rtr_applied, 0) <> COALESCE(f.rtr_applied, 0)
					OR COALESCE(t.pp_adjustment, 0) <> COALESCE(f.pp_adjustment, 0)
					OR COALESCE(t.pp_applied, 0) <> COALESCE(f.pp_applied, 0)
					OR COALESCE(t.margin_applied, 0) <> COALESCE(f.margin_applied, 0)
					OR COALESCE(t.gross_revenue, 0) <> COALESCE(f.gross_revenue, 0)
					OR COALESCE(t.applied_amount, 0) <> COALESCE(f.applied_amount, 0)
					OR COALESCE(t.gross_received, 0) <> COALESCE(f.gross_received, 0)
					OR COALESCE(t.net_other_income, 0) <> COALESCE(f.net_other_income, 0)
					OR COALESCE(t.ach_revenue, 0) <> COALESCE(f.ach_revenue, 0)
					OR COALESCE(t.total_fees_collected, 0) <> COALESCE(f.total_fees_collected, 0)
					OR COALESCE(t.gross_dollars_rec, 0) <> COALESCE(f.gross_dollars_rec, 0)
					OR COALESCE(t.last_trans_amount, 0) <> COALESCE(f.last_trans_amount, 0)
					OR COALESCE(t.total_portfolio_adjustment, 0) <> COALESCE(f.total_portfolio_adjustment, 0)
					OR COALESCE(t.estimated_final_payment_date, '1900-01-01') <> COALESCE(f.estimated_final_payment_date, '1900-01-01')
					OR COALESCE(t.anticipated_final_payment_date, '1900-01-01') <> COALESCE(f.anticipated_final_payment_date, '1900-01-01')
					OR COALESCE(t.last_draft_date, '1900-01-01') <> COALESCE(f.last_draft_date, '1900-01-01')	
					OR COALESCE(t.pp_settlement, 0) <> COALESCE(f.pp_settlement, 0)
					OR COALESCE(t.margin_settlement, 0) <> COALESCE(f.margin_settlement, 0)
					OR COALESCE(t.actual_turn, 0) <> COALESCE(f.actual_turn, 0)
					OR COALESCE(t.variance_amount, 0) <> COALESCE(f.variance_amount, 0)
				)
			)


	--		SELECT rtr_out, fees_out, payoff_amount, rtr_applied, cash_to_rtr, rtr_adjustment, start_date, cash_to_margin, cash_to_pp,  * FROM #tempFacts
--		SELECT rtr_out, fees_out, payoff_amount, rtr_applied, cash_to_rtr, rtr_adjustment, start_date, cash_to_margin, cash_to_pp,  * FROM #tempFacts WHERE funding_id = 183
		--SELECT rtr_out, fees_out, payoff_amount, rtr_applied, cash_to_rtr, rtr_adjustment, cash_to_margin, cash_to_pp,  * FROM #tempFundings WHERE funding_id = 183
		--SELECT rtr_out, fees_out, payoff_amount, rtr_applied, cash_to_rtr, rtr_adjustment, cash_to_margin, cash_to_pp,  * FROM #tempFundings_original WHERE id = 183
		--SELECT rtr_out, fees_out, payoff_amount, rtr_applied, cash_to_rtr, rtr_adjustment, start_date, cash_to_margin, cash_to_pp,  * FROM fact_fundings

		--	SELECT rtr_out, fees_out, payoff_amount, rtr_applied, cash_to_rtr, rtr_adjustment, * FROM #tempFundings WHERE funding_id = 183
	--	SELECT rtr_out, fees_out, payoff_amount, rtr_applied, cash_to_rtr, rtr_adjustment, * FROM #tempFundings_original WHERE funding_id = 183
	--	SELECT CONVERT(VARCHAR (50), @tempDate), rtr_out, fees_out, payoff_amount, rtr_applied, cash_to_rtr, rtr_adjustment, * FROM #tempFacts WHERE funding_id = 183
	--	SELECT * FROM fact_fundings

	


			-- now need to update any records that had @endDate for their end date and are no longer the end date (meaning their start date will be less)
			-- get the latest start date for each funding that has end_date = @endDate

		UPDATE f
		SET end_date = DATEADD(DAY, -1, @tempDate)
		FROM fact_fundings f
		INNER JOIN (
			SELECT f.funding_id, MAX(f.start_date) AS max_start_date
			FROM fact_fundings f
			GROUP BY f.funding_id
		) x ON f.funding_id = x.funding_id AND f.start_date <> x.max_start_date AND f.end_date = @endDate



		--- **************************************************
		--- **************************************************
		--- **************************************************
		-- Now do the entire porfolio summary


		INSERT INTO fact_fundings_portfolio(payoff_amount, paid_off_amount, rtr_out, pp_out, margin_out, fees_out, percent_paid, percent_performance, margin_adjustment, start_date, end_date, float_amount,
			ach_draft, refund_out, create_date, cash_to_pp, cash_to_margin, cash_fees, counter_deposit_revenue, ach_received, cc_revenue, wires_revenue, wires_revenue_no_renewal,
			discount_received, fees_applied, settlement_received, writeoff_received, cash_to_chargeback, cash_to_bad_debt, cash_to_reapplication, cash_from_reapplication, writeoff_adjustment_received, 
			checks_revenue, ach_reject, refund, refund_processed, refund_reapplied, refund_returned, continuous_pull, cash_to_rtr, rtr_adjustment, rtr_applied, pp_adjustment, pp_applied, 
			margin_applied, gross_revenue, applied_amount, gross_received, net_other_income, net_revenue, ach_revenue, total_fees_collected, gross_dollars_rec, total_portfolio_adjustment, end_of_month, 
			pp_settlement, margin_settlement, variance_amount)
		SELECT SUM(f.payoff_amount), SUM(f.paid_off_amount), SUM(f.rtr_out), SUM(f.pp_out), SUM(f.margin_out), SUM(f.fees_out), 
			SUM(COALESCE(f.cash_to_rtr, 0)) / SUM(ff.portfolio_value) * 100.00, 
			SUM((ff.estimated_turn - COALESCE(ff.actual_turn, 0))) / SUM(ff.estimated_turn) * 100.00, 
			(SELECT SUM(t.margin_adjustment) FROM #tempFacts t), @tempDate, @endDate,
			(SELECT SUM(t.float_amount) FROM #tempFacts t),
			(SELECT SUM(t.ach_draft) FROM #tempFacts t),
			(SELECT SUM(t.refund_out) FROM #tempFacts t), GETUTCDATE(), 
			(SELECT SUM(t.cash_to_pp) FROM #tempFacts t),
			(SELECT SUM(t.cash_to_margin) FROM #tempFacts t),
			(SELECT SUM(t.cash_fees) FROM #tempFacts t),
			(SELECT SUM(t.counter_deposit_revenue) FROM #tempFacts t),
			(SELECT SUM(t.ach_received) FROM #tempFacts t),
			(SELECT SUM(t.cc_revenue) FROM #tempFacts t),
			(SELECT SUM(t.wires_revenue) FROM #tempFacts t),
			(SELECT SUM(t.wires_revenue_no_renewal) FROM #tempFacts t),
			(SELECT SUM(t.discount_received) FROM #tempFacts t),
			(SELECT SUM(t.fees_applied) FROM #tempFacts t),
			(SELECT SUM(t.settlement_received) FROM #tempFacts t), 
			(SELECT SUM(t.writeoff_received) FROM #tempFacts t),
			(SELECT SUM(t.cash_to_chargeback) FROM #tempFacts t),
			(SELECT SUM(t.cash_to_bad_debt) FROM #tempFacts t),
			(SELECT SUM(t.cash_to_reapplication) FROM #tempFacts t),
			(SELECT SUM(t.cash_from_reapplication) FROM #tempFacts t),
			(SELECT SUM(t.writeoff_adjustment_received) FROM #tempFacts t),
			(SELECT SUM(t.checks_revenue) FROM #tempFacts t),
			(SELECT SUM(t.ach_reject) FROM #tempFacts t),
			(SELECT SUM(t.refund) FROM #tempFacts t),
			(SELECT SUM(t.refund_processed) FROM #tempFacts t),
			(SELECT SUM(t.refund_reapplied) FROM #tempFacts t),
			(SELECT SUM(t.refund_returned) FROM #tempFacts t),
			(SELECT SUM(t.continuous_pull) FROM #tempFacts t),
			(SELECT SUM(t.cash_to_rtr) FROM #tempFacts t),
			(SELECT SUM(t.rtr_adjustment) FROM #tempFacts t),
			(SELECT SUM(t.rtr_applied) FROM #tempFacts t),
			(SELECT SUM(t.pp_adjustment) FROM #tempFacts t),
			(SELECT SUM(t.pp_applied) FROM #tempFacts t),
			(SELECT SUM(t.margin_applied) FROM #tempFacts t),
			(SELECT SUM(t.gross_revenue) FROM #tempFacts t),
			(SELECT SUM(t.applied_amount) FROM #tempFacts t),
			(SELECT SUM(t.gross_received) FROM #tempFacts t),
			(SELECT SUM(t.net_other_income) FROM #tempFacts t),
			(SELECT SUM(t.net_revenue) FROM #tempFacts t),
			(SELECT SUM(t.ach_revenue) FROM #tempFacts t),
			(SELECT SUM(t.total_fees_collected) FROM #tempFacts t),
			(SELECT SUM(t.gross_dollars_rec) FROM #tempFacts t),
			(SELECT SUM(t.total_portfolio_adjustment) FROM #tempFacts t),
			IIF(@tempDate = EOMONTH(@tempDate), 1, 0),
			(SELECT SUM(t.pp_settlement) FROM #tempFacts t),
			(SELECT SUM(t.margin_settlement) FROM #tempFacts t), 
			(SELECT SUM(t.variance_amount) FROM #tempFacts t)
		FROM #tempFacts f WITH (NOLOCK)
		INNER JOIN #tempFundings_original ff WITH (NOLOCK) ON f.funding_id = ff.id
		

		SET @tempDate = DATEADD(DD, 1, @tempDate)
	END
-----    END LOOP
/*
UPDATE f
SET float_amount = ff.float_amount
FROM #tempFacts f
INNER JOIN #tempFloat ff ON f.funding_id = ff.funding_id AND f.start_date = ff.start_date
*/

UPDATE ff
SET float_amount = t.float_amount
FROM fact_fundings ff
INNER JOIN tempFloatNewFormulas t ON ff.funding_id = t.funding_id AND ff.start_date = t.start_date 

UPDATE p
SET float_amount = x.float_amount
FROM fact_fundings_portfolio p 
INNER JOIN (
	SELECT start_date, SUM(float_amount) AS float_amount
	FROM tempFloatNewFormulas t
	GROUP BY t.start_date
) x ON p.start_date = x.start_date


COMMIT TRANSACTION
GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)

Done:

IF OBJECT_ID('tempdb..#tempTrans') IS NOT NULL DROP TABLE #tempTrans

-- need to store in a temp table the values up to today.
-- then compare to the last one for each funding.  if any field is different, insert a new record.



IF OBJECT_ID('tempdb..#tempFacts') IS NOT NULL DROP TABLE #tempFacts

END
GO
