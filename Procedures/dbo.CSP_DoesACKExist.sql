SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-02-26
-- Description:	Checks to see if an ACK has been received yet.  Used by the advice file.  ACK HAS to be received before Advice can be processed.
-- =============================================
CREATE PROCEDURE CSP_DoesACKExist
(
	@batch_date			DATE,
	@vendor				VARCHAR (50)
)
AS
BEGIN

SET NOCOUNT ON;

IF EXISTS(SELECT 1 FROM batches	WHERE batch_type = CASE @vendor WHEN '53' THEN 'FifthThirdDraft' ELSE '' END AND batch_date = @batch_date AND batch_status = 'Closed')
	BEGIN
		SELECT 1 AS ACKExists
	END
ELSE 
	BEGIN
		SELECT 0 AS ACKExists
	END

END
GO
