SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Rick Pina
-- Create date: 2021-03-24
-- Description:	Updates Contacts Address
-- =============================================
CREATE PROCEDURE [dbo].BSK_UpdContactAddress
(
	@address_1				NVARCHAR (100),
	@address_2				NVARCHAR (100),
	@city					NVARCHAR (50),
	@state					NVARCHAR (50),
	@postal_code			NVARCHAR (50),
	@contact_id					INT,
	@address_type_id			INT,
	@change_user				NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON;

UPDATE addresses
SET address_1 = @address_1, address_2 = @address_2, address_type_id = @address_type_id,
	city = @city, state = @state, state_abbrev = (SELECT state_code FROM states WHERE state_name = @state),
	postal_code = @postal_code, change_date = GETUTCDATE(), change_user = @change_user
WHERE contact_id = @contact_id

END

GO
