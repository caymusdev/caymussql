SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-08-26
-- Description:	Partially Writes off a funding given a funding id.  
-- =============================================
CREATE PROCEDURE [dbo].[CSP_PartialWriteoffFunding]
(
	@funding_id			BIGINT,
	--@username			NVARCHAR (100),
	@trans_date			DATETIME,
	@writeoff_amount	MONEY,
	@margin_adjust_amount	MONEY
)	
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
DECLARE @trans_id INT, @comments NVARCHAR (500), @portfolio VARCHAR (50)

BEGIN TRANSACTION

SET @comments = 'Partial Writeoff'

-- create a new batch
DECLARE @new_batch_id INT
EXEC @new_batch_id = dbo.CSP_GetNewBatchNumber 'Partial Writeoff', 'SYSTEM' , 'Partial Writeoff', '', @trans_date

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR


-- write off the principal amount
INSERT INTO transactions (trans_date, funding_id, trans_amount, trans_type_id, comments, batch_id, record_id, previous_id, redistributed, transaction_id, record_type, settle_date, 
	redirect_approval_id, change_user, change_date, settle_code, portfolio)
SELECT @trans_date, @funding_id, @writeoff_amount, 11, @comments, @new_batch_id, NULL, NULL, 0, NULL, 'transactions', @trans_date, NULL, 'SYSTEM', GETUTCDATE(), NULL, 'caymus'

SELECT @trans_id = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR

INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
VALUES (@trans_id, 11, @writeoff_amount, @comments, 'SYSTEM', GETUTCDATE())

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR

-- now discount the margin
INSERT INTO transactions (trans_date, funding_id, trans_amount, trans_type_id, comments, batch_id, record_id, previous_id, redistributed, transaction_id, record_type, settle_date, 
	redirect_approval_id, change_user, change_date, settle_code, portfolio)
SELECT @trans_date, @funding_id, @margin_adjust_amount, 17, @comments, @new_batch_id, NULL, NULL, 0, NULL, 'transactions', @trans_date, NULL, 'SYSTEM', GETUTCDATE(), NULL,
	'caymus'

SELECT @trans_id = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR

INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
VALUES (@trans_id, 17, @margin_adjust_amount, @comments, 'SYSTEM', GETUTCDATE())

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR


UPDATE batches SET batch_status = 'Closed', change_date = GETUTCDATE(), change_user = 'SYSTEM' WHERE batch_id = @new_batch_id

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR


COMMIT TRANSACTION
GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)	

Done:

END

GO
