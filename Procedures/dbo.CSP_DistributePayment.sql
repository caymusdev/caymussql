SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-11-29
-- Description:	Contains the logic for distributing a payment.  First to ACH Draft and then to details of PP and Margin
-- =============================================
CREATE PROCEDURE [dbo].[CSP_DistributePayment]
(
	@payment_id			INT,
	@transaction_id		NVARCHAR (100),
	@batch_id			INT,
	@processor			VARCHAR (50)= NULL,
	@portfolio			VARCHAR (50) = 'caymus'
)
AS
BEGIN
SET NOCOUNT ON;
-------------- THIS CREATES ACH DRAFT RECORDS

DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''

DECLARE @trans_id INT, @trans_date DATE, @funding_id BIGINT, @trans_amount MONEY, @record_id INT, @new_trans_amount MONEY, @is_fee BIT, @redirect_approval_id INT, 
	@redirect_funding_id BIGINT, @redirect_trans_id INT, @is_refund BIT
DECLARE @mainTransType INT; SET @mainTransType = 18; SET @redirect_approval_id = NULL

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SELECT @tomorrow = DATEADD(DD, CASE DATEPART(WEEKDAY, @today) WHEN 6 THEN 3 WHEN 7 THEN 2 ELSE 1 END, @today)


SELECT @trans_date = @tomorrow, @funding_id = p.funding_id,  --COALESCE(f.id, f2.id), 
	@trans_amount = p.amount, @record_id = p.payment_id, @is_fee = COALESCE(p.is_fee, 0), @redirect_approval_id = p.redirect_approval_id, 
	@redirect_funding_id = p.redirect_funding_id, @is_refund = COALESCE(p.is_refund, 0)
FROM payments p
--LEFT JOIN payment_schedules ps ON p.payment_schedule_id = ps.payment_schedule_id
--LEFT JOIN funding_payment_schedules fps ON ps.payment_schedule_id = fps.payment_schedule_id
--LEFT JOIN fundings f ON fps.funding_id = f.id
--LEFT JOIN fundings f2 ON p.funding_id = f2.id
WHERE p.payment_id = @payment_id

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
IF @errorNum != 0 GOTO ERROR

IF @is_refund = 0  -- only create ACH DRAFT for regular payments, not refunds
	BEGIN
		BEGIN TRANSACTION
		-- if the @trans_date is further in the past, need to update it to be tomorrow's.  For example, a fee may sit in the queue for a while and finally get sent
		-- 3 weeks later so it's trans_date is quite old but should be set to tomorrow.  Don't want to do it for every payment because there may be some that should 
		-- have been sent yesterday, for example, but had a mess-up on our side or on Forte's so we still want that original date.

		IF DATEDIFF(DD, @trans_date, @tomorrow) > 10 SET @trans_date = @tomorrow -- should be long enough to also handle weekly payments that might have skipped for some reason

		INSERT INTO transactions (trans_date, funding_id, trans_amount, trans_type_id, comments, batch_id, record_id, previous_id, redistributed, record_type, transaction_id, payment_id,
			redirect_approval_id, change_user, change_date, portfolio)
		SELECT @trans_date, @funding_id, @trans_amount, @mainTransType, NULL, @batch_id, @record_id, NULL, 0, 'payments', @transaction_id, @payment_id, @redirect_approval_id, 
			'SYSTEM', GETUTCDATE(), @portfolio

		SELECT @trans_id = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()
		IF @errorNum != 0 GOTO ERROR

		DECLARE @contract_status INT, @pricing_ratio NUMERIC(10, 4), @rtr MONEY
		SELECT @contract_status = contract_status, @pricing_ratio = pricing_ratio, @rtr = portfolio_value
		FROM fundings 
		WHERE id = @funding_id

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR

		IF @is_fee = 1
			BEGIN
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				VALUES (@trans_id, 1, @trans_amount, '', 'SYSTEM', GETUTCDATE())

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR
			END	 -- @is_fee = 1
		ELSE IF @redirect_approval_id IS NOT NULL
			BEGIN
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				VALUES (@trans_id, 26, @trans_amount, '', 'SYSTEM', GETUTCDATE())

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR

				EXEC dbo.CSP_RedirectTransaction @redirect_approval_id, @trans_amount, @batch_id, @record_id, 'payments', @transaction_id, @payment_id,	@trans_date, @trans_id

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	;IF @errorNum != 0 GOTO ERROR
			END
		ELSE
			BEGIN
				EXEC dbo.CSP_ApplyPPAndMargin @funding_id, @trans_amount, @trans_id, ''
				----IF @contract_status = 1
				----	BEGIN
				----		-- apply to purchase price and margin
				----		-- do purchase price first
				----		SET @new_trans_amount = ROUND(@trans_amount / @pricing_ratio, 2)	
				
				----		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				----		IF @errorNum != 0 GOTO ERROR	

				----		-- we have had issues where, due to rounding, too much purchase price is taken out.  For example, a payment of 120 and pricing ratio of 1.35 after 20 payments
				----		-- we will have overdrawn the purchase price by 2.2 cents.  So, we need to adjust at the end, essentially, if the purchse price outstanding is more less than
				----		-- what we have calculated it should be, then lower the pp payment this time.  Likely should only ever happen on the last payment.
				----		-- we do store pp_out but it may not be accurate at this moment because, for example, we could be getting 2 payments to the same funding
				----		-- so, add up all trans_type of 7 and then compare to a calcualted value
				----		DECLARE @expected_pp MONEY, @current_pp MONEY
				----		SET @expected_pp = ROUND(@rtr / @pricing_ratio, 2)
				----		SELECT @current_pp = SUM(d.trans_amount) 
				----		FROM transactions t
				----		INNER JOIN transaction_details d ON t.trans_id = d.trans_id
				----		WHERE t.funding_id = @funding_id AND d.trans_type_id = 7

				----		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				----		IF @errorNum != 0 GOTO ERROR	

				----		IF @expected_pp - @current_pp < @new_trans_amount SET @new_trans_amount = @expected_pp - @current_pp

				----		INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments)
				----		VALUES (@trans_id, 7, @new_trans_amount, '')

				----		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				----		IF @errorNum != 0 GOTO ERROR	

				----		-- now do margin
				----		INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments)
				----		VALUES (@trans_id, 6, @trans_amount - @new_trans_amount, '')

				----		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				----		IF @errorNum != 0 GOTO ERROR	
				----	END
				----ELSE IF @contract_status = 4
				----	BEGIN
				----		INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments)
				----		VALUES (@trans_id, 15, @trans_amount, '')

				----		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				----		IF @errorNum != 0 GOTO ERROR
				----	END
				------ if it is a different contract status, nothing will happen and then an existing alert will pick up on it because we'll have a transaction
				------ without matching details, so don't worry about the other contract statuses
			END -- @is_fee = 0

		-- update the payment
		UPDATE payments SET transaction_id = @transaction_id, processed_date = GETUTCDATE(), batch_id = @batch_id, change_date = GETUTCDATE(), change_user = 'API', processor = @processor
		WHERE payment_id = @payment_id

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR

		COMMIT TRANSACTION

		-- i don't want the transaction to rollback if for some reason applying to oldest contract does not work so I am putting this outside the commit transaction
		IF @is_fee = 0 AND @redirect_approval_id IS NULL AND @portfolio != 'spv'
			BEGIN
				EXEC dbo.CSP_RedistributeTransactionToOldestContract @funding_id, @trans_id
			END


		-- need to record SPV transactions.  Also outside the transaction intentionally.  these records can be fixed manually if necessary but distributing
		-- the payment is critical.
		IF @portfolio = 'spv' AND @mainTransType = 18
			BEGIN
				DECLARE @new_spv_trans_id INT
				DECLARE @warehouse_margin MONEY, @pp MONEY

				SELECT @warehouse_margin = trans_amount FROM transaction_details WHERE trans_id = @trans_id AND trans_type_id = 6
				SELECT @pp = trans_amount FROM transaction_details WHERE trans_id = @trans_id AND trans_type_id = 7
				IF COALESCE(@warehouse_margin, 0) = 0 OR COALESCE(@pp, 0) = 0
					BEGIN
						DECLARE @string VARCHAR (500)
						SET @string = 'Margin or PP is 0 on main trans ' + CONVERT(VARCHAR (50), @trans_id) + '.  We were expecting margin and purchase price ' +
							' because this is SPV.  Has this funding gone to bad debt? '
						EXEC dbo.CSP_InsAlert 'Funding', 'WarehouseNotActive', @funding_id, @string
					END
				ELSE
					BEGIN
						INSERT INTO transactions (trans_date, funding_id, trans_amount, trans_type_id, comments, batch_id, record_id, previous_id, redistributed, record_type, 
							transaction_id, payment_id,	redirect_approval_id, change_user, change_date, spv_main_id, portfolio)
						SELECT @trans_date, @funding_id, @trans_amount, 27, 'ACH DRAFT TRANS ID: ' + CONVERT(VARCHAR(50), @trans_id), @batch_id, @record_id, NULL, 0, 'warehouse', 
							@transaction_id, @payment_id, @redirect_approval_id, 'SYSTEM', GETUTCDATE(), @trans_id, @portfolio

						SELECT @new_spv_trans_id = SCOPE_IDENTITY()

						DECLARE @caymus_pp MONEY, @spv_pp MONEY; SET @caymus_pp = ROUND(@pp * .15, 2); SET @spv_pp = @pp - @caymus_pp
							
						INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
						VALUES (@new_spv_trans_id, 28, @warehouse_margin, '', 'SYSTEM', GETUTCDATE())						

						INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
						VALUES (@new_spv_trans_id, 29, @caymus_pp, '', 'SYSTEM', GETUTCDATE())

						INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
						VALUES (@new_spv_trans_id, 30, @spv_pp, '', 'SYSTEM', GETUTCDATE())
				END
			END


		GOTO Done

		ERROR:	
			ROLLBACK TRANSACTION
			RAISERROR (@ErrorMessage, 16, 1)	
	END
Done:

END

GO
