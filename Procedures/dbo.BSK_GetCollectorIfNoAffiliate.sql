SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Rick Pina
-- Create date: 2021-03-11
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.BSK_GetCollectorIfNoAffiliate
(
	@previous_email				NVARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON;

SELECT TOP 1 u.email, up.phone_number 
FROM users u
INNER JOIN user_statuses us ON u.user_id = us.user_id AND us.end_date IS NULL
INNER JOIN user_presence_statuses ups ON u.presence_status = ups.user_presence_status_id
INNER JOIN user_phones up ON u.user_id = up.user_id
INNER JOIN user_departments ud ON u.user_id = ud.user_id 
WHERE ups.status_name IN ('Available') AND up.is_primary = 1 AND u.email != @previous_email AND u.active = 1
ORDER BY ud.department_id ASC, us.start_date ASC


END

GO
