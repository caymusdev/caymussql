SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-12-01
-- Description:	Returns various items (scheduled payments, reminders, etc) for a given collector to be displayed on a calendar
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetCollectorCalendar]
(
	@start_date			DATE,
	@end_date			DATE,
	@user_email			NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON;
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())

DECLARE @user_id INT
SELECT @user_id = user_id FROM users WHERE email = @user_email


SELECT x.record_id, x.affiliate_id, x.contact_type, x.contact_id, x.notes, x.record_date, a.affiliate_name, x.contact_type_desc
FROM (
	SELECT r.reminder_id AS record_id, r.affiliate_id,ct.contact_type, ct.contact_type_desc AS contact_type_desc, r.contact_id, r.notes, r.reminder_date AS record_date
	FROM reminders r
	LEFT JOIN contact_types ct ON r.contact_type_id = ct.contact_type_id
	WHERE r.user_id = @user_id AND COALESCE(completed, 0) = 0 AND r.reminder_date BETWEEN @start_date AND @end_date
	UNION ALL
	SELECT sp.scheduled_payment_id AS record_id, sp.affiliate_id, 'ScheduledPayment' AS contact_type, 'Scheduled Payment' AS contact_type_desc, 
		NULL AS contact_id, CONVERT(VARCHAR (50), sp.payment_amount) AS notes, sp.payment_date AS record_date
	FROM scheduled_payments sp
	INNER JOIN affiliates a ON sp.affiliate_id = a.affiliate_id
	INNER JOIN cases c ON a.affiliate_id = c.affiliate_id
	INNER JOIN collector_cases cc ON c.case_id = cc.case_id AND cc.start_date <= @today AND COALESCE(cc.end_date, '2050-05-09') >= @today
	WHERE cc.collector_user_id = @user_id AND sp.payment_date BETWEEN @start_date AND @end_date
) x 
LEFT JOIN affiliates a ON x.affiliate_id = a.affiliate_id
ORDER BY x.record_date, a.affiliate_name

END

GO
