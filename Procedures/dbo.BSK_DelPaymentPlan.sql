SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2020-12-22
-- Description: Marks a payment plan as deleted
-- =============================================
CREATE PROCEDURE dbo.[BSK_DelPaymentPlan]
(
	@payment_plan_id	INT,
	@users_email		NVARCHAR (100),
	@change_reason_id	INT,
	@comments			NVARCHAR (2000)
)
AS
BEGIN
SET NOCOUNT ON

DECLARE @payment_amount MONEY, @payment_plan_type VARCHAR (50), @start_date DATE, @description NVARCHAR (2000), @frequency VARCHAR (50), @affiliate_id BIGINT


UPDATE payment_plans
SET deleted = 1, change_date = GETUTCDATE(), change_user = @users_email
WHERE payment_plan_id = @payment_plan_id

UPDATE scheduled_payments 
SET deleted = 1, change_date = GETUTCDATE(), change_user = @users_email, comments = @comments, change_reason_id = @change_reason_id 
WHERE payment_plan_id = @payment_plan_id

-- First need to check and see if this user needs approval for editing/creating payment schedules
DECLARE @approved BIT; SET @approved = 1; SET @description = ''

IF EXISTS (SELECT 1 FROM sec_payment_schedules s WHERE s.submitter_user_id = @users_email)
	BEGIN
		SET @approved = 0
	END

IF @approved = 0
	BEGIN
		SELECT @payment_amount = p.payment_amount, @payment_plan_type = p.payment_plan_type, @start_date = p.start_date, @frequency = p.frequency, @affiliate_id = p.affiliate_id
		FROM payment_plans p
		WHERE p.payment_plan_id = @payment_plan_id

		SET @description = 'Cancelled ' + @payment_plan_type + ' for ' + CONVERT(VARCHAR, CAST(@payment_amount AS MONEY), 1) + ' ' + LOWER(@frequency) + 
			' starting on ' + CONVERT(VARCHAR (10), @start_date, 126) + '.'

		IF @change_reason_id IS NOT NULL
			BEGIN
				SELECT @description = @description + ' The reason for the change is ' + reason + '.'
				FROM payment_schedule_change_reasons
				WHERE id = @change_reason_id
			END

		IF COALESCE(@comments, '') != ''
			BEGIN
				SET @description = @description + @comments + '.'
			END

		INSERT INTO approvals (approval_type_id, description, comments, record_id, new_value, create_date, change_date, change_user, create_user, approved_by,
			approved_date, rejected_by, rejected_date, affiliate_id)
		SELECT t.approval_type_id, LTRIM(@description), NULL, @payment_plan_id, NULL, GETUTCDATE(), GETUTCDATE(), @users_email, @users_email, NULL, NULL, NULL, NULL, 
			@affiliate_id
		FROM approval_types t 
		WHERE t.approval_type = 'PaymentPlanDelete'
	END
ELSE  -- @approved = 1
	BEGIN
		EXEC dbo.BSK_ApproveSchedPaymentPlan @payment_plan_id, @users_email
	END

END

GO
