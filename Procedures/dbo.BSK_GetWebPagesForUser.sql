SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-06
-- Description:	Gets a list of webpages the user has access to (used in building the menu items)
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetWebPagesForUser]
	@email			NVARCHAR (100)
AS
BEGIN
SET NOCOUNT ON;

-- first check if they are admin, if so they get access to everything
IF EXISTS (SELECT 1
			FROM users u WITH (NOLOCK)
			INNER JOIN user_roles ur WITH (NOLOCK) ON u.user_id = ur.user_id						
			INNER JOIN roles r WITH (NOLOCK) ON ur.role_id = r.role_id
			WHERE u.email = @email AND r.role_name = 'Administrator' AND u.active = 1
			)
	BEGIN
		-- has admin
		SELECT p.permission_name, 1 AS has_permission
		FROM permissions p WITH (NOLOCK)
		WHERE p.is_menu_item = 1
		ORDER BY p.permission_name
	END
ELSE
	BEGIN
		SELECT p.permission_name, CASE WHEN x.permission_id IS NULL THEN 0 ELSE 1 END AS has_permission
		FROM permissions p WITH (NOLOCK)
		LEFT JOIN (
			SELECT DISTINCT p.permission_id 
			FROM users u WITH (NOLOCK)
			INNER JOIN user_roles ur WITH (NOLOCK) ON u.user_id = ur.user_id
			INNER JOIN role_permissions  rp WITH (NOLOCK) ON ur.role_id = rp.role_id
			INNER JOIN permissions p WITH (NOLOCK) ON rp.permission_id = p.permission_id
			WHERE u.email = @email AND p.is_menu_item = 1 AND u.active = 1
		) x ON p.permission_id = x.permission_id
		WHERE p.is_menu_item = 1
		ORDER BY p.permission_name

	END
END

GO
