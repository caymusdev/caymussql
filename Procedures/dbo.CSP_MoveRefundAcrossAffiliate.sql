SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-04-03
-- Description:	Move a refund to a different contract of a different merchant but same affiliate
-- =============================================
CREATE PROCEDURE [dbo].[CSP_MoveRefundAcrossAffiliate]
(
	@orig_funding_id			BIGINT,
	@new_funding_id				BIGINT,
	@userid						NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON;


/* vars */
DECLARE @orig_contract_number VARCHAR (50), @new_contract_number VARCHAR (50)
DECLARE @tempAmount MONEY, @tempComments VARCHAR (500), @refund MONEY
DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
DECLARE @today DATE, @newBatchID INT; SET @today = DATEADD(HOUR, -5, GETUTCDATE())
/* end vars */

SELECT @refund = refund_out, @orig_contract_number = contract_number FROM fundings WHERE id = @orig_funding_id
SELECT @new_contract_number = contract_number FROM fundings WHERE id = @new_funding_id

BEGIN TRANSACTION

-- get a new batch id
EXEC @newBatchID = dbo.CSP_GetNewBatchNumber '', @userid, 'Refund Moved Across Affiliate', '', @today, 0

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
					
SET @tempComments = 'Refund-Reapply to ' + @new_contract_number
SET @tempAmount = ABS(@refund)
-- move money from first funding
EXEC dbo.CSP_DistributeTransaction_v2 @orig_funding_id, @tempAmount, NULL, @today, @tempComments, @newBatchID, NULL, NULL, NULL, @today, NULL, NULL, 23

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

-- move money to second funding
SET @tempComments = 'Refund-Reapply from ' + @orig_contract_number

EXEC dbo.CSP_DistributeTransaction_v2 @new_funding_id, @tempAmount, NULL, @today, @tempComments, @newBatchID, NULL, NULL, NULL, @today, NULL, NULL, 24

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
						
-- set the batch to Closed
UPDATE batches SET batch_status = 'Closed', settle_date = @today WHERE batch_id = @newBatchID
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

COMMIT TRANSACTION
GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)

Done:

EXEC dbo.CSP_UpdCalculations 'SYSTEM'



END
GO
