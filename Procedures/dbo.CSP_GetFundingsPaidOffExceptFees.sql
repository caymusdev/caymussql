SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-08-06
-- Description:	Checks for fundings that still owe fees but are otherwise paid off and might have payments in the queue
-- =============================================
CREATE PROCEDURE dbo.CSP_GetFundingsPaidOffExceptFees
AS
BEGIN
SET NOCOUNT ON;


SELECT f.id AS funding_id, f.legal_name, f.rtr_out, f.payoff_amount, f.fees_out, f.process_fees, f.last_payment_date, f.on_hold, 
	CASE WHEN EXISTS (SELECT 1 FROM payments p WHERE p.processed_date IS NULL AND p.funding_id = f.id AND p.is_fee = 1 AND p.deleted = 0 AND p.waived = 0)
		THEN 'yes' ELSE 'no' END AS payments_in_queue
FROM fundings f WITH (NOLOCK)
WHERE f.rtr_out = 0 AND f.payoff_amount > 0 AND f.contract_status = 1 AND f.payoff_amount = COALESCE(f.fees_out, 0)
	AND (NOT EXISTS (SELECT 1 FROM payments p WHERE p.processed_date IS NULL AND p.funding_id = f.id AND p.is_fee = 1 AND p.deleted = 0 AND p.waived = 0)
		OR COALESCE(f.on_hold, 0) = 1
	)



END
GO
