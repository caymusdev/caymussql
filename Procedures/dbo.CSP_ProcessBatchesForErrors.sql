SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-05-31
-- Description:	Checks batches that are not New or Completed for errors.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_ProcessBatchesForErrors]
AS
BEGIN
SET NOCOUNT ON;

-- First need to clear batches that are in error, set them to Processed
UPDATE batches
SET batch_status = 'Processed' , status_message = '', change_user = 'SYSTEM', change_date = GETUTCDATE()
WHERE batch_status = 'Error'

-- SELECT @@ROWCOUNT

-- check for null fundings
UPDATE batches
SET batch_status = 'Error', status_message = 'At least one transaction does not have a funding.', change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM batches b 
INNER JOIN transactions t ON b.batch_id = t.batch_id
WHERE t.funding_id IS NULL 
	AND (b.batch_status IN ('Processed')
		OR (b.batch_status = 'Settled' AND b.batch_type = 'BOA Upload'))
	AND t.redistributed = 0

--  SELECT @@ROWCOUNT


-- check for transactions that do not total up in the details
UPDATE batches
SET batch_status = 'Error', status_message = 'Transaction Details do not match the Transactions.', change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM batches b
WHERE (SELECT SUM(trans_amount) FROM transactions t WHERE t.batch_id = b.batch_id AND t.redistributed = 0) 
	<> 
	(	SELECT SUM(d.trans_amount) 
		FROM transaction_details d 
		INNER JOIN transactions t2 ON d.trans_id = t2.trans_id
		WHERE t2.batch_id = b.batch_id AND t2.redistributed = 0
			-- RKB - 2018-11-16 removing trans_type_id 9.  Not sure why it was this way but what was happening is when adding fees for negative amounts(chargebacks essentially)
			--  we were only inserting the 72 fee into the details, not in the main transactions.  Which is why were then removing it here.  But it should not work that way.
			-- I have updated CSP_DistributeTransaction to create both the main and detail transactions so we no longer need this going forward.
			--AND d.trans_type_id <> 9) -- this removes the fees that we add in as part of the processing.  they will cause the batch to not balance.
		)
	AND b.batch_status IN ('Processed')

--  SELECT @@ROWCOUNT


-- look for forte fundings where total amount of settlements does not match funding
UPDATE batches
SET batch_status = 'Error', status_message = 'The Forte Funding amount does not equal the sum of the Forte Settlements', change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM batches b 
INNER JOIN forte_fundings f ON b.batch_id = f.batch_id
WHERE (SELECT SUM(s.settle_amount) FROM forte_settlements s WHERE s.funding_id = f.funding_id) <> f.net_amount
	AND b.batch_status IN ('Processed')

--SELECT @@ROWCOUNT



--  ************************************************************************
--  Also look for empty batches
--  ************************************************************************

UPDATE b
SET batch_status = 'Empty', change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM batches b
LEFT JOIN evo_staging s ON b.batch_id = s.BatchID
WHERE b.batch_type LIKE '%EVO%' AND s.evo_staging_id IS NULL AND b.batch_status = 'Processed' -- only set to empty if we've processed it and there is nothing there.

UPDATE b
SET batch_status = 'Empty', change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM batches b
LEFT JOIN ips_staging s ON b.batch_id = s.BatchID
WHERE b.batch_type LIKE '%IPS%' AND s.ips_staging_id IS NULL AND b.batch_status = 'Processed' -- only set to empty if we've processed it and there is nothing there.


-- check for fundings that are negative and not marked complete
DELETE FROM alerts WHERE alert_type = 'Funding' AND alert_type_2 = 'Negative' AND COALESCE(deleted, 0) = 0
INSERT INTO alerts (alert_type, alert_type_2, record_id, alert_message, deleted)
SELECT 'Funding', 'Negative', f.id, 'Payoff Amount is Negative', 0
FROM fundings f
WHERE payoff_amount < 0

-- look for Forte Settlements that do not have a Forte funding Id
DELETE FROM alerts WHERE alert_type = 'Forte' AND alert_type_2 = 'Settlement' AND COALESCE(deleted, 0) = 0
INSERT INTO alerts (alert_type, alert_type_2, record_id, alert_message, deleted)
SELECT 'Forte', 'Settlement', s.forte_settle_id, 'Forte Settlement without Funding', 0
FROM forte_settlements s 
WHERE COALESCE(s.funding_id, '') = '' AND s.settle_response_code = 'S01' AND s.settle_date >= '2018-11-28'

END
GO
