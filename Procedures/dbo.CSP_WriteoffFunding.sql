SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-07-13
-- Description:	Writes off a funding given a funding id.  
-- =============================================
CREATE PROCEDURE [dbo].[CSP_WriteoffFunding]
(
	@id				BIGINT,
	@username		NVARCHAR (100),
	@batch_date		DATETIME,
	@comments		NVARCHAR (500)
)	
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''

BEGIN TRANSACTION
-- first, make sure the calculated fields are up to date 
EXEC dbo.CSP_UpdCalculationsForFunding @id, @username

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR

-- create a new batch
DECLARE @new_batch_id INT
EXEC @new_batch_id = dbo.CSP_GetNewBatchNumber 'Writeoff', @username , 'Writeoff', '', @batch_date

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR

DECLARE @pp_out MONEY, @margin_out MONEY
SELECT @pp_out = pp_out, @margin_out = margin_out FROM fundings WHERE id = @id

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR

-- write off the principal
-- EXEC dbo.CSP_DistributeTransaction @batch_date, @id, @pp_out, @comments, @new_batch_id, NULL, NULL, NULL, NULL, 11
EXEC dbo.CSP_DistributeTransaction_v2 @id, @pp_out, NULL, @batch_date, @comments, @new_batch_id, NULL, 'transactions', 
			NULL, @batch_date, NULL, NULL, 11

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR

-- now discount the margin
-- EXEC dbo.CSP_DistributeTransaction @batch_date, @id, @margin_out, @comments, @new_batch_id, NULL, NULL, NULL, NULL, 17
EXEC dbo.CSP_DistributeTransaction_v2 @id, @margin_out, NULL, @batch_date, @comments, @new_batch_id, NULL, 'transactions', 
			NULL, @batch_date, NULL, NULL, 17

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR

UPDATE batches SET batch_status = 'Closed', change_date = GETUTCDATE(), change_user = @username WHERE batch_id = @new_batch_id

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR

UPDATE fundings SET writeoff_date = @batch_date, contract_substatus_id = 1, change_date = GETUTCDATE(), change_user = @username  WHERE id = @id
IF @errorNum != 0 GOTO ERROR

-- update the numbers again
EXEC dbo.CSP_UpdCalculationsForFunding @id, @username

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR


COMMIT TRANSACTION
GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)	

Done:

END
GO
