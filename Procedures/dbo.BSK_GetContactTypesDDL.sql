SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-19
-- Description:	Gets a list of contact types
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetContactTypesDDL]

AS
BEGIN
SET NOCOUNT ON;

SELECT contact_type_id, contact_type_desc
FROM contact_types
ORDER BY contact_type_desc


END

GO
