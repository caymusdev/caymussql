SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-07-19
-- Description:	Fills in the payment_forecasts table 
-- =============================================
CREATE PROCEDURE [dbo].[CSP_InsPaymentForecasts]
AS
BEGIN	
SET NOCOUNT ON;
-- loop through each funding that is in active status
-- if weekly payment, get last payment date to find the day of the week.
-- insert into payment_forecasts the funding id, trans date, payment amount
-- then go and update received amount

-- NOTE: This is based off of the weekly/weekday payments in fundings so it has nothing to do with when money hits the bank.
-- So, we can use transactions and no need to wait for batches to settle.

-- First, make sure payoff amounts are up to date
EXEC dbo.CSP_UpdCalculations

-- clear out any previous records
-- TRUNCATE TABLE payment_forecasts  -- 2018-07-20 - Now going to store the received amount so we'll keep history

-- Use the same Date over and over
DECLARE @tomorrow DATE, @today DATE
SELECT @today = CONVERT(DATE, DATEADD(HH, -5, GETUTCDATE()))  
SELECT @tomorrow = DATEADD(DD, CASE DATEPART(WEEKDAY, @today) WHEN 6 THEN 3 WHEN 7 THEN 2 ELSE 1 END, @today)

--DECLARE @funding_id BIGINT, @final_payment_date DATE, @frequency VARCHAR (50), @amount MONEY, @first_payment_date DATE, @payment_schedule_id INT
DECLARE @funding_id BIGINT, @frequency VARCHAR (50), @payment MONEY, @first_payment_date DATE, @payment_schedule_id INT

--DECLARE fundings_cursor CURSOR FOR
/*
SELECT id, CONVERT(DATE, DATEADD(D, calc_turn * 21.0, GETDATE())) AS final_payment_date, frequency, 
	CASE frequency WHEN 'Weekly' THEN weekly_payment ELSE weekday_payment END AS payment, COALESCE(x.last_payment_date, f.funded_date) AS last_payment_date
FROM fundings f
--LEFT JOIN (
--	SELECT funding_id, MAX(trans_date) AS last_payment_date
--	FROM transactions t
--	WHERE trans_type_id IN (2, 3, 4, 5) 
--	GROUP BY funding_id
--) x ON f.id = x.funding_id
WHERE contract_status = 1 AND frequency IS NOT NULL AND calc_turn IS NOT NULL AND COALESCE(on_hold, 0) = 0
*/


-- 2018-10-23 - RKB: Changed this around a little.  Just go through each funding in the db that is not marked complete 
-- and create payment forecasts using the payment schedules or if none, then use the original payment plan.  
-- Also take into account the payoff amount and reduce that as we go to get a true list of expected payments.

-- first make a temp table of all fundings that are not complete. add identity field so that we can use while loop to loop through
-- them instead of a cursor

IF OBJECT_ID('tempdb..#tempFundings') IS NOT NULL DROP TABLE #tempFundings
CREATE TABLE #tempFundings (id int not null identity, funding_id BIGINT)

IF OBJECT_ID('tempdb..#tempSchedules') IS NOT NULL DROP TABLE #tempSchedules
CREATE TABLE #tempSchedules (id int not null identity, payment_schedule_id INT)

-- DELETE any future expected payments since they will all get recreated with current data.
DELETE p
FROM payment_forecasts p
WHERE p.payment_date >= @tomorrow

-- get a list of fundings that we are still expecting payments on
INSERT INTO #tempFundings (funding_id)
SELECT f.id
FROM fundings f
WHERE f.contract_status = 1 -- RKB 2018-10-26 Don't actually want to put in expected payments for non-active contracts --IN (1,3,4) AND COALESCE(f.contract_substatus_id, -1) <> 2
	--AND f.payoff_amount - COALESCE(f.float_amount, 0) > 0
	AND f.payoff_amount > 0
ORDER BY f.id

DECLARE @minID BIGINT, @maxID BIGINT, @tempID BIGINT
SELECT @minID = MIN(id), @maxID = MAX(id) FROM #tempFundings
SET @tempID = @minID

WHILE @tempID <= @maxID
	BEGIN
		SELECT @funding_id = funding_id FROM #tempFundings WHERE id = @tempID
		DECLARE @tempDate DATE; SET @tempDate = @tomorrow
		DECLARE @payoff_amount MONEY--, @float_amount MONEY, 
		DECLARE @remaining_amount MONEY, @tempAmount MONEY, @orig_payment MONEY

		--INSERT INTO temp_trash(funding_id) VALUES (@funding_id)

		SELECT @payoff_amount = payoff_amount, --@float_amount = float_amount, 
			@frequency = frequency, @payment = CASE frequency WHEN 'Daily' THEN weekday_payment ELSE weekly_payment END, @first_payment_date = first_payment_date
		FROM fundings WHERE id = @funding_id

		-- if this funding id does not have a payment schedule, then use what is on the funding
		IF EXISTS (
			SELECT 1 FROM funding_payment_schedules fps
			INNER JOIN payment_schedules ps ON fps.payment_schedule_id = ps.payment_schedule_id
			WHERE funding_id = @funding_id AND COALESCE(ps.active, 1) = 1) 
			BEGIN
				-- First, fill temp table with all payment schedules for this funding
				INSERT INTO #tempSchedules (payment_schedule_id)
				SELECT ps.payment_schedule_id
				FROM funding_payment_schedules fps
				INNER JOIN payment_schedules ps ON fps.payment_schedule_id = ps.payment_schedule_id
				WHERE fps.funding_id = @funding_id AND COALESCE(ps.active, 1) = 1
				ORDER BY COALESCE(ps.end_date, '2050-12-31')  -- get any that have no end date at the end

			--	DECLARE @todaysPayments MONEY
			--	SELECT @todaysPayments = SUM(amount) FROM payment_forecasts WHERE funding_id = @funding_id AND payment_date = @today
 				
				---- now, remove any records for this funding and payment schedule after today's date
				--DELETE p
				--FROM payment_forecasts p
				--INNER JOIN #tempSchedules t ON p.payment_schedule_id = t.payment_schedule_id
				--WHERE p.funding_id = @funding_id AND p.payment_date >= @tomorrow

				DECLARE @tempScheduleID INT, @minSchedID INT, @maxSchedID INT
				SELECT @tempScheduleID = MIN(id), @minSchedID = MIN(id), @maxSchedID = MAX(id)
				FROM #tempSchedules
				DECLARE @runningTotal MONEY; SET @runningTotal = 0

				WHILE @tempScheduleID <= @maxSchedID
					BEGIN
						SET @tempDate = @tomorrow
						DECLARE @end_date DATE;
						SELECT @payment_schedule_id = payment_schedule_id FROM #tempSchedules WHERE id = @tempScheduleID
						SELECT @frequency = ps.frequency, @payment = ps.amount, @end_date = ps.end_date, @first_payment_date = ps.start_date
						FROM payment_schedules ps 
						WHERE ps.payment_schedule_id = @payment_schedule_id

						-- if there is an end date, don't worry about remaining amount, just dump it all in
						IF @end_date IS NOT NULL
							BEGIN
								WHILE @tempDate <= @end_date
									BEGIN											
										IF @frequency = 'Daily'
											BEGIN
												IF DATEPART(dw, @tempDate) IN (2,3,4,5,6) -- only do Monday Through Friday even though CC might have weekend amounts
													BEGIN
														INSERT INTO payment_forecasts(funding_id, payment_date, amount, payment_schedule_id)
														SELECT @funding_id, @tempDate, @payment, @payment_schedule_id

														SET @runningTotal = @runningTotal + @payment
													END
											END
										ELSE IF @frequency = 'Weekly'
											BEGIN
												IF DATEPART(dw, @tempDate) = DATEPART(dw, @first_payment_date)
													BEGIN
														-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
														INSERT INTO payment_forecasts(funding_id, payment_date, amount, payment_schedule_id)
														SELECT @funding_id, @tempDate, @payment, @payment_schedule_id

														SET @runningTotal = @runningTotal + @payment
													END
											END
										ELSE IF @frequency = 'Monthly'
											BEGIN
												IF DAY(@tempDate) = DAY(@first_payment_date)
													BEGIN
														-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
														INSERT INTO payment_forecasts(funding_id, payment_date, amount, payment_schedule_id)
														SELECT @funding_id, @tempDate, @payment, @payment_schedule_id

														SET @runningTotal = @runningTotal + @payment
													END
											END
										-- increment @tempDate
										SET @tempDate = DATEADD(D, 1, @tempDate)
									END		
							END
						ELSE
							BEGIN
								-- end date is null
							--	SET @remaining_amount = @payoff_amount - @float_amount - @runningTotal - COALESCE(@todaysPayments, 0)
								SET @remaining_amount = @payoff_amount - @runningTotal -- - COALESCE(@todaysPayments, 0)
								SET @orig_payment = @payment -- @payment can be changed

								WHILE @remaining_amount > 0
									BEGIN
									--	PRINT CONVERT(VARCHAR(50), @funding_id) + ':' + CONVERT(VARCHAR(50), @remaining_amount)
										SET @tempAmount = @remaining_amount - @payment
										IF @tempAmount < 0
											BEGIN
												SET @payment = @remaining_amount
											END
										IF @frequency = 'Daily'
											BEGIN
												IF DATEPART(dw, @tempDate) IN (2,3,4,5,6) -- only do Monday Through Friday even though CC might have weekend amounts
													BEGIN
														INSERT INTO payment_forecasts(funding_id, payment_date, amount, payment_schedule_id)
														SELECT @funding_id, @tempDate, @payment, @payment_schedule_id

														SET @remaining_amount = @remaining_amount - @payment
													END
											END
										ELSE IF @frequency = 'Weekly'
											BEGIN
												IF DATEPART(dw, @tempDate) = DATEPART(dw, @first_payment_date)
													BEGIN
														-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
														INSERT INTO payment_forecasts(funding_id, payment_date, amount, payment_schedule_id)
														SELECT @funding_id, @tempDate, @payment, @payment_schedule_id

														SET @remaining_amount = @remaining_amount - @payment
													END
											END
										ELSE IF @frequency = 'Monthly'
											BEGIN
												IF DAY(@tempDate) = DAY(@first_payment_date)
													BEGIN
														-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
														INSERT INTO payment_forecasts(funding_id, payment_date, amount, payment_schedule_id)
														SELECT @funding_id, @tempDate, @payment, @payment_schedule_id

														SET @remaining_amount = @remaining_amount - @payment
													END
											END
										-- increment @tempDate
										SET @tempDate = DATEADD(D, 1, @tempDate)		
										-- reset @payment variable
										SET @payment = @orig_payment						
									END	
							END
							SET @tempScheduleID  = @tempScheduleID + 1
					END

				-- finally truncate temp table
				TRUNCATE TABLE #tempSchedules

				-- need to clear out any extras.  For example, when there are multiple schedules with no end date, each schedule plays all the way out
				-- because of the loop and is unaware of the others.
				SET @remaining_amount = @payoff_amount --  - COALESCE(@todaysPayments, 0)
				-- insert all future payments into a temp table with identity so we can loop through and keep a running total.  Then we can delete any that
				-- go beyond once we find an ending, won't match exactly.
				IF OBJECT_ID('tempdb..#tempFutures') IS NOT NULL DROP TABLE #tempFutures
				CREATE TABLE #tempFutures (id int not null identity, payment_forecast_id INT, amount MONEY)
				INSERT INTO #tempFutures(payment_forecast_id, amount)
				SELECT f.id, f.amount
				FROM payment_forecasts f
				WHERE f.payment_date > @today AND f.funding_id = @funding_id
				ORDER BY f.payment_date

				SELECT @maxSchedID = MAX(id), @tempScheduleID = 1 FROM #tempFutures				
				WHILE @tempScheduleID <= @maxSchedID AND @remaining_amount > 0
					BEGIN
						SELECT @remaining_amount = @remaining_amount - amount
						FROM #tempFutures
						WHERE id = @tempScheduleID

						SET @tempScheduleID = @tempScheduleID + 1
					END

				-- once the loop finishes, if @remaining_amount <=0 then AND @tempScheduleID < @maxSchedID we know there are more payments that did not get processed
				IF @remaining_amount <= 0 AND @tempScheduleID < @maxSchedID
					BEGIN
						DELETE f
						FROM payment_forecasts f
						INNER JOIN #tempFutures t ON f.id = t.payment_forecast_id
						WHERE t.id > @tempScheduleID
					END
			END
		ELSE
			-- use the funding original payment plan
			BEGIN
				-- first, remove any records for this funding and payment schedule after today's date
				--DELETE FROM payment_forecasts WHERE funding_id = @funding_id AND payment_date >= @tomorrow
				
				IF @payment IS NOT NULL AND @frequency IS NOT NULL
					BEGIN
						SET @remaining_amount = @payoff_amount --   - COALESCE(@todaysPayments, 0)
						SET @orig_payment = @payment -- @payment can be changed

						WHILE @remaining_amount > 0
							BEGIN
							--	PRINT CONVERT(VARCHAR(50), @funding_id) + ':' + CONVERT(VARCHAR(50), @remaining_amount)
								SET @tempAmount = @remaining_amount - @payment
								IF @tempAmount < 0
									BEGIN
										SET @payment = @remaining_amount
									END
								IF @frequency = 'Daily'
									BEGIN
										IF DATEPART(dw, @tempDate) IN (2,3,4,5,6) -- only do Monday Through Friday even though CC might have weekend amounts
											BEGIN
												INSERT INTO payment_forecasts(funding_id, payment_date, amount, payment_schedule_id)
												SELECT @funding_id, @tempDate, @payment, NULL

												SET @remaining_amount = @remaining_amount - @payment
											END
									END
								ELSE IF @frequency = 'Weekly'
									BEGIN
										IF DATEPART(dw, @tempDate) = DATEPART(dw, @first_payment_date)
											BEGIN
												-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
												INSERT INTO payment_forecasts(funding_id, payment_date, amount, payment_schedule_id)
												SELECT @funding_id, @tempDate, @payment, NULL

												SET @remaining_amount = @remaining_amount - @payment
											END
									END
								ELSE IF @frequency = 'Monthly'
									BEGIN
										IF DAY(@tempDate) = DAY(@first_payment_date)
											BEGIN
												-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
												INSERT INTO payment_forecasts(funding_id, payment_date, amount, payment_schedule_id)
												SELECT @funding_id, @tempDate, @payment, NULL

												SET @remaining_amount = @remaining_amount - @payment
											END
									END
								-- increment @tempDate
								SET @tempDate = DATEADD(D, 1, @tempDate)		
								-- reset @payment variable
								SET @payment = @orig_payment						
							END	
					END	
			END

		SET @tempID = @tempID + 1
	END

/*
SELECT id, CONVERT(DATE, DATEADD(D, est_months_left * 30.4, GETDATE())) AS expected_final_payment_date, frequency, 
	CASE frequency WHEN 'Weekly' THEN weekly_payment ELSE weekday_payment END AS payment, f.first_payment_date, NULL AS payment_schedule_id
FROM fundings f
WHERE NOT EXISTS (SELECT 1 FROM funding_payment_schedules WHERE funding_id = f.id)
	AND f.contract_status IN (1,3,4) AND COALESCE(f.contract_substatus_id, -1) <> 2
-- now get those that do have a payment schedule
UNION ALL
SELECT f.id, COALESCE(ps.end_date, CONVERT(DATE, DATEADD(D, f.est_months_left * 30.4, GETDATE()))) AS expected_final_payment_date, 
	ps.frequency, ps.amount AS payment, ps.start_date, ps.payment_schedule_id
FROM fundings f
INNER JOIN funding_payment_schedules fps ON f.id = fps.funding_id
INNER JOIN payment_schedules ps ON fps.payment_schedule_id = ps.payment_schedule_id
WHERE f.contract_status IN (1,3,4) AND COALESCE(f.contract_substatus_id, -1) <> 2
	AND COALESCE(ps.active, 1) = 1
	AND COALESCE(ps.end_date, '2050-12-31') >= @tomorrow
	*/

	/*
OPEN fundings_cursor

FETCH NEXT FROM fundings_cursor INTO @funding_id, @final_payment_date, @frequency, @amount, @first_payment_date, @payment_schedule_id
WHILE @@FETCH_STATUS = 0	
	BEGIN		
		-- first, remove any records for this funding and payment schedule after today's date
		DELETE FROM payment_forecasts WHERE funding_id = @funding_id AND payment_date >= @tomorrow AND COALESCE(payment_schedule_id, -1) = COALESCE(@payment_schedule_id, -1)

		-- now loop through the days from tomorrow on to the @final_payment_date
		DECLARE @tempDate DATE; SET @tempDate = @tomorrow
		WHILE @tempDate <= @final_payment_date
			BEGIN											
				IF @frequency = 'Daily'
					BEGIN
						IF DATEPART(dw, @tempDate) IN (2,3,4,5,6) -- only do Monday Through Friday even though CC might have weekend amounts
							BEGIN
								INSERT INTO payment_forecasts(funding_id, payment_date, amount, payment_schedule_id)
								SELECT @funding_id, @tempDate, @amount, @payment_schedule_id
							END
					END
				ELSE IF @frequency = 'Weekly'
					BEGIN
						IF DATEPART(dw, @tempDate) = DATEPART(dw, @first_payment_date)
							BEGIN
								-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
								INSERT INTO payment_forecasts(funding_id, payment_date, amount, payment_schedule_id)
								SELECT @funding_id, @tempDate, @amount, @payment_schedule_id
							END
					END
				-- increment @tempDate
				SET @tempDate = DATEADD(D, 1, @tempDate)
				-- PRINT CONVERT(VARCHAR (50), @tempDate) + ':' + CONVERT(VARCHAR (50), @funding_id)
			END		
		FETCH NEXT FROM fundings_cursor INTO @funding_id, @final_payment_date, @frequency, @amount, @first_payment_date, @payment_schedule_id
	END 	

CLOSE fundings_cursor
DEALLOCATE fundings_cursor
*/

-- now update the received amount.  Looks at the last 14 days.
/*
UPDATE p
SET rec_amount = COALESCE(x.rec_amount, 0)
FROM payment_forecasts p
INNER JOIN (
	SELECT SUM(trans_amount) AS rec_amount, t.funding_id, t.trans_date
	FROM transactions t 
	WHERE trans_type_id IN (2, 3, 4, 5, 19) AND t.trans_date BETWEEN GETDATE() - 14 AND GETDATE()
	GROUP BY t.funding_id, t.trans_date
) x ON p.funding_id = x.funding_id AND p.payment_date = x.trans_date
*/
UPDATE pf
SET rec_amount = COALESCE(p.amount, 0)
FROM payment_forecasts pf
INNER JOIN payments p ON pf.funding_id = p.funding_id AND pf.payment_date = p.trans_date AND p.transaction_id IS NOT NULL
WHERE pf.payment_date BETWEEN GETDATE() - 14 AND GETDATE()

-- cleanup
-- delete any records in the future ( >= @tomorrow) for fundings that are no longer in active
/*
DELETE p
FROM payment_forecasts p
LEFT JOIN fundings f ON p.funding_id = f.id
WHERE p.payment_date >= @tomorrow
	AND (
			f.id IS NULL -- funding no longer exists for some reason so delete future payment forecasts
		OR
			(f.contract_status = 2 
			OR (f.contract_status = 4 AND f.contract_substatus_id = 2)
			)
	)
*/

/*
-- delete any in the future that are on inactive payment schedules
DELETE p
FROM payment_forecasts p
INNER JOIN payment_schedules ps ON p.payment_schedule_id = ps.payment_schedule_id
WHERE p.payment_date >= @tomorrow AND COALESCE(ps.active, 1) = 0
*/

IF OBJECT_ID('tempdb..#tempFundings') IS NOT NULL DROP TABLE #tempFundings
IF OBJECT_ID('tempdb..#tempSchedules') IS NOT NULL DROP TABLE #tempSchedules
IF OBJECT_ID('tempdb..#tempFutures') IS NOT NULL DROP TABLE #tempFutures
END
GO
