SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-05-02
-- Description:	Returns a funding record and will accept either funding_id from Forte or the identity one (forte_funding_id)
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetForteFunding]
(
	@forte_funding_id		INT = NULL,
	@funding_id				NVARCHAR (50) = NULL
)
AS
BEGIN	
SET NOCOUNT ON;

SELECT f.forte_funding_id, f.funding_id, f.status, f.effective_date, f.origination_date, f.net_amount, f.routing_number, f.entry_description, f.batch_id, f.create_date, 
	f.change_date, f.change_user, b.batch_status
FROM forte_fundings f WITH (NOLOCK)
LEFT JOIN batches b WITH (NOLOCK) ON f.batch_id = b.batch_id
WHERE f.funding_id = COALESCE(@funding_id, '-1') OR f.forte_funding_id = COALESCE(@forte_funding_id, -1)

END
GO
