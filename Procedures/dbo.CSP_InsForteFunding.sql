SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-05-21
-- Description:	Will check to see if the funding is already in the db.  If so, it will be ignored.  
-- Uniqueness is determined by funding_id
-- =============================================
CREATE PROCEDURE [dbo].[CSP_InsForteFunding]
(
	@funding_id				NVARCHAR (50),
	@status					NVARCHAR (50), 
	@effective_date			DATETIME,
	@origination_date		DATETIME,
	@net_amount				MONEY,
	@routing_number			NVARCHAR (50),
	@entry_description		NVARCHAR (50),
	@batch_id				INT,
	@change_user			NVARCHAR (100)
)	
AS
BEGIN
SET NOCOUNT ON;


IF NOT EXISTS(SELECT 1 FROM forte_fundings WHERE funding_id = @funding_id)
	BEGIN
		INSERT INTO forte_fundings(funding_id, status, effective_date, origination_date, net_amount, routing_number, entry_description, 
			batch_id, create_date, change_date, change_user)
		VALUES (@funding_id, @status, @effective_date, @origination_date, @net_amount, @routing_number, @entry_description, 
			@batch_id, GETUTCDATE(), GETUTCDATE(), @change_user)
	END
ELSE
	BEGIN
		-- if it already exists it may just need a status update
		UPDATE forte_fundings SET status = @status WHERE funding_id = @funding_id
	END
END
GO
