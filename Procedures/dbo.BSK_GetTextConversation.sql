SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-12-02
-- Description:	returns the entire text conversation history for a given number
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetTextConversation]
(
	@phone_number			NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON;

SELECT t.text_id, CASE WHEN t.[to] = @phone_number THEN 1 ELSE 0 END AS from_me,
    CONVERT(DATETIME, SWITCHOFFSET(COALESCE(t.received, t.sent), DATEPART(TZOFFSET, COALESCE(t.received, t.sent) AT TIME ZONE 'Eastern Standard Time'))) AS received, t.text_message,
    t.create_date
FROM texts t WITH (NOLOCK)
WHERE (t.[to] = @phone_number OR t.[from] = @phone_number) AND t.text_message IS NOT NULL
UNION ALL
SELECT t.text_id, CASE WHEN t.[to] = @phone_number THEN 1 ELSE 0 END AS from_me,
    CONVERT(DATETIME, SWITCHOFFSET(COALESCE(t.received, t.sent), DATEPART(TZOFFSET, COALESCE(t.received, t.sent) AT TIME ZONE 'Eastern Standard Time')))AS received, ti.image_url,
    ti.create_date
FROM texts t WITH (NOLOCK)
INNER JOIN text_images ti WITH (NOLOCK) ON t.text_id = ti.text_id
WHERE (t.[to] = @phone_number OR t.[from] = @phone_number) AND ti.image_url IS NOT NULL
ORDER BY create_date

END

GO
