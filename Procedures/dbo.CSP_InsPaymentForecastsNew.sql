SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-02-17
-- Description:	Fills in the payment_forecasts table 
-- Created a NEW one and left the old one alone because there were too many changes.  Much simpler now, but wanted to keep old one just for reference
-- =============================================
CREATE PROCEDURE [dbo].[CSP_InsPaymentForecastsNew]
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @tomorrow DATE, @today DATE
SELECT @today = CONVERT(DATE, DATEADD(HH, -5, GETUTCDATE()))  
SELECT @tomorrow = DATEADD(DD, CASE DATEPART(WEEKDAY, @today) WHEN 6 THEN 3 WHEN 7 THEN 2 ELSE 1 END, @today)


-- DELETE any future expected payments since they will all get recreated with current data.
DELETE p
FROM payment_forecasts p
WHERE p.payment_date > @tomorrow

INSERT INTO payment_forecasts(funding_id, payment_date, amount, funding_payment_date_id, change_date, change_user)
SELECT pd.funding_id, pd.payment_date, pd.amount, pd.id, GETUTCDATE(), 'SYSTEM'
FROM funding_payment_dates pd
INNER JOIN fundings f ON pd.funding_id = f.id
WHERE pd.payment_date > @tomorrow AND pd.active = 1 AND COALESCE(f.on_hold, 0) = 0

/*
UPDATE pf
SET rec_amount = COALESCE(p.amount, 0)
FROM payment_forecasts pf
INNER JOIN payments p ON (pf.funding_id = p.funding_id AND pf.payment_date = p.trans_date AND p.transaction_id IS NOT NULL) OR pf.funding_payment_date_id = p.funding_payment_date_id
WHERE pf.payment_date BETWEEN GETDATE() - 14 AND GETDATE()
*/
UPDATE pf
SET rec_amount = COALESCE(pd.rec_amount, 0) - COALESCE(pd.chargeback_amount, 0), change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM payment_forecasts pf 
INNER JOIN funding_payment_dates pd ON pf.funding_payment_date_id = pd.id
WHERE pf.payment_date BETWEEN GETDATE() - 14 AND GETDATE()

END
GO
