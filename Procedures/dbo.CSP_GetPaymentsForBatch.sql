SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


--   EXEC dbo.CSP_GetFTPSettings 'Regions'



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-02-11
-- Description:	Gets a list of payments based on Batch ID - In use by NACHA after receiving an ACK file
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetPaymentsForBatch]
(
	@batch_id				INT
)
AS
BEGIN
SET NOCOUNT ON;

SELECT p.amount, p.batch_id, p.complete, p.funding_id, p.is_fee, p.orig_transaction_id, p.previous_transaction_id, p.payment_id, p.payment_schedule_id, p.previous_transaction_id,
	p.processed_date, p.retry_complete, p.retry_level, p.retry_sublevel, p.trans_date, p.transaction_id, f.legal_name 
FROM payments p
LEFT JOIN fundings f ON p.funding_id = f.id
WHERE p.batch_id = @batch_id
	-- AND p.is_refund = 0 -- 2021-03-31 no more advice file, refunds sent with Regions
	-- 2019-08-01 needed to NOT include refunds here because when we get the advice file back we create the refund transaction.  from this dataset will be created
		-- draft records so we don't want it here.

END

GO
