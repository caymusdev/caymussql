SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-15
-- Description:	Saves a phone call into the database
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsPhoneCall]
(
	@affiliate_id				INT,
	@merchant_id				INT,
	@contract_id				INT, 
	@user_email					NVARCHAR (100),
	@call_duration				INT,
	@start_time					DATETIME,
	@end_time					DATETIME,
	@disposition				VARCHAR (50),
	@call_id					NVARCHAR (200) = NULL,
	@conversation_id			NVARCHAR (200)= NULL,
	@incoming_call				BIT = NULL
)
AS
BEGIN
SET NOCOUNT ON;
DECLARE @contact_id INT, @record_id INT, @first_call_log_id INT, @call_to NVARCHAR (50), @call_from NVARCHAR (50)

DECLARE @user_id INT
SELECT @user_id = user_id FROM users WHERE email = @user_email

-- get the first phone log for this record
SELECT @first_call_log_id = MIN(phone_log_id)
FROM phone_logs l WITH (NOLOCK)
WHERE (l.call_id = @call_id OR l.conversation_id = @call_id)

-- if start time is null find it from the first record for this call.
IF @start_time IS NULL
	BEGIN
		SELECT @start_time = start_time
		FROM phone_logs l WITH (NOLOCK)
		WHERE l.phone_log_id = @first_call_log_id
	END


-- first record has the appropriate call to and call from 
SELECT @call_to = l.call_to, @call_from = l.call_from, @affiliate_id = COALESCE(@affiliate_id, l.affiliate_id)
FROM phone_logs l WITH (NOLOCK) 
WHERE l.phone_log_id = @first_call_log_id


INSERT INTO phone_calls([user_id], [call_to], [call_from], affiliate_id, merchant_id, contract_id, call_duration, start_time, end_time, disposition, create_date, change_date, change_user, create_user, 
	call_id, conversation_id, incoming_call)
VALUES (@user_id, @call_to, @call_from, @affiliate_id, @merchant_id, @contract_id, @call_duration, @start_time, @end_time, @disposition, GETUTCDATE(), GETUTCDATE(), @user_email, @user_email, 
	@call_id, @conversation_id, @incoming_call)

SELECT @record_id = SCOPE_IDENTITY()


SELECT @contact_id = c.contact_id
FROM contacts c
INNER JOIN phone_numbers p ON c.contact_id = p.contact_id
WHERE c.affiliate_id = @affiliate_id AND p.phone_number = @call_from

EXEC dbo.BSK_InsContactLog @affiliate_id, @merchant_id, @contract_id, 'phone', @contact_id, '', null, '', @user_email, '', 	@record_id

END

GO
