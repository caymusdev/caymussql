SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-06-03
-- Description:	Inserts records and updates existing ones in the merchant audit table
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdMerchantAudit]
--(	
--	@funding_id			BIGINT = NULL
--)
AS
BEGIN	
SET NOCOUNT ON;

-- first need to make sure there is the opening balance (portfolio value) for each funding
INSERT INTO merchant_audit (funding_id, trans_id, trans_date, trans_amount, trans_type, payment_id, is_fee, orig_payment_id, previous_payment_id, balance, portfolio_value, transaction_id, 
	orig_transaction_id, previous_transaction_id, retry_level, retry_sublevel, retry_level_text, response_code)
SELECT f.id, -1, f.funded_date, f.portfolio_value, NULL, NULL, 0, NULL, NULL, f.portfolio_value, f.portfolio_value, NULL, NULL, NULL, NULL, NULL, NULL, NULL
FROM fundings f
WHERE f.id NOT IN (SELECT funding_id FROM merchant_audit WHERE portfolio_value IS NOT NULL)



IF OBJECT_ID('tempdb..#tempIDs') IS NOT NULL DROP TABLE #tempIDs
CREATE TABLE #tempIDs (id int not null identity, trans_id INT, funding_id BIGINT)

IF OBJECT_ID('tempdb..#tempFundingIDs') IS NOT NULL DROP TABLE #tempFundingIDs
CREATE TABLE #tempFundingIDs (id int not null identity, funding_id BIGINT, portfolio_value MONEY)

INSERT INTO #tempIDs (trans_id, funding_id)
SELECT t.trans_id, t.funding_id
FROM transactions t
LEFT JOIN batches b ON t.batch_id = b.batch_id
WHERE t.merchant_audit_id IS NULL AND t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 
ORDER BY t.trans_date, t.trans_id


-- now, get a list of unique fundings affected by running this
INSERT INTO #tempFundingIDs (funding_id, portfolio_value)
SELECT DISTINCT t.funding_id, f.portfolio_value
FROM #tempIDs t
INNER JOIN fundings f ON t.funding_id = f.id

INSERT INTO merchant_audit (funding_id, trans_id, trans_date, trans_amount, trans_type, payment_id, is_fee, orig_payment_id, previous_payment_id, balance, portfolio_value, transaction_id, 
	orig_transaction_id, previous_transaction_id, retry_level, retry_sublevel, retry_level_text, response_code, trans_comments)
SELECT x.funding_id, x.trans_id, x.trans_date, x.trans_amount, x.trans_type_id, x.payment_id, x.is_fee, x.orig_payment_id, x.previous_payment_id, x.balance, x.portfolio_value, 
	x.transaction_id, x.orig_transaction_id, x.previous_transaction_id, x.retry_level, x.retry_sublevel, x.retry_level_text, x.response_code, x.comments
FROM (
	SELECT DISTINCT x.funding_id, x.trans_id, x.trans_date, x.trans_amount, x.trans_type_id, x.payment_id,  
		x.is_fee, p.payment_id AS orig_payment_id, p2.payment_id AS previous_payment_id, NULL AS balance, NULL AS portfolio_value, x.transaction_id, 
		x.orig_transaction_id, x.previous_transaction_id, x.retry_level, x.retry_sublevel, 
		CONVERT(VARCHAR (50), x.retry_level) + ' - ' + CONVERT(VARCHAR (50), x.retry_sublevel) AS retry_level_text, 
		CASE WHEN x.trans_type_id IN (3, 18, 21, 14) 
			THEN 
				CASE WHEN b.batch_id IS NULL THEN COALESCE(fs.settle_response_code, fsb.settle_response_code, fs2.settle_response_code, ft.status, nt.reason_code, ntb.reason_code)
					ELSE CASE WHEN NULLIF(x.comments, '') IS NULL 
						THEN COALESCE(fs.settle_response_code, fsb.settle_response_code, fs2.settle_response_code, ft.status, nt.reason_code, ntb.reason_code)
						ELSE CASE CHARINDEX(' ', x.comments) WHEN 0 THEN x.comments ELSE 
							CASE REVERSE(LEFT(REVERSE(REPLACE(x.comments, ' : withdrawal', '')), CHARINDEX(' ', REVERSE(REPLACE(x.comments, ' : withdrawal', '')))-1)) 
							WHEN 'deposit' THEN 'S01' 
							ELSE CASE CHARINDEX(' : reject', x.comments) 
								WHEN 0 THEN REVERSE(LEFT(REVERSE(REPLACE(x.comments, ' : withdrawal', '')), CHARINDEX(' ', REVERSE(REPLACE(x.comments, ' : withdrawal', '')))-1)) 
								ELSE REVERSE(LEFT(REVERSE(REPLACE(x.comments, ' : withdrawal', '')), CHARINDEX(' ', REVERSE(REPLACE(x.comments, ' : withdrawal', '')))-1)) END
							END
						END
					END
				END
			ELSE ''
		END AS response_code, x.comments
	FROM (
		SELECT t.trans_id, t.trans_date, 
			CASE t.trans_type_id WHEN 9 THEN t.trans_amount ELSE -1.00 * t.trans_amount END AS trans_amount, t.trans_type_id,
			COALESCE(t.transaction_id, p.transaction_id) AS transaction_id, 
			CASE WHEN t.payment_id IS NULL THEN COALESCE(p2.orig_transaction_id, p2.transaction_id)
				WHEN p.orig_transaction_id IS NULL THEN COALESCE(t.transaction_id, p.transaction_id)
				ELSE p.orig_transaction_id END AS orig_transaction_id, 
			CASE WHEN t.payment_id IS NULL THEN COALESCE(p.previous_transaction_id, p2.transaction_id) ELSE p.previous_transaction_id END AS previous_transaction_id, 
			p.payment_id, t.funding_id, 
			CASE WHEN (p.is_fee = 1 OR t.trans_type_id = 9) THEN 1 ELSE 0 END AS is_fee, 
			--CASE WHEN p.is_fee = 1 THEN p.retry_level + 1 ELSE NULLIF(p.retry_level, 0) END AS retry_level, 
			NULLIF(p.retry_level, 0) AS retry_level,
			p.retry_sublevel + 1 AS retry_sublevel, t.batch_id, t.comments, t.record_id
		FROM transactions t
		LEFT JOIN transaction_details d ON t.trans_id = d.trans_id AND t.trans_type_id = 3 AND d.trans_type_id = 3
		LEFT JOIN payments p ON t.payment_id = p.payment_id AND p.deleted = 0 AND p.waived = 0
		LEFT JOIN payments p2 ON t.payment_id IS NULL AND t.transaction_id = p2.transaction_id AND p2.deleted = 0 AND p2.waived = 0
		LEFT JOIN batches b ON t.batch_id = b.batch_id
		WHERE d.trans_detail_id IS NULL
			AND t.redistributed = 0
			AND t.merchant_audit_id IS NULL
			AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 	
		--ORDER BY t.trans_date
	) x
	INNER JOIN fundings f ON x.funding_id = f.id
	LEFT JOIN payments p ON COALESCE(x.orig_transaction_id, '-1') = COALESCE(p.transaction_id, '-2')
	LEFT JOIN payments p2 ON COALESCE(x.previous_transaction_id, '-1') = COALESCE(p2.transaction_id, '-2')
	LEFT JOIN dbo.batches b ON x.batch_id = b.batch_id AND (b.batch_type LIKE '%FifthThird%' OR b.batch_type LIKE '%Regions%')
	LEFT JOIN forte_settlements fs ON x.transaction_id = fs.transaction_id AND fs.settle_response_code LIKE 'S0%'
	LEFT JOIN forte_settlements fsb ON x.transaction_id = fsb.transaction_id AND fsb.settle_response_code NOT LIKE 'S0%'
	LEFT JOIN forte_trans ft ON x.transaction_id = ft.transaction_id
	LEFT JOIN forte_settlements fs2 ON x.transaction_id IS NULL AND x.record_id = fs2.forte_settle_id
	LEFT JOIN nacha_trans nt WITH (NOLOCK) ON x.transaction_id = CONVERT(VARCHAR (50), nt.payment_id) AND nt.reason_code LIKE 'S0%' AND x.trans_type_id NOT IN (14, 21)
	LEFT JOIN nacha_trans ntb WITH (NOLOCK) ON x.transaction_id = CONVERT(VARCHAR (50), ntb.payment_id) AND ntb.reason_code NOT LIKE 'S0%' AND x.trans_type_id IN (14, 21)
) x
WHERE NOT (x.response_code = 'S01' AND x.trans_type_id = 9) -- because some settle and then reject, the fee has the transaction_id so it gets associated with both S01 and R01.  The S01 is not relevant here.
ORDER BY x.trans_date, x.trans_id, x.is_fee

-- update transactions with their new merchant audit it
UPDATE t
SET merchant_audit_id = m.merchant_audit_id, change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM transactions t
INNER JOIN merchant_audit m ON t.trans_id = m.trans_id AND t.merchant_audit_id IS NULL

-- now go through fundings and set the balance.
DECLARE @current_id INT, @max_id INT, @temp_funding_id BIGINT, @temp_portfolio_value MONEY
SELECT @current_id = 1, @max_id = MAX(id) FROM #tempFundingIDs

WHILE @current_id <= @max_id -- AND 1 = 2
	BEGIN
		SELECT @temp_funding_id = funding_id, @temp_portfolio_value = portfolio_value FROM #tempFundingIDs WHERE id = @current_id

		UPDATE m
		SET balance = x.balance
		FROM merchant_audit m
		INNER JOIN (
			SELECT m.merchant_audit_id, @temp_portfolio_value + SUM(m.trans_amount) OVER (ORDER BY m.trans_date, m.trans_id, m.is_fee) AS balance
			FROM merchant_audit m
			WHERE m.funding_id = @temp_funding_id AND m.portfolio_value IS NULL
		) x ON m.merchant_audit_id = x.merchant_audit_id
		

		SET @current_id = @current_id + 1
	END


-- now update payment_path on each one
-- 2 routes.  First do transactions that have a payment id
UPDATE m
SET payment_path = COALESCE(',' + CONVERT(VARCHAR (50), p5.payment_id) + ',', '') +
		COALESCE(',' + CONVERT(VARCHAR (50), p4.payment_id) + ',', '') +	
		COALESCE(',' + CONVERT(VARCHAR (50), p3.payment_id) + ',', '') +
		COALESCE(',' + CONVERT(VARCHAR (50), p2.payment_id) + ',', '') +
		COALESCE(',' + CONVERT(VARCHAR (50), p1.payment_id) + ',', '') +
		COALESCE(',' + CONVERT(VARCHAR (50), p0.payment_id) + ',', '') 
FROM merchant_audit m
INNER JOIN payments p0 WITH (NOLOCK) ON m.payment_id = p0.payment_id
LEFT JOIN payments p1 WITH (NOLOCK) ON COALESCE(p0.previous_transaction_id, p0.orig_transaction_id) = p1.transaction_id
LEFT JOIN payments p2 WITH (NOLOCK) ON COALESCE(p1.previous_transaction_id, p1.orig_transaction_id) = p2.transaction_id
LEFT JOIN payments p3 WITH (NOLOCK) ON COALESCE(p2.previous_transaction_id, p2.orig_transaction_id) = p3.transaction_id
LEFT JOIN payments p4 WITH (NOLOCK) ON COALESCE(p3.previous_transaction_id, p3.orig_transaction_id) = p4.transaction_id
LEFT JOIN payments p5 WITH (NOLOCK) ON COALESCE(p4.previous_transaction_id, p4.orig_transaction_id) = p5.transaction_id
WHERE m.payment_path IS NULL

-- now do transactions that don't have a payment id but we might be able to get data from the transaction id
UPDATE m
SET payment_path = COALESCE(',' + CONVERT(VARCHAR (50), p13.payment_id) + ',', '') +
		COALESCE(',' + CONVERT(VARCHAR (50), p12.payment_id) + ',', '') +
		COALESCE(',' + CONVERT(VARCHAR (50), p11.payment_id) + ',', '') +
		COALESCE(',' + CONVERT(VARCHAR (50), p10.payment_id) + ',', '') 
FROM merchant_audit m
INNER JOIN payments p10 WITH (NOLOCK) ON m.transaction_id = p10.transaction_id
LEFT JOIN payments p11 WITH (NOLOCK) ON COALESCE(p10.previous_transaction_id, p10.orig_transaction_id) = p11.transaction_id
LEFT JOIN payments p12 WITH (NOLOCK) ON COALESCE(p11.previous_transaction_id, p11.orig_transaction_id) = p12.transaction_id
LEFT JOIN payments p13 WITH (NOLOCK) ON COALESCE(p12.previous_transaction_id, p12.orig_transaction_id) = p13.transaction_id
WHERE m.payment_path IS NULL



-- update the result_desc for each one
UPDATE m
SET result_desc = x._result_desc
--SELECT DISTINCT x.merchant_audit_id, x._result_desc
FROM merchant_audit m
INNER JOIN 
(
	SELECT COALESCE(CASE
		WHEN m.trans_id = -1 THEN 'Original RTR' 
		WHEN m.trans_type = 1 THEN 'Fees Collected: ' + COALESCE(t.comments, '')
		WHEN m.trans_type = 2 THEN 'Counter Deposit: ' + COALESCE(t.comments, '')
		WHEN m.trans_type = 4 THEN 'Credit Card Split: ' + COALESCE(t.comments, '')
		WHEN m.trans_type = 5 THEN 'Wire: ' + COALESCE(t.comments, '')
		WHEN m.trans_type = 19 THEN 'Check: ' + COALESCE(t.comments, '')
		WHEN m.trans_type IN (8, 10, 11, 16, 17, 22, 23, 24, 25) THEN COALESCE(tt.trans_type + ': ', '') + COALESCE(t.comments, '')
		WHEN m.trans_type IN (3,18)
			THEN CASE 
				--WHEN m.response_code = 'S01'
				--	THEN CASE 
						WHEN m.retry_level IS NULL THEN 'Regular Payment'
						ELSE 'Retry Payment (' + COALESCE(p_pre.response_code + ': ', '') + '$' + CONVERT(VARCHAR (50), ABS(p_pre.trans_amount)) + ') from ' + CONVERT(VARCHAR (10), p_pre.trans_date, 126) END
				--ELSE 'Reject' END
		WHEN m.trans_type = 9 THEN
			'Fee for ' + COALESCE(m.response_code, 'reject') + ' of $' + CONVERT(VARCHAR (50), ABS(COALESCE(p_pre.trans_amount, p_orig.trans_amount)))
				+ ' payment from ' + CONVERT(VARCHAR(10), COALESCE(p_pre.trans_date, p_orig.trans_date), 126)
		WHEN m.trans_type = 21 THEN
			'Reject ' + m.response_code + ' of $' + CONVERT(VARCHAR(50), ABS(COALESCE(p_pre.trans_amount, p_orig.trans_amount)))
				+ ' payment from ' + CONVERT(VARCHAR(10), COALESCE(p_pre.trans_date, p_orig.trans_date), 126)
		WHEN m.trans_type = 14 THEN
			'Fee for ' + COALESCE(m.response_code, 'chargeback') + ' of $' + CONVERT(VARCHAR (50), ABS(COALESCE(p_pre.trans_amount, p_orig.trans_amount)))
				+ ' payment from ' + CONVERT(VARCHAR(10), COALESCE(p_pre.trans_date, p_orig.trans_date), 126)
	END, t.comments) AS _result_desc,
	m.trans_type AS _trans_type, m.response_code AS _response_code, m.retry_level AS _retry_level, m.merchant_audit_id	
	FROM merchant_audit m
	LEFT JOIN merchant_audit p_pre ON m.previous_payment_id = p_pre.payment_id 
	LEFT JOIN merchant_audit p_orig ON m.orig_payment_id = p_orig.payment_id 
	LEFT JOIN transactions t ON m.trans_id = t.trans_id
	LEFT JOIN transaction_types tt ON m.trans_type = tt.trans_type_id
	WHERE m.result_desc IS NULL
) x ON m.merchant_audit_id = x.merchant_audit_id
WHERE m.result_desc IS NULL


IF OBJECT_ID('tempdb..#tempIDs') IS NOT NULL DROP TABLE #tempIDs
IF OBJECT_ID('tempdb..#tempFundingIDs') IS NOT NULL DROP TABLE #tempFundingIDs
END


GO
