SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-06-19
-- Description:	returns records for a given batch c# then loops through and creates transactions records
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetIPSStagingForBatch]
(
	@batch_id			INT
)

AS
BEGIN
SET NOCOUNT ON;

SELECT s.ips_staging_id
FROM ips_staging s
WHERE s.BatchID = @batch_id
ORDER BY s.file_date


END
GO
