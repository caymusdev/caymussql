SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-05-30
-- Description:	Gets various SQL values for the main dashboarde
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetDashboard]
(
	@userid				NVARCHAR (100), -- not used right now,
	@date				DATETIME = NULL
)
AS
BEGIN	
SET NOCOUNT ON;

IF @date IS NULL SET @date = GETUTCDATE()

SELECT SUM(trans_amount) AS GrossRevenue
FROM transactions
WHERE trans_type_id IN (2,3, 4, 5, 10, 14)
AND MONTH(trans_date) = MONTH(@date) AND YEAR(trans_date) = YEAR(@date) AND redistributed = 0

SELECT SUM(d.trans_amount) AS GrossMargin
FROM transaction_details d
INNER JOIN transactions t ON d.trans_id = t.trans_id
WHERE d.trans_type_id IN (6)
AND  MONTH(trans_date) = MONTH(@date) AND YEAR(trans_date) = YEAR(@date) AND t.redistributed = 0

SELECT SUM(d.trans_amount) AS GrossPP
FROM transaction_details d
INNER JOIN transactions t ON d.trans_id = t.trans_id
WHERE d.trans_type_id IN (7)
AND  MONTH(trans_date) = MONTH(@date) AND YEAR(trans_date) = YEAR(@date) AND t.redistributed = 0



END
GO
