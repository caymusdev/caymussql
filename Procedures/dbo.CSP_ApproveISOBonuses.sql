SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-09-24
-- Description:	Accepts a delimited list of  ids to approve the ISO bonusesk
-- =============================================
CREATE PROCEDURE [dbo].[CSP_ApproveISOBonuses]
(	
	@ids			NVARCHAR (4000), -- could be a delimited list of funding ids
	@user_id		NVARCHAR (100)
)
AS
BEGIN	
SET NOCOUNT ON;
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SELECT @tomorrow = dbo.CF_GetTomorrow(@today)

UPDATE b
SET pay_date = @tomorrow, change_date = GETUTCDATE(), change_user = @user_id, approved_by = @user_id, approved_date = GETUTCDATE()
FROM iso_bonuses b 
INNER JOIN STRING_SPLIT(@ids, '|') a ON b.iso_bonus_id = a.value

-- put a payment in queue for them
INSERT INTO payments (payment_schedule_id, amount, trans_date, transaction_id, approved_flag, processed_date, change_date, change_user, previous_transaction_id,
	orig_transaction_id, is_fee, retry_level, retry_sublevel, funding_id, complete, batch_id, retry_complete, processor, is_refund, is_debit, deleted, 
	waived, redirect_funding_id, redirect_approval_id, funding_payment_date_id, iso_id, iso_bonus_id)
SELECT NULL, b.bonus_amount, @tomorrow, CONVERT(VARCHAR (50), b.iso_bonus_id), 1, NULL, GETUTCDATE(), @user_id, NULL, NULL, 0 AS is_fee, 0, 0, 
	NULL AS funding_id, NULL, NULL AS batch_id, NULL, NULL AS processor, 1, 0 AS is_debit, 0, 0, NULL, NULL, NULL, b.iso_id, b.iso_bonus_id
FROM iso_bonuses b
INNER JOIN STRING_SPLIT(@ids, '|') a ON b.iso_bonus_id = a.value
INNER JOIN funding_bonuses fb ON b.funding_bonus_id = fb.funding_bonus_id
INNER JOIN funding_isos i ON fb.funding_id = i.funding_id AND i.is_referring = 0
WHERE NOT EXISTS (SELECT 1 FROM payments WHERE approved_flag = 1 AND amount = b.bonus_amount AND transaction_id = CONVERT(VARCHAR (50), b.iso_bonus_id))
	AND COALESCE(b.bonus_amount, 0) > 0


END

GO
