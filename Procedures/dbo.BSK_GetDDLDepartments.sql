SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2021-01-18
-- Description: Gets a list of departments for the dropdown
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetDDLDepartments]
AS
BEGIN
SET NOCOUNT ON

SELECT d.department_id, d.department_desc
FROM departments d
ORDER BY d.workflow_order

END
GO
