SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-07-16
-- Description:	Insert commissions into commissions table.  payable commissions
-- =============================================
CREATE PROCEDURE [dbo].[CSP_InsCommission]

AS
BEGIN	
SET NOCOUNT ON;
DECLARE @start_date DATE, @end_date DATE; SELECT @start_date = '2019-08-01'--, @end_date = '2019-08-30'

DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
BEGIN TRANSACTION

INSERT INTO commissions (payment_id, trans_id, trans_date, draft_date, trans_amount, commission_rate, commission_rate_id, commission_amount, collection_type, collector, funding_id,
	total_assigned, num_with_plans, collection_start_date, collection_end_date, payment_plan_created, payment_plan_status, age_in_collections, funding_payment_date_id, affiliate_bucket,
	target_rate_met, change_date, change_user)
SELECT y.payment_id, y.trans_id, y.trans_date, y.draft_date, y.trans_amount, 
	CASE WHEN y.total_assigned = 0 THEN 0 
		ELSE CASE WHEN y.num_with_plans / y.total_assigned >= y.target_percent THEN y.target_commission_rate ELSE y.rate END END AS commission_rate, y.commission_rate_id, 
	CASE WHEN y.total_assigned = 0 THEN 0 
		ELSE CONVERT(NUMERIC(10,2), 
			CASE WHEN y.num_with_plans / y.total_assigned >= y.target_percent THEN y.trans_amount * y.target_commission_rate ELSE y.trans_amount * y.rate END)
		END AS commission_amount,
	y.collection_type, y.assigned_collector, y.funding_id, y.total_assigned, y.num_with_plans, y.collection_start_date, y.collection_end_date, y.payment_plan_create_date, 
	y.payment_plan_status, y.age_in_collections, y.funding_payment_id, y.affiliate_bucket, 
	CASE 
		WHEN y.total_assigned = 0 THEN 0
		WHEN y.num_with_plans / y.total_assigned >= y.target_percent THEN 1 
		ELSE 0 
	END AS target_rate_met, GETUTCDATE(), 'SYSTEM'
FROM (
	-- this part will join up with the specific commission rate
	SELECT x.payment_id, x.trans_id, x.trans_date, x.draft_date, x.trans_amount, 
	COALESCE((SELECT COUNT(DISTINCT f.affiliate_id) 
			FROM funding_collections fcAll WITH (NOLOCK)
			INNER JOIN fundings f WITH (NOLOCK) ON fcAll.funding_id = f.id
			WHERE fcAll.collection_type = x.collection_type AND fcAll.assigned_collector = x.assigned_collector 
				-- AND x.trans_date BETWEEN x.collection_start_date AND COALESCE(x.collection_end_date, '2050-12-31') --- not sure where this came from, but I believe is just wrong
				AND x.trans_date BETWEEN fcAll.start_date AND COALESCE(fcAll.end_date, '2050-12-31')				
				AND COALESCE(cr0.collection_level, cr1.collection_level, 0) = COALESCE(x.affiliate_bucket, cr0.collection_level, cr1.collection_level, 0)
			)
		, 0) AS total_assigned,
	COALESCE((SELECT COUNT(DISTINCT pp.affiliate_id)
			--FROM funding_payment_schedules fps WITH (NOLOCK)
			--INNER JOIN payment_schedules ps WITH (NOLOCK) ON fps.payment_schedule_id = ps.payment_schedule_id
			FROM funding_payment_dates pd
			INNER JOIN payment_plans pp ON pd.payment_plan_id = pp.payment_plan_id
			WHERE pd.funding_id IN 
				(SELECT fcALL.funding_id FROM funding_collections fcAll WITH (NOLOCK) 
					WHERE fcAll.collection_type = x.collection_type AND fcAll.assigned_collector = x.assigned_collector 
				--AND x.trans_date BETWEEN x.collection_start_date AND COALESCE(x.collection_end_date, '2050-12-31')
				AND x.trans_date BETWEEN fcAll.start_date AND COALESCE(fcAll.end_date, '2050-12-31')
				AND COALESCE(cr0.collection_level, cr1.collection_level, 0) = COALESCE(x.affiliate_bucket, cr0.collection_level, cr1.collection_level, 0))), 0) AS num_with_plans,
	COALESCE(cr0.target_percent, cr1.target_percent) AS target_percent, COALESCE(cr0.target_commission_rate, cr1.target_commission_rate) AS target_commission_rate, 
	COALESCE(cr0.rate, cr1.rate) AS rate, COALESCE(cr0.commission_rate_id, cr1.commission_rate_id) AS commission_rate_id, x.collection_type, x.assigned_collector, x.funding_id, x.funding_name,
	x.collection_start_date, x.collection_end_date, x.payment_plan_create_date, x.payment_plan_status, x.age_in_collections, x.funding_payment_id, x.affiliate_bucket
	FROM (
		-- This part gets all transactions not yet tied to a commision that are tied to a funding in some sort of collections
		-- added DISTINCT because funding_collections are getting duplicated.  Will fix the fc issue but for now need DISTINCT
		SELECT DISTINCT t.trans_id, f.legal_name + ' - ' + f.contract_number AS funding_name, t.trans_date,  
			CASE t.trans_type_id WHEN 25 THEN -1.00 * t.trans_amount ELSE t.trans_amount END AS trans_amount,
			tt.trans_type, fc.collection_type, fc.start_date AS collection_start_date, fc.end_date AS collection_end_date, fc.assigned_collector, 
		--	c.comments AS change_comments, 
		    NULL AS change_comments, 
			--- r.reason AS change_reason, 
			NULL AS change_reason, pp.create_date AS payment_plan_create_date, 
			COALESCE(ppsc.performance_status, '') AS payment_plan_status, pd.payment_date AS scheduled_payment, 
			p.payment_id, pd.amount AS expected_amount, pd.rec_amount, pd.chargeback_amount, 
			DATEDIFF(DD, fc.start_date, 
			--  COALESCE(pp.create_date, ps.create_date, ps2.create_date, t.trans_date)) AS age_in_collections, -- no longer need payment schedules start date.  Buckets only get locked in when payment plan is created.
			COALESCE(pp.create_date, t.trans_date)) AS age_in_collections,
			COALESCE(pd.id, pd2.id) AS funding_payment_id, t.funding_id, 			
			f.merchant_id, f.affiliate_id, COALESCE(tDraft.trans_date, t.trans_date) AS draft_date, ab.collection_level AS affiliate_bucket
		FROM transactions t WITH (NOLOCK)
		LEFT JOIN transactions tDraft WITH (NOLOCK) ON t.transaction_id = tDraft.transaction_id AND tDraft.trans_type_id = 18
		INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
		INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
		INNER JOIN funding_collections fc WITH (NOLOCK) ON f.id = fc.funding_id
			AND t.trans_date BETWEEN fc.start_date AND COALESCE(fc.end_date, '2050-12-31')			
		LEFT JOIN payments p ON t.transaction_id = p.transaction_id
		--LEFT JOIN payment_schedules ps  WITH (NOLOCK) ON p.payment_schedule_id = ps.payment_schedule_id AND p.payment_schedule_id IS NOT NULL
		--LEFT JOIN payment_schedules ps2 WITH (NOLOCK) ON ps2.payment_schedule_id = (SELECT payment_schedule_id FROM payments p2 WHERE p2.transaction_id = p.orig_transaction_id)  -- gets us the original payment schedule for retries and fees\
		LEFT JOIN funding_payment_dates pd  WITH (NOLOCK) ON p.funding_payment_date_id = pd.id AND pd.id IS NOT NULL
		LEFT JOIN funding_payment_dates pd2 WITH (NOLOCK) ON pd2.id = (SELECT funding_payment_date_id FROM payments p2 WHERE p2.transaction_id = p.orig_transaction_id)  -- gets us the original payment date id for retries and fees
		--LEFT JOIN payment_schedule_changes c WITH (NOLOCK) ON COALESCE(ps.payment_schedule_id, ps2.payment_schedule_id) = c.payment_schedule_id
		--LEFT JOIN payment_schedule_change_reasons r WITH (NOLOCK) ON c.change_reason_id = r.id
		LEFT JOIN payment_plans pp WITH (NOLOCK) ON COALESCE(pd.payment_plan_id, pd2.payment_plan_id) = pp.payment_plan_id
		LEFT JOIN payment_plan_statuses pps WITH (NOLOCK) ON pp.payment_plan_id = pps.payment_plan_id 
			AND t.trans_date BETWEEN pps.start_date AND COALESCE(pps.end_date, '2050-12-31')
		LEFT JOIN payment_plan_status_codes ppsc WITH (NOLOCK) ON pps.payment_plan_status_code = ppsc.id
		--LEFT JOIN payment_schedule_payments psp WITH (NOLOCK) ON COALESCE(ps.payment_schedule_id, ps2.payment_schedule_id) = psp.payment_schedule_id AND psp.trans_id = t.trans_id
		--LEFT JOIN payment_sched_status_hist h WITH (NOLOCK) ON COALESCE(ps.payment_schedule_id, ps2.payment_schedule_id) = h.payment_sched_id 
			--AND t.trans_date BETWEEN h.start_date AND COALESCE(h.end_date, '2050-12-31')
		--LEFT JOIN payment_schedule_status ss WITH (NOLOCK) ON h.payment_sched_status_code = ss.id
		INNER JOIN transaction_types tt WITH (NOLOCK) ON t.trans_type_id = tt.trans_type_id
		LEFT JOIN affiliate_buckets ab WITH (NOLOCK) ON f.affiliate_id = ab.affiliate_id AND t.trans_date BETWEEN ab.start_date AND COALESCE(ab.end_date, '2050-12-31')
		WHERE (t.trans_type_id IN (2, 3, 4, 5, 14, 19, 25)  
			OR (t.trans_type_id = 21 AND EXISTS (SELECT 1 FROM transactions t2 WHERE t2.trans_type_id = 3 AND t2.transaction_id = t.transaction_id))
			)  
			-- AND COALESCE(tDraft.trans_date, t.trans_date) >= @start_date --AND @end_date -- don't care about draft date anymore
			AND t.trans_date >= @start_date AND t.redistributed = 0
			AND b.batch_status IN('Settled', 'Closed') AND b.batch_type NOT IN ('ContractRenewal')  -- these are wire payoffs.  Do not include them
			AND t.commission_id IS NULL
			AND NULLIF(fc.collection_type, '') IS NOT NULL

	) x
	LEFT JOIN commission_rates cr0 WITH (NOLOCK) ON x.collection_type = cr0.collection_type AND x.trans_date BETWEEN cr0.start_date AND COALESCE(cr0.end_date, '2050-12-31')
		AND (x.assigned_collector = cr0.collector OR x.funding_id = cr0.funding_id OR x.merchant_id = cr0.merchant_id OR x.affiliate_id = cr0.affiliate_id)
	LEFT JOIN commission_rates cr1 WITH (NOLOCK) ON x.collection_type = cr1.collection_type AND x.trans_date BETWEEN cr1.start_date AND COALESCE(cr1.end_date, '2050-12-31')
		--AND cr1.collection_level = CASE WHEN x.age_in_collections <= 30 THEN 1
		--							WHEN x.age_in_collections BETWEEN 31 AND 60 THEN 2
		--							WHEN x.age_in_collections BETWEEN 61 AND 90 THEN 3
		--							WHEN x.age_in_collections > 90 THEN 4
		--						END
		AND cr1.collection_level = x.affiliate_bucket
		AND COALESCE(x.assigned_collector, '') = COALESCE(cr1.collector, x.assigned_collector, '')
		AND x.funding_id = COALESCE(cr1.funding_id, x.funding_id)
		AND x.merchant_id = COALESCE(cr1.merchant_id, x.merchant_id)
		AND x.affiliate_id = COALESCE(cr1.affiliate_id, x.affiliate_id)
		--AND cr0.commission_rate_id IS NULL  -- if cr0 was found, don't want another record (duplicates)
) y
ORDER BY y.funding_name, y.trans_date


SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


-- now update the commission_id on the transactions

UPDATE t
SET commission_id = c.commission_id, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM transactions t
INNER JOIN commissions c ON t.trans_id = c.trans_id
WHERE t.commission_id IS NULL

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


COMMIT TRANSACTION
GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)

Done:

END
GO
