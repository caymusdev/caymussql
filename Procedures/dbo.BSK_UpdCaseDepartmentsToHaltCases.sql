SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Rick Pina
-- Create date: 2021-1-19
-- Description:	Updates one or many department cases to be set to do_not_move
-- =============================================
CREATE PROCEDURE [dbo].[BSK_UpdCaseDepartmentsToHaltCases]
(
	@case_department_ids					NVARCHAR(4000),
	@day_amount						INT,
	@do_not_move_by					NVARCHAR(100),
	@change_user					NVARCHAR(100)

)
AS
BEGIN
SET NOCOUNT ON;

UPDATE case_departments
SET do_not_move = GETUTCDATE(), do_not_move_by = @do_not_move_by,
	do_not_move_until = CONVERT(DATE, DATEADD(day, (@day_amount % 5) + 
		CASE WHEN ((@@DATEFIRST + DATEPART(weekday, GETUTCDATE())) % 7 + (@day_amount % 5)) > 6 THEN 2 ELSE 0 END, DATEADD(week, (@day_amount / 5), GETUTCDATE()))),
	change_date = GETUTCDATE(), change_user = @change_user
FROM case_departments
INNER JOIN string_split(@case_department_ids, '|') a ON a.value = case_department_id 
WHERE case_department_id = a.value


END

GO
