SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-27
-- Description:	updates a notification record
-- =============================================
CREATE PROCEDURE [dbo].[BSK_UpdNotification]
(
	@notification_id			INT,
	@user_email					NVARCHAR (100), 
	@displayed_time				DATETIME,
	@acted_on_time				DATETIME
)
AS
BEGIN
SET NOCOUNT ON;

UPDATE notifications
SET displayed_time = COALESCE(@displayed_time, displayed_time, @acted_on_time), -- if displayed time is passed in use that otherwise leave as what is in db, unless it is null
	-- and in that case set it to acted on time passed in if it is not null
	acted_on_time = COALESCE(@acted_on_time, acted_on_time),
	change_date = GETUTCDATE(), change_user = @user_email
WHERE notification_id = @notification_id

END

GO
