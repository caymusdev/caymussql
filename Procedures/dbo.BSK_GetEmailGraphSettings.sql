SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-11-03
-- Description:	Retrieves all settings related to sending an email via Microsoft Graph
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetEmailGraphSettings]
(
	@affiliate_id				INT = NULL
)
AS
BEGIN
SET NOCOUNT ON;

IF @affiliate_id IS NULL
	BEGIN
		SELECT dbo.BF_GetSetting('GraphTenantID') AS GraphTenantID, dbo.BF_GetSetting('GraphClientID') AS GraphClientID, dbo.BF_GetSetting('GraphClientSecret') AS GraphClientSecret, 
			dbo.BF_GetSetting('GraphSenderEmail') AS GraphSenderEmail, dbo.BF_GetSetting('GraphEmailLastChecked') AS GraphEmailLastChecked, 
			dbo.BF_GetSetting('CheckEmailInterval') AS CheckEmailInterval
	END
ELSE
	BEGIN
		DECLARE @department VARCHAR (50), @setting_extra VARCHAR (50)
		SELECT @department = d.department
		FROM cases c
		INNER JOIN departments d ON c.department_id = d.department_id
		WHERE c.affiliate_id = @affiliate_id AND c.active = 1

		SELECT @setting_extra = CASE WHEN @department = 'CustomerService' THEN 'CS' WHEN @department LIKE 'Collections%' THEN 'Col' WHEN @department = 'Legal' THEN 'Legal' ELSE 'CS' END

		SELECT dbo.BF_GetSetting('GraphTenantID') AS GraphTenantID, dbo.BF_GetSetting('GraphClientID') AS GraphClientID, dbo.BF_GetSetting('GraphClientSecret') AS GraphClientSecret, 
			dbo.BF_GetSetting('GraphSenderEmail' + @setting_extra) AS GraphSenderEmail, dbo.BF_GetSetting('GraphEmailLastChecked') AS GraphEmailLastChecked, 
			dbo.BF_GetSetting('CheckEmailInterval') AS CheckEmailInterval
	END
	
END


GO
