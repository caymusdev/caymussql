SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-06-28
-- Description:	Accepts a delimited list of funding ids and what processor to set them too.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_SaveProcessors]
(	
	@ids			NVARCHAR (4000), -- could be a delimited list of payment ids
	@processors		NVARCHAR (MAX), -- delimited list of procesors
	@dates			NVARCHAR (MAX), -- delimited list of funding dates,
	@firstDates		NVARCHAR (MAX), -- delimited list of first payment dates,
	@userid			NVARCHAR (100),
	@contract_types	NVARCHAR (MAX) -- delimited list of contract types (new/renewal)
)
AS
BEGIN	
SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#tempIDs') IS NOT NULL DROP TABLE #tempIDs
CREATE TABLE #tempIDs (id int not null identity, contract_id INT)

IF OBJECT_ID('tempdb..#tempProcs') IS NOT NULL DROP TABLE #tempProcs
CREATE TABLE #tempProcs (id int not null identity, processor VARCHAR(50))

IF OBJECT_ID('tempdb..#tempDates') IS NOT NULL DROP TABLE #tempDates
CREATE TABLE #tempDates (id int not null identity, funded_date VARCHAR(50))

IF OBJECT_ID('tempdb..#tempFirstDates') IS NOT NULL DROP TABLE #tempFirstDates
CREATE TABLE #tempFirstDates (id int not null identity, first_payment_date VARCHAR(50))

IF OBJECT_ID('tempdb..#tempContractTypes') IS NOT NULL DROP TABLE #tempContractTypes
CREATE TABLE #tempContractTypes (id int not null identity, contract_type VARCHAR(50), orig_contract_type VARCHAR (50) NULL)

INSERT INTO #tempIDs(contract_id)
SELECT a.value
FROM string_split(@ids, '|') a

INSERT INTO #tempProcs(processor)
SELECT a.value
FROM string_split(@processors, '|') a

INSERT INTO #tempDates(funded_date)
SELECT a.value
FROM string_split(@dates, '|') a

INSERT INTO #tempFirstDates(first_payment_date)
SELECT a.value
FROM string_split(@firstDates, '|') a

INSERT INTO #tempContractTypes(contract_type)
SELECT a.value
FROM string_split(@contract_types, '|') a


-- now update the original contract type so we can see if it changed
UPDATE c
SET orig_contract_type = f.contract_type
FROM #tempContractTypes c
INNER JOIN #tempIDs t ON c.id = t.id
INNER JOIN fundings f ON t.contract_id = f.id


UPDATE fundings 
SET processor = p.processor, funded_date = d.funded_date, first_payment_date = d2.first_payment_date, change_user = @userid,
	change_date = GETUTCDATE(), contract_type = c.contract_type 
FROM fundings f
INNER JOIN #tempIDs i ON f.id = i.contract_id
INNER JOIN #tempProcs p ON i.id = p.id
INNER JOIN #tempDates d ON i.id = d.id
INNER JOIN #tempFirstDates d2 ON i.id = d2.id
INNER JOIN #tempContractTypes c ON i.id = c.id
WHERE f.processor IS NULL   -- prevent user error on fundings queue of setting a funding that is already funded

--------------------- NOW DO PAYMENT DATES -----------------------------
DECLARE @tempId INT, @maxID INT, @tempFundingID INT
SELECT @tempId = 1, @maxID = MAX(id) FROM #tempIDs

WHILE @tempId <= @maxID
	BEGIN
		SELECT @tempFundingID = contract_id FROM #tempIDs WHERE id = @tempId
		
		EXEC dbo.CSP_InsFundingPaymentDatesForFunding @tempFundingID, @userid

		-- now want to check if they changed it from New to Renewal, if so we need to call Renewal code
		DECLARE @orig_contract_type VARCHAR (50), @new_contract_type VARCHAR (50)
		SELECT @orig_contract_type = orig_contract_type, @new_contract_type = contract_type 
		FROM #tempContractTypes 
		WHERE Id = @tempId

		IF LOWER(@orig_contract_type) != LOWER(@new_contract_type) AND LOWER(@new_contract_type) = 'renewal'
			BEGIN
				EXEC dbo.CSP_RenewContract @tempFundingID
			END

		SET @tempId = @tempId + 1
	END

----------------------- END PAYMENT DATES

IF OBJECT_ID('tempdb..#tempIDs') IS NOT NULL DROP TABLE #tempIDs
IF OBJECT_ID('tempdb..#tempProcs') IS NOT NULL DROP TABLE #tempProcs
IF OBJECT_ID('tempdb..#tempDates') IS NOT NULL DROP TABLE #tempDates
IF OBJECT_ID('tempdb..#tempFirstDates') IS NOT NULL DROP TABLE #tempFirstDates
END
GO
