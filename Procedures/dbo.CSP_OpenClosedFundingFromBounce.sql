SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Ryan Brown
-- Create date: 2022-11-16
-- Description:	Reopens a closed contract that had something bounce afer being closed
-- =============================================
CREATE PROCEDURE [dbo].[CSP_OpenClosedFundingFromBounce] 
(
	@funding_id			INT
)
AS
BEGIN
SET NOCOUNT ON;

-- make sure to get the correct "today's" date since this runs in UTC
DECLARE @today DATE; SELECT @today = CONVERT(DATE, DATEADD(HOUR, -5, GETUTCDATE()))
DECLARE @tomorrow DATE; SELECT @tomorrow = dbo.CF_GetTomorrow(@today)


UPDATE fundings
SET contract_status = 1, final_payment_date = NULL, completed_date = NULL, change_date = GETUTCDATE(), change_user = 'REOPEN'
WHERE id = @funding_id


EXEC dbo.CSP_RedoCalculationsForFunding @funding_id, 'REOPEN'


END
GO
