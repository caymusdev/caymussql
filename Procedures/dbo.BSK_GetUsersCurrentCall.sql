SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:     Ryan Brown
-- Create Date: 2021-04-15
-- Description: Returns Call ID for the call a user is currently on
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetUsersCurrentCall]
(
	@users_email			NVARCHAR (100)
)
AS

SET NOCOUNT ON


SELECT TOP 1 WITH TIES x.email, x.call_id, x.create_date
FROM (
	SELECT u.email, COALESCE(NULLIF(l.conversation_id, ''), l.call_id) AS call_id, l.create_date
	FROM phone_logs l
	LEFT JOIN user_phones p ON l.call_to = p.phone_number
	LEFT JOIN users u ON p.user_id = u.user_id
	WHERE l.status = 'in-progress' AND l.source = 'TwilioEvents' AND l.raw_response LIKE '%ForwardedFrom%'
	UNION
	SELECT l.users_email, COALESCE(NULLIF(l.conversation_id, ''), l.call_id) , l.create_date
	FROM phone_logs l
	LEFT JOIN user_phones p ON l.call_to = p.phone_number
	LEFT JOIN users u ON p.user_id = u.user_id
	WHERE l.source = 'MakeCall' 
) x 
WHERE x.email = @users_email
ORDER BY CASE WHEN ROW_NUMBER() OVER(PARTITION BY x.email ORDER BY x.create_date DESC) <= 1 then 0 else 1 end



GO
