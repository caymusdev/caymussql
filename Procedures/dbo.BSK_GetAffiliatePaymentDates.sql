SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-12-09
-- Description:	Gets the payments dates for a specific affiliate and date range
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetAffiliatePaymentDates]
(
	@affiliate_id			INT,
	@start_date				DATETIME = NULL, 
	@end_date				DATETIME = NULL
)
AS
BEGIN	
SET NOCOUNT ON;


SELECT p.scheduled_payment_id, p.payment_method, p.payment_date, p.payment_amount, m.merchant_name, p.bank_name, p.routing_number, p.account_number, 
	p.affiliate_id, COALESCE(m.merchant_name, c.contract_number) AS merchant_name
FROM scheduled_payments p
LEFT JOIN merchants m ON p.merchant_id = m.merchant_id
LEFT JOIN contracts c ON p.contract_id = c.contract_id
WHERE (p.payment_date >= @start_date OR @start_date IS NULL)
	AND (p.payment_date <= @end_date OR @end_date IS NULL)
	AND p.affiliate_id = @affiliate_id
ORDER BY p.payment_date



END

GO
