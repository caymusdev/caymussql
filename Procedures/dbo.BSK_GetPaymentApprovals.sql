SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:     Ryan Brown
-- Create Date: 2021-03-24
-- Description: Returns any approvals not yet approved
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetPaymentApprovals]

AS

SET NOCOUNT ON

SELECT DISTINCT u.email
FROM approvals a WITH (NOLOCK)
INNER JOIN approval_types aa WITH (NOLOCK) ON a.approval_type_id = aa.approval_type_id
INNER JOIN approval_users au WITH (NOLOCK) ON aa.approval_type_id = au.approval_type_id
INNER JOIN users u WITH (NOLOCK) ON au.user_id = u.user_id
WHERE aa.approval_type IN ('PaymentChanges', 'PaymentPlanChanges', 'PaymentDelete', 'PaymentPlanDelete')


GO
