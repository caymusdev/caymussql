SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-08-02
-- Description:	Updates the status of payment schedules
-- =============================================
CREATE PROC [dbo].[CSP_UpdatePaymentScheduleStatuses]
AS
BEGIN

SET NOCOUNT ON;

-- VARS
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @yesterday DATE; SELECT @yesterday = DATEADD(DD, CASE DATEPART(WEEKDAY, @today) WHEN 1 THEN -2 WHEN 2 THEN -3 ELSE -1 END, @today)
DECLARE @pending_id INT; SELECT @pending_id = id FROM payment_schedule_status WHERE code = 'Pending'
DECLARE @complete_id INT; SELECT @complete_id = id FROM payment_schedule_status WHERE code = 'Complete'
DECLARE @in_progress_id INT; SELECT @in_progress_id = id FROM payment_schedule_status WHERE code = 'InProgress'
DECLARE @broken_id INT; SELECT @broken_id = id FROM payment_schedule_status WHERE code = 'Broken'
DECLARE @broken_payment_id INT; SELECT @broken_payment_id = id FROM payment_schedule_status WHERE code = 'BrokenPayment'


-- END VARS

-- temp table / work table
IF OBJECT_ID('tempdb..#tempIDs2') IS NOT NULL DROP TABLE #tempIDs2
CREATE TABLE #tempIDs2 (id int not null identity, payment_schedule_id INT)

IF OBJECT_ID('tempdb..#tempScheds2') IS NOT NULL DROP TABLE #tempScheds2
CREATE TABLE #tempScheds2 (id int not null identity, payment_schedule_id INT, sched_status INT NULL, orderby INT NULL, original BIT NULL)
-- end temp table

-- put into temp table all payment schedules that DO NOT HAVE any status records.  Shouldn't be any, but just in case.
INSERT INTO #tempIDs2 (payment_schedule_id)
SELECT ps.payment_schedule_id
FROM payment_schedules ps WITH (NOLOCK)
WHERE NOT EXISTS (SELECT 1 FROM payment_sched_status_hist s WHERE s.payment_sched_id = ps.payment_schedule_id AND s.payment_sched_status_code = @pending_id)

-- First make sure there is a Pending record for each payment_schdedule
INSERT INTO payment_sched_status_hist(payment_sched_id, payment_sched_status_code, start_date, end_date, change_date, change_user)
SELECT ps.payment_schedule_id, @pending_id, CONVERT(DATE, ps.create_date), NULL, GETUTCDATE(), 'SYSTEM'
FROM payment_schedules ps WITH (NOLOCK)
INNER JOIN #tempIDs2 t ON ps.payment_schedule_id = t.payment_schedule_id

-- make sure current status id is now set to pending
UPDATE ps
SET current_status = @pending_id
FROM payment_schedules ps WITH (NOLOCK)
INNER JOIN #tempIDs2 t ON ps.payment_schedule_id = t.payment_schedule_id
WHERE ps.current_status IS NULL

-- clear out work table
TRUNCATE TABLE #tempIDs2


-- Secondly, set to Complete all those that have a complete funding now or that the end date has passed
INSERT INTO #tempIDs2 (payment_schedule_id)
SELECT ps.payment_schedule_id
FROM payment_schedules ps WITH (NOLOCK)
INNER JOIN funding_payment_schedules fps WITH (NOLOCK) ON  ps.payment_schedule_id = fps.payment_schedule_id
INNER JOIN fundings f ON fps.funding_id = f.id
WHERE ps.current_status <> @complete_id 
	AND (f.contract_status = 2 OR COALESCE(ps.end_date, '2050-12-31') < @today)

INSERT INTO payment_sched_status_hist (payment_sched_id, payment_sched_status_code, start_date, end_date, change_date, change_user)
SELECT t.payment_schedule_id, @complete_id, @today, NULL, GETUTCDATE(), 'SYSTEM'
FROM #tempIDs2 t

-- now update payment sched current status
UPDATE payment_schedules 
SET current_status = @complete_id
WHERE payment_schedule_id IN (SELECT payment_schedule_id FROM #tempIDs2)

-- now set end date to yesterday on all history ones that have no end date
UPDATE payment_sched_status_hist
SET end_date = @yesterday, change_date = GETUTCDATE(), change_user = 'SYSTEM'
WHERE payment_sched_id IN (SELECT payment_schedule_id FROM #tempIDs2) AND end_date IS NULL AND payment_sched_status_code <> @complete_id

-- clear out work table
TRUNCATE TABLE #tempIDs2




----**********************************************************--------------------------
----**********************************************************--------------------------
----**********************************************************--------------------------
-- Now, load up temp table with original values.  Will be used at the end to compare to and update statuses
INSERT INTO #tempScheds2(payment_schedule_id, sched_status, orderby, original)
SELECT ps.payment_schedule_id, ps.current_status, 0, 1
FROM payment_schedules ps WITH (NOLOCK)
WHERE ps.current_status NOT IN (@complete_id)

-- want to keep a separate list of just the ids we are dealing with
INSERT INTO #tempIDs2 (payment_schedule_id)
SELECT DISTINCT payment_schedule_id
FROM #tempScheds2



-- now insert in progress status for those that have any payment
INSERT INTO #tempScheds2(payment_schedule_id, sched_status, orderby, original)
SELECT DISTINCT i.payment_schedule_id, @in_progress_id, 1, 0
FROM #tempIDs2 i
INNER JOIN payment_schedules ps WITH (NOLOCK) ON i.payment_schedule_id = ps.payment_schedule_id
INNER JOIN payment_schedule_payments psp WITH (NOLOCK) ON ps.payment_schedule_id = psp.payment_schedule_id AND COALESCE(psp.rec_amount, 0) > 0

-- now check for any missed payments
INSERT INTO #tempScheds2(payment_schedule_id, sched_status, orderby, original)
SELECT DISTINCT i.payment_schedule_id, @broken_payment_id, 2, 0
FROM #tempIDs2 i
INNER JOIN payment_schedules ps WITH (NOLOCK) ON i.payment_schedule_id = ps.payment_schedule_id
INNER JOIN payment_schedule_payments psp WITH (NOLOCK) ON ps.payment_schedule_id = psp.payment_schedule_id AND (COALESCE(psp.rec_amount, 0) = 0 OR COALESCE(psp.chargeback_amount, 0) <> 0)


-- now check for broken payment
INSERT INTO #tempScheds2(payment_schedule_id, sched_status, orderby, original)
SELECT DISTINCT i.payment_schedule_id, @broken_id, 3, 0
FROM #tempIDs2 i
INNER JOIN payment_schedules ps WITH (NOLOCK) ON i.payment_schedule_id = ps.payment_schedule_id
INNER JOIN payment_schedule_payments psp WITH (NOLOCK) ON ps.payment_schedule_id = psp.payment_schedule_id AND (COALESCE(psp.rec_amount, 0) = 0 OR COALESCE(psp.chargeback_amount, 0) <> 0)
WHERE DATEDIFF(DD, COALESCE(ps.broken_payment_date, psp.payment_date), psp.payment_date) > 7


--************************************************************ Find last and set to new status

-- first set end date to yesterday on all history ones that have no end date because we're about to enter some with no end date.  Do this first.
UPDATE sh
SET end_date = @yesterday, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM payment_sched_status_hist sh
INNER JOIN #tempScheds2 t ON sh.payment_sched_id = t.payment_schedule_id
INNER JOIN (
	SELECT ROW_NUMBER() OVER (PARTITION BY t.payment_schedule_id ORDER BY t.orderby DESC) AS RowNumber, t.id, t.payment_schedule_id, t.sched_status, t.original
	FROM #tempScheds2 t
	WHERE t.original = 0
) x  ON t.payment_schedule_id = x.payment_schedule_id AND x.RowNumber = 1
WHERE t.original = 1 AND t.sched_status <> x.sched_status AND sh.end_date IS NULL 



-- now get the last one, if there is one (ordered by orderby DESC) and that will be the new status
INSERT INTO payment_sched_status_hist(payment_sched_id, payment_sched_status_code, start_date, end_date, change_date, change_user)
SELECT t.payment_schedule_id, x.sched_status, @today, NULL, GETUTCDATE(), 'SYSTEM'
FROM #tempScheds2 t
INNER JOIN (
	SELECT ROW_NUMBER() OVER (PARTITION BY t.payment_schedule_id ORDER BY t.orderby DESC) AS RowNumber, t.id, t.payment_schedule_id, t.sched_status, t.original
	FROM #tempScheds2 t
	WHERE t.original = 0
) x  ON t.payment_schedule_id = x.payment_schedule_id AND x.RowNumber = 1
WHERE t.original = 1 AND t.sched_status <> x.sched_status


-- now update payment scheds current status
UPDATE ps
SET current_status = x.sched_status
FROM payment_schedules ps
INNER JOIN #tempScheds2 t ON ps.payment_schedule_id = t.payment_schedule_id
INNER JOIN (
	SELECT ROW_NUMBER() OVER (PARTITION BY t.payment_schedule_id ORDER BY t.orderby DESC) AS RowNumber, t.id, t.payment_schedule_id, t.sched_status, t.original
	FROM #tempScheds2 t
	WHERE t.original = 0
) x  ON t.payment_schedule_id = x.payment_schedule_id AND x.RowNumber = 1
WHERE t.original = 1 AND t.sched_status <> x.sched_status






END
GO
