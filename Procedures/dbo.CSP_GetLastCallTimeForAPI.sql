SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-04-26
-- Description:	This returns the last time a particular api call was made so that the caller can determine if a new call should be made
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetLastCallTimeForAPI]
(
	@api_call		NVARCHAR (100)
)
AS
BEGIN
	
SET NOCOUNT ON;

DECLARE @api_date DATE, @batch_date DATE

IF @api_call LIKE 'Forte%'
	BEGIN 
		SELECT COALESCE(MAX(create_date), dbo.CF_GetSetting('ForteSettlementsAsOf')) FROM api_logs WITH (NOLOCK) WHERE api_call = @api_call
	END
ELSE IF @api_call LIKE 'EVO%'
	BEGIN
		SELECT COALESCE(MAX(create_date), dbo.CF_GetSetting('EVOStartDate')) FROM api_logs WITH (NOLOCK) WHERE api_call = @api_call
	END
ELSE IF @api_call LIKE 'IPS%'
	BEGIN
		--SELECT COALESCE(MAX(create_date), dbo.CF_GetSetting('IPSStartDate')) FROM api_logs WITH (NOLOCK) WHERE api_call = @api_call
		SELECT @api_date = COALESCE(MAX(create_date), dbo.CF_GetSetting('IPSStartDate')) FROM api_logs WITH (NOLOCK) WHERE api_call = @api_call
		SELECT @batch_date = COALESCE(MAX(batch_date), dbo.CF_GetSetting('IPSStartDate')) FROM batches WITH (NOLOCK) WHERE batch_type = 'IPS Upload'
		IF DATEDIFF(D, @batch_date, @api_date) < 5 -- this means that the most recent batch is less than 5 days ago (from api)
			BEGIN
				SELECT @batch_date
			END
		ELSE
			BEGIN
				SELECT @api_date
			END
	END
ELSE IF @api_call LIKE 'Collect%'
	BEGIN
		SELECT COALESCE(MAX(create_date), '2019-01-01') FROM api_logs WITH (NOLOCK) WHERE api_call = @api_call
	END
ELSE 
	BEGIN
		SELECT COALESCE(MAX(create_date), '2019-01-01') FROM api_logs WITH (NOLOCK) WHERE api_call = @api_call
	END
END
GO
