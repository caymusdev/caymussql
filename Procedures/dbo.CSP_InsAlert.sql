SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-02-11
-- Description:	After receiving an ACK file, verify that what we sent matches what they ACK
-- Will have to return the batch id since it is not provided in the ack file
-- =============================================
CREATE PROCEDURE [CSP_InsAlert]
(
	@alert_type			VARCHAR (50),
	@alert_type_2		VARCHAR (50),
	@record_id			BIGINT,
	@alert_message		NVARCHAR (2000)
)
AS
BEGIN
SET NOCOUNT ON;

INSERT INTO alerts (alert_type, alert_type_2, record_id, alert_message, deleted)
SELECT @alert_type, @alert_type_2, @record_id, @alert_message, 0

END
GO
