SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Rick Pina
-- Create Date: 2021-03-09
-- Description: Gets the next Collector based on time they have been available and what department they are in. 
-- Excludes the user_id of the last person contacted.
-- =============================================
CREATE PROCEDURE [dbo].[BSK_HuntGroupNextCollector]
(
	@collectors_email			NVARCHAR (100),
	@affiliate_id				INT
)
AS
BEGIN
SET NOCOUNT ON
DECLARE @dept_id INT

/*
SELECT @dept_id = c.department_id  
FROM users u WITH (NOLOCK)
INNER JOIN collector_cases cc WITH (NOLOCK) ON u.user_id = cc.collector_user_id
INNER JOIN cases c WITH (NOLOCK) ON cc.case_id = c.case_id AND c.affiliate_id = @affiliate_id
WHERE u.email = @collectors_email AND cc.end_date IS NULL AND u.active = 1
*/
-- Just need to get the department id of current active case of affiliate, or last case that is no longer active
SELECT TOP 1 @dept_id = c.department_id
FROM cases c WITH (NOLOCK) 
WHERE c.affiliate_id = @affiliate_id
ORDER BY c.active DESC

IF @dept_id IS NULL
	BEGIN
		SELECT @dept_id = department_id FROM departments WHERE department = 'CustomerService'
	END


SELECT TOP 1 up.phone_number, u.email
FROM users u WITH (NOLOCK)
INNER JOIN user_statuses us WITH (NOLOCK) ON u.user_id = us.user_id AND us.end_date IS NULL
INNER JOIN user_presence_statuses ups WITH (NOLOCK) ON u.presence_status = ups.user_presence_status_id
INNER JOIN user_phones up WITH (NOLOCK) ON u.user_id = up.user_id
INNER JOIN user_departments ud WITH (NOLOCK) ON u.user_id = ud.user_id 
WHERE ups.status_name IN ('Available') AND up.is_primary = 1 AND u.email != @collectors_email AND u.active = 1
ORDER BY CASE WHEN ud.department_id < @dept_id THEN 99 ELSE ud.department_id END, DATEDIFF(s, us.start_date, GETUTCDATE()) DESC

END
GO
