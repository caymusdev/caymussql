SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Rick Pina
-- Create Date: 2021-1-04
-- Description: Deletes a Note from the Affiliates Page
-- =============================================
CREATE PROCEDURE [dbo].[BSK_DelNote]
(
	@note_id			INT,
	@change_user		NVARCHAR (100)
)
AS
BEGIN
    SET NOCOUNT ON
		BEGIN

		DECLARE @create_user NVARCHAR (100)
		DECLARE @is_management BIT
		
	    SELECT @is_management = dbo.BF_DoesUserHaveManagementRole(@change_user)

	    SELECT @create_user = create_user FROM notes WHERE notes_id = @note_id
	    IF @create_user = @change_user  OR @is_management = 1 

		BEGIN
		-- for audit trigger
		UPDATE notes SET change_date = GETUTCDATE(), change_user = GETUTCDATE() WHERE notes_id = @note_id

			
		DELETE FROM notes WHERE notes_id = @note_id
		END	
		 
								
		END

END
GO
