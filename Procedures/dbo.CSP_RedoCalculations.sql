SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-11-15
-- Description:	THIS WILL REDO THE CALCULATIONS ON ALL CLOSED FUNDINGS.  BE CAUTIOUS RUNNING THIS!!!!!!!!!!!!
-- =============================================
CREATE PROCEDURE [dbo].[CSP_RedoCalculations]
(
	@change_user			NVARCHAR (100) = 'SYSTEM'
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
BEGIN TRANSACTION

DECLARE @today DATE; SET @today = CONVERT(DATE, DATEADD(HH, -5, GETUTCDATE()))
DECLARE @timeStamp DATETIME; SET @timeStamp = GETUTCDATE()

-- temp table to store all transactions that have not been used in calculations yet
IF OBJECT_ID('tempdb..#tempTrans') IS NOT NULL DROP TABLE #tempTrans
CREATE TABLE #tempTrans (id int not null identity, funding_id BIGINT, trans_type_id INT, trans_amount MONEY, used BIT, trans_id INT, batch_id INT, trans_date DATE)

-- this temp table will store transactions grouped by funding and transaction type.  Some of the formulas are simple sums which will use this table.
IF OBJECT_ID('tempdb..#tempTransGrouped') IS NOT NULL DROP TABLE #tempTransGrouped
CREATE TABLE #tempTransGrouped (id int not null identity, funding_id BIGINT, trans_type_id INT, trans_amount MONEY)

-- keep one record per funding with all the fields so that at the end, we can do one update on the fundings table of all these calculated fields.
IF OBJECT_ID('tempdb..#tempFundings') IS NOT NULL DROP TABLE #tempFundings
CREATE TABLE #tempFundings (id int not null identity, funding_id BIGINT, cash_fees MONEY, counter_deposit_revenue MONEY, ach_received MONEY, cc_revenue MONEY, 
	wires_revenue MONEY, fees_applied MONEY, settlement_received MONEY, writeoff_received MONEY, cash_to_chargeback MONEY, cash_to_bad_debt MONEY,
	cash_to_reapplication MONEY, cash_from_reapplication MONEY, writeoff_adjustment_received MONEY, ach_draft MONEY, checks_revenue MONEY, ach_reject MONEY,
	refund MONEY, refund_processed MONEY, refund_reapplied MONEY, refund_returned MONEY, continuous_pull MONEY, cash_to_pp MONEY, cash_to_margin MONEY, 
	wires_revenue_no_renewal MONEY, ach_revenue MONEY, pp_adjustment MONEY, margin_adjustment MONEY, pp_applied MONEY, margin_applied MONEY, cash_to_rtr MONEY, rtr_adjustment MONEY, 
	discount_received MONEY, gross_revenue MONEY, applied_amount MONEY, gross_received MONEY, gross_dollars_rec MONEY, rtr_applied MONEY, net_other_income MONEY, total_portfolio_adjustment MONEY, 
	percent_paid NUMERIC (10, 2), rtr_out MONEY, pp_out MONEY, margin_out MONEY, fees_out MONEY, net_revenue MONEY, workdays_since_funded INT, funded_date DATE, payoff_amount MONEY, 
	total_fees_collected MONEY, orig_fee MONEY, paid_off_amount MONEY, portfolio_value MONEY, purchase_price MONEY, unearned_income MONEY, pp_settlement MONEY, margin_settlement MONEY)

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

-- first insert into the temp table all transactions that are part of settled/closed batches but have not been used in calculations yet
INSERT INTO #tempTrans (funding_id, trans_type_id, trans_amount, used, trans_id, batch_id, trans_date)
SELECT t.funding_id, t.trans_type_id, t.trans_amount, 0, t.trans_id, b.batch_id, t.trans_date
FROM transactions t WITH (NOLOCK)
LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
	AND (f.contract_status = 2 OR (f.contract_status = 4 AND contract_substatus_id = 2)) -- only doing closed fundings in this REDOCalcs proc

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

-- store transactions also grouped by funding and trans type.  some formulas are simpler and can use this data.
INSERT INTO #tempTransGrouped (funding_id, trans_type_id, trans_amount)
SELECT t.funding_id, t.trans_type_id, SUM(t.trans_amount)
FROM #tempTrans t
GROUP BY t.funding_id, t.trans_type_id
ORDER BY t.funding_id, t.trans_type_id

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

-- one record per funding that is on the transactions we are dealing with
INSERT INTO #tempFundings (funding_id)
SELECT DISTINCT t.funding_id
FROM #tempTrans t

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

-- Update a few fields. Pull in a few static fields from the funding that is used in calculations.
UPDATE t
SET funded_date = f.funded_date, orig_fee = f.orig_fee, portfolio_value = f.portfolio_value, purchase_price = f.purchase_price, unearned_income = f.unearned_income
FROM #tempFundings t
INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id


-- First, do the easy ones that can be pulled from the group by table.  Because they are MAIN transaction types.  Should be one record per funding/trans type combo
UPDATE f
SET counter_deposit_revenue = x.counter_deposit_revenue, ach_received = x.ach_received, cc_revenue = x.cc_revenue, wires_revenue = x.wires_revenue,-- fees_applied = x.fees_applied,
	--settlement_received = x.settlement_received, 
	writeoff_received = x.writeoff_received, cash_to_chargeback = ABS(x.cash_to_chargeback), writeoff_adjustment_received = x.writeoff_adjustment_received,
	ach_draft = x.ach_draft, checks_revenue = x.checks_revenue, ach_reject = ABS(x.ach_reject), refund = ABS(x.refund), refund_processed = x.refund_processed, refund_reapplied = x.refund_reapplied,
	refund_returned = x.refund_returned, continuous_pull = x.continuous_pull, discount_received = x.discount_received 
FROM #tempFundings f WITH (NOLOCK)
INNER JOIN (
	SELECT DISTINCT funding_id,  
		(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 2 AND funding_id = t.funding_id) AS counter_deposit_revenue,
		(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 3 AND funding_id = t.funding_id) AS ach_received,
		(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 4 AND funding_id = t.funding_id) AS cc_revenue,
		(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 5 AND funding_id = t.funding_id) AS wires_revenue,
		(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 8 AND funding_id = t.funding_id) AS discount_received,
		--(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 9 AND funding_id = t.funding_id) AS fees_applied,
		--(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 10 AND funding_id = t.funding_id) AS settlement_received,
		(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 11 AND funding_id = t.funding_id) AS writeoff_received,
		(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 14 AND funding_id = t.funding_id) AS cash_to_chargeback,
		(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 17 AND funding_id = t.funding_id) AS writeoff_adjustment_received,
		(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 18 AND funding_id = t.funding_id) AS ach_draft,
		(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 19 AND funding_id = t.funding_id) AS checks_revenue,
		(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 21 AND funding_id = t.funding_id) AS ach_reject,
		(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 22 AND funding_id = t.funding_id) AS refund,
		(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 23 AND funding_id = t.funding_id) AS refund_processed,
		(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 24 AND funding_id = t.funding_id) AS refund_reapplied,
		(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 25 AND funding_id = t.funding_id) AS refund_returned,
		(SELECT SUM(trans_amount) FROM #tempTransGrouped WHERE trans_type_id = 26 AND funding_id = t.funding_id) AS continuous_pull
	FROM #tempTransGrouped t	
)  x ON f.funding_id = x.funding_id


UPDATE f
SET cash_to_pp =  COALESCE(cash_to_pp.cash_to_pp, 0), 
	cash_to_margin = COALESCE(cash_to_margin.cash_to_margin, 0),
	wires_revenue_no_renewal = COALESCE(wires_no_renewal.wires_no_renewal, 0),
	cash_to_bad_debt = COALESCE(cash_to_bad_debt.cash_to_bad_debt, 0),
	cash_to_reapplication = ABS(COALESCE(cash_to_reapplication.cash_to_reapplication, 0)),
	cash_from_reapplication = COALESCE(cash_from_reapplication.cash_from_reapplication, 0),
	ach_revenue = COALESCE(ach_revenue.ach_revenue, 0), 
	pp_settlement = COALESCE(pp_settlement.pp_settlement, 0),
	margin_settlement = COALESCE(margin_settlement.margin_settlement, 0),
	cash_fees = COALESCE(cash_fees.cash_fees, 0),
	fees_applied = COALESCE(fees_applied.fees_applied, 0),
	settlement_received = COALESCE(settlement_received.settlement_received, 0)
FROM #tempFundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT tt.funding_id, SUM(d.trans_amount) AS cash_to_pp
	FROM #tempTrans tt WITH (NOLOCK)
	INNER JOIN transaction_details d WITH (NOLOCK) ON tt.trans_id = d.trans_id	
	WHERE tt.trans_type_id IN (1, 2, 3, 4, 5, 14, 16, 18, 19, 21, 22, 24) -- actually received (cash to pp)  -- 1 was used to fix some trans so need to put it here
		AND d.trans_type_id = 7
	GROUP BY tt.funding_id
) cash_to_pp ON f.funding_id = cash_to_pp.funding_id
LEFT JOIN (
	SELECT tt.funding_id, SUM(d.trans_amount) AS cash_to_margin
	FROM #tempTrans tt WITH (NOLOCK)
	INNER JOIN transaction_details d WITH (NOLOCK) ON tt.trans_id = d.trans_id	
	WHERE tt.trans_type_id IN (1, 2, 3, 4, 5, 14, 16, 18, 19, 21, 22, 24) -- actually received (cash to margin) -- 1 was used to fix some trans so need to put it here
		AND d.trans_type_id = 6
	GROUP BY tt.funding_id
) cash_to_margin ON f.funding_id = cash_to_margin.funding_id
LEFT JOIN (
	SELECT tt.funding_id, SUM(tt.trans_amount) AS wires_no_renewal
	FROM #tempTrans tt WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON tt.batch_id = b.batch_id
	WHERE tt.trans_type_id = 5 AND COALESCE(b.batch_type, '') <> 'ContractRenewal'
	GROUP BY tt.funding_id
) wires_no_renewal ON f.funding_id = wires_no_renewal.funding_id
LEFT JOIN (
	SELECT tt.funding_id, SUM(d.trans_amount) AS cash_to_bad_debt
	FROM #tempTrans tt WITH (NOLOCK)
	INNER JOIN transaction_details d WITH (NOLOCK) ON tt.trans_id = d.trans_id
	WHERE d.trans_type_id = 15 
	GROUP BY tt.funding_id
) cash_to_bad_debt ON f.funding_id = cash_to_bad_debt.funding_id
LEFT JOIN (
	SELECT tt.funding_id, SUM(tt.trans_amount) AS cash_to_reapplication
	FROM #tempTrans tt WITH (NOLOCK)	
	WHERE tt.trans_type_id = 16 AND tt.trans_amount < 0
	GROUP BY tt.funding_id
) cash_to_reapplication ON f.funding_id = cash_to_reapplication.funding_id
LEFT JOIN (
	SELECT tt.funding_id, SUM(tt.trans_amount) AS cash_from_reapplication
	FROM #tempTrans tt WITH (NOLOCK)	
	WHERE tt.trans_type_id = 16 AND tt.trans_amount > 0
	GROUP BY tt.funding_id
) cash_from_reapplication ON f.funding_id = cash_from_reapplication.funding_id
LEFT JOIN (
	SELECT tt.funding_id, SUM(tt.trans_amount) AS ach_revenue
	FROM #tempTrans tt WITH (NOLOCK)	
	WHERE ((tt.trans_type_id = 3 AND tt.trans_date < '2019-01-01')
		OR (tt.trans_type_id = 18 AND tt.trans_date >= '2019-01-01'))
	GROUP BY tt.funding_id
) ach_revenue ON f.funding_id = ach_revenue.funding_id
LEFT JOIN (
	SELECT tt.funding_id, SUM(d.trans_amount) AS pp_settlement
	FROM #tempTrans tt WITH (NOLOCK)
	INNER JOIN transaction_details d WITH (NOLOCK) ON tt.trans_id = d.trans_id	
	WHERE tt.trans_type_id IN (10) 
		AND d.trans_type_id = 7
	GROUP BY tt.funding_id
) pp_settlement ON f.funding_id = pp_settlement.funding_id
LEFT JOIN (
	SELECT tt.funding_id, SUM(d.trans_amount) AS margin_settlement
	FROM #tempTrans tt WITH (NOLOCK)
	INNER JOIN transaction_details d WITH (NOLOCK) ON tt.trans_id = d.trans_id	
	WHERE tt.trans_type_id IN (10) 
		AND d.trans_type_id = 6
	GROUP BY tt.funding_id
) margin_settlement ON f.funding_id = margin_settlement.funding_id
LEFT JOIN (
	SELECT tt.funding_id, SUM(d.trans_amount) AS cash_fees
	FROM #tempTrans tt WITH (NOLOCK)
	INNER JOIN transaction_details d WITH (NOLOCK) ON tt.trans_id = d.trans_id	
	WHERE d.trans_type_id = 1
	GROUP BY tt.funding_id
) cash_fees ON f.funding_id = cash_fees.funding_id
LEFT JOIN (
	SELECT tt.funding_id, SUM(d.trans_amount) AS fees_applied
	FROM #tempTrans tt WITH (NOLOCK)
	INNER JOIN transaction_details d WITH (NOLOCK) ON tt.trans_id = d.trans_id	
	WHERE d.trans_type_id = 9
	GROUP BY tt.funding_id
) fees_applied ON f.funding_id = fees_applied.funding_id
LEFT JOIN (
	SELECT tt.funding_id, SUM(tt.trans_amount) AS settlement_received
	FROM #tempTrans tt WITH (NOLOCK)
	--INNER JOIN transaction_details d WITH (NOLOCK) ON tt.trans_id = d.trans_id	
	WHERE tt.trans_type_id = 10
	GROUP BY tt.funding_id
) settlement_received ON f.funding_id = settlement_received.funding_id


SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR



UPDATE f
SET pp_adjustment = COALESCE(f.writeoff_received, 0) + COALESCE(f.pp_settlement, 0), 
	margin_adjustment = COALESCE(f.discount_received, 0) + COALESCE(f.margin_settlement, 0) + COALESCE(f.writeoff_adjustment_received, 0)
FROM #tempFundings f WITH (NOLOCK)

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR




UPDATE f
SET pp_applied = COALESCE(f.cash_to_pp, 0) + COALESCE(f.pp_adjustment, 0), margin_applied = COALESCE(f.cash_to_margin, 0) + COALESCE(f.margin_adjustment, 0), 
	cash_to_rtr = COALESCE(f.cash_to_pp, 0) + COALESCE(f.cash_to_margin, 0), rtr_adjustment = COALESCE(f.discount_received, 0) + COALESCE(f.writeoff_received, 0) + 
			-- only add in settlement when it has NOT been written off.  Write off already zeroes out rtr so do not adjust again when there is writeoff
		CASE WHEN COALESCE(f.writeoff_received, 0) > 0 THEN 0 ELSE COALESCE(f.settlement_received, 0) END + 
	COALESCE(f.writeoff_adjustment_received, 0),
	gross_revenue = COALESCE(f.orig_fee, 0) + COALESCE(f.cash_fees, 0)  + COALESCE(f.cash_to_margin, 0)  + COALESCE(f.cash_to_pp, 0)  + COALESCE(f.cash_to_bad_debt, 0), 
	gross_received = COALESCE(f.counter_deposit_revenue, 0) + COALESCE(f.ach_revenue, 0) + COALESCE(f.cc_revenue, 0) + COALESCE(f.wires_revenue, 0) + COALESCE(f.checks_revenue, 0),
	gross_dollars_rec = COALESCE(f.counter_deposit_revenue, 0) + COALESCE(f.ach_revenue, 0) + COALESCE(f.cc_revenue, 0) + COALESCE(f.wires_revenue_no_renewal, 0) + COALESCE(f.checks_revenue, 0)
FROM #tempFundings f WITH (NOLOCK)

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR



UPDATE f
SET rtr_applied = COALESCE(f.cash_to_rtr, 0) + COALESCE(f.rtr_adjustment, 0), net_other_income = COALESCE(f.gross_dollars_rec, 0) - COALESCE(f.cash_to_rtr, 0), 
	total_portfolio_adjustment = COALESCE(margin_adjustment, 0) + COALESCE(pp_adjustment, 0),
--	percent_paid = COALESCE(f.cash_to_rtr, 0) / f.portfolio_value * 100.00,
	applied_amount = COALESCE(f.cash_fees, 0) + COALESCE(f.margin_applied, 0) + COALESCE(f.pp_applied, 0) + COALESCE(f.cash_to_bad_debt, 0) + COALESCE(f.discount_received, 0) 
		- COALESCE(f.cash_to_chargeback, 0)
FROM #tempFundings f WITH (NOLOCK)

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR



UPDATE f
SET rtr_out = COALESCE(f.portfolio_value, 0) - COALESCE(f.rtr_applied, 0), pp_out = COALESCE(f.purchase_price, 0) - COALESCE(f.pp_applied, 0),
	margin_out = COALESCE(f.unearned_income, 0) - COALESCE(f.margin_applied, 0), fees_out = COALESCE(f.fees_applied, 0) - COALESCE(f.cash_fees, 0), 
	net_revenue = COALESCE(f.cash_to_margin, 0) + COALESCE(f.net_other_income, 0)
FROM #tempFundings f WITH (NOLOCK)


SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


UPDATE f
SET workdays_since_funded = DATEDIFF(DAY, f.funded_date, @today) + 1 - (DATEDIFF(WEEK, funded_date, @today) * 2)
	+ CASE WHEN DATEPART(WEEKDAY, DATEADD(DAY, @@datefirst, funded_date)) = 1 THEN -1 ELSE 0 END
	+ CASE WHEN DATEPART(WEEKDAY, DATEADD(DAY, @@datefirst, GETDATE())) = 7 THEN -1 ELSE 0 END,
	--  payoff_amount = COALESCE(f.rtr_out, 0) + COALESCE(f.fees_out, 0),
	total_fees_collected = COALESCE(f.orig_fee, 0) + COALESCE(f.cash_fees, 0)--, paid_off_amount = f.portfolio_value - COALESCE(f.rtr_out, 0)	
FROM #tempFundings f WITH (NOLOCK)


SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


-- this might be where we need to update fundings table with all temp values from #tempFundings
--UPDATE f
--SET ach_draft = COALESCE(f.ach_draft, 0) + COALESCE(t.ach_draft, 0), ach_received = COALESCE(t.ach_received, 0) + COALESCE(f.ach_received, 0), 
--	ach_reject = COALESCE(t.ach_reject, 0) + COALESCE(f.ach_reject, 0), ach_revenue = COALESCE(t.ach_revenue, 0) + COALESCE(f.ach_revenue, 0), 
--	applied_amount = COALESCE(t.applied_amount, 0) + COALESCE(f.applied_amount, 0), cash_fees = COALESCE(t.cash_fees, 0) + COALESCE(f.cash_fees, 0), 
--	cash_from_reapplication = COALESCE(t.cash_from_reapplication, 0) + COALESCE(f.cash_from_reapplication, 0), cash_to_bad_debt = COALESCE(t.cash_to_bad_debt, 0) + COALESCE(f.cash_to_bad_debt, 0), 
--	cash_to_chargeback = COALESCE(t.cash_to_chargeback, 0) + COALESCE(f.cash_to_chargeback, 0), workdays_since_funded = COALESCE(t.workdays_since_funded, 0), 
--	cash_to_margin = COALESCE(t.cash_to_margin, 0) + COALESCE(f.cash_to_margin, 0), cash_to_pp = COALESCE(t.cash_to_pp, 0) + COALESCE(f.cash_to_pp, 0), 
--	cash_to_reapplication = COALESCE(t.cash_to_reapplication, 0) + COALESCE(f.cash_to_reapplication, 0), cash_to_rtr = COALESCE(t.cash_to_rtr, 0) + COALESCE(f.cash_to_rtr, 0), 
--	cc_revenue = COALESCE(t.cc_revenue, 0) + COALESCE(f.cc_revenue, 0), checks_revenue = COALESCE(t.checks_revenue, 0) + COALESCE(f.checks_revenue, 0), 
--	continuous_pull = COALESCE(t.continuous_pull, 0) + COALESCE(f.continuous_pull, 0), counter_deposit_revenue = COALESCE(t.counter_deposit_revenue, 0) + COALESCE(f.counter_deposit_revenue, 0), 
--	discount_received = COALESCE(t.discount_received, 0) + COALESCE(f.discount_received, 0), fees_applied = COALESCE(t.fees_applied, 0) + COALESCE(f.fees_applied, 0), 
--	fees_out = COALESCE(t.fees_out, 0) + COALESCE(f.fees_out, 0), gross_dollars_rec = COALESCE(t.gross_dollars_rec, 0) + COALESCE(f.gross_dollars_rec, 0), 
--	gross_received = COALESCE(t.gross_received, 0) + COALESCE(f.gross_received, 0), gross_revenue = COALESCE(t.gross_revenue, 0) + COALESCE(f.gross_revenue, 0),
--	margin_adjustment = COALESCE(t.margin_adjustment, 0) + COALESCE(f.margin_adjustment, 0), margin_applied = COALESCE(t.margin_applied, 0) + COALESCE(f.margin_applied, 0), 
--	margin_out = COALESCE(t.margin_out, 0) + COALESCE(f.margin_out, 0), net_other_income = COALESCE(t.net_other_income, 0) + COALESCE(f.net_other_income, 0), 
--	net_revenue = COALESCE(t.net_revenue, 0) + COALESCE(f.net_revenue, 0), paid_off_amount = COALESCE(t.paid_off_amount, 0) + COALESCE(f.paid_off_amount, 0), 
--	--percent_paid = COALESCE(t.percent_paid, 0),
--	pp_adjustment = COALESCE(t.pp_adjustment, 0) + COALESCE(f.pp_adjustment, 0), pp_applied = COALESCE(t.pp_applied, 0) + COALESCE(f.pp_applied, 0), 
--	pp_out = COALESCE(t.pp_out, 0) + COALESCE(f.pp_out, 0), refund = COALESCE(t.refund, 0) + COALESCE(f.refund, 0), 
--	refund_processed = COALESCE(t.refund_processed, 0) + COALESCE(f.refund_processed, 0), refund_reapplied = COALESCE(t.refund_reapplied, 0) + COALESCE(f.refund_reapplied, 0), 
--	refund_returned = COALESCE(t.refund_returned, 0) + COALESCE(f.refund_returned, 0), rtr_adjustment = COALESCE(t.rtr_adjustment, 0) + COALESCE(f.rtr_adjustment, 0), 
--	rtr_applied = COALESCE(t.rtr_applied, 0) + COALESCE(f.rtr_applied, 0), rtr_out = COALESCE(t.rtr_out, 0) + COALESCE(f.rtr_out, 0), 
--	settlement_received = COALESCE(t.settlement_received, 0) + COALESCE(f.settlement_received, 0), total_fees_collected = COALESCE(t.total_fees_collected, 0) + COALESCE(f.total_fees_collected, 0), 
--	total_portfolio_adjustment = COALESCE(t.total_portfolio_adjustment, 0) + COALESCE(f.total_portfolio_adjustment, 0), wires_revenue = COALESCE(t.wires_revenue, 0) + COALESCE(f.wires_revenue, 0), 
--	wires_revenue_no_renewal = COALESCE(t.wires_revenue_no_renewal, 0) + COALESCE(f.wires_revenue_no_renewal, 0), 
--	writeoff_adjustment_received = COALESCE(t.writeoff_adjustment_received, 0) + COALESCE(f.writeoff_adjustment_received, 0), 
--	writeoff_received = COALESCE(t.writeoff_received, 0) + COALESCE(f.writeoff_received, 0), change_date = @timeStamp, change_user = @change_user
--FROM fundings f 
--INNER JOIN #tempFundings t ON f.id = t.funding_id
UPDATE f
SET ach_draft = COALESCE(t.ach_draft, 0), ach_received = COALESCE(t.ach_received, 0), 
	ach_reject = COALESCE(t.ach_reject, 0), ach_revenue = COALESCE(t.ach_revenue, 0), 
	applied_amount = COALESCE(t.applied_amount, 0), cash_fees = COALESCE(t.cash_fees, 0), 
	cash_from_reapplication = COALESCE(t.cash_from_reapplication, 0), cash_to_bad_debt = COALESCE(t.cash_to_bad_debt, 0), 
	cash_to_chargeback = COALESCE(t.cash_to_chargeback, 0), workdays_since_funded = COALESCE(t.workdays_since_funded, 0), 
	cash_to_margin = COALESCE(t.cash_to_margin, 0), cash_to_pp = COALESCE(t.cash_to_pp, 0), 
	cash_to_reapplication = COALESCE(t.cash_to_reapplication, 0), cash_to_rtr = COALESCE(t.cash_to_rtr, 0), 
	cc_revenue = COALESCE(t.cc_revenue, 0), checks_revenue = COALESCE(t.checks_revenue, 0), 
	continuous_pull = COALESCE(t.continuous_pull, 0), counter_deposit_revenue = COALESCE(t.counter_deposit_revenue, 0), 
	discount_received = COALESCE(t.discount_received, 0), fees_applied = COALESCE(t.fees_applied, 0), 
	fees_out = COALESCE(t.fees_out, 0), gross_dollars_rec = COALESCE(t.gross_dollars_rec, 0), 
	gross_received = COALESCE(t.gross_received, 0), gross_revenue = COALESCE(t.gross_revenue, 0),
	margin_adjustment = COALESCE(t.margin_adjustment, 0), margin_applied = COALESCE(t.margin_applied, 0), 
	margin_out = COALESCE(t.margin_out, 0), net_other_income = COALESCE(t.net_other_income, 0), 
	net_revenue = COALESCE(t.net_revenue, 0), -- paid_off_amount = COALESCE(t.paid_off_amount, 0), 
	--percent_paid = COALESCE(t.percent_paid, 0),
	pp_adjustment = COALESCE(t.pp_adjustment, 0), pp_applied = COALESCE(t.pp_applied, 0), 
	margin_settlement = COALESCE(t.margin_settlement, 0),
	pp_settlement = COALESCE(t.pp_settlement, 0),
	pp_out = COALESCE(t.pp_out, 0), refund = COALESCE(t.refund, 0), 
	refund_processed = COALESCE(t.refund_processed, 0), refund_reapplied = COALESCE(t.refund_reapplied, 0), 
	refund_returned = COALESCE(t.refund_returned, 0), rtr_adjustment = COALESCE(t.rtr_adjustment, 0), 
	rtr_applied = COALESCE(t.rtr_applied, 0), rtr_out = COALESCE(t.rtr_out, 0), 
	settlement_received = COALESCE(t.settlement_received, 0), total_fees_collected = COALESCE(t.total_fees_collected, 0), 
	total_portfolio_adjustment = COALESCE(t.total_portfolio_adjustment, 0), wires_revenue = COALESCE(t.wires_revenue, 0), 
	wires_revenue_no_renewal = COALESCE(t.wires_revenue_no_renewal, 0), 
	writeoff_adjustment_received = COALESCE(t.writeoff_adjustment_received, 0), 
	writeoff_received = COALESCE(t.writeoff_received, 0), change_date = @timeStamp, change_user = @change_user
FROM fundings f 
INNER JOIN #tempFundings t ON f.id = t.funding_id

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


-- UPDATE fundings SET payoff_amount = COALESCE(rtr_out, 0) + COALESCE(fees_out, 0), change_date = GETUTCDATE(), change_user = @change_user
UPDATE fundings SET payoff_amount = (portfolio_value - COALESCE(cash_to_margin, 0) - COALESCE(cash_to_pp, 0) - COALESCE(cash_to_bad_debt, 0) - COALESCE(discount_received, 0) 
	 - COALESCE(settlement_received, 0) + COALESCE(fees_out, 0)), change_date = @timeStamp, change_user = @change_user
WHERE (contract_status = 2 OR (contract_status = 4 AND contract_substatus_id = 2)) 
	AND COALESCE(payoff_amount, 0) <> (portfolio_value - COALESCE(cash_to_margin, 0) - COALESCE(cash_to_pp, 0) - COALESCE(cash_to_bad_debt, 0) - COALESCE(discount_received, 0) 
	 - COALESCE(settlement_received, 0) + COALESCE(fees_out, 0))

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR



UPDATE fundings SET paid_off_amount = portfolio_value - COALESCE(rtr_out, portfolio_value), change_date = @timeStamp, change_user = @change_user
WHERE (contract_status = 2 OR (contract_status = 4 AND contract_substatus_id = 2))
	AND COALESCE(paid_off_amount, 0) <> portfolio_value - COALESCE(rtr_out, portfolio_value)

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


UPDATE fundings SET percent_paid = COALESCE(cash_to_rtr, 0) / portfolio_value * 100.00, change_date = GETUTCDATE(), change_user = @change_user
WHERE (contract_status = 2 OR (contract_status = 4 AND contract_substatus_id = 2)) AND COALESCE(percent_paid, 0) <> COALESCE(cash_to_rtr, 0) / portfolio_value * 100.00


SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


UPDATE f
SET percent_complete = COALESCE(x.paid_off, 0) / f.portfolio_value * 100.00, change_date = GETUTCDATE(), change_user = @change_user
FROM fundings f
LEFT JOIN (
	SELECT f.id, SUM(CASE WHEN d.trans_type_id IN (1, 6, 7, 15, 8, 14) THEN d.trans_amount -- 11, 8
						WHEN d.trans_type_id IN (9) THEN -1 * d.trans_amount 
					  ELSE 0 END) AS paid_off
	FROM fundings f WITH (NOLOCK)
	INNER JOIN transactions t WITH (NOLOCK) ON f.id = t.funding_id
	INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0
		AND (f.contract_status = 2 OR (f.contract_status = 4 AND contract_substatus_id = 2))
		AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 
	GROUP BY f.id
) x ON f.id = x.id
WHERE COALESCE(percent_complete, 0) <> COALESCE(x.paid_off, 0) / f.portfolio_value * 100.00
	AND (contract_status = 2 OR (contract_status = 4 AND contract_substatus_id = 2))

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


-- actual turn is always changing so update separately each time
UPDATE fundings
SET actual_turn = CASE WHEN 
					ABS(CASE WHEN DATEDIFF(DD, funded_date, @today) <= 3  AND frequency = 'Daily' THEN estimated_turn
						WHEN DATEDIFF(DD, funded_date, @today) <= 10 AND frequency = 'Weekly' THEN estimated_turn
						ELSE (portfolio_value / ((COALESCE(cash_to_rtr, 0) / COALESCE(workdays_since_funded, 1)) + 0.0000001)) / 21
					END) > 60  -- shouldn't have negative cash_to_rtr but it has happened (I think it may have been miscalculations)
					THEN 60
					ELSE 
						CASE WHEN DATEDIFF(DD, funded_date, @today) <= 3  AND frequency = 'Daily' THEN estimated_turn
							WHEN DATEDIFF(DD, funded_date, @today) <= 10 AND frequency = 'Weekly' THEN estimated_turn
							ELSE (portfolio_value / ((COALESCE(cash_to_rtr, 0) / COALESCE(workdays_since_funded, 1)) + 0.0000001)) / 21
						END
					END, change_date = @timeStamp, change_user = @change_user
WHERE (contract_status = 2 OR (contract_status = 4 AND contract_substatus_id = 2))
	AND funded_date IS NOT NULL
	AND COALESCE(actual_turn, -1) <> (
				  CASE WHEN 
					ABS(CASE WHEN DATEDIFF(DD, funded_date, @today) <= 3  AND frequency = 'Daily' THEN estimated_turn
						WHEN DATEDIFF(DD, funded_date, @today) <= 10 AND frequency = 'Weekly' THEN estimated_turn
						ELSE (portfolio_value / ((COALESCE(cash_to_rtr, 0) / COALESCE(workdays_since_funded, 1)) + 0.0000001)) / 21
					END) > 60
					THEN 60
					ELSE 
						CASE WHEN DATEDIFF(DD, funded_date, @today) <= 3  AND frequency = 'Daily' THEN estimated_turn
							WHEN DATEDIFF(DD, funded_date, @today) <= 10 AND frequency = 'Weekly' THEN estimated_turn
							ELSE (portfolio_value / ((COALESCE(cash_to_rtr, 0) / COALESCE(workdays_since_funded, 1)) + 0.0000001)) / 21
						END
					END
	)


SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


UPDATE fundings
SET percent_performance = (estimated_turn - actual_turn) / estimated_turn * 100.00, change_date = @timeStamp, change_user = @change_user
WHERE (contract_status = 2 OR (contract_status = 4 AND contract_substatus_id = 2))
	AND actual_turn IS NOT NULL
	AND (
		COALESCE(percent_performance, 0) <> ((estimated_turn - actual_turn) / estimated_turn * 100.00)	
	)

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


UPDATE fundings
SET change_date = @timeStamp, change_user = @change_user, anticipated_final_payment_date = DATEADD(DD, actual_turn * 30.4, funded_date)
WHERE (contract_status = 2 OR (contract_status = 4 AND contract_substatus_id = 2))
	AND actual_turn IS NOT NULL
	AND funded_date IS NOT NULL
	AND COALESCE(anticipated_final_payment_date, '2000-01-01') <> DATEADD(DD, actual_turn * 30.4, funded_date)


SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


UPDATE fundings
SET est_months_left = DATEDIFF(DD, @today, anticipated_final_payment_date)/30.4, change_date = @timeStamp, change_user = @change_user
WHERE (contract_status = 2 OR (contract_status = 4 AND contract_substatus_id = 2))
	AND anticipated_final_payment_date IS NOT NULL
	AND COALESCE(est_months_left, 0) <> DATEDIFF(DD, @today, anticipated_final_payment_date)/30.4
	

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


UPDATE fundings
SET last_payment_date = x.last_payment_date, change_date = @timeStamp, change_user = @change_user
FROM fundings f WITH (NOLOCK)
INNER JOIN (
	SELECT t.funding_id, MAX(t.trans_date) AS last_payment_date
	FROM transactions t	 WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.trans_type_id IN (2, 3, 4, 5, 19) AND t.trans_amount <> 0 AND t.redistributed = 0
		AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE (contract_status = 2 OR (contract_status = 4 AND contract_substatus_id = 2))
	AND COALESCE(f.last_payment_date, '2000-01-01') != COALESCE(x.last_payment_date, '2000-01-01')


SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


UPDATE fundings
SET last_trans_amount = t.trans_amount, change_date = @timeStamp, change_user = @change_user
FROM fundings f WITH (NOLOCK)
INNER JOIN transactions t WITH (NOLOCK) ON f.id = t.funding_id AND t.trans_type_id IN (2, 3, 4, 5, 19) AND t.trans_amount <> 0 AND t.redistributed = 0
	AND t.trans_date = f.last_payment_date
LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
WHERE (contract_status = 2 OR (contract_status = 4 AND contract_substatus_id = 2))
	AND COALESCE(f.last_trans_amount, -1) != COALESCE(t.trans_amount, -1)
	AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)


SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


UPDATE fundings
SET first_received_date = x.first_date, change_date = @timeStamp, change_user = @change_user
FROM fundings f WITH (NOLOCK)
INNER JOIN (
	SELECT t.funding_id, MIN(t.trans_date) AS first_date
	FROM transactions t	 WITH (NOLOCK)	
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND t.trans_type_id IN (2, 3, 4, 5, 19) AND t.trans_amount <> 0 
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE (f.contract_status = 2 OR (f.contract_status = 4 AND contract_substatus_id = 2))


SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR



UPDATE fundings
SET last_draft_date = x.last_payment_date, change_date = @timeStamp, change_user = @change_user
FROM fundings f WITH (NOLOCK)
INNER JOIN (
	SELECT t.funding_id, MAX(t.trans_date) AS last_payment_date
	FROM transactions t	 WITH (NOLOCK)	
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND t.trans_type_id IN (2, 18, 4, 5, 19) AND t.trans_amount <> 0
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE (f.contract_status = 2 OR (f.contract_status = 4 AND contract_substatus_id = 2))
	AND COALESCE(f.last_draft_date, '2000-01-01') != COALESCE(x.last_payment_date, '2000-01-01')


SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR



UPDATE fundings
SET first_draft_date = x.first_date, change_date = @timeStamp, change_user = @change_user
FROM fundings f WITH (NOLOCK)
INNER JOIN (
	SELECT t.funding_id, MIN(t.trans_date) AS first_date
	FROM transactions t	 WITH (NOLOCK)	
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND t.trans_type_id IN (2, 18, 4, 5, 19) AND t.trans_amount <> 0
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE (f.contract_status = 2 OR (f.contract_status = 4 AND contract_substatus_id = 2))


SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


-- now do the float
UPDATE f 
SET float_amount = COALESCE(x.total_float, 0) + COALESCE(y.total_float, 0), change_date =  @timeStamp, change_user = @change_user
FROM fundings f WITH (NOLOCK)
-- get float from batches
LEFT JOIN (
	SELECT f2.id, SUM(CASE WHEN t.trans_type_id IN (9, 14, 17) THEN -1* t.trans_amount ELSE t.trans_amount END) AS total_float
	FROM fundings f2 WITH (NOLOCK)
	JOIN transactions t WITH (NOLOCK) ON f2.id = t.funding_id
	INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE b.batch_status IN ('Processed') AND b.batch_type NOT IN ('ForteAPI') -- if it IS ForteAPI, we've already distributed the money so it is not in float.
	GROUP BY f2.id
) x ON f.id = x.id
LEFT JOIN (
-- now get float from forte trans
	SELECT SUM(t.authorization_amount) AS total_float, f.id
	FROM forte_trans t WITH (NOLOCK)
	INNER JOIN fundings f WITH (NOLOCK) ON t.customer_id = f.contract_number
	LEFT JOIN forte_trans t2 WITH (NOLOCK) ON t.transaction_id = t2.transaction_id AND t2.status <> 'settling'
	WHERE t.status = 'settling' AND t2.forte_trans_id IS NULL -- this makes sure it does not have a matching record (which would mean there is an update to the settling)
	GROUP BY f.id
) y ON f.id = y.id
WHERE ((f.float_amount <> COALESCE(x.total_float, 0) + COALESCE(y.total_float, 0)) 
	OR f.float_amount IS NULL)
	AND (f.contract_status = 2 OR (f.contract_status = 4 AND contract_substatus_id = 2))

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR



-- RKB  2019-10-17
-- completed contracts may still have an outstanding balance and refund_out needs to be updated 
UPDATE f
SET change_date =  @timeStamp, change_user = @change_user, refund_out = COALESCE(refund.refund_out, 0)
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT t.funding_id, SUM(t.trans_amount) AS refund_out
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id		
	WHERE t.trans_type_id IN (22,23, 25) AND t.redistributed = 0  -- 24 is not included because 23 is money leaving the account, 24 is money going on to an account
		AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 
	GROUP BY t.funding_id
) refund ON f.id = refund.funding_id
WHERE (f.contract_status = 2 OR (f.contract_status = 4 AND contract_substatus_id = 2))
	AND COALESCE(f.refund_out, -999) <> COALESCE(refund.refund_out, 999)
	AND COALESCE(f.refund_out, 0) <> 0	
	
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR



-- Estimated final payment date
UPDATE fundings
SET estimated_final_payment_date = x.final_date, change_date = @timeStamp, change_user = @change_user
FROM fundings f WITH (NOLOCK)
INNER JOIN (
	SELECT MAX(payment_date) AS final_date, funding_id
	FROM payment_forecasts WITH (NOLOCK)
	GROUP BY funding_id
) x ON f.id = x.funding_id
WHERE (f.contract_status = 2 OR (f.contract_status = 4 AND contract_substatus_id = 2))  -- need to include 4 here otherwise it won't ever get set on writeoffs 
	AND COALESCE(estimated_final_payment_date, '2001-01-01') <> x.final_date


SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR



-- update past due amount
UPDATE f
SET variance_amount = f.portfolio_value - f.cash_to_rtr, change_date = @timeStamp, change_user = @change_user
FROM fundings f WITH (NOLOCK)
WHERE (f.contract_status = 2 OR (f.contract_status = 4 AND contract_substatus_id = 2)) AND COALESCE(variance_amount, 0) <> f.portfolio_value - f.cash_to_rtr

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


-------------------------
-------------------------
-- NOW UPDATE THE TRANSACTIONS 
-------------------------
-------------------------
UPDATE t
SET calculated = 1
FROM transactions t WITH (NOLOCK)
INNER JOIN #tempTrans tt ON t.trans_id = tt.trans_id


SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


COMMIT TRANSACTION
GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)

Done:

IF OBJECT_ID('tempdb..#tempTrans') IS NOT NULL DROP TABLE #tempTrans

END
GO
