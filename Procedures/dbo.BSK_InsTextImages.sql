SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rick Pina
-- Create date: 2021-04-02
-- Description:	Saves text images into the database
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsTextImages]
(
	@text_id				INT,
	@image_url				NVARCHAR (1000),
	@create_user			NVARCHAR (50)
)
AS
BEGIN
SET NOCOUNT ON;

INSERT INTO text_images(text_id, image_url, create_date, create_user, change_date, change_user)
VALUES (@text_id, @image_url, GETUTCDATE(), @create_user, GETUTCDATE(), @create_user)

END
GO
