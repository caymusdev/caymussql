SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-02-07
-- Description:	Takes an id for the worker table and then creates the changes in the actual table
-- =============================================
CREATE PROCEDURE [dbo].[CSP_ApprovePaymentDateChange]
(
	@worker_id			INT,
	@approver			NVARCHAR (100)
)
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @funding_payment_date_id INT, @change_type VARCHAR (50), @newID INT, @payment_date DATE, @new_amount MONEY
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SET @tomorrow = dbo.CF_GetTomorrow(@today)
DECLARE @yesterday DATE; SELECT @yesterday = DATEADD(DD, CASE DATEPART(WEEKDAY, @today) WHEN 1 THEN -2 WHEN 2 THEN -3 ELSE -1 END, @today)

DECLARE @original_user NVARCHAR (100), @funding_id BIGINT

DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
DECLARE @row_count INT; SET @row_count = 0


-- first, just make sure it hasn't already been approved or rejected
-- This doesn't work because if they want to make a change to an approved one, this will error
--IF (SELECT approved FROM funding_payment_dates_work WHERE id = @worker_id) = 1
--	BEGIN
--		RAISERROR ('Already approved.', 16, 1)	
--	END
--ELSE IF (SELECT rejected_date FROM funding_payment_dates_work WHERE id = @worker_id) IS NOT NULL
--	BEGIN
--		RAISERROR ('Already rejected.', 16, 1)	
--	END
--ELSE 
--	BEGIN
		BEGIN TRANSACTION

		-- get the existing payment that we are changing, if there is one
		SELECT @funding_payment_date_id = w.funding_payment_id, 
			@change_type = CASE WHEN w.funding_payment_id IS NULL THEN 'New' WHEN w.active = 0 THEN 'Delete' ELSE 'Change' END, 
			@payment_date = w.payment_date, @original_user = w.userid, @funding_id = w.funding_id, @new_amount = w.amount
		FROM funding_payment_dates_work w
		WHERE w.id = @worker_id

		-- need to check if this is revising an original payment schedule
		IF (SELECT revised_original FROM funding_payment_dates_work WHERE id = @worker_id) = 1
			BEGIN
				-- this is a revised original so we are approving all, not just the one worker id.
				-- if it happens to be in the past we are going to ignore that fact and just delete them.  shouldn't happen because the UI should prevent changes in the past, 
				-- but if the approver were to get to it the next day one might be in the past.  Don't think this will happen much
				UPDATE funding_payment_dates_work
				SET change_date = GETUTCDATE(), change_user = @approver
				WHERE revised_original = 1 AND COALESCE(approved, 0) = 0 AND ready_for_approval = 1
					AND payment_date < @tomorrow AND funding_id = @funding_id AND userid = @original_user

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERROR

				DELETE funding_payment_dates_work	
				WHERE revised_original = 1 AND COALESCE(approved, 0) = 0 AND ready_for_approval = 1
					AND payment_date < @tomorrow AND funding_id = @funding_id AND userid = @original_user

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERROR

		
				-- now do the valid ones

				IF OBJECT_ID('tempdb..#tempWorkerIDs') IS NOT NULL DROP TABLE #tempWorkerIDs2
				CREATE TABLE #tempWorkerIDs(id int not null identity (1, 1), worker_id INT, change_type VARCHAR (50))

				INSERT INTO #tempWorkerIDs (worker_id, change_type)
				SELECT w.id, CASE w.active WHEN 0 THEN 'Delete' ELSE 'New' END
				FROM funding_payment_dates_work w
				WHERE revised_original = 1 AND COALESCE(approved, 0) = 0 AND ready_for_approval = 1 AND funding_id = @funding_id AND userid = @original_user

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERROR

				-- we now have a list of all worker ids that are part of this revision
				-- deactive the delete ones
				/*UPDATE d
				SET active = 0, payment_schedule_change_id = t.worker_id, change_user = @approver, change_date = GETUTCDATE()
				FROM funding_payment_dates d
				INNER JOIN #tempWorkerIDs t ON d.id = t.worker_id AND t.change_type = 'Delete'
				*/
				-- i think the above might have stopped working after I changed the code to only load the dates the calendar is showing
				-- all we need to do is deactivate all original payments between start date and end date
				-- RKB - 2020-05-04 Thought I had already changed this but looks like not.  Revise Original will remove ALL future payments, not just originals.
				-- Just originals has caused confusion on accounts where there have been lots of changes.
				UPDATE funding_payment_dates
				SET active = 0, payment_schedule_change_id = @worker_id, change_user = @approver, change_date = GETUTCDATE()
				WHERE --COALESCE(original, 0) = 1 AND 
					funding_id = @funding_id AND payment_date >= @payment_date AND COALESCE(active, 0) = 1

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERROR
		

				-- now insert all new ones, get a timestamp so that we can indentify afterwards which ones changed and then update the workers with the newly created ids
				DECLARE @timeStamp DATETIME; SET @timeStamp = GETUTCDATE()
		
				INSERT INTO funding_payment_dates (funding_id, payment_date, amount, rec_amount, payment_schedule_id, create_date, active, original, chargeback_amount, 
					merchant_bank_account_id, payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, change_date, change_user, payment_plan_id)
				SELECT w.funding_id, w.payment_date, w.amount, 0, NULL, @timeStamp, 1, 0, NULL, w.merchant_bank_account_id, w.payment_method, w.payment_processor, 
					w.id, w.comments, w.change_reason_id, @timeStamp, w.change_user, w.payment_plan_id
				FROM funding_payment_dates_work w
				INNER JOIN #tempWorkerIDs t ON w.id = t.worker_id AND t.change_type = 'New'

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERROR

				-- now, update worker records with the newly created ids
				UPDATE w
				SET funding_payment_id = d.id, change_date = GETUTCDATE(), change_user = @approver
				FROM funding_payment_dates_work w
				INNER JOIN #tempWorkerIDs t ON w.id = t.worker_id AND t.change_type = 'New'
				INNER JOIN funding_payment_dates d ON w.payment_date = d.payment_date AND w.funding_id = d.funding_id AND d.create_date = @timeStamp
				WHERE funding_payment_id IS NULL

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERROR

				UPDATE w 
				SET approved = 1, approver = @approver, approved_date = GETUTCDATE(), change_user = @approver, change_date = GETUTCDATE(), ready_for_approval = 0
				FROM funding_payment_dates_work w
				INNER JOIN #tempWorkerIDs t ON w.id = t.worker_id

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERROR
		
				-- update any payments that might be in the queue for tomorrow
				UPDATE p
				SET amount = @new_amount, change_user = @original_user, change_date = GETUTCDATE()
				FROM payments p
				WHERE p.funding_id = @funding_id AND p.trans_date = @tomorrow AND p.amount <> @new_amount AND COALESCE(p.waived, 0) = 0
					AND COALESCE(p.deleted, 0) = 0 AND p.processed_date IS NULL AND p.approved_flag = 1

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERROR

				-- now need to set the off hold date if this funding is on hold
				INSERT INTO funding_off_hold_dates (funding_id, off_hold_date, create_date, change_date, change_user)
				SELECT f.id, dbo.CF_GetYesterday(@payment_date), GETUTCDATE(), GETUTCDATE(), @approver
				FROM fundings f
				WHERE f.id = @funding_id AND f.on_hold = 1
					AND NOT EXISTS (
						SELECT 1
						FROM funding_off_hold_dates o
						WHERE o.funding_id = @funding_id AND o.off_hold_date BETWEEN @today AND dbo.CF_GetYesterday(@payment_date)
					)

				SELECT @row_count = @@ROWCOUNT, @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERROR

				IF @row_count > 0
					-- if we inserted a funding off hold date, any future ones already there need to be deleted
					BEGIN
						DELETE FROM funding_off_hold_dates
						WHERE funding_id = @funding_id AND processed_date IS NULL AND off_hold_date > dbo.CF_GetYesterday(@payment_date)
					END
			END
		ELSE
			BEGIN
				-- want to double-check we are not making any changes in the past
				IF @payment_date < @tomorrow
					BEGIN
						SELECT 'Payment Date cannot be in the past: ' + CONVERT(VARCHAR (50), @payment_date) + ' / ' + CONVERT(VARCHAR (50), @worker_id) AS ErrorText
					END
				ELSE 
					BEGIN
						IF @change_type = 'Delete'
							BEGIN				
								UPDATE funding_payment_dates
								SET active = 0, payment_schedule_change_id = @worker_id, change_user = @approver, change_date = GETUTCDATE()
								WHERE id = @funding_payment_date_id

								SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERROR

								-- update any payment that might be in the queue for tomorrow
								UPDATE p
								SET deleted = 1, change_user = @original_user, change_date = GETUTCDATE()
								FROM payments p
								WHERE p.funding_payment_date_id = @funding_payment_date_id AND COALESCE(p.waived, 0) = 0
									AND COALESCE(p.deleted, 0) = 0 AND p.processed_date IS NULL AND p.approved_flag = 1
									AND p.trans_date = @tomorrow

								SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERROR

							END
						ELSE IF @change_type = 'New'
							BEGIN
								INSERT INTO funding_payment_dates (funding_id, payment_date, amount, rec_amount, payment_schedule_id, create_date, active, original, chargeback_amount, 
									merchant_bank_account_id, payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, change_date, change_user, 
									payment_plan_id)
								SELECT w.funding_id, w.payment_date, w.amount, 0, NULL, GETUTCDATE(), 1, 0, NULL, w.merchant_bank_account_id, w.payment_method, w.payment_processor, 
									w.id, w.comments, w.change_reason_id, GETUTCDATE(), w.change_user, w.payment_plan_id
								FROM funding_payment_dates_work w
								WHERE w.id = @worker_id
						
						
								SELECT @newID = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERROR

								UPDATE funding_payment_dates_work
								SET funding_payment_id = @newID, change_date = GETUTCDATE(), change_user = @original_user
								WHERE id = @worker_id AND funding_payment_id IS NULL

								SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERROR

								INSERT INTO payments (payment_schedule_id, amount, trans_date, transaction_id, approved_flag, processed_date, change_date, change_user, previous_transaction_id,
									orig_transaction_id, is_fee, retry_level, retry_sublevel, funding_id, complete, batch_id, retry_complete, processor, is_refund, is_debit, deleted, 
									waived, redirect_funding_id, redirect_approval_id, funding_payment_date_id)
								SELECT NULL, pd.amount, pd.payment_date, NULL, 1, NULL, GETUTCDATE(), @original_user, NULL, NULL, 0, 0, 0, pd.funding_id, NULL, NULL, NULL, NULL, 0, 1, 0, 
									0, NULL, NULL, pd.id
								FROM funding_payment_dates pd
								WHERE pd.id = @newID AND pd.payment_date = @tomorrow

								SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERROR
							
									-- now need to set the off hold date if this funding is on hold
								INSERT INTO funding_off_hold_dates (funding_id, off_hold_date, create_date, change_date, change_user)
								SELECT f.id, dbo.CF_GetYesterday(@payment_date), GETUTCDATE(), GETUTCDATE(), @approver
								FROM fundings f
								WHERE f.id = @funding_id AND f.on_hold = 1
									AND NOT EXISTS (
										SELECT 1
										FROM funding_off_hold_dates o
										WHERE o.funding_id = @funding_id AND o.off_hold_date BETWEEN @today AND dbo.CF_GetYesterday(@payment_date)
									)

								SELECT @row_count = @@ROWCOUNT, @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERROR

								IF @row_count > 0
									-- if we inserted a funding off hold date, any future ones already there need to be deleted
									BEGIN
										DELETE FROM funding_off_hold_dates
										WHERE funding_id = @funding_id AND processed_date IS NULL AND off_hold_date > dbo.CF_GetYesterday(@payment_date)
									END
							END
						ELSE IF @change_type = 'Change'
							BEGIN						
								DECLARE @orig_date DATE, @newDate DATE
								SELECT @orig_date = d.payment_date, @newDate = w.payment_date
								FROM funding_payment_dates d
								INNER JOIN funding_payment_dates_work w ON d.id = w.funding_payment_id
								WHERE w.id = @worker_id

								UPDATE d
								SET d.amount = w.amount, d.merchant_bank_account_id = w.merchant_bank_account_id, d.payment_method = w.payment_method, d.payment_processor = w.payment_processor, 
									d.payment_schedule_change_id = @worker_id, d.comments = w.comments, d.change_reason_id = w.change_reason_id, change_date = GETUTCDATE(), change_user = w.change_user,
									d.payment_plan_id = w.payment_plan_id, d.payment_date = w.payment_date -- in case they moved the payment
								FROM funding_payment_dates d
								INNER JOIN funding_payment_dates_work w ON d.id = w.funding_payment_id
								WHERE w.id = @worker_id

								SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERROR

								-- update any payment that might be in the queue for tomorrow
								UPDATE p
								SET amount = @new_amount, change_date = GETUTCDATE(), change_user = @approver
								FROM payments p
								WHERE p.funding_payment_date_id = @funding_payment_date_id AND COALESCE(p.waived, 0) = 0
									AND COALESCE(p.deleted, 0) = 0 AND p.processed_date IS NULL AND p.approved_flag = 1
									AND p.trans_date = @tomorrow AND @newDate = @tomorrow  -- only update the amount on the payment if they have not moved the date

								SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERROR

								-- need to delete a payment if they have moved tomorrow's payment
								UPDATE payments
								SET deleted = 1, change_date = GETUTCDATE(), change_user = @approver
								WHERE funding_payment_date_id = @funding_payment_date_id AND COALESCE(waived, 0) = 0 AND COALESCE(deleted, 0) = 0
									AND processed_date IS NULL AND approved_flag = 1 AND trans_date = @tomorrow AND @newDate != @tomorrow

								SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERROR

								-- need to create a payment if they moved it to tomorrow
								IF @newDate != @orig_date AND @newDate = @tomorrow
									BEGIN
										INSERT INTO payments (payment_schedule_id, amount, trans_date, transaction_id, approved_flag, processed_date, change_date, change_user, previous_transaction_id,
											orig_transaction_id, is_fee, retry_level, retry_sublevel, funding_id, complete, batch_id, retry_complete, processor, is_refund, is_debit, deleted, 
											waived, redirect_funding_id, redirect_approval_id, funding_payment_date_id)
										SELECT NULL, pd.amount, pd.payment_date, NULL, 1, NULL, GETUTCDATE(), @original_user, NULL, NULL, 0, 0, 0, pd.funding_id, NULL, NULL, NULL, NULL, 0, 1, 0, 
											0, NULL, NULL, pd.id
										FROM funding_payment_dates pd
										WHERE pd.id = @funding_payment_date_id 

										SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERROR
									END

								-- now need to set the off hold date if this funding is on hold
								INSERT INTO funding_off_hold_dates (funding_id, off_hold_date, create_date, change_date, change_user)
								SELECT f.id, dbo.CF_GetYesterday(@payment_date), GETUTCDATE(), GETUTCDATE(), @approver
								FROM fundings f
								WHERE f.id = @funding_id AND f.on_hold = 1
									AND NOT EXISTS (
										SELECT 1
										FROM funding_off_hold_dates o
										WHERE o.funding_id = @funding_id AND o.off_hold_date BETWEEN @today AND dbo.CF_GetYesterday(@payment_date)
									)

								SELECT @row_count = @@ROWCOUNT, @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERROR

								IF @row_count > 0
									-- if we inserted a funding off hold date, any future ones already there need to be deleted
									BEGIN
										DELETE FROM funding_off_hold_dates
										WHERE funding_id = @funding_id AND processed_date IS NULL AND off_hold_date > dbo.CF_GetYesterday(@payment_date)
									END
							END


						UPDATE funding_payment_dates_work 
						SET approved = 1, approver = @approver, approved_date = GETUTCDATE(), change_user = @approver, change_date = GETUTCDATE(), ready_for_approval = 0
						WHERE id = @worker_id

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERROR
			
					END
			END

	

		COMMIT TRANSACTION
		GOTO Done

		ERROR:	
			ROLLBACK TRANSACTION
			RAISERROR (@ErrorMessage, 16, 1)	

		Done:
--	END

END
GO
