SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Rick Pina
-- Create Date: 2021-01-20
-- Description: It does.
-- =============================================
CREATE PROCEDURE [dbo].[BSK_UpdCollectorCasesAssignedCollector]
(
	@case_department_ids			NVARCHAR (2000),
	@user_id						INT,
	@change_user					NVARCHAR (200)
)
AS
BEGIN
SET NOCOUNT ON
DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE());
DECLARE @yesterday DATE; SELECT @yesterday = DATEADD(DD, -1, @today)

BEGIN TRANSACTION

IF OBJECT_ID('tempdb..#tempIDs') IS NOT NULL DROP TABLE #tempIDs
CREATE TABLE #tempIDs (id INT IDENTITY (1, 1), current_collector_user_id INT, case_department_id INT, case_id INT, current_department_id INT)
 
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

INSERT INTO #tempIDs (case_department_id, current_collector_user_id, case_id, current_department_id)
SELECT cd.case_department_id, cc.collector_user_id, c.case_id, c.department_id
FROM string_split(@case_department_ids, '|') a 
INNER JOIN case_departments cd ON a.value = cd.case_department_id
INNER JOIN cases c ON cd.case_id = c.case_id
INNER JOIN collector_cases cc on c.case_id = cc.case_id AND cc.start_date <= DATEADD(HH, -5, GETUTCDATE()) AND COALESCE(cc.end_date, '2050-05-09') >= DATEADD(HH, -5, GETUTCDATE())

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

DECLARE @maxID INT, @currentID INT, @CurrentCaseID INT, @NewUserDepartment INT, @CurrentUserDepartment INT, @CurrentUserID INT
SELECT @maxID = MAX(id) , @currentID = 1
FROM #tempIDs

WHILE @currentID <= @maxID
BEGIN
	SELECT @CurrentCaseID = case_id, @CurrentUserDepartment = current_department_id, @CurrentUserID = current_collector_user_id
	FROM #tempIDs
	WHERE id = @CurrentID

	SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

	-- first select the first department the new user is in that is further along than the current department
	SELECT @NewUserDepartment = MIN(department_id)
	FROM user_departments
	where user_id = @user_id AND department_id >= @CurrentUserDepartment
	
	-- if new user department is still null, moving case backwards for example, then find the highest department the user works in and we'll put it there.
	IF @NewUserDepartment IS NULL
		BEGIN
			SELECT @NewUserDepartment = MAX(department_id)
			FROM user_departments
			WHERE user_id = @user_id 
		END

	SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
	IF @NewUserDepartment IS NOT NULL AND @CurrentUserID != @user_id
		BEGIN 
			UPDATE collector_cases
			SET end_date = @yesterday, change_user = @change_user, change_date = @today
			WHERE case_id = @CurrentCaseID AND end_date IS NULL

			SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

	
			INSERT INTO collector_cases (collector_user_id, case_id, start_date, end_date, create_date, change_date, change_user, create_user)
			VALUES (@user_id, @CurrentCaseID, @today, NULL, GETUTCDATE(), GETUTCDATE(), @change_user, @change_user)

			SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

				-- create a notification for the new case
			DECLARE @affiliate_id INT, @affiliate_name NVARCHAR (100)
			SELECT @affiliate_id = affiliate_id FROM cases WHERE case_id = @CurrentCaseID 

			SELECT @affiliate_name = 'New Case for ' + affiliate_name FROM affiliates WHERE affiliate_id = @affiliate_id

			SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


			EXEC dbo.BSK_InsNotification @user_id, NULL, @affiliate_id, NULL, NULL, @affiliate_name, 'NewCase', @change_user, @CurrentCaseID

			SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

			-- mark the case as new
			UPDATE cases SET new_case = 1, change_date = GETUTCDATE(), change_user = @change_user WHERE case_id = @CurrentCaseID
			SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


			IF @NewUserDepartment != @CurrentUserDepartment
				BEGIN 
					-- move case first - change the department_id
					UPDATE cases
					SET department_id = @NewUserDepartment, change_user = @change_user, change_date = GETUTCDATE()
					WHERE case_id = @CurrentCaseID AND department_id = @CurrentUserDepartment

					SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

					-- update previous case_departments records' end dates
					UPDATE case_departments
					SET end_date = @yesterday, change_user = @change_user, change_date = GETUTCDATE()
					WHERE case_id = @CurrentCaseID AND department_id = @CurrentUserDepartment AND end_date IS NULL

					SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


					-- add records in case_departments for the new department
					INSERT INTO case_departments (case_id, department_id, start_date, end_date, create_date, change_date, change_user, create_user, do_not_move, do_not_move_by, next_move_on, next_move_to)
					SELECT @CurrentCaseID, @NewUserDepartment, @today, NULL, GETUTCDATE(), GETUTCDATE(), @change_user, @change_user, NULL, NULL, NULL, NULL

					SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

				END
		END



	SET @currentID = @currentID + 1
END

COMMIT TRANSACTION
GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)

Done:
	


END

GO
