SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Rick Pina
-- Create Date: 2021-02-19
-- Description: Updates Latest record then inserts new one
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsUserStatus]
(
	@user_email					NVARCHAR (100),
	@presence_status			INT
)
AS

SET NOCOUNT ON

DECLARE @user_id INT
SELECT @user_id = user_id FROM users WHERE email = @user_email

UPDATE user_statuses SET end_date = GETUTCDATE(), change_date = GETUTCDATE(), change_user = @user_email WHERE user_id = @user_id AND end_date IS NULL

INSERT INTO user_statuses(user_id, status_id, start_date, end_date, create_date, change_date, change_user, create_user)
Values (@user_id, @presence_status, GETUTCDATE(), null, GETUTCDATE(), GETUTCDATE(), @user_email, @user_email)

UPDATE users SET presence_status = @presence_status, change_date = GETUTCDATE(), change_user = @user_email
WHERE email = @user_email


GO
