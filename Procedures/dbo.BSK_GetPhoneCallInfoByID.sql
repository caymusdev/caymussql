SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Rick Pina
-- Create Date: 2021-02-01
-- Description: 
-- =============================================
CREATE PROCEDURE dbo.BSK_GetPhoneCallInfoByID
(
	@record_id			VARCHAR (100)	
)
AS
BEGIN
SET NOCOUNT ON

SELECT a.affiliate_name, pc.call_duration, pc.call_to, pc.call_from, pc.start_time, pc.end_time
FROM contact_log cl
INNER JOIN affiliates a WITH (NOLOCK) ON cl.affiliate_id = a.affiliate_id 
INNER JOIN phone_calls pc WITH (NOLOCK) ON cl.record_id = pc.phone_call_id
WHERE record_id = @record_id 

END
GO
