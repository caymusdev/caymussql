SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-05-02
-- Description:	This SP is run after loading forte fundings and settlement transactions.  
--  Will update settlements to match their fundings and will process any New batches
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdateNewForteSettlements]
AS
BEGIN	
SET NOCOUNT ON;

UPDATE forte_settlements
SET forte_funding_id = f.forte_funding_id, batch_id = f.batch_id
FROM forte_settlements t
INNER JOIN forte_fundings f ON t.funding_id = f.funding_id
INNER JOIN batches b ON f.batch_id = b.batch_id
WHERE b.batch_status = 'New' AND t.forte_funding_id IS NULL 

-- Now need to get a new batch number for any forte settlements that are on their own, not part of a forte funding.  This happens for "upfront" rejects rather than unfundings.
IF EXISTS (SELECT * FROM forte_settlements s
		   WHERE s.forte_funding_id IS NULL AND s.batch_id IS NULL AND s.settle_response_code LIKE 'R%' AND s.settle_date >= CONVERT(DATETIME, dbo.CF_GetSetting('ForteSettlementsAsOf')))
	BEGIN
		DECLARE @today DATE, @newBatchID INT
		SET @today = DATEADD(HOUR, -5, GETUTCDATE())
		EXEC @newBatchID = dbo.CSP_GetNewBatchNumber '', 'API', 'RejectForte', '', @today, 0 
		
		UPDATE forte_settlements
		SET batch_id = @newBatchID, change_user = 'SYSTEM', change_date = GETUTCDATE()
		WHERE forte_funding_id IS NULL AND batch_id IS NULL AND settle_response_code LIKE 'R%' AND settle_date >= CONVERT(DATETIME, dbo.CF_GetSetting('ForteSettlementsAsOf'))
	END


-- Now update batch statuses for just forte batches
UPDATE batches 
SET batch_status = 'Loaded', change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM batches b 
INNER JOIN forte_settlements s ON b.batch_id = s.batch_id
WHERE b.batch_status = 'New' AND b.batch_date >= CONVERT(DATETIME, dbo.CF_GetSetting('ForteSettlementsAsOf'))

-- any previous batches can be set to Old
UPDATE batches 
SET batch_status = 'Duplicate-old', change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM batches b 
INNER JOIN forte_settlements s ON b.batch_id = s.batch_id
WHERE b.batch_status = 'New' AND b.batch_date < CONVERT(DATETIME, dbo.CF_GetSetting('ForteSettlementsAsOf'))

END
GO
