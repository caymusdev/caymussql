SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Rick Pina
-- Create Date: 2020-10-28
-- Description: Gets Email Conversation/Email Reply Chain
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetEmailConversation]
(
	@email_id				NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON

SELECT e.email_id, u.email, e.subject, e.body, e.create_date, e.message_id
FROM emails e WITH (NOLOCK)
INNER JOIN users u WITH (NOLOCK) ON e.sender_id = u.[user_id]
Where e.message_id = (SELECT message_id FROM emails WHERE email_id = @email_id)
ORDER BY e.create_date DESC;

END
GO
