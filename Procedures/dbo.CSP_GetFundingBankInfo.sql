SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-10-16
-- Description:	Gets the bank information for a funding, to be used when creating a payment schedule
-- =============================================
CREATE PROCEDURE CSP_GetFundingBankInfo
(
	@funding_id			BIGINT
)
AS
BEGIN	
SET NOCOUNT ON;

SELECT f.receivables_bank_name, f.receivables_account_nbr, f.receivables_aba
FROM fundings f
WHERE f.id = @funding_id

END
GO
