SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-08-01
-- Description:	Called after getting the current status and current collector from the Collect! system
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdCollectorFromCollect]
(
	@affiliate_id				BIGINT,
	@collector					VARCHAR (50),
	@status						VARCHAR (100),
	@modified_date				DATE
)
AS
BEGIN	
SET NOCOUNT ON;


UPDATE affiliates SET collect_status = @status, change_date = GETUTCDATE(), change_user = 'SYSTEM'
WHERE affiliate_id = @affiliate_id AND COALESCE(collect_status, '') != @status

-- now see if funding_collections needs updated

-- currently, hardcoded to NIC for customer service and NIC for collections.  Can't go from collections to customer service
-- and since Portfolio starts it by sending NIC, if @collector = NIC then no need to do anything because the record will already be there.
IF @collector != 'NIC'
	BEGIN
		-- this means the collector is Charles.  In this case, if there is NOT already a Collections record, then create one.
		IF NOT EXISTS (SELECT 1 
						FROM funding_collections fc
						INNER JOIN fundings f ON fc.funding_id = f.id
						WHERE f.affiliate_id = @affiliate_id AND fc.collection_type = 'Collections')
			BEGIN
				-- NO COLLECTIONS RECORD SO JUST INSERT.  BUT THERE SHOULD BE A Customer_service or else it should not have been possible to get here.
				INSERT INTO funding_collections(funding_id, collection_type, start_date, end_date, assigned_collector, change_user, change_date)	
				SELECT fc.funding_id, 'Collections', @modified_date, NULL, 'vaughnm@caymusfunding.com', 'SYSTEM', GETUTCDATE()
				FROM funding_collections fc
				INNER JOIN fundings f ON fc.funding_id = f.id
				WHERE f.affiliate_id = @affiliate_id AND fc.collection_type = 'Customer_Service'

				-- Now update the Customer service ones to have an end date
				UPDATE fc
				SET end_date = DATEADD(DD, -1, @modified_date), change_user = 'SYSTEM', change_date = GETUTCDATE()
				FROM funding_collections fc 
				INNER JOIN fundings f ON fc.funding_id = f.id
				WHERE f.affiliate_id = @affiliate_id AND fc.collection_type = 'Customer_Service' AND end_date IS NULL

				UPDATE f
				SET collection_type = 'Collections', change_user = 'SYSTEM', change_date = GETUTCDATE()
				FROM fundings f
				WHERE affiliate_id = @affiliate_id AND collection_type = 'Customer_Service'
			END 
		ELSE
			BEGIN
				-- this means that a Collections record already exists
				PRINT 1 -- don't think we need to do anything, since it already exists.
			END
	END

END
GO
