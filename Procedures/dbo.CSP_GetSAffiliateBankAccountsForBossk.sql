SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-12-08
-- Description:	Gets a list of merchant bank accounts to send to bossk
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetSAffiliateBankAccountsForBossk]

AS
BEGIN
SET NOCOUNT ON;


SELECT DISTINCT mba.merchant_bank_account_id AS affiliate_bank_account_id, f.affiliate_id, f.merchant_id, mba.bank_name, mba.routing_number, mba.account_number,
	mba.account_status,  COALESCE(mba.nickname, COALESCE(mba.bank_name, '') + ': ' + COALESCE(mba.routing_number, '') + ' / ' + COALESCE(mba.account_number, '')) AS nickname,
	CONVERT(BIT, CASE WHEN EXISTS (SELECT 1 FROM fundings f2 WHERE f2.merchant_id = mba.merchant_id AND f2.receivables_aba = mba.routing_number AND f2.receivables_account_nbr = mba.account_number 
		AND f2.payoff_amount > 0) THEN 1 ELSE 0 END) AS is_main_account
FROM fundings f
INNER JOIN affiliates a ON f.affiliate_id = a.affiliate_id
INNER JOIN merchant_bank_accounts mba ON f.merchant_id = mba.merchant_id
WHERE ((COALESCE(f.collection_type, '') <> '' AND f.payoff_amount > 0)
	OR (a.sent_to_collect IS NOT NULL AND a.closed_in_collections IS NULL))
ORDER BY mba.merchant_bank_account_id
	
END
GO
