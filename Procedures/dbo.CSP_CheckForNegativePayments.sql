SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-12-02
-- Description:	Checks for negative payments in queue.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_CheckForNegativePayments]

AS
BEGIN
SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#tempFundings') IS NOT NULL DROP TABLE #tempFundings
CREATE TABLE #tempFundings (payment_id INT NOT NULL, amount MONEY, trans_date DATE, bank_name NVARCHAR (100), account_number VARCHAR (50), routing_number VARCHAR (50), legal_name NVARCHAR (100), 
	contract_number VARCHAR(50), funding_id BIGINT, payoff_amount MONEY, float_amount MONEY, processor VARCHAR (50), retry_level INT, retry_sublevel INT, is_fee BIT, is_refund BIT,
	is_debit BIT, redirect_contract_number NVARCHAR (50), portfolio VARCHAR (50))

INSERT INTO #tempFundings
EXEC CSP_GetPaymentsToSendACH NULL

SELECT COUNT(*) AS NegativeCount FROM #tempFundings WHERE amount <= 0


IF OBJECT_ID('tempdb..#tempFundings') IS NOT NULL DROP TABLE #tempFundings
END

GO
