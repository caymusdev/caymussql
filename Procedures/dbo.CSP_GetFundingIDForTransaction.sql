SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-06-08
-- Description:	Contains the logic for determining what funding to apply something to
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetFundingIDForTransaction]
(
	@trans_date			DATETIME,
	@contract_number	NVARCHAR (100),
	@mid				NVARCHAR (100),
	@merchant_name		NVARCHAR (200)
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @funding_id INT, @merchant_id INT, @affiliate_id INT

IF COALESCE(@contract_number, '') <> ''
	BEGIN
		SELECT @funding_id = f.id 
		FROM fundings f
		WHERE f.contract_number = @contract_number
		
		-- for now, this is all we care about if we find the specific funding id
		GOTO LookForApplyTo
	END

-- if a MID is passed in, try to find the merchant for it so we can find funding(s)
IF COALESCE(@mid, '') <> '' AND @merchant_id IS NULL
	BEGIN
		SELECT @merchant_id = m.merchant_id, @affiliate_id = m.affiliate_id 
		FROM merchants m
		WHERE m.MID = @mid
	END

IF COALESCE(@merchant_name, '') <> '' AND @merchant_id IS NULL  -- only do this if merchant id was not already found
	BEGIN
		SELECT @merchant_id = m.merchant_id, @affiliate_id = m.affiliate_id 
		FROM merchants m
		WHERE m.merchant_name = @merchant_name
	END

-- if the @merchant_id is still null then we return null funding id -- not necessary but for clarity
IF @merchant_id IS NULL
	BEGIN
		SELECT @merchant_id = NULL; 
	END
ELSE
	BEGIN
		SELECT TOP 1 @funding_id = id 
		FROM fundings
		WHERE merchant_id = @merchant_id AND contract_status IN (1,4)
		ORDER BY contract_status, 
			CASE WHEN payoff_amount < 0 THEN 1 ELSE 0 END, -- if one funding is negative, we want to find the one that still owes money
			funded_date ASC -- apply to the oldest active first
		
		GOTO LookForApplyTo
	END

LookForApplyTo:

DECLARE @apply_to_date DATETIME, @apply_to_funding_id NVARCHAR (100)
SELECT @apply_to_date = apply_to_date, @apply_to_funding_id = f.apply_to_contract
FROM fundings f 
WHERE f.id = @funding_id

IF @trans_date >= COALESCE(@apply_to_date, '9999-01-01')
	BEGIN
		-- change the funding id to be the apply to one
		SELECT @funding_id = id 
		FROM fundings f 
		WHERE f.funding_id = @apply_to_funding_id
	END


DONE:
RETURN COALESCE(@funding_id, -1)
END
GO
