SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2020-09-17
-- Description: Deletes a new role
-- =============================================
CREATE PROCEDURE [dbo].[BSK_DelRole]
(
	@role_id			INT,
	@change_user		NVARCHAR (100)
)
AS
BEGIN
    SET NOCOUNT ON
	DECLARE @ErrorMessage NVARCHAR (MAX)
	IF (SELECT system_role FROM roles WHERE role_id = @role_id) = 1
		BEGIN
			SELECT @ErrorMessage = dbo.BF_TranslateWord('NoDeleteSystemRole')
			RAISERROR (@ErrorMessage, 16, 1)	
		END
	ELSE IF EXISTS (SELECT 1 FROM user_roles WHERE role_id = @role_id)
		BEGIN
			SELECT @ErrorMessage = dbo.BF_TranslateWord('RoleHasUsers')
			RAISERROR (@ErrorMessage, 16, 1)
		END
	ELSE 
		BEGIN
			-- for audit trigger
			UPDATE roles SET change_date = GETUTCDATE(), change_user = GETUTCDATE() WHERE role_id = @role_id
			UPDATE role_permissions SET change_date = GETUTCDATE(), change_user = GETUTCDATE() WHERE role_id = @role_id
			
			DELETE FROM role_permissions WHERE role_id = @role_id
			DELETE FROM roles WHERE role_id = @role_id
		END

END
GO
