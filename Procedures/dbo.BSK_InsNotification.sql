SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-27
-- Description:	Inserts a new notification record
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsNotification]
(
	@user_id					INT,
	@user_email					NVARCHAR (100), -- either pass in the user id or the email
	@affiliate_id				INT, 
	@merchant_id				INT,
	@contract_id				INT,
	@notification_message		NVARCHAR (MAX),
	@notification_type			VARCHAR (50),
	@change_user				NVARCHAR (100),
	@record_id					INT = NULL
)
AS
BEGIN
SET NOCOUNT ON;

IF @user_id IS NULL
	BEGIN
		SELECT @user_id = user_id FROM users WHERE email = @user_email
	END

INSERT INTO notifications (user_id, affiliate_id, merchant_id, contract_id, notification_message, notification_type_id,
	displayed_time, acted_on_time, create_date, change_date, change_user, create_user, record_id)
SELECT @user_id, @affiliate_id, @merchant_id, @contract_id, @notification_message, t.notification_type_id, NULL, 
	NULL, GETUTCDATE(), GETUTCDATE(), @change_user, @change_user, @record_id
FROM notification_types t
WHERE t.notification_type = @notification_type



END

GO
