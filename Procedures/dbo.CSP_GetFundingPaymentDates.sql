SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-01-29
-- Description:	Gets the funding payments dates for a specific funding and date range
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetFundingPaymentDates]
(
	@funding_id				BIGINT,
	@start_date				DATETIME = NULL, 
	@end_date				DATETIME = NULL,
	--@session_id				NVARCHAR (100), 
	@userid					NVARCHAR (100)
)
AS
BEGIN	
SET NOCOUNT ON;

---- first time for this session we'll dump ALL funding payment dates for the funding into it, ignoring the start and end
--IF NOT EXISTS (SELECT 1 FROM funding_payment_dates_work WHERE userid = @userid AND funding_id = @funding_id)
--	BEGIN
--		INSERT INTO funding_payment_dates_work (userid, funding_payment_id, payment_date, amount, create_date, active, merchant_bank_account_id, payment_method,
--			payment_processor, payment_schedule_change_id, comments, change_reason_id, change_date, change_user, approved, changed, funding_id)
--		SELECT @userid, pd.id, pd.payment_date, pd.amount, GETUTCDATE(), pd.active, pd.merchant_bank_account_id, pd.payment_method, pd.payment_processor, 
--			NULL AS payment_schedule_change_id, pd.comments, pd.change_reason_id, GETUTCDATE(), @userid, 0, 0, @funding_id
--		FROM funding_payment_dates pd WITH (NOLOCK)
--		WHERE pd.funding_id = @funding_id
--		ORDER BY pd.payment_date
--	END

------------------********************** Need to rewrite this and fix a couple of issues.
/*
-- insert any missing payment dates.
-- if any have been approved since they started working here they'll need to submit their changes first, which will wipe out any unchanged records
INSERT INTO funding_payment_dates_work (userid, funding_payment_id, payment_date, amount, create_date, active, merchant_bank_account_id, payment_method,
	payment_processor, payment_schedule_change_id, comments, change_reason_id, change_date, change_user, approved, changed, funding_id, payment_plan_id, missed)
SELECT @userid, pd.id, pd.payment_date, pd.amount, GETUTCDATE(), pd.active, pd.merchant_bank_account_id, pd.payment_method, pd.payment_processor, 
	NULL AS payment_schedule_change_id, pd.comments, pd.change_reason_id, GETUTCDATE(), @userid, 0, 0, @funding_id, pd.payment_plan_id, COALESCE(pd.missed, 0)
FROM funding_payment_dates pd WITH (NOLOCK)  
WHERE NOT EXISTS (
	SELECT 1
	FROM funding_payment_dates_work w
	WHERE (w.payment_date = pd.payment_date AND w.funding_id = pd.funding_id AND w.userid = @userid AND w.amount = pd.amount)  --AND COALESCE(w.approved, 0) = 0 AND w.reject_reason IS NULL
	OR (w.funding_payment_id = pd.id AND w.userid = @userid ) -- in case a payment has changed, don't reload.  problem's will occur if multiple people edit the same one, but not sure how to handle that anyway
	-- RKB - 2020-04-29 for some reason approved was commented out.  Don't recall.  However, if you don't include it then a user will see the one they already approved
	-- but the code won't pick up on it for approval, because already approved.  Plus need the history of changes so if they want to edit the same date a second time, 
	-- a new record needs created anyway.
	-- I remember now.  You get duplicates showing up when approved is included.  Because you have the actual one and then a new one for the duplicate.
	-- However, I think we include it here and then exclude below any that are approved.
	 AND COALESCE(w.approved, 0) = 0 
) AND pd.payment_date BETWEEN @start_date AND @end_date
AND pd.funding_id = @funding_id


SELECT pd.id, f.id AS funding_id, f.legal_name, f.contract_number,  COALESCE(pdw.payment_date, pd.payment_date) AS payment_date, 
	COALESCE(pdw.amount, pd.amount) AS amount, COALESCE(pd.rec_amount, 0) AS rec_amount, COALESCE(pdw.active, pd.active) AS active, pd.original, pd.chargeback_amount, 
	COALESCE(pdw.merchant_bank_account_id, pd.merchant_bank_account_id) AS merchant_bank_account_id, mba.bank_name, mba.routing_number, mba.account_number, 
	COALESCE(pdw.payment_method, pd.payment_method) AS payment_method, COALESCE(pdw.payment_processor, pd.payment_processor) AS payment_processor, 
	COALESCE(pdw.comments, pd.comments) AS comments, COALESCE(pdw.change_reason_id, pd.change_reason_id) AS change_reason_id, 
	cr.reason AS [change_reason], pdw.id AS worker_id, COALESCE(pdw.changed, 0) AS changed, pdw.payment_plan_id, COALESCE(pd.missed, 0) AS missed
FROM funding_payment_dates_work pdw WITH (NOLOCK)
LEFT JOIN funding_payment_dates pd WITH (NOLOCK) ON pdw.funding_payment_id = pd.id
INNER JOIN fundings f WITH (NOLOCK) ON COALESCE(pd.funding_id, pdw.funding_id) = f.id
LEFT JOIN merchant_bank_accounts mba ON COALESCE(pd.merchant_bank_account_id, pdw.merchant_bank_account_id) = mba.merchant_bank_account_id
LEFT JOIN payment_schedule_change_reasons cr ON COALESCE(pd.change_reason_id, pdw.change_reason_id) = cr.id
WHERE pdw.userid = @userid AND COALESCE(pd.funding_id, pdw.funding_id) = @funding_id
	AND (pdw.payment_date >= @start_date OR @start_date IS NULL)
	AND (pdw.payment_date <= @end_date OR @end_date IS NULL)
	AND COALESCE(pdw.active, 1) = 1
ORDER BY pdw.payment_date
*/

-----------------
-- 1. Delete any unapproved, unchanged, unrejected
-- 2. Reinsert all unapproved   -- same as before except adding in the approved filter
-- 3. Only select unapproved -- same as before except filter for unapproved


DELETE FROM funding_payment_dates_work 
WHERE COALESCE(changed, 0) = 0 AND COALESCE(approved, 0) = 0 AND COALESCE(ready_for_approval, 0) = 0 AND rejected_date IS NULL
	AND userid = @userid AND payment_date BETWEEN @start_date AND @end_date AND funding_id = @funding_id


INSERT INTO funding_payment_dates_work (userid, funding_payment_id, payment_date, amount, create_date, active, merchant_bank_account_id, payment_method,
	payment_processor, payment_schedule_change_id, comments, change_reason_id, change_date, change_user, approved, changed, funding_id, payment_plan_id, missed)
SELECT @userid, pd.id, pd.payment_date, pd.amount, GETUTCDATE(), pd.active, pd.merchant_bank_account_id, pd.payment_method, pd.payment_processor, 
	NULL AS payment_schedule_change_id, pd.comments, pd.change_reason_id, GETUTCDATE(), @userid, 0, 0, @funding_id, pd.payment_plan_id, COALESCE(pd.missed, 0)
FROM funding_payment_dates pd WITH (NOLOCK)  
WHERE NOT EXISTS (
	SELECT 1
	FROM funding_payment_dates_work w
	WHERE ((w.payment_date = pd.payment_date AND w.funding_id = pd.funding_id AND w.userid = @userid AND w.amount = pd.amount)  --AND COALESCE(w.approved, 0) = 0 AND w.reject_reason IS NULL
	OR (w.funding_payment_id = pd.id AND w.userid = @userid )) -- in case a payment has changed, don't reload.  problem's will occur if multiple people edit the same one, but not sure how to handle that anyway
	-- RKB - 2020-04-29 for some reason approved was commented out.  Don't recall.  However, if you don't include it then a user will see the one they already approved
	-- but the code won't pick up on it for approval, because already approved.  Plus need the history of changes so if they want to edit the same date a second time, 
	-- a new record needs created anyway.
	-- I remember now.  You get duplicates showing up when approved is included.  Because you have the actual one and then a new one for the duplicate.
	-- However, I think we include it here and then exclude below any that are approved.
	  AND (COALESCE(w.approved, 0) = 0 AND w.rejected_date IS NULL)  -- not approved and not rejected
) AND pd.payment_date BETWEEN @start_date AND @end_date
AND pd.funding_id = @funding_id


SELECT pd.id, f.id AS funding_id, f.legal_name, f.contract_number,  COALESCE(pdw.payment_date, pd.payment_date) AS payment_date, 
	COALESCE(pdw.amount, pd.amount) AS amount, COALESCE(pd.rec_amount, 0) AS rec_amount, COALESCE(pdw.active, pd.active) AS active, pd.original, pd.chargeback_amount, 
	COALESCE(pdw.merchant_bank_account_id, pd.merchant_bank_account_id) AS merchant_bank_account_id, mba.bank_name, mba.routing_number, mba.account_number, 
	COALESCE(pdw.payment_method, pd.payment_method) AS payment_method, COALESCE(pdw.payment_processor, pd.payment_processor) AS payment_processor, 
	COALESCE(pdw.comments, pd.comments) AS comments, COALESCE(pdw.change_reason_id, pd.change_reason_id) AS change_reason_id, 
	cr.reason AS [change_reason], pdw.id AS worker_id, COALESCE(pdw.changed, 0) AS changed, pdw.payment_plan_id, COALESCE(pd.missed, 0) AS missed
FROM funding_payment_dates_work pdw WITH (NOLOCK)
LEFT JOIN funding_payment_dates pd WITH (NOLOCK) ON pdw.funding_payment_id = pd.id
INNER JOIN fundings f WITH (NOLOCK) ON COALESCE(pd.funding_id, pdw.funding_id) = f.id
LEFT JOIN merchant_bank_accounts mba ON COALESCE(pd.merchant_bank_account_id, pdw.merchant_bank_account_id) = mba.merchant_bank_account_id
LEFT JOIN payment_schedule_change_reasons cr ON COALESCE(pd.change_reason_id, pdw.change_reason_id) = cr.id
WHERE pdw.userid = @userid AND COALESCE(pd.funding_id, pdw.funding_id) = @funding_id
	AND (pdw.payment_date >= @start_date OR @start_date IS NULL)
	AND (pdw.payment_date <= @end_date OR @end_date IS NULL)
	AND COALESCE(pdw.active, 1) = 1
	AND COALESCE(pdw.approved, 0) = 0 -- approved ones need to be left alone so that they can make changes to ones that have already been approved previously
ORDER BY pdw.payment_date


END
GO
