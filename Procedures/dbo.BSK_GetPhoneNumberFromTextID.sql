SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-02-19
-- Description: Used for showing a conversation of texts
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetPhoneNumberFromTextID]
(
	@text_id		INT

)
AS
BEGIN
SET NOCOUNT ON;

SELECT CASE WHEN t.sent_from_bossk = 1 THEN [to] ELSE [from] END AS phone_number 
FROM texts t WITH (NOLOCK)
WHERE t.text_id = @text_id 

END

GO
