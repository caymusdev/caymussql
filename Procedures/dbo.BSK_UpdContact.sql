SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Rick Pina
-- Create date: 2021-01-06
-- Description:	Updates Contact
-- =============================================
CREATE PROCEDURE [dbo].[BSK_UpdContact]
(
	@contact_id					INT,
	@contact_person_type		INT,
	@active						BIT,
	--@best_contact				BIT,
	@contact_first_name			NVARCHAR (100),
	@contact_last_name			NVARCHAR (100),
	@business_name				NVARCHAR (100),
	@note						NVARCHAR (MAX),
	@email						NVARCHAR(100),
	@change_user				NVARCHAR(100),
	@from_bossk					BIT
)
AS
BEGIN
SET NOCOUNT ON;

UPDATE contacts
SET contact_person_type = @contact_person_type, active = @active, --, best_contact = @best_contact,
	contact_first_name = @contact_first_name, contact_last_name = @contact_last_name, business_name = @business_name, note = @note, email = @email, from_bossk = @from_bossk,
	change_date = GETUTCDATE(), change_user = @change_user
WHERE contact_id = @contact_id

END

GO
