SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-10-10
-- Description:	Updates batch amounts.  May need to be changed later but currently just sums transaction amounts per batch.  
-- This value may not make much sense for mixed batches.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdBatchAmounts]
AS
BEGIN
SET NOCOUNT ON;

UPDATE b
SET batch_amount = x.total_amount, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM batches b
INNER JOIN (
	SELECT SUM(CASE WHEN t.trans_type_id IN (25) THEN -1 * t.trans_amount ELSE t.trans_amount END) AS total_amount, b.batch_id
	FROM batches b WITH (NOLOCK)
	INNER JOIN transactions t WITH (NOLOCK) ON b.batch_id = t.batch_id
	WHERE b.batch_amount IS NULL AND t.redistributed = 0
	GROUP BY b.batch_id
) x ON b.batch_id = x.batch_id
WHERE b.batch_amount IS NULL AND b.batch_status IN('Settled', 'Closed') 

END
GO
