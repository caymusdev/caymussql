SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2020-12-22
-- Description: Returns payment details for a payment plan assuming there is still a payment that is attached to the plan
-- =============================================
CREATE PROCEDURE dbo.[BSK_GetPaymentPlanDetails]
(
	@payment_plan_id			INT
)
AS
BEGIN
SET NOCOUNT ON

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())


SELECT pp.payment_method, pp.payment_amount, ba.merchant_bank_account_id, pp.payment_plan_id,
	CONVERT(VARCHAR (50), ba.merchant_bank_account_id) + '|' + ba.bank_name + '|' + ba.routing_number + '|' + ba.account_number AS selected_bank_account,
	pp.frequency, pp.on_monday, pp.on_tuesday, pp.on_wednesday, pp.on_thursday, pp.on_friday, 
	CASE WHEN pp.start_date <= @today THEN @today ELSE pp.start_date END AS start_date, pp.how_long, pp.num_periods
FROM payment_plans pp
LEFT JOIN merchant_bank_accounts ba ON pp.bank_account_id = ba.merchant_bank_account_id
WHERE pp.payment_plan_id = @payment_plan_id


END

GO
