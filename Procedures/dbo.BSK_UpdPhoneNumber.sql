SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Rick Pina
-- Create date: 2021-01-06
-- Description:	Updates Phone Number
-- =============================================
CREATE PROCEDURE [dbo].[BSK_UpdPhoneNumber]
(
	@phone_type_id					INT,
	@phone_number				VARCHAR(50),
	@change_user							NVARCHAR(100),
	@phone_number_id					INT

)
AS
BEGIN
SET NOCOUNT ON;

UPDATE phone_numbers
SET phone_type_id = @phone_type_id, phone_number = @phone_number, change_date = GETUTCDATE(), change_user = @change_user
WHERE phone_number_id = @phone_number_id

END

GO
