SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-05-18
-- Description:	Gets a list of fundings that ARE on hold and have been placed on hold within the last 30 days
-- An attempt to keep things from falling through the cracks since users were not looking at the fudings on hold page.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetRecentFundingsOnHold]

AS
BEGIN
SET NOCOUNT ON;

SELECT f.id AS funding_id, f.legal_name, f.contract_number, x.changed_to_on_hold
FROM fundings f WITH (NOLOCK)
INNER JOIN (
	SELECT id AS funding_id, CONVERT(DATE, MAX(change_date)) AS changed_to_on_hold
	FROM fundings_audit a WITH (NOLOCK)
	WHERE field = 'on_hold' AND COALESCE(changed_to_value, '0') = '1'
		AND DATEDIFF(DD, change_date, GETUTCDATE()) <= 30
	GROUP BY id
) x ON f.id = x.funding_id
WHERE DATEDIFF(DD, x.changed_to_on_hold, GETUTCDATE()) <= 30
	AND f.contract_status IN (1,4) AND COALESCE(f.contract_substatus_id, -1) <> 2 AND f.on_hold = 1
	AND f.payoff_amount > 0
ORDER BY f.legal_name

END
GO
