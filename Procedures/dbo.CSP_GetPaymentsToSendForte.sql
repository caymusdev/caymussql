SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-06-28
-- Description:	Gets a list of payments that need to be sent to Forte
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetPaymentsToSendForte]

AS
BEGIN

SET NOCOUNT ON;
DECLARE @today DATETIME; SET @today = CONVERT(DATE, DATEADD(HH, -5, GETUTCDATE()))
DECLARE @weekDay INT; SET @weekDay = DATEPART(WEEKDAY, @today)

SELECT p.payment_id, p.amount, p.trans_date, ps.bank_name, ps.account_number, ps.routing_number, f.legal_name, f.contract_number,
	f.id AS funding_id, f.payoff_amount, f.float_amount, f.processor,COALESCE(p.retry_level, 0) AS retry_level, COALESCE(p.retry_sublevel, 0) AS retry_sublevel, COALESCE(p.is_fee, 0) AS is_fee
FROM payments p
INNER JOIN payment_schedules ps ON p.payment_schedule_id = ps.payment_schedule_id
INNER JOIN funding_payment_schedules fps ON p.payment_schedule_id = fps.payment_schedule_id
INNER JOIN fundings f ON fps.funding_id = f.id
WHERE p.approved_flag = 1 AND trans_date <= DATEADD(D, CASE WHEN @weekDay < 6 THEN 1 WHEN @weekDay = 6 THEN 3 ELSE 2 END, @today) AND processed_date IS NULL
	AND p.amount <> 0 AND ps.payment_processor = 'Forte'
UNION 
-- get ones tied directly to a funding, probably a fee for retrying payments
SELECT p.payment_id, p.amount, p.trans_date, COALESCE(ps.bank_name, f.receivables_bank_name), COALESCE(ps.account_number, f.receivables_account_nbr), 
	COALESCE(ps.routing_number, f.receivables_aba), f.legal_name, f.contract_number,
	f.id AS funding_id, f.payoff_amount, f.float_amount, f.processor, COALESCE(p.retry_level, 0), COALESCE(p.retry_sublevel, 0), COALESCE(p.is_fee, 0)
FROM payments p
INNER JOIN fundings f ON p.funding_id = f.id
INNER JOIN payments p_orig ON p.orig_transaction_id = p_orig.transaction_id
LEFT JOIN payment_schedules ps ON p_orig.payment_schedule_id = ps.payment_schedule_id
WHERE p.approved_flag = 1 AND p.trans_date <= DATEADD(D, CASE WHEN @weekDay < 6 THEN 1 WHEN @weekDay = 6 THEN 3 ELSE 2 END, @today) AND p.processed_date IS NULL
	AND p.amount <> 0 AND COALESCE(ps.payment_processor, f.processor) = 'Forte'
	AND p.payment_schedule_id IS NULL
UNION 
-- need to get ones tied directly to a funding but the orig_transaction is not in our db so try to get active payment schedule
SELECT p.payment_id, p.amount, p.trans_date, COALESCE(ps.bank_name, f.receivables_bank_name), 
	COALESCE(ps.account_number, f.receivables_account_nbr), COALESCE(ps.routing_number, f.receivables_aba), f.legal_name, f.contract_number,
	f.id AS funding_id, f.payoff_amount, f.float_amount, f.processor, COALESCE(p.retry_level, 0), COALESCE(p.retry_sublevel, 0), COALESCE(p.is_fee, 0)
FROM payments p
INNER JOIN fundings f ON p.funding_id = f.id
LEFT JOIN payments p_orig ON p.orig_transaction_id = p_orig.transaction_id
LEFT JOIN payment_schedules ps ON ps.payment_schedule_id = (
	SELECT MAX(ps.payment_schedule_id) AS payment_schedule_id
	FROM funding_payment_schedules fps
	INNER JOIN payment_schedules ps ON fps.payment_schedule_id = ps.payment_schedule_id
	WHERE ps.active = 1 AND fps.funding_id = f.id
)
WHERE p.approved_flag = 1 AND p.trans_date <= DATEADD(D, CASE WHEN @weekDay < 6 THEN 1 WHEN @weekDay = 6 THEN 3 ELSE 2 END, @today) AND p.processed_date IS NULL
	AND p.amount <> 0 AND COALESCE(ps.payment_processor, f.processor) = 'Forte'
	AND p.payment_schedule_id IS NULL
	AND p_orig.payment_id IS NULL -- the origination transaction was not sent as a payment from our system (mostly at time of migration)
ORDER BY f.legal_name, COALESCE(p.is_fee, 0), COALESCE(p.retry_level, 0), COALESCE(p.retry_sublevel, 0)
END
GO
