SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[CSP_InsPaymentScheduleForFunding] 
(
	@funding_id			BIGINT,
	@userid				NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON;
-- *****************************************
-- NOTE: This is only used for brand new fundings and takes the payment frequency and info right off of the funding itself
-- *****************************************

DECLARE @frequency NVARCHAR(50), @weekday_payment MONEY, @receivables_bank_name	NVARCHAR (100), @receivables_account_nbr NVARCHAR (100),
	@receivables_aba NVARCHAR (100), @weekly_payment MONEY, @processor VARCHAR (50), @start_date DATETIME

SELECT @frequency = f.frequency, @weekday_payment = f.weekday_payment, @receivables_bank_name = f.receivables_bank_name, 
	@receivables_account_nbr = f.receivables_account_nbr, @receivables_aba = f.receivables_aba, @weekly_payment = f.weekly_payment,
	@processor = f.processor, @start_date = f.first_payment_date
FROM fundings f 
WHERE f.id = @funding_id

DECLARE @payment_method VARCHAR (50); SET @payment_method = 'ACH'
IF @processor NOT IN ('53', 'Forte', 'Regions')
	BEGIN	
		SET @payment_method = 'CC'
	END

-- Since this is called only when fundings are first created we check to see if a payment schedule already exists.  If so, don't do this.
-- This will help prevent double fundings from user error in the fundings queue

IF NOT EXISTS (SELECT 1 FROM funding_payment_schedules WHERE funding_id = @funding_id)
	BEGIN
		IF LOWER(@frequency) = 'weekly'
			BEGIN
				EXEC dbo.CSP_InsPaymentSchedule @payment_method, @frequency, @weekly_payment, @start_date, NULL, @receivables_bank_name, @receivables_aba,
					@receivables_account_nbr, @processor, @funding_id, @userid, null, 1, NULL, NULL, NULL, NULL, NULL
			END
		ELSE IF LOWER(@frequency) = 'daily'
			BEGIN
				EXEC dbo.CSP_InsPaymentSchedule @payment_method, @frequency, @weekday_payment, @start_date, NULL, @receivables_bank_name, @receivables_aba,
					@receivables_account_nbr, @processor, @funding_id, @userid, null, 1, NULL, NULL, NULL, NULL, NULL
			END
	END

END


GO
