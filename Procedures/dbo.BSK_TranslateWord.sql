SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2020-08-27
-- Description: Takes a word as a parameter and returns the translation.  Returns the same word if no translation exists
-- =============================================
CREATE PROCEDURE dbo.BSK_TranslateWord
(
	@word			VARCHAR (100)	
)
AS
BEGIN
SET NOCOUNT ON

DECLARE @translated_word NVARCHAR (MAX)

SELECT @translated_word = w.translated_word
FROM words w WITH (NOLOCK)
WHERE w.word = @word

SELECT COALESCE(@translated_word, @word) AS translated_word


END
GO
