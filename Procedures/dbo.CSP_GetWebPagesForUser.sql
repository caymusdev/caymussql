SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-12-11
-- Description:	Gets a list of webpages the user has access to (used in building the menu items)
-- =============================================
CREATE PROCEDURE dbo.CSP_GetWebPagesForUser
	@userid			NVARCHAR (100)
AS
BEGIN
SET NOCOUNT ON;

IF EXISTS (SELECT 1 FROM sec_user_objects WHERE userid = @userid)
	BEGIN
		-- they have specific security
		SELECT REPLACE(s.sec_object_name, 'Controller', '') AS WebPage
		FROM sec_objects s WITH (NOLOCK)
		INNER JOIN sec_user_objects u WITH (NOLOCK) ON s.sec_object_id = u.sec_object_id
		WHERE u.userid = @userid AND s.sec_object_type = 'page'
		ORDER BY WebPage
	END
ELSE 
	BEGIN
		SELECT REPLACE(s.sec_object_name, 'Controller', '') AS WebPage
		FROM sec_objects s WITH (NOLOCK)
		WHERE s.sec_object_type = 'page'
		ORDER BY WebPage
	END

END
GO
