SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-07-02
-- Description:	Tries to Settle a batch by comparing it to the bank statement.  
-- On Success it will mark the batch as Settled.  On Failure, it wil mark it in Error.  On no Match, it will leave it alone.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_SettleBatches]

AS
BEGIN
SET NOCOUNT ON;

-- before we settle any, we need to make sure there are no errors
EXEC dbo.CSP_ProcessBatchesForErrors

-- ****************** FORTE
-- Do Forte batches first
-- update any that match
UPDATE b
SET settle_date = s.Date, batch_status = 'Settled', status_message = 'Bank Batch: ' + CONVERT(VARCHAR(50), s.BatchID), change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM forte_fundings f 
INNER JOIN boa_staging s ON f.net_amount = s.Amount AND CONVERT(DATE, f.effective_date) = CONVERT(DATE, s.Date) AND s.Description LIKE '%FORTE DES:%'
INNER JOIN batches b ON f.batch_id = b.batch_id
WHERE b.batch_status = 'Processed' AND b.batch_type IN ('Forte', 'ForteAPI')

-- now check for errors.  Dates will match but amounts will be different
UPDATE b
SET settle_date = s.Date, batch_status = 'Error', 
	status_message = 'The amount does not match with the bank. Forte amount: ' + CONVERT(VARCHAR(50), f.net_amount) + ', Bank Amount: ' + CONVERT(VARCHAR(50), s.Amount),
	change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM forte_fundings f 
INNER JOIN boa_staging s ON f.net_amount <> s.Amount AND CONVERT(DATE, f.effective_date) = CONVERT(DATE, s.Date) AND s.Description LIKE '%FORTE DES:%'
INNER JOIN batches b ON f.batch_id = b.batch_id
WHERE b.batch_status = 'Processed' AND b.batch_type IN ('Forte', 'ForteAPI')

-- now update any old ones that still have no match
-- after 10 days we'll mark it in error if still no batch
UPDATE b
SET settle_date = s.Date, batch_status = 'Error', status_message = 'Unable to locate a bank batch to match.', change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM forte_fundings f 
LEFT JOIN boa_staging s ON f.net_amount <> s.Amount AND CONVERT(DATE, f.effective_date) = CONVERT(DATE, s.Date) AND s.Description LIKE '%FORTE DES:%'
INNER JOIN batches b ON f.batch_id = b.batch_id
WHERE b.batch_status = 'Processed' AND b.batch_type IN ('Forte', 'ForteAPI') AND s.boa_staging_id IS NULL AND f.effective_date < (GETDATE() - 10)  

---***************** IPS
-- Now update IPS batches
-- do matches first
UPDATE b
SET settle_date = s.Date, batch_status = 'Settled', status_message = 'Bank Batch: ' + CONVERT(VARCHAR(50), s.BatchID), change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM (
	SELECT CONVERT(MONEY, SUM(hold_amt)) AS hold_amt, BatchID, file_date
	FROM ips_staging
	GROUP BY BatchID, file_date
) x
INNER JOIN boa_staging s ON x.hold_amt = s.Amount AND s.Date BETWEEN x.file_date AND DATEADD(dd, 7, x.file_date) 
	AND (s.Description LIKE '%IPS ACH PROGRAM%' OR s.Description LIKE '%CO ID:1999999991%')
INNER JOIN batches b ON x.BatchID = b.batch_id
WHERE b.batch_status = 'Processed' AND b.batch_type LIKE '%IPS%'

-- now update any old ones that still have no match
-- after 10 days we'll mark it in error if still no batch
UPDATE b
SET settle_date = s.Date, batch_status = 'Error', status_message = 'Unable to locate a bank batch to match.', change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM (
	SELECT CONVERT(MONEY, SUM(hold_amt)) AS hold_amt, BatchID, file_date
	FROM ips_staging
	GROUP BY BatchID, file_date
) x
LEFT JOIN boa_staging s ON x.hold_amt = s.Amount AND s.Date BETWEEN x.file_date AND DATEADD(dd, 7, x.file_date) 
	AND (s.Description LIKE '%IPS ACH PROGRAM%' OR s.Description LIKE '%CO ID:1999999991%')
INNER JOIN batches b ON x.BatchID = b.batch_id
WHERE b.batch_status = 'Processed' AND b.batch_type LIKE '%IPS%' AND s.boa_staging_id IS NULL AND x.file_date  < (GETDATE() - 10)


---***************** EVO/VPS
-- first update each transaction since evo transactions show up separately on bank statements.
-- then, if all evo transactions for a given batch have their bank batch updated, then update the batch itself to be settled
UPDATE s
SET bank_batch_id = bs.BatchID
FROM evo_staging s
INNER JOIN boa_staging bs ON s.[Split Amount] = bs.amount AND bs.Date BETWEEN s.Date AND DATEADD(dd, 7, s.Date)
	AND (bs.Description LIKE '%BKCD PROCESSING DES%' OR bs.Description LIKE '%CO ID:9000478020%')--MERCHANT SERVICE DES%')	
	AND CHARINDEX(RIGHT(s.[A360 MID], 10), bs.Description, 1) > 0
INNER JOIN batches b ON s.BatchID = b.batch_id
WHERE s.bank_batch_id IS NULL AND b.batch_status = 'Processed' AND b.batch_type LIKE '%EVO%' AND s.Date >= '2019-04-12'

-- Now update EVO batches
-- do matches first
/*
UPDATE b
SET settle_date = s.Date, batch_status = 'Settled', status_message = 'Bank Batch: ' + CONVERT(VARCHAR(50), s.BatchID)
FROM (
	SELECT SUM([Split Amount]) AS amount, BatchID, Date, [A360 MID]
	FROM evo_staging
	GROUP BY BatchID, Date, [A360 MID]
) x
INNER JOIN boa_staging s ON x.amount = s.Amount AND s.Date BETWEEN x.Date AND DATEADD(dd, 7, x.Date) 
	AND (s.Description LIKE '%BKCD PROCESSING DES%' OR s.Description LIKE '%CO ID:9000478020%')--MERCHANT SERVICE DES%')
	--  AND CHARINDEX(REPLACE(x.[A360 MID], '00000', '0000'), s.Description, 1) > 0  -- EVO support actually recommends using the rightmost 10 digits
	AND CHARINDEX(RIGHT(x.[A360 MID], 10), s.Description, 1) > 0
INNER JOIN batches b ON x.BatchID = b.batch_id
WHERE b.batch_status = 'Processed' AND b.batch_type LIKE '%EVO%'
*/
-- now update batch based on if all evo stagings in a batch have a matching bank batch
UPDATE b
SET settle_date = bs.Date, batch_status = 'Settled', status_message = 'Bank Batch: ' + CONVERT(VARCHAR(50), bs.BatchID), change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM batches b
INNER JOIN evo_staging s ON b.batch_id = s.BatchID
INNER JOIN boa_staging bs ON s.bank_batch_id = bs.BatchID
WHERE b.batch_status = 'Processed' AND b.batch_type LIKE '%EVO%'
	AND NOT EXISTS (SELECT 1 FROM evo_staging s2 WHERE s2.BatchID = b.batch_id AND s2.bank_batch_id IS NULL)

-- now update any old ones that still have no match
-- after 10 days we'll mark it in error if still no batch
UPDATE b
SET settle_date = s.Date, batch_status = 'Error', status_message = 'Unable to locate a bank batch to match.', change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM (
	SELECT SUM([Split Amount]) AS amount, BatchID, Date, [A360 MID]
	FROM evo_staging
	GROUP BY BatchID, Date, [A360 MID]
) x
LEFT JOIN boa_staging s ON x.amount = s.Amount AND s.Date BETWEEN x.Date AND DATEADD(dd, 7, x.Date) 
	AND (s.Description LIKE '%BKCD PROCESSING DES%' OR s.Description LIKE '%CO ID:9000478020%')--MERCHANT SERVICE DES%')
	--AND CHARINDEX(REPLACE(x.[A360 MID], '00000', '0000'), s.Description, 1) > 0  -- EVO support actually recommends using the rightmost 10 digits
	AND CHARINDEX(RIGHT(x.[A360 MID], 10), s.Description, 1) > 0
INNER JOIN batches b ON x.BatchID = b.batch_id
WHERE b.batch_status = 'Processed' AND b.batch_type LIKE '%EVO%' AND s.boa_staging_id IS NULL AND x.Date < (GETDATE() - 10)


-- RKB - 2018-10-08
-- now need to change BOA Upload's that are in Processed to Settled.
-- They get into Processed when they first are settled but the batch has errors and goes into Error mode.  When errors are cleared it gets set back to
-- processed but then this SP never was settling them back.
UPDATE batches SET batch_status = 'Settled', change_user = 'SYSTEM', change_date = GETUTCDATE() WHERE batch_status = 'Processed' AND batch_type = 'BOA Upload'

-- RKB - 2018-10-08
-- Need to manually settle RejectForte batches.  They don't ever hit the bank so once they hit processed, we can settle them
UPDATE batches SET batch_status = 'Settled', settle_date = batch_date, change_user = 'SYSTEM', change_date = GETUTCDATE() WHERE batch_status = 'Processed' AND batch_type = 'RejectForte'

END
GO
