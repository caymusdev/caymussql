SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-02-18
-- Description:	When discounting a renewal the margin is reduced but then the system needs to create a wire to pay off the existing funding.
-- However, it can't use the regular pricing ratio or too much margin will be taken out so first we need to zero out purchase price
-- and anything after that can go to margin
-- =============================================
CREATE PROCEDURE [dbo].[CSP_ApplyPPAndMarginDiscountRenewal]
(
	@funding_id			BIGINT,
	@trans_amount		MONEY,
	@trans_id			INT,
	@comments			NVARCHAR (500)
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
BEGIN TRANSACTION

DECLARE @expected_pp MONEY, @current_pp MONEY, @pp_remaining MONEY

SELECT @expected_pp = f.purchase_price FROM fundings f WHERE f.id = @funding_id

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR

SELECT @current_pp = SUM(d.trans_amount) 
FROM transactions t
INNER JOIN transaction_details d ON t.trans_id = d.trans_id
WHERE t.funding_id = @funding_id AND d.trans_type_id = 7 AND COALESCE(t.redistributed, 0) = 0

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR

/*
@expected_pp - @current_pp needs to be inserted as purchase price.  This is @pp_remaining
@trans_amount - (@expected_pp - @current_pp) needs to be inserted as margin
*/
SET @pp_remaining = @expected_pp - COALESCE(@current_pp, 0)

IF @trans_amount < @pp_remaining 
	BEGIN
		-- this is a case where underwriting put in a payoff amount less than the remaining purchase price, which should never happen
		-- if it does, we'll just create an alert
		EXEC dbo.CSP_InsAlert 'Funding', 'DiscountRenewal', @funding_id, 'Payoff amount from Solo is less than the remaining Purchase Price.' 
	END
ELSE
	BEGIN
		-- do all purchase price first and any left goes to margin
		INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
		VALUES (@trans_id, 7, @pp_remaining, @comments, 'system', GETUTCDATE())

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR

		-- now do margin
		INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
		VALUES (@trans_id, 6, @trans_amount - @pp_remaining, @comments, 'system', GETUTCDATE())

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR

	END


COMMIT TRANSACTION
GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)

Done:
END

GO
