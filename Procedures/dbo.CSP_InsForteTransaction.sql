SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-05-21
-- Description:	Will check to see if the transaction is already in the db.  If so, it will be ignored.  
-- Uniqueness is determined by transaction_id, status, and response code
-- =============================================
CREATE PROCEDURE [dbo].[CSP_InsForteTransaction]
(
	@customer_token			NVARCHAR (50),
	@transaction_id			NVARCHAR (100),
	@customer_id			NVARCHAR (50),
	@status					NVARCHAR (50),
	@action					NVARCHAR (50),
	@authorization_amount	MONEY,
	@authorization_code		NVARCHAR (50),
	@entered_by				NVARCHAR (50),
	@received_date			DATETIME,
	@origination_date		DATETIME,
	@company_name			NVARCHAR (50),
	@response_code			NVARCHAR (50),
	@change_user			NVARCHAR (100)
)	
AS
BEGIN
SET NOCOUNT ON;


IF NOT EXISTS(SELECT 1 FROM forte_trans WHERE transaction_id = @transaction_id AND status = @status AND response_code = @response_code)
	BEGIN
		INSERT INTO forte_trans (customer_token, transaction_id, customer_id, status, action, authorization_amount, authorization_code, entered_by, received_date, origination_date, 
			company_name, response_code, create_date, change_date, change_user)
		VALUES (@customer_token, @transaction_id, @customer_id, @status, @action, @authorization_amount, @authorization_code, @entered_by, @received_date, @origination_date, 
			@company_name, @response_code, GETUTCDATE(), GETUTCDATE(), @change_user)
	END

END
GO
