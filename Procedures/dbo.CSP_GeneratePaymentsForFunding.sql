SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-10-16
-- Description:	Creates payments for the most recent schedule for a funding (used in the create payment schedule page)
-- 2018-11-15 - Changing this to allow creating payments for all active schedules on a funding (added @NewestOnly bit = 1)
-- 2020-02-24 - Changing again but now to use the new funding payment dates, called when creating a new funding, so only if the first payment date is tomorrow
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GeneratePaymentsForFunding]
(
	@funding_id				BIGINT,
	@userid					NVARCHAR (100)
)
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SELECT @tomorrow = DATEADD(DD, CASE DATEPART(WEEKDAY, @today) WHEN 6 THEN 3 WHEN 7 THEN 2 ELSE 1 END, @today)

INSERT INTO payments (payment_schedule_id, amount, trans_date, transaction_id, approved_flag, processed_date, change_date, change_user, previous_transaction_id,
	orig_transaction_id, is_fee, retry_level, retry_sublevel, funding_id, complete, batch_id, retry_complete, processor, is_refund, is_debit, deleted, 
	waived, redirect_funding_id, redirect_approval_id, funding_payment_date_id)
SELECT NULL, pd.amount, pd.payment_date, NULL, 1, NULL, GETUTCDATE(), @userid, NULL, NULL, 0, 0, 0, pd.funding_id, NULL, NULL, NULL, NULL, 0, 1, 0, 
	0, NULL, NULL, pd.id
FROM funding_payment_dates pd
WHERE pd.funding_id = @funding_id AND pd.payment_date = @tomorrow
	
END
GO
