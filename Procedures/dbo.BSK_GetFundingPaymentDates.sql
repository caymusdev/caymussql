SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-03-01
-- Description:	Gets the funding payments dates for a specific funding and date range
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetFundingPaymentDates]
(
	@affiliate_id			BIGINT,
	@users_email			NVARCHAR (100)
)
AS
BEGIN	
SET NOCOUNT ON;
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
-----------------
-- 1. Delete any unapproved, unchanged, unrejected
-- 2. Reinsert all unapproved   -- same as before except adding in the approved filter
-- 3. Only select unapproved -- same as before except filter for unapproved


DELETE FROM funding_payment_dates_work 
WHERE COALESCE(changed, 0) = 0 AND COALESCE(approved, 0) = 0 AND COALESCE(ready_for_approval, 0) = 0 AND rejected_date IS NULL
	AND userid = @users_email AND payment_date > @today AND affiliate_id = @affiliate_id


INSERT INTO funding_payment_dates_work (userid, funding_payment_id, payment_date, amount, create_date, active, merchant_bank_account_id, payment_method,
	payment_processor, payment_schedule_change_id, comments, change_reason_id, change_date, change_user, approved, changed, funding_id, payment_plan_id, missed, affiliate_id)
SELECT @users_email, pd.id, pd.payment_date, pd.amount, GETUTCDATE(), pd.active, pd.merchant_bank_account_id, pd.payment_method, pd.payment_processor, 
	NULL AS payment_schedule_change_id, pd.comments, pd.change_reason_id, GETUTCDATE(), @users_email, 0, 0, pd.funding_id, pd.payment_plan_id, COALESCE(pd.missed, 0), f.affiliate_id
FROM funding_payment_dates pd WITH (NOLOCK)
INNER JOIN fundings f ON pd.funding_id = f.id
WHERE NOT EXISTS (
	SELECT 1
	FROM funding_payment_dates_work w
	WHERE ((w.payment_date = pd.payment_date AND w.funding_id = pd.funding_id AND w.userid = @users_email AND w.amount = pd.amount)  --AND COALESCE(w.approved, 0) = 0 AND w.reject_reason IS NULL
	OR (w.funding_payment_id = pd.id AND w.userid = @users_email )) -- in case a payment has changed, don't reload.  problem's will occur if multiple people edit the same one, but not sure how to handle that anyway
	-- RKB - 2020-04-29 for some reason approved was commented out.  Don't recall.  However, if you don't include it then a user will see the one they already approved
	-- but the code won't pick up on it for approval, because already approved.  Plus need the history of changes so if they want to edit the same date a second time, 
	-- a new record needs created anyway.
	-- I remember now.  You get duplicates showing up when approved is included.  Because you have the actual one and then a new one for the duplicate.
	-- However, I think we include it here and then exclude below any that are approved.
	  AND (COALESCE(w.approved, 0) = 0 AND w.rejected_date IS NULL)  -- not approved and not rejected
) AND pd.payment_date > @today
AND pd.affiliate_id = @affiliate_id


END



GO
