SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-11-11
-- Description:	Retrieves all settings related to SMS
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetSMSSettings]
	
AS
BEGIN
SET NOCOUNT ON;

SELECT dbo.BF_GetSetting('SMSFrom') AS SMSFrom, dbo.BF_GetSetting('SMSAPIKey') AS SMSAPIKey, dbo.BF_GetSetting('SMSAPISecret') AS SMSAPISecret, 
	dbo.BF_GetSetting('SMSApplicationID') AS SMSApplicationID, dbo.BF_GetSetting('SMSPrivateKey') AS SMSPrivateKey,
	dbo.BF_GetSetting('TwilioAccountSID') AS TwilioAccountSID, dbo.BF_GetSetting('TwilioAuthToken') AS TwilioAuthToken,
	dbo.BF_GetSetting('TwilioFromNumber') AS TwilioFromNumber
	
END
GO
