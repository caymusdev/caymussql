SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-09-27
-- Description:	Gets a list of negative fundings to email out as an alert
-- =============================================
CREATE PROCEDURE CSP_GetNegativeFundings
	
AS
BEGIN
SET NOCOUNT ON;

SELECT f.id, f.legal_name, f.contract_number, f.payoff_amount, f.float_amount
FROM alerts a
INNER JOIN fundings f ON a.record_id = f.id
WHERE a.alert_type = 'Funding' AND a.alert_type_2 = 'Negative' AND COALESCE(a.deleted, 0) = 0

END
GO
