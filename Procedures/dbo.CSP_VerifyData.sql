SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-05-22
-- Description:	Used for verifying data.   Internal alerts essentially.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_VerifyData]

AS
BEGIN
SET NOCOUNT ON;

--
--  EXEC dbo.CSP_UpdCalculations 'ryan'
--

SELECT 'ach_draft', f.ach_draft, x.trans_amount, COALESCE(f.ach_draft, 0) - COALESCE(x.trans_amount, 0) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT SUM(trans_amount) AS trans_amount, t.funding_id
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND t.trans_type_id = 18
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.ach_draft, 0) <> COALESCE(x.trans_amount, 0)

--  EXEC dbo.CSP_RedoCalculationsForFunding 1409, 'ryan'

--   SELECT * FROM transactions WHERE funding_id = 1409 AND COALESCE(redistributed, 0) = 0 ORDER BY trans_date DESC


SELECT 'ach_received', f.ach_received, x.trans_amount, COALESCE(f.ach_received, 0) - COALESCE(x.trans_amount, 0) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT SUM(trans_amount) AS trans_amount, t.funding_id
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND t.trans_type_id = 3
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.ach_received, 0) <> COALESCE(x.trans_amount, 0)


SELECT 'ach_reject', f.ach_reject, x.trans_amount, COALESCE(f.ach_reject, 0) - COALESCE(x.trans_amount, 0) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT ABS(SUM(trans_amount)) AS trans_amount, t.funding_id
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND t.trans_type_id = 21
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.ach_reject, 0) <> COALESCE(x.trans_amount, 0)


SELECT 'cash_from_reapplication', f.cash_from_reapplication, x.trans_amount, COALESCE(f.cash_from_reapplication, 0) - COALESCE(x.trans_amount, 0) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT SUM(trans_amount) AS trans_amount, t.funding_id
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND t.trans_type_id = 16 AND t.trans_amount > 0
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.cash_from_reapplication, 0) <> COALESCE(x.trans_amount, 0)


SELECT 'cash_to_reapplication', f.cash_to_reapplication, x.trans_amount, COALESCE(f.cash_to_reapplication, 0) - COALESCE(x.trans_amount, 0) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT ABS(SUM(trans_amount)) AS trans_amount, t.funding_id
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND t.trans_type_id = 16 AND t.trans_amount < 0
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.cash_to_reapplication, 0) <> COALESCE(x.trans_amount, 0)


SELECT 'cash_to_bad_debt', f.cash_to_bad_debt, x.trans_amount, COALESCE(f.cash_to_bad_debt, 0) - COALESCE(x.trans_amount, 0) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT SUM(d.trans_amount) AS trans_amount, t.funding_id
	FROM transactions t WITH (NOLOCK)
	INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND d.trans_type_id = 15
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.cash_to_bad_debt, 0) <> COALESCE(x.trans_amount, 0)


SELECT 'cash_to_chargeback', f.cash_to_chargeback, x.trans_amount, COALESCE(f.cash_to_chargeback, 0) - COALESCE(x.trans_amount, 0) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT ABS(SUM(trans_amount)) AS trans_amount, t.funding_id
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND trans_type_id = 14
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.cash_to_chargeback, 0) <> COALESCE(x.trans_amount, 0)


SELECT 'cash_to_margin', f.cash_to_margin, x.trans_amount, COALESCE(f.cash_to_margin, 0) - COALESCE(x.trans_amount, 0) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT SUM(d.trans_amount) AS trans_amount, t.funding_id
	FROM transactions t WITH (NOLOCK)
	INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND t.trans_type_id IN (1, 2, 3, 4, 5, 14, 16, 18, 19, 21, 22, 24) -- actually received (cash to margin)
		AND d.trans_type_id = 6
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.cash_to_margin, 0) <> COALESCE(x.trans_amount, 0)


SELECT 'cash_to_pp', f.cash_to_pp, x.trans_amount, COALESCE(f.cash_to_pp, 0) - COALESCE(x.trans_amount, 0) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT SUM(d.trans_amount) AS trans_amount, t.funding_id
	FROM transactions t WITH (NOLOCK)
	INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND t.trans_type_id IN (1, 2, 3, 4, 5, 14, 16, 18, 19, 21, 22, 24) -- actually received (cash to margin)
		AND d.trans_type_id = 7
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.cash_to_pp, 0) <> COALESCE(x.trans_amount, 0)


SELECT 'cash_to_rtr', f.cash_to_rtr, f.cash_to_pp, f.cash_to_margin, COALESCE(f.cash_to_rtr, 0) - (COALESCE(f.cash_to_pp, 0) + COALESCE(f.cash_to_margin, 0)) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
WHERE COALESCE(f.cash_to_rtr, 0) <> (COALESCE(f.cash_to_pp, 0) + COALESCE(f.cash_to_margin, 0))


SELECT 'checks_revenue', f.checks_revenue, x.trans_amount, COALESCE(f.checks_revenue, 0) - COALESCE(x.trans_amount, 0) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT SUM(trans_amount) AS trans_amount, t.funding_id
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND trans_type_id = 19
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.checks_revenue, 0) <> COALESCE(x.trans_amount, 0)


SELECT 'continuous_pull', f.continuous_pull, x.trans_amount, COALESCE(f.continuous_pull, 0) - COALESCE(x.trans_amount, 0) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT SUM(trans_amount) AS trans_amount, t.funding_id
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND trans_type_id = 26
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.continuous_pull, 0) <> COALESCE(x.trans_amount, 0)



SELECT 'counter_deposit_revenue', f.counter_deposit_revenue, x.trans_amount, COALESCE(f.counter_deposit_revenue, 0) - COALESCE(x.trans_amount, 0) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT SUM(trans_amount) AS trans_amount, t.funding_id
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND trans_type_id = 2
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.counter_deposit_revenue, 0) <> COALESCE(x.trans_amount, 0)



SELECT 'cc_revenue', f.cc_revenue, x.trans_amount, COALESCE(f.cc_revenue, 0) - COALESCE(x.trans_amount, 0) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT SUM(trans_amount) AS trans_amount, t.funding_id
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND trans_type_id = 4
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.cc_revenue, 0) <> COALESCE(x.trans_amount, 0)



SELECT 'discount_received', f.discount_received, x.trans_amount, COALESCE(f.discount_received, 0) - COALESCE(x.trans_amount, 0) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT SUM(trans_amount) AS trans_amount, t.funding_id
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND trans_type_id = 8
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.discount_received, 0) <> COALESCE(x.trans_amount, 0)


SELECT 'fees_applied', f.fees_applied, x.trans_amount, COALESCE(f.fees_applied, 0) - COALESCE(x.trans_amount, 0) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT SUM(d.trans_amount) AS trans_amount, t.funding_id
	FROM transactions t WITH (NOLOCK)
	INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND d.trans_type_id = 9
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.fees_applied, 0) <> COALESCE(x.trans_amount, 0)



SELECT 'cash_fees', f.cash_fees, x.trans_amount, COALESCE(f.cash_fees, 0) - COALESCE(x.trans_amount, 0) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT SUM(d.trans_amount) AS trans_amount, t.funding_id
	FROM transactions t WITH (NOLOCK)
	INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND d.trans_type_id = 1
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.cash_fees, 0) <> COALESCE(x.trans_amount, 0)


SELECT 'fees_out', f.fees_out, f.fees_applied, f.cash_fees, COALESCE(f.fees_out, 0) - (COALESCE(f.fees_applied, 0) - COALESCE(f.cash_fees, 0)) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
WHERE COALESCE(f.fees_out, 0) <> (COALESCE(f.fees_applied, 0) - COALESCE(f.cash_fees, 0))


SELECT 'first_draft_date', f.first_draft_date, x.trans_date, DATEDIFF(DD, f.first_draft_date, x.trans_date) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT MIN(trans_date) AS trans_date, t.funding_id
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND t.trans_type_id IN (2, 18, 4, 5, 19) AND t.trans_amount <> 0
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE f.first_draft_date IS NOT NULL
	AND f.first_draft_date != x.trans_date


SELECT 'first_received_date', f.first_received_date, x.trans_date, DATEDIFF(DD, f.first_received_date, x.trans_date) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT MIN(trans_date) AS trans_date, t.funding_id
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		and t.trans_type_id IN (2, 3, 4, 5, 19) AND t.trans_amount <> 0 
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE f.first_received_date IS NOT NULL
	AND f.first_received_date != x.trans_date



SELECT 'payoff_amount', f.payoff_amount, f.portfolio_value, f.cash_to_margin, f.cash_to_pp, f.cash_to_bad_debt, f.discount_received, f.settlement_received, f.fees_out, 
	COALESCE(f.payoff_amount, 0) - (COALESCE(f.portfolio_value, 0) - COALESCE(f.cash_to_margin, 0) - COALESCE(f.cash_to_pp, 0)
		- COALESCE(f.cash_to_bad_debt, 0) - COALESCE(f.discount_received, 0) - COALESCE(f.settlement_received, 0) + COALESCE(f.fees_out, 0)) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
WHERE COALESCE(f.payoff_amount, 0) <> (COALESCE(f.portfolio_value, 0) - COALESCE(f.cash_to_margin, 0)- COALESCE(f.cash_to_pp, 0)- COALESCE(f.cash_to_bad_debt, 0)
	 - COALESCE(f.discount_received, 0)- COALESCE(f.settlement_received, 0) + COALESCE(f.fees_out, 0))



SELECT 'gross_dollars_rec', f.gross_dollars_rec, f.counter_deposit_revenue, f.ach_revenue, f.cc_revenue, f.wires_revenue_no_renewal, f.checks_revenue,  
	COALESCE(f.gross_dollars_rec, 0) - (COALESCE(f.counter_deposit_revenue, 0) + COALESCE(f.ach_revenue, 0) + COALESCE(f.cc_revenue, 0)
		+ COALESCE(f.wires_revenue_no_renewal, 0) + COALESCE(f.checks_revenue, 0)) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
WHERE COALESCE(f.gross_dollars_rec, 0) <> (COALESCE(f.counter_deposit_revenue, 0) + COALESCE(f.ach_revenue, 0) + COALESCE(f.cc_revenue, 0)+ COALESCE(f.wires_revenue_no_renewal, 0)
	 + COALESCE(f.checks_revenue, 0))



SELECT 'gross_revenue', f.gross_revenue, f.orig_fee, f.cash_fees, f.cash_to_margin, f.cash_to_pp, f.cash_to_bad_debt,  
	COALESCE(f.gross_revenue, 0) - (COALESCE(f.orig_fee, 0) + COALESCE(f.cash_fees, 0) + COALESCE(f.cash_to_margin, 0)
		+ COALESCE(f.cash_to_pp, 0) + COALESCE(f.cash_to_bad_debt, 0)) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
WHERE COALESCE(f.gross_revenue, 0) <> (COALESCE(f.orig_fee, 0) + COALESCE(f.cash_fees, 0) + COALESCE(f.cash_to_margin, 0)+ COALESCE(f.cash_to_pp, 0)
	 + COALESCE(f.cash_to_bad_debt, 0))
	 AND f.gross_revenue IS NOT NULL


SELECT 'last_draft_date', f.last_draft_date, x.trans_date, DATEDIFF(DD, f.last_draft_date, x.trans_date) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT MAX(trans_date) AS trans_date, t.funding_id
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND t.trans_type_id IN (2, 18, 4, 5, 19) AND t.trans_amount <> 0
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE f.last_draft_date IS NOT NULL AND x.trans_date IS NOT NULL
	AND f.last_draft_date != x.trans_date
	


SELECT 'last_payment_date', f.last_payment_date, x.trans_date, DATEDIFF(DD, f.last_payment_date, x.trans_date) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT MAX(trans_date) AS trans_date, t.funding_id
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND t.trans_type_id IN (2, 3, 4, 5, 19) AND t.trans_amount <> 0
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE f.last_payment_date IS NOT NULL
	AND f.last_payment_date != x.trans_date
	AND f.contract_status IN (1,3,4) AND COALESCE(f.contract_substatus_id, -1) <> 2


/*  -- Not doing for now because it fails written like this because some fundings have multiple transactions on a given day and last_trans_amount
-- is just a random hit in that case and don't care about it enough for right now.
SELECT 'last_trans_amount', f.last_trans_amount, t.trans_amount, f.last_trans_amount - t.trans_amount AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
INNER JOIN (
	SELECT MAX(trans_date) AS trans_date, t.funding_id  -- this will be last payment date
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND t.trans_type_id IN (2, 3, 4, 5, 19) AND t.trans_amount <> 0
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
INNER JOIN transactions t WITH (NOLOCK) ON x.funding_id = t.funding_id AND t.trans_type_id IN (2, 3, 4, 5, 19) AND t.trans_amount <> 0 AND t.redistributed = 0
	AND t.trans_date = x.trans_date
LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
WHERE COALESCE(f.last_trans_amount, -1) != COALESCE(t.trans_amount, -1)
	AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
	*/


SELECT 'margin_adjustment', f.margin_adjustment, 
	COALESCE(f.margin_adjustment, 0) - (COALESCE(f.discount_received, 0) + COALESCE(f.settlement_received, 0) + COALESCE(f.writeoff_adjustment_received, 0)) AS diff, 
	f.discount_received, f.margin_settlement, f.writeoff_adjustment_received,  f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
WHERE COALESCE(f.margin_adjustment, 0) <> (COALESCE(f.discount_received, 0) + COALESCE(f.margin_settlement, 0) + COALESCE(f.writeoff_adjustment_received, 0))




SELECT 'margin_applied', f.margin_applied, 
	COALESCE(f.margin_applied, 0) - (COALESCE(f.cash_to_margin, 0) + COALESCE(f.margin_adjustment, 0)) AS diff, 
	f.cash_to_margin, f.margin_adjustment, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
WHERE COALESCE(f.margin_applied, 0) <> (COALESCE(f.cash_to_margin, 0) + COALESCE(f.margin_adjustment, 0))


SELECT 'margin_out', f.margin_out, 
	COALESCE(f.margin_out, 0) - (COALESCE(f.unearned_income, 0) - COALESCE(f.margin_applied, 0)) AS diff, 
	f.unearned_income, f.margin_applied, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
WHERE COALESCE(f.margin_out, 0) <> (COALESCE(f.unearned_income, 0) - COALESCE(f.margin_applied, 0))
	AND f.margin_out IS NOT NULL


SELECT 'net_revenue', f.net_revenue, 
	COALESCE(f.net_revenue, 0) - (COALESCE(f.cash_to_margin, 0) + COALESCE(f.net_other_income, 0)) AS diff, 
	f.cash_to_margin, f.net_other_income, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
WHERE COALESCE(f.net_revenue, 0) <> (COALESCE(f.cash_to_margin, 0) + COALESCE(f.net_other_income, 0))


SELECT 'paid_off_amount', f.paid_off_amount, 
	COALESCE(f.paid_off_amount, 0) - (COALESCE(f.portfolio_value, 0) - COALESCE(f.rtr_out, 0)) AS diff, 
	f.portfolio_value, f.rtr_out, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
WHERE COALESCE(f.paid_off_amount, 0) <> (COALESCE(f.portfolio_value, 0) - COALESCE(f.rtr_out, f.portfolio_value, 0))



SELECT 'percent_paid', f.percent_paid, 
	COALESCE(f.percent_paid, 0) - (COALESCE(f.cash_to_rtr, 0) / COALESCE(f.portfolio_value, 0)* 100.00) AS diff, 
	f.cash_to_rtr, f.portfolio_value, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
WHERE COALESCE(f.percent_paid, 0) <> (COALESCE(f.cash_to_rtr, 0) / COALESCE(f.portfolio_value, 0)* 100.00)


SELECT 'percent_performance', f.percent_performance, 
	COALESCE(f.percent_performance, 0) - ((COALESCE(f.estimated_turn, 0) - COALESCE(f.actual_turn, 0)) / COALESCE(f.estimated_turn, 0)*100.00) AS diff, 
	f.estimated_turn, f.actual_turn, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
WHERE COALESCE(f.percent_performance, 0) <> CONVERT(NUMERIC(10, 2), ((COALESCE(f.estimated_turn, 0) - COALESCE(f.actual_turn, 0)) / COALESCE(f.estimated_turn, 0)*100.00))
	AND f.actual_turn IS NOT NULL



SELECT 'pp_adjustment', f.pp_adjustment, 
	COALESCE(f.pp_adjustment, 0) - (COALESCE(f.writeoff_received, 0) + COALESCE(f.pp_settlement, 0) ) AS diff, 
	f.writeoff_received, f.pp_settlement,  f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
WHERE COALESCE(f.pp_adjustment, 0) <> (COALESCE(f.writeoff_received, 0) + COALESCE(f.pp_settlement, 0) )


SELECT 'pp_applied', f.pp_applied, 
	COALESCE(f.pp_applied, 0) - (COALESCE(f.pp_adjustment, 0) + COALESCE(f.cash_to_pp, 0) ) AS diff, 
	f.pp_adjustment, f.cash_to_pp,  f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
WHERE COALESCE(f.pp_applied, 0) <> (COALESCE(f.pp_adjustment, 0) + COALESCE(f.cash_to_pp, 0) )


SELECT 'pp_out', f.pp_out, 
	COALESCE(f.pp_out, 0) - (COALESCE(f.purchase_price, 0) - COALESCE(f.pp_applied, 0) ) AS diff, 
	f.purchase_price, f.pp_applied,  f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
WHERE COALESCE(f.pp_out, 0) <> (COALESCE(f.purchase_price, 0) - COALESCE(f.pp_applied, 0) )
	AND f.pp_out IS NOT NULL

SELECT 'rtr_adjustment', f.rtr_adjustment, 
	COALESCE(f.rtr_adjustment, 0) - (COALESCE(f.discount_received, 0) + COALESCE(f.writeoff_received, 0) + 
	-- only add in settlement when it has NOT been written off.  Write off already zeroes out rtr so do not adjust again when there is writeoff
		CASE WHEN COALESCE(f.writeoff_received, 0) > 0 THEN 0 ELSE COALESCE(f.settlement_received, 0) END + 
		COALESCE(f.writeoff_adjustment_received, 0)) AS diff, 
	f.discount_received, f.writeoff_received, f.settlement_received, f.writeoff_adjustment_received, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
WHERE COALESCE(f.rtr_adjustment, 0) <> (COALESCE(f.discount_received, 0) + COALESCE(f.writeoff_received, 0) + 
		-- only add in settlement when it has NOT been written off.  Write off already zeroes out rtr so do not adjust again when there is writeoff
		CASE WHEN COALESCE(f.writeoff_received, 0) > 0 THEN 0 ELSE COALESCE(f.settlement_received, 0) END + 
		COALESCE(f.writeoff_adjustment_received, 0))


SELECT 'rtr_applied', f.rtr_applied, 
	COALESCE(f.rtr_applied, 0) - (COALESCE(f.cash_to_rtr, 0) + COALESCE(f.rtr_adjustment, 0) ) AS diff, 
	f.cash_to_rtr, f.rtr_adjustment, f.settlement_received, f.writeoff_adjustment_received, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
WHERE COALESCE(f.rtr_applied, 0) <> (COALESCE(f.cash_to_rtr, 0) + COALESCE(f.rtr_adjustment, 0))



SELECT 'rtr_out', f.rtr_out, 
	COALESCE(f.rtr_out, 0) - (COALESCE(f.portfolio_value, 0) - COALESCE(f.rtr_applied, 0) ) AS diff, 
	f.portfolio_value, f.rtr_applied, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
WHERE COALESCE(f.rtr_out, f.portfolio_value, 0) <> (COALESCE(f.portfolio_value, 0) - COALESCE(f.rtr_applied, 0))


SELECT 'rtr_out_2', f.rtr_out, 
	COALESCE(f.rtr_out, 0) - (COALESCE(f.pp_out, 0) + COALESCE(f.margin_out, 0) ) AS diff, 
	f.pp_out, f.margin_out, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
WHERE COALESCE(f.rtr_out, 0) <> (COALESCE(f.pp_out, 0) + COALESCE(f.margin_out, 0))


SELECT 'rtr zero but not pp and margin', rtr_out, pp_out, margin_out, contract_status, contract_status, id, legal_name
FROM fundings
WHERE rtr_out = 0 AND (COALESCE(margin_out, 0) <> 0 OR COALESCE(pp_out, 0) <> 0)



SELECT 'settlement_received', f.settlement_received, x.trans_amount, COALESCE(f.cash_fees, 0) - COALESCE(x.trans_amount, 0) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT SUM(t.trans_amount) AS trans_amount, t.funding_id
	FROM transactions t WITH (NOLOCK)
	--INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND t.trans_type_id = 10
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.settlement_received, 0) <> COALESCE(x.trans_amount, 0)


SELECT 'total_fees_collected', f.total_fees_collected, 
	COALESCE(f.total_fees_collected, 0) - (COALESCE(f.orig_fee, 0) + COALESCE(f.cash_fees, 0) ) AS diff, 
	f.orig_fee, f.cash_fees, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
WHERE COALESCE(f.total_fees_collected, 0) <> (COALESCE(f.orig_fee, 0) + COALESCE(f.cash_fees, 0))
	AND f.total_fees_collected IS NOT NULL

SELECT 'total_portfolio_adjustment', f.total_portfolio_adjustment, 
	COALESCE(f.total_portfolio_adjustment, 0) - (COALESCE(f.margin_adjustment, 0) + COALESCE(f.pp_adjustment, 0) ) AS diff, 
	f.margin_adjustment, f.pp_adjustment, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
WHERE COALESCE(f.total_portfolio_adjustment, 0) <> (COALESCE(f.margin_adjustment, 0) + COALESCE(f.pp_adjustment, 0))



SELECT 'wires_revenue', f.wires_revenue, x.trans_amount, COALESCE(f.wires_revenue, 0) - COALESCE(x.trans_amount, 0) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT SUM(trans_amount) AS trans_amount, t.funding_id
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND t.trans_type_id = 5
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.wires_revenue, 0) <> COALESCE(x.trans_amount, 0)


SELECT 'wires_revenue_no_renewal', f.wires_revenue_no_renewal, x.trans_amount, COALESCE(f.ach_draft, 0) - COALESCE(x.trans_amount, 0) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT SUM(trans_amount) AS trans_amount, t.funding_id
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND t.trans_type_id = 5 AND COALESCE(b.batch_type, '') <> 'ContractRenewal'
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.wires_revenue_no_renewal, 0) <> COALESCE(x.trans_amount, 0)


SELECT 'writeoff_adjustment_received', f.writeoff_adjustment_received, x.trans_amount, COALESCE(f.ach_draft, 0) - COALESCE(x.trans_amount, 0) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT SUM(trans_amount) AS trans_amount, t.funding_id
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND t.trans_type_id = 17
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.writeoff_adjustment_received, 0) <> COALESCE(x.trans_amount, 0)



SELECT 'writeoff_received', f.writeoff_received, x.trans_amount, COALESCE(f.ach_draft, 0) - COALESCE(x.trans_amount, 0) AS diff, f.contract_status, f.id, f.legal_name
FROM fundings f WITH (NOLOCK)
LEFT JOIN (
	SELECT SUM(trans_amount) AS trans_amount, t.funding_id
	FROM transactions t WITH (NOLOCK)
	LEFT JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0 AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1)
		AND t.trans_type_id = 11
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE COALESCE(f.writeoff_received, 0) <> COALESCE(x.trans_amount, 0)


SELECT 'closed but payoff not 0', contract_status, payoff_amount, on_hold, *
FROM fundings 
WHERE (contract_status = 2 OR (contract_status = 4 AND contract_substatus_id = 2)) AND payoff_amount <> 0



-- get batches that are still in New even after 7 days
SELECT 'Batches still in New' AS issue, b.batch_id, b.batch_type, b.file_name
FROM batches b 
WHERE b.batch_status IN ('New') AND b.deleted_flag = 0 AND b.batch_date > '2022-06-03'
	AND DATEDIFF(DD, b.batch_date, GETUTCDATE()) > 7
ORDER BY batch_id DESC


END
GO
