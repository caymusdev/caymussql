SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-05-01
-- Description:	Updates a list of affiliates to mark that they have been sent to collections
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdAffiliatesSentToCollect]
(
	@affiliate_ids			VARCHAR (MAX) -- | delimited list of affiliate_ids that were sent to collections
)
AS
BEGIN
SET NOCOUNT ON;

UPDATE a
SET a.sent_to_collect = COALESCE(a.sent_to_collect, GETUTCDATE()), -- if it hasn't been sent yet, then set GETUTCDATE otherwise leasve as it
	a.closed_in_collections = CASE
		WHEN EXISTS (SELECT 1 FROM fundings f WHERE f.payoff_amount > 0 AND f.affiliate_id = a.affiliate_id) THEN a.closed_in_collections
		ELSE GETUTCDATE() END, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM affiliates a
INNER JOIN string_split(@affiliate_ids, '|') x ON a.affiliate_id = x.value


UPDATE f
SET f.sent_to_collect = COALESCE(f.sent_to_collect, GETUTCDATE()), -- if it hasn't been sent yet, then set GETUTCDATE otherwise leasve as it
	f.closed_in_collections = CASE
		WHEN EXISTS (SELECT 1 FROM fundings f2 WHERE f2.payoff_amount > 0 AND f2.affiliate_id = f.affiliate_id) THEN f.closed_in_collections
		ELSE GETUTCDATE() END, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM fundings f
INNER JOIN string_split(@affiliate_ids, '|') x ON f.affiliate_id = x.value

END
GO
