SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-02-22
-- Description: Returns the contact id for a given phone number
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetContactIDFromPhone]
(
	@phone_number		VARCHAR (50)

)
AS
BEGIN
SET NOCOUNT ON;

SELECT contact_id
FROM phone_numbers WITH (NOLOCK)
WHERE phone_number = @phone_number

END

GO
