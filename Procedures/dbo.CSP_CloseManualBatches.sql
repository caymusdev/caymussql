SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-08-24
-- Description:	Closes manual batches - Need to at least wait until next day.  When manual is first entered it will be in float.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_CloseManualBatches]

AS
BEGIN
SET NOCOUNT ON;

-- make sure to get the correct "today's" date since this runs in UTC
DECLARE @today DATE; SELECT @today = CONVERT(DATE, DATEADD(HOUR, -5, GETUTCDATE()))


-- close any manual batches that are not closed that have a batch date before today
UPDATE batches
SET batch_status = 'Closed', settle_date = @today,  --batch_date
	change_user = 'SYSTEM', change_date = GETUTCDATE()
WHERE batch_status = 'Processed' AND batch_date < @today AND manual_flag = 1
END
GO
