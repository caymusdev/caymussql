SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-09-07
-- Description:	Gets the payment journey information for a given payment
-- =============================================
CREATE PROCEDURE CSP_GetPaymentJourney
(
	@payment_id			INT
)
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @payment_ids TABLE(payment_id int); DECLARE @payment_path VARCHAR (500), @retry_sublevel INT 
SELECT @payment_path = payment_path, @retry_sublevel = retry_sublevel FROM cv_funding_audit WHERE payment_id = @payment_id; 
SET @payment_path = REPLACE(@payment_path, ',,', ',')

INSERT INTO @payment_ids(payment_id) 
SELECT a.value 
FROM string_split(@payment_path, ',') a 
WHERE COALESCE(a.value, '') <> ''


SELECT f.id, f.trans_id, f.trans_date, f.trans_amount, f.payment_id, f.funding_id, f.is_fee, f.legal_name, f.orig_payment_id, f.previous_payment_id, 
	f.contract_number, f.balance, f.trans_type, f.ROWID, f.retry_level, f.retry_sublevel, f.retry_level_text, f.response_code, 
	 f.merchant_id, f.merchant_name, f.affiliate_id, f.affiliate_name, f.funded_date, f.fees_out, f.total_fees, f.payment_path, f.trans_path,
	 f.nacha_comments, f.nacha_description, f.result_desc, f.trans_comments
FROM cv_funding_audit f
LEFT JOIN @payment_ids p0 ON f.payment_id = p0.payment_id
WHERE (
	f.payment_path LIKE '%,' + CONVERT(VARCHAR(50), @payment_id) + ',%'
	OR f.payment_id IN (SELECT payment_id FROM @payment_ids)
	-- this next line will include the other retry, but not it's whole path, just the retry
	--OR (f.previous_payment_id IN (SELECT payment_id FROM @payment_ids))
	-- this next line will exclude the other retry involved with this payment's parent if there is one
	OR (f.previous_payment_id IN (SELECT payment_id FROM @payment_ids) AND COALESCE(f.retry_sublevel, @retry_sublevel) = COALESCE(@retry_sublevel, 0))		
	)	
ORDER BY f.trans_date, f.trans_id, f.is_fee

END
GO
