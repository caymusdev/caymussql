SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-01-24
-- Description:	Redoes the funding payments dates from scratch using the payment schedules which will no longer be used.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_RedoFundingPaymentDates]

AS
BEGIN	
SET NOCOUNT ON;



DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
DECLARE @today DATE; SET @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SET @tomorrow = dbo.CF_GetTomorrow(@today)
BEGIN TRANSACTION

-- first let's store off what is currently in funding payment dates, just as a backup in case
IF OBJECT_ID('tempFundingPaymentDates') IS NULL 
	BEGIN
		SELECT *
		INTO tempFundingPaymentDates
		FROM funding_payment_dates
	END

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

-- now clear out what is currently in the table
TRUNCATE TABLE funding_payment_dates
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

DECLARE @tempDate DATE;
DECLARE @funding_id BIGINT, @frequency VARCHAR (50), @payment MONEY, @first_payment_date DATE, @payment_schedule_id INT, @orig_payment_sched_id INT
DECLARE @maxID BIGINT, @tempID BIGINT
DECLARE @merchant_bank_acccount_id INT, @payment_method VARCHAR (50), @payment_processor VARCHAR (50)
DECLARE @payment_schedule_change_id INT, @comments NVARCHAR (4000), @change_reason_id INT, @payment_plan_id INT
DECLARE @remaining_amount MONEY, @tempAmount MONEY, @orig_payment MONEY
DECLARE @rtr MONEY,  @schedule_start_date DATE, @payoff_amount MONEY

IF OBJECT_ID('tempdb..#tempFundings') IS NOT NULL DROP TABLE #tempFundings
CREATE TABLE #tempFundings (id int not null identity (1, 1), funding_id BIGINT, rtr MONEY, frequency VARCHAR (50), payment MONEY, first_payment_date DATE, 
	merchant_bank_acccount_id INT, payment_method VARCHAR (50), payment_processor VARCHAR (50), payoff_amount MONEY)

IF OBJECT_ID('tempdb..#tempSchedules') IS NOT NULL DROP TABLE #tempSchedules
CREATE TABLE #tempSchedules (id int not null identity (1, 1), payment_schedule_id INT, frequency VARCHAR(50), payment MONEY, end_date DATE, first_payment_date DATE, 
	merchant_bank_acccount_id INT, payment_method VARCHAR (50), payment_processor VARCHAR (50), payment_schedule_change_id INT, comments NVARCHAR (4000), change_reason_id INT, 
	payment_plan_id INT)

IF OBJECT_ID('tempdb..#tempScheduleChanges') IS NOT NULL DROP TABLE #tempScheduleChanges  -- the AllChanges temp table will store all and then this temp table will be specific 
-- to a payment schedule id as we're processing.  It's a working table
CREATE TABLE #tempScheduleChanges (id int not null identity (1, 1), payment_schedule_id INT, start_date DATE, end_date DATE, active BIT)

IF OBJECT_ID('tempdb..#tempScheduleAllChanges') IS NOT NULL DROP TABLE #tempScheduleAllChanges -- this table will store the changes for each payment schedule that has more 
	-- than one in audit table
CREATE TABLE #tempScheduleAllChanges (id int not null identity (1, 1), payment_schedule_id INT, start_date DATE, end_date DATE, active BIT)



-- *-**************************************** FIRST, DO FUNDINGS THAT HAVE NO PAYMENT SCHEDULES   ------------**********************************
INSERT INTO #tempFundings (funding_id, rtr, frequency, payment, first_payment_date, merchant_bank_acccount_id, payment_method, payment_processor, payoff_amount)
SELECT f.id, portfolio_value, frequency, CASE frequency WHEN 'Daily' THEN weekday_payment ELSE weekly_payment END, first_payment_date, mba.merchant_bank_account_id, 
	CASE f.hard_offer_type WHEN 'Split' THEN 'CC' ELSE f.hard_offer_type END, f.processor, f.payoff_amount
FROM fundings f
LEFT JOIN merchant_bank_accounts mba ON f.merchant_id = mba.merchant_id AND f.receivables_bank_name = mba.bank_name AND f.receivables_aba = mba.routing_number
	AND f.receivables_account_nbr = mba.account_number
WHERE NOT EXISTS (SELECT 1 FROM funding_payment_schedules fps WHERE f.id = fps.funding_id)
	AND f.frequency IS NOT NULL 
ORDER BY f.id

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

SELECT @tempID = 1, @maxID = MAX(id) FROM #tempFundings
-- loop through fundings with no payment schedule
WHILE @tempID <= @maxID
	BEGIN
		SELECT @funding_id = funding_id,  @rtr = rtr, @frequency = frequency, @payment = payment, @first_payment_date = first_payment_date, 
			@merchant_bank_acccount_id = merchant_bank_acccount_id, @payment_method = payment_method, @payment_processor = payment_processor, 
			@payoff_amount = payoff_amount
		FROM #tempFundings WHERE id = @tempID

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		SET @tempDate = @first_payment_date
				
		IF @payment IS NOT NULL AND @frequency IS NOT NULL
			BEGIN
				SET @remaining_amount = @rtr 
				SET @orig_payment = @payment -- @payment can be changed

				WHILE @remaining_amount > 0 
					BEGIN
						SET @tempAmount = @remaining_amount - @payment
						IF @tempAmount < 0
							BEGIN
								SET @payment = @remaining_amount
							END
						IF @frequency = 'Daily'
							BEGIN
								IF DATEPART(dw, @tempDate) IN (2,3,4,5,6) -- only do Monday Through Friday even though CC might have weekend amounts
									BEGIN
										INSERT INTO funding_payment_dates(funding_id, payment_date, amount, payment_schedule_id, rec_amount, original, active, 
											merchant_bank_account_id, payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, payment_plan_id)
										SELECT @funding_id, @tempDate, @payment, @payment_schedule_id, 0, 1, 1, @merchant_bank_acccount_id, @payment_method, 
											@payment_processor, NULL, NULL, NULL, NULL AS payment_plan_id

										SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

										SET @remaining_amount = @remaining_amount - @payment
									END
							END
						ELSE IF @frequency = 'Weekly'
							BEGIN
								IF DATEPART(dw, @tempDate) = DATEPART(dw, @first_payment_date)
									BEGIN
										-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
										INSERT INTO funding_payment_dates(funding_id, payment_date, amount, payment_schedule_id, rec_amount, original, active, 
											merchant_bank_account_id, payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, payment_plan_id)
										SELECT @funding_id, @tempDate, @payment, @payment_schedule_id, 0, 1, 1, @merchant_bank_acccount_id, @payment_method, 
											@payment_processor, NULL, NULL, NULL, NULL AS payment_plan_id

										SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

										SET @remaining_amount = @remaining_amount - @payment
									END
							END
						ELSE IF @frequency = 'Monthly'
							BEGIN
								IF DAY(@tempDate) = DAY(@first_payment_date)
									BEGIN
										-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
										INSERT INTO funding_payment_dates(funding_id, payment_date, amount, payment_schedule_id, rec_amount, original, active, 
											merchant_bank_account_id, payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, payment_plan_id)
										SELECT @funding_id, @tempDate, @payment, @payment_schedule_id, 0, 1, 1, @merchant_bank_acccount_id, @payment_method, 
											@payment_processor, NULL, NULL, NULL, NULL AS payment_plan_id

										SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

										SET @remaining_amount = @remaining_amount - @payment
									END
							END
						-- increment @tempDate
						SET @tempDate = DATEADD(D, 1, @tempDate)		
						-- reset @payment variable
						SET @payment = @orig_payment						
					END	
			END
		SET @tempID = @tempID + 1
	END
-- end looping through fundings with no payment schedule

TRUNCATE TABLE #tempFundings
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

-- get a list of all fundings that have a frequency and their payment schedules
INSERT INTO #tempFundings (funding_id, rtr, frequency, payment, first_payment_date, payoff_amount)
SELECT f.id, portfolio_value, frequency, CASE frequency WHEN 'Daily' THEN weekday_payment ELSE weekly_payment END, first_payment_date, f.payoff_amount
FROM fundings f
WHERE f.frequency IS NOT NULL AND EXISTS (SELECT 1 FROM funding_payment_schedules fps WHERE f.id = fps.funding_id)
ORDER BY f.id

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

-- store off any payment schedules that have gone back and forth between active and not active
INSERT INTO #tempScheduleAllChanges (payment_schedule_id, start_date, end_date, active)
SELECT DISTINCT x.payment_schedule_id, x.change_date, x.next_change_date, CASE x.changed_to_value WHEN '1' THEN 1 ELSE 0 END
FROM (
	SELECT TOP 100 PERCENT payment_schedule_id, CONVERT(DATE, change_date) AS change_date, LEAD(CONVERT(DATE, change_date)) OVER (ORDER BY payment_schedule_id, audit_id) AS next_change_date, 
		changed_to_value
	FROM payment_schedules_audit 
	WHERE field = 'active'  --AND payment_schedule_id = 1151
	ORDER BY payment_schedule_id, audit_id
) x 
INNER JOIN (
	SELECT payment_schedule_id
	FROM payment_schedules_audit
	WHERE field = 'active'
	GROUP BY payment_schedule_id
	HAVING COUNT(*) > 1
) y ON x.payment_schedule_id = y.payment_schedule_id
ORDER BY x.payment_schedule_id, x.change_date, x.next_change_date, CASE x.changed_to_value WHEN '1' THEN 1 ELSE 0 END

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


SELECT @maxID = MAX(id) FROM #tempFundings
SET @tempID = 1
-- loop through all fundings and create payment dates
WHILE @tempID <= @maxID
	BEGIN
		SELECT @funding_id = funding_id, @rtr = rtr, @frequency = frequency, @payment = payment, @first_payment_date = first_payment_date, @payoff_amount = payoff_amount
		FROM #tempFundings WHERE id = @tempID

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		SELECT @orig_payment_sched_id = MIN(payment_schedule_id)  -- this will be the original payment schedule
		FROM funding_payment_schedules fps 
		WHERE fps.funding_id = @funding_id

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		SET @tempDate = @first_payment_date					
		
		-- First, fill temp table with all payment schedules for this funding
		INSERT INTO #tempSchedules (payment_schedule_id, frequency, payment, end_date, first_payment_date, merchant_bank_acccount_id, payment_method, payment_processor, 
			payment_schedule_change_id, comments, change_reason_id, payment_plan_id)
		SELECT ps.payment_schedule_id, ps.frequency, ps.amount, COALESCE(y.SetToInactiveDate, ps.end_date), COALESCE(x.SetToActiveDate, ps.start_date), mba.merchant_bank_account_id,	
			ps.payment_method, ps.payment_processor, ps.payment_schedule_change_id, ps.comments, ps.change_reason_id, ps.payment_plan_id
		FROM funding_payment_schedules fps
		INNER JOIN payment_schedules ps ON fps.payment_schedule_id = ps.payment_schedule_id
		LEFT JOIN (
			SELECT psa.payment_schedule_id, MAX(psa.change_date) AS SetToActiveDate
			FROM payment_schedules_audit psa 
			WHERE psa.field = 'active' AND changed_to_value = '1'
			GROUP BY psa.payment_schedule_id
		) x ON ps.payment_schedule_id = x.payment_schedule_id
		LEFT JOIN (
			SELECT psa.payment_schedule_id, MAX(psa.change_date) AS SetToInactiveDate
			FROM payment_schedules_audit psa 
			WHERE psa.field = 'active' AND changed_to_value = '0'
			GROUP BY psa.payment_schedule_id
		) y ON ps.payment_schedule_id = y.payment_schedule_id
		INNER JOIN (  -- -- makes sure it was active at some point in time
			SELECT ps.payment_schedule_id, ps.active
			FROM payment_schedules ps
			WHERE ps.active = 1
			UNION
			SELECT psa.payment_schedule_id, 1
			FROM payment_schedules_audit psa
			WHERE psa.field = 'active' -- changed to value does not matter here.  If it is changed to 0 that means it WAS active before
				-- and if it was changed to 1 then it was active also.
		) z ON ps.payment_schedule_id = z.payment_schedule_id
		INNER JOIN fundings f ON fps.funding_id = f.id AND f.id = @funding_id
		LEFT JOIN merchant_bank_accounts mba ON ps.bank_name = mba.bank_name AND ps.routing_number = mba.routing_number AND ps.account_number = mba.account_number
			AND mba.merchant_id = f.merchant_id
		WHERE fps.funding_id = @funding_id 
			--AND (COALESCE(ps.active, 1) = 1 OR x.SetToActiveDate IS NOT NULL) -- if it isn't active now but was then include it
		ORDER BY COALESCE(y.SetToInactiveDate, ps.end_date, '2050-12-31'), ps.payment_schedule_id  -- get any that have no end date at the end

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
 					
		DECLARE @tempScheduleID INT, @maxSchedID INT
		SELECT @tempScheduleID = 1, @maxSchedID = MAX(id)
		FROM #tempSchedules
		DECLARE @runningTotal MONEY; SET @runningTotal = 0

		WHILE @tempScheduleID <= @maxSchedID
			BEGIN			
				DECLARE @end_date DATE;
				SELECT @payment_schedule_id = payment_schedule_id, @frequency = frequency, @payment = payment, @end_date = end_date, @first_payment_date = first_payment_date, 
					@merchant_bank_acccount_id = merchant_bank_acccount_id, @payment_method = payment_method, @payment_processor = payment_processor, 
					@payment_schedule_change_id = payment_schedule_change_id, @comments = comments, @change_reason_id = change_reason_id, @payment_plan_id = payment_plan_id
				FROM #tempSchedules WHERE id = @tempScheduleID
				
				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

				TRUNCATE TABLE #tempScheduleChanges; SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

				---- #tempScheduleChanges will store any payment schedules that went active to inactive several times
				-- as we are inserting the records we can record if the payment was active or not at the time
				INSERT INTO #tempScheduleChanges (payment_schedule_id, start_date, end_date, active)
				SELECT t.payment_schedule_id, t.start_date, t.end_date, t.active
				FROM #tempScheduleAllChanges t
				WHERE t.payment_schedule_id = @payment_schedule_id
				ORDER BY t.start_date, t.end_date, t.active

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

						
				SET @tempDate = @first_payment_date
				DECLARE @active BIT; SET @active = 1								
				
				-- if there is an end date, don't worry about remaining amount, just dump it all in
				IF @end_date IS NOT NULL
					BEGIN
						WHILE @tempDate <= @end_date							
							BEGIN
								IF EXISTS (SELECT 1 FROM #tempScheduleChanges t WHERE t.active = 0 AND @tempDate BETWEEN t.start_date AND t.end_date AND t.payment_schedule_id = @payment_schedule_id)
									BEGIN
										SET @active = 0
									END
								IF @frequency = 'Daily'
									BEGIN
										IF DATEPART(dw, @tempDate) IN (2,3,4,5,6) -- only do Monday Through Friday even though CC might have weekend amounts
											BEGIN
												INSERT INTO funding_payment_dates(funding_id, payment_date, amount, payment_schedule_id, rec_amount, original, active, 
													merchant_bank_account_id, payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, 
													payment_plan_id)
												SELECT @funding_id, @tempDate, @payment, @payment_schedule_id, 0, 
													CASE WHEN @orig_payment_sched_id = @payment_schedule_id THEN 1 ELSE 0 END, @active, @merchant_bank_acccount_id, 
														@payment_method, @payment_processor, @payment_schedule_change_id, @comments, @change_reason_id, @payment_plan_id

												SET @runningTotal = @runningTotal + @payment
											END
									END
								ELSE IF @frequency = 'Weekly'
									BEGIN
										IF DATEPART(dw, @tempDate) = DATEPART(dw, @first_payment_date)
											BEGIN
												-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
												INSERT INTO funding_payment_dates(funding_id, payment_date, amount, payment_schedule_id, rec_amount, original, active, 
													merchant_bank_account_id, payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, 
													payment_plan_id)
												SELECT @funding_id, @tempDate, @payment, @payment_schedule_id, 0, 
													CASE WHEN @orig_payment_sched_id = @payment_schedule_id THEN 1 ELSE 0 END, @active, @merchant_bank_acccount_id, 
														@payment_method, @payment_processor, @payment_schedule_change_id, @comments, @change_reason_id, @payment_plan_id

												SET @runningTotal = @runningTotal + @payment
											END
									END
								ELSE IF @frequency = 'Monthly'
									BEGIN
										IF DAY(@tempDate) = DAY(@first_payment_date)
											BEGIN
												-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
												INSERT INTO funding_payment_dates(funding_id, payment_date, amount, payment_schedule_id, rec_amount, original, active, 
													merchant_bank_account_id, payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, 
													payment_plan_id)
												SELECT @funding_id, @tempDate, @payment, @payment_schedule_id, 0, 
													CASE WHEN @orig_payment_sched_id = @payment_schedule_id THEN 1 ELSE 0 END, @active, @merchant_bank_acccount_id, 
														@payment_method, @payment_processor, @payment_schedule_change_id, @comments, @change_reason_id, @payment_plan_id

												SET @runningTotal = @runningTotal + @payment
											END
									END
								-- increment @tempDate
								SET @tempDate = DATEADD(D, 1, @tempDate)
							END		-- @tempDate <= @end_date
					END  -- IF @end_date IS NOT NULL
				ELSE
					BEGIN
						-- end date is null					
						SET @remaining_amount = @rtr - @runningTotal --- COALESCE(@todaysPayments, 0)
						
						SET @orig_payment = @payment -- @payment can be changed

						WHILE @remaining_amount > 0
							BEGIN
								-- once we hit today we want to start using the payoff amount since we have that as of today
								IF @tempDate = @tomorrow
									BEGIN
										SET @remaining_amount = @payoff_amount
									END
								IF EXISTS (SELECT 1 FROM #tempScheduleChanges t WHERE t.active = 0 AND @tempDate BETWEEN t.start_date AND t.end_date AND t.payment_schedule_id = @payment_schedule_id)
									BEGIN
										SET @active = 0
									END

								SET @tempAmount = @remaining_amount - @payment
								IF @tempAmount < 0
									BEGIN
										SET @payment = @remaining_amount
									END
								IF @frequency = 'Daily'
									BEGIN
										IF DATEPART(dw, @tempDate) IN (2,3,4,5,6) -- only do Monday Through Friday even though CC might have weekend amounts
											BEGIN
												INSERT INTO funding_payment_dates(funding_id, payment_date, amount, payment_schedule_id, rec_amount, original, active, 
													merchant_bank_account_id, payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, 
													payment_plan_id)
												SELECT @funding_id, @tempDate, @payment, @payment_schedule_id, 0, 
													CASE WHEN @orig_payment_sched_id = @payment_schedule_id THEN 1 ELSE 0 END, @active, @merchant_bank_acccount_id, 
														@payment_method, @payment_processor, @payment_schedule_change_id, @comments, @change_reason_id, @payment_plan_id

												SET @remaining_amount = @remaining_amount - @payment
											END
									END
								ELSE IF @frequency = 'Weekly'
									BEGIN
										IF DATEPART(dw, @tempDate) = DATEPART(dw, @first_payment_date)
											BEGIN
												-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
												INSERT INTO funding_payment_dates(funding_id, payment_date, amount, payment_schedule_id, rec_amount, original, active, 
													merchant_bank_account_id, payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, 
													payment_plan_id)
												SELECT @funding_id, @tempDate, @payment, @payment_schedule_id, 0, 
													CASE WHEN @orig_payment_sched_id = @payment_schedule_id THEN 1 ELSE 0 END, @active, @merchant_bank_acccount_id, 
														@payment_method, @payment_processor, @payment_schedule_change_id, @comments, @change_reason_id, @payment_plan_id

												SET @remaining_amount = @remaining_amount - @payment
											END
									END
								ELSE IF @frequency = 'Monthly'
									BEGIN
										IF DAY(@tempDate) = DAY(@first_payment_date)
											BEGIN
												-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
												INSERT INTO funding_payment_dates(funding_id, payment_date, amount, payment_schedule_id, rec_amount, original, active, 
													merchant_bank_account_id, payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id,
													payment_plan_id)
												SELECT @funding_id, @tempDate, @payment, @payment_schedule_id, 0, 
													CASE WHEN @orig_payment_sched_id = @payment_schedule_id THEN 1 ELSE 0 END, @active, @merchant_bank_acccount_id, 
														@payment_method, @payment_processor, @payment_schedule_change_id, @comments, @change_reason_id, @payment_plan_id

												SET @remaining_amount = @remaining_amount - @payment
											END
									END
								-- increment @tempDate
								SET @tempDate = DATEADD(D, 1, @tempDate)		
								-- reset @payment variable
								SET @payment = @orig_payment						
							END	 -- WHILE @remaining_amount > 0
					END	-- -- end date is null
				SET @tempScheduleID  = @tempScheduleID + 1
			END  -- WHILE @tempScheduleID <= @maxSchedID
												

		-- finally truncate temp table
		TRUNCATE TABLE #tempSchedules			

		-- loop to next funding
		SET @tempID = @tempID + 1
	END

-- first update received amount for those payments that DO HAVE a matching received transaction
UPDATE pd
SET rec_amount = COALESCE(t0.trans_amount, 0), rec_trans_id = t0.trans_id, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM funding_payment_dates pd
INNER JOIN payment_schedules ps ON pd.payment_schedule_id = ps.payment_schedule_id
INNER JOIN payments p ON ps.payment_schedule_id = p.payment_schedule_id AND pd.payment_date = p.trans_date
INNER JOIN transactions t0 ON p.transaction_id = t0.transaction_id AND t0.trans_type_id = 
	CASE ps.payment_method 
		WHEN 'Check' THEN 19
		WHEN 'Wire' THEN 5
		WHEN 'CC' THEN 4
		WHEN 'ACH' THEN 3
	END				

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


-- now update received amount for those funding payment dates where there is NOT a matching payment record but IS a matching transcation
-- that does not match a payment as well
UPDATE pd
SET rec_amount = COALESCE(t.trans_amount, t1.trans_amount, 0), rec_trans_id = COALESCE(t.trans_id, t1.trans_id), change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM funding_payment_dates pd
LEFT JOIN payment_schedules ps ON pd.payment_schedule_id = ps.payment_schedule_id
LEFT JOIN transactions t ON pd.payment_date = t.trans_date AND pd.funding_id = t.funding_id
	AND t.trans_type_id = CASE ps.payment_method 
							WHEN 'Check' THEN 19
							WHEN 'Wire' THEN 5
							WHEN 'CC' THEN 4
							WHEN 'ACH' THEN 3
						END
	 AND (
		NULLIF(t.transaction_id, '') IS NULL
			OR NOT EXISTS (SELECT 1 FROM payments p WHERE p.transaction_id = t.transaction_id)
		)
LEFT JOIN transactions t1 ON pd.funding_id = t1.funding_id AND t1.trans_type_id IN (2, 3, 4, 5, 19) AND pd.payment_date = CONVERT(DATE, t1.trans_date)
	AND (
		NULLIF(t1.transaction_id, '') IS NULL
			OR NOT EXISTS (SELECT 1 FROM payments p2 WHERE p2.transaction_id = t1.transaction_id)
		)
WHERE NULLIF(pd.rec_amount, 0) IS NULL

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR




-- now need to do chargebacks and set chargeback_amount
UPDATE pd
SET chargeback_amount = COALESCE(t0.trans_amount, 0), chargeback_trans_id = t0.trans_id, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM funding_payment_dates pd
INNER JOIN payment_schedules ps ON pd.payment_schedule_id = ps.payment_schedule_id
INNER JOIN payments p ON ps.payment_schedule_id = p.payment_schedule_id AND pd.payment_date = p.trans_date
INNER JOIN transactions t0 ON p.transaction_id = t0.transaction_id AND t0.trans_type_id = 14

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

-- now do chargebacks for those funding payment dates where there is NOT a matching payment record but IS a matching transcation
-- that does not match a payment as well
UPDATE pd
SET chargeback_amount = COALESCE(t.trans_amount, 0), chargeback_trans_id = t.trans_id, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM funding_payment_dates pd
INNER JOIN transactions t ON pd.payment_date = t.trans_date AND pd.funding_id = t.funding_id AND t.trans_type_id = 14
	 AND (
		NULLIF(t.transaction_id, '') IS NULL
			OR NOT EXISTS (SELECT 1 FROM payments p WHERE p.transaction_id = t.transaction_id)
		)
WHERE NULLIF(pd.rec_amount, 0) IS NULL

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


-- Now do ACH Reject
UPDATE pd
SET rec_amount = 0, reject_trans_id = t0.trans_id, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM funding_payment_dates pd
INNER JOIN payment_schedules ps ON pd.payment_schedule_id = ps.payment_schedule_id
INNER JOIN payments p ON ps.payment_schedule_id = p.payment_schedule_id AND pd.payment_date = p.trans_date
INNER JOIN transactions t0 ON p.transaction_id = t0.transaction_id AND t0.trans_type_id = 21

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

-- now do rejects for those funding payment dates where there is NOT a matching payment record but IS a matching transcation
-- that does not match a payment as well
UPDATE pd
SET rec_amount = 0, reject_trans_id = t.trans_id, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM funding_payment_dates pd
INNER JOIN transactions t ON pd.payment_date = t.trans_date AND pd.funding_id = t.funding_id AND t.trans_type_id = 21
	 AND (
		NULLIF(t.transaction_id, '') IS NULL
			OR NOT EXISTS (SELECT 1 FROM payments p WHERE p.transaction_id = t.transaction_id)
		)
WHERE pd.rec_amount != 0

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR



-- delete any null funding_payment_dates for a given date where there is also a payment schedule
-- SELECT *
--DELETE d
--FROM funding_payment_dates d
--WHERE d.payment_schedule_id IS NULL
	--AND EXISTS (SELECT 1 FROM funding_payment_dates d2 WHERE d.funding_id = d2.funding_id AND d.payment_date = d2.payment_date AND d2.payment_schedule_id IS NOT NULL)


IF OBJECT_ID('tempdb..#tempFundings') IS NOT NULL DROP TABLE #tempFundings
IF OBJECT_ID('tempdb..#tempSchedules') IS NOT NULL DROP TABLE #tempSchedules
IF OBJECT_ID('tempdb..#tempScheduleChanges') IS NOT NULL DROP TABLE #tempScheduleChanges


SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR



COMMIT TRANSACTION
GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)

Done:


END
GO
