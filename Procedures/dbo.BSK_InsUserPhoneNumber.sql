SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Rick Pina
-- Create date: 2021-02-22
-- Description:	Inserts into User Phone Number
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsUserPhoneNumber]
(
	@user_id					INT,
	@user_email					NVARCHAR(100),
	@phone_number 				NVARCHAR (25),
	@phone_type					NVARCHAR (25),
	@is_primary                 BIT,
	@change_user				NVARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON;

SELECT @user_id = user_id FROM users WHERE email = @user_email

UPDATE user_phones
SET is_primary = 0, change_date = GETUTCDATE(), change_user = @change_user
WHERE user_id = @user_id AND @is_primary = 1

INSERT INTO user_phones(user_id, phone_number, phone_type, is_primary, create_date, change_date, create_user, change_user)
VALUES (@user_id, @phone_number, @phone_type, @is_primary, GETUTCDATE(), GETUTCDATE(), @change_user, @change_user)

SELECT SCOPE_IDENTITY() as user_phone_id
END

GO
