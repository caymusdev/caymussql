SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-05-17
-- Description:	Gets a list of payment schedule changes and how to email about them
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetPaymentScheduleChanges]
AS
BEGIN	
SET NOCOUNT ON;


SELECT c.id, c.change_type, c.payment_schedule_id, c.frequency, c.amount, c.start_date, c.end_date, c.userid, s.approvers
FROM payment_schedule_changes c 
LEFT JOIN sec_payment_schedules s ON c.userid = s.submitter_user_id
WHERE c.approved_by IS NULL


END
GO
