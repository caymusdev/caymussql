SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-01-08
-- Description:	Returns the affiliate name for a given phone number
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetAffiliateNameForPhoneNumber]
(	
	@phone_number				VARCHAR(50)
)
AS
BEGIN
SET NOCOUNT ON;

SELECT a.affiliate_name, a.affiliate_id
FROM phone_numbers p
INNER JOIN contacts c ON p.contact_id = c.contact_id
INNER JOIN affiliates a ON c.affiliate_id = a.affiliate_id
WHERE p.phone_number = @phone_number

END

GO
