SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-10-26
-- Description:	Goes through each funding and sets the performance status id
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdateFundingPerformances]

AS
BEGIN
SET NOCOUNT ON;

DECLARE @today DATE; SET @today = CONVERT(DATE, DATEADD(HH, -5, GETUTCDATE()))
DECLARE @MissedDailyNonPer INT, @MissedWeeklyNonPer INT
SELECT @MissedDailyNonPer = dbo.CF_GetSetting('MissedDailyNonPer'), @MissedWeeklyNonPer = dbo.CF_GetSetting('MissedWeeklyNonPer')

EXEC dbo.CSP_UpdCalculations

-- RKB 2018-12-17 - New process from Cliff and Davis
-- simpler new formula, and writeoffs
-- if daily frequency and have not had any money within 5 calendar days set to NonPerforming
-- if weekly frequency and have not had any money within 10 calendar days set to NonPerforming
UPDATE fundings
SET performance_status_id = CASE 
	WHEN contract_status = 4 THEN 4  -- once a contract is written off, write off the status
	WHEN frequency = 'Daily' AND DATEDIFF(DD, COALESCE(last_payment_date, first_payment_date), @today) > @MissedDailyNonPer 
		-- make sure enough time has passed
		AND float_amount = 0
		AND DATEDIFF(DD, first_payment_date, @today) > 7 
		THEN 3
	WHEN frequency = 'Weekly' AND DATEDIFF(DD, last_payment_date, @today) > @MissedWeeklyNonPer 
		AND float_amount = 0
		AND DATEDIFF(DD, first_payment_date, @today) > 7 
		THEN 3
	-- RKB: 2019-01-16 - Need a case for newly funded.  If within the past 2 weeks, just leave as Normal pay (0)
	WHEN DATEDIFF(DD, funded_date, @today) < 14 THEN 0
	WHEN estimated_turn / (COALESCE(actual_turn, 0) + 0.0001) > 1.20 THEN 1 -- fast pay
	WHEN estimated_turn / (COALESCE(actual_turn, 0) + 0.0001) < 0.80 THEN 2 -- slow pay
	WHEN estimated_turn / (COALESCE(actual_turn, 0) + 0.0001) BETWEEN 0.80 AND 1.20 THEN 0 -- normal pay
END, change_user = 'SYSTEM', change_date = GETUTCDATE()
WHERE COALESCE(performance_status_id, -1) <> CASE 
	WHEN contract_status = 4 THEN 4  -- once a contract is written off, write off the status
	WHEN frequency = 'Daily' AND DATEDIFF(DD, COALESCE(last_payment_date, first_payment_date), @today) > @MissedDailyNonPer 
		-- make sure enough time has passed
		AND float_amount = 0
		AND DATEDIFF(DD, first_payment_date, @today) > 7 
		THEN 3
	WHEN frequency = 'Weekly' AND DATEDIFF(DD, last_payment_date, @today) > @MissedWeeklyNonPer 
		AND float_amount = 0
		AND DATEDIFF(DD, first_payment_date, @today) > 7 
		THEN 3
	WHEN DATEDIFF(DD, funded_date, @today) < 14 THEN 0
	WHEN estimated_turn / (COALESCE(actual_turn, 0) + 0.0001) > 1.20 THEN 1 -- fast pay
	WHEN estimated_turn / (COALESCE(actual_turn, 0) + 0.0001) < 0.80 THEN 2 -- slow pay
	WHEN estimated_turn / (COALESCE(actual_turn, 0) + 0.0001) BETWEEN 0.80 AND 1.20 THEN 0 -- normal pay
END
	AND contract_status IN (1,3,4) AND COALESCE(contract_substatus_id, -1) <> 2


-- update merchants
UPDATE m
-- if there are no fundings with a payoff amount
SET status = CASE (SELECT COUNT(*)
					FROM fundings f
					INNER JOIN merchants m2 ON f.merchant_id = m2.merchant_id
					WHERE m2.merchant_id = m.merchant_id AND (f.payoff_amount <> 0 OR f.float_amount <> 0))
			WHEN 0 THEN 'Complete' ELSE 'Active' END, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM merchants m

UPDATE a
SET status = CASE (SELECT COUNT(*)
					FROM affiliates a2
					INNER JOIN merchants m2 ON a2.affiliate_id = m2.affiliate_id
					WHERE a2.affiliate_id = a.affiliate_id AND m2.status = 'Active'	)
				WHEN 0 THEN 'Complete' ELSE 'Active' END, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM affiliates a
	

END
GO
