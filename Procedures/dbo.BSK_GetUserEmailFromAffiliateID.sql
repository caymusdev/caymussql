SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Rick Pina
-- Create date: 2021-02-05
-- Description:	Returns the email of the assigned collector for the active case for an affiliate
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetUserEmailFromAffiliateID]
(
	@affiliate_id			INT
)
AS
BEGIN	
SET NOCOUNT ON;

	SELECT u.email 
	FROM cases c 
	INNER JOIN collector_cases cc ON c.case_id = cc.case_id AND cc.end_date IS NULL
	INNER JOIN users u ON cc.collector_user_id = u.user_id
	WHERE c.affiliate_id = @affiliate_id AND c.active = 1
END
GO
