SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2020-10-02
-- Description: Checks for duplicate merchant bank accounts
-- =============================================
CREATE PROCEDURE dbo.CSP_CheckForDupeBankAccounts

AS
BEGIN

SET NOCOUNT ON

SELECT m.merchant_id, m.merchant_name, ba.account_number, ba.routing_number, ba.bank_name
FROM merchants m
INNER JOIN (
	SELECT ba.merchant_id, ba.account_number, ba.routing_number--, COUNT(*)
	FROM merchant_bank_accounts ba
	GROUP BY ba.merchant_id, ba.routing_number, ba.account_number
	HAVING COUNT(*) > 1
) x ON m.merchant_id = x.merchant_id
INNER JOIN merchant_bank_accounts ba ON x.merchant_id = ba.merchant_id AND x.account_number = ba.account_number AND x.routing_number = ba.routing_number
ORDER BY m.merchant_name, ba.account_number, ba.routing_number, ba.bank_name


END
GO
