SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-12-23
-- Description:	Inserts a new affiliate bank account
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsAffiliateBankAccount]
(
	@affiliate_id			INT,
	@nickname				NVARCHAR (50), 
	@bank_name				NVARCHAR (100), 
	@routing_number			VARCHAR (50),
	@account_number			VARCHAR (50),
	@userid					NVARCHAR (100)--,
--	@main_account			BIT,
--	@update_future			BIT--,
	--@funding_id				BIGINT
)
AS
BEGIN	
SET NOCOUNT ON;
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @merchant_id INT

-- need to store in merchant_bank_accounts so it must be on a merchant.  Just get the most recent merchant for the affiliate
SELECT TOP 1 @merchant_id = merchant_id
FROM merchants
WHERE affiliate_id = @affiliate_id
ORDER BY merchant_id DESC


/*
INSERT INTO affiliate_bank_accounts(affiliate_id, nickname, bank_name, routing_number, account_number, account_status, create_date, change_date, change_user, create_user)
SELECT @affiliate_id, @nickname, @bank_name, @routing_number, @account_number, 'Active', GETUTCDATE(), GETUTCDATE(), @userid, @userid
WHERE NOT EXISTS (
	SELECT 1 
	FROM affiliate_bank_accounts a2
	WHERE a2.affiliate_id = @affiliate_id AND a2.bank_name = @bank_name AND a2.account_number = @account_number AND a2.routing_number = @routing_number)
	*/

INSERT INTO merchant_bank_accounts(affiliate_id, merchant_id, nickname, bank_name, routing_number, account_number, account_status, create_date, change_date, change_user)
SELECT @affiliate_id, @merchant_id, @nickname, @bank_name, @routing_number, @account_number, 'Active', GETUTCDATE(), GETUTCDATE(), @userid
WHERE NOT EXISTS (
	SELECT 1 
	FROM merchant_bank_accounts a2
	WHERE a2.affiliate_id = @affiliate_id AND a2.bank_name = @bank_name AND a2.account_number = @account_number AND a2.routing_number = @routing_number)

--IF @main_account = 1
--	BEGIN
--		UPDATE fundings 
--		SET receivables_aba = @routing_number, receivables_account_nbr = @account_number, receivables_bank_name = @bank_name, 
--			change_date = GETUTCDATE(), change_user = @userid
--		WHERE id = @funding_id
--	END

--IF @update_future = 1
--	BEGIN
--		DECLARE @merchant_bank_account_id INT
--		SELECT @merchant_bank_account_id = merchant_bank_account_id 
--		FROM merchant_bank_accounts ba
--		WHERE ba.merchant_id = 1 AND ba.routing_number = @routing_number AND ba.account_number = @account_number AND account_status = 'Active'
--			AND ba.affiliate_id = @affiliate_id
		
--		UPDATE funding_payment_dates 
--		SET merchant_bank_account_id = @merchant_bank_account_id, change_user = @userid, change_date = GETUTCDATE()
--		WHERE funding_id = @funding_id AND payment_date > @today AND merchant_bank_account_id != @merchant_bank_account_id
--	END


-- return the data to be added to dropdown
SELECT ba.bank_name, ba.routing_number, ba.account_number, ba.merchant_bank_account_id AS affiliate_bank_account_id, ba.nickname
FROM merchant_bank_accounts ba
WHERE ba.affiliate_id = @affiliate_id AND ba.bank_name = @bank_name AND ba.routing_number = @routing_number AND ba.account_number = @account_number


END



GO
