SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
 -- =============================================
-- Author:      Ryan Brown
-- Create Date: 2020-09-15
-- Description: Either adds or removes a permission from a role
-- =============================================
CREATE PROCEDURE [dbo].[BSK_UpdRolePermission]
(
	@role_id				INT,
	@has_permission			BIT,
	@permission_id			INT,
	@change_user			NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON

IF @has_permission = 1
	BEGIN
		INSERT INTO role_permissions( role_id, permission_id, create_date, change_date, change_user, create_user)
		SELECT @role_id, @permission_id, GETUTCDATE(), GETUTCDATE(), @change_user, @change_user
		WHERE NOT EXISTS (SELECT 1 FROM role_permissions rp WHERE rp.role_id = @role_id AND rp.permission_id = @permission_id)
	END
ELSE
	BEGIN
		-- update for sake of trigger
		UPDATE role_permissions
		SET change_date = GETUTCDATE(), change_user = @change_user
		WHERE role_id = @role_id AND permission_id = @permission_id

		DELETE FROM role_permissions WHERE role_id = @role_id AND permission_id = @permission_id
	END

END
GO
