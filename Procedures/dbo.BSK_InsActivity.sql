SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2020-09-04
-- Description: Inserts a record into the Activity table
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsActivity]
(
	@activity_type_id		INT,
	@user_id				NVARCHAR (100),
	@comments				NVARCHAR (1000),
	@affiliate_id			BIGINT
)
AS
BEGIN
SET NOCOUNT ON

INSERT INTO activity_log (activity_type_id, user_id, comments, affiliate_id, create_date, change_date, change_user, create_user)
VALUES (@activity_type_id, @user_id, @comments, @affiliate_id, GETUTCDATE(), GETUTCDATE(), @user_id, @user_id)

END
GO
