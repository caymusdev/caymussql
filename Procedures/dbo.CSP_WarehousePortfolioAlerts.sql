SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2022-05-10
-- Description:	Runs various checks for warehouse portfolio concentrations.  Sends alert to Davis.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_WarehousePortfolioAlerts]

AS
BEGIN
SET NOCOUNT ON;

-- call this after [CSP_UpdateEligibility] is called

DECLARE @max_percent NUMERIC(10, 2); SET @max_percent = 0.90

SELECT TOP 1 *
INTO #tempPortData
FROM portfolio_eligibility
ORDER BY portfolio_date DESC
 

SELECT 'Incorporation State' AS issue, incorp_state_percent, incorp_state, incorp_state_percent_max
FROM #tempPortData
WHERE incorp_state_percent / incorp_state_percent_max >= @max_percent


SELECT 'Industry' AS issue, industry_percent, industry, industry_percent_max
FROM #tempPortData
WHERE industry_percent / industry_percent_max >= @max_percent


SELECT 'Average Balance' AS issue, avg_balance, avg_balance_max
FROM #tempPortData
WHERE avg_balance / avg_balance_max >= @max_percent


SELECT 'Remaining Term' AS issue, remaining_term, remaining_term_max
FROM #tempPortData
WHERE remaining_term / remaining_term_max >= @max_percent


END

GO
