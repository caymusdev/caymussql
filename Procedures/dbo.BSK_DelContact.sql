SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Rick Pina
-- Create Date: 2021-01-12
-- Description: Deletes an Contact from the Affiliates Page
-- =============================================
CREATE PROCEDURE [dbo].[BSK_DelContact]
(
	@contact_id			INT,
	@change_user		NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON
		
DECLARE @create_user NVARCHAR (100)
DECLARE @is_management BIT
		
SELECT @is_management = dbo.BF_DoesUserHaveManagementRole(@change_user)

SELECT @create_user = create_user FROM contacts WHERE contact_id = @contact_id
IF @create_user = @change_user  OR @is_management = 1 
	BEGIN
		UPDATE phone_numbers SET change_date = GETUTCDATE(), change_user = @change_user WHERE contact_id = @contact_id
		DELETE FROM phone_numbers WHERE contact_id = @contact_id

		-- for audit trigger
		UPDATE contacts SET change_date = GETUTCDATE(), change_user = @change_user WHERE contact_id = @contact_id

		DELETE FROM contacts WHERE contact_id = @contact_id	
	END	

END
GO
