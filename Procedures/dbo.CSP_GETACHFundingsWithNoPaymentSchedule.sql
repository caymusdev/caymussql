SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-09-19
-- Description:	Checks to see if there are any fundings that still do not have a payment schedule but do have Forte as the processor
-- UPDATE 2020-02-14: Since payment schedules are gone this will check to see if there are fundings that have no funding payment dates period.
-- Just a system check that will email me
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GETACHFundingsWithNoPaymentSchedule]	
AS
BEGIN
SET NOCOUNT ON;

DECLARE @goLiveDate DATETIME; SELECT @goLiveDate = CONVERT(DATETIME, dbo.CF_GetSetting('GoLiveDate'))

SELECT f.id, f.legal_name
FROM fundings f
--LEFT JOIN funding_payment_schedules fs ON f.id = fs.funding_id
WHERE COALESCE(f.funded_date, GETDATE()) >= @goLiveDate
	AND f.processor IS NOT NULL
	--AND fs.payment_schedule_id IS NULL
	AND NOT EXISTS (SELECT 1 FROM funding_payment_dates WHERE funding_id = f.id)
	AND f.hard_offer_type = 'ACH'

END
GO
