SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2020-09-17
-- Description: Inserts a new role
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsRole]
(
	@role_name			NVARCHAR (100),
	@role_desc			NVARCHAR (500),
	@change_user		NVARCHAR (100)
)
AS
BEGIN
    SET NOCOUNT ON
	IF EXISTS (SELECT 1 FROM roles WHERE role_name = @role_name)
		BEGIN
			DECLARE @ErrorMessage NVARCHAR (MAX)
			SELECT @ErrorMessage = dbo.BF_TranslateWord('RecordExists')
			RAISERROR (@ErrorMessage, 16, 1)
		END
	ELSE 
		BEGIN
			INSERT INTO roles (role_name, role_desc, create_date, change_date, change_user, create_user)
			VALUES (@role_name, @role_desc, GETUTCDATE(), GETUTCDATE(), @change_user, @change_user)
		END

END
GO
