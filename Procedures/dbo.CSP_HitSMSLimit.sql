SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-06-04
-- Description:	Returns true when the system has sent X number of text within the last Y minutes
-- =============================================
CREATE PROCEDURE dbo.CSP_HitSMSLimit

AS
BEGIN	
SET NOCOUNT ON;

SELECT COUNT(*) AS NumberOfCalls
FROM sms_logs s WITH (NOLOCK)
WHERE DATEDIFF(MINUTE, s.create_date, GETUTCDATE()) < 60

-- NOT USING RIGHT NOW BECAUSE WE ARE ONLY SENDING TEXTS WHEN ONLY CERTAIN EVENTS FAIL.

END
GO
