SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-08-13
-- Description:	Gets a list of redirect approvals.  Used in logic to email users when there are unapproved redirects.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetUnapprovedRedirectApprovals]

AS
BEGIN	
SET NOCOUNT ON;

SELECT ra.*
FROM redirect_approvals ra WITH (NOLOCK)
WHERE ra.approved_by IS NULL AND ra.rejected_by IS NULL


END
GO
