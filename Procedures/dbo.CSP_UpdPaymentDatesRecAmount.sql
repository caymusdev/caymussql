SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-02-18
-- Description: Copied from CSP_InsPaymentSchedPayments but now doing payment dates instead of payment schedules.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdPaymentDatesRecAmount]
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
BEGIN TRANSACTION

-- VARS
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SELECT @tomorrow = DATEADD(DD, CASE DATEPART(WEEKDAY, @today) WHEN 6 THEN 3 WHEN 7 THEN 2 ELSE 1 END, @today)
DECLARE @maxID INT, @currentID INT, @start_date DATE, @end_date DATE, @tempDate DATE, @tempID INT
-- END VARS

-- First do ACH received
UPDATE pd
SET rec_amount = t.trans_amount, rec_trans_id = t.trans_id, change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM funding_payment_dates pd
INNER JOIN payments p ON pd.id = p.funding_payment_date_id
INNER JOIN transactions t ON p.transaction_id = t.transaction_id AND t.trans_type_id = 3
WHERE pd.payment_method = 'ACH' AND COALESCE(pd.rec_amount, 0) = 0 AND pd.rec_trans_id IS NULL

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

PRINT CONCAT('1) ', CONVERT(TIME, GETUTCDATE()))


-- Now do ACH Chargeback
UPDATE pd
SET chargeback_amount = t.trans_amount, chargeback_trans_id = t.trans_id, change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM funding_payment_dates pd
INNER JOIN payments p ON pd.id = p.funding_payment_date_id
INNER JOIN transactions t ON p.transaction_id = t.transaction_id AND t.trans_type_id = 14
WHERE pd.payment_method = 'ACH' AND (pd.chargeback_amount IS NULL OR pd.chargeback_trans_id IS NULL)

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

PRINT CONCAT('2) ', CONVERT(TIME, GETUTCDATE()))


-- Now do ACH Reject
UPDATE pd
SET rec_amount = 0, reject_trans_id = t.trans_id, change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM funding_payment_dates pd
INNER JOIN payments p ON pd.id = p.funding_payment_date_id
INNER JOIN transactions t ON p.transaction_id = t.transaction_id AND t.trans_type_id = 21
WHERE pd.payment_method = 'ACH' AND COALESCE(pd.rec_amount, 0) != 0 AND pd.reject_trans_id IS NULL

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

PRINT CONCAT('3) ', CONVERT(TIME, GETUTCDATE()))


-- Checks, wires, and credit card all have to be done one at a time.  Need temp table to do work
IF OBJECT_ID('tempdb..#tempTrans') IS NOT NULL DROP TABLE #tempTrans
CREATE TABLE #tempTrans (id int not null identity, funding_payment_date_id INT)

-- start a list of in-use trans ids
SELECT DISTINCT pd.rec_trans_id
INTO #tempRecTransIDs
FROM funding_payment_dates pd WITH (NOLOCK)
WHERE pd.rec_trans_id IS NOT NULL
ORDER BY pd.rec_trans_id

PRINT CONCAT('4) ', CONVERT(TIME, GETUTCDATE()))


DECLARE @bufferDays INT; SET @bufferDays = 5


-- Now do Checks   ************************************************** CHECKS BEGIN *****************************************
TRUNCATE TABLE #tempTrans

-- all check received transactions
SELECT t.*
INTO #tempTransactions
FROM transactions t WITH (NOLOCK)
WHERE t.trans_type_id = 19 AND t.redistributed = 0 AND t.trans_date >= (GETUTCDATE() - 60)

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

PRINT CONCAT('5) ', CONVERT(TIME, GETUTCDATE()))


-- get all expected check dates that have not been reconciled
INSERT INTO #tempTrans (funding_payment_date_id)
SELECT pd.id
FROM funding_payment_dates pd
WHERE (pd.rec_amount IS NULL OR pd.rec_trans_id IS NULL) AND pd.payment_method = 'Check' 
	AND pd.payment_date >= (GETUTCDATE() - 60) AND pd.payment_date <= (GETUTCDATE() + 1)
ORDER BY pd.payment_date

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

PRINT CONCAT('6) ', CONVERT(TIME, GETUTCDATE()))

SET @currentID = 1
SELECT @maxID = COALESCE(MAX(t.id), 0) FROM #tempTrans t

WHILE @currentID <= @maxID
	BEGIN
		SELECT @tempID = t.funding_payment_date_id
		FROM #tempTrans t
		WHERE t.id = @currentID

		PRINT CONCAT('7) ', @tempID, ' # ', CONVERT(TIME, GETUTCDATE()))

		UPDATE pd
		SET rec_amount = COALESCE(t.trans_amount, t2.trans_amount), rec_trans_id = COALESCE(t.trans_id, t2.trans_id), change_user = 'SYSTEM', change_date = GETUTCDATE()
		FROM funding_payment_dates pd WITH (NOLOCK)	
		LEFT JOIN #tempTransactions t WITH (NOLOCK) ON pd.funding_id = t.funding_id AND pd.payment_date = t.trans_date AND t.trans_id NOT IN (SELECT rec_trans_id FROM #tempRecTransIDs)
		LEFT JOIN #tempTransactions t2 WITH (NOLOCK) ON pd.funding_id = t2.funding_id AND ABS(DATEDIFF(DD, pd.payment_date, t2.trans_date)) <= @bufferDays 
			AND t2.trans_id NOT IN (SELECT rec_trans_id FROM #tempRecTransIDs)-- get the one that is same amount but on different day		
		WHERE pd.id = @tempID

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		PRINT CONCAT('8) ', @tempID, ' # ', CONVERT(TIME, GETUTCDATE()))

		-- if the trans_id is now gotten, add to temp table so it does not get used again
		INSERT INTO #tempRecTransIDs (rec_trans_id)
		SELECT pd.rec_trans_id
		FROM funding_payment_dates pd WITH (NOLOCK)
		WHERE pd.id = @tempID AND pd.rec_trans_id IS NOT NULL

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		PRINT CONCAT('9) ', @tempID, ' # ', CONVERT(TIME, GETUTCDATE()))

		SET @currentID = @currentID + 1
	END

--   ************************************************** CHECKS END *****************************************


--   ************************************************** WIRES BEGIN *****************************************
TRUNCATE TABLE #tempTrans

SELECT t.*
INTO #tempTransactionsWires
FROM transactions t WITH (NOLOCK)
WHERE t.trans_type_id = 5 AND t.redistributed = 0 AND t.trans_date >= (GETUTCDATE() - 60)
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

PRINT CONCAT('10) ', CONVERT(TIME, GETUTCDATE()))


INSERT INTO #tempTrans (funding_payment_date_id)
SELECT pd.id
FROM funding_payment_dates pd
WHERE (pd.rec_amount IS NULL OR pd.rec_trans_id IS NULL) AND pd.payment_method = 'Wire' 
	 AND pd.payment_date >= (GETUTCDATE() - 60) AND pd.payment_date <= (GETUTCDATE() + 1)
ORDER BY pd.payment_date

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

PRINT CONCAT('11) ', CONVERT(TIME, GETUTCDATE()))

SET @currentID = 1
SELECT @maxID = COALESCE(MAX(t.id), 0) FROM #tempTrans t

WHILE @currentID <= @maxID
	BEGIN
		SELECT @tempID = t.funding_payment_date_id
		FROM #tempTrans t
		WHERE t.id = @currentID

		PRINT CONCAT('12) ', @tempID, ' # ', CONVERT(TIME, GETUTCDATE()))

		UPDATE pd
		SET rec_amount = COALESCE(t.trans_amount, t2.trans_amount), rec_trans_id = COALESCE(t.trans_id, t2.trans_id), change_user = 'SYSTEM', change_date = GETUTCDATE()
		--OUTPUT inserted.*, fps.*, t2.*
		FROM funding_payment_dates pd WITH (NOLOCK)
		LEFT JOIN #tempTransactionsWires t WITH (NOLOCK) ON pd.funding_id = t.funding_id AND pd.payment_date = t.trans_date AND t.trans_id NOT IN (SELECT rec_trans_id FROM #tempRecTransIDs)
		LEFT JOIN #tempTransactionsWires t2 WITH (NOLOCK) ON pd.funding_id = t2.funding_id AND ABS(DATEDIFF(DD, pd.payment_date, t2.trans_date)) <= @bufferDays 
			AND t2.trans_id NOT IN (SELECT rec_trans_id FROM #tempRecTransIDs)
			-- get the one that is same amount but on different day		
		WHERE pd.id = @tempID

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		PRINT CONCAT('13) ', @tempID, ' # ', CONVERT(TIME, GETUTCDATE()))

		-- if the trans_id is now gotten, add to temp table so it does not get used again
		INSERT INTO #tempRecTransIDs (rec_trans_id)
		SELECT pd.rec_trans_id
		FROM funding_payment_dates pd WITH (NOLOCK)
		WHERE pd.id = @tempID AND pd.rec_trans_id IS NOT NULL

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		PRINT CONCAT('14) ', @tempID, ' # ', CONVERT(TIME, GETUTCDATE()))

		SET @currentID = @currentID + 1
	END

--   ************************************************** WIRES END *****************************************

--   ************************************************** CC BEGIN *****************************************
TRUNCATE TABLE #tempTrans

SELECT t.*
INTO #tempTransactionsCC
FROM transactions t WITH (NOLOCK)
WHERE t.trans_type_id = 4 AND t.redistributed = 0 AND t.trans_date >= (GETUTCDATE() - 60)
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

PRINT CONCAT('15) ', CONVERT(TIME, GETUTCDATE()))


INSERT INTO #tempTrans (funding_payment_date_id)
SELECT pd.id
FROM funding_payment_dates pd WITH (NOLOCK)
WHERE (pd.rec_amount IS NULL OR pd.rec_trans_id IS NULL) AND pd.payment_method = 'CC' 
	 AND pd.payment_date >= (GETUTCDATE() - 60) AND pd.payment_date <= (GETUTCDATE() + 1)
ORDER BY pd.payment_date

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

PRINT CONCAT('16) ', CONVERT(TIME, GETUTCDATE()))

SET @currentID = 1
SELECT @maxID = COALESCE(MAX(t.id), 0) FROM #tempTrans t

WHILE @currentID <= @maxID
	BEGIN
		SELECT @tempID = t.funding_payment_date_id
		FROM #tempTrans t
		WHERE t.id = @currentID

		PRINT CONCAT('17) ', @tempID, ' # ', CONVERT(TIME, GETUTCDATE()))

		UPDATE pd
		SET rec_amount = COALESCE(t.trans_amount, t2.trans_amount), rec_trans_id = COALESCE(t.trans_id, t2.trans_id), change_user = 'SYSTEM', change_date = GETUTCDATE()
		FROM funding_payment_dates pd WITH (NOLOCK)				
		LEFT JOIN #tempTransactionsCC t WITH (NOLOCK) ON pd.funding_id = t.funding_id AND pd.payment_date = t.trans_date AND t.trans_id NOT IN (SELECT rec_trans_id FROM #tempRecTransIDs)
		LEFT JOIN #tempTransactionsCC t2 WITH (NOLOCK) ON pd.funding_id = t2.funding_id AND ABS(DATEDIFF(DD, pd.payment_date, t2.trans_date)) <= @bufferDays 
			AND t2.trans_id NOT IN (SELECT rec_trans_id FROM #tempRecTransIDs) 	-- get the one that is same amount but on different day
		WHERE pd.id = @tempID

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		PRINT CONCAT('18) ', @tempID, ' # ', CONVERT(TIME, GETUTCDATE()))

		-- if the trans_id is now gotten, add to temp table so it does not get used again
		INSERT INTO #tempRecTransIDs (rec_trans_id)
		SELECT pd.rec_trans_id
		FROM funding_payment_dates pd WITH (NOLOCK)
		WHERE pd.id = @tempID AND pd.rec_trans_id IS NOT NULL

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		PRINT CONCAT('19) ', @tempID, ' # ', CONVERT(TIME, GETUTCDATE()))

		SET @currentID = @currentID + 1
	END

--   ************************************************** CC END *****************************************

PRINT CONCAT('20) ', CONVERT(TIME, GETUTCDATE()))
COMMIT TRANSACTION
PRINT CONCAT('21) ', CONVERT(TIME, GETUTCDATE()))
GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)

Done:

IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData
IF OBJECT_ID('tempdb..#tempTrans') IS NOT NULL DROP TABLE #tempTrans
PRINT CONCAT('22) ', CONVERT(TIME, GETUTCDATE()))

END
GO
