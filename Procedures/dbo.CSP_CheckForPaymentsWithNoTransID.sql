SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-12-17
-- Description:	Checks for payments that have no transaction id
-- =============================================
CREATE PROCEDURE [dbo].[CSP_CheckForPaymentsWithNoTransID] 
AS
BEGIN
SET NOCOUNT ON;

DECLARE @date DATE SET @date = DATEADD(DD, -7, GETUTCDATE())
--SELECT @date

-- retries on on_hold fundings need to be deleted first.
UPDATE p 
SET deleted = 1, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM payments p WITH (NOLOCK)
INNER JOIN fundings f WITH (NOLOCK) ON p.funding_id = f.id
WHERE p.retry_level = 1 AND p.deleted = 0 AND p.processed_date IS NULL AND f.on_hold = 1 


SELECT p.payment_id, p.amount, p.trans_date
FROM payments p WITH (NOLOCK)
--LEFT JOIN payment_schedules ps WITH (NOLOCK) ON p.payment_schedule_id = ps.payment_schedule_id
LEFT JOIN funding_payment_dates pd WITH (NOLOCK) ON p.funding_payment_date_id = pd.id
WHERE p.trans_date <= @date AND p.transaction_id IS NULL AND p.deleted = 0 AND p.waived = 0
	AND COALESCE(p.is_fee, 0) = 0 AND COALESCE(pd.payment_method, 'ACH') = 'ACH'

END
GO
