SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-02-04
-- Description:	Gets a list of the changes for a given user or alternatively all unapproved changes
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetFundingPaymentDateChanges]
(
	@userid				NVARCHAR (100) = NULL
)
AS
BEGIN	
SET NOCOUNT ON;

IF @userid = ''
	SET @userid = NULL


SELECT ROW_NUMBER() OVER (ORDER BY x.userid, x.legal_name, x.contract_number, x.payment_date) AS RowNumber, 
	x.change_type, x.payment_date, x.amount, x.original_amount, x.merchant_bank_account_id, x.original_merchant_bank_account_id, x.payment_method, x.original_payment_method, 
	x.payment_processor, x.original_payment_processor, x.comments, x.original_comments, x.change_reason_id, x.original_change_reason_id, x.funding_id, x.legal_name, x.contract_number, 
	x.userid AS change_user, x.bank_name, x.nickname, x.routing_number, x.account_number, x.original_bank_name, x.original_nickname, x.original_routing_number, x.original_account_number, 
	x.worker_id, x.funding_payment_id, x.change_reason, x.original_change_reason, x.original_payment_date, s.approvers
FROM (
	SELECT CASE WHEN w.funding_payment_id IS NULL THEN 'New' WHEN w.active = 0 THEN 'Delete' ELSE 'Change' END AS change_type,
		w.payment_date, w.amount, d.amount AS original_amount, w.merchant_bank_account_id, d.merchant_bank_account_id AS original_merchant_bank_account_id, 
		w.payment_method, d.payment_method AS original_payment_method, w.payment_processor, d.payment_processor AS original_payment_processor, w.comments, d.comments AS original_comments, 
		w.change_reason_id, d.change_reason_id AS original_change_reason_id, COALESCE(w.funding_id, d.funding_id) AS funding_id, 
		f.legal_name, f.contract_number, w.userid, ba.bank_name, ba.nickname, ba.routing_number, ba.account_number, 
		ba2.bank_name AS original_bank_name, ba2.nickname AS original_nickname, ba2.routing_number AS original_routing_number, ba2.account_number AS original_account_number, 
		w.id AS worker_id, d.id AS funding_payment_id, cr1.reason AS change_reason, cr2.reason AS original_change_reason, d.payment_date AS original_payment_date
	FROM funding_payment_dates_work w
	LEFT JOIN funding_payment_dates d ON w.funding_payment_id = d.id
	INNER JOIN fundings f ON COALESCE(w.funding_id ,d.funding_id) = f.id
	LEFT JOIN merchant_bank_accounts ba ON w.merchant_bank_account_id = ba.merchant_bank_account_id
	LEFT JOIN merchant_bank_accounts ba2 ON d.merchant_bank_account_id = ba2.merchant_bank_account_id
	LEFT JOIN payment_schedule_change_reasons cr1 ON w.change_reason_id = cr1.id
	LEFT JOIN payment_schedule_change_reasons cr2 ON d.change_reason_id = cr2.id
	WHERE (w.userid = @userid OR @userid IS NULL)
		AND w.changed = 1 AND COALESCE(w.approved, 0) = 0 AND (COALESCE(w.ready_for_approval, 0) = 1 OR @userid IS NOT NULL) AND w.rejected_date IS NULL
		AND COALESCE(w.revised_original, 0) = 0

	UNION ALL

	SELECT 'ReviseOriginal' AS change_type, MIN(w.payment_date) AS payment_date, MAX(w.amount) AS amount, 
		MIN(CASE f.frequency WHEN 'Daily' THEN weekday_payment ELSE weekly_payment END) AS original_amount, 
		w.merchant_bank_account_id, ba2.merchant_bank_account_id AS original_merchant_bank_account_id, 
		w.payment_method, CASE f.hard_offer_type WHEN 'Split' THEN 'CC' ELSE f.hard_offer_type END AS original_payment_method, 
		w.payment_processor, f.processor AS original_payment_processor, w.comments, NULL AS original_comments, 
		w.change_reason_id, NULL AS original_change_reason_id, f.id AS funding_id, 
		f.legal_name, f.contract_number, w.userid, ba.bank_name, ba.nickname, ba.routing_number, ba.account_number, 
		ba2.bank_name AS original_bank_name, ba2.nickname AS original_nickname, ba2.routing_number AS original_routing_number, ba2.account_number AS original_account_number, 
		MIN(w.id) AS worker_id, NULL AS funding_payment_id, cr1.reason AS change_reason, NULL AS original_change_reason, NULL AS original_payment_date
	FROM funding_payment_dates_work w
	INNER JOIN fundings f ON w.funding_id = f.id
	LEFT JOIN merchant_bank_accounts ba ON w.merchant_bank_account_id = ba.merchant_bank_account_id
	LEFT JOIN merchant_bank_accounts ba2 ON f.merchant_id = ba2.merchant_id AND f.receivables_bank_name = ba2.bank_name
		AND f.receivables_aba = ba2.routing_number AND f.receivables_account_nbr = ba2.account_number
	LEFT JOIN payment_schedule_change_reasons cr1 ON w.change_reason_id = cr1.id
	WHERE (w.userid = @userid OR @userid IS NULL)
		AND w.changed = 1 AND COALESCE(w.approved, 0) = 0 AND (COALESCE(w.ready_for_approval, 0) = 1 OR @userid IS NOT NULL)
		AND COALESCE(w.revised_original, 0) = 1 AND w.active = 1 AND w.rejected_date IS NULL
	GROUP BY w.merchant_bank_account_id, ba2.merchant_bank_account_id, w.payment_method, CASE f.hard_offer_type WHEN 'Split' THEN 'CC' ELSE f.hard_offer_type END, 
		w.payment_processor, f.processor, w.comments,  
		w.change_reason_id, f.id, f.legal_name, f.contract_number, w.userid, ba.bank_name, ba.nickname, ba.routing_number, 
		ba.account_number, ba2.bank_name, ba2.nickname, ba2.routing_number, ba2.account_number, cr1.reason
) x
LEFT JOIN sec_payment_schedules s ON x.userid = s.submitter_user_id
ORDER BY x.userid, x.legal_name, x.contract_number, x.payment_date

END
GO
