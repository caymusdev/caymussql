SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-12
-- Description:	Goes through each import table and processes any records not yet processede
-- =============================================
CREATE PROCEDURE [dbo].[BSK_ProcessImportedRecords]
	
AS
BEGIN
SET NOCOUNT ON;

DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''


DECLARE @now DATETIME; SET @now = GETUTCDATE()

DECLARE @affiliates_added INT, @merchants_added INT, @contracts_added INT, @bad_contract_records INT, @transactions_added INT, @bad_trans_records INT
DECLARE @contacts_added INT, @contact_addresses_added INT, @contact_phones_added INT, @bad_contact_records INT, @bad_contact_phones_records INT, @bad_contact_addresses_records INT
DECLARE @scheduled_payments_added INT, @bad_scheduled_payment_records INT, @bank_accounts_added INT, @bad_bank_account_records INT

/*
BEGIN TRANSACTION

UPDATE import_contracts SET processed_date = @now WHERE processed_date IS NULL
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


-- do contracts first
-- insert new affiliates into the affiliates table
INSERT INTO affiliates (source_affiliate_id, affiliate_name, gross_due, create_date, change_date, change_user, create_user)
SELECT ic.affiliate_id, ic.affiliate_name, SUM(CONVERT(MONEY, COALESCE(ic.gross_due, '0'))), GETUTCDATE(), GETUTCDATE(), 'Import', 'Import'
FROM import_contracts ic 
WHERE ic.processed_date = @now AND COALESCE(affiliate_id, '') != ''
	AND NOT EXISTS (SELECT 1 FROM affiliates a WHERE a.source_affiliate_id = ic.affiliate_id)
GROUP BY ic.affiliate_id, ic.affiliate_name

SELECT @affiliates_added = @@ROWCOUNT, @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

-- NOT GOING TO update affiliates now, insert only

-- now insert merchants
INSERT INTO merchants (affiliate_id, source_merchant_id, merchant_name, create_date, change_date, change_user, create_user)
SELECT a.affiliate_id, ic.merchant_id, ic.merchant_name, GETUTCDATE(), GETUTCDATE(), 'Import', 'Import'
FROM import_contracts ic 
INNER JOIN affiliates a ON ic.affiliate_id = a.source_affiliate_id
WHERE ic.processed_date = @now AND COALESCE(merchant_id, '') != ''
	AND NOT EXISTS (SELECT 1 FROM merchants m WHERE m.source_merchant_id = ic.merchant_id)
GROUP BY a.affiliate_id, ic.merchant_id, ic.merchant_name

SELECT @merchants_added = @@ROWCOUNT, @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


-- now do contracts
INSERT INTO contracts (merchant_id, source_contract_id, contract_number, contract_status_id, gross_due, last_payment_date, last_payment_amount, create_date, change_date, change_user, create_user, 
	fees_outstanding, original_rtr, pp_outstanding, writeoff_date, collect_status)
SELECT m.merchant_id, ic.contract_id, ic.contract_number, NULL, CONVERT(MONEY, COALESCE(ic.gross_due, '0')), NULL, NULL, GETUTCDATE(), GETUTCDATE(), 'Import', 'Import', 
	CONVERT(MONEY, COALESCE(ic.fees_outstanding, '0')), CONVERT(MONEY, COALESCE(ic.original_rtr, '0')), CONVERT(MONEY, COALESCE(ic.pp_outstanding, '0')), 
	ic.writeoff_date, ic.collect_status
FROM import_contracts ic
INNER JOIN merchants m ON ic.merchant_id = m.source_merchant_id
WHERE ic.processed_date = @now AND COALESCE(contract_id, '') != ''
	AND NOT EXISTS (SELECT 1 FROM contracts c WHERE c.source_contract_id = ic.contract_id)

SELECT @contracts_added = @@ROWCOUNT, @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

-- now, in case any did not actually update for some reason flag as bad record
UPDATE ic
SET bad_record = 1
FROM import_contracts ic
WHERE processed_date = @now 
	AND (NOT EXISTS(SELECT 1 FROM contracts c WHERE c.source_contract_id = ic.contract_id) OR ic.contract_id IS NULL)
	AND (NOT EXISTS(SELECT 1 FROM merchants m WHERE m.source_merchant_id = ic.merchant_id) OR ic.merchant_id IS NULL)
	AND (NOT EXISTS(SELECT 1 FROM affiliates a WHERE a.source_affiliate_id = ic.affiliate_id) OR ic.affiliate_id IS NULL)

SELECT @bad_contract_records = @@ROWCOUNT, @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

COMMIT TRANSACTION
GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)
	GOTO TheEnd

Done:
*/

/*
BEGIN TRANSACTION

UPDATE import_transactions SET processed_date = @now WHERE processed_date IS NULL
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

INSERT INTO transactions(source_transaction_id, transaction_type, trans_date, trans_amount, source_contract_id, funding_id, 
	source_merchant_id, merchant_id, create_date, change_date, change_user, create_user, settle_code)
SELECT it.transaction_id, it.transaction_type, it.transaction_date, CONVERT(MONEY, COALESCE(it.transaction_amount, '0')), it.contract_id, c.contract_id, 
	it.merchant_id, m.merchant_id, GETUTCDATE(), GETUTCDATE(), 'Import', 'Import', it.settle_code
FROM import_transactions it
--LEFT JOIN payments p ON it.payment_id = p.source_payment_id
LEFT JOIN contracts c ON it.contract_id = c.source_contract_id
LEFT JOIN merchants m ON it.merchant_id = m.source_merchant_id
WHERE it.processed_date = @now AND COALESCE(it.transaction_id, '') != ''
	AND NOT EXISTS (SELECT 1 FROM transactions t WHERE t.source_contract_id = it.transaction_id)

SELECT @transactions_added = @@ROWCOUNT, @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

-- now, in case any did not actually update for some reason flag as bad record
UPDATE it
SET bad_record = 1
FROM import_transactions it
WHERE processed_date = @now 
	AND (NOT EXISTS(SELECT 1 FROM transactions t WHERE t.source_transaction_id = it.transaction_id) OR it.transaction_id IS NULL)	

SELECT @bad_trans_records = @@ROWCOUNT, @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2


COMMIT TRANSACTION
GOTO Done2

ERROR2:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)
	GOTO TheEnd

Done2:

*/
------------------------ CONTACTS------------------------------
BEGIN TRANSACTION

UPDATE import_contacts SET processed_date = @now WHERE processed_date IS NULL
UPDATE import_contact_phones SET processed_date = @now WHERE processed_date IS NULL
UPDATE import_contact_addresses SET processed_date = @now WHERE processed_date IS NULL
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR3

-- contacts
INSERT INTO contacts(source_contact_id, merchant_id, affiliate_id, contract_id, contact_person_type, active, best_contact, contact_first_name, contact_middle_name, contact_last_name, business_name, 
	note, email, success_rate, create_date, change_date, change_user, create_user)
SELECT ic.contact_id, m.merchant_id, a.affiliate_id, c.contract_id, pt.contact_person_type_id, ic.active, NULL, ic.contact_first_name, ic.contact_middle_name, ic.contact_last_name, 
	ic.business_name, ic.note, ic.email, NULL, GETUTCDATE(), GETUTCDATE(), 'Import', 'Import'
FROM import_contacts ic
LEFT JOIN contracts c ON ic.contract_id = c.source_contract_id
LEFT JOIN merchants m ON ic.merchant_id = m.source_merchant_id
LEFT JOIN affiliates a ON ic.affiliate_id = a.source_affiliate_id
LEFT JOIN contact_person_types pt ON ic.contact_person_type = pt.contact_type
WHERE ic.processed_date = @now AND COALESCE(ic.contact_id, '') != ''
	AND NOT EXISTS (SELECT 1 FROM contacts c WHERE c.source_contact_id = ic.contact_id)

SELECT @contacts_added = @@ROWCOUNT, @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR3


-- clean up imported phone numbers
UPDATE n
SET phone_number = CASE WHEN ISNUMERIC(x.cleaned_number) = 1 AND LEN(x.cleaned_number) = 10 THEN '1' + x.cleaned_number ELSE x.cleaned_number END,
	change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM import_contact_phones n
INNER JOIN (
	SELECT phone_number, TRIM(REPLACE(REPLACE(REPLACE(REPLACE(phone_number, '-', ''), '(', ''), ')', ''), ' ', '')) AS cleaned_number 
	FROM import_contact_phones
	WHERE processed_date = @now
) x ON n.phone_number = x.phone_number
WHERE n.phone_number != CASE WHEN ISNUMERIC(x.cleaned_number) = 1 AND LEN(x.cleaned_number) = 10 THEN '1' + x.cleaned_number ELSE x.cleaned_number END
	AND n.processed_date = @now

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR3

-- contact phones
INSERT INTO phone_numbers(phone_type_id, phone_number, contact_id, create_date, change_date, change_user, create_user)
SELECT t.phone_type_id, ic.phone_number, c.contact_id, GETUTCDATE(), GETUTCDATE(), 'Import', 'Import'
FROM import_contact_phones ic
LEFT JOIN contacts c ON ic.contact_id = c.source_contact_id
LEFT JOIN phone_types t ON ic.phone_type = t.phone_type
WHERE ic.processed_date = @now AND COALESCE(ic.contact_id, '') != '' AND COALESCE(ic.phone_number, '') != ''
	AND NOT EXISTS (SELECT 1 FROM phone_numbers p WHERE p.phone_number = ic.phone_number AND p.contact_id = c.contact_id)

SELECT @contact_phones_added = @@ROWCOUNT, @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR3

-- contact addresses
INSERT INTO addresses(affiliate_id, merchant_id, contract_id, address_number, street_name, suffix_id, postdirectional, city, state, postal_code, 
	create_date, change_date, change_user, create_user)
SELECT c.affiliate_id, c.merchant_id, c.contract_id, ic.address_number, ic.street_name, s.address_suffix_id, ic.postdirectional, ic.city, ic.state, ic.postal_code, 
	GETUTCDATE(), GETUTCDATE(), 'Import', 'Import'
FROM import_contact_addresses ic
LEFT JOIN contacts c ON ic.contact_id = c.source_contact_id
LEFT JOIN address_suffixes s ON ic.suffix = s.suffix
WHERE ic.processed_date = @now AND COALESCE(ic.contact_id, '') != '' AND COALESCE(ic.address_number, '') != ''
	AND NOT EXISTS (SELECT 1 FROM addresses p WHERE p.address_number = ic.address_number AND p.street_name = ic.street_name AND p.postal_code = ic.postal_code
		AND p.affiliate_id = c.affiliate_id)

SELECT @contact_addresses_added = @@ROWCOUNT, @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR3

-- now, in case any did not actually update for some reason flag as bad record
UPDATE i
SET bad_record = 1
FROM import_contacts i
WHERE processed_date = @now 
	AND (NOT EXISTS(SELECT 1 FROM contacts t WHERE t.source_contact_id = i.contact_id) OR i.contact_id IS NULL)	

SELECT @bad_contact_records = @@ROWCOUNT, @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR3

UPDATE i
SET bad_record = 1
FROM import_contact_phones i
WHERE processed_date = @now 
	AND (NOT EXISTS(SELECT 1 FROM phone_numbers t WHERE t.phone_number = i.phone_number) OR i.contact_id IS NULL)	

SELECT @bad_contact_phones_records = @@ROWCOUNT, @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR3


UPDATE i
SET bad_record = 1
FROM import_contact_addresses i
WHERE processed_date = @now 
	AND (NOT EXISTS(SELECT 1 FROM addresses t WHERE t.address_number = i.address_number AND t.street_name = i.street_name AND t.postal_code = i.postal_code) OR i.contact_id IS NULL)

SELECT @bad_contact_addresses_records = @@ROWCOUNT, @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR3

-- clean up phone numbers
UPDATE n
SET phone_number = CASE WHEN ISNUMERIC(x.cleaned_number) = 1 AND LEN(x.cleaned_number) = 10 THEN '1' + x.cleaned_number ELSE x.cleaned_number END,
	change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM phone_numbers n
INNER JOIN (
	SELECT phone_number_id, TRIM(REPLACE(REPLACE(REPLACE(REPLACE(phone_number, '-', ''), '(', ''), ')', ''), ' ', '')) AS cleaned_number 
	FROM phone_numbers
) x ON n.phone_number_id = x.phone_number_id
WHERE phone_number != CASE WHEN ISNUMERIC(x.cleaned_number) = 1 AND LEN(x.cleaned_number) = 10 THEN '1' + x.cleaned_number ELSE x.cleaned_number END



COMMIT TRANSACTION
GOTO Done3

ERROR3:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)
	GOTO TheEnd

Done3:

/*

------------------------ PAYMENTS ------------------------------
BEGIN TRANSACTION

UPDATE import_scheduled_payments SET processed_date = @now WHERE processed_date IS NULL
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR4


INSERT INTO scheduled_payments (source_scheduled_payment_id, payment_method, payment_date, payment_amount, source_contract_id, contract_id, source_merchant_id, 
	merchant_id, source_affiliate_id, affiliate_id, is_debit, create_date, change_date, change_user, create_user, bank_name, routing_number, account_number, changed, exported)
SELECT i.scheduled_payment_id, i.payment_method, i.payment_date, i.payment_amount, i.contract_id, c.contract_id, i.merchant_id, m.merchant_id, i.affiliate_id, 
	a.affiliate_id, CASE WHEN LOWER(i.is_debit) = 'true' THEN 1 ELSE 0 END, GETUTCDATE(), GETUTCDATE(), 'Import', 'Import', i.bank_name, i.routing_number, i.account_number,
	0, NULL
FROM import_scheduled_payments i
LEFT JOIN contracts c ON i.contract_id = c.source_contract_id
LEFT JOIN merchants m ON i.merchant_id = m.source_merchant_id
LEFT JOIN affiliates a ON i.affiliate_id = a.source_affiliate_id
WHERE i.processed_date = @now AND COALESCE(i.scheduled_payment_id, '') != ''
	AND NOT EXISTS (SELECT 1 FROM scheduled_payments p WHERE p.source_scheduled_payment_id = i.scheduled_payment_id)

SELECT @scheduled_payments_added = @@ROWCOUNT, @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR4

-- now, in case any did not actually update for some reason flag as bad record
UPDATE i
SET bad_record = 1
FROM import_scheduled_payments i
WHERE processed_date = @now 
	AND (NOT EXISTS(SELECT 1 FROM scheduled_payments p WHERE p.source_scheduled_payment_id = i.scheduled_payment_id) OR i.scheduled_payment_id IS NULL)	

SELECT @bad_scheduled_payment_records = @@ROWCOUNT, @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR4



COMMIT TRANSACTION
GOTO Done4

ERROR4:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)
	GOTO TheEnd

Done4:

*/

/*
------------------------ BANK ACCOUNTS ------------------------------
BEGIN TRANSACTION

UPDATE import_affiliate_bank_accounts SET processed_date = @now WHERE processed_date IS NULL
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR4


INSERT INTO affiliate_bank_accounts (source_affiliate_bank_account_id, source_affiliate_id, affiliate_id, source_merchant_id, bank_name, routing_number, account_number, 
	account_status, nickname, create_date, change_date, change_user, create_user)
SELECT i.affiliate_bank_account_id, i.affiliate_id, a.affiliate_id, i.merchant_id, i.bank_name, i.routing_number, i.account_number, i.account_status, i.nickname,
	GETUTCDATE(), GETUTCDATE(), 'Import', 'Import'
FROM import_affiliate_bank_accounts i
LEFT JOIN affiliates a ON i.affiliate_id = a.source_affiliate_id
WHERE i.processed_date = @now AND COALESCE(i.affiliate_bank_account_id, '') != ''
	AND NOT EXISTS (SELECT 1 FROM affiliate_bank_accounts ab WHERE ab.source_affiliate_bank_account_id = i.affiliate_bank_account_id)

SELECT @bank_accounts_added = @@ROWCOUNT, @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR5

-- now, in case any did not actually update for some reason flag as bad record
UPDATE i
SET bad_record = 1
FROM import_affiliate_bank_accounts i
WHERE processed_date = @now 
	AND (NOT EXISTS(SELECT 1 FROM affiliate_bank_accounts ab WHERE ab.source_affiliate_bank_account_id = i.affiliate_bank_account_id) OR i.affiliate_bank_account_id IS NULL)	

SELECT @bad_bank_account_records = @@ROWCOUNT, @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR5



COMMIT TRANSACTION
GOTO Done5

ERROR5:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)
	GOTO TheEnd

Done5:

*/

TheEnd:

SELECT @affiliates_added AS affiliates_added, @merchants_added AS merchants_added, @contracts_added AS contracts_added, @bad_contract_records AS bad_contract_records, 
	@transactions_added AS transactions_added, @bad_trans_records AS bad_trans_records, @contacts_added AS contacts_added, @contact_addresses_added AS contact_addresses_added, 
	@contact_phones_added AS contact_phones_added, @bad_contact_records AS bad_contact_records, @bad_contact_phones_records AS bad_contact_phones_records, 
	@bad_contact_addresses_records AS bad_contact_addresses_records, @scheduled_payments_added AS scheduled_payments_added, 
	@bad_scheduled_payment_records AS bad_scheduled_payment_records, @bank_accounts_added AS bank_accounts_added, @bad_bank_account_records AS bad_bank_account_records

INSERT INTO processed_imports(affiliates_added, merchants_added, contracts_added, bad_contract_records, transactions_added, bad_trans_records, contacts_added, 
	contact_addresses_added, contact_phones_added, bad_contact_records, bad_contact_phones_records, bad_contact_addresses_records, scheduled_payments_added, 
	bad_scheduled_payment_records, bank_accounts_added, bad_bank_account_records, create_date, change_date, create_user, change_user)
VALUES (@affiliates_added, @merchants_added, @contracts_added, @bad_contract_records, @transactions_added, @bad_trans_records, @contacts_added, @contact_addresses_added,
	@contact_phones_added, @bad_contact_records, @bad_contact_phones_records, @bad_contact_addresses_records, @scheduled_payments_added, @bad_scheduled_payment_records, 
	@bank_accounts_added, @bad_bank_account_records, GETUTCDATE(), GETUTCDATE(), 'SYSTEM', 'SYSTEM')




END

GO
