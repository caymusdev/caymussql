SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[CSP_InsPaymentSchedule] 
(
	@payment_method			NVARCHAR (50),
	@frequency				NVARCHAR (50),
	@amount					MONEY,
	@start_date				DATETIME,
	@cc_percentage			NUMERIC(10,2),
	@bank_name				NVARCHAR (100),
	@routing_number			NVARCHAR (50),
	@account_number			NVARCHAR (50),
	@payment_processor		NVARCHAR (50),
	@funding_id				BIGINT,
	@userid					NVARCHAR (100),
	@end_date				DATETIME = NULL,
	@active					BIT = 1,
	@approved_by			NVARCHAR (100) = NULL,
	@change_id				INT = NULL,
	@comments				NVARCHAR (4000) = NULL,
	@change_reason_id		INT = NULL,
	@part_of_plan			BIT = NULL
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @newID INT
DECLARE @now DATETIME; SET @now = GETUTCDATE()

-- First need to check and see if this user needs approval for editing/creating payment schedules
DECLARE @needs_approval BIT; SET @needs_approval = 0
--DECLARE @pending_perf_status INT; SELECT @pending_perf_status = id FROM payment_schedule_status WHERE code = 'Pending' AND parent_id IS NULL
DECLARE @pending_perf_status INT; SELECT @pending_perf_status = id FROM payment_plan_status_codes WHERE code = 'Pending' AND parent_id IS NULL
DECLARE @payment_plan_id INT
SELECT @payment_plan_id = pp.payment_plan_id
FROM payment_plans pp
INNER JOIN fundings f ON pp.merchant_id = f.merchant_id AND f.id = @funding_id
WHERE COALESCE(@part_of_plan, 0) = 1


IF @approved_by IS NULL AND EXISTS (SELECT 1 FROM sec_payment_schedules s WHERE s.submitter_user_id = @userid)
	BEGIN
		SET @needs_approval = 1
		
		INSERT INTO payment_schedule_changes (change_type, payment_schedule_id, payment_method, frequency, amount, start_date, bank_name, routing_number, account_number, 
			payment_processor, funding_id, userid, end_date, active, comments, change_reason_id, part_of_plan)
		VALUES ('Insert', NULL, @payment_method, @frequency,CAST(ROUND(@amount, 2) AS money), @start_date, @bank_name, @routing_number, @account_number, @payment_processor,
			@funding_id, @userid, @end_date, @active, @comments, @change_reason_id, @part_of_plan)

		SELECT 1 AS NeedsApproval
	END
ELSE IF @approved_by IS NULL
	BEGIN
		-- so, there is NOT a security record which means they have no need to get approved separaretly.
		SET @approved_by = @userid
	END

IF @needs_approval = 0
	BEGIN
		
		IF @payment_plan_id IS NULL AND @part_of_plan = 1
			BEGIN
				-- need to create it since it does not exist
				INSERT INTO payment_plans(merchant_id, affiliate_id, collector, current_status)
				SELECT f.merchant_id, f.affiliate_id, @userid, @pending_perf_status
				FROM fundings f WITH (NOLOCK)
				WHERE f.id = @funding_id

				SELECT @payment_plan_id = SCOPE_IDENTITY()

				INSERT INTO payment_plan_statuses (payment_plan_id, payment_plan_status_code, start_date, end_date)
				SELECT @payment_plan_id, c.id, CONVERT(DATE, GETUTCDATE()), NULL
				FROM payment_plan_status_codes c
				WHERE c.code = 'Pending'
			END

		INSERT INTO payment_schedules (payment_method, frequency, amount, start_date, cc_percentage, bank_name, routing_number, account_number, 
			payment_processor, change_user, change_date, end_date, active, payment_schedule_change_id, comments, change_reason_id, performance_status_id, payment_plan_id)
		VALUES (@payment_method, @frequency,CAST(ROUND( @amount, 2) AS money), @start_date, @cc_percentage, @bank_name, @routing_number, @account_number, @payment_processor,
			@userid, @now, @end_date, @active, @change_id, @comments, @change_reason_id, @pending_perf_status, @payment_plan_id)

			-- get new id and put into many to many table with the funding id
		SELECT @newID = SCOPE_IDENTITY()

		INSERT INTO funding_payment_schedules(funding_id, payment_schedule_id, change_user, change_date)
		VALUES (@funding_id, @newID, @userid, @now)

		-- update the promise payment
		-- UPDATE promise_payments SET payment_schedule_id = @newID WHERE promise_payment_id = @promise_payment_id

		
		-- now add bank account if necessary
		INSERT INTO merchant_bank_accounts (merchant_id, bank_name, routing_number, account_number, account_status, change_date, change_user)
		SELECT f.merchant_id, @bank_name, @routing_number, @account_number, 'Active', GETUTCDATE(), @userid
		FROM fundings f
		WHERE f.id = @funding_id
			AND NOT EXISTS (SELECT 1 FROM merchant_bank_accounts ba 
							WHERE ba.merchant_id = f.merchant_id AND ba.bank_name = @bank_name AND ba.routing_number = @routing_number AND ba.account_number = @account_number)
			AND COALESCE(@bank_name, '') <> ''

		IF @change_id IS NOT NULL
			BEGIN
				UPDATE payment_schedule_changes
				SET payment_schedule_id = @newID
				WHERE id = @change_id AND payment_schedule_id IS NULL
			END


		EXEC dbo.CSP_GeneratePaymentsForFunding @funding_id, @userid


		EXEC dbo.CSP_SyncMerchantBankAccountsAndSchedules 

		SELECT 0 AS NeedsApproval
	END

END
GO
