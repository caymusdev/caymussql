SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Browm
-- Create Date: 2020-09-18
-- Description: Adds user to users table
-- =============================================
CREATE PROCEDURE dbo.BSK_InsUser
(
	@email				NVARCHAR (100),
	@timezone			NVARCHAR (100),
	@change_user		NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON
DECLARE @ErrorMessage NVARCHAR (MAX)

IF EXISTS (SELECT 1 FROM users WHERE email = @email)
	BEGIN		
		SELECT @ErrorMessage = dbo.BF_TranslateWord('RecordExists')
		RAISERROR (@ErrorMessage, 16, 1)
	END
ELSE 
	BEGIN
		INSERT INTO users (email, hash_password, salt, timezone, create_date, change_date, change_user, create_user)
		Values (@email, NULL, NULL, @timezone, GETUTCDATE(), GETUTCDATE(), @change_user, @change_user)
	END

END

GO
