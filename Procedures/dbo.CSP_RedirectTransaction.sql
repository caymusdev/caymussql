SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-08-15
-- Description:	Creates a transaction for the redirected to funding.  Used when drafting, receiving, rejecting, chargeback
-- =============================================
CREATE PROCEDURE [dbo].[CSP_RedirectTransaction]
(
	@redirect_approval_id			INT,
	@trans_amount					MONEY,
	@batch_id						INT,
	@record_id						BIGINT,
	@record_type					VARCHAR (50),
	@transaction_id					NVARCHAR (100),
	@payment_id						BIGINT,
	@trans_date						DATETIME,
	@orig_trans_id					INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE @comments VARCHAR (500), @redirect_funding_id BIGINT, @redirect_trans_id INT, @orig_trans_type_id INT
DECLARE @needs_dist BIT; SET @needs_dist = 1
DECLARE @portfolio VARCHAR (50)

SELECT @portfolio = f.portfolio
FROM fundings f 
WHERE f.id = (SELECT to_funding_id FROM redirect_approvals WHERE redirect_approval_id = @redirect_approval_id)

SET @portfolio = COALESCE(@portfolio, 'caymus')

SELECT @comments = 'Redirect due to Continuous Pull of ' + COALESCE(f.contract_number, CONVERT(VARCHAR (50), f.id), ''), 
	@redirect_funding_id = ra.to_funding_id
FROM redirect_approvals ra WITH (NOLOCK) 
INNER JOIN fundings f WITH (NOLOCK) ON ra.from_funding_id = f.id
WHERE ra.redirect_approval_id = @redirect_approval_id

SELECT @orig_trans_type_id = trans_type_id FROM transactions WITH (NOLOCK) WHERE trans_id = @orig_trans_id

IF @orig_trans_type_id = 3 AND EXISTS (SELECT 1 
			FROM transactions t WITH (NOLOCK)
			INNER JOIN transactions t2 WITH (NOLOCK) ON t.redirect_from_id = t2.trans_id
			WHERE t.funding_id = @redirect_funding_id AND t.transaction_id = @transaction_id AND t.trans_type_id = 26 AND t2.trans_type_id = 18) 
	SET @needs_dist = 0
		
INSERT INTO transactions (trans_date, funding_id, trans_amount, trans_type_id, comments, batch_id, record_id, previous_id, redistributed, record_type, transaction_id, payment_id,
	redirect_approval_id, redirect_from_id, change_user, change_date, portfolio)
SELECT @trans_date, @redirect_funding_id, @trans_amount, 26, @comments, @batch_id, @record_id, NULL, 0, @record_type, @transaction_id, @payment_id, @redirect_approval_id, @orig_trans_id, 
	'SYSTEM', GETUTCDATE(), @portfolio

SELECT @redirect_trans_id = SCOPE_IDENTITY()

IF @needs_dist = 1
	BEGIN
		EXEC dbo.CSP_ApplyPPAndMargin @redirect_funding_id, @trans_amount, @redirect_trans_id, @comments
	END
ELSE
	BEGIN
		INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
		VALUES (@redirect_trans_id, @orig_trans_type_id, @trans_amount, @comments, 'SYSTEM', GETUTCDATE())
	END
END

GO
