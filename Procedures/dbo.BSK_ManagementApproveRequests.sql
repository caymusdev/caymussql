SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Rick Pina
-- Create Date: 2021-03-01
-- Description: 
-- =============================================
CREATE PROCEDURE [dbo].[BSK_ManagementApproveRequests]
(
	@approval_ids			NVARCHAR (1000),
	@user_email				NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON

DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE());
DECLARE @yesterday DATE; SELECT @yesterday = DATEADD(DD, -1, @today)
BEGIN TRANSACTION

IF OBJECT_ID('tempdb..#tempIDs') IS NOT NULL DROP TABLE #tempIDs
CREATE TABLE #tempIDs (id INT IDENTITY (1, 1), approval_id INT, approval_type NVARCHAR(100), record_id INT, new_value INT)
 
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

INSERT INTO #tempIDs (approval_id, approval_type, record_id, new_value)
SELECT a.approval_id, at.approval_type, a.record_id, a.new_value
FROM string_split(@approval_ids, '|') v 
INNER JOIN approvals a ON v.value = a.approval_id
INNER JOIN approval_types at ON  a.approval_type_id = at.approval_type_id

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

DECLARE @maxID INT, @currentID INT, @ApprovalID INT, @ApprovalType NVARCHAR(100), @RecordID INT, @NewValue INT
SELECT @maxID = MAX(id) , @currentID = 1
FROM #tempIDs

WHILE @currentID <= @maxID
BEGIN
	
	SELECT @ApprovalID = approval_id, @ApprovalType = approval_type, @RecordID = record_id, @NewValue = new_value
	FROM #tempIDs
	WHERE id = @CurrentID

	SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR	

	IF @ApprovalType = 'PaymentChanges'
		BEGIN 
			EXEC dbo.BSK_ApproveSchedPayment @RecordID, @user_email
			SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
		END
	ELSE IF @ApprovalType = 'PaymentPlanChanges'
		BEGIN 
			-- 2021-05-19 - Need to delete out any funding_payment_dates that have this payment plan id and are active and have not processed
			UPDATE funding_payment_dates
			SET active = 0, change_date = GETUTCDATE(), change_user = @user_email
			WHERE payment_plan_id = @RecordID AND active = 1 AND payment_date > @today

			SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

			EXEC dbo.BSK_ApproveSchedPaymentPlan @RecordID, @user_email
			SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
		END
	ELSE IF @ApprovalType = 'PaymentDelete'
		BEGIN 
			EXEC dbo.BSK_ApproveSchedPayment @RecordID, @user_email
			SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
		END
	ELSE IF @ApprovalType = 'PaymentPlanDelete'
		BEGIN 
			-- 2021-05-19 - Need to delete out any funding_payment_dates that have this payment plan id and are active and have not processed
			UPDATE funding_payment_dates
			SET active = 0, change_date = GETUTCDATE(), change_user = @user_email
			WHERE payment_plan_id = @RecordID AND active = 1 AND payment_date > @today

			SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

			EXEC dbo.BSK_ApproveSchedPaymentPlan @RecordID, @user_email
			SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
		END
	ELSE IF @ApprovalType = 'ChangeCaseDepartment'
		BEGIN 			
			EXEC dbo.BSK_MoveCase @RecordID, @NewValue, @user_email

			SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
		END

	UPDATE approvals
	SET approved_by = @user_email, approved_date = GETUTCDATE()
	WHERE approval_id = @ApprovalID;

	SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

	SET @currentID = @currentID + 1
END

COMMIT TRANSACTION

-- because the above calls to other SPs might return datatsets, this has to stay the same because code is looking for the number of columns
-- as well as the names of the columns.  Update ApproveRequests in c# if you change the below select
SELECT ap.description, ap.create_user AS collector_email, a.affiliate_name, @user_email AS manager_email
FROM approvals ap
INNER JOIN #tempIDs t ON ap.approval_id = t.approval_id
LEFT JOIN affiliates a ON ap.affiliate_id = a.affiliate_id
ORDER BY ap.create_user, a.affiliate_name


GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)

Done:
	
END

GO
