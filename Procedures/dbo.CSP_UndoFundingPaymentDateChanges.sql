SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-02-04
-- Description:	Deletes from work table for the session
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UndoFundingPaymentDateChanges]
(
--	@session_id				NVARCHAR (100),
	@userid					NVARCHAR (100),
	@funding_id				BIGINT
)
AS
BEGIN	
SET NOCOUNT ON;

UPDATE funding_payment_dates_work
SET change_date = GETUTCDATE(), change_user = @userid
WHERE userid = @userid AND funding_id = @funding_id AND COALESCE(approved, 0) = 0 AND changed = 1

DELETE FROM funding_payment_dates_work 
WHERE userid = @userid AND funding_id = @funding_id AND COALESCE(approved, 0) = 0 AND changed = 1

END
GO
