SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-06-18
-- Description:	returns records for a given batch c# then loops through and creates transactions records
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetBOAStagingForBatch]
(
	@batch_id			INT
)

AS
BEGIN
SET NOCOUNT ON;

-- for BOA, we only create transactions for WIRES, everything else is done from the source.

SELECT s.boa_staging_id
FROM boa_staging s
WHERE s.BatchID = @batch_id 
	AND s.Amount != 0  -- 2022-12-02 - Davis asked to change this so that they can see when counter deposits reject (as an example)
	--AND s.Description LIKE '%WIRE TYPE:%' 
	AND s.Description NOT LIKE '%MPCF II LLC%' 
	AND s.Description NOT LIKE '%FORTE DES:%'
	AND s.Description NOT LIKE '%IPS ACH PROGRAM%' 
	AND s.Description NOT LIKE '%CO ID:1999999991%'
	AND s.Description NOT LIKE '%BKCD PROCESSING DES%' 
	AND s.Description NOT LIKE '%CO ID:9000478020%'

	-- AND s.Description LIKE '%WIRE TYPE:%' AND s.Description NOT LIKE '%MPCF II LLC%' -- this is the investors wiring in money
ORDER BY s.[Date]


END
GO
