SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-01-06
-- Description:	Creates the initial expected payment dates for fundings based on original rtr and original schedule and does not change
-- Can be run anytime as it checks for existing records.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_InsOriginalFundingPaymentDates]

AS
BEGIN	
SET NOCOUNT ON;

DECLARE @tempDate DATE;
DECLARE @funding_id BIGINT, @frequency VARCHAR (50), @payment MONEY, @first_payment_date DATE
DECLARE @maxID BIGINT, @tempID BIGINT
DECLARE @remaining_amount MONEY, @tempAmount MONEY
DECLARE @rtr MONEY

IF OBJECT_ID('tempdb..#tempFundings') IS NOT NULL DROP TABLE #tempFundings
CREATE TABLE #tempFundings (id int not null identity, funding_id BIGINT, first_payment_date DATE NULL, rtr MONEY, frequency VARCHAR (50), payment MONEY)

-- do any fundings that have not been done yet
INSERT INTO #tempFundings (funding_id, first_payment_date, rtr, frequency, payment)
SELECT f.id, f.first_payment_date, f.portfolio_value, f.frequency, 
	CASE frequency WHEN 'Daily' THEN weekday_payment ELSE weekly_payment END
FROM fundings f WITH (NOLOCK)
WHERE NOT EXISTS (SELECT 1 FROM funding_payment_dates_original orig WHERE f.id = orig.funding_id) AND f.frequency IS NOT NULL 
ORDER BY f.id


SELECT @maxID = MAX(id) FROM #tempFundings
SET @tempID = 1

WHILE @tempID <= @maxID
	BEGIN
		SELECT @funding_id = funding_id FROM #tempFundings WHERE id = @tempID

		SELECT @rtr = rtr, @frequency = frequency, @payment = payment, @first_payment_date = first_payment_date
		FROM #tempFundings 
		WHERE funding_id = @funding_id

		SET @tempDate = @first_payment_date
				
		IF @payment IS NOT NULL AND @frequency IS NOT NULL
			BEGIN
				SET @remaining_amount = @rtr 				

				WHILE @remaining_amount > 0 
					BEGIN
						SET @tempAmount = @remaining_amount - @payment
						IF @tempAmount < 0
							BEGIN
								SET @payment = @remaining_amount
							END
						IF @frequency = 'Daily'
							BEGIN
								IF DATEPART(dw, @tempDate) IN (2,3,4,5,6) -- only do Monday Through Friday even though CC might have weekend amounts
									BEGIN
										INSERT INTO funding_payment_dates_original(funding_id, payment_date, amount, rec_amount)
										SELECT @funding_id, @tempDate, @payment, 0

										SET @remaining_amount = @remaining_amount - @payment
									END
							END
						ELSE IF @frequency = 'Weekly'
							BEGIN
								IF DATEPART(dw, @tempDate) = DATEPART(dw, @first_payment_date)
									BEGIN
										-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
										INSERT INTO funding_payment_dates_original(funding_id, payment_date, amount, rec_amount)
										SELECT @funding_id, @tempDate, @payment, 0

										SET @remaining_amount = @remaining_amount - @payment
									END
							END
						ELSE IF @frequency = 'Monthly'
							BEGIN
								IF DAY(@tempDate) = DAY(@first_payment_date)
									BEGIN
										-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
										INSERT INTO funding_payment_dates_original(funding_id, payment_date, amount, rec_amount)
										SELECT @funding_id, @tempDate, @payment, 0

										SET @remaining_amount = @remaining_amount - @payment
									END
							END
						-- increment @tempDate
						SET @tempDate = DATEADD(D, 1, @tempDate)												
					END	
			END
		SET @tempID = @tempID + 1
	END

UPDATE pd
SET rec_amount = COALESCE(p.amount, 0)
FROM funding_payment_dates_original pd
INNER JOIN payments p ON pd.funding_id = p.funding_id AND pd.payment_date = p.trans_date AND p.transaction_id IS NOT NULL
WHERE pd.payment_date BETWEEN GETDATE() - 14 AND GETDATE()


IF OBJECT_ID('tempdb..#tempFundings') IS NOT NULL DROP TABLE #tempFundings

END
GO
