SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-02-26
-- Description:	Logs a nacha file
-- =============================================
CREATE PROCEDURE CSP_LogNachaFile
(
	@nacha_file_name			VARCHAR (100),
	@batch_id					INT
)
AS
BEGIN

SET NOCOUNT ON;

INSERT INTO nacha_logs (batch_id, nacha_file_name, file_data, processed_date, proc_status, create_date, change_user)
VALUES (@batch_id, @nacha_file_name, NULL, GETUTCDATE(), 'Processed', GETUTCDATE(), 'SYSTEM')

END
GO
