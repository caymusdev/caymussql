SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-14
-- Description:	Saves an email address into the database
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsEmailAddress]
(
	@email_id				INT,
	@email_address			NVARCHAR (1000), 
	@address_type			VARCHAR (50),
	@user_email				NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON;

INSERT INTO email_addresses (email_id, email_address, address_type, create_date, change_date, change_user, create_user)
VALUES (@email_id, @email_address, @address_type, GETUTCDATE(), GETUTCDATE(), @user_email, @user_email)

END

GO
