SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-02-10
-- Description:	gets metadata for fundings to be used on the page for various spots.  Also gets the last original payment date and sends the payment info for it.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetFundingInfoForPaymentDates]
(	
	@funding_id			BIGINT
)
AS
BEGIN	
SET NOCOUNT ON;
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SELECT @tomorrow = dbo.CF_GetTomorrow(@today)

SELECT f.funded_date, f.legal_name, f.contract_number, f.payoff_amount, f.merchant_id, ba.merchant_bank_account_id, 
	CASE f.hard_offer_type WHEN 'Split' THEN 'CC' ELSE f.hard_offer_type END AS payment_method, f.processor AS payment_processor, 
	CASE f.frequency WHEN 'Daily' THEN f.weekday_payment ELSE f.weekly_payment END AS amount, NULL AS end_date, f.frequency, 
	CONVERT(VARCHAR(50), ba.merchant_bank_account_id) + '|' + ba.bank_name + '|' + ba.routing_number + '|' + ba.account_number AS merchant_bank_account, 
	COALESCE(f.on_hold, 0) AS on_hold, x.off_hold_date, f.estimated_final_payment_date
FROM fundings f WITH (NOLOCK) 
--LEFT JOIN (
--	SELECT d.funding_id, MIN(d.payment_date) AS LastPaymentDate
--	FROM funding_payment_dates d WITH (NOLOCK)
--	WHERE COALESCE(d.original, 0) = 1
--	GROUP BY d.funding_id
--) x ON f.id = x.funding_id
-- LEFT JOIN funding_payment_dates d WITH (NOLOCK) ON x.funding_id = d.funding_id AND d.payment_date = x.LastPaymentDate AND COALESCE(d.original, 0) = 1
LEFT JOIN merchant_bank_accounts ba WITH (NOLOCK) ON f.merchant_id = ba.merchant_id AND f.receivables_bank_name = ba.bank_name AND f.receivables_aba = ba.routing_number
	AND f.receivables_account_nbr = ba.account_number
LEFT JOIN (
	SELECT o.funding_id, MIN(o.off_hold_date) AS off_hold_date
	FROM funding_off_hold_dates o
	WHERE o.off_hold_date >= @today AND o.processed_date IS NULL
	GROUP BY o.funding_id
) x ON f.id = x.funding_id
WHERE f.id = @funding_id

END
GO
