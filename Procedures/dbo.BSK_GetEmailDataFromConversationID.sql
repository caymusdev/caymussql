SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-11-04
-- Description:	Returns affiliate_id, merchant_id, contract_id, etc for a certain message_id in emails.
-- Used when an email is received to try and match up with affiliate
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetEmailDataFromConversationID]
(
	@message_id				NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON;

SELECT TOP 1 affiliate_id, merchant_id, contract_id
FROM emails e
WHERE e.message_id = @message_id
	AND (e.affiliate_id IS NOT NULL OR e.merchant_id IS NOT NULL OR e.contract_id IS NOT NULL)
ORDER BY e.create_date

	
END

GO
