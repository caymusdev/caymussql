SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[CSP_DistributeTransaction_v2]
(
	@funding_id				BIGINT, 
	@trans_amount			MONEY, 
	@transaction_id			NVARCHAR (100), 
	@trans_date				DATETIME, 
	@comments				NVARCHAR (500), 
	@batch_id				INT,
	@record_id				INT, 
	@record_type			VARCHAR (50), 
	@previous_id			INT, 
	@settle_date			DATETIME, 
	@settle_type			NVARCHAR (50), 
	@response_code			VARCHAR (50),
	@trans_type_id			INT = NULL,
	@portfolio				VARCHAR (50) = NULL
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''

IF @portfolio IS NULL
	BEGIN
		SELECT @portfolio = COALESCE(portfolio, 'caymus') FROM fundings WHERE id = @funding_id
		-- funding id can be null resulting in @portfolio still null
		IF @portfolio IS NULL SET @portfolio = 'caymus'
	END

BEGIN TRANSACTION

DECLARE @has_payment BIT, @has_ACHDraft BIT, @trans_id INT, @temp_amount MONEY, @newFeeTransId INT, @temp_trans_id INT, @on_hold BIT, @is_fee BIT, @process_fees BIT
DECLARE @contract_status INT, @pricing_ratio NUMERIC(10, 4), @temp_trans_date DATETIME, @redirect_approval_id INT, @redirect_funding_id BIGINT, @rowCount INT
SELECT @has_payment = 0, @has_ACHDraft = 0

DECLARE @feeAmount MONEY; SELECT @feeAmount = COALESCE(dbo.CF_GetSetting('FeeAmount'), 72)
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SELECT @tomorrow = DATEADD(DD, CASE DATEPART(WEEKDAY, @today) WHEN 6 THEN 3 WHEN 7 THEN 2 ELSE 1 END, @today)
DECLARE @isFifthThird BIT;  SET @isFifthThird = ISNUMERIC(@transaction_id)

-- get the redirect approval id if there is one
SELECT @redirect_approval_id = t.redirect_approval_id FROM transactions t WITH (NOLOCK) WHERE t.transaction_id = @transaction_id AND t.trans_type_id = 18
SELECT @redirect_funding_id = ra.to_funding_id FROM redirect_approvals ra WITH (NOLOCK) WHERE ra.redirect_approval_id = @redirect_approval_id AND @redirect_approval_id IS NOT NULL

-- get on_hold value.  Don't create payments for fees if they are on hold.
SELECT @on_hold = COALESCE(on_hold, 0), @process_fees = COALESCE(process_fees, 1) FROM fundings WHERE id = @funding_id

-- first see if this is a transaction we sent out
IF @transaction_id IS NOT NULL AND EXISTS (SELECT 1 FROM payments p WHERE p.transaction_id = @transaction_id) SET @has_payment = 1
IF @transaction_id IS NOT NULL AND EXISTS (SELECT 1 FROM transactions t WHERE t.funding_id = @funding_id AND t.transaction_id = @transaction_id AND t.trans_type_id = 18) SET @has_ACHDraft = 1
SELECT @is_fee = COALESCE(p.is_fee, 0) FROM payments p WHERE p.transaction_id = @transaction_id

IF @settle_type IN ('WIRE', 'CC', 'CounterCredit')
	BEGIN
		-- insert wire received
		INSERT INTO transactions (trans_date, funding_id, trans_amount, trans_type_id, comments, batch_id, record_id, previous_id, redistributed, settle_date, record_type, transaction_id, 
			redirect_approval_id, change_user, change_date, settle_code, portfolio)
		VALUES (@trans_date, @funding_id, @trans_amount, @trans_type_id, @comments, @batch_id, @record_id, @previous_id, 0, @settle_date, @record_type, @transaction_id, 
			@redirect_approval_id, 'SYSTEM', GETUTCDATE(), @response_code, @portfolio)

		SELECT @trans_id = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR
		
		EXEC dbo.CSP_ApplyPPAndMargin @funding_id, @trans_amount, @trans_id, @comments

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
	END  -- @settle_type = wire
ELSE IF @trans_type_id IS NOT NULL
	BEGIN
		INSERT INTO transactions (trans_date, funding_id, trans_amount, trans_type_id, comments, batch_id, record_id, previous_id, redistributed, transaction_id, record_type, settle_date, 
			redirect_approval_id, change_user, change_date, settle_code, portfolio)
		SELECT @trans_date, @funding_id, @trans_amount, @trans_type_id, @comments, @batch_id, @record_id, @previous_id, 0, @transaction_id, @record_type, @settle_date, 
			@redirect_approval_id, 'SYSTEM', GETUTCDATE(), @response_code, @portfolio

		SELECT @trans_id = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR

		IF @trans_type_id IN (1, 8, 9, 11, 17, 23, 25)
			BEGIN
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				VALUES (@trans_id, @trans_type_id, @trans_amount, @comments, 'SYSTEM', GETUTCDATE())

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR

			-- if the trans type is writeoff then also writeoff the contract
				IF @trans_type_id = 11
					BEGIN
						UPDATE fundings SET contract_status = 4, writeoff_date = @trans_date, contract_substatus_id = 1, change_user = 'SYSTEM2', change_date = GETUTCDATE()
						WHERE id = @funding_id

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
						IF @errorNum != 0 GOTO ERROR
					END
			END
		ELSE --IF @trans_type_id IN (18)
			BEGIN
				EXEC dbo.CSP_ApplyPPAndMargin @funding_id, @trans_amount, @trans_id, @comments
			END
	END  -- @trans_type_id IS NOT NULL
ELSE IF @settle_type = 'deposit' -- All S01's are 'deposit'
	BEGIN
		-- no matter what we are going to record ACH received
		INSERT INTO transactions (trans_date, funding_id, trans_amount, trans_type_id, comments, batch_id, record_id, previous_id, redistributed, settle_date, record_type, transaction_id,
			redirect_approval_id, change_user, change_date, settle_code, portfolio)
		VALUES (@trans_date, @funding_id, @trans_amount, 3, @comments, @batch_id, @record_id, @previous_id, 0, @settle_date, @record_type, @transaction_id, @redirect_approval_id, 
			'SYSTEM', GETUTCDATE(), @response_code, @portfolio)

		SELECT @trans_id = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR

		IF @has_ACHDraft = 1 AND @redirect_approval_id IS NULL
			BEGIN
				-- since there is an ACH Draft, just save an ACH Recieved.  No need to distribute as it was done when the payment was requested.			
				-- need matching details so we'll create a detail of the same trans type
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				VALUES (@trans_id, 3, @trans_amount, @comments, 'SYSTEM', GETUTCDATE())

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR
			END
		ELSE IF @redirect_approval_id IS NOT NULL
			BEGIN
				-- this money is being redirected so we'll log it as such
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				VALUES (@trans_id, 26, @trans_amount, @comments, 'SYSTEM', GETUTCDATE())

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE();IF @errorNum != 0 GOTO ERROR

				EXEC dbo.CSP_RedirectTransaction @redirect_approval_id, @trans_amount, @batch_id, @record_id, @record_type, @transaction_id, NULL, @trans_date, @trans_id
			END
		ELSE
			BEGIN
				EXEC dbo.CSP_ApplyPPAndMargin @funding_id, @trans_amount, @trans_id, @comments

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR
			END -- @has_ACHDraft = 0
	END -- @settle_type = 'deposit'
ELSE IF @settle_type = 'withdrawal' -- this seems to always be a chargeback
	BEGIN
		-- enter the chageback record
		INSERT INTO transactions (trans_date, funding_id, trans_amount, trans_type_id, comments, batch_id, record_id, previous_id, redistributed, settle_date, record_type, transaction_id,
			redirect_approval_id, change_user, change_date, settle_code, portfolio)
		VALUES (@trans_date, @funding_id, @trans_amount, 14, @comments, @batch_id, @record_id, @previous_id, 0, @settle_date, @record_type, @transaction_id, @redirect_approval_id, 
			'SYSTEM', GETUTCDATE(), @response_code, @portfolio)

		SELECT @trans_id = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR
		-- if it has a payment already, negative out what it originally did
		-- if it does not have a payment then do -purchase price and -margin or -bad debt recovery
		IF @has_ACHDraft = 1 AND @redirect_approval_id IS NULL
			BEGIN
				SELECT @temp_trans_id = trans_id
				FROM transactions t WITH (NOLOCK)
				WHERE t.transaction_id = @transaction_id AND t.trans_type_id = 18 AND COALESCE(t.redistributed, 0) = 0

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR

				-- put in the negative of whatever was originally done
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				SELECT @trans_id, d.trans_type_id, -1.00 * d.trans_amount, d.comments, 'SYSTEM', GETUTCDATE()
				FROM transaction_details d
				WHERE d.trans_id = @temp_trans_id

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR
			END -- end @@has_ACHDraft = 1
		ELSE IF @redirect_approval_id IS NOT NULL
			BEGIN
				-- this money is being redirected so we'll log it as such
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				VALUES (@trans_id, 26, @trans_amount, @comments, 'SYSTEM', GETUTCDATE())

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE();IF @errorNum != 0 GOTO ERROR

				EXEC dbo.CSP_RedirectTransaction @redirect_approval_id, @trans_amount, @batch_id, @record_id, @record_type, @transaction_id, NULL, @trans_date, @trans_id

				SELECT @on_hold = 1, @funding_id = @redirect_funding_id
			END
		ELSE 
			BEGIN
				IF @contract_status = 1
					BEGIN
						-- apply to purchase price and margin
						SET @temp_amount = ROUND(@trans_amount / @pricing_ratio, 2)

						INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
						VALUES (@trans_id, 7, @temp_amount, @comments, 'SYSTEM', GETUTCDATE())

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
						IF @errorNum != 0 GOTO ERROR

						-- now do margin
						INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
						VALUES (@trans_id, 6, @trans_amount - @temp_amount, @comments, 'SYSTEM', GETUTCDATE())

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
						IF @errorNum != 0 GOTO ERROR
					END
				ELSE IF @contract_status = 4 
					BEGIN
						INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
						VALUES (@trans_id, 15, @trans_amount, '', 'SYSTEM', GETUTCDATE())

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
						IF @errorNum != 0 GOTO ERROR
					END
				ELSE  -- contract status not in (1,4)
					BEGIN
						-- these are either not active or writeoff contracts.  Shouldn't be having money anyway so if there is, by not inserting a details
						-- an alert will be generated where these will be caught.
						PRINT 0
					END  -- end @contract_status
			END  -- @@has_ACHDraft = 0

		-- now need to assess a fee

		-- if it has a payment then the trans date might be a day earlier (forte records the date we sent it, not the date they took it out)
		IF @has_payment = 1 AND @transaction_id IS NOT NULL
			BEGIN
				SELECT @temp_trans_date = p.trans_date
				FROM payments p
				WHERE p.transaction_id = @transaction_id

				IF CONVERT(DATE, @trans_date) < CONVERT(DATE, @temp_trans_date)
					BEGIN
						SET @trans_date = @temp_trans_date
					END
			END

		-- insert a new transaction for a fee and for it's detail
		INSERT INTO transactions (trans_date, funding_id, trans_amount, trans_type_id, comments, batch_id, record_id, previous_id, redistributed, 
			settle_date, record_type, transaction_id, redirect_approval_id, change_user, change_date, portfolio)
		SELECT @trans_date, @funding_id, @feeAmount, 9, 'Fee for ' + @response_code + ' : ' + @settle_type, @batch_id, @record_id, @previous_id, 0,
			@settle_date, @record_type, @transaction_id, @redirect_approval_id, 'SYSTEM', GETUTCDATE(), @portfolio
	
		SELECT @newFeeTransId = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR

		INSERT INTO transaction_details(trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
		SELECT @newFeeTransId, 9, @feeAmount, 'Fee for ' + @response_code + ' : ' + @settle_type, 'SYSTEM', GETUTCDATE()

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR

		-- create payment for the fee if not on hold and if not for a fee
		IF @on_hold = 0 AND @is_fee = 0 AND @process_fees = 1
			BEGIN
				INSERT INTO payments(payment_schedule_id, amount, trans_date, transaction_id, approved_flag, processed_date, change_date, change_user, previous_transaction_id,
					complete, retry_level, is_fee, funding_id, orig_transaction_id, retry_sublevel)
				VALUES (NULL, @feeAmount, @tomorrow, NULL, 1, NULL, GETUTCDATE(), 'SYSTEM', NULL, 0, 0, 1, @funding_id, @transaction_id, 0)

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR
			END

		-- create a retry for this failed payment
		IF @redirect_approval_id IS NULL
			BEGIN
				EXEC dbo.CSP_CreateRetryPayments @transaction_id, @funding_id, @trans_amount
			END

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR

		IF (@response_code <> 'R01' AND @response_code <> 'R09' AND @redirect_approval_id IS NULL) OR (@response_code LIKE 'U%')
			BEGIN
				-- this means it is a bad R code so put the account on hold
				UPDATE fundings SET on_hold = 1, change_date = GETUTCDATE(), change_user = 'SYSTEM', on_hold_date = GETUTCDATE(), payments_stopped_date = GETUTCDATE()
				WHERE id = @funding_id

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR

				EXEC dbo.BSK_MarkFundingCaseActive @funding_id

				-- RKB : 2019-05-07 -- deactivate all active payment schedules for this funding since it is now on hold
				-- no longer do this.  funding payment dates will be marked as missed if the account is on hold when the payment should process
				--UPDATE ps
				--SET active = 0
				--FROM payment_schedules ps 
				--INNER JOIN funding_payment_schedules fps ON ps.payment_schedule_id = fps.payment_schedule_id
				--WHERE fps.funding_id = @funding_id AND ps.active = 1

				--SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				--IF @errorNum != 0 GOTO ERROR
			END
		ELSE IF @response_code <> 'R01' AND @response_code <> 'R09' AND @redirect_approval_id IS NOT NULL
			BEGIN
				UPDATE redirect_approvals SET on_hold = 1, change_date = GETUTCDATE(), change_user = 'SYSTEM' 
				WHERE redirect_approval_id = @redirect_approval_id
			END
	END -- @settle_type = 'withdrawal'
ELSE IF @settle_type = 'reject'
	BEGIN
		-- get some info off of the related transaction
		-- assess fee and put account on hold if needed.
		DECLARE @trans_status VARCHAR (50)--, @orig_date DATETIME
		SELECT @trans_status = t.status--, @orig_date = COALESCE(p.trans_date, t.origination_date)
		FROM forte_trans t  -- NO NEED TO CHECK FIFTHI THIRD HERE BECAUSE THERE ARE NO REJECTS.  ALL REJECTS ARE WITHDRAWLS
		LEFT JOIN payments p ON t.transaction_id = p.transaction_id
		WHERE t.transaction_id = @transaction_id AND t.status NOT IN ('settling')

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR

		-- if it has a payment then the trans date might be a day earlier (forte records the date we sent it, not the date they took it out)
		IF @has_payment = 1 AND @transaction_id IS NOT NULL
			BEGIN
				SELECT @temp_trans_date = p.trans_date
				FROM payments p
				WHERE p.transaction_id = @transaction_id

				IF @temp_trans_date IS NOT NULL AND CONVERT(DATE, @trans_date) < CONVERT(DATE, @temp_trans_date)
					BEGIN
						SET @trans_date = @temp_trans_date  -- 
					END
			END

		-- need to get temp trans id here because 53 rejects show $0.00 so we have to know the amount first.
		SELECT @temp_trans_id = trans_id
		FROM transactions t WITH (NOLOCK)
		WHERE t.transaction_id = @transaction_id AND t.trans_type_id = 18 AND COALESCE(t.redistributed, 0) = 0

		IF @trans_amount = 0
			BEGIN
				-- This is Fifth Third reject -- did not know they did reject until today (2020-04-08)
				SELECT @trans_amount = trans_amount FROM transactions WHERE trans_id = @temp_trans_id
			END

		-- record a ACH Reject to then zero out the old stuff against.
		INSERT INTO transactions (trans_date, funding_id, trans_amount, trans_type_id, comments, batch_id, record_id, previous_id, redistributed, settle_date, record_type, transaction_id, 
			redirect_approval_id, change_user, change_date, settle_code, portfolio)
		VALUES (@trans_date, @funding_id, @trans_amount * -1.00, 21, @comments, @batch_id, @record_id, @previous_id, 0, @settle_date, @record_type, @transaction_id, @redirect_approval_id, 
			'SYSTEM', GETUTCDATE(), @response_code, @portfolio)

		SELECT @trans_id = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR

		IF @has_ACHDraft = 1 AND @redirect_approval_id IS NULL
			BEGIN
				-- put in the negative of whatever was originally done
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				SELECT @trans_id, d.trans_type_id, -1.00 * d.trans_amount, d.comments, 'SYSTEM', GETUTCDATE()
				FROM transaction_details d
				WHERE d.trans_id = @temp_trans_id

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR
			END -- end @@has_ACHDraft = 1
		ELSE IF @redirect_approval_id IS NOT NULL
			BEGIN
				SET @trans_amount = -1.00 * @trans_amount
				-- this money is being redirected so we'll log it as such
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				VALUES (@trans_id, 26, @trans_amount, @comments, 'SYSTEM', GETUTCDATE())

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE();IF @errorNum != 0 GOTO ERROR

				EXEC dbo.CSP_RedirectTransaction @redirect_approval_id, @trans_amount, @batch_id, @record_id, @record_type, @transaction_id, NULL, @trans_date, @trans_id

				SELECT @on_hold = 1, @funding_id = @redirect_funding_id
			END
		ELSE -- has no ACH Draft -- need to put in a 21 into details to match
			BEGIN
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				SELECT @trans_id, 21, @trans_amount * -1.00, @comments, 'SYSTEM', GETUTCDATE()

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR
			END

		-- declined are handled in a different process because they do not always have a settlement
		IF @trans_status <> 'declined'
			BEGIN
				-- if it has a payment then the trans date might be a day earlier (forte records the date we sent it, not the date they took it out)
				/*
				IF @has_payment = 1 AND @transaction_id IS NOT NULL
					BEGIN
						SELECT @temp_trans_date = p.trans_date
						FROM payments p
						WHERE p.transaction_id = @transaction_id

						IF CONVERT(DATE, @orig_date) < CONVERT(DATE, @temp_trans_date)
							BEGIN
								SET @orig_date = @temp_trans_date
							END
					END
				*/

				-- insert a new transaction for a fee and for it's detail		
				INSERT INTO transactions (trans_date, funding_id, trans_amount, trans_type_id, comments, batch_id, record_id, previous_id, redistributed, 
					settle_date, record_type, transaction_id, redirect_approval_id, change_user, change_date, portfolio)
				SELECT @trans_date, @funding_id, @feeAmount, 9, 'Fee for ' + @response_code + ' : ' + @settle_type, @batch_id, @record_id, @previous_id, 0,
					@settle_date, @record_type, @transaction_id, @redirect_approval_id, 'SYSTEM', GETUTCDATE(), @portfolio
	
				SELECT @newFeeTransId = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR

				INSERT INTO transaction_details(trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				SELECT @newFeeTransId, 9, @feeAmount, 'Fee for ' + @response_code + ' : ' + @settle_type, 'SYSTEM', GETUTCDATE()	

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR
				
				-- create payment for the fee if not on hold
				IF @on_hold = 0 AND @is_fee = 0 AND @process_fees = 1
					BEGIN
						INSERT INTO payments(payment_schedule_id, amount, trans_date, transaction_id, approved_flag, processed_date, change_date, change_user, previous_transaction_id,
							complete, retry_level, is_fee, funding_id, orig_transaction_id, retry_sublevel)
						VALUES (NULL, @feeAmount, @tomorrow, NULL, 1, NULL, GETUTCDATE(), 'SYSTEM', NULL, 0, 0, 1, @funding_id, @transaction_id, 0)

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
						IF @errorNum != 0 GOTO ERROR
					END

				IF @redirect_approval_id IS NULL
					BEGIN
						-- create a retry for this failed payment
						EXEC dbo.CSP_CreateRetryPayments @transaction_id, @funding_id, @trans_amount			
					END

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR
			END

		IF @response_code <> 'R01' AND @response_code <> 'R09' AND @redirect_approval_id IS NULL
			BEGIN
				-- this means it is a bad R code so put the account on hold
				UPDATE fundings SET on_hold = 1, change_user = 'SYSTEM', change_date = GETUTCDATE() , on_hold_date = GETUTCDATE(), payments_stopped_date = GETUTCDATE()
				WHERE id = @funding_id

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR

				EXEC dbo.BSK_MarkFundingCaseActive @funding_id
				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

				-- RKB : 2019-05-07 -- deactivate all active payment schedules for this funding since it is now on hold
				--UPDATE ps
				--SET active = 0
				--FROM payment_schedules ps 
				--INNER JOIN funding_payment_schedules fps ON ps.payment_schedule_id = fps.payment_schedule_id
				--WHERE fps.funding_id = @funding_id AND ps.active = 1

				--SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				--IF @errorNum != 0 GOTO ERROR
			END
		ELSE IF @response_code <> 'R01' AND @response_code <> 'R09' AND @redirect_approval_id IS NOT NULL
			BEGIN
				UPDATE redirect_approvals SET on_hold = 1, change_user = 'SYSTEM', change_date = GETUTCDATE()
				WHERE redirect_approval_id = @redirect_approval_id
			END

	END -- @settle_type = 'reject'

-- now check for consecutive misses to put on hold or in to customer serivce
IF @settle_type IN ('withdrawal', 'reject') AND @redirect_approval_id IS NULL
	BEGIN		
		-- first, see if we can zero out the received amount in payment_forecasts AND funding_payment_dates
		UPDATE pf
		SET rec_amount = 0, change_date = GETUTCDATE(), change_user = 'SYSTEM'
		FROM payment_forecasts pf
		INNER JOIN payments p ON pf.payment_date = p.trans_date AND pf.funding_id = p.funding_id
		WHERE p.transaction_id = @transaction_id

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		UPDATE pf
		SET rec_amount = CASE @settle_type WHEN 'reject' THEN 0 ELSE rec_amount END,
			chargeback_amount = CASE @settle_type WHEN 'withdrawal' THEN @trans_amount ELSE chargeback_amount END,
			reject_trans_id = CASE @trans_type_id WHEN 21 THEN @trans_id ELSE reject_trans_id END,
			 chargeback_trans_id = CASE @trans_type_id WHEN 14 THEN @trans_id ELSE chargeback_trans_id END,
			 change_user = 'SYSTEM', change_date = GETUTCDATE()
		FROM funding_payment_dates pf
		INNER JOIN payments p ON pf.payment_date = p.trans_date AND pf.funding_id = p.funding_id
		WHERE p.transaction_id = @transaction_id

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR		

		-- need to make sure they have at least X number of trans 
		DECLARE @transCount INT
		SELECT @transCount = COUNT(*) 
		FROM forte_settlements s
		INNER JOIN fundings f ON s.customer_id = f.contract_number
		WHERE f.id = @funding_id

		-- could be Fifth Third
		IF @transCount = 0 OR @isFifthThird = 1
			BEGIN
				SELECT @transCount = COUNT(*) FROM nacha_trans WHERE funding_id = @funding_id
			END

		DECLARE @MissedDailyCS INT, @MissedWeeklyCS INT, @MissedDailyCollections INT, @MissedWeeklyCollections INT
		
		SELECT @MissedDailyCS = dbo.CF_GetSetting('MissedDailyCS'), @MissedWeeklyCS = dbo.CF_GetSetting('MissedWeeklyCS'), 
			@MissedDailyCollections = dbo.CF_GetSetting('MissedDailyCollections'), @MissedWeeklyCollections = dbo.CF_GetSetting('MissedWeeklyCollections')

		DECLARE @frequency VARCHAR (50), @collection_type VARCHAR (50); 
		SELECT @frequency = frequency, @collection_type = COALESCE(NULLIF(collection_type, ''), 'Active_contract') FROM fundings WHERE id = @funding_id
		-- if the funding has payment schedules it is possible the frequency has changed so we need to look at the payment schedule with no end date.
		--DECLARE @tempFrequency VARCHAR (50)
		--SELECT TOP 1 @tempFrequency = frequency
		--FROM payment_schedules ps 
		--INNER JOIN funding_payment_schedules fps ON ps.payment_schedule_id = fps.payment_schedule_id
		--WHERE fps.funding_id = @funding_id AND ps.end_date IS NULL AND ps.active = 1
		--ORDER BY ps.payment_schedule_id DESC -- get the latest if there is more than one that has no end date

		-- SET @frequency = COALESCE(@tempFrequency, @frequency)

		-- collection types: Active_contract, Customer_Service, Collections, Legal

		-- if a case exists, set missing_payment = true.  if a case does not exist, create one
		
		IF NOT EXISTS (SELECT 1 
						FROM cases c
						INNER JOIN affiliates a ON c.affiliate_id = a.affiliate_id
						INNER JOIN fundings f ON a.affiliate_id = f.affiliate_id AND f.id = @funding_id
						WHERE c.active = 1)
			BEGIN
				EXEC dbo.BSK_CreateCase @funding_id, NULL
			END

		DECLARE @active_id INT, @inactive_id INT
		SELECT @active_id = cs.case_status_id FROM case_statuses cs WITH (NOLOCK) WHERE cs.status = 'Active'

		UPDATE c
		SET missed_payment = 1, change_date = GETUTCDATE(), change_user = 'SYSTEM', case_status_id = @active_id  -- make case active again if it missed payment
		FROM cases c
		INNER JOIN affiliates a ON c.affiliate_id = a.affiliate_id
		INNER JOIN fundings f ON a.affiliate_id = f.affiliate_id AND f.id = @funding_id
		WHERE c.active = 1


		IF @collection_type NOT IN ('Legal', 'Collections')
			BEGIN
				-- could be Active or in Customer Service but still check for Collections

				DECLARE @setToCollections BIT; SET @setToCollections = 0
				IF @isFifthThird = 1
					BEGIN
						-- check the last X number of transactions.  If any were S01 then ignore.  So use NOT EXISTS
						IF (NOT EXISTS 
							(SELECT 1 FROM 
								(
									SELECT TOP (CASE @frequency WHEN 'Daily' THEN @MissedDailyCollections WHEN 'Weekly' THEN @MissedWeeklyCollections END) reason_code
									FROM nacha_trans t
									WHERE t.funding_id = @funding_id
									ORDER BY t.settle_date DESC, t.reason_code -- get the rejects before the settles
								) x
								WHERE x.reason_code = 'S01'
							) AND @transCount >= CASE @frequency WHEN 'Daily' THEN @MissedDailyCollections WHEN 'Weekly' THEN @MissedWeeklyCollections END)
							BEGIN
								SET @setToCollections = 1
							END
					END
				ELSE
					BEGIN
						-- check the last X number of transactions.  If any were S01 then ignore.  So use NOT EXISTS
						IF (NOT EXISTS 
							(SELECT 1 FROM 
								(
									SELECT TOP (CASE @frequency WHEN 'Daily' THEN @MissedDailyCollections WHEN 'Weekly' THEN @MissedWeeklyCollections END) settle_response_code
									FROM forte_settlements s
									INNER JOIN fundings f ON s.customer_id = f.contract_number
									WHERE f.id = @funding_id
									ORDER BY settle_date DESC, s.settle_response_code -- get the rejects before the settles
								) x
								WHERE x.settle_response_code = 'S01'
							) AND @transCount >= CASE @frequency WHEN 'Daily' THEN @MissedDailyCollections WHEN 'Weekly' THEN @MissedWeeklyCollections END)
							BEGIN
								SET @setToCollections = 1
							END
					END
				
				
				IF @setToCollections = 1
					BEGIN				
						EXEC dbo.CSP_UpdFundingCollection @funding_id, 'Collections'

						UPDATE fundings SET collection_type = 'Collections', change_user = 'SYSTEM', change_date = GETUTCDATE()
						WHERE id = @funding_id
						-- in case they are not already on hold,
						UPDATE fundings SET on_hold = 1, payments_stopped_date = GETUTCDATE(), change_user = 'SYSTEM', change_date = GETUTCDATE(), on_hold_date = GETUTCDATE()
						WHERE id = @funding_id AND COALESCE(on_hold, 0) = 0
							AND @response_code <> 'R01' AND @response_code <> 'R09' -- if they are still R01 no need to stop payments when sending to collections - RKB 2019-09-17

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(), @rowCount = @@ROWCOUNT; IF @errorNum != 0 GOTO ERROR

						IF @rowCount > 0
							BEGIN
								EXEC dbo.BSK_MarkFundingCaseActive @funding_id
								SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
							END

						-- RKB : 2019-05-07 -- deactivate all active payment schedules for this funding since it is now on hold
						--UPDATE ps
						--SET active = 0
						--FROM payment_schedules ps 
						--INNER JOIN funding_payment_schedules fps ON ps.payment_schedule_id = fps.payment_schedule_id
						--WHERE fps.funding_id = @funding_id AND ps.active = 1
						--	AND @response_code <> 'R01' -- if they are still R01 no need to stop payments when sending to collections - RKB 2019-09-17

						--SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
						--IF @errorNum != 0 GOTO ERROR

						SET @collection_type = 'Collections'					
					END
			END
		IF @collection_type = 'Active_contract' -- did not go into Collections
			BEGIN
				DECLARE @setToCS BIT; SET @setToCS = 0
				-- RKB - 2019-10-08 - FIXING BUG.  NOW THAT EACH SETTING IS 1 WE NEED TO SET TO CS AFTER FIRST MISSED PAYMENT SO NO NEED TO CHECK THIS ANYMORE
				SET @setToCS = 1

				--IF @isFifthThird = 1
				--	BEGIN
				--		-- check the last X number of transactions.  If any were S01 then ignore.  So use NOT EXISTS
				--		IF (NOT EXISTS 
				--			(SELECT 1 FROM 
				--				(
				--					SELECT TOP (CASE @frequency WHEN 'Daily' THEN @MissedDailyCS WHEN 'Weekly' THEN @MissedWeeklyCS END) reason_code
				--					FROM nacha_trans t
				--					WHERE t.funding_id = @funding_id
				--					ORDER BY t.settle_date DESC, t.reason_code -- get the rejects before the settles
				--				) x
				--				WHERE x.reason_code = 'S01'
				--			) AND @transCount >= CASE @frequency WHEN 'Daily' THEN @MissedDailyCS WHEN 'Weekly' THEN @MissedWeeklyCS END)
				--			BEGIN
				--				SET @setToCS = 1
				--			END
				--	END
				--ELSE
				--	BEGIN		
				--		-- check the last X number of transactions.  If any were S01 then ignore.  So use NOT EXISTS
				--		IF (NOT EXISTS 
				--			(SELECT 1 FROM 
				--				(
				--					SELECT TOP (CASE @frequency WHEN 'Daily' THEN @MissedDailyCS WHEN 'Weekly' THEN @MissedWeeklyCS END) settle_response_code
				--					FROM forte_settlements s
				--					INNER JOIN fundings f ON s.customer_id = f.contract_number
				--					WHERE f.id = @funding_id
				--					ORDER BY settle_date DESC, s.settle_response_code -- get the rejects before the settles
				--				) x
				--				WHERE x.settle_response_code = 'S01'
				--			) AND @transCount >= CASE @frequency WHEN 'Daily' THEN @MissedDailyCS WHEN 'Weekly' THEN @MissedWeeklyCS END)
				--			BEGIN
				--				SET @setToCS = 1
				--			END
				--	END

				IF @setToCS = 1
					BEGIN				
						EXEC dbo.CSP_UpdFundingCollection @funding_id, 'Customer_Service'

						UPDATE fundings SET collection_type = 'Customer_Service', change_user = 'SYSTEM', change_date = GETUTCDATE()
						WHERE id = @funding_id 					
					END
			END
	END  -- @settle_type IN ('withdrawal', 'reject')

COMMIT TRANSACTION
GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)	

Done:

END

GO
