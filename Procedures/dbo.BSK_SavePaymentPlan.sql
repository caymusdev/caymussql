SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2020-12-18
-- Description: Can either update a single payment (payment_id is passed in), edit a payment plan (payment_plan_id passed in) or create a new payment plan
-- =============================================
CREATE PROCEDURE dbo.[BSK_SavePaymentPlan]
(
	@bank_account_id			INT,
	@payment_method				VARCHAR (50),
	@amount						MONEY,
	@start_date					DATE,
	@comments					NVARCHAR (4000),
	@affiliate_id				INT,
	@is_recurring				BIT,
	@days_of_week				VARCHAR (50),
	@how_long					VARCHAR (50),   -- either "run" or "end" meaning run till it pays off or end after x number of payments
	@num_periods				INT,
	@payment_plan_id			INT,
	@payment_id					INT,
	@frequency					VARCHAR (50),
	@users_email				NVARCHAR (100),
	@will_pay_off				VARCHAR (10),
	@change_reason_id			INT,
	@payoff_date				VARCHAR (50) = NULL
)
AS
BEGIN
SET NOCOUNT ON

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
DECLARE @user_id INT; SELECT @user_id = user_id FROM users WHERE email = @users_email
DECLARE @bank_name NVARCHAR (100), @routing_number VARCHAR (50), @account_number VARCHAR (50)
DECLARE @orig_bank_name NVARCHAR(100), @orig_routing_number VARCHAR(50), @orig_account_number VARCHAR (50), @orig_payment_method VARCHAR (50)
DECLARE @orig_amount MONEY, @orig_start_date DATE, @description NVARCHAR (2000)
DECLARE @new_scheduled_payment_id INT

-- First need to check and see if this user needs approval for editing/creating payment schedules
DECLARE @approved BIT; SET @approved = 1; SET @description = ''

IF EXISTS (SELECT 1 FROM sec_payment_schedules s WHERE s.submitter_user_id = @users_email)
	BEGIN
		SET @approved = 0
	END

SELECT @bank_name = ba.bank_name, @routing_number = ba.routing_number, @account_number = ba.account_number
FROM merchant_bank_accounts ba 
WHERE ba.merchant_bank_account_id = @bank_account_id

SELECT @orig_bank_name = p.bank_name, @orig_routing_number = p.routing_number, @orig_account_number = p.account_number, @orig_payment_method = p.payment_method,
	@orig_amount = p.payment_amount, @orig_start_date = p.payment_date
FROM scheduled_payments p
INNER JOIN affiliates a ON p.affiliate_id = a.affiliate_id
WHERE p.scheduled_payment_id = @payment_id

IF @payment_id IS NOT NULL
	-- updating a single payment
	BEGIN
		UPDATE scheduled_payments
		SET payment_method = @payment_method, payment_amount = @amount, bank_name = @bank_name, routing_number = @routing_number, account_number = @account_number, changed = 1, 
			approved_date = NULL, approved_user = NULL, exported = NULL, comments = @comments, payment_plan_id = NULL, change_reason_id = @change_reason_id,
			merchant_bank_account_id = @bank_account_id, rejected_user = NULL, rejected_date = NULL
		WHERE scheduled_payment_id = @payment_id

		IF @approved = 0
			BEGIN
				IF @orig_start_date != @start_date SET @description = @description + 'Moved the payment on ' + CONVERT(VARCHAR(10), @orig_start_date, 126)
					+ ' to ' + CONVERT(VARCHAR (10), @start_date, 126) + '.'
				IF @orig_amount != @amount SET @description = @description + ' Changed amount from ' + CONVERT(VARCHAR, CAST(@orig_amount AS MONEY), 1)
							+ ' to ' + CONVERT(VARCHAR, CAST(@amount AS MONEY), 1) + '.'
				IF @orig_bank_name != @bank_name OR @orig_routing_number != @routing_number OR @orig_account_number != @account_number
					SET @description = @description + ' Changed bank account.'

				IF @change_reason_id IS NOT NULL
					BEGIN
						SELECT @description = @description + ' The reason for the change is ' + reason + '.'
						FROM payment_schedule_change_reasons
						WHERE id = @change_reason_id
					END

				IF COALESCE(@comments, '') != ''
					BEGIN
						SET @description = @description + @comments + '.'
					END

				-- since this is updating something that has not been approved yet, may need to delete a previous approval
				DELETE a
				FROM approvals a
				INNER JOIN approval_types t ON a.approval_type_id = t.approval_type_id
				WHERE t.approval_type = 'PaymentChanges' AND affiliate_id = @affiliate_id AND record_id = @payment_id

				INSERT INTO approvals (approval_type_id, description, comments, record_id, new_value, create_date, change_date, change_user, create_user, approved_by,
					approved_date, rejected_by, rejected_date, affiliate_id)
				SELECT t.approval_type_id, LTRIM(@description), NULL, @payment_id, NULL, GETUTCDATE(), GETUTCDATE(), @users_email, @users_email, NULL, NULL, NULL, NULL, 
					@affiliate_id
				FROM approval_types t 
				WHERE t.approval_type = 'PaymentChanges'
			END
		ELSE  -- @approved = 1
			BEGIN
				EXEC dbo.BSK_ApproveSchedPayment @payment_id, @users_email
			END
	END
ELSE IF @is_recurring = 0
	BEGIN
		-- this is a one time payment
		INSERT INTO scheduled_payments (source_scheduled_payment_id, payment_method, payment_date, payment_amount, source_contract_id, contract_id, source_merchant_id, 
			merchant_id, source_affiliate_id, affiliate_id, is_debit, create_date, change_date, change_user, create_user, bank_name, routing_number, account_number, changed, exported,
			payment_plan_id, approved_date, approved_user, change_reason_id, rejected_user, rejected_date, merchant_bank_account_id, comments)
		SELECT NULL, @payment_method, @start_date, @amount, NULL, NULL, NULL, NULL, NULL, @affiliate_id, 1, GETUTCDATE(), GETUTCDATE(), @users_email, @users_email, 
			@bank_name, @routing_number, @account_number, 1, NULL, @payment_plan_id, NULL, NULL, @change_reason_id, NULL, NULL, @bank_account_id, @comments

		SELECT @new_scheduled_payment_id = SCOPE_IDENTITY()

		IF @approved = 0
			BEGIN
				SET @description = 'New ' + @payment_method + ' payment on ' + CONVERT(VARCHAR (10), @start_date, 126) + ' for ' + CONVERT(VARCHAR, CAST(@amount AS MONEY), 1) + '.'

				IF @change_reason_id IS NOT NULL
					BEGIN
						SELECT @description = @description + ' The reason for the change is ' + reason + '.'
						FROM payment_schedule_change_reasons
						WHERE id = @change_reason_id
					END

				IF COALESCE(@comments, '') != ''
					BEGIN
						SET @description = @description + @comments + '.'
					END

				INSERT INTO approvals (approval_type_id, description, comments, record_id, new_value, create_date, change_date, change_user, create_user, approved_by,
					approved_date, rejected_by, rejected_date, affiliate_id)
				SELECT at.approval_type_id, LTRIM(@description), NULL, @new_scheduled_payment_id, NULL, GETUTCDATE(), GETUTCDATE(), @users_email, @users_email, NULL, NULL, NULL, NULL, 
					@affiliate_id
				FROM approval_types at 
				WHERE at.approval_type = 'PaymentChanges'
			END
		ELSE  -- @approved = 1
			BEGIN
				EXEC dbo.BSK_ApproveSchedPayment @new_scheduled_payment_id, @users_email
			END
	END
ELSE
	BEGIN
		BEGIN TRANSACTION

		-- IF @payment plan is being passed in we need to delete all payments on or after the @start_date that have the same payment plan id
		IF @payment_plan_id IS NOT NULL
			BEGIN
				-- update for change user on trigger
				UPDATE scheduled_payments SET change_date = GETUTCDATE(), change_user = @users_email
				WHERE payment_plan_id = @payment_plan_id AND payment_date >= @start_date

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

				DELETE FROM scheduled_payments
				WHERE payment_plan_id = @payment_plan_id AND payment_date >= @start_date

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
			END
		ELSE
			BEGIN
				-- need to create a payment plan id for these changes
				INSERT INTO payment_plans (affiliate_id, current_status, broken_payment_date, create_date, change_date, change_user, create_user, payment_plan_type, frequency, on_monday,
					on_tuesday, on_wednesday, on_thursday, on_friday, start_date, how_long, num_periods, payment_method, payment_amount, bank_account_id, collector, payment_plan_name)
				SELECT @affiliate_id, s.id, NULL, GETUTCDATE(), GETUTCDATE(), @users_email, @users_email, 
					CASE @will_pay_off WHEN 'true' THEN 'Payment Plan' ELSE 'Temporary Arrangement' END, @frequency, 
					CASE WHEN CHARINDEX('M', @days_of_week) > 0 THEN 1 ELSE 0 END, CASE WHEN CHARINDEX('T', @days_of_week) > 0 THEN 1 ELSE 0 END,
					CASE WHEN CHARINDEX('W', @days_of_week) > 0 THEN 1 ELSE 0 END, CASE WHEN CHARINDEX('H', @days_of_week) > 0 THEN 1 ELSE 0 END,
					CASE WHEN CHARINDEX('F', @days_of_week) > 0 THEN 1 ELSE 0 END, @start_date, @how_long, @num_periods, @payment_method, @amount, @bank_account_id, 
					@users_email, CASE @will_pay_off WHEN 'true' THEN 'Payment Plan ' ELSE 'Temporary Arrangement-' END + CONVERT(VARCHAR (10), @today, 126)
				FROM payment_plan_status_codes s 
				WHERE s.code = 'Pending'

				SELECT @payment_plan_id = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
			END

		-- Now need to loop based on start date and number of periods or if this is run to completion
		DECLARE @gross_due MONEY; SELECT @gross_due = gross_due FROM affiliates WHERE affiliate_id = @affiliate_id
		DECLARE @remaining_balance MONEY; SET @remaining_balance = @gross_due;
        DECLARE @temp_date DATE, @tempPeriods INT; SET @tempPeriods = COALESCE(@num_periods, 0)
		DECLARE @days_to_add INT; SET @days_to_add = 1
		IF @frequency = 'Weekly' SET @days_to_add = 7
		IF @frequency = 'BiWeekly' SET @days_to_add = 14		

		IF @frequency = 'Daily' or @frequency = 'Monthly'
			BEGIN
				WHILE (@remaining_balance > 0 AND @how_long =  'run') OR (@how_long = 'end' AND @tempPeriods > 0 AND @remaining_balance > 0)
					BEGIN
						INSERT INTO scheduled_payments (source_scheduled_payment_id, payment_method, payment_date, payment_amount, source_contract_id, contract_id, source_merchant_id, 
							merchant_id, source_affiliate_id, affiliate_id, is_debit, create_date, change_date, change_user, create_user, bank_name, routing_number, account_number, 
								changed, exported, payment_plan_id, approved_date, approved_user, change_reason_id, merchant_bank_account_id, comments)
						SELECT NULL, @payment_method, @start_date, CASE WHEN @remaining_balance < @amount THEN @remaining_balance ELSE @amount END, 
							NULL, NULL, NULL, NULL, NULL, @affiliate_id, 1, GETUTCDATE(), GETUTCDATE(), @users_email, @users_email, 
							@bank_name, @routing_number, @account_number, 1, NULL, @payment_plan_id, NULL, NULL, @change_reason_id, @bank_account_id, @comments

						SELECT @new_scheduled_payment_id = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

						IF @approved = 1
							BEGIN
								EXEC dbo.BSK_ApproveSchedPayment @new_scheduled_payment_id, @users_email
							END

						IF @frequency = 'Monthly'
							BEGIN
								SET @temp_date = DATEADD(MONTH,1, @start_date)
							END
						ELSE
							BEGIN
								SET @temp_date = DATEADD(DD, @days_to_add, @start_date)
							END

						IF DATEPART(DW, @temp_date) = 7 -- saturday
							SET @temp_date = DATEADD(DD, 2, @temp_date)
						ELSE IF DATEPART(DW, @temp_date) = 1 -- Sunday
							SET @temp_date = DATEADD(DD, 1, @temp_date)

						SET @start_date = @temp_date
						SET @remaining_balance = @remaining_balance - @amount
						SET @tempPeriods = @tempPeriods - 1
					END
			END
		ELSE IF @frequency = 'Weekly' or @frequency = 'BiWeekly'
			BEGIN
				DECLARE @extraDays INT; SET @extraDays = CASE WHEN @frequency = 'BiWeekly' THEN 7 ELSE 0 END

				WHILE (@remaining_balance > 0 AND @how_long =  'run') OR (@how_long = 'end' AND @tempPeriods >= 0 AND @remaining_balance > 0)
					BEGIN
						DECLARE @tempDay INT
						SELECT @tempDay = DATEPART(DW, @start_date)

						IF (@tempDay = 2 AND CHARINDEX('M', @days_of_week) > 0) OR (@tempDay = 3 AND CHARINDEX('T', @days_of_week) > 0)
							OR (@tempDay = 4 AND CHARINDEX('W', @days_of_week) > 0) OR (@tempDay = 5 AND CHARINDEX('H', @days_of_week) > 0)
							OR (@tempDay = 6 AND CHARINDEX('F', @days_of_week) > 0)
							BEGIN
								INSERT INTO scheduled_payments (source_scheduled_payment_id, payment_method, payment_date, payment_amount, source_contract_id, contract_id, 
									source_merchant_id, merchant_id, source_affiliate_id, affiliate_id, is_debit, create_date, change_date, change_user, create_user, bank_name, 
									routing_number, account_number, changed, exported, payment_plan_id, approved_date, approved_user, change_reason_id, merchant_bank_account_id,
									comments)
								SELECT NULL, @payment_method, @start_date, CASE WHEN @remaining_balance < @amount THEN @remaining_balance ELSE @amount END, 
									NULL, NULL, NULL, NULL, NULL, @affiliate_id, 1, GETUTCDATE(), GETUTCDATE(), @users_email, @users_email, 
									@bank_name, @routing_number, @account_number, 1, NULL, @payment_plan_id, NULL, NULL, @change_reason_id, @bank_account_id, @comments

								SELECT @new_scheduled_payment_id = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

								IF @approved = 1
									BEGIN
										EXEC dbo.BSK_ApproveSchedPayment @new_scheduled_payment_id, @users_email
									END

								SET @remaining_balance = @remaining_balance - @amount								
							END

						SET @temp_date = DATEADD(DD, 1, @start_date)
						
						IF DATEPART(DW, @temp_date) = 7 -- saturday
							SET @temp_date = DATEADD(DD, 2 + @extraDays, @temp_date)
						ELSE IF DATEPART(DW, @temp_date) = 1 -- Sunday
							SET @temp_date = DATEADD(DD, 1 + @extraDays, @temp_date)

						
						IF DATEPART(WEEK, @start_date) != DATEPART(WEEK, @temp_date)
							BEGIN
								SET @tempPeriods = @tempPeriods - 1
							END
						SET @start_date = @temp_date
					END
			END

			IF @approved = 0
				BEGIN
					SET @description = 'A new ' + CASE @will_pay_off WHEN 'true' THEN 'Payment Plan' ELSE 'Temporary Arrangement' END + ' for ' 
						+ CONVERT(VARCHAR, CAST(@amount AS MONEY), 1) + ' ' + LOWER(@frequency) + 
						' starting on ' + CONVERT(VARCHAR (10), @start_date, 126) + ' will result in payoff on ' + COALESCE(@payoff_date, 'unknown') + '.'

					IF @change_reason_id IS NOT NULL
						BEGIN
							SELECT @description = @description + ' The reason for the change is ' + reason + '.'
							FROM payment_schedule_change_reasons
							WHERE id = @change_reason_id
						END

					IF COALESCE(@comments, '') != ''
						BEGIN
							SET @description = @description + @comments + '.'
						END

					INSERT INTO approvals (approval_type_id, description, comments, record_id, new_value, create_date, change_date, change_user, create_user, approved_by,
						approved_date, rejected_by, rejected_date, affiliate_id)
					SELECT at.approval_type_id, LTRIM(@description), NULL, @payment_plan_id, NULL, GETUTCDATE(), GETUTCDATE(), @users_email, @users_email, NULL, NULL, NULL, NULL, 
						@affiliate_id
					FROM approval_types at 
					WHERE at.approval_type = 'PaymentPlanChanges'
				END
	

		COMMIT TRANSACTION
		GOTO Done

		ERROR:	
			ROLLBACK TRANSACTION
			RAISERROR (@ErrorMessage, 16, 1)
	END

Done:

END

GO
