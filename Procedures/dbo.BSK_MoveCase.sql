SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2021-01-18
-- Description: Gets the department for a specific case 
-- =============================================
CREATE PROCEDURE [dbo].[BSK_MoveCase]
(
	@case_id				INT,
	@department_id			INT,
	@user_email				NVARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON
DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @old_department_id INT;  SELECT @old_department_id = c.department_id FROM cases c WHERE c.case_id = @case_id 
DECLARE @is_management BIT 
DECLARE @retVal NVARCHAR (50); SET @retVal = ''
DECLARE @approval_description NVARCHAR(100), @affiliate_id INT

BEGIN TRANSACTION

SELECT @is_management = dbo.BF_DoesUserHaveManagementRole(@user_email)
IF @is_management = 0 
	BEGIN
		SELECT @approval_description = 'Changing ' + a.affiliate_name + ' from ' + d.department_desc + ' to ' + d2.department_desc, 
			@affiliate_id = c.affiliate_id
		FROM cases c
		INNER JOIN affiliates a ON c.affiliate_id = a.affiliate_id
		INNER JOIN departments d ON c.department_id = d.department_id
		INNER JOIN departments d2 ON @department_id = d2.department_id
		WHERE case_id = @case_id
		
		INSERT INTO approvals(approval_type_id, description, comments, record_id, new_value, create_date, change_date, change_user, create_user, affiliate_id)
		SELECT approval_type_id, @approval_description, NULL, @case_id, @department_id, GETUTCDATE(), GETUTCDATE(), @user_email, @user_email, @affiliate_id 
		FROM approval_types
		WHERE approval_type = 'ChangeCaseDepartment'
	
		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
		SET @retVal = 'approval' 
	END
ELSE
	BEGIN
		-- move case first - change the department_id
		UPDATE cases
		SET department_id = @department_id, change_user = 'System', change_date = GETUTCDATE()
		WHERE case_id = @case_id AND department_id = @old_department_id

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		-- update previous case_departments records' end dates
		DECLARE @yesterday DATE; SELECT @yesterday = DATEADD(DD, -1, @today)
		UPDATE case_departments
		SET end_date = @yesterday, change_user = 'SystemMoveCase', change_date = GETUTCDATE()
		WHERE case_id = @case_id AND department_id = @old_department_id AND end_date IS NULL

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


		-- add records in case_departments for the new department
		INSERT INTO case_departments (case_id, department_id, start_date, end_date, create_date, change_date, change_user, create_user, do_not_move, do_not_move_by, next_move_on, next_move_to)
		SELECT @case_id, @department_id, @today, NULL, GETUTCDATE(), GETUTCDATE(), 'SystemMoveCase', 'SystemMoveCase', NULL, NULL, NULL, NULL

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


		-- NOW ASSIGN THE NEW CASES TO A COLLECTOR
		DECLARE @CaseIDs IDTableType
		INSERT INTO @CaseIDs(record_id)
		SELECT @case_id

		EXEC dbo.BSK_AssignCases @CaseIDs

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		SET @retVal = 'moved' 
	END
COMMIT TRANSACTION
GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)

Done:
SELECT @retVal AS result

END

GO
