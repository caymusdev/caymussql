SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:     Ryan Brown
-- Create Date: 2021-05-26
-- Description: Gets settings for calling LexisNexis
-- =============================================
CREATE PROCEDURE dbo.CSP_GetLexisNexisSettings

AS
BEGIN
SET NOCOUNT ON

SELECT dbo.CF_GetSetting('BPSReportRequestUrl') AS BPSReportRequestUrl, dbo.CF_GetSetting('LexisNexisUserName') AS LexisNexisUserName, 
	dbo.CF_GetSetting('LexisNexisPassword') AS LexisNexisPassword


END
GO
