SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
 -- =============================================
-- Author:      Ryan Brown
-- Create Date: 2020-09-15
-- Description: Either adds or removes a user from a role
-- =============================================
CREATE PROCEDURE [dbo].[BSK_UpdUserRole]
(
	@role_id				INT,
	@has_role				BIT,
	@user_id				INT,
	@change_user			NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON

IF @has_role = 1
	BEGIN
		INSERT INTO user_roles (role_id, user_id, create_date, change_date, change_user, create_user)
		SELECT @role_id, @user_id, GETUTCDATE(), GETUTCDATE(), @change_user, @change_user
		WHERE NOT EXISTS (SELECT 1 FROM user_roles ur WHERE ur.role_id = @role_id AND ur.user_id = @user_id)
	END
ELSE
	BEGIN
		-- update for sake of trigger
		UPDATE user_roles
		SET change_date = GETUTCDATE(), change_user = @change_user
		WHERE role_id = @role_id AND user_id = @user_id

		DELETE FROM user_roles WHERE role_id = @role_id AND user_id = @user_id
	END

END
GO
