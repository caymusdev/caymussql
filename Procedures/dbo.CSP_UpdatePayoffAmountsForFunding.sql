SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-07-16
-- Description:	Updates the payoff amount in the fundings table based on all the transactions.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdatePayoffAmountsForFunding]
(
	@id				BIGINT,
	@change_user	NVARCHAR (100) = 'SYSTEM'
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE @today DATE; SET @today = CONVERT(DATE, DATEADD(HH, -5, GETUTCDATE()))

-- store the changes in a temp table so that we make one single write to the table.  
-- testing for performance reasons
IF OBJECT_ID('tempdb..#tempFunding') IS NOT NULL DROP TABLE #tempFunding
CREATE TABLE #tempFunding (id bigint, paid_off_amount MONEY, payoff_amount MONEY, percent_paid NUMERIC (10, 2), change_date DATETIME, change_user NVARCHAR (100), rtr_out MONEY, 
	pp_out MONEY, margin_out MONEY, refund_out MONEY,
	fees_out MONEY, est_months_left NUMERIC(10, 2),	calc_turn NUMERIC (10, 2), paid_calc_turn NUMERIC(10, 2), last_payment_date DATETIME, actual_turn NUMERIC (10, 2),
	percent_performance NUMERIC (10, 2), float_amount MONEY, portfolio_value MONEY, purchase_price MONEY, funded_date DATETIME, contract_status VARCHAR (50),
	completed_date DATETIME, contract_substatus_id INT, estimated_turn NUMERIC(10,2), frequency VARCHAR (50), weekly_payment MONEY, weekday_payment MONEY)

INSERT INTO #tempFunding (id, change_user, change_date, portfolio_value, purchase_price, funded_date, contract_status, completed_date, contract_substatus_id, 
	estimated_turn, frequency, weekly_payment, weekday_payment) 
SELECT @id, @change_user, GETUTCDATE(), f.portfolio_value, f.purchase_price, f.funded_date, f.contract_status, f.completed_date, f.contract_substatus_id, 
	f.estimated_turn, f.frequency, f.weekly_payment, f.weekday_payment
FROM fundings f WITH (NOLOCK)
WHERE f.id = @id

--  SELECT * FROM #tempFunding


UPDATE f
SET paid_off_amount = COALESCE(x.paid_off, 0), payoff_amount = f.portfolio_value - COALESCE(x.paid_off, 0), percent_paid = COALESCE(x.paid_off, 0) / f.portfolio_value * 100.00--,
	--change_date = GETUTCDATE(), change_user = @change_user
-- FROM fundings f
FROM #tempFunding f
LEFT JOIN (
	SELECT f.id, SUM(CASE WHEN d.trans_type_id IN (1, 6, 7, 15, 8, 14, 10) THEN d.trans_amount  -- 11, 8-- RKB - 2020-03-02 added settlement to reduce payoff amount
						WHEN d.trans_type_id IN (9) THEN -1 * d.trans_amount 
					  ELSE 0 END) AS paid_off
	FROM fundings f WITH (NOLOCK)
	INNER JOIN transactions t  WITH (NOLOCK) ON f.id = t.funding_id
	INNER JOIN transaction_details d  WITH (NOLOCK)  ON t.trans_id = d.trans_id
	LEFT JOIN batches b WITH (NOLOCK)  ON t.batch_id = b.batch_id
	WHERE t.redistributed = 0
		AND f.id = @id
		AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 
	GROUP BY f.id
) x ON f.id = x.id
WHERE f.id = @id


UPDATE f
SET f.rtr_out = f.purchase_price - COALESCE(principal.principal_paid, 0) - COALESCE(writeoff.writeoff, 0) + f.portfolio_value - f.purchase_price - COALESCE(margin.margin_paid, 0) 
		- COALESCE(discount.discount, 0) - COALESCE(margin_adjustment.margin_adjustment, 0), -- rtr_out is the same as pp_out + margin_out
	f.pp_out = f.purchase_price - COALESCE(principal.principal_paid, 0) - COALESCE(writeoff.writeoff, 0), 
	f.margin_out = f.portfolio_value - f.purchase_price - COALESCE(margin.margin_paid, 0) - COALESCE(discount.discount, 0) - COALESCE(margin_adjustment.margin_adjustment, 0), 
	f.fees_out = COALESCE(fees.fees_out, 0), --change_date = GETUTCDATE(), change_user = @change_user, 
	refund_out = COALESCE(refund.refund_out, 0)
--FROM fundings f
FROM #tempFunding f
LEFT JOIN (
	SELECT t.funding_id, SUM(d.trans_amount) AS margin_paid
	FROM transactions t WITH (NOLOCK) 
	INNER JOIN transaction_details d  WITH (NOLOCK) ON t.trans_id = d.trans_id
	LEFT JOIN batches b  WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE d.trans_type_id = 6 AND t.redistributed = 0
		AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 
	GROUP BY t.funding_id
) margin ON f.id = margin.funding_id
LEFT JOIN (
	SELECT t.funding_id, SUM(d.trans_amount) AS principal_paid
	FROM transactions t WITH (NOLOCK) 
	INNER JOIN transaction_details d WITH (NOLOCK)  ON t.trans_id = d.trans_id
	LEFT JOIN batches b WITH (NOLOCK)  ON t.batch_id = b.batch_id
	WHERE d.trans_type_id = 7 AND t.redistributed = 0
		AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 
	GROUP BY t.funding_id
) principal ON f.id = principal.funding_id
LEFT JOIN (
	SELECT t.funding_id, SUM(CASE d.trans_type_id WHEN 9 THEN d.trans_amount ELSE -1 * d.trans_amount END) AS fees_out
	FROM transactions t WITH (NOLOCK) 
	INNER JOIN transaction_details d  WITH (NOLOCK) ON t.trans_id = d.trans_id
	LEFT JOIN batches b WITH (NOLOCK)  ON t.batch_id = b.batch_id
	WHERE d.trans_type_id IN (1,9) AND t.redistributed = 0
		AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 
	GROUP BY t.funding_id
) fees ON f.id = fees.funding_id
LEFT JOIN (
	SELECT t.funding_id, SUM(d.trans_amount) AS writeoff
	FROM transactions t WITH (NOLOCK) 
	INNER JOIN transaction_details d  WITH (NOLOCK) ON t.trans_id = d.trans_id
	LEFT JOIN batches b  WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE d.trans_type_id IN (11) AND t.redistributed = 0
		AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 
	GROUP BY t.funding_id
) writeoff ON f.id = writeoff.funding_id
LEFT JOIN (
	SELECT t.funding_id, SUM(d.trans_amount) AS discount
	FROM transactions t WITH (NOLOCK) 
	INNER JOIN transaction_details d WITH (NOLOCK)  ON t.trans_id = d.trans_id
	LEFT JOIN batches b  WITH (NOLOCK) ON t.batch_id = b.batch_id
	WHERE d.trans_type_id IN (8) AND t.redistributed = 0
		AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 
	GROUP BY t.funding_id
) discount ON f.id = discount.funding_id
LEFT JOIN (
	SELECT t.funding_id, SUM(d.trans_amount) AS margin_adjustment
	FROM transactions t WITH (NOLOCK) 
	INNER JOIN transaction_details d WITH (NOLOCK)  ON t.trans_id = d.trans_id
	LEFT JOIN batches b WITH (NOLOCK)  ON t.batch_id = b.batch_id
	WHERE d.trans_type_id IN (17) AND t.redistributed = 0
		AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 
	GROUP BY t.funding_id
) margin_adjustment ON f.id = margin_adjustment.funding_id
LEFT JOIN (
	SELECT t.funding_id, SUM(t.trans_amount) AS refund_out
	FROM transactions t WITH (NOLOCK) 
	LEFT JOIN batches b  WITH (NOLOCK) ON t.batch_id = b.batch_id		
	WHERE t.trans_type_id IN (22,23, 25) AND t.redistributed = 0
		AND (b.batch_status IN('Settled', 'Closed') OR t.from_access = 1) 
	GROUP BY t.funding_id
) refund ON f.id = refund.funding_id
WHERE f.id = @id

-- update calc turn for active contract
--UPDATE fundings
UPDATE f
SET --est_months_left = ((f.payoff_amount - COALESCE(f.float_amount, 0)) / CASE WHEN f.frequency = 'Weekly' THEN f.weekly_payment / 5 ELSE f.weekday_payment END) / 21.0,
	--calc_turn = (((f.payoff_amount - COALESCE(f.float_amount, 0)) / CASE WHEN f.frequency = 'Weekly' THEN f.weekly_payment / 5 ELSE f.weekday_payment END) / 21.0) 
	--	+ (DATEDIFF(D, funded_date, @today) /30.4),
	est_months_left = DATEDIFF(D, @today, x.final_date)/30.4,
	calc_turn = DATEDIFF(D, funded_date, x.final_date)/30.4--,
	--change_date = GETUTCDATE(), change_user = @change_user
--FROM fundings f
FROM #tempFunding f
INNER JOIN (
	SELECT MAX(payment_date) AS final_date, funding_id
	FROM payment_forecasts WITH (NOLOCK) 
	WHERE funding_id = @id
	GROUP BY funding_id
) x ON f.id = x.funding_id
WHERE f.id = @id AND f.contract_status = 1

-- update calc turn for non active contract
UPDATE f
SET est_months_left = (f.payoff_amount / CASE WHEN f.frequency = 'Weekly' THEN f.weekly_payment / 5 ELSE f.weekday_payment END) / 21.0,
	calc_turn = ((f.payoff_amount / CASE WHEN f.frequency = 'Weekly' THEN f.weekly_payment / 5 ELSE f.weekday_payment END) / 21.0) 
		+ (DATEDIFF(D, funded_date, @today) /30.4)--,		
	--change_date = GETUTCDATE(), change_user = @change_user
--FROM fundings f
FROM #tempFunding f
INNER JOIN (
	SELECT MAX(payment_date) AS final_date, funding_id
	FROM payment_forecasts WITH (NOLOCK) 
	WHERE funding_id = @id
	GROUP BY funding_id
) x ON f.id = x.funding_id
WHERE f.id = @id AND f.contract_status IN (3,4) AND COALESCE(f.contract_substatus_id, -1) <> 2

---- now need to update the paid_calc_turn which is based on how much they have paid off so far, not what their payment plan is.
--UPDATE f
--SET	paid_calc_turn = CASE 
--	WHEN (f.portfolio_value * DATEDIFF(D, funded_date, @today))/(f.paid_off_amount+0.001) / 30.4 > 60 THEN 60
--		ELSE (f.portfolio_value * DATEDIFF(D, funded_date, @today))/(f.paid_off_amount+0.001) / 30.4 END--,
--	--change_date = GETUTCDATE(), change_user = @change_user
---- FROM fundings f
--FROM #tempFunding f
--WHERE f.contract_status IN (1,3,4) AND COALESCE(f.contract_substatus_id, -1) <> 2
--	AND (
--		COALESCE(paid_calc_turn, -999) <> CASE 
--	WHEN (f.portfolio_value * DATEDIFF(D, funded_date, @today))/(f.paid_off_amount+0.001) / 30.4 > 60 THEN 60
--		ELSE (f.portfolio_value * DATEDIFF(D, funded_date, @today))/(f.paid_off_amount+0.001) / 30.4 END 
--	)
--	AND f.id = @id

---- if this funding is closed, then don't use @today, use completed_date
--UPDATE f
--SET	paid_calc_turn = CASE 
--	WHEN (f.portfolio_value * DATEDIFF(D, funded_date, completed_date))/(f.paid_off_amount+0.001) / 30.4 > 60 THEN 60
--		ELSE (f.portfolio_value * DATEDIFF(D, funded_date, completed_date))/(f.paid_off_amount+0.001) / 30.4 END--,
--	--change_date = GETUTCDATE(), change_user = @change_user
----FROM fundings f
--FROM #tempFunding f
--WHERE (f.contract_status IN (2) OR (f.contract_status = 4 AND COALESCE(f.contract_substatus_id, -1) = 2))
--	AND (
--		COALESCE(paid_calc_turn, -999) <> CASE 
--	WHEN (f.portfolio_value * DATEDIFF(D, funded_date, completed_date))/(f.paid_off_amount+0.001) / 30.4 > 60 THEN 60
--		ELSE (f.portfolio_value * DATEDIFF(D, funded_date, completed_date))/(f.paid_off_amount+0.001) / 30.4 END 
--	)
--	AND f.id = @id



UPDATE f
SET last_payment_date = x.last_payment_date--, change_date = GETUTCDATE(), change_user = @change_user
-- FROM fundings f
FROM #tempFunding f
INNER JOIN (
	SELECT t.funding_id, MAX(t.trans_date) AS last_payment_date
	FROM transactions t	 WITH (NOLOCK) 	
	WHERE t.trans_type_id IN (2, 3, 4, 5, 19) AND t.funding_id = @id AND t.trans_amount <> 0
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
WHERE f.id = @id

UPDATE #tempFunding
SET actual_turn = DATEDIFF(D, funded_date, COALESCE(completed_date, last_payment_date)) /30.4--, change_date = GETUTCDATE(), change_user = @change_user
WHERE id = @id
	AND (contract_status = 2 
		OR (contract_status = 4 AND COALESCE(contract_substatus_id, -1) = 2)
		)

UPDATE #tempFunding 
SET percent_performance = (estimated_turn - calc_turn) / estimated_turn * 100.00, change_date = GETUTCDATE(), change_user = @change_user
WHERE id = @id

-- now do the float
UPDATE f 
SET float_amount = COALESCE(x.total_float, 0) + COALESCE(y.total_float, 0)
--FROM fundings f
FROM #tempFunding f
LEFT JOIN (
	SELECT f2.id, SUM(CASE WHEN t.trans_type_id IN (9, 14, 17) THEN -1* t.trans_amount ELSE t.trans_amount END) AS total_float
	FROM fundings f2 WITH (NOLOCK) 
	JOIN transactions t WITH (NOLOCK)  ON f2.id = t.funding_id
	INNER JOIN batches b WITH (NOLOCK)  ON t.batch_id = b.batch_id
	WHERE b.batch_status IN ('Processed') AND t.funding_id = @id
		AND b.batch_type NOT IN ('ForteAPI') -- if it IS ForteAPI, we've already distributed the money so it is not in float.
	GROUP BY f2.id
) x ON f.id = x.id
LEFT JOIN (
-- now get float from forte trans
	SELECT SUM(t.authorization_amount) AS total_float, f.id
	FROM forte_trans t WITH (NOLOCK) 
	INNER JOIN fundings f WITH (NOLOCK)  ON t.customer_id = f.contract_number
	LEFT JOIN forte_trans t2  WITH (NOLOCK) ON t.transaction_id = t2.transaction_id AND t2.status <> 'settling'
	WHERE t.status = 'settling' AND t2.forte_trans_id IS NULL -- this makes sure it does not have a matching record (which would mean there is an update to the settling)
		AND f.id = @id
	GROUP BY f.id
) y ON f.id = y.id
WHERE f.id = @id

-- SELECT * FROM #tempFunding

UPDATE f
SET paid_off_amount = x.paid_off_amount, payoff_amount = x.payoff_amount, percent_paid = x.percent_paid, change_date = x.change_date, change_user = x.change_user, 
	rtr_out = x.rtr_out, pp_out = x.pp_out, margin_out = x.margin_out, refund_out = x.refund_out, fees_out = x.fees_out, est_months_left = x.est_months_left,
	calc_turn = x.calc_turn, -- paid_calc_turn = x.paid_calc_turn, 
	last_payment_date = x.last_payment_date, actual_turn = x.actual_turn, percent_performance = x.percent_performance, 
	float_amount = x.float_amount
FROM fundings f 
INNER JOIN #tempFunding x ON f.id = x.id

IF OBJECT_ID('tempdb..#tempFunding') IS NOT NULL DROP TABLE #tempFunding


END
GO
