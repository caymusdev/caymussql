SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-10-01 - It's October but it's still 93 degrees.  Ugh!
-- Description:	Updates the affiliates_bucket on the affiliate.  Nightly process.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdAffiliateBuckets]
AS
BEGIN
SET NOCOUNT ON;

-- TODO: At some point may need to add logic in here to move up a collection level when a paypent plan brakes.  
-- However, when payment schedules reject and are put on hold the system no longer expects payments so it won't go into a broken state
-- because broken looks for missed payments and when it's deactivated we no longer expect payments.  Need to review this when we look at all the calculations and how they should work.

DECLARE @current_id INT, @max_id INT, @tempDate DATE
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @affiliate_id BIGINT, @collections_date DATE, @payment_plan_date DATE, @current_bucket INT, @new_bucket INT, @start_date DATE
DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
BEGIN TRANSACTION

IF OBJECT_ID('tempdb..#tempAffiliates') IS NOT NULL DROP TABLE #tempAffiliates
CREATE TABLE #tempAffiliates (id int not null identity, affiliate_id BIGINT, collections_date DATETIME, payment_plan_date DATETIME NULL)

IF (SELECT COUNT(*) FROM affiliate_buckets WITH (NOLOCK)) = 0
	BEGIN
		-- need to first do a initial script
		INSERT INTO #tempAffiliates (affiliate_id, collections_date, payment_plan_date)
		SELECT f.affiliate_id, MIN(fc.start_date) AS collections_date, MIN(pp.create_date) AS payment_plan_date
		FROM fundings f WITH (NOLOCK)
		INNER JOIN funding_collections fc WITH (NOLOCK) ON f.id = fc.funding_id AND fc.collection_type = 'Collections'
		LEFT JOIN payment_plans pp WITH (NOLOCK) ON f.affiliate_id = pp.affiliate_id
		GROUP BY f.affiliate_id
		ORDER BY f.affiliate_id

		SELECT @current_id = MIN(id), @max_id = MAX(id) FROM #tempAffiliates

		WHILE @current_id <= @max_id
			BEGIN
				SELECT @affiliate_id = a.affiliate_id, @collections_date = a.collections_date, @payment_plan_date = a.payment_plan_date
				FROM #tempAffiliates a
				WHERE a.id = @current_id

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

				SELECT @current_bucket = COALESCE(collection_level, 0) FROM affiliates WITH (NOLOCK) WHERE affiliate_id = @affiliate_id
				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

				SET @tempDate = @collections_date
				WHILE @tempDate <= @today
					BEGIN
						IF COALESCE(@payment_plan_date, '2050-12-31') <= @tempDate 
							BEGIN
								SELECT @new_bucket = 
									CASE 
										WHEN DATEDIFF(DD, @collections_date, @payment_plan_date) <= 30 THEN 1
										WHEN DATEDIFF(DD, @collections_date, @payment_plan_date) BETWEEN 31 AND 60 THEN 2
										WHEN DATEDIFF(DD, @collections_date, @payment_plan_date) BETWEEN 61 AND 90 THEN 3
										ELSE 4
									END, @start_date = @payment_plan_date	
					
									SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR								 
							END
						ELSE
							BEGIN
								-- if there is no payment plan then the bucket is determined by the amount of time in collections regardless of if any payments are coming in 
								SELECT @new_bucket = 
									CASE 
										WHEN DATEDIFF(DD, @collections_date, @tempDate) <= 30 THEN 1
										WHEN DATEDIFF(DD, @collections_date, @tempDate) BETWEEN 31 AND 60 THEN 2
										WHEN DATEDIFF(DD, @collections_date, @tempDate) BETWEEN 61 AND 90 THEN 3
										ELSE 4
									END, @start_date = @tempDate

									SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
							END

						-- now, if the new bucket and the current bucket are different, we need to make some entries
						IF @new_bucket != @current_bucket AND @new_bucket > @current_bucket -- we don't want the system lowering the bucket automatically.  
							BEGIN
								INSERT INTO affiliate_buckets (affiliate_id, start_date, end_date, collection_level, change_user, change_reason, payment_plan_date)
								VALUES (@affiliate_id, @start_date, NULL, @new_bucket, 'SYSTEM', 'Initial Load', @payment_plan_date)

								SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

								-- now update the record for the old bucket
								UPDATE affiliate_buckets
								SET end_date = DATEADD(DD, -1, @start_date), change_user = 'SYSTEM', change_date = GETUTCDATE()
								WHERE affiliate_id = @affiliate_id AND collection_level = @current_bucket

								SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

								-- now update affiliate record
								UPDATE affiliates 
								SET collection_level = @new_bucket, change_date = GETUTCDATE(), change_user = 'SYSTEM'
								WHERE affiliate_id = @affiliate_id 

								SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

								SET @current_bucket = @new_bucket
							END
						

						SET @tempDate = DATEADD(DD, 1, @tempDate)
						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
					END


				SET @current_id = @current_id + 1
			END
	END


TRUNCATE TABLE #tempAffiliates

-- first, get a list of all affiliates that are still open and in collections as well as the first date they went into collections
INSERT INTO #tempAffiliates (affiliate_id, collections_date, payment_plan_date)
SELECT f.affiliate_id, MIN(fc.start_date) AS collections_date, MIN(pp.create_date) AS payment_plan_date
FROM fundings f WITH (NOLOCK)
INNER JOIN funding_collections fc WITH (NOLOCK) ON f.id = fc.funding_id AND fc.collection_type = 'Collections'
LEFT JOIN payment_plans pp WITH (NOLOCK) ON f.affiliate_id = pp.affiliate_id
WHERE (f.contract_status IN (1,3) OR (f.contract_status = 4 AND f.contract_substatus_id = 1))
GROUP BY f.affiliate_id
ORDER BY f.affiliate_id

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


-- now, loop through each affiliate, figure out where they should be and then compare to what is recorded

SELECT @current_id = MIN(id), @max_id = MAX(id) FROM #tempAffiliates

WHILE @current_id <= @max_id
	BEGIN
		SELECT @affiliate_id = a.affiliate_id, @collections_date = a.collections_date, @payment_plan_date = a.payment_plan_date
		FROM #tempAffiliates a
		WHERE a.id = @current_id

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		SELECT @current_bucket = COALESCE(collection_level, 0) FROM affiliates WITH (NOLOCK) WHERE affiliate_id = @affiliate_id

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		-- if they have a payment plan then the collection bucket is based on the payment plan date and the date it went into collections
		IF @payment_plan_date IS NOT NULL
			BEGIN
				SELECT @new_bucket = 
					CASE 
						WHEN DATEDIFF(DD, @collections_date, @payment_plan_date) <= 30 THEN 1
						WHEN DATEDIFF(DD, @collections_date, @payment_plan_date) BETWEEN 31 AND 60 THEN 2
						WHEN DATEDIFF(DD, @collections_date, @payment_plan_date) BETWEEN 61 AND 90 THEN 3
						ELSE 4
					END, @start_date = @payment_plan_date	
					
					SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR								 
			END
		ELSE
			BEGIN
				-- if there is no payment plan then the bucket is determined by the amount of time in collections regardless of if any payments are coming in 
				SELECT @new_bucket = 
					CASE 
						WHEN DATEDIFF(DD, @collections_date, GETUTCDATE()) <= 30 THEN 1
						WHEN DATEDIFF(DD, @collections_date, GETUTCDATE()) BETWEEN 31 AND 60 THEN 2
						WHEN DATEDIFF(DD, @collections_date, GETUTCDATE()) BETWEEN 61 AND 90 THEN 3
						ELSE 4
					END, @start_date = GETUTCDATE()

					SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
			END

		-- now, if the new bucket and the current bucket are different, we need to make some entries
		IF @new_bucket != @current_bucket AND @new_bucket > @current_bucket -- we don't want the system lowering the bucket automatically.  
			BEGIN
				INSERT INTO affiliate_buckets (affiliate_id, start_date, end_date, collection_level, change_user, change_reason, payment_plan_date)
				VALUES (@affiliate_id, @start_date, NULL, @new_bucket, 'SYSTEM', 'Auto Run', @payment_plan_date)

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

				-- now update the record for the old bucket
				UPDATE affiliate_buckets
				SET end_date = DATEADD(DD, -1, @start_date), change_user = 'SYSTEM', change_date = GETUTCDATE()
				WHERE affiliate_id = @affiliate_id AND collection_level = @current_bucket

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

				-- now update affiliate record
				UPDATE affiliates 
				SET collection_level = @new_bucket, change_date = GETUTCDATE(), change_user = 'SYSTEM'
				WHERE affiliate_id = @affiliate_id 

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
			END


		SET @current_id = @current_id + 1
	END



IF OBJECT_ID('tempdb..#tempAffiliates') IS NOT NULL DROP TABLE #tempAffiliates


COMMIT TRANSACTION
GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)

Done:

END
GO
