SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-03-13
-- Description:	Redistributes a transaction to an open contract, if it exists.  Safe to call anytime since checks are done.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_RedistributeTransactionToOldestContract]
(
	@funding_id				INT, 
	@trans_id				INT
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @funded_date DATE, @merchant_id BIGINT, @never_redistribute BIT, @payment_amount MONEY, @contract_type VARCHAR (50)
SELECT @funded_date = funded_date, @merchant_id = merchant_id, @never_redistribute = COALESCE(never_redistribute, 0), @contract_type = contract_type 
FROM fundings WHERE id = @funding_id

DECLARE @older_funding_id BIGINT, @older_payoff_amount MONEY

DECLARE @today DATE; SET @today = DATEADD(HOUR, -5, GETUTCDATE())

IF @never_redistribute = 0 AND @contract_type != 'Renewal_Concurrent'
	BEGIN
		SELECT TOP 1 @older_funding_id = f.id, @older_payoff_amount = f.payoff_amount
		FROM fundings f
		WHERE f.merchant_id = @merchant_id
			AND f.funded_date < @funded_date
			AND f.id <> @funding_id AND f.contract_status = 1
			AND f.never_redistribute = 0
			AND f.payoff_amount > 0
			AND NOT EXISTS (SELECT 1 FROM payments p WHERE p.funding_id = f.id 
						AND ((p.processed_date IS NULL OR p.trans_date >= @today) AND p.deleted = 0 AND p.waived = 0)  -- don't want to redistribute if there is current payments for the other funding
					)
		ORDER BY f.funded_date

		IF @older_funding_id IS NOT NULL
			BEGIN
				-- now need to redistribute.  If the payoff amount is >= than payment amount, it can all be redistributed.  If it is less, 
				-- then we redistribute enough to payoff and the rest goes back to the original.
				SELECT @payment_amount = t.trans_amount FROM transactions t WHERE t.trans_id = @trans_id

				IF @older_payoff_amount >= @payment_amount
					BEGIN
						 EXEC dbo.[CSP_RedistributeTransaction] @trans_id, @older_funding_id, @payment_amount, 'Redistributed to older contract'
					END
				ELSE
					BEGIN
						DECLARE @funding_ids VARCHAR(50), @amounts VARCHAR (100), @comments VARCHAR(200)
						SELECT @funding_ids = CONVERT(VARCHAR(50), @older_funding_id) + '|' + CONVERT(VARCHAR(50), @funding_id), 
							@amounts = CONVERT(VARCHAR (50), @older_payoff_amount) + '|' + CONVERT(VARCHAR (50), (@payment_amount - @older_payoff_amount)),
							@comments = 'Redistributed to older contract|Redistributed to older contract'

						EXEC dbo.[CSP_RedistributeTransaction] @trans_id, @funding_ids, @amounts, @comments
					END
			END
	END

END
GO
