SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-08-12
-- Description:	Checks for fundings that are paying off and that also have another related contract not paid off yet
-- =============================================
CREATE PROCEDURE [dbo].[CSP_InsRedirectApprovals]

AS
BEGIN	
SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData
CREATE TABLE #tempData (id int not null identity, funding_id BIGINT, est_months_left NUMERIC(10, 2), start_date DATE, end_date DATE, frequency VARCHAR (50), non_paying_funding_id BIGINT,
	slow_pay_funding_id BIGINT, collection_funding_id BIGINT, payment_schedule_id INT)

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())

-- first update the anticipated start date of any records that have not yet started
UPDATE ra
SET anticipated_start_date = DATEADD(DD, f.est_months_left * 30.4, GETUTCDATE())
FROM redirect_approvals ra WITH (NOLOCK)
INNER JOIN fundings f WITH (NOLOCK) ON ra.from_funding_id = f.id
WHERE ra.actual_start_date IS NULL


-- store off 20ish last records from fact fundings to check if the est_months_left is decreasing.
INSERT INTO #tempData (funding_id, est_months_left, start_date, end_date, frequency, non_paying_funding_id, slow_pay_funding_id, collection_funding_id, payment_schedule_id)
SELECT f.id, ff.est_months_left, ff.start_date, ff.end_date, f.frequency, 
	-- get non paying fundings that do not have a payment plan in place
	(SELECT TOP 1 fNon.id 
	 FROM fundings fNon WITH (NOLOCK)
	 --LEFT JOIN funding_payment_schedules fps WITH (NOLOCK) ON fNon.id = fps.funding_id
	 --LEFT JOIN payment_schedules ps WITH (NOLOCK) ON fps.payment_schedule_id = ps.payment_schedule_id
	 LEFT JOIN funding_payment_dates pd WITH (NOLOCK) ON fnon.id = pd.funding_id AND pd.active = 1
	 LEFT JOIN payment_plans pp WITH (NOLOCK) ON pd.payment_plan_id = pp.payment_plan_id AND pp.current_status IN (1, 2, 3)
	 WHERE fNon.affiliate_id = f.affiliate_id AND fNon.id <> f.id AND fNon.performance_status_id IN (3, 4) AND pp.payment_plan_id IS NULL -- no active payment plan
		AND fNon.payoff_amount > 0 --AND ps.active = 1
	 ORDER BY fNon.funded_date) AS non_paying_funding_id,
	(SELECT TOP 1 fNon.id 
	 FROM fundings fNon WITH (NOLOCK) 
	-- LEFT JOIN funding_payment_schedules fps WITH (NOLOCK) ON fNon.id = fps.funding_id
	 --LEFT JOIN payment_schedules ps WITH (NOLOCK) ON fps.payment_schedule_id = ps.payment_schedule_id
	 LEFT JOIN funding_payment_dates pd WITH (NOLOCK) ON fnon.id = pd.funding_id AND pd.active = 1
	 LEFT JOIN payment_plans pp WITH (NOLOCK) ON pd.payment_plan_id = pp.payment_plan_id AND pp.current_status IN (1, 2, 3)
	 WHERE fNon.affiliate_id = f.affiliate_id AND fNon.id <> f.id 
		--AND (fNon.performance_status_id IN (2) OR NULLIF(fNon.collection_type, '') IS NOT NULL) 
		AND fNon.performance_status_id IN (2) 
		AND pp.payment_plan_id IS NULL -- no active payment plan
		AND fNon.payoff_amount > 0 -- AND ps.active = 1
	 ORDER BY fNon.funded_date) AS slow_pay_funding_id,
	 (SELECT TOP 1 fNon.id 
	 FROM fundings fNon WITH (NOLOCK) 
	-- LEFT JOIN funding_payment_schedules fps WITH (NOLOCK) ON fNon.id = fps.funding_id
	 --LEFT JOIN payment_schedules ps WITH (NOLOCK) ON fps.payment_schedule_id = ps.payment_schedule_id
	 LEFT JOIN funding_payment_dates pd WITH (NOLOCK) ON fnon.id = pd.funding_id AND pd.active = 1
	 LEFT JOIN payment_plans pp WITH (NOLOCK) ON pd.payment_plan_id = pp.payment_plan_id AND pp.current_status IN (1, 2, 3)
	 WHERE fNon.affiliate_id = f.affiliate_id AND fNon.id <> f.id 
		AND NULLIF(fNon.collection_type, '') IS NOT NULL
		AND pp.payment_plan_id IS NULL -- no active payment plan
		AND fNon.payoff_amount > 0 --AND ps.active = 1
	 ORDER BY fNon.funded_date) AS collection_funding_id,
	--(SELECT TOP 1 ps.payment_schedule_id
	---- FROM funding_payment_schedules fps WITH (NOLOCK)
	-- --INNER JOIN payment_schedules ps WITH (NOLOCK) ON fps.payment_schedule_id = ps.payment_schedule_id	 
	-- WHERE fps.funding_id = f.id AND ps.active = 1
	-- ORDER BY ps.start_date DESC) AS payment_schedule_id
	NULL AS payment_schedule_id
FROM fundings f WITH (NOLOCK)
INNER JOIN fact_fundings ff WITH (NOLOCK) ON f.id = ff.funding_id AND ff.start_date BETWEEN DATEADD(DD, -20, @today) AND @today
WHERE f.est_months_left <= 0.33 
	AND (f.contract_status = 1 OR (f.contract_status = 4 AND f.contract_substatus_id = 1))
	-- make sure they have an active payment schedule
	AND EXISTS (SELECT 1 FROM funding_payment_dates pd WHERE pd.funding_id = f.id AND pd.active = 1 AND pd.payment_date > @today)
	--AND EXISTS (SELECT 1 FROM payment_schedules ps WITH (NOLOCK) 
	--			INNER JOIN funding_payment_schedules fps WITH (NOLOCK) ON ps.payment_schedule_id = fps.payment_schedule_id
	--			INNER JOIN fundings fInner WITH (NOLOCK) ON fps.funding_id = fInner.id
	--			WHERE ps.active = 1 AND fInner.id = f.id)
ORDER BY f.id, ff.start_date


-- The derived table will take the first and the last record for each funding and then join outside back to the main temp data to find those
-- that are decreasing.  
INSERT INTO redirect_approvals (from_funding_id, to_funding_id, approved_by, approved_date, anticipated_start_date, actual_start_date, payment_schedule_id, 
	end_date, redirect_reason, change_date, change_user, on_hold)
SELECT x.funding_id, COALESCE(t2.non_paying_funding_id, t2.slow_pay_funding_id, t2.collection_funding_id), 
	CASE WHEN t2.non_paying_funding_id IS NOT NULL THEN 'SYSTEM' ELSE NULL END AS approved_by,
	CASE WHEN t2.non_paying_funding_id IS NOT NULL THEN GETUTCDATE() ELSE NULL END AS approved_date,
	DATEADD(DD, f.est_months_left * 30.4, GETUTCDATE()) AS anticipated_start_date, 
	NULL AS actual_start_date, 
	t2.payment_schedule_id, NULL AS end_date,
	CASE WHEN t2.non_paying_funding_id IS NOT NULL THEN 'Non-paying Funding'
		WHEN t2.slow_pay_funding_id IS NOT NULL THEN 'Slow-paying Funding'
		WHEN t2.collection_funding_id IS NOT NULL THEN 'Funding in Collection'
	END AS redirect_reason, GETUTCDATE(), 'SYSTEM', 0 AS on_hold
FROM (
	SELECT t.id, t.funding_id, t.est_months_left, t.start_date, t.end_date, t.frequency, LEAD(t.id) OVER (ORDER BY t.funding_id, t.start_date) NextID,
		CASE WHEN t.start_date = x.min_start_date THEN 0 ELSE 1 END AS OrderBy
	FROM #tempData t
	INNER JOIN (
		SELECT t.funding_id, MIN(t.start_date) AS min_start_date, MAX(t.start_date) AS max_start_date
		FROM #tempData t 
		GROUP BY t.funding_id
	) x ON t.funding_id = x.funding_id
	WHERE t.start_date = x.min_start_date OR t.start_date = x.max_start_date
) x
INNER JOIN #tempData t2 ON x.NextID = t2.id
INNER JOIN fundings f WITH (NOLOCK) ON x.funding_id = f.id
WHERE x.OrderBy = 0 AND x.est_months_left > t2.est_months_left
	AND (t2.non_paying_funding_id IS NOT NULL OR t2.slow_pay_funding_id IS NOT NULL OR t2.collection_funding_id IS NOT NULL)
	AND NOT EXISTS (SELECT 1 FROM redirect_approvals ra WHERE ra.from_funding_id = x.funding_id)


END
GO
