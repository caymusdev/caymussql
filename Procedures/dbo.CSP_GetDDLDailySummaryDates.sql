SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-06-04
-- Description:	Gets the last X number of batch dates to show in drop down
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetDDLDailySummaryDates]
AS
BEGIN
SET NOCOUNT ON;

SELECT DISTINCT TOP 30  CONVERT(DATE, b.settle_date) AS Display,  CONVERT(DATE, b.settle_date) AS Value, CONVERT(DATE, b.settle_date) AS batch_date
FROM batches b 
WHERE b.batch_status IN ('Settled', 'Closed') AND COALESCE(b.deleted_flag, 0) = 0
ORDER BY CONVERT(date, b.settle_date) DESC

END
GO
