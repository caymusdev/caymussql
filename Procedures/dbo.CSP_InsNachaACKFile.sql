SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-07-19
-- Description:	Inserts the NACHA ack file
-- =============================================
CREATE PROCEDURE [dbo].[CSP_InsNachaACKFile]
(
	@customer_id				[VARCHAR] (50),
	@ref_number					[VARCHAR] (50),
	@debit_amount				[MONEY],
	@credit_amount				[MONEY],
	@process_date				[DATETIME],
	@date_provided				[DATETIME],
	@file_name					[VARCHAR] (100)
)
AS
BEGIN	
SET NOCOUNT ON;

INSERT INTO nacha_ack(customer_id, ref_number, debit_amount, credit_amount, process_date, date_provided, file_name)
VALUES (@customer_id, @ref_number, @debit_amount, @credit_amount, @process_date, @date_provided, @file_name)

END
GO
