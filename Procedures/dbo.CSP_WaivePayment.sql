SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-05-22
-- Description:	Waives a payment.  Then creates a negative Fee Assessed transaction
-- =============================================
CREATE PROCEDURE [dbo].[CSP_WaivePayment]
(
	@payment_id			INT,
	@userid				NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @funding_id BIGINT, @trans_amount MONEY
DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''

BEGIN TRANSACTION

UPDATE payments
SET change_user = @userid, change_date = GETUTCDATE(), waived = 1
WHERE payment_id = @payment_id

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

-- get the amount to waive and also the funding id
SELECT @trans_amount = p.amount, @funding_id = p.funding_id FROM payments p WHERE p.payment_id = @payment_id

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

-- now need to enter a negative fee assessed to wipe out the fees.
-- get a new batch
DECLARE @today DATE, @newBatchID INT
SET @today = DATEADD(HOUR, -5, GETUTCDATE())
EXEC @newBatchID = dbo.CSP_GetNewBatchNumber '', @userid, 'Fee Waived', '', @today, 1

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


SET @trans_amount = -1.00 * @trans_amount

EXEC dbo.CSP_DistributeTransaction_v2 @funding_id, @trans_amount, @payment_id, @today, 'Fee Waived', @newBatchID, @payment_id, 'payments', NULL, @today, NULL, NULL, 9
 
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

-- set the batch to Closed
UPDATE batches SET batch_status = 'Processed' WHERE batch_id = @newBatchID
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


COMMIT TRANSACTION
GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)

Done:




END
GO
