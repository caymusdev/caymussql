SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-10-26
-- Description:	Creates the initial expected payment dates for fundings based on original rtr and also any payment schedules
-- Can be run anytime as it checks for existing records.
-- RKB: 2020-01-24 - THIS SP WILL NO LONGER DO ANYTHING.  THERE IS A SCHEDULE AT NIGHT THAT RUNS THIS BUT WE WANT THESE RECORDS
-- CREATE AS SOON AS A FUNDING IS FUNDED SO WE DON'T WANT THIS TO DO ANYTHING ANYMORE.
-- BUT FOR NOW, NOT DELETING UNTIL WE GET THE CHANGES ALL STRAIGHTENED OUT
-- =============================================
CREATE PROCEDURE [dbo].[CSP_InsFundingPaymentDates]

AS
BEGIN	
SET NOCOUNT ON;

RETURN  -- DON'T WANT TO CHANGE ANYTHING JUST YET.


DECLARE @tempDate DATE;
DECLARE @funding_id BIGINT, @frequency VARCHAR (50), @payment MONEY, @first_payment_date DATE, @payment_schedule_id INT
DECLARE @minID BIGINT, @maxID BIGINT, @tempID BIGINT
DECLARE @remaining_amount MONEY, @tempAmount MONEY, @orig_payment MONEY
DECLARE @rtr MONEY,  @schedule_start_date DATE

IF OBJECT_ID('tempdb..#tempFundings') IS NOT NULL DROP TABLE #tempFundings
CREATE TABLE #tempFundings (id int not null identity, funding_id BIGINT, schedule_start_date DATE NULL)

IF OBJECT_ID('tempdb..#tempSchedules') IS NOT NULL DROP TABLE #tempSchedules
CREATE TABLE #tempSchedules (id int not null identity, payment_schedule_id INT)

-- first do fundings that have no payment schedule
INSERT INTO #tempFundings (funding_id)
SELECT f.id 
FROM fundings f
WHERE NOT EXISTS (SELECT 1 FROM funding_payment_schedules fps WHERE f.id = fps.funding_id)
	AND f.frequency IS NOT NULL 
	AND NOT EXISTS (SELECT 1 FROM funding_payment_dates fpd WHERE f.id = fpd.funding_id)
	OR (f.id IN (284, 285, 286, 287, 288, 289)
		AND NOT EXISTS (SELECT 1 FROM funding_payment_dates fpd WHERE f.id = fpd.funding_id)
	) 
ORDER BY f.id


SELECT @minID = MIN(id), @maxID = MAX(id) FROM #tempFundings
SET @tempID = @minID
-- loop through fundings with no payment schedule
WHILE @tempID <= @maxID
	BEGIN
		SELECT @funding_id = funding_id FROM #tempFundings WHERE id = @tempID

		SELECT @rtr = portfolio_value, @frequency = frequency, 
			@payment = CASE frequency WHEN 'Daily' THEN weekday_payment ELSE weekly_payment END, @first_payment_date = first_payment_date
		FROM fundings WHERE id = @funding_id

		SET @tempDate = @first_payment_date
				
		IF @payment IS NOT NULL AND @frequency IS NOT NULL
			BEGIN
				SET @remaining_amount = @rtr 
				SET @orig_payment = @payment -- @payment can be changed

				WHILE @remaining_amount > 0 --AND @tempDate <= @today
					BEGIN
						SET @tempAmount = @remaining_amount - @payment
						IF @tempAmount < 0
							BEGIN
								SET @payment = @remaining_amount
							END
						IF @frequency = 'Daily'
							BEGIN
								IF DATEPART(dw, @tempDate) IN (2,3,4,5,6) -- only do Monday Through Friday even though CC might have weekend amounts
									BEGIN
										INSERT INTO funding_payment_dates(funding_id, payment_date, amount, payment_schedule_id, rec_amount)
										SELECT @funding_id, @tempDate, @payment, @payment_schedule_id, 0

										SET @remaining_amount = @remaining_amount - @payment
									END
							END
						ELSE IF @frequency = 'Weekly'
							BEGIN
								IF DATEPART(dw, @tempDate) = DATEPART(dw, @first_payment_date)
									BEGIN
										-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
										INSERT INTO funding_payment_dates(funding_id, payment_date, amount, payment_schedule_id, rec_amount)
										SELECT @funding_id, @tempDate, @payment, @payment_schedule_id, 0

										SET @remaining_amount = @remaining_amount - @payment
									END
							END
						ELSE IF @frequency = 'Monthly'
							BEGIN
								IF DAY(@tempDate) = DAY(@first_payment_date)
									BEGIN
										-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
										INSERT INTO funding_payment_dates(funding_id, payment_date, amount, payment_schedule_id, rec_amount)
										SELECT @funding_id, @tempDate, @payment, @payment_schedule_id, 0

										SET @remaining_amount = @remaining_amount - @payment
									END
							END
						-- increment @tempDate
						SET @tempDate = DATEADD(D, 1, @tempDate)		
						-- reset @payment variable
						SET @payment = @orig_payment						
					END	
			END
		SET @tempID = @tempID + 1
	END
-- end looping through fundings with no payment schedule


TRUNCATE TABLE #tempFundings


INSERT INTO #tempFundings (funding_id)
SELECT DISTINCT f.id 
FROM fundings f
INNER JOIN funding_payment_schedules fps ON f.id = fps.funding_id
INNER JOIN payment_schedules ps ON fps.payment_schedule_id = ps.payment_schedule_id
WHERE --NOT EXISTS (SELECT 1 FROM funding_payment_dates fpd WHERE f.id = fpd.funding_id AND fps.payment_schedule_id = COALESCE(fpd.payment_schedule_id, -1))
	 f.id NOT IN (284, 285, 286, 287, 288, 289)
	 AND COALESCE(ps.active, 1) = 1
ORDER BY f.id

SELECT @minID = MIN(id), @maxID = MAX(id) FROM #tempFundings
SET @tempID = @minID

WHILE @tempID <= @maxID
	BEGIN
		SELECT @funding_id = funding_id FROM #tempFundings WHERE id = @tempID	
		
		SELECT @rtr = portfolio_value, @frequency = frequency, 
			@payment = CASE frequency WHEN 'Daily' THEN weekday_payment ELSE weekly_payment END, @first_payment_date = first_payment_date
		FROM fundings WHERE id = @funding_id
		
		-- First, fill temp table with all payment schedules for this funding
		INSERT INTO #tempSchedules (payment_schedule_id)
		SELECT ps.payment_schedule_id
		FROM funding_payment_schedules fps
		INNER JOIN payment_schedules ps ON fps.payment_schedule_id = ps.payment_schedule_id
		WHERE fps.funding_id = @funding_id AND COALESCE(ps.active, 1) = 1
		ORDER BY COALESCE(ps.end_date, '2050-12-31')  -- get any that have no end date at the end
 					
		DECLARE @tempScheduleID INT, @minSchedID INT, @maxSchedID INT
		SELECT @tempScheduleID = MIN(id), @minSchedID = MIN(id), @maxSchedID = MAX(id)
		FROM #tempSchedules
		DECLARE @runningTotal MONEY; SET @runningTotal = 0

		WHILE @tempScheduleID <= @maxSchedID
			BEGIN			
				DECLARE @end_date DATE;
				SELECT @payment_schedule_id = payment_schedule_id FROM #tempSchedules WHERE id = @tempScheduleID
				SELECT @frequency = ps.frequency, @payment = ps.amount, @end_date = ps.end_date, @first_payment_date = ps.start_date
				FROM payment_schedules ps 
				WHERE ps.payment_schedule_id = @payment_schedule_id

				SET @tempDate = @first_payment_date			

				IF NOT EXISTS(SELECT 1 FROM funding_payment_dates fpd WHERE fpd.funding_id = @funding_id AND COALESCE(fpd.payment_schedule_id, -1) = @payment_schedule_id)
					BEGIN
						-- if there is an end date, don't worry about remaining amount, just dump it all in
						IF @end_date IS NOT NULL
							BEGIN
								WHILE @tempDate <= @end_date
									BEGIN											
										IF @frequency = 'Daily'
											BEGIN
												IF DATEPART(dw, @tempDate) IN (2,3,4,5,6) -- only do Monday Through Friday even though CC might have weekend amounts
													BEGIN
														INSERT INTO funding_payment_dates(funding_id, payment_date, amount, payment_schedule_id, rec_amount)
														SELECT @funding_id, @tempDate, @payment, @payment_schedule_id, 0

														SET @runningTotal = @runningTotal + @payment
													END
											END
										ELSE IF @frequency = 'Weekly'
											BEGIN
												IF DATEPART(dw, @tempDate) = DATEPART(dw, @first_payment_date)
													BEGIN
														-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
														INSERT INTO funding_payment_dates(funding_id, payment_date, amount, payment_schedule_id, rec_amount)
														SELECT @funding_id, @tempDate, @payment, @payment_schedule_id, 0

														SET @runningTotal = @runningTotal + @payment
													END
											END
										ELSE IF @frequency = 'Monthly'
											BEGIN
												IF DAY(@tempDate) = DAY(@first_payment_date)
													BEGIN
														-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
														INSERT INTO funding_payment_dates(funding_id, payment_date, amount, payment_schedule_id, rec_amount)
														SELECT @funding_id, @tempDate, @payment, @payment_schedule_id, 0

														SET @runningTotal = @runningTotal + @payment
													END
											END
										-- increment @tempDate
										SET @tempDate = DATEADD(D, 1, @tempDate)
									END		
							END
						ELSE
							BEGIN
								-- end date is null
								SET @remaining_amount = @rtr - @runningTotal --- COALESCE(@todaysPayments, 0)
								SET @orig_payment = @payment -- @payment can be changed

								WHILE @remaining_amount > 0
									BEGIN							
										SET @tempAmount = @remaining_amount - @payment
										IF @tempAmount < 0
											BEGIN
												SET @payment = @remaining_amount
											END
										IF @frequency = 'Daily'
											BEGIN
												IF DATEPART(dw, @tempDate) IN (2,3,4,5,6) -- only do Monday Through Friday even though CC might have weekend amounts
													BEGIN
														INSERT INTO funding_payment_dates(funding_id, payment_date, amount, payment_schedule_id, rec_amount)
														SELECT @funding_id, @tempDate, @payment, @payment_schedule_id, 0

														SET @remaining_amount = @remaining_amount - @payment
													END
											END
										ELSE IF @frequency = 'Weekly'
											BEGIN
												IF DATEPART(dw, @tempDate) = DATEPART(dw, @first_payment_date)
													BEGIN
														-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
														INSERT INTO funding_payment_dates(funding_id, payment_date, amount, payment_schedule_id, rec_amount)
														SELECT @funding_id, @tempDate, @payment, @payment_schedule_id, 0

														SET @remaining_amount = @remaining_amount - @payment
													END
											END
										ELSE IF @frequency = 'Monthly'
											BEGIN
												IF DAY(@tempDate) = DAY(@first_payment_date)
													BEGIN
														-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
														INSERT INTO funding_payment_dates(funding_id, payment_date, amount, payment_schedule_id, rec_amount)
														SELECT @funding_id, @tempDate, @payment, @payment_schedule_id, 0

														SET @remaining_amount = @remaining_amount - @payment
													END
											END
										-- increment @tempDate
										SET @tempDate = DATEADD(D, 1, @tempDate)		
										-- reset @payment variable
										SET @payment = @orig_payment						
									END	
							END
					END
					SET @tempScheduleID  = @tempScheduleID + 1
			END

		-- finally truncate temp table
		TRUNCATE TABLE #tempSchedules			

		-- loop to next funding
		SET @tempID = @tempID + 1
	END

-- now update the received amount.  Looks at the last 14 days.
/*
UPDATE p
SET rec_amount = COALESCE(x.rec_amount, 0)
FROM funding_payment_dates p
INNER JOIN (
	SELECT SUM(trans_amount) AS rec_amount, t.funding_id, t.trans_date
	FROM transactions t 
	WHERE trans_type_id IN (2, 3, 4, 5, 19) --AND t.trans_date BETWEEN GETDATE() - 14 AND GETDATE()
	GROUP BY t.funding_id, t.trans_date
) x ON p.funding_id = x.funding_id AND p.payment_date = x.trans_date
WHERE p.rec_amount = 0
*/

UPDATE pd
SET rec_amount = COALESCE(p.amount, 0)
FROM funding_payment_dates pd
INNER JOIN payments p ON pd.funding_id = p.funding_id AND pd.payment_date = p.trans_date AND p.transaction_id IS NOT NULL
WHERE pd.payment_date BETWEEN GETDATE() - 14 AND GETDATE()

-- delete any funding_payment_dates WHERE the schedule is no longer active and these dates are in the future
DECLARE @today DATE; SET @today = DATEADD(HH, -5, GETUTCDATE())

-- delete future dates on non-active schedules
DELETE fpd
--SELECT *
FROM funding_payment_dates fpd
INNER JOIN payment_schedules ps ON fpd.payment_schedule_id = ps.payment_schedule_id
WHERE ps.active = 0 
	AND (fpd.payment_date > @today OR fpd.payment_date > COALESCE(ps.end_date, @today))
--ORDER BY funding_id, payment_date, fpd.payment_schedule_id


-- delete any null funding_payment_dates for a given date where there is also a payment schedule
-- SELECT *
DELETE d
FROM funding_payment_dates d
WHERE d.payment_schedule_id IS NULL
	AND EXISTS (SELECT 1 FROM funding_payment_dates d2 WHERE d.funding_id = d2.funding_id AND d.payment_date = d2.payment_date AND d2.payment_schedule_id IS NOT NULL)


IF OBJECT_ID('tempdb..#tempFundings') IS NOT NULL DROP TABLE #tempFundings
IF OBJECT_ID('tempdb..#tempSchedules') IS NOT NULL DROP TABLE #tempSchedules

END
GO
