SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-05-02
-- Description:	Gets a setting from the settings table
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetSetting] 
(
	@setting			NVARCHAR (50)
)

AS
BEGIN	
SET NOCOUNT ON;

SELECT s.setting, s.setting_desc, s.setting_group, s.setting_id, s.value
FROM settings s
WHERE setting = @setting
END
GO
