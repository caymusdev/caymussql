SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-06-04
-- Description:	Gets settings for sending a text message
-- =============================================
CREATE PROCEDURE CSP_GetSMSSettings	
AS
BEGIN
SET NOCOUNT ON;

SELECT dbo.CF_GetSetting('SMSUserName') AS sms_user_name, dbo.CF_GetSetting('NumberToSMS') AS number_to_text, dbo.CF_GetSetting('SMSAPIKey') AS api_key

END
GO
