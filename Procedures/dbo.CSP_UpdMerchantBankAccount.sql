SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-02-12
-- Description:	Updates a merchant account if not in use
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdMerchantBankAccount]
(
	@merchant_bank_account_id	INT,
	@nickname					NVARCHAR (200),
	@bank_name					NVARCHAR (100), 
	@routing_number				VARCHAR (50), 
	@account_number				VARCHAR (50), 
	@userid						NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @original_routing_number VARCHAR (50), @original_account_number VARCHAR (50)
SELECT @original_routing_number = routing_number, @original_account_number = account_number
FROM merchant_bank_accounts 
WHERE merchant_bank_account_id = @merchant_bank_account_id

-- when updating, we only care if there are no payment dates in the past.  They can't change account number or routing number
IF EXISTS(SELECT 1 
			FROM funding_payment_dates d
			WHERE d.merchant_bank_account_id = @merchant_bank_account_id
				AND payment_date < CONVERT(DATE, GETUTCDATE())
			)
	AND (COALESCE(@original_routing_number, '') != COALESCE(@routing_number, '') OR COALESCE(@original_account_number, '') != COALESCE(@account_number, ''))
	BEGIN
		RAISERROR ('The bank account has been used in the past and cannot be changed.', 16, 1)
	END
ELSE
	BEGIN
		UPDATE merchant_bank_accounts 
		SET change_user  = @userid, change_date  = GETUTCDATE(), nickname = @nickname, bank_name = @bank_name, routing_number = @routing_number, 
			account_number = @account_number
		WHERE merchant_bank_account_id = @merchant_bank_account_id
		
	END
END
GO
