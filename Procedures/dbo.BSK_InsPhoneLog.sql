SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-11-18
-- Description:	Saves a phone log into the database.  Many logs can happen during a single call
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsPhoneLog]
(
	@call_to				[VARCHAR] (50),
	@call_from				[VARCHAR] (50),
	@call_id				[NVARCHAR] (200),
	@conversation_id		[NVARCHAR] (200),
	@status					[NVARCHAR] (100),
	@direction				[VARCHAR] (50),
	@start_time				[DATETIME],
	@end_time				[DATETIME],
	@duration				[INT],
	@rate					[NUMERIC] (16, 10),
	@price					[NUMERIC] (16, 10),
	@raw_response			[NVARCHAR] (MAX),
	@event_type				[VARCHAR] (50),
	@affiliate_id			[INT],
	@users_email			[NVARCHAR] (100),
	@step					[VARCHAR] (15),
	@source					[VARCHAR] (50),
	@dial_call_status		[VARCHAR] (50)
)
AS
BEGIN
SET NOCOUNT ON;


INSERT INTO phone_logs(call_to, call_from, call_id, conversation_id, status, direction, start_time, end_time, duration, rate, price, create_date, change_date, change_user, create_user, 
	raw_response, event_type, affiliate_id, users_email, step, [source], dial_call_status)
VALUES (@call_to, @call_from, @call_id, @conversation_id, @status, @direction, @start_time, @end_time, @duration, @rate, @price, GETUTCDATE(), GETUTCDATE(), 'SYSTEM', 'SYSTEM', 
	@raw_response, @event_type, @affiliate_id, @users_email, @step, @source, @dial_call_status)

END

GO
