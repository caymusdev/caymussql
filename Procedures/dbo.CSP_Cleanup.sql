SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-04-18
-- Description:	Used to clean up old data such as the imports from forte that are in the forte-staging table
-- =============================================
CREATE PROCEDURE [dbo].[CSP_Cleanup]	
AS
BEGIN		

SET NOCOUNT ON;

-- delete from Forte-staging anything older than 6 months
-- DELETE FROM forte_staging WHERE [Created Date] < DATEADD(month, -6, GETUTCDATE())
DELETE FROM api_logs WHERE create_date < DATEADD(month, -6, GETUTCDATE()) -- older than 6 months
DELETE FROM app_logs WHERE create_date < DATEADD(month, -36, GETUTCDATE()) -- older than 3 years
-- DELETE FROM boa_staging WHERE create_date < DATEADD(month, -6, GETUTCDATE()) -- older than 6 months
DELETE FROM exception_logs WHERE create_date < DATEADD(month, -1, GETUTCDATE()) -- older than 1 years
DELETE FROM import_logs WHERE create_date < DATEADD(month, -12, GETUTCDATE()) -- older than 1 years

DELETE FROM api_logs WHERE api_call = 'SOLOLogs' AND create_date < DATEADD(month, -1, GETUTCDATE()) -- solo logs older than 1 month


-- delete any work records that are not changed
DELETE FROM funding_payment_dates_work WHERE COALESCE(changed, 0) = 0 AND COALESCE(approved, 0) = 0 AND COALESCE(ready_for_approval, 0) = 0
	AND rejected_date IS NULL

DELETE FROM keep_alives WHERE create_date < DATEADD(month, -6, GETUTCDATE()) -- older than 6 months


END
GO
