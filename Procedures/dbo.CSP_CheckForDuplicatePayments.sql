SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-12-17
-- Description:	Checks for duplicate payments in the queue
-- =============================================
CREATE PROCEDURE [dbo].[CSP_CheckForDuplicatePayments]
	
AS
BEGIN	
SET NOCOUNT ON;

-- all payments have a funding on them so donl't need this first sql
/*
SELECT p.payment_id, p.trans_date, p.amount, f.legal_name, f.contract_number
FROM payments p
INNER JOIN funding_payment_dates pd ON p.funding_payment_date_id = pd.id
INNER JOIN fundings f ON pd.funding_id = f.id
WHERE EXISTS 
	(SELECT 1 FROM payments p2
	 INNER JOIN funding_payment_dates pd2 ON p2.funding_payment_date_id = pd2.id
	 WHERE p.amount = p2.amount AND p.trans_date = p2.trans_date AND p.transaction_id IS NULL AND p2.transaction_id IS NULL AND p.payment_id <> p2.payment_id
		AND pd.funding_id = pd2.funding_id AND p2.deleted = 0 AND COALESCE(p2.is_fee, 0) = 0)
	AND p.deleted = 0 AND COALESCE(p.is_fee, 0) = 0
	AND COALESCE(f.on_hold, 0) = 0
UNION
*/
SELECT p.payment_id, p.trans_date, p.amount, f.legal_name, f.contract_number
FROM payments p
INNER JOIN fundings f ON p.funding_id = f.id
WHERE EXISTS 
	(SELECT 1 FROM payments p2
	 INNER JOIN fundings f2 ON p2.funding_id = f2.id
	 WHERE p.amount = p2.amount AND p.trans_date = p2.trans_date AND p.transaction_id IS NULL AND p2.transaction_id IS NULL AND p.payment_id <> p2.payment_id
		AND p.funding_id = p2.funding_id AND p2.deleted = 0 AND COALESCE(p2.is_fee, 0) = 0)
	AND p.deleted = 0 AND COALESCE(p.is_fee, 0) = 0
	AND COALESCE(f.on_hold, 0) = 0
ORDER BY f.legal_name, p.amount, p.payment_id



END
GO
