SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-05-05
-- Description:	Gets data about all of the files that are sent and or received
-- =============================================
CREATE PROCEDURE dbo.CSP_WidgetsGetFiles

AS
BEGIN
SET NOCOUNT ON;


SELECT y.RowNumber, y.file_type, y.last_download, y.next_scheduled_download, y.downloaded_today
FROM (
	SELECT ROW_NUMBER() OVER (PARTITION BY x.file_type ORDER BY x.last_download DESC) AS RowNumber, x.file_type, x.last_download, 
		x.next_scheduled_download, x.downloaded_today
	FROM (
		SELECT CASE LEFT(file_name, 3)
			WHEN 'BOA' THEN 'Bank Of America' 
			WHEN 'EVO' THEN 'EVO'
			WHEN 'IPS' THEN 'IPS'  END AS file_type, 
			i.create_date AS last_download, 
			'' AS next_scheduled_download, 
			CASE WHEN CONVERT(DATE, create_date) = CONVERT(DATE, GETUTCDATE()) THEN 1 ELSE 0 END AS downloaded_today
		FROM import_logs i
		WHERE i.message LIKE '%file was imported at%'
		UNION ALL
		SELECT 'Fifth Third', l.processed_date, '', 
			CASE WHEN CONVERT(DATE, processed_date) = CONVERT(DATE, GETUTCDATE()) THEN 1 ELSE 0 END AS downloaded_today
		FROM nacha_logs l
		WHERE l.nacha_file_name LIKE 'CAYF_ACHADVICE%'
		UNION ALL
		SELECT 'Forte', l.create_date, '', 
			CASE WHEN CONVERT(DATE, create_date) = CONVERT(DATE, GETUTCDATE()) THEN 1 ELSE 0 END AS downloaded_today
		FROM api_logs l
		WHERE l.api_call = 'ForteFundings'
	) x
) y
WHERE y.RowNumber = 1
ORDER BY y.file_type



END
GO
