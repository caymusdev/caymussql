SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-20
-- Description:	Inserts a new reminder record
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsReminder]
(
	@merchant_id				INT,
	@affiliate_id				INT, 
	@contract_id				INT,
	@contact_type				INT,
	@reminder_date				DATETIME,
	@contact_id					INT,
	@user_email 				NVARCHAR (100),
	@notes						NVARCHAR (4000),
	@subject                    NVARCHAR (2000)--,
	--@last_contact_status		NVARCHAR (50) = NULL
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @user_id INT
SELECT @user_id = user_id FROM users WHERE email = @user_email

DECLARE @day_amount INT
SELECT @day_amount = value FROM settings WHERE setting = 'LeftVMFollowup'

INSERT INTO reminders(merchant_id, affiliate_id, contract_id, contact_type_id, reminder_date, contact_id, user_id, notes, completed, [subject], 
	create_date, change_date, change_user, create_user)
VALUES (@merchant_id, @affiliate_id, @contract_id, @contact_type,
--	CASE WHEN @reminder_date IS NULL AND (@last_contact_status IN ('LVM', 'NoAnswer') OR @last_contact_status IS NULL) 
--		THEN (SELECT DATEADD(day, @day_amount, GETUTCDATE()) AS DateAdd) ELSE @reminder_date END,
	@reminder_date,
	@contact_id, @user_id, @notes, NULL, @subject, GETUTCDATE(), GETUTCDATE(), @user_email, @user_email)


SELECT SCOPE_IDENTITY() as reminder_id

UPDATE c
SET next_follow_up_date = @reminder_date, change_date = GETUTCDATE(), change_user = @user_email
FROM cases c
INNER JOIN affiliates a ON c.affiliate_id = a.affiliate_id AND a.affiliate_id = @affiliate_id
WHERE c.active = 1
	AND COALESCE(c.next_follow_up_date, '2050-01-01') >= @reminder_date


END

GO
