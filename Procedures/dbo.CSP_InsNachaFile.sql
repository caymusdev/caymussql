SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-01-14
-- Description:	Writes to the Nacha Logs table after a file is downloaded and processed 
-- =============================================
CREATE PROCEDURE CSP_InsNachaFile
(
		@nacha_file_name		[VARCHAR](100), 
		@file_data				[NVARCHAR] (MAX)
)
AS
BEGIN
SET NOCOUNT ON;

INSERT INTO nacha_logs(batch_id, nacha_file_name, file_data, processed_date, proc_status, change_user)
VALUES (NULL, @nacha_file_name, @file_data, GETUTCDATE(), 'New', 'SYSTEM')

END
GO
