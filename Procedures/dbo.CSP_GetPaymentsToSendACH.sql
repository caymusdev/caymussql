SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-01-03
-- Description:	Gets a list of payments that need to be sent for ACH Processing
-- NOTE *********************** CSP_GetFundingsWithMultiplePaymentsToday is dependent on the result sets columns.  Update CSP_GetFundingsWithMultiplePaymentsToday, 
--    AND CSP_CheckPaymentsQueue AND CSP_CheckForNegativePayments if this changes.
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- UPDATE: 2020-02-14 - Changing from payment schedules to funding payment dates
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetPaymentsToSendACH]
(
	@processor		VARCHAR (50) = NULL,
	@portfolio		VARCHAR (50) = NULL
)
AS
BEGIN
 --SET FMTONLY OFF

IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData
CREATE TABLE #tempData (id int not null identity, payment_id INT, amount MONEY, trans_date DATE, bank_name VARCHAR (100), account_number VARCHAR (50), routing_number VARCHAR (50), 
	legal_name NVARCHAR (100), contract_number VARCHAR (50), funding_id BIGINT, payoff_amount MONEY, float_amount MONEY, processor VARCHAR (50), retry_level INT, retry_sublevel INT,
	is_fee INT, process_fees BIT, is_refund BIT, is_debit BIT, redirect_contract_number NVARCHAR (50), portfolio VARCHAR (50))


SET NOCOUNT ON;
DECLARE @today DATETIME; SET @today = CONVERT(DATE, DATEADD(HH, -5, GETUTCDATE()))
DECLARE @weekDay INT; SET @weekDay = DATEPART(WEEKDAY, @today)


INSERT INTO #tempData (payment_id, amount, trans_date, bank_name, account_number, routing_number, legal_name, contract_number, funding_id, payoff_amount, float_amount, processor, retry_level,
	retry_sublevel, is_fee, process_fees, is_refund, is_debit, redirect_contract_number, portfolio)
SELECT p.payment_id, p.amount, p.trans_date, ba.bank_name, ba.account_number, ba.routing_number, f.legal_name, f.contract_number,
	f.id AS funding_id, f.payoff_amount, f.float_amount, 
	CASE WHEN p.is_refund = 1 THEN 'Regions' WHEN COALESCE(p.is_fee, 0) = 1 OR COALESCE(p.retry_level, 0) > 0 THEN 'Forte' 
		ELSE CASE pd.payment_processor WHEN '53' THEN 'Fifth Third' ELSE pd.payment_processor END END AS processor,
	COALESCE(p.retry_level, 0) AS retry_level, COALESCE(p.retry_sublevel, 0) AS retry_sublevel, COALESCE(p.is_fee, 0) AS is_fee, f.process_fees, p.is_refund, p.is_debit,
	f2.contract_number, f.portfolio
FROM payments p
--INNER JOIN payment_schedules ps ON p.payment_schedule_id = ps.payment_schedule_id
--INNER JOIN funding_payment_schedules fps ON p.payment_schedule_id = fps.payment_schedule_id
INNER JOIN funding_payment_dates pd ON p.funding_payment_date_id = pd.id
INNER JOIN fundings f ON pd.funding_id = f.id
LEFT JOIN fundings f2 ON p.redirect_funding_id = f2.id
INNER JOIN merchant_bank_accounts ba ON pd.merchant_bank_account_id = ba.merchant_bank_account_id
WHERE p.approved_flag = 1 AND trans_date <= DATEADD(D, CASE WHEN @weekDay < 6 THEN 1 WHEN @weekDay = 6 THEN 3 ELSE 2 END, @today) AND processed_date IS NULL
	AND p.amount <> 0 AND p.deleted = 0 AND p.waived = 0
	AND ((pd.payment_processor = COALESCE(@processor, pd.payment_processor) AND p.is_refund = 0)
		OR (p.is_refund = 1 AND COALESCE(@processor, 'Regions') = 'Regions'))
	AND pd.payment_method = 'ACH'
	AND (COALESCE(f.on_hold, 0) = 0
		OR f.on_hold = 1 AND f.payoff_amount = 0 AND p.is_refund = 1) --- don't pull payment for someone on hold unless it is a refund (for overpayment on closed account)
	AND (f.portfolio = @portfolio OR @portfolio IS NULL)
UNION 
-- get ones tied directly to a funding, probably a fee for retrying payments
SELECT p.payment_id, p.amount, p.trans_date, COALESCE(ba.bank_name, f.receivables_bank_name), COALESCE(ba.account_number, f.receivables_account_nbr), 
	COALESCE(ba.routing_number, f.receivables_aba), f.legal_name, f.contract_number,
	f.id AS funding_id, f.payoff_amount, f.float_amount, 
	CASE WHEN p.is_refund = 1 THEN 'Regions' WHEN COALESCE(p.is_fee, 0) = 1 OR COALESCE(p.retry_level, 0) > 0 THEN 'Forte' 
		ELSE CASE COALESCE(f.processor, '')  WHEN '53' THEN 'Fifth Third' ELSE COALESCE(f.processor, '')  END END AS processor, 
	COALESCE(p.retry_level, 0), COALESCE(p.retry_sublevel, 0), COALESCE(p.is_fee, 0),
	f.process_fees, p.is_refund, p.is_debit, f2.contract_number, f.portfolio
FROM payments p
INNER JOIN fundings f ON p.funding_id = f.id
--INNER JOIN payments p_orig ON p.orig_transaction_id = p_orig.transaction_id
--LEFT JOIN payment_schedules ps ON p_orig.payment_schedule_id = ps.payment_schedule_id
LEFT JOIN fundings f2 ON p.redirect_funding_id = f2.id
LEFT JOIN merchant_bank_accounts ba ON f.receivables_bank_name = ba.bank_name AND f.receivables_aba = ba.routing_number AND f.receivables_account_nbr = ba.account_number
	AND f.merchant_id = ba.merchant_id
WHERE p.approved_flag = 1 AND p.trans_date <= DATEADD(D, CASE WHEN @weekDay < 6 THEN 1 WHEN @weekDay = 6 THEN 3 ELSE 2 END, @today) AND p.processed_date IS NULL
	AND p.amount <> 0 AND p.deleted = 0 AND p.waived = 0
	AND ((COALESCE(f.processor, '') = COALESCE(@processor, f.processor) AND p.is_refund = 0)
		OR (p.is_refund = 1 AND COALESCE(@processor, 'Regions') = 'Regions'))
	AND p.funding_payment_date_id IS NULL
		AND (COALESCE(f.on_hold, 0) = 0
			OR f.on_hold = 1 AND f.payoff_amount = 0 AND p.is_refund = 1) --- don't pull payment for someone on hold unless it is a refund (for overpayment on closed account)
	AND (f.portfolio = @portfolio OR @portfolio IS NULL)
UNION 
-- get iso bonus refunds - not tied to a contract
SELECT p.payment_id, p.amount, p.trans_date, i.bank_name, i.account_number, i.routing_number, i.legal_name, i.name_on_check,
	b.iso_id AS funding_id, p.amount, 0, 'Regions' AS processor,
	COALESCE(p.retry_level, 0) AS retry_level, COALESCE(p.retry_sublevel, 0) AS retry_sublevel, COALESCE(p.is_fee, 0) AS is_fee, 0 AS process_fees, p.is_refund, p.is_debit,
	i.name_on_check, 'caymus' AS portfolio
FROM payments p
INNER JOIN iso_bonuses b ON p.iso_bonus_id = b.iso_bonus_id
INNER JOIN (
	SELECT MAX(f.id) AS funding_id, f.iso_bonus_id
	FROM fundings f
	GROUP BY f.iso_bonus_id
) x ON b.iso_bonus_id = x.iso_bonus_id
INNER JOIN funding_isos i ON x.funding_id = i.funding_id AND i.is_referring = 0
WHERE p.approved_flag = 1 AND trans_date <= DATEADD(D, CASE WHEN @weekDay < 6 THEN 1 WHEN @weekDay = 6 THEN 3 ELSE 2 END, @today) AND processed_date IS NULL
	AND p.amount <> 0 AND p.deleted = 0 AND p.waived = 0
	AND p.is_refund = 1 AND COALESCE(@processor, 'Regions') = 'Regions'	
	AND p.funding_id IS NULL AND p.funding_payment_date_id IS NULL
	AND COALESCE(@portfolio, 'caymus') = 'caymus'

	
--UNION 
-- need to get ones tied directly to a funding but the orig_transaction is not in our db so try to get active payment schedule
-- don't need this one anymore because when it's not tied to a funding payment date we just get the bank info off of the funding directly (original one)
/*
SELECT p.payment_id, p.amount, p.trans_date, COALESCE(ps.bank_name, f.receivables_bank_name), 
	COALESCE(ps.account_number, f.receivables_account_nbr), COALESCE(ps.routing_number, f.receivables_aba), f.legal_name, f.contract_number,
	f.id AS funding_id, f.payoff_amount, f.float_amount,
	CASE p.is_refund WHEN 1 THEN 'Fifth Third' ELSE CASE COALESCE(ps.payment_processor, f.processor)  WHEN '53' THEN 'Fifth Third' ELSE COALESCE(ps.payment_processor, f.processor)  END END AS processor, 
	COALESCE(p.retry_level, 0), COALESCE(p.retry_sublevel, 0), COALESCE(p.is_fee, 0),
	f.process_fees, p.is_refund, p.is_debit, f2.contract_number
FROM payments p
INNER JOIN fundings f ON p.funding_id = f.id
LEFT JOIN payments p_orig ON p.orig_transaction_id = p_orig.transaction_id
LEFT JOIN payment_schedules ps ON ps.payment_schedule_id = (
	SELECT MAX(ps.payment_schedule_id) AS payment_schedule_id
	FROM funding_payment_schedules fps
	INNER JOIN payment_schedules ps ON fps.payment_schedule_id = ps.payment_schedule_id
	WHERE ps.active = 1 AND fps.funding_id = f.id AND ps.payment_method = 'ACH'
)
LEFT JOIN fundings f2 ON p.redirect_funding_id = f2.id
WHERE p.approved_flag = 1 AND p.trans_date <= DATEADD(D, CASE WHEN @weekDay < 6 THEN 1 WHEN @weekDay = 6 THEN 3 ELSE 2 END, @today) AND p.processed_date IS NULL
	AND p.amount <> 0  AND p.deleted = 0 AND p.waived = 0
	AND ((COALESCE(ps.payment_processor, f.processor) = COALESCE(@processor, ps.payment_processor, f.processor) AND p.is_refund = 0)
		OR (p.is_refund = 1 AND COALESCE(@processor, '53') = '53'))
	AND p.payment_schedule_id IS NULL
	AND p_orig.payment_id IS NULL -- the origination transaction was not sent as a payment from our system (mostly at time of migration)
*/
ORDER BY f.legal_name, COALESCE(p.is_fee, 0), COALESCE(p.retry_level, 0), COALESCE(p.retry_sublevel, 0)



-- Put top X per funding payments into a temp table
SELECT TOP 1 WITH TIES d.payment_id, d.amount, d.trans_date, d.bank_name, d.account_number, d.routing_number, d.legal_name, d.contract_number, d.funding_id, d.payoff_amount, d.float_amount, 
	d.processor, d.retry_level, d.retry_sublevel, d.is_fee, d.is_refund, d.is_debit, d.redirect_contract_number, d.portfolio
INTO #temp2
FROM #tempData d
WHERE d.is_fee = 0
ORDER BY CASE WHEN ROW_NUMBER() OVER(PARTITION BY d.funding_id ORDER BY d.payment_id) <= 5 then 0 else 1 end
-- TOP 1 WITH TIES will take just one record per funding but with ties so you use the ORDER BY CASE statement returning only 0 or 1 so that the first X will be returned


-- NOW pull out all payments that are not fees and then just 1 fee per customer
SELECT d.payment_id, d.amount, d.trans_date, d.bank_name, d.account_number, d.routing_number, d.legal_name, d.contract_number, d.funding_id, d.payoff_amount, d.float_amount, 
	d.processor, d.retry_level, d.retry_sublevel, d.is_fee, d.is_refund, d.is_debit, d.redirect_contract_number, portfolio
FROM #temp2 d
UNION ALL
SELECT d.payment_id, d.amount, d.trans_date, d.bank_name, d.account_number, d.routing_number, d.legal_name, d.contract_number, d.funding_id, d.payoff_amount, d.float_amount, 
	d.processor, d.retry_level, d.retry_sublevel, d.is_fee, d.is_refund, d.is_debit, d.redirect_contract_number, portfolio
FROM #tempData d
INNER JOIN (
	SELECT MIN(t.id) AS min_id, t.funding_id
	FROM #tempData t
	WHERE t.is_fee = 1
	GROUP BY t.funding_id
) x ON d.id = x.min_id 
WHERE d.is_fee = 1 AND d.process_fees = 1
ORDER BY legal_name, is_fee, retry_level, retry_sublevel


IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData
IF OBJECT_ID('tempdb..#temp2') IS NOT NULL DROP TABLE #temp2

END

GO
