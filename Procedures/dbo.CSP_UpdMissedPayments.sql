SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-02-17
-- Description:	when an account is on hold we need to mark payments and payment dates as being missed
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdMissedPayments]

AS
BEGIN	
SET NOCOUNT ON;

-- this wil run in the morning so we're looking for payments that are less than today that are NOT processed and on fundings that ARE on hold
-- mark the payment has deleted and the payment date as missed
DECLARE @today DATE; SET @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''

BEGIN TRANSACTION

-- to ensure data consistency we'll put all the funding payment dates missed into a temp table so that we can separately update the payments table
IF OBJECT_ID('tempdb..#tempIDs') IS NOT NULL DROP TABLE #tempIDs
CREATE TABLE #tempIDs (id int not null identity (1, 1), funding_payment_date_id INT)

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

INSERT INTO #tempIDs (funding_payment_date_id)
SELECT pd.id
FROM funding_payment_dates pd
INNER JOIN fundings f ON pd.funding_id = f.id
INNER JOIN payments p ON pd.id = p.funding_payment_date_id
WHERE f.on_hold = 1 AND p.processed_date IS NULL AND p.waived = 0 AND p.deleted = 0 AND pd.payment_date <= @today

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

UPDATE pd
SET missed = 1, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM funding_payment_dates pd
INNER JOIN #tempIDs t ON pd.id = t.funding_payment_date_id

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

-- now delete the payments
UPDATE p
SET deleted = 1, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM payments p
INNER JOIN #tempIDs t ON p.funding_payment_date_id = t.funding_payment_date_id
WHERE p.processed_date IS NULL AND p.waived = 0 AND p.deleted = 0 -- extra sure where clause(shouldn't be necessary because of temp table join)


COMMIT TRANSACTION
GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)

Done:


END
GO
