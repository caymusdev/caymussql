SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
 -- =============================================
-- Author:      Rick Pina
-- Create Date: 2021-02-04
-- Description: Either adds or removes a user from a department
-- =============================================
CREATE PROCEDURE [dbo].[BSK_UpdUserDepartment]
(
	@department_id				INT,
	@has_role				BIT,
	@user_id				INT,
	@change_user			NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON

IF @has_role = 1
	BEGIN
		INSERT INTO user_departments(department_id, user_id, create_date, change_date, change_user, create_user)
		SELECT @department_id, @user_id, GETUTCDATE(), GETUTCDATE(), @change_user, @change_user
		WHERE NOT EXISTS (SELECT 1 FROM user_departments ud WHERE ud.department_id = @department_id AND ud.user_id = @user_id)
	END
ELSE
	BEGIN
		-- update for sake of trigger
		UPDATE user_departments
		SET change_date = GETUTCDATE(), change_user = @change_user
		WHERE department_id = @department_id AND user_id = @user_id

		DELETE FROM user_departments WHERE department_id = @department_id AND user_id = @user_id
	END

END
GO
