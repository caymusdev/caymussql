SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-14
-- Description:	Saves an email into the database
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsEmail]
(
	@affiliate_id				INT,
	@merchant_id				INT,
	@contract_id				INT, 
	@subject					NVARCHAR (1000),
	@body						NVARCHAR (MAX),
	@user_email					NVARCHAR (100),
	@sent						DATETIME,
	@received					DATETIME,
	@message_id					NVARCHAR (100),
	@unique_id					NVARCHAR (200),
	@incoming					BIT = NULL,
	@from_address				NVARCHAR (100) = NULL
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @user_id INT
SELECT @user_id = user_id FROM users WHERE email = @user_email

INSERT INTO emails(affiliate_id, merchant_id, contract_id, subject, body, sender_id, sent, received, create_date, change_date, change_user, create_user, message_id, unique_id, 
	incoming, from_address)
SELECT @affiliate_id, @merchant_id, @contract_id, @subject, @body, @user_id, @sent, @received, GETUTCDATE(), GETUTCDATE(), @user_email, @user_email, @message_id, @unique_id, 
	@incoming, @from_address
WHERE NOT EXISTS (SELECT 1 FROM emails WHERE unique_id = @unique_id)

SELECT SCOPE_IDENTITY() AS NewEmailID

END

GO
