SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-16
-- Description:	Checks for affiliates with no cases and creates them
-- =============================================
CREATE PROCEDURE [dbo].[BSK_CreateCases]

AS
BEGIN
SET NOCOUNT ON;

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())

IF OBJECT_ID('tempdb..#tempAffiliates') IS NOT NULL DROP TABLE #tempAffiliates
CREATE TABLE #tempAffiliates (id INT IDENTITY (1, 1), affiliate_id INT)

IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL DROP TABLE #tempUsers
CREATE TABLE #tempUsers (id INT IDENTITY (1, 1), user_id INT, num_cases INT)

-- get a list of affiliates without cases  
/*
INSERT INTO #tempAffiliates (affiliate_id)
SELECT affiliate_id 
FROM affiliates 
WHERE affiliate_id NOT IN (SELECT affiliate_id FROM cases c INNER JOIN collector_cases cc ON c.case_id = cc.case_id)
*/

INSERT INTO #tempAffiliates(affiliate_id)
SELECT DISTINCT f.affiliate_id
FROM fundings f
INNER JOIN affiliates a ON f.affiliate_id = a.affiliate_id
WHERE (COALESCE(f.collection_type, '') <> '' AND f.payoff_amount > 0)
	OR (a.sent_to_collect IS NOT NULL AND a.closed_in_collections IS NULL)


-- get a count of assigned cases for each collector that is a Customer Service
INSERT INTO #tempUsers (user_id, num_cases)
SELECT ud.user_id, SUM(CASE WHEN c.case_id IS NULL THEN 0 ELSE 1 END) AS num_cases
FROM user_departments ud
INNER JOIN departments d ON ud.department_id = d.department_id
LEFT JOIN collector_cases cc ON ud.user_id = cc.collector_user_id AND cc.start_date <= @today AND COALESCE(cc.end_date, '2050-05-09') >= @today
LEFT JOIN cases c ON cc.case_id = c.case_id AND c.active = 1
WHERE d.department = 'CustomerService' 
GROUP BY ud.user_id
ORDER BY num_cases 

DECLARE @maxID INT, @currentID INT, @tempUserID INT, @newCaseID INT
SELECT @maxID = MAX(id), @currentID = 1 FROM #tempAffiliates

DECLARE @department_id INT, @case_status_id INT, @bucket_status_id INT
SELECT @department_id = department_id FROM departments WHERE department = 'CustomerService'
SELECT @case_status_id = case_status_id FROM case_statuses WHERE status = 'Active'
SELECT @bucket_status_id = bucket_status_id FROM bucket_statuses WHERE status = 'InProcess'

WHILE @currentID <= @maxID
	BEGIN
		SELECT TOP 1 @tempUserID = user_id
		FROM #tempUsers
		ORDER BY num_cases

		INSERT INTO cases(affiliate_id, create_date, change_date, create_user, change_user, active, department_id, case_status_id, bucket_status_id, new_case, collection_date)
		SELECT affiliate_id, GETUTCDATE(), GETUTCDATE(), 'System', 'System', 1, @department_id, @case_status_id, @bucket_status_id, 1, GETUTCDATE()
		FROM #tempAffiliates
		WHERE id = @currentID

		SELECT @newCaseID = SCOPE_IDENTITY()

		INSERT INTO collector_cases (collector_user_id, case_id, start_date, end_date, create_date, change_date, change_user, create_user)
		VALUES (@tempUserID, @newCaseID, @today, NULL, GETUTCDATE(), GETUTCDATE(), 'SystemCreateCases', 'SystemCreateCases')

		INSERT INTO case_departments (case_id, department_id, start_date, end_date, create_date, change_date, change_user, create_user, do_not_move, do_not_move_by, next_move_on, next_move_to)
		VALUES (@newCaseID, @department_id, @today, NULL, GETUTCDATE(), GETUTCDATE(), 'SystemCreateCases', 'SystemCreateCases', NULL, NULL, NULL, NULL)

		-- create a notification for the new case
		DECLARE @affiliate_id INT, @affiliate_name NVARCHAR (100)
		SELECT @affiliate_id = affiliate_id FROM #tempAffiliates WHERE id = @currentID
		SELECT @affiliate_name = 'New Case for ' + affiliate_name FROM affiliates WHERE affiliate_id = @affiliate_id

		EXEC dbo.BSK_InsNotification @tempUserID, NULL, @affiliate_id, NULL, NULL, @affiliate_name, 'NewCase', 'System', @newCaseID

		-- update the count of cases for this user
		UPDATE #tempUsers SET num_cases = num_cases + 1 WHERE user_id = @tempUserID

		SET @currentID = @currentID + 1
	END


UPDATE a
SET a.sent_to_collect = COALESCE(a.sent_to_collect, GETUTCDATE()), -- if it hasn't been sent yet, then set GETUTCDATE otherwise leasve as it
	a.closed_in_collections = CASE
		WHEN EXISTS (SELECT 1 FROM fundings f WHERE f.payoff_amount > 0 AND f.affiliate_id = a.affiliate_id) THEN a.closed_in_collections
		ELSE GETUTCDATE() END, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM affiliates a
INNER JOIN #tempAffiliates x ON a.affiliate_id = x.affiliate_id


UPDATE f
SET f.sent_to_collect = COALESCE(f.sent_to_collect, GETUTCDATE()), -- if it hasn't been sent yet, then set GETUTCDATE otherwise leasve as it
	f.closed_in_collections = CASE
		WHEN EXISTS (SELECT 1 FROM fundings f2 WHERE f2.payoff_amount > 0 AND f2.affiliate_id = f.affiliate_id) THEN f.closed_in_collections
		ELSE GETUTCDATE() END, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM fundings f
INNER JOIN #tempAffiliates x ON f.affiliate_id = x.affiliate_id
END

GO
