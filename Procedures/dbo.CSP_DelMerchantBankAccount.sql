SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-01-31
-- Description:	Deletes a Merchant Bank Account if it is not in use.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_DelMerchantBankAccount]
(
	@merchant_bank_account_id	INT,
	@userid						NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON;

IF EXISTS(SELECT 1 
			FROM funding_payment_dates d WITH (NOLOCK)	
			WHERE d.merchant_bank_account_id = @merchant_bank_account_id
			UNION ALL
			SELECT 1
			FROM funding_payment_dates_work d WITH (NOLOCK)	
			WHERE d.merchant_bank_account_id = @merchant_bank_account_id
			)
	BEGIN
		RAISERROR ('The bank account has been used and cannot be deleted.', 16, 1)
	END
ELSE
	BEGIN
		UPDATE merchant_bank_accounts SET change_user  = @userid, change_date  = GETUTCDATE() WHERE merchant_bank_account_id = @merchant_bank_account_id
		DELETE FROM merchant_bank_accounts WHERE merchant_bank_account_id = @merchant_bank_account_id
	END
END
GO
