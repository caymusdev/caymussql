SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-01-29
-- Description:	Checks to see if key fields are the same on the resultant records.  Used when editing multiple days in calendar view.  Want to know if all selected dates
-- have the same info and can be edited or if something is different and they should not be edited together
-- =============================================
CREATE PROCEDURE [dbo].[CSP_AreAllFundedDatesSame]
(
	@funding_id				BIGINT,
	@start_date				DATETIME, 
	@end_date				DATETIME
)
AS
BEGIN	
SET NOCOUNT ON;

SELECT COUNT(*) AS NumberOfDifferences
FROM (
	SELECT pd.amount, pd.merchant_bank_account_id, mba.bank_name, mba.routing_number, mba.account_number, pd.payment_method, pd.payment_processor
	FROM funding_payment_dates pd WITH (NOLOCK)
	LEFT JOIN merchant_bank_accounts mba ON pd.merchant_bank_account_id = mba.merchant_bank_account_id
	WHERE pd.funding_id = @funding_id AND pd.payment_date BETWEEN @start_date AND @end_date AND pd.active = 1
	GROUP BY pd.amount, pd.merchant_bank_account_id, mba.bank_name, mba.routing_number, mba.account_number, pd.payment_method, pd.payment_processor
) x

END
GO
