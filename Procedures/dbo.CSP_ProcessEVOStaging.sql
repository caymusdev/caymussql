SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-06-08
-- Description:	Takes a evo_staging id and will create transactions for it
-- =============================================
CREATE PROCEDURE [dbo].[CSP_ProcessEVOStaging]
(
	@evo_staging_id			INT
)

AS
BEGIN
SET NOCOUNT ON;

DECLARE @funding_id INT;

-- first need to get the funding for the settlement
DECLARE @mid NVARCHAR (100), @merchant_name NVARCHAR (200), @trans_date DATE, @trans_amount MONEY, @batch_id INT, @record_id INT
SELECT @mid = s.MID, @merchant_name = s.DBA, @trans_date = s.[Date], @trans_amount = s.[Split Amount], @batch_id = s.BatchID, @record_id = s.evo_staging_id
FROM evo_staging s
WHERE s.evo_staging_id = @evo_staging_id

EXEC @funding_id = dbo.CSP_GetFundingIDForTransaction @trans_date, NULL, @mid, @merchant_name
IF @funding_id = -1 SET @funding_id = NULL


-- EXEC dbo.CSP_DistributeTransaction @trans_date, @funding_id, @trans_amount, @merchant_name, @batch_id, @record_id, NULL, 'CC'
EXEC dbo.CSP_DistributeTransaction_v2 @funding_id, @trans_amount, NULL, @trans_date, @merchant_name, @batch_id, @record_id, 'evo_staging', 
	NULL, @trans_date, 'CC', NULL, 4

END
GO
