SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:     Ryan Brown
-- Create Date: 2020-08-19
-- Description: Gets a list of active fundings that have lots of outstanding fees and compares to their original payment plan
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetFundingsWithLotsOfFees]

AS
BEGIN
SET NOCOUNT ON

SELECT f.id AS funding_id, f.fees_out, CASE f.frequency WHEN 'Daily' THEN f.weekday_payment ELSE f.weekly_payment END AS payment, 
	f.legal_name, f.contract_number, f.frequency, 
	CONVERT(DECIMAL(10,2), f.fees_out / CASE f.frequency WHEN 'Daily' THEN f.weekday_payment ELSE f.weekly_payment END) AS NumExtraPayments, 
	f.last_payment_date, f.collection_type
FROM fundings f 
WHERE f.fees_out > CASE f.frequency WHEN 'Daily' THEN f.weekday_payment ELSE f.weekly_payment END
	AND f.contract_status = 1
ORDER BY f.legal_name


END
GO
