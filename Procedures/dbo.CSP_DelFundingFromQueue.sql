SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-01-20
-- Description:	Deletes a funding out of the queue
-- =============================================
CREATE PROCEDURE [dbo].[CSP_DelFundingFromQueue]
(
	@funding_id		BIGINT,
	@userid			NVARCHAR (100)
)
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @affiliate_id BIGINT, @merchant_id BIGINT, @processor VARCHAR (50), @contract_type VARCHAR (50), @funded_date DATETIME
DECLARE @retVal INT

DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''

SELECT @processor = f.processor, @contract_type = f.contract_type, @funded_date = f.funded_date FROM fundings f WHERE f.id = @funding_id


IF @processor IS NOT NULL OR @funded_date IS NOT NULL
	BEGIN
		-- this already has a processor or funded date (so it shouldn't even be in the queue but just as an extra check)
		SET @retVal = 1
	END
ELSE IF LOWER(@contract_type) = 'renewal' 
	BEGIN
		-- this is a renewal and much more work to cleanup so not going to have the system do it.
		SET @retVal = 2
	END
ELSE IF @processor IS NULL AND @funded_date IS NULL  -- hasn't not been funded so we can clear it out
	BEGIN
		BEGIN TRANSACTION

		SELECT @affiliate_id = affiliate_id, @merchant_id = merchant_id 
		FROM fundings f
		WHERE f.id = @funding_id AND f.processor IS NULL
	
		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		-- first delete any competitor payoffs
		DELETE FROM funding_distributions 
		WHERE funding_id = @funding_id
		
		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		UPDATE funding_isos 
		SET change_date = GETUTCDATE(), change_user = @userid 
		WHERE funding_id = @funding_id

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		DELETE FROM funding_isos 
		WHERE funding_id = @funding_id
		
		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		-- now can delete the funding
		UPDATE fundings SET change_user = @userid, change_date = GETUTCDATE() WHERE id = @funding_id AND processor IS NULL
		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		DELETE FROM fundings
		WHERE id = @funding_id AND processor IS NULL

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
	
		-- delete the merchant if it has no other fundings
		DELETE FROM merchants
		WHERE merchant_id = @merchant_id AND NOT EXISTS (SELECT 1 FROM fundings WHERE merchant_id = @merchant_id)

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		-- delete the affiliate if it has no other fundings
		DELETE FROM affiliates
		WHERE affiliate_id = @affiliate_id AND NOT EXISTS (SELECT 1 FROM fundings WHERE affiliate_id = @affiliate_id)

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


		COMMIT TRANSACTION
		SET @retVal = 0
		GOTO Done

		ERROR:
			ROLLBACK TRANSACTION
			SET @retVal = 3
			RAISERROR (@ErrorMessage, 16, 1)
	END

Done:

SELECT @retVal AS ResultCode

END
GO
