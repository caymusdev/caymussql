SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:     Ryan Brown
-- Create Date: 2021-04-22
-- Description: When a funding goes on hold we need to mark active any collection cases the affiliate may have
-- =============================================
CREATE PROCEDURE [dbo].[BSK_MarkFundingCaseActive]
(
	@funding_id				BIGINT
)
AS

SET NOCOUNT ON;
DECLARE @active_id INT, @inactive_id INT, @rowCount INT, @affiliate_id INT, @contract_number NVARCHAR (50)
SELECT @active_id = cs.case_status_id FROM case_statuses cs WITH (NOLOCK) WHERE cs.status = 'Active'
SELECT @inactive_id = cs.case_status_id FROM case_statuses cs WITH (NOLOCK) WHERE cs.status = 'Inactive'


UPDATE c
SET case_status_id = @active_id, change_date = GETUTCDATE(), change_user = 'FundingOnHold'
FROM cases c
INNER JOIN fundings f ON c.affiliate_id = f.affiliate_id 
WHERE f.id = @funding_id AND c.active = 1 AND c.case_status_id = @inactive_id

SELECT @rowCount = @@ROWCOUNT

IF @rowCount > 0
	BEGIN
		SELECT @affiliate_id = f.affiliate_id, @contract_number = f.contract_number FROM fundings f WHERE f.id = @funding_id
		DECLARE @note NVARCHAR (MAX)
		SET @note = 'Funding ' + @contract_number + ' was put on hold which caused this Case to become active again.'
		-- create a note on that affiliate
		EXEC dbo.BSK_InsAffiliateNote @affiliate_id, 0, @note, 'FundingOnHold'
	END

GO
