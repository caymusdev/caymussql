SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-05-31
-- Description:	returns records for a given batch 
-- c# then loops through and creates transactions records
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetForteStagingForBatch]
(
	@batch_id			INT
)

AS
BEGIN
SET NOCOUNT ON;

SELECT s.[Origination Date], s.[Settle Date], f.id, s.[Total Amount], s.[Settled Response Code], s.Status, s.forte_staging_id
FROM forte_staging s
LEFT JOIN fundings f ON s.[Consumer ID] = f.contract_number
WHERE s.BatchID = @batch_id
ORDER BY s.[Origination Date]


END
GO
