SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-02-10
-- Description:	Used when dragging and dropping a payment to a new date
-- =============================================
CREATE PROCEDURE [dbo].[CSP_MoveFundingPaymentDate]
(
	@worker_id					INT,
--	@session_id					NVARCHAR (100), 
	@userid						NVARCHAR (100),
	@payment_date				DATE,
	@comments					NVARCHAR (4000),
	@change_reason_id			INT
)
AS
BEGIN	
SET NOCOUNT ON;

-- First need to check and see if this user needs approval for editing/creating payment schedules
DECLARE @approved BIT; SET @approved = 1

IF EXISTS (SELECT 1 FROM sec_payment_schedules s WHERE s.submitter_user_id = @userid)
	BEGIN
		SET @approved = 0
	END

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SELECT @tomorrow = dbo.CF_GetTomorrow(@today)

IF @payment_date < @tomorrow
	BEGIN
		SELECT 'Payment Date cannot be in the past: ' + CONVERT(VARCHAR (50), @payment_date) + '/' + CONVERT(VARCHAR (50), @worker_id) AS ErrorText
	END
ELSE
	BEGIN
		-- only editing a single payment date
		UPDATE funding_payment_dates_work
		SET payment_date = @payment_date, change_date = GETUTCDATE(), change_user = @userid, changed = 1, approved = 0,
			comments = @comments, change_reason_id = @change_reason_id
		WHERE id = @worker_id

		IF @approved = 1
			BEGIN
				EXEC dbo.CSP_ApprovePaymentDateChange @worker_id, @userid
			END			
	END
END
GO
