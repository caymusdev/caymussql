SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Rick Pina
-- Create Date: 2021-03-01
-- Description: 
-- =============================================
CREATE PROCEDURE dbo.BSK_ManagementRejectRequests
(
	@approval_id			INT,
	@manager_email			NVARCHAR (100),
	@reject_reason			NVARCHAR (500)
)
AS
BEGIN
SET NOCOUNT ON

DECLARE @approval_type VARCHAR (50), @record_id INT
SELECT @approval_type = approval_type, @record_id = record_id
FROM approvals a 
INNER JOIN approval_types t ON a.approval_type_id = t.approval_type_id
WHERE a.approval_id = @approval_id

UPDATE approvals
SET rejected_by = @manager_email, rejected_date = GETUTCDATE()
WHERE approval_id = @approval_id;

IF @approval_type IN ('PaymentChanges')
	BEGIN
		UPDATE scheduled_payments SET change_date = GETUTCDATE(), change_user = @manager_email, rejected_date = GETUTCDATE(), rejected_user = @manager_email,
			reject_reason = @reject_reason
		WHERE scheduled_payment_id = @record_id				
	END
ELSE IF @approval_type IN ('PaymentPlanChanges')
	BEGIN
		UPDATE scheduled_payments SET change_date = GETUTCDATE(), change_user = @manager_email, rejected_date = GETUTCDATE(), rejected_user = @manager_email,
			reject_reason = @reject_reason
		WHERE payment_plan_id = @record_id
	END
ELSE IF @approval_type IN ('PaymentDelete')
	BEGIN
		UPDATE scheduled_payments SET change_date = GETUTCDATE(), change_user = @manager_email, rejected_date = GETUTCDATE(), rejected_user = @manager_email,
			reject_reason = @reject_reason
		WHERE payment_plan_id = @record_id
	END
ELSE IF @approval_type IN ('PaymentPlanDelete')
	BEGIN
		UPDATE scheduled_payments SET change_date = GETUTCDATE(), change_user = @manager_email, rejected_date = GETUTCDATE(), rejected_user = @manager_email,
			reject_reason = @reject_reason
		WHERE payment_plan_id = @record_id
	END

SELECT ap.description, ap.create_user AS collector_email, a.affiliate_name, @manager_email AS manager_email
FROM approvals ap
LEFT JOIN affiliates a ON ap.affiliate_id = a.affiliate_id
WHERE approval_id = @approval_id
	
END
GO
