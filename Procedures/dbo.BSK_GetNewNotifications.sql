SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-11-09
-- Description:	Gets a list of notifications that have not been sent yet
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetNewNotifications]
AS
BEGIN
SET NOCOUNT ON;

SELECT n.notification_id, n.user_id, u.email, n.affiliate_id, n.notification_message, nt.notification_type, n.record_id
FROM notifications n
INNER JOIN notification_types nt ON n.notification_type_id = nt.notification_type_id
INNER JOIN users u ON n.user_id = u.user_id
WHERE n.displayed_time IS NULL AND n.acted_on_time IS NULL
ORDER BY n.create_date
	
END

GO
