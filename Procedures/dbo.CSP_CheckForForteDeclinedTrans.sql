SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-11-26
-- Description:	Have found transactions that decline but never settle.
-- They sometimes will not have a settlement and other times the transaction will reject the next day and then have a reject settlement.  
--  Will need to change the code for rejects to make sure we did not already assess a fee.
--  See examples below:
--  SELECT * FROM forte_trans WHERE transaction_id = 'trn_8ee0315f-6ba3-4024-b885-8937d842ca33'
--  SELECT * FROM forte_settlements WHERE transaction_id = 'trn_8ee0315f-6ba3-4024-b885-8937d842ca33'
-- =============================================
CREATE PROCEDURE [dbo].[CSP_CheckForForteDeclinedTrans] 
AS
BEGIN
SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#tempIDs') IS NOT NULL DROP TABLE #tempIDs
CREATE TABLE #tempIDs (id int not null identity (1,1), forte_trans_id INT, transaction_id NVARCHAR (100), funding_id BIGINT, amount MONEY)

INSERT INTO #tempIDs (forte_trans_id, transaction_id, funding_id, amount)
SELECT ft.forte_trans_id, ft.transaction_id, f.id, ft.authorization_amount
FROM forte_trans ft
LEFT JOIN transactions t ON ft.forte_trans_id = t.record_id AND t.trans_type_id = 9
INNER JOIN fundings f ON ft.customer_id = f.contract_number
WHERE ft.status = 'declined' AND t.trans_id IS NULL
	AND ft.received_date >= '2018-11-26'
	AND ft.batch_id IS NULL
	AND DATEADD(D, -8, GETUTCDATE()) <= ft.received_date

IF EXISTS (SELECT 1 FROM #tempIDs)
	BEGIN
		DECLARE @maxId INT, @tempId INT, @forte_trans_id INT, @transaction_id NVARCHAR (100), @funding_id BIGINT, @amount MONEY
		SELECT @tempId = 1, @maxId = MAX(id) FROM #tempIDs

		DECLARE @feeAmount MONEY; SELECT @feeAmount = COALESCE(dbo.CF_GetSetting('FeeAmount'), 72)

		DECLARE @today DATE, @newBatchID INT
		SET @today = DATEADD(HOUR, -5, GETUTCDATE())
		EXEC @newBatchID = dbo.CSP_GetNewBatchNumber '', 'API', 'RejectForte', '', @today, 0 
		
		UPDATE forte_settlements
		SET batch_id = @newBatchID
		WHERE forte_funding_id IS NULL AND batch_id IS NULL AND settle_response_code LIKE 'R%' AND settle_date >= CONVERT(DATETIME, dbo.CF_GetSetting('ForteSettlementsAsOf'))

		WHILE @tempId <= @maxId
			BEGIN
				SELECT @forte_trans_id = forte_trans_id, @transaction_id = transaction_id, @funding_id = funding_id, @amount = amount
				FROM #tempIDs 
				WHERE id = @tempId

				-- insert a new transaction for a fee and for it's detail
				DECLARE @newFeeTransId INT
				INSERT INTO transactions (trans_date, funding_id, trans_amount, trans_type_id, comments, batch_id, record_id, previous_id, redistributed, transaction_id, 
					change_user, change_date, portfolio)
				SELECT ft.received_date, f.id, @feeAmount, 9, 'Fee for ' + ft.status, @newBatchID, ft.forte_trans_id, NULL, 0, @transaction_id, 'system', GETUTCDATE(),
					'caymus'
				FROM forte_trans ft
				INNER JOIN fundings f ON ft.customer_id = f.contract_number
				LEFT JOIN payments p ON ft.transaction_id = p.transaction_id
				WHERE ft.forte_trans_id = @forte_trans_id	

				SELECT @newFeeTransId = SCOPE_IDENTITY()
				INSERT INTO transaction_details(trans_id, trans_type_id, trans_amount, comments)
				SELECT @newFeeTransId, 9, @feeAmount, 'Fee for declined' 

				-- now update the forte trans to make sure we don't do it again
				UPDATE forte_trans SET batch_id = @newBatchID WHERE forte_trans_id = @forte_trans_id

				-- create a retry for this failed transaction
				EXEC dbo.CSP_CreateRetryPayments @transaction_id, @funding_id, @amount	
	
				--move on to the next record
				SET @tempId = @tempId + 1
			END
			
		-- RKB - 2022-06-03, batches were remaining in New status
		-- now CSP_SettleBatches will settle these batches
		UPDATE batches SET batch_status = 'Processed' WHERE batch_id = @newBatchID
END

IF OBJECT_ID('tempdb..#tempIDs') IS NOT NULL DROP TABLE #tempIDs

END

GO
