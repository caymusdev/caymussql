SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-06-04
-- Description:	Gets a daily summary
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetDailySummary]
(
	@date		DATETIME = NULL
)
AS
BEGIN
SET NOCOUNT ON;
-- 2018-07-12 - Kimberly said she only wants to see the amount received for each batch and then will click the details to see the details.	
-- 2018-10-08 - Nina asked for this to be put back the way it was, to see everything on the main screen
IF @date IS NULL SET @date = CONVERT(DATE,  GETUTCDATE()-1)

/*
SELECT x.trans_type, SUM(x.trans_amount) AS trans_amount, x.batch_type, x.batch_id
FROM (
	SELECT tt.trans_type, t.trans_amount, 1 AS IsMain, b.batch_type, b.batch_id
	FROM batches b 
	INNER JOIN transactions t ON b.batch_id = t.batch_id	
	LEFT JOIN transaction_types tt ON t.trans_type_id = tt.trans_type_id
	WHERE CONVERT(DATE, b.settle_date) = @date AND b.batch_status IN ('Settled', 'Closed') AND t.redistributed = 0			
	UNION ALL
	SELECT tt.trans_type, d.trans_amount, 0 AS IsMain, b.batch_type, b.batch_id
	FROM batches b
	INNER JOIN transactions t ON b.batch_id = t.batch_id	
	INNER JOIN transaction_details d ON t.trans_id = d.trans_id
	LEFT JOIN transaction_types tt ON d.trans_type_id = tt.trans_type_id
	WHERE CONVERT(DATE, b.settle_date) = @date AND b.batch_status IN ('Settled', 'Closed') AND t.redistributed = 0
		AND d.trans_type_id NOT IN (8, 9, 11) -- exclude those that are also in main, to avoid duplicates
) x
GROUP BY x.batch_id, x.batch_type, x.trans_type, x.IsMain
ORDER BY x.batch_type, x.batch_id, x.IsMain DESC, x.trans_type
*/


-- RKB 2019-03-15 - To get sorting and no duplicates, had to use temp table and grouping
IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData
CREATE TABLE #tempData (id INT IDENTITY (1,1), trans_type VARCHAR (100), trans_amount MONEY, batch_type VARCHAR (100), batch_id INT)

INSERT INTO #tempData (trans_type, trans_amount, batch_type, batch_id)
SELECT x.trans_type, SUM(x.trans_amount) AS trans_amount, x.batch_type, x.batch_id
FROM (
	SELECT tt.trans_type, t.trans_amount, 1 AS IsMain, b.batch_type, b.batch_id
	FROM batches b 
	INNER JOIN transactions t ON b.batch_id = t.batch_id	
	LEFT JOIN transaction_types tt ON t.trans_type_id = tt.trans_type_id
	WHERE CONVERT(DATE, b.settle_date) = @date AND b.batch_status IN ('Settled', 'Closed') AND t.redistributed = 0			
	UNION ALL
	SELECT tt.trans_type, d.trans_amount, 0 AS IsMain, b.batch_type, b.batch_id
	FROM batches b
	INNER JOIN transactions t ON b.batch_id = t.batch_id	
	INNER JOIN transaction_details d ON t.trans_id = d.trans_id
	LEFT JOIN transaction_types tt ON d.trans_type_id = tt.trans_type_id
	WHERE CONVERT(DATE, b.settle_date) = @date AND b.batch_status IN ('Settled', 'Closed') AND t.redistributed = 0
		AND d.trans_type_id NOT IN (8, 9, 11) -- exclude those that are also in main, to avoid duplicates
) x
GROUP BY x.batch_id, x.batch_type, x.trans_type, x.IsMain
ORDER BY x.batch_type, x.batch_id, x.IsMain DESC, x.trans_type


SELECT DISTINCT batch_id, batch_type, trans_type, trans_amount, MIN(id) 
FROM #tempData 
GROUP BY batch_id, batch_type, trans_type, trans_amount
ORDER BY MIN(id)

IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData


/*
SELECT tt.trans_type, SUM(t.trans_amount) AS trans_amount, b.batch_type, b.batch_id
FROM batches b 
INNER JOIN transactions t ON b.batch_id = t.batch_id	
LEFT JOIN transaction_types tt ON t.trans_type_id = tt.trans_type_id
WHERE CONVERT(DATE, b.settle_date) = @date AND b.batch_status IN ('Settled', 'Closed') AND t.redistributed = 0		
	AND t.trans_type_id IN (2, 3, 4, 5)
GROUP BY b.batch_id, b.batch_type, tt.trans_type
ORDER BY b.batch_type, b.batch_id, tt.trans_type
*/
END
GO
