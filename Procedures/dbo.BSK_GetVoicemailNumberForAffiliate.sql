SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-03-23
-- Description:	returns the voicemail number for a given affiliate
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetVoicemailNumberForAffiliate]
(
	@affiliate_id			INT
)
AS
BEGIN
SET NOCOUNT ON;

SELECT TOP 1 x.users_phone_number
FROM (
	SELECT up.phone_number AS users_phone_number, CASE WHEN COALESCE(up.phone_type, '') = 'VoiceMail' THEN 2 ELSE 1 END AS phone_types
	FROM users u WITH (NOLOCK)
	INNER JOIN collector_cases cc ON u.user_id = cc.collector_user_id AND cc.end_date IS NULL
	INNER JOIN cases c ON cc.case_id = c.case_id
	INNER JOIN user_phones up ON u.user_id = up.user_id
	WHERE c.affiliate_id = @affiliate_id AND u.active = 1
	UNION ALL 
	SELECT '17702994888', 0
	--  SELECT '14703480606', 0
) x 
ORDER BY x.phone_types DESC


END

GO
