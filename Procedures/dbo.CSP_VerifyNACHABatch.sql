SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-02-11
-- Description:	After receiving an ACK file, verify that what we sent matches what they ACK
-- Will have to return the batch id since it is not provided in the ack file
-- =============================================
CREATE PROCEDURE CSP_VerifyNACHABatch
(
	@amount					MONEY,
	@batch_date				DATETIME,
	@batch_type				VARCHAR (50)
)
AS
BEGIN
SET NOCOUNT ON;

SELECT b.batch_amount, b.batch_date, b.batch_id, b.batch_status, b.batch_type, b.comments, b.deleted_flag, b.status_message
FROM batches b
WHERE b.batch_type = @batch_type AND b.batch_date = CONVERT(DATE, @batch_date) AND b.batch_amount = @amount


END
GO
