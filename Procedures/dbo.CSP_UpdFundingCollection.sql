SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-07-15
-- Description:	Updates the funding_collections table when a funding's collection_type changes
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdFundingCollection]
(	
	@funding_id				BIGINT,
	@collection_type		VARCHAR (50)--,
--	@assigned_collector		NVARCHAR (100)
)
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @yesterday DATE; SELECT @yesterday = DATEADD(DD, -1, @today)


-- need to check to see if anything is actually changing.  Calling SP does not care if it is changing or not.
DECLARE @current_collection_type VARCHAR (50), @assigned_collector NVARCHAR (100)
SELECT @current_collection_type = COALESCE(NULLIF(f.collection_type, ''), 'Active_contract') FROM fundings f WHERE f.id = @funding_id

IF LOWER(@collection_type) <> LOWER(@current_collection_type)
	BEGIN
		-- for now, hardcode the person
		IF LOWER(@collection_type) = 'customer_service' SET @assigned_collector = 'nicoles@caymusfunding.com'
		ELSE SET @assigned_collector = 'vaughnm@caymusfunding.com'
		
		DECLARE @tempUserID INT, @newCaseID INT, @department_id INT, @affiliate_id BIGINT
		
		SELECT @affiliate_id = affiliate_id FROM fundings WHERE id = @funding_id

		-- get a count of assigned cases for each collector that is a Customer Service
		SELECT TOP 1 @tempUserID = ud.user_id, @department_id = ud.department_id--, SUM(CASE WHEN c.case_id IS NULL THEN 0 ELSE 1 END) AS num_cases
		FROM user_departments ud
		INNER JOIN departments d ON ud.department_id = d.department_id
		LEFT JOIN collector_cases cc ON ud.user_id = cc.collector_user_id AND cc.start_date <= @today AND COALESCE(cc.end_date, '2050-05-09') >= @today
		LEFT JOIN cases c ON cc.case_id = c.case_id AND c.active = 1
		WHERE (d.department = 'CustomerService' AND LOWER(@collection_type) = 'customer_service')
			OR (d.department = 'Collections1' AND LOWER(@collection_type) != 'customer_service')
		GROUP BY ud.user_id, ud.department_id
		ORDER BY SUM(CASE WHEN c.case_id IS NULL THEN 0 ELSE 1 END) 
			
		SELECT @newCaseID = c.case_id
		FROM cases c
		INNER JOIN affiliates a ON c.affiliate_id = a.affiliate_id
		INNER JOIN fundings f ON a.affiliate_id = f.affiliate_id AND f.id = @funding_id
		WHERE c.active = 1

		UPDATE collector_cases
		SET end_date = @yesterday, change_date = GETUTCDATE(), change_user = 'System'
		WHERE case_id = @newCaseID AND end_date IS NULL

		INSERT INTO collector_cases (collector_user_id, case_id, start_date, end_date, create_date, change_date, change_user, create_user)
		VALUES (@tempUserID, @newCaseID, @today, NULL, GETUTCDATE(), GETUTCDATE(), 'System', 'System')

		UPDATE case_departments
		SET end_date = @yesterday, change_date = GETUTCDATE(), change_user = 'SystemUpdFundingCollection'
		WHERE case_id = @newCaseID AND end_date IS NULL

		INSERT INTO case_departments (case_id, department_id, start_date, end_date, create_date, change_date, change_user, create_user, do_not_move, do_not_move_by, next_move_on, next_move_to)
		VALUES (@newCaseID, @department_id, @today, NULL, GETUTCDATE(), GETUTCDATE(), 'SystemUpdFundingCollection', 'SystemUpdFundingCollection', NULL, NULL, NULL, NULL)

		-- create a notification for the new case
		DECLARE @affiliate_name NVARCHAR (100)
		SELECT @affiliate_name = 'New Case for ' + affiliate_name FROM affiliates WHERE affiliate_id = @affiliate_id

		EXEC dbo.BSK_InsNotification @tempUserID, NULL, @affiliate_id, NULL, NULL, @affiliate_name, 'NewCase', 'System', @newCaseID

			   
		-- old code
		-- Update any existing records that are different than the collection type passed in.  Set their end date.
		UPDATE funding_collections
		SET end_date = @yesterday, change_user = 'SYSTEM', change_date = GETUTCDATE()
		WHERE funding_id = @funding_id AND end_date IS NULL

		-- Now, insert the record with the new collection type
		INSERT INTO funding_collections (funding_id, collection_type, start_date, end_date, assigned_collector)
		VALUES (@funding_id, @collection_type, @today, NULL, @assigned_collector)

		UPDATE fundings 
		SET collection_type = @collection_type, change_user = 'SYSTEM', change_date = GETUTCDATE()
		WHERE id = @funding_id AND COALESCE(collection_type, '') <> @collection_type
	END
END


GO
