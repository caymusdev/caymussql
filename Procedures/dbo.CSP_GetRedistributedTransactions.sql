SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-09-04
-- Description:	Checks for redistributed transactions.  Should not likely happen so if it does, should investigate.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetRedistributedTransactions]
	
AS
BEGIN
SET NOCOUNT ON;

SELECT t.trans_id, t.funding_id, t.trans_date
FROM transactions t WITH (NOLOCK)
WHERE t.comments = 'Redistributed to older contract' AND t.trans_date >= '2019-09-05'
	AND DATEDIFF(DD, t.trans_date, GETUTCDATE()) < 7
ORDER BY t.trans_date DESC

END
GO
