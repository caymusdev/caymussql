SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-01-11
-- Description:	Gets ftp settings for a given connection
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetFTPSettings]
(
	@vendor			VARCHAR (50)
)
AS
BEGIN	
SET NOCOUNT ON;


IF @vendor = '53'
	BEGIN
		SELECT dbo.CF_GetSetting('53Server') AS HostServer, dbo.CF_GetSetting('53Port') AS HostPort, dbo.CF_GetSetting('53UserName') AS UserName, 
			dbo.CF_GetSetting('53Password') AS [Password], dbo.CF_GetSetting('53PassPhrase') AS PassPhrase, dbo.CF_GetSetting('53PrivateKey') AS PrivateKey
	END
ELSE IF @vendor = 'Collect'
	BEGIN
		SELECT dbo.CF_GetSetting('CollectFtpServer') AS HostServer, dbo.CF_GetSetting('CollectFtpPort') AS HostPort, dbo.CF_GetSetting('CollectFtpUserName') AS UserName, 
			dbo.CF_GetSetting('CollectFtpPassword') AS [Password], dbo.CF_GetSetting('CollectFtpPassPhrase') AS PassPhrase, dbo.CF_GetSetting('CollectFtpPrivateKey') AS PrivateKey
	END
ELSE IF @vendor = 'Regions'
	BEGIN
		SELECT dbo.CF_GetSetting('RegionsFtpServer') AS HostServer, dbo.CF_GetSetting('RegionsFtpPort') AS HostPort, dbo.CF_GetSetting('RegionsFtpUserName') AS UserName, 
			dbo.CF_GetSetting('RegionsFtpPassword') AS [Password], dbo.CF_GetSetting('RegionsFtpPassPhrase') AS PassPhrase, dbo.CF_GetSetting('RegionsFtpPrivateKey') AS PrivateKey
	END
ELSE IF @vendor = 'RegionsSPV'
	BEGIN
		SELECT dbo.CF_GetSetting('RegionsFtpServerSPV') AS HostServer, dbo.CF_GetSetting('RegionsFtpPortSPV') AS HostPort, dbo.CF_GetSetting('RegionsFtpUserNameSPV') AS UserName, 
			dbo.CF_GetSetting('RegionsFtpPasswordSPV') AS [Password], dbo.CF_GetSetting('RegionsFtpPassPhraseSPV') AS PassPhrase, dbo.CF_GetSetting('RegionsFtpPrivateKeySPV') AS PrivateKey
	END
END


GO
