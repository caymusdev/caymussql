SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2020-12-22
-- Description: Returns details for a payment (to be displayed in popup)
-- =============================================
CREATE PROCEDURE dbo.[BSK_GetPayment]
(
	@payment_id			INT
)
AS
BEGIN
SET NOCOUNT ON

SELECT p.scheduled_payment_id, p.payment_method, p.payment_date, p.payment_amount, ba.merchant_bank_account_id, p.payment_plan_id,
	CONVERT(VARCHAR (50), ba.merchant_bank_account_id) + '|' + ba.bank_name + '|' + ba.routing_number + '|' + ba.account_number AS selected_bank_account
FROM scheduled_payments p
LEFT JOIN merchant_bank_accounts ba ON p.merchant_bank_account_id = ba.merchant_bank_account_id
WHERE p.scheduled_payment_id = @payment_id

END

GO
