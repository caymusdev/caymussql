SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-04-25
-- Description:	Gets a record from the acct_files table
-- =============================================
CREATE PROCEDURE CSP_GetAcctFile
(
	@acct_file_id		INT
)
AS
BEGIN
SET NOCOUNT ON;

SELECT a.acct_file_id, a.acct_file_name, a.acct_folder, a.batch_ids, a.create_date, a.downloaded_last, a.userid
FROM acct_files a
WHERE a.acct_file_id = @acct_file_id

END
GO
