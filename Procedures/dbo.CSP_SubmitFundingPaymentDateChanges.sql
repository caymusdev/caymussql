SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-02-10
-- Description:	when the user submits their changes for approval
-- =============================================
CREATE PROCEDURE [dbo].[CSP_SubmitFundingPaymentDateChanges]
(	
--	@session_id					NVARCHAR (100), 
	@userid						NVARCHAR (100),
	@funding_id					BIGINT
)
AS
BEGIN	
SET NOCOUNT ON;


UPDATE funding_payment_dates_work
SET ready_for_approval = 1
WHERE userid = @userid AND funding_id = @funding_id AND changed = 1

END
GO
