SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-08-06
-- Description:	Fills in the fact tables for use in the cubes
-- =============================================
CREATE PROCEDURE [dbo].[CSP_InsFactTables]
	
AS
BEGIN
SET NOCOUNT ON;

DECLARE @today DATE; SET @today = DATEADD(HOUR, -5, GETUTCDATE())
DECLARE @endDate DATE; set @endDate = '9999-12-31'

-- need to store in a temp table the values up to today.
-- then compare to the last one for each funding.  if any field is different, insert a new record.

-- In case it was run earlier today, just delete them.  data may have been updated.
DELETE FROM fact_fundings WHERE start_date = @today
DELETE FROM fact_fundings_portfolio WHERE start_date = @today

IF OBJECT_ID('tempdb..#tempFacts') IS NOT NULL DROP TABLE #tempFacts
CREATE TABLE #tempFacts ([funding_id] [bigint] NOT NULL, [contract_status] [int] NULL, [payoff_amount] [money] NULL, [frequency] [nvarchar](50) NULL, [weekday_payment] [money] NULL,
	[weekly_payment] [money] NULL, [processor] [varchar](50) NULL, [paid_off_amount] [money] NULL, [last_payment_date] [datetime] NULL, [rtr_out] [money] NULL, [pp_out] [money] NULL,
	[margin_out] [money] NULL, [fees_out] [money] NULL, [percent_paid] [numeric](10, 2) NULL, [percent_performance] [numeric](10, 2) NULL, [on_hold] [bit] NULL, 
	[collection_type] [varchar](50) NULL, [margin_adjustment] [money] NULL, [start_date] [datetime] NOT NULL, [end_date] [datetime] NOT NULL, [float_amount] [money] NULL, 
	[payments_stopped_date] [datetime] NULL, [est_months_left] [numeric](10, 2) NULL, [performance_status_id] [int] NULL, [contract_substatus_id] [int] NULL, [ach_draft] [money] NULL,
	[refund_out] [money] NULL, [create_date] [datetime] NULL, [cash_to_pp] [money] NULL, [cash_to_margin] [money] NULL, [cash_fees] [money] NULL, [counter_deposit_revenue] [money] NULL,
	[ach_received] [money] NULL, [cc_revenue] [money] NULL, [wires_revenue] [money] NULL, [wires_revenue_no_renewal] [money] NULL, [discount_received] [money] NULL, [fees_applied] [money] NULL,
	[settlement_received] [money] NULL, [writeoff_received] [money] NULL, [cash_to_chargeback] [money] NULL, [cash_to_bad_debt] [money] NULL, [cash_to_reapplication] [money] NULL, 
	[cash_from_reapplication] [money] NULL, [writeoff_adjustment_received] [money] NULL, [checks_revenue] [money] NULL, [ach_reject] [money] NULL, [refund] [money] NULL, 
	[refund_processed] [money] NULL, [refund_reapplied] [money] NULL, [refund_returned] [money] NULL, [continuous_pull] [money] NULL, [cash_to_rtr] [money] NULL, [rtr_adjustment] [money] NULL,
	[rtr_applied] [money] NULL, [pp_adjustment] [money] NULL, [pp_applied] [money] NULL, [margin_applied] [money] NULL, [gross_revenue] [money] NULL, [applied_amount] [money] NULL,
	[gross_received] [money] NULL, [net_other_income] [money] NULL, [net_revenue] [money] NULL, [ach_revenue] [money] NULL, [total_fees_collected] [money] NULL, [gross_dollars_rec] [money] NULL,
	[last_trans_amount] [money] NULL, [total_portfolio_adjustment] [money] NULL, [estimated_final_payment_date] [date] NULL, [anticipated_final_payment_date] [date] NULL, 
	[last_draft_date] [datetime] NULL, [end_of_month] BIT NULL, pp_settlement MONEY, margin_settlement MONEY, [percent_complete] [numeric] (10, 2), variance_amount MONEY)

--insert into the temp table data up till today
INSERT INTO #tempFacts (funding_id, contract_status, payoff_amount, frequency, weekday_payment, weekly_payment, processor, paid_off_amount, last_payment_date, rtr_out, pp_out, margin_out, 
	fees_out, percent_paid, percent_performance, on_hold, collection_type, margin_adjustment, start_date, end_date, float_amount, payments_stopped_date, est_months_left, performance_status_id, 
	contract_substatus_id, ach_draft, refund_out, create_date, cash_to_pp, cash_to_margin, cash_fees, counter_deposit_revenue, ach_received, cc_revenue, wires_revenue, wires_revenue_no_renewal,
	discount_received, fees_applied, settlement_received, writeoff_received, cash_to_chargeback, cash_to_bad_debt, cash_to_reapplication, cash_from_reapplication, writeoff_adjustment_received, 
	checks_revenue, ach_reject, refund, refund_processed, refund_reapplied, refund_returned, continuous_pull, cash_to_rtr, rtr_adjustment, rtr_applied, pp_adjustment, pp_applied, 
	margin_applied, gross_revenue, applied_amount, gross_received, net_other_income, net_revenue, ach_revenue, total_fees_collected, gross_dollars_rec, last_trans_amount, 
	total_portfolio_adjustment, estimated_final_payment_date, anticipated_final_payment_date, last_draft_date, end_of_month, pp_settlement, margin_settlement, percent_complete, variance_amount)
SELECT id, contract_status, payoff_amount, frequency, weekday_payment, weekly_payment, processor, paid_off_amount, last_payment_date, rtr_out, pp_out, margin_out, 
	fees_out, percent_paid, percent_performance, on_hold, collection_type, margin_adjustment, @today, @endDate, float_amount, payments_stopped_date, est_months_left, performance_status_id, 
	contract_substatus_id, ach_draft, refund_out, GETUTCDATE(), cash_to_pp, cash_to_margin, cash_fees, counter_deposit_revenue, ach_received, cc_revenue, wires_revenue, wires_revenue_no_renewal,
	discount_received, fees_applied, settlement_received, writeoff_received, cash_to_chargeback, cash_to_bad_debt, cash_to_reapplication, cash_from_reapplication, writeoff_adjustment_received, 
	checks_revenue, ach_reject, refund, refund_processed, refund_reapplied, refund_returned, continuous_pull, cash_to_rtr, rtr_adjustment, rtr_applied, pp_adjustment, pp_applied, 
	margin_applied, gross_revenue, applied_amount, gross_received, net_other_income, net_revenue, ach_revenue, total_fees_collected, gross_dollars_rec, last_trans_amount, 
	total_portfolio_adjustment, estimated_final_payment_date, anticipated_final_payment_date, last_draft_date, 
	IIF(@today = EOMONTH(@today), 1, 0), pp_settlement, margin_settlement, percent_complete, variance_amount
FROM fundings f 

-- now the temp table stores all data to date.  need to compare each funding to the last entry in the fact_fundings and if any field is different
-- we need to insert a new record
INSERT INTO fact_fundings(funding_id, contract_status, payoff_amount, frequency, weekday_payment, weekly_payment, processor, paid_off_amount, last_payment_date, rtr_out, pp_out, margin_out, 
	fees_out, percent_paid, percent_performance, on_hold, collection_type, margin_adjustment, start_date, end_date, float_amount, payments_stopped_date, est_months_left, performance_status_id, 
	contract_substatus_id, ach_draft, refund_out, create_date, cash_to_pp, cash_to_margin, cash_fees, counter_deposit_revenue, ach_received, cc_revenue, wires_revenue, wires_revenue_no_renewal,
	discount_received, fees_applied, settlement_received, writeoff_received, cash_to_chargeback, cash_to_bad_debt, cash_to_reapplication, cash_from_reapplication, writeoff_adjustment_received, 
	checks_revenue, ach_reject, refund, refund_processed, refund_reapplied, refund_returned, continuous_pull, cash_to_rtr, rtr_adjustment, rtr_applied, pp_adjustment, pp_applied, 
	margin_applied, gross_revenue, applied_amount, gross_received, net_other_income, net_revenue, ach_revenue, total_fees_collected, gross_dollars_rec, last_trans_amount, 
	total_portfolio_adjustment, estimated_final_payment_date, anticipated_final_payment_date, last_draft_date, end_of_month, pp_settlement, margin_settlement, percent_complete, variance_amount) 
SELECT t.funding_id, t.contract_status, t.payoff_amount, t.frequency, t.weekday_payment, t.weekly_payment, t.processor, t.paid_off_amount, t.last_payment_date, t.rtr_out, t.pp_out, t.margin_out, 
	t.fees_out, t.percent_paid, t.percent_performance, t.on_hold, t.collection_type, t.margin_adjustment, @today, @endDate, t.float_amount, t.payments_stopped_date, t.est_months_left, 
	t.performance_status_id, t.contract_substatus_id, t.ach_draft, t.refund_out, t.create_date, t.cash_to_pp, t.cash_to_margin, t.cash_fees, t.counter_deposit_revenue, t.ach_received, 
	t.cc_revenue, t.wires_revenue, t.wires_revenue_no_renewal, t.discount_received, t.fees_applied, t.settlement_received, t.writeoff_received, t.cash_to_chargeback, t.cash_to_bad_debt, 
	t.cash_to_reapplication, t.cash_from_reapplication, t.writeoff_adjustment_received, t.checks_revenue, t.ach_reject, t.refund, t.refund_processed, t.refund_reapplied, t.refund_returned, 
	t.continuous_pull, t.cash_to_rtr, t.rtr_adjustment, t.rtr_applied, t.pp_adjustment, t.pp_applied, t.margin_applied, t.gross_revenue, t.applied_amount, t.gross_received, t.net_other_income, 
	t.net_revenue, t.ach_revenue, t.total_fees_collected, t.gross_dollars_rec, t.last_trans_amount, t.total_portfolio_adjustment, t.estimated_final_payment_date, 
	t.anticipated_final_payment_date, t.last_draft_date, t.end_of_month, t.pp_settlement, t.margin_settlement, t.percent_complete, t.variance_amount
FROM #tempFacts t
LEFT JOIN fact_fundings f ON t.funding_id = f.funding_id AND f.end_date = @endDate
WHERE f.funding_id IS NULL
	OR (
		t.funding_id = f.funding_id 
		AND (			   
			   t.contract_status <> f.contract_status
			OR COALESCE(t.payoff_amount, 0) <> COALESCE(f.payoff_amount, 0)
			OR t.frequency <> f.frequency
			OR COALESCE(t.weekday_payment, 0) <> COALESCE(f.weekday_payment, 0)
			OR COALESCE(t.weekly_payment, 0) <> COALESCE(f.weekly_payment, 0)
			OR COALESCE(t.processor, '') <> COALESCE( f.processor, '')
			OR COALESCE(t.paid_off_amount, 0) <> COALESCE(f.paid_off_amount, 0)
			OR COALESCE(t.last_payment_date, '1900-01-01') <> COALESCE(f.last_payment_date, '1900-01-01')
			OR COALESCE(t.rtr_out, 0) <> COALESCE(f.rtr_out, 0)
			OR COALESCE(t.pp_out, 0) <> COALESCE(f.pp_out, 0)
			OR COALESCE(t.margin_out, 0) <> COALESCE(f.margin_out, 0)
			OR COALESCE(t.fees_out, 0) <> COALESCE(f.fees_out, 0)
			OR COALESCE(t.percent_paid, 0) <> COALESCE(f.percent_paid, 0)
			OR COALESCE(t.percent_complete, 0) <> COALESCE(f.percent_complete, 0)
			OR COALESCE(t.percent_performance, 0) <> COALESCE(f.percent_performance, 0)
			OR COALESCE(t.on_hold, 0) <> COALESCE(f.on_hold, 0)
			OR COALESCE(t.collection_type, '') <> COALESCE(f.collection_type, '')
			OR COALESCE(t.margin_adjustment, 0) <> COALESCE(f.margin_adjustment, 0)
			OR COALESCE(t.float_amount, 0) <> COALESCE(f.float_amount, 0)
			OR COALESCE(t.payments_stopped_date, '1900-01-01') <> COALESCE(f.payments_stopped_date, '1900-01-01')
			OR COALESCE(t.est_months_left, 0) <> COALESCE(f.est_months_left, 0)			
			OR COALESCE(t.performance_status_id, -1) <> COALESCE(f.performance_status_id, -1)
			OR COALESCE(t.contract_substatus_id, -1) <> COALESCE(f.contract_substatus_id, -1)
			OR COALESCE(t.ach_draft, 0) <> COALESCE(f.ach_draft, 0)						
			OR COALESCE(t.cash_to_pp, 0) <> COALESCE(f.cash_to_pp, 0)
			OR COALESCE(t.cash_to_margin, 0) <> COALESCE(f.cash_to_margin, 0)
			OR COALESCE(t.cash_fees, 0) <> COALESCE(f.cash_fees, 0)
			OR COALESCE(t.counter_deposit_revenue, 0) <> COALESCE(f.counter_deposit_revenue, 0)
			OR COALESCE(t.ach_received, 0) <> COALESCE(f.ach_received, 0)
			OR COALESCE(t.cc_revenue, 0) <> COALESCE(f.cc_revenue, 0)
			OR COALESCE(t.wires_revenue, 0) <> COALESCE(f.wires_revenue, 0)
			OR COALESCE(t.wires_revenue_no_renewal, 0) <> COALESCE(f.wires_revenue_no_renewal, 0)
			OR COALESCE(t.discount_received, 0) <> COALESCE(f.discount_received, 0)
			OR COALESCE(t.fees_applied, 0) <> COALESCE(f.fees_applied, 0)
			OR COALESCE(t.settlement_received, 0) <> COALESCE(f.settlement_received, 0)
			OR COALESCE(t.writeoff_received, 0) <> COALESCE(f.writeoff_received, 0)
			OR COALESCE(t.cash_to_chargeback, 0) <> COALESCE(f.cash_to_chargeback, 0)
			OR COALESCE(t.cash_to_bad_debt, 0) <> COALESCE(f.cash_to_bad_debt, 0)
			OR COALESCE(t.cash_to_reapplication, 0) <> COALESCE(f.cash_to_reapplication, 0)
			OR COALESCE(t.cash_from_reapplication, 0) <> COALESCE(f.cash_from_reapplication, 0)
			OR COALESCE(t.writeoff_adjustment_received, 0) <> COALESCE(f.writeoff_adjustment_received, 0)
			OR COALESCE(t.checks_revenue, 0) <> COALESCE(f.checks_revenue, 0)
			OR COALESCE(t.ach_reject, 0) <> COALESCE(f.ach_reject, 0)
			OR COALESCE(t.refund, 0) <> COALESCE(f.refund, 0)
			OR COALESCE(t.refund_processed, 0) <> COALESCE(f.refund_processed, 0)
			OR COALESCE(t.refund_reapplied, 0) <> COALESCE(f.refund_reapplied, 0)
			OR COALESCE(t.refund_returned, 0) <> COALESCE(f.refund_returned, 0)
			OR COALESCE(t.continuous_pull, 0) <> COALESCE(f.continuous_pull, 0)
			OR COALESCE(t.cash_to_rtr, 0) <> COALESCE(f.cash_to_rtr, 0)
			OR COALESCE(t.rtr_adjustment, 0) <> COALESCE(f.rtr_adjustment, 0)
			OR COALESCE(t.rtr_applied, 0) <> COALESCE(f.rtr_applied, 0)
			OR COALESCE(t.pp_adjustment, 0) <> COALESCE(f.pp_adjustment, 0)
			OR COALESCE(t.pp_applied, 0) <> COALESCE(f.pp_applied, 0)
			OR COALESCE(t.margin_applied, 0) <> COALESCE(f.margin_applied, 0)
			OR COALESCE(t.gross_revenue, 0) <> COALESCE(f.gross_revenue, 0)
			OR COALESCE(t.applied_amount, 0) <> COALESCE(f.applied_amount, 0)
			OR COALESCE(t.gross_received, 0) <> COALESCE(f.gross_received, 0)
			OR COALESCE(t.net_other_income, 0) <> COALESCE(f.net_other_income, 0)
			OR COALESCE(t.ach_revenue, 0) <> COALESCE(f.ach_revenue, 0)
			OR COALESCE(t.total_fees_collected, 0) <> COALESCE(f.total_fees_collected, 0)
			OR COALESCE(t.gross_dollars_rec, 0) <> COALESCE(f.gross_dollars_rec, 0)
			OR COALESCE(t.last_trans_amount, 0) <> COALESCE(f.last_trans_amount, 0)
			OR COALESCE(t.total_portfolio_adjustment, 0) <> COALESCE(f.total_portfolio_adjustment, 0)
			OR COALESCE(t.estimated_final_payment_date, '1900-01-01') <> COALESCE(f.estimated_final_payment_date, '1900-01-01')
			OR COALESCE(t.anticipated_final_payment_date, '1900-01-01') <> COALESCE(f.anticipated_final_payment_date, '1900-01-01')
			OR COALESCE(t.last_draft_date, '1900-01-01') <> COALESCE(f.last_draft_date, '1900-01-01')
			OR COALESCE(t.pp_settlement, 0) <> COALESCE(f.pp_settlement, 0)
			OR COALESCE(t.margin_settlement, 0) <> COALESCE(f.margin_settlement, 0)
			OR COALESCE(t.variance_amount, 0) <> COALESCE(f.variance_amount, 0)	
		)
	)


-- now need to update any records that had @endDate for their end date and are no longer the end date (meaning their start date will be less)
-- get the latest start date for each funding that has end_date = @endDate

UPDATE f
SET end_date = DATEADD(DAY, -1, @today)
FROM fact_fundings f
INNER JOIN (
	SELECT f.funding_id, MAX(f.start_date) AS max_start_date
	FROM fact_fundings f
	GROUP BY f.funding_id
) x ON f.funding_id = x.funding_id AND f.start_date <> x.max_start_date AND f.end_date = @endDate


--- **************************************************
--- **************************************************
--- **************************************************
-- Now do the entire porfolio summary


INSERT INTO fact_fundings_portfolio(payoff_amount, paid_off_amount, rtr_out, pp_out, margin_out, fees_out, percent_paid, percent_performance, margin_adjustment, start_date, end_date, float_amount,
	ach_draft, refund_out, create_date, cash_to_pp, cash_to_margin, cash_fees, counter_deposit_revenue, ach_received, cc_revenue, wires_revenue, wires_revenue_no_renewal,
	discount_received, fees_applied, settlement_received, writeoff_received, cash_to_chargeback, cash_to_bad_debt, cash_to_reapplication, cash_from_reapplication, writeoff_adjustment_received, 
	checks_revenue, ach_reject, refund, refund_processed, refund_reapplied, refund_returned, continuous_pull, cash_to_rtr, rtr_adjustment, rtr_applied, pp_adjustment, pp_applied, 
	margin_applied, gross_revenue, applied_amount, gross_received, net_other_income, net_revenue, ach_revenue, total_fees_collected, gross_dollars_rec, total_portfolio_adjustment, end_of_month, 
	pp_settlement, margin_settlement, variance_amount)
SELECT SUM(f.payoff_amount), SUM(f.paid_off_amount), SUM(f.rtr_out), SUM(f.pp_out), SUM(f.margin_out), SUM(f.fees_out), 
	SUM(COALESCE(f.cash_to_rtr, 0)) / SUM(f.portfolio_value) * 100.00, 
	SUM((estimated_turn - COALESCE(actual_turn, 0))) / SUM(estimated_turn) * 100.00, 
	(SELECT SUM(t.margin_adjustment) FROM #tempFacts t), @today, @endDate,
	(SELECT SUM(t.float_amount) FROM #tempFacts t),
	(SELECT SUM(t.ach_draft) FROM #tempFacts t),
	(SELECT SUM(t.refund_out) FROM #tempFacts t), GETUTCDATE(), 
	(SELECT SUM(t.cash_to_pp) FROM #tempFacts t),
	(SELECT SUM(t.cash_to_margin) FROM #tempFacts t),
	(SELECT SUM(t.cash_fees) FROM #tempFacts t),
	(SELECT SUM(t.counter_deposit_revenue) FROM #tempFacts t),
	(SELECT SUM(t.ach_received) FROM #tempFacts t),
	(SELECT SUM(t.cc_revenue) FROM #tempFacts t),
	(SELECT SUM(t.wires_revenue) FROM #tempFacts t),
	(SELECT SUM(t.wires_revenue_no_renewal) FROM #tempFacts t),
	(SELECT SUM(t.discount_received) FROM #tempFacts t),
	(SELECT SUM(t.fees_applied) FROM #tempFacts t),
	(SELECT SUM(t.settlement_received) FROM #tempFacts t),
	(SELECT SUM(t.writeoff_received) FROM #tempFacts t),
	(SELECT SUM(t.cash_to_chargeback) FROM #tempFacts t),
	(SELECT SUM(t.cash_to_bad_debt) FROM #tempFacts t),
	(SELECT SUM(t.cash_to_reapplication) FROM #tempFacts t),
	(SELECT SUM(t.cash_from_reapplication) FROM #tempFacts t),
	(SELECT SUM(t.writeoff_adjustment_received) FROM #tempFacts t),
	(SELECT SUM(t.checks_revenue) FROM #tempFacts t),
	(SELECT SUM(t.ach_reject) FROM #tempFacts t),
	(SELECT SUM(t.refund) FROM #tempFacts t),
	(SELECT SUM(t.refund_processed) FROM #tempFacts t),
	(SELECT SUM(t.refund_reapplied) FROM #tempFacts t),
	(SELECT SUM(t.refund_returned) FROM #tempFacts t),
	(SELECT SUM(t.continuous_pull) FROM #tempFacts t),
	(SELECT SUM(t.cash_to_rtr) FROM #tempFacts t),
	(SELECT SUM(t.rtr_adjustment) FROM #tempFacts t),
	(SELECT SUM(t.rtr_applied) FROM #tempFacts t),
	(SELECT SUM(t.pp_adjustment) FROM #tempFacts t),
	(SELECT SUM(t.pp_applied) FROM #tempFacts t),
	(SELECT SUM(t.margin_applied) FROM #tempFacts t),
	(SELECT SUM(t.gross_revenue) FROM #tempFacts t),
	(SELECT SUM(t.applied_amount) FROM #tempFacts t),
	(SELECT SUM(t.gross_received) FROM #tempFacts t),
	(SELECT SUM(t.net_other_income) FROM #tempFacts t),
	(SELECT SUM(t.net_revenue) FROM #tempFacts t),
	(SELECT SUM(t.ach_revenue) FROM #tempFacts t),
	(SELECT SUM(t.total_fees_collected) FROM #tempFacts t),
	(SELECT SUM(t.gross_dollars_rec) FROM #tempFacts t),
	(SELECT SUM(t.total_portfolio_adjustment) FROM #tempFacts t),
	IIF(@today = EOMONTH(@today), 1, 0), 
	(SELECT SUM(t.pp_settlement) FROM #tempFacts t),
	(SELECT SUM(t.margin_settlement) FROM #tempFacts t),
	(SELECT SUM(t.variance_amount) FROM #tempFacts t)
FROM fundings f WITH (NOLOCK)




IF OBJECT_ID('tempdb..#tempFacts') IS NOT NULL DROP TABLE #tempFacts

END
GO
