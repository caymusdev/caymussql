SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-01-16
-- Description:	Takes a payment id and settles/distributes it (for NACHA)
-- =============================================
CREATE PROCEDURE [dbo].[CSP_ProcessNACHAPayment]
(
	@payment_id			INT,
	@trans_amount		MONEY,
	@funding_id			BIGINT,
	@company_name		VARCHAR (100),	
	@settle_date		DATE,
	@batch_id			INT,
	@response_code		VARCHAR (50),
	@settle_type		VARCHAR (50),
	@portfolio			VARCHAR (50) = 'caymus'
)

AS
BEGIN
SET NOCOUNT ON;


DECLARE @record_id INT, @trans_date DATE, @is_refund BIT
DECLARE @comments NVARCHAR (500)
DECLARE @transaction_id VARCHAR (100); SET @transaction_id = NULL
DECLARE @today DATE, @newBatchID INT; SET @today = DATEADD(HOUR, -5, GETUTCDATE())

UPDATE payments SET transaction_id = @payment_id, change_user = 'SYSTEM', change_date = GETUTCDATE() 
WHERE payment_id = @payment_id AND transaction_id IS NULL

IF @settle_type NOT IN ('deposit')
	BEGIN
		SET @trans_amount = -1.00 * @trans_amount
	END


SELECT	@comments = @company_name + ' - ' + CONVERT(VARCHAR(50), @funding_id) + ' ' + @response_code + ' : ' + @settle_type, 
	@record_id = @payment_id, @trans_date = p.trans_date, @transaction_id = p.transaction_id, @is_refund = p.is_refund
FROM payments p
WHERE p.payment_id = @payment_id

DECLARE @previous_id INT; SET @previous_id = NULL
DECLARE @trans_type_id INT; SET @trans_type_id = NULL

IF COALESCE(@is_refund, 0) = 1
	BEGIN
		SET @trans_type_id = 25
	END


-- RKB - 2020-02-03 - Always want to use today's date as the trans date, not the payment date because the payment date could have been sitting in the queue for a while
SET @trans_date = @today
-- RKB - 2020-03-04
SET @trans_date = @settle_date -- this comes in as the batch date (effective date) from fifth third so we should actually use this one
-- @today caused issue running on saturday morning

EXEC dbo.CSP_DistributeTransaction_v2 @funding_id, @trans_amount, @transaction_id, @trans_date, @comments, @batch_id, @record_id, 'nacha', 
	NULL, @settle_date, @settle_type, @response_code, @trans_type_id, @portfolio

END

GO
