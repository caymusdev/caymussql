SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Rick Pina
-- Create Date: 2020-09-01
-- Description: Creates a new User
-- =============================================
CREATE PROCEDURE dbo.BSK_InsUser_Custom
(
	@email			NVARCHAR (100),
	@hash_password	NVARCHAR (200),
	@salt			NVARCHAR (100),
	@timezone		NVARCHAR (100),
	@user			NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON

-- validate email and password
	
	INSERT INTO users (email, hash_password, salt, timezone, create_date, change_date, change_user, create_user)
	Values (@email, @hash_password, @salt, @timezone, GETUTCDATE(), GETUTCDATE(), @user, @user)



END
GO
