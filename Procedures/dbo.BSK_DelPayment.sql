SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2020-12-22
-- Description: Marks a payment as deleted
-- =============================================
CREATE PROCEDURE dbo.[BSK_DelPayment]
(
	@payment_id			INT,
	@users_email		NVARCHAR (100),
	@change_reason_id	INT,
	@comments			NVARCHAR (2000)
)
AS
BEGIN
SET NOCOUNT ON

DECLARE @orig_amount MONEY, @orig_start_date DATE, @description NVARCHAR (2000), @payment_method VARCHAR (50), @affiliate_id BIGINT

-- First need to check and see if this user needs approval for editing/creating payment schedules
DECLARE @approved BIT; SET @approved = 1; SET @description = ''

IF EXISTS (SELECT 1 FROM sec_payment_schedules s WHERE s.submitter_user_id = @users_email)
	BEGIN
		SET @approved = 0
	END


UPDATE scheduled_payments
SET deleted = 1, change_date = GETUTCDATE(), change_user = @users_email, comments = @comments, change_reason_id = @change_reason_id, 
	changed = 1, approved_date = NULL, approved_user = NULL, exported = NULL, rejected_user = NULL, rejected_date = NULL
WHERE scheduled_payment_id = @payment_id

IF @approved = 0
	BEGIN
		SELECT @orig_amount = p.payment_amount, @orig_start_date = p.payment_date, @change_reason_id = p.change_reason_id, @comments = p.comments, 
			@payment_method = p.payment_method, @affiliate_id = p.affiliate_id
		FROM scheduled_payments p
		WHERE p.scheduled_payment_id = @payment_id

		SET @description = 'Cancelled ' + @payment_method + ' payment on ' + CONVERT(VARCHAR(10), @orig_start_date, 126) + ' for ' + 
			CONVERT(VARCHAR, CAST(@orig_amount AS MONEY), 1) + '.'

		IF @change_reason_id IS NOT NULL
			BEGIN
				SELECT @description = @description + ' The reason for the change is ' + reason + '.'
				FROM payment_schedule_change_reasons
				WHERE id = @change_reason_id
			END

		IF COALESCE(@comments, '') != ''
			BEGIN
				SET @description = @description + @comments + '.'
			END

		INSERT INTO approvals (approval_type_id, description, comments, record_id, new_value, create_date, change_date, change_user, create_user, approved_by,
			approved_date, rejected_by, rejected_date, affiliate_id)
		SELECT t.approval_type_id, LTRIM(@description), NULL, @payment_id, NULL, GETUTCDATE(), GETUTCDATE(), @users_email, @users_email, NULL, NULL, NULL, NULL, 
			@affiliate_id
		FROM approval_types t 
		WHERE t.approval_type = 'PaymentDelete'
	END
ELSE  -- @approved = 1
	BEGIN
		EXEC dbo.BSK_ApproveSchedPayment @payment_id, @users_email
	END

END

GO
