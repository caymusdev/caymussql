SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-02-01
-- Description:	Updates the the merchant bank account id on a payment schedule
-- =============================================
CREATE PROCEDURE CSP_SyncMerchantBankAccountsAndSchedules
(
	@merchant_bank_account_id		INT = NULL,
	@payment_schedule_id			INT = NULL
)
AS
BEGIN
SET NOCOUNT ON;

-- 1. First, syncs up the merchant_bank_account_id on the payment schedules table.
-- 2. If the merchant bank account id is passed in, then assume that IT was changed and so we need to update the payment schedules.
-- 3. If the payment schedule id was passed in, then assume the payment schedule was changed and we need to update the bank account on the merchant

UPDATE ps
SET merchant_bank_account_id = ba.merchant_bank_account_id
FROM payment_schedules ps
INNER JOIN funding_payment_schedules fps ON ps.payment_schedule_id = fps.payment_schedule_id
INNER JOIN fundings f ON fps.funding_id = f.id
INNER JOIN merchant_bank_accounts ba ON ps.bank_name = ba.bank_name AND ps.routing_number = ba.routing_number AND ps.account_number = ba.account_number
	AND f.merchant_id = ba.merchant_id
WHERE ps.merchant_bank_account_id IS NULL

IF @merchant_bank_account_id IS NOT NULL
	BEGIN
		UPDATE ps
		SET ps.bank_name = ba.bank_name, ps.routing_number = ba.routing_number, ps.account_number = ba.account_number
		FROM merchant_bank_accounts ba
		INNER JOIN payment_schedules ps ON ba.merchant_bank_account_id = ps.merchant_bank_account_id
		WHERE ba.merchant_bank_account_id = @merchant_bank_account_id
	END
ELSE IF @payment_schedule_id IS NOT NULL
	BEGIN
		UPDATE ba
		SET ba.bank_name = ps.bank_name, ba.routing_number = ps.routing_number, ba.account_number = ps.account_number
		FROM payment_schedules ps 
		INNER JOIN merchant_bank_accounts ba ON ps.merchant_bank_account_id = ba.merchant_bank_account_id
		WHERE ps.payment_schedule_id = @payment_schedule_id
	END

END
GO
