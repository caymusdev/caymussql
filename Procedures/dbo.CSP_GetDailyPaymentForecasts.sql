SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-07-19
-- Description:	Gets data from payment_forecasts grouped by day for the calendar
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetDailyPaymentForecasts]
(
	@start_date			DATE,
	@end_date			DATE
)
AS
BEGIN
SET NOCOUNT ON;

SELECT payment_date, SUM(COALESCE(amount, 0)) AS amount, SUM(COALESCE(rec_amount, 0)) AS rec_amount,
	CONVERT(VARCHAR(50), CONVERT(NUMERIC(10,2),SUM(COALESCE(rec_amount, 0))/ SUM(COALESCE(amount, 0)) * 100.00)) + '%' AS percent_rec
FROM payment_forecasts
WHERE payment_date BETWEEN @start_date AND @end_date
GROUP BY payment_date
ORDER BY payment_date

END
GO
