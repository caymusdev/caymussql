SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-01-19
-- Description:	Gets the various settings for the management main tab
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetManagementSettings]

AS
BEGIN
SET NOCOUNT ON;

SELECT dbo.BF_GetSetting('LeftVMFollowup') AS LeftVMFollowup

SELECT d.department_id, d.department, d.department_desc, d.days_before_moving
FROM departments d
WHERE d.department IN ('Collections2', 'Collections1', 'CustomerService')

/*
Initial sort and filter effort is way too much for the value that they'll get.  Not doing for now.

SELECT *
FROM settings s
WHERE s.setting LIKE 'CasesInitialSort%'
*/

END

GO
