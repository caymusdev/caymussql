SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-27
-- Description:	updates a notification record specifically for a case
-- =============================================
CREATE PROCEDURE [dbo].[BSK_UpdCaseNotification]
(
	@case_id					INT,
	@user_email					NVARCHAR (100),
	@notification_type			VARCHAR (50)
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @now DATETIME; SET @now = GETUTCDATE()


IF @notification_type = 'NewCase'
	BEGIN
		UPDATE cases SET new_case = 0, change_user = @user_email, change_date = GETUTCDATE() 
		WHERE case_id = @case_id AND new_case = 1

		DECLARE @notification_id INT, @affiliate_id INT
		SELECT @affiliate_id = affiliate_id FROM cases WHERE case_id = @case_id

		SELECT @notification_id = notification_id 
		FROM notifications n
		INNER JOIN notification_types t ON n.notification_type_id = t.notification_type_id
		WHERE t.notification_type = @notification_type AND acted_on_time IS NULL AND affiliate_id = @affiliate_id

		IF @notification_id IS NOT NULL
			BEGIN
				EXEC dbo.BSK_UpdNotification @notification_id, @user_email, NULL, @now
			END
	END
ELSE IF @notification_type = 'MissedPayment'
	BEGIN
		UPDATE cases SET missed_payment = 0, change_user = @user_email, change_date = GETUTCDATE() 
		WHERE case_id = @case_id AND missed_payment = 1		
	END
ELSE IF @notification_type = 'NewCommunication'
	BEGIN
		UPDATE cases SET new_communication = 0, change_user = @user_email, change_date = GETUTCDATE() 
		WHERE case_id = @case_id AND new_communication = 1		
	END

END

GO
