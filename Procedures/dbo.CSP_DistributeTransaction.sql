SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-06-08
-- Description:	Contains the logic for distributing a transaction
-- NOTE: Called from transactionsController.cs 
-- =============================================
CREATE PROCEDURE [dbo].[CSP_DistributeTransaction]
(
	@trans_date				DATETIME,
	@funding_id				BIGINT,
	@trans_amount			MONEY,
	@comments				NVARCHAR (500),
	@batch_id				INT,
	@record_id				INT,
	@response_code			NVARCHAR (50),
	@settle_type			NVARCHAR (50),
	@previous_id			INT = NULL,
	@trans_type_id			INT = NULL
)
AS
BEGIN
SET NOCOUNT ON;

-- first create the transaction and store off the new trans_id
DECLARE @trans_id INT
DECLARE @mainTransType INT
SET @mainTransType = CASE @settle_type WHEN 'WIRE' THEN 5 WHEN 'CC' THEN 4 
	ELSE CASE COALESCE(@trans_type_id, -1) WHEN -1 THEN 3 ELSE @trans_type_id END END

INSERT INTO transactions (trans_date, funding_id, trans_amount, trans_type_id, comments, batch_id, record_id, previous_id, redistributed)
SELECT @trans_date, @funding_id, @trans_amount, CASE WHEN @trans_amount < 0 AND @trans_type_id IS NULL THEN 14 ELSE @mainTransType END, @comments, @batch_id, @record_id, @previous_id, 0

SELECT @trans_id = SCOPE_IDENTITY()

DECLARE @fees MONEY, @remaining_amount MONEY, @new_trans_amount MONEY
DECLARE @feeAmount MONEY
SELECT @feeAmount = COALESCE(dbo.CF_GetSetting('FeeAmount'), 72)
-- SELECT @feeAmount

DECLARE @contract_status INT, @pricing_ratio NUMERIC(10, 4)
SELECT @contract_status = contract_status, @pricing_ratio = pricing_ratio 
FROM fundings 
WHERE id = @funding_id

IF @trans_type_id IN (8, 9, 11)
	BEGIN
		INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments)
		VALUES (@trans_id, @trans_type_id, @trans_amount, @comments)

	-- if the trans type is writeoff then also writeoff the contract
		IF @trans_type_id = 11
			BEGIN
				UPDATE fundings SET contract_status = 4 WHERE id = @funding_id
			END
	END
ELSE
	BEGIN
		-- first create a fee if there is one
		IF @response_code LIKE 'R%'
			BEGIN
				-- insert a new transaction for a fee and for it's detail
				DECLARE @newFeeTransId INT
				INSERT INTO transactions (trans_date, funding_id, trans_amount, trans_type_id, comments, batch_id, record_id, previous_id, redistributed)
				SELECT @trans_date, @funding_id, @feeAmount, 9, 'Fee for ' + @response_code + ' : ' + @settle_type, @batch_id, @record_id, @previous_id, 0
	
				SELECT @newFeeTransId = SCOPE_IDENTITY()
				INSERT INTO transaction_details(trans_id, trans_type_id, trans_amount, comments)
				SELECT @newFeeTransId, 9, @feeAmount, 'Fee for ' + @response_code + ' : ' + @settle_type
			END

		IF @contract_status IN (1, 4)
			BEGIN
				-- need to see if there are fees
				SET @remaining_amount = @trans_amount

				SELECT @fees = (
					SELECT SUM(CASE d.trans_type_id WHEN 1 THEN d.trans_amount WHEN 9 THEN -1 * d.trans_amount END) 
					FROM transaction_details d
					INNER JOIN transactions t ON d.trans_id = t.trans_id 
					WHERE t.funding_id = @funding_id AND d.trans_type_id IN (1, 9)
					)

				IF @fees < 0 AND @trans_amount > 0
				-- this means they still have fees.  Fees get put in negative and paid fees positive so if there is still negative fee balance, it means they owe
					BEGIN
						IF @trans_amount >= ABS(@fees)
							BEGIN
								SET @new_trans_amount = ABS(@fees)
								SET @remaining_amount = @trans_amount - ABS(@fees)
							END
						ELSE -- @settle_amount < ABS(@fees)
							BEGIN
								SET @new_trans_amount = @trans_amount
								SET @remaining_amount = 0
							END
						-- now insert the fee payment
						INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments)
						VALUES (@trans_id, 1, @new_trans_amount, '')
					END

				IF @remaining_amount <> 0 AND @contract_status = 1
					BEGIN
						-- apply to purchase price and margin
						-- do purchase price first
						SET @new_trans_amount = ROUND(@remaining_amount / @pricing_ratio, 2)
						INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments)
						VALUES (@trans_id, 7, @new_trans_amount, '')

						-- now do margin
						INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments)
						VALUES (@trans_id, 6, @remaining_amount - @new_trans_amount, '')
					END
				ELSE IF @remaining_amount <> 0 AND @contract_status = 4
					BEGIN
					INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments)
						VALUES (@trans_id, 15, @remaining_amount, '')
					END
			END
			-- 2018-07-13 - Kimberly: Writeoff still needs to apply fees and then remainder goes to bad debt recovery.
		--ELSE IF @contract_status = 4
			--BEGIN
	--			-- if contract status is 4 (writeoff) all money goes to Bad Debt Recovery
--				IF @trans_amount <> 0
		--			BEGIN
			--			INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments)
				--		VALUES (@trans_id, 15, @trans_amount, '')
				--	END
	--		END
	END

-- if we get certain reject codes we need to set contract status to On Hold (5)
IF @response_code LIKE 'R%'
	BEGIN
	/* RJB - 2018-10-31 - Happy Halloween - I don't believe this old code is actually correct. 
		DECLARE @MaxConsecutiveR01s INT; SELECT @MaxConsecutiveR01s = dbo.CF_GetSetting('MaxConsecutiveR01s')
		-- need to make sure they have at least @MaxConsecutiveR01s settlements to begin with
		DECLARE @transCount INT
		SELECT @transCount = COUNT(*) 
		FROM forte_settlements s
		INNER JOIN fundings f ON s.customer_id = f.contract_number
		WHERE f.id = @funding_id

		-- if all previous @MaxConsecutiveR01s are NOT R01 or the response code is not R01
		-- then set the contract to On Hold (5)
		IF (NOT EXISTS 
			(SELECT 1 FROM 
				(
					SELECT TOP (@MaxConsecutiveR01s) settle_response_code
					FROM forte_settlements s
					INNER JOIN fundings f ON s.customer_id = f.contract_number
					WHERE f.id = @funding_id
					ORDER BY settle_date DESC
				) x
				WHERE x.settle_response_code <> 'R01'
			) AND @transCount >= @MaxConsecutiveR01s) OR @response_code <> 'R01'
			BEGIN				
				UPDATE fundings SET on_hold = 1, payments_stopped_date = GETUTCDATE() WHERE id = @funding_id 
			END

	*/
		-- If not R01 then put on hold
		IF @response_code <> 'R01'
			BEGIN				
				UPDATE fundings SET on_hold = 1, payments_stopped_date = GETUTCDATE() WHERE id = @funding_id 
			END 

		-- need to make sure they have at least X number of trans 
		DECLARE @transCount INT
		SELECT @transCount = COUNT(*) 
		FROM forte_settlements s
		INNER JOIN fundings f ON s.customer_id = f.contract_number
		WHERE f.id = @funding_id

		DECLARE @MissedDailyCS INT, @MissedWeeklyCS INT, @MissedDailyCollections INT, @MissedWeeklyCollections INT
		
		SELECT @MissedDailyCS = dbo.CF_GetSetting('MissedDailyCS'), @MissedWeeklyCS = dbo.CF_GetSetting('MissedWeeklyCS'), 
			@MissedDailyCollections = dbo.CF_GetSetting('MissedDailyCollections'), @MissedWeeklyCollections = dbo.CF_GetSetting('MissedWeeklyCollections')

		DECLARE @frequency VARCHAR (50), @collection_type VARCHAR (50); 
		SELECT @frequency = frequency, @collection_type = COALESCE(NULLIF(collection_type, ''), 'Active_contract') FROM fundings WHERE id = @funding_id
		-- if the funding has payment schedules it is possible the frequency has changed so we need to look at the payment schedule with no end date.
		DECLARE @tempFrequency VARCHAR (50)
		SELECT TOP 1 @tempFrequency = frequency
		FROM payment_schedules ps 
		INNER JOIN funding_payment_schedules fps ON ps.payment_schedule_id = fps.payment_schedule_id
		WHERE fps.funding_id = @funding_id AND ps.end_date IS NULL
		ORDER BY ps.payment_schedule_id DESC -- get the latest if there is more than one that has no end date

		SET @frequency = COALESCE(@tempFrequency, @frequency)

		-- collection types: Active_contract, Customer_Service, Collections, Legal
		IF @collection_type NOT IN ('Legal', 'Collections')
			BEGIN
				-- could be Active or in Customer Service but still check for Collections

				-- check the last X number of transactiopns.  If any were S01 then ignore.  So use NOT EXISTS
				IF (NOT EXISTS 
				(SELECT 1 FROM 
					(
						SELECT TOP (CASE @frequency WHEN 'Daily' THEN @MissedDailyCollections WHEN 'Weekly' THEN @MissedWeeklyCollections END) settle_response_code
						FROM forte_settlements s
						INNER JOIN fundings f ON s.customer_id = f.contract_number
						WHERE f.id = @funding_id
						ORDER BY settle_date DESC
					) x
					WHERE x.settle_response_code = 'S01'
				) AND @transCount >= CASE @frequency WHEN 'Daily' THEN @MissedDailyCollections WHEN 'Weekly' THEN @MissedWeeklyCollections END)
				BEGIN				
					UPDATE fundings SET collection_type = 'Collections' WHERE id = @funding_id
					-- in case they are not already on hold,
					UPDATE fundings SET on_hold = 1, payments_stopped_date = GETUTCDATE() WHERE id = @funding_id AND COALESCE(on_hold, 0) = 0
					SET @collection_type = 'Collections'
				END
			END
		IF @collection_type = 'Active_contract' -- did not go into Collections
			BEGIN				
				-- check the last X number of transactions.  If any were S01 then ignore.  So use NOT EXISTS
				IF (NOT EXISTS 
				(SELECT 1 FROM 
					(
						SELECT TOP (CASE @frequency WHEN 'Daily' THEN @MissedDailyCS WHEN 'Weekly' THEN @MissedWeeklyCS END) settle_response_code
						FROM forte_settlements s
						INNER JOIN fundings f ON s.customer_id = f.contract_number
						WHERE f.id = @funding_id
						ORDER BY settle_date DESC
					) x
					WHERE x.settle_response_code = 'S01'
				) AND @transCount >= CASE @frequency WHEN 'Daily' THEN @MissedDailyCollections WHEN 'Weekly' THEN @MissedWeeklyCollections END)
				BEGIN				
					UPDATE fundings SET collection_type = 'Customer_Service' WHERE id = @funding_id 
				END
			END
	END
END
GO
