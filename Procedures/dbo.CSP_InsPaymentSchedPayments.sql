SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-07-25
-- Description:	Insert payment schedule payments for all payment schedules
-- This essentially works blindly.  It does not care about other payment schedules and does not care much about funding payoff amount.
-- It takes the payment schedule as defined if there is an end date.  If there is no end date, then it does look at funding payoff amount to see
-- if one more should happen.  When fundings are marked complete, future payments in here will need to be removed.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_InsPaymentSchedPayments]
--(
--	@payment_schedule_id			INT
--)
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
BEGIN TRANSACTION

-- VARS
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SELECT @tomorrow = DATEADD(DD, CASE DATEPART(WEEKDAY, @today) WHEN 6 THEN 3 WHEN 7 THEN 2 ELSE 1 END, @today)
DECLARE @maxID INT, @currentID INT, @start_date DATE, @end_date DATE, @tempDate DATE, @frequency VARCHAR (50), @amount MONEY, @payment_schedule_id INT
-- END VARS


-- First delete any in the future if there is no end date or if the funding is paid off
DELETE psp
FROM payment_schedule_payments psp WITH (NOLOCK)
INNER JOIN funding_payment_schedules fps WITH (NOLOCK) ON psp.payment_schedule_id = fps.payment_schedule_id
INNER JOIN payment_schedules ps WITH (NOLOCK) ON psp.payment_schedule_id = ps.payment_schedule_id
INNER JOIN fundings f WITH (NOLOCK) ON fps.funding_id = f.id
WHERE psp.payment_date > COALESCE(f.completed_date, @today) AND psp.rec_amount IS NULL
	AND (ps.end_date IS NULL OR f.contract_status IN (2))

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

PRINT CONCAT('1: ',  SYSDATETIME())

IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData
CREATE TABLE #tempData (id int not null identity, payment_schedule_id INT, frequency VARCHAR (50), amount MONEY, start_date DATE, end_date DATE)

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


-- if the end date is NULL then we can blindly dump in the records.  Don't care about payoff or other payment schedules.  They'll get removed
-- by the above code if the contract pays off before the end date


-- first get a list of payment_schedules with end dates that are NOT in the payment_schedule_payments table
INSERT INTO #tempData (payment_schedule_id, frequency, amount, start_date, end_date)
SELECT ps.payment_schedule_id, ps.frequency, ps.amount, ps.start_date, ps.end_date
FROM payment_schedules ps WITH (NOLOCK)
WHERE ps.end_date IS NOT NULL --AND ps.active = 1
	AND NOT EXISTS (SELECT 1 FROM payment_schedule_payments psp WHERE psp.payment_schedule_id = ps.payment_schedule_id)

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

SELECT @maxID = COALESCE(MAX(t.id), 0) FROM #tempData t
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

-- now loop through all those that have end dates and create records for them
SET @currentID = 1
WHILE @currentID <= @maxID
	BEGIN
		SELECT @start_date = t.start_date, @end_date = t.end_date, @tempDate = t.start_date, @frequency = t.frequency, @amount = t.amount, @payment_schedule_id = t.payment_schedule_id
		FROM #tempData t
		WHERE t.id = @currentID

		WHILE @tempDate <= @end_date --AND @tempDate >= @start_date
			BEGIN											
				IF @frequency = 'Daily'
					BEGIN
						IF DATEPART(dw, @tempDate) IN (2,3,4,5,6) -- only do Monday Through Friday even though CC might have weekend amounts
							BEGIN
								INSERT INTO payment_schedule_payments(payment_schedule_id, payment_date, expected_amount, rec_amount, payment_id, trans_id)
								VALUES (@payment_schedule_id, @tempDate, @amount, NULL, NULL, NULL)

								SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
							END
					END
				ELSE IF @frequency = 'Weekly'
					BEGIN
						IF DATEPART(dw, @tempDate) = DATEPART(dw, @start_date) AND @tempDate >= @start_date
							BEGIN
								-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
								INSERT INTO payment_schedule_payments(payment_schedule_id, payment_date, expected_amount, rec_amount, payment_id, trans_id)
								VALUES (@payment_schedule_id, @tempDate, @amount, NULL, NULL, NULL)

								SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
							END
					END
				ELSE IF @frequency = 'Monthly'
					BEGIN
						IF DAY(@tempDate) = DAY(@start_date) AND @tempDate >= @start_date
							BEGIN
								-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
								INSERT INTO payment_schedule_payments(payment_schedule_id, payment_date, expected_amount, rec_amount, payment_id, trans_id)
								VALUES (@payment_schedule_id, @tempDate, @amount, NULL, NULL, NULL)

								SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
							END
					END
				-- increment @tempDate
				SET @tempDate = DATEADD(D, 1, @tempDate)
			END	
		SET @currentID = @currentID + 1
	END

	PRINT CONCAT('2: ',  SYSDATETIME())

-- now those that have NULL end date, just insert another one for tomorrow if there is still an active contract with a balance
TRUNCATE TABLE #tempData
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

INSERT INTO #tempData (payment_schedule_id, frequency, amount, start_date, end_date)
SELECT ps.payment_schedule_id, ps.frequency, ps.amount, --ps.start_date, 
	COALESCE((SELECT MAX(payment_date) FROM payment_schedule_payments psp WHERE payment_schedule_id = ps.payment_schedule_id), ps.start_date) AS start_date,  -- start where we left off
	COALESCE(ps.end_date, f.completed_date, @tomorrow)
FROM payment_schedules ps WITH (NOLOCK)
INNER JOIN funding_payment_schedules fps WITH (NOLOCK) ON ps.payment_schedule_id = fps.payment_schedule_id
INNER JOIN fundings f WITH (NOLOCK) ON fps.funding_id = f.id
WHERE ps.end_date IS NULL --AND ps.active = 1
	AND (f.contract_status = 1 OR (f.contract_status = 4 AND f.contract_substatus_id = 1)) AND f.payoff_amount > 0

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

PRINT CONCAT('3: ',  SYSDATETIME())

SELECT @maxID = COALESCE(MAX(t.id), 0) FROM #tempData t
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

-- now loop through all those that DO NOT have end dates and create records for them
SET @currentID = 1
WHILE @currentID <= @maxID
	BEGIN
		SELECT @start_date = t.start_date, @end_date = t.end_date, @tempDate = t.start_date, @frequency = t.frequency, @amount = t.amount, @payment_schedule_id = t.payment_schedule_id
		FROM #tempData t
		WHERE t.id = @currentID

		WHILE @tempDate <= @end_date-- AND @tempDate >= @start_date
			BEGIN											
				IF @frequency = 'Daily'
					BEGIN
						IF DATEPART(dw, @tempDate) IN (2,3,4,5,6) -- only do Monday Through Friday even though CC might have weekend amounts
							BEGIN
								INSERT INTO payment_schedule_payments(payment_schedule_id, payment_date, expected_amount, rec_amount, payment_id, trans_id)
								SELECT @payment_schedule_id, @tempDate, @amount, NULL, NULL, NULL
								WHERE NOT EXISTS (SELECT 1 FROM payment_schedule_payments psp WITH (NOLOCK) WHERE psp.payment_date = @tempDate AND psp.payment_schedule_id = @payment_schedule_id
									AND psp.expected_amount = @amount)

								SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
							END
					END
				ELSE IF @frequency = 'Weekly'
					BEGIN
						IF DATEPART(dw, @tempDate) = DATEPART(dw, @start_date) AND @tempDate >= @start_date
							BEGIN
								-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
								INSERT INTO payment_schedule_payments(payment_schedule_id, payment_date, expected_amount, rec_amount, payment_id, trans_id)
								SELECT  @payment_schedule_id, @tempDate, @amount, NULL, NULL, NULL
								WHERE NOT EXISTS (SELECT 1 FROM payment_schedule_payments psp WITH (NOLOCK) WHERE psp.payment_date = @tempDate AND psp.payment_schedule_id = @payment_schedule_id
									AND psp.expected_amount = @amount)

								SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
							END
					END
				ELSE IF @frequency = 'Monthly'
					BEGIN
						IF DAY(@tempDate) = DAY(@start_date) AND @tempDate >= @start_date
							BEGIN
								-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
								INSERT INTO payment_schedule_payments(payment_schedule_id, payment_date, expected_amount, rec_amount, payment_id, trans_id)
								SELECT  @payment_schedule_id, @tempDate, @amount, NULL, NULL, NULL
								WHERE NOT EXISTS (SELECT 1 FROM payment_schedule_payments psp WITH (NOLOCK) WHERE psp.payment_date = @tempDate AND psp.payment_schedule_id = @payment_schedule_id
									AND psp.expected_amount = @amount)

								SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
							END
					END
				-- increment @tempDate
				SET @tempDate = DATEADD(D, 1, @tempDate)
			END	
		SET @currentID = @currentID + 1
	END
PRINT CONCAT('4: ',  SYSDATETIME())

COMMIT TRANSACTION
GOTO Step2

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)
	Goto Done


Step2:
PRINT CONCAT('5: ',  SYSDATETIME())
BEGIN TRANSACTION
-- now that we have inserted all expected payment schedule payments through tomorrow, let's update with any payments received
-- First do Forte received
UPDATE psp
SET rec_amount = p.amount, payment_id = p.payment_id, trans_id = t.trans_id
FROM payment_schedule_payments psp WITH (NOLOCK)
INNER JOIN payment_schedules ps WITH (NOLOCK) ON psp.payment_schedule_id = ps.payment_schedule_id
INNER JOIN payments p WITH (NOLOCK) ON ps.payment_schedule_id = p.payment_schedule_id AND psp.payment_date = p.trans_date
INNER JOIN forte_settlements fs WITH (NOLOCK) ON p.transaction_id = fs.transaction_id AND fs.settle_response_code = 'S01'
INNER JOIN transactions t WITH (NOLOCK) ON p.transaction_id = t.transaction_id AND t.trans_type_id = 3
WHERE (psp.rec_amount IS NULL OR psp.trans_id IS NULL OR psp.payment_id IS NULL) 
	AND ps.payment_method = 'ACH' AND ps.payment_processor = 'Forte' AND ps.frequency = 'Daily'

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

PRINT CONCAT('6: ',  SYSDATETIME())

-- now do forte rejected
UPDATE psp
SET rec_amount = 0, payment_id = p.payment_id, trans_id = t.trans_id
FROM payment_schedule_payments psp WITH (NOLOCK)
INNER JOIN payment_schedules ps WITH (NOLOCK) ON psp.payment_schedule_id = ps.payment_schedule_id
INNER JOIN payments p WITH (NOLOCK) ON ps.payment_schedule_id = p.payment_schedule_id AND psp.payment_date = p.trans_date
INNER JOIN forte_settlements fs WITH (NOLOCK) ON p.transaction_id = fs.transaction_id AND fs.settle_response_code != 'S01'
INNER JOIN transactions t WITH (NOLOCK) ON p.transaction_id = t.transaction_id AND t.trans_type_id = 21
WHERE (psp.rec_amount IS NULL OR psp.trans_id IS NULL OR psp.payment_id IS NULL) 
	AND ps.payment_method = 'ACH' AND ps.payment_processor = 'Forte' AND ps.frequency = 'Daily'

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

PRINT CONCAT('7: ',  SYSDATETIME())

-- now do forte chargeback
UPDATE psp
SET chargeback_amount = p.amount, payment_id = p.payment_id, trans_id = t.trans_id, chargeback_date = fs.settle_date
FROM payment_schedule_payments psp WITH (NOLOCK)
INNER JOIN payment_schedules ps WITH (NOLOCK) ON psp.payment_schedule_id = ps.payment_schedule_id
INNER JOIN payments p WITH (NOLOCK) ON ps.payment_schedule_id = p.payment_schedule_id AND psp.payment_date = p.trans_date
INNER JOIN forte_settlements fs WITH (NOLOCK) ON p.transaction_id = fs.transaction_id AND fs.settle_response_code != 'S01'
INNER JOIN transactions t WITH (NOLOCK) ON p.transaction_id = t.transaction_id AND t.trans_type_id = 14
WHERE (psp.chargeback_amount IS NULL OR psp.trans_id IS NULL OR psp.payment_id IS NULL) 
	AND ps.payment_method = 'ACH' AND ps.payment_processor = 'Forte' AND ps.frequency = 'Daily'

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

PRINT CONCAT('8: ',  SYSDATETIME())


	-- Secondly do NACHA received
UPDATE psp
SET rec_amount = p.amount, payment_id = p.payment_id, trans_id = t.trans_id
FROM payment_schedule_payments psp WITH (NOLOCK)
INNER JOIN payment_schedules ps WITH (NOLOCK) ON psp.payment_schedule_id = ps.payment_schedule_id
INNER JOIN payments p WITH (NOLOCK) ON ps.payment_schedule_id = p.payment_schedule_id AND psp.payment_date = p.trans_date
INNER JOIN nacha_trans nt WITH (NOLOCK) ON p.payment_id = nt.payment_id AND nt.reason_code = 'S01'
INNER JOIN transactions t WITH (NOLOCK) ON p.transaction_id = t.transaction_id AND t.trans_type_id = 3
WHERE (psp.rec_amount IS NULL OR psp.trans_id IS NULL OR psp.payment_id IS NULL) 
	AND ps.payment_method = 'ACH' AND ps.payment_processor IN ('53', 'Regions') AND ps.frequency = 'Daily'

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

PRINT CONCAT('9: ',  SYSDATETIME())

-- now do NACHA chargeback
UPDATE psp
SET chargeback_amount = p.amount, payment_id = p.payment_id, trans_id = t.trans_id, chargeback_date = nt.settle_date
FROM payment_schedule_payments psp WITH (NOLOCK)
INNER JOIN payment_schedules ps ON psp.payment_schedule_id = ps.payment_schedule_id
INNER JOIN payments p WITH (NOLOCK) ON ps.payment_schedule_id = p.payment_schedule_id AND psp.payment_date = p.trans_date
INNER JOIN nacha_trans nt WITH (NOLOCK) ON p.payment_id = nt.payment_id AND nt.reason_code != 'S01'
INNER JOIN transactions t WITH (NOLOCK) ON p.transaction_id = t.transaction_id AND t.trans_type_id = 14
WHERE (psp.chargeback_amount IS NULL OR psp.trans_id IS NULL OR psp.payment_id IS NULL) 
	AND ps.payment_method = 'ACH' AND ps.payment_processor IN ('53', 'Regions') AND ps.frequency = 'Daily'

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

PRINT CONCAT('10: ',  SYSDATETIME())

-- Checks, wires, and credit card all have to be done one at a time.  Need temp table to do work
-- 2019-08-05 - Weekly pulls also need to be done this way since they won't necessarily match to a date
IF OBJECT_ID('tempdb..#tempTrans') IS NOT NULL DROP TABLE #tempTrans
CREATE TABLE #tempTrans (id int not null identity, payment_schedule_payment_id INT, frequency VARCHAR (50), amount MONEY, start_date DATE, end_date DATE)

DECLARE @bufferDays INT; SET @bufferDays = 5


-- Now do WEEKLY ACH   ************************************************** WEEKLY ACH BEGIN *****************************************
SELECT t.*
INTO #tempTransactionsACH
FROM transactions t WITH (NOLOCK)
WHERE t.trans_type_id = 3 AND t.redistributed = 0

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

PRINT CONCAT('11: ',  SYSDATETIME())

-- start a list of in-use trans ids
SELECT DISTINCT psp.trans_id
INTO #tempPSPayments
FROM payment_schedule_payments psp WITH (NOLOCK)
WHERE psp.trans_id IS NOT NULL

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

PRINT CONCAT('12: ',  SYSDATETIME())


INSERT INTO #tempTrans (payment_schedule_payment_id)
SELECT psp.id
FROM payment_schedule_payments psp WITH (NOLOCK)
INNER JOIN payment_schedules ps WITH (NOLOCK) ON psp.payment_schedule_id = ps.payment_schedule_id
WHERE (psp.rec_amount IS NULL OR psp.trans_id IS NULL) 
	AND ps.payment_method = 'ACH' AND ps.frequency = 'Weekly'
ORDER BY psp.payment_date

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

PRINT CONCAT('13: ',  SYSDATETIME())

SET @currentID = 1
SELECT @maxID = COALESCE(MAX(t.id), 0) FROM #tempTrans t
DECLARE @tempID INT

WHILE @currentID <= @maxID
	BEGIN
		SELECT @tempID = t.payment_schedule_payment_id
		FROM #tempTrans t
		WHERE t.id = @currentID

		UPDATE psp
		SET rec_amount = COALESCE(t.trans_amount, t2.trans_amount, t3.trans_amount), trans_id = COALESCE(t.trans_id, t2.trans_id, t3.trans_id)
		FROM payment_schedule_payments psp WITH (NOLOCK)
		INNER JOIN payment_schedules ps WITH (NOLOCK) ON psp.payment_schedule_id = ps.payment_schedule_id
		INNER JOIN funding_payment_schedules fps WITH (NOLOCK) ON ps.payment_schedule_id = fps.payment_schedule_id
		LEFT JOIN #tempTransactionsACH t WITH (NOLOCK) ON fps.funding_id = t.funding_id AND psp.payment_date = t.trans_date AND t.trans_id NOT IN (SELECT trans_id FROM #tempPSPayments)
		LEFT JOIN #tempTransactionsACH t2 WITH (NOLOCK) ON fps.funding_id = t2.funding_id AND ABS(DATEDIFF(DD, psp.payment_date, t2.trans_date)) <= @bufferDays 
			AND t2.trans_id NOT IN (SELECT trans_id FROM #tempPSPayments) AND t2.trans_amount = psp.expected_amount  -- get the one that is same amount but on different day
		LEFT JOIN #tempTransactionsACH t3 WITH (NOLOCK) ON fps.funding_id = t3.funding_id AND ABS(DATEDIFF(DD, psp.payment_date, t3.trans_date)) <= @bufferDays 
			AND t3.trans_id NOT IN (SELECT trans_id FROM #tempPSPayments) AND t3.trans_amount != psp.expected_amount  -- get the one that is different amount and on different day

		WHERE psp.id = @tempID

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

		-- if the trans_id is now gotten, add to temp table so it does not get used again
		INSERT INTO #tempPSPayments (trans_id)
		SELECT psp.trans_id
		FROM payment_schedule_payments psp WITH (NOLOCK)
		WHERE psp.id = @tempID AND psp.trans_id IS NOT NULL

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

		SET @currentID = @currentID + 1
	END

--   ************************************************** WEEKLY ACH END *****************************************

PRINT CONCAT('14: ',  SYSDATETIME())

-- Now do Checks   ************************************************** CHECKS BEGIN *****************************************
TRUNCATE TABLE #tempTrans

SELECT t.*
INTO #tempTransactions
FROM transactions t WITH (NOLOCK)
WHERE t.trans_type_id = 19 AND t.redistributed = 0

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

PRINT CONCAT('15: ',  SYSDATETIME())

INSERT INTO #tempTrans (payment_schedule_payment_id)
SELECT psp.id
FROM payment_schedule_payments psp WITH (NOLOCK)
INNER JOIN payment_schedules ps WITH (NOLOCK) ON psp.payment_schedule_id = ps.payment_schedule_id
WHERE (psp.rec_amount IS NULL OR psp.trans_id IS NULL) 
	AND ps.payment_method = 'Check' 
ORDER BY psp.payment_date

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

PRINT CONCAT('16: ',  SYSDATETIME())

SET @currentID = 1
SELECT @maxID = COALESCE(MAX(t.id), 0) FROM #tempTrans t

WHILE @currentID <= @maxID
	BEGIN
		SELECT @tempID = t.payment_schedule_payment_id
		FROM #tempTrans t
		WHERE t.id = @currentID

		UPDATE psp
		SET rec_amount = COALESCE(t.trans_amount, t2.trans_amount, t3.trans_amount), trans_id = COALESCE(t.trans_id, t2.trans_id, t3.trans_id)
		FROM payment_schedule_payments psp WITH (NOLOCK)
		INNER JOIN payment_schedules ps WITH (NOLOCK) ON psp.payment_schedule_id = ps.payment_schedule_id
		INNER JOIN funding_payment_schedules fps WITH (NOLOCK) ON ps.payment_schedule_id = fps.payment_schedule_id
		LEFT JOIN #tempTransactions t WITH (NOLOCK) ON fps.funding_id = t.funding_id AND psp.payment_date = t.trans_date AND t.trans_id NOT IN (SELECT trans_id FROM #tempPSPayments)
		LEFT JOIN #tempTransactions t2 WITH (NOLOCK) ON fps.funding_id = t2.funding_id AND ABS(DATEDIFF(DD, psp.payment_date, t2.trans_date)) <= @bufferDays 
			AND t2.trans_id NOT IN (SELECT trans_id FROM #tempPSPayments)-- get the one that is same amount but on different day
		LEFT JOIN #tempTransactionsACH t3 WITH (NOLOCK) ON fps.funding_id = t3.funding_id AND ABS(DATEDIFF(DD, psp.payment_date, t3.trans_date)) <= @bufferDays 
			AND t3.trans_id NOT IN (SELECT trans_id FROM #tempPSPayments) AND t3.trans_amount != psp.expected_amount  -- get the one that is different amount and on different day
		WHERE psp.id = @tempID

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

		-- if the trans_id is now gotten, add to temp table so it does not get used again
		INSERT INTO #tempPSPayments (trans_id)
		SELECT psp.trans_id
		FROM payment_schedule_payments psp WITH (NOLOCK)
		WHERE psp.id = @tempID AND psp.trans_id IS NOT NULL

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

		SET @currentID = @currentID + 1
	END

--   ************************************************** CHECKS END *****************************************

PRINT CONCAT('17: ',  SYSDATETIME())

--   ************************************************** WIRES BEGIN *****************************************
TRUNCATE TABLE #tempTrans

SELECT t.*
INTO #tempTransactionsWires
FROM transactions t WITH (NOLOCK)
WHERE t.trans_type_id = 5 AND t.redistributed = 0
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

PRINT CONCAT('18: ',  SYSDATETIME())

INSERT INTO #tempTrans (payment_schedule_payment_id)
SELECT psp.id
FROM payment_schedule_payments psp WITH (NOLOCK)
INNER JOIN payment_schedules ps WITH (NOLOCK) ON psp.payment_schedule_id = ps.payment_schedule_id
WHERE (psp.rec_amount IS NULL OR psp.trans_id IS NULL) 
	AND ps.payment_method = 'Wire' 
ORDER BY psp.payment_date

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

PRINT CONCAT('19: ',  SYSDATETIME())

SET @currentID = 1
SELECT @maxID = COALESCE(MAX(t.id), 0) FROM #tempTrans t

WHILE @currentID <= @maxID
	BEGIN
		SELECT @tempID = t.payment_schedule_payment_id
		FROM #tempTrans t
		WHERE t.id = @currentID

		UPDATE psp
		SET rec_amount = COALESCE(t.trans_amount, t2.trans_amount, t3.trans_amount), trans_id = COALESCE(t.trans_id, t2.trans_id, t3.trans_id)
		--OUTPUT inserted.*, fps.*, t2.*
		FROM payment_schedule_payments psp WITH (NOLOCK)
		INNER JOIN payment_schedules ps WITH (NOLOCK) ON psp.payment_schedule_id = ps.payment_schedule_id
		INNER JOIN funding_payment_schedules fps WITH (NOLOCK) ON ps.payment_schedule_id = fps.payment_schedule_id
		LEFT JOIN #tempTransactionsWires t WITH (NOLOCK) ON fps.funding_id = t.funding_id AND psp.payment_date = t.trans_date AND t.trans_id NOT IN (SELECT trans_id FROM #tempPSPayments)
		LEFT JOIN #tempTransactionsWires t2 WITH (NOLOCK) ON fps.funding_id = t2.funding_id AND ABS(DATEDIFF(DD, psp.payment_date, t2.trans_date)) <= @bufferDays 
			AND t2.trans_id NOT IN (SELECT trans_id FROM #tempPSPayments)
			-- get the one that is same amount but on different day
		LEFT JOIN #tempTransactionsACH t3 WITH (NOLOCK) ON fps.funding_id = t3.funding_id AND ABS(DATEDIFF(DD, psp.payment_date, t3.trans_date)) <= @bufferDays 
			AND t3.trans_id NOT IN (SELECT trans_id FROM #tempPSPayments) AND t3.trans_amount != psp.expected_amount  -- get the one that is different amount and on different day
		WHERE psp.id = @tempID

		

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

		-- if the trans_id is now gotten, add to temp table so it does not get used again
		INSERT INTO #tempPSPayments (trans_id)
		SELECT psp.trans_id
		FROM payment_schedule_payments psp WITH (NOLOCK)
		WHERE psp.id = @tempID AND psp.trans_id IS NOT NULL

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

		SET @currentID = @currentID + 1
	END

--   ************************************************** WIRES END *****************************************

PRINT CONCAT('20: ',  SYSDATETIME())

--   ************************************************** CC BEGIN *****************************************
TRUNCATE TABLE #tempTrans

SELECT t.*
INTO #tempTransactionsCC
FROM transactions t WITH (NOLOCK)
WHERE t.trans_type_id = 4 AND t.redistributed = 0
SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

PRINT CONCAT('21: ',  SYSDATETIME())


INSERT INTO #tempTrans (payment_schedule_payment_id)
SELECT psp.id
FROM payment_schedule_payments psp WITH (NOLOCK)
INNER JOIN payment_schedules ps WITH (NOLOCK) ON psp.payment_schedule_id = ps.payment_schedule_id
WHERE (psp.rec_amount IS NULL OR psp.trans_id IS NULL) 
	AND ps.payment_method = 'CC' 
ORDER BY psp.payment_date

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

PRINT CONCAT('22: ',  SYSDATETIME())

SET @currentID = 1
SELECT @maxID = COALESCE(MAX(t.id), 0) FROM #tempTrans t

WHILE @currentID <= @maxID
	BEGIN
		SELECT @tempID = t.payment_schedule_payment_id
		FROM #tempTrans t
		WHERE t.id = @currentID

		UPDATE psp
		SET rec_amount = COALESCE(t.trans_amount, t2.trans_amount, t3.trans_amount), trans_id = COALESCE(t.trans_id, t2.trans_id, t3.trans_id)
		FROM payment_schedule_payments psp WITH (NOLOCK)
		INNER JOIN payment_schedules ps WITH (NOLOCK) ON psp.payment_schedule_id = ps.payment_schedule_id
		INNER JOIN funding_payment_schedules fps WITH (NOLOCK) ON ps.payment_schedule_id = fps.payment_schedule_id
		LEFT JOIN #tempTransactionsCC t WITH (NOLOCK) ON fps.funding_id = t.funding_id AND psp.payment_date = t.trans_date AND t.trans_id NOT IN (SELECT trans_id FROM #tempPSPayments)
		LEFT JOIN #tempTransactionsCC t2 WITH (NOLOCK) ON fps.funding_id = t2.funding_id AND ABS(DATEDIFF(DD, psp.payment_date, t2.trans_date)) <= @bufferDays 
			AND t2.trans_id NOT IN (SELECT trans_id FROM #tempPSPayments) 	-- get the one that is same amount but on different day
		LEFT JOIN #tempTransactionsACH t3 WITH (NOLOCK) ON fps.funding_id = t3.funding_id AND ABS(DATEDIFF(DD, psp.payment_date, t3.trans_date)) <= @bufferDays 
			AND t3.trans_id NOT IN (SELECT trans_id FROM #tempPSPayments) AND t3.trans_amount != psp.expected_amount  -- get the one that is different amount and on different day
		WHERE psp.id = @tempID

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

		-- if the trans_id is now gotten, add to temp table so it does not get used again
		INSERT INTO #tempPSPayments (trans_id)
		SELECT psp.trans_id
		FROM payment_schedule_payments psp WITH (NOLOCK)
		WHERE psp.id = @tempID AND psp.trans_id IS NOT NULL

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

		SET @currentID = @currentID + 1
	END

--   ************************************************** CC END *****************************************

PRINT CONCAT('23: ',  SYSDATETIME())

COMMIT TRANSACTION
GOTO Done

ERROR2:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)

Done:

IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData
IF OBJECT_ID('tempdb..#tempTrans') IS NOT NULL DROP TABLE #tempTrans

PRINT CONCAT('24: ',  SYSDATETIME())
END


GO
