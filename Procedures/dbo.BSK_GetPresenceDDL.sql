SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2021-03-26
-- Description: Gets the dropdown options for presence 
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetPresenceDDL]

AS

SET NOCOUNT ON


SELECT s.user_presence_status_id, s.status_name, s.status_description
FROM user_presence_statuses s
ORDER BY s.status_description

GO
