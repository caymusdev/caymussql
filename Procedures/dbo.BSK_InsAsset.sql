SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Rick Pina
-- Create Date: 2020-10-20
-- Description: Adds user to users table
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsAsset]
(
	@affiliate_id				INT,
	@asset_types_id				INT,
	@asset_name					NVARCHAR (100),
	@asset_price				NVARCHAR (100),
	@asset_description			NVARCHAR (100),
	@asset_owner				NVARCHAR (100),
	@asset_located				BIT,
	@create_user				NVARCHAR (100)
)
AS

SET NOCOUNT ON

	INSERT INTO assets(affiliate_id, asset_types_id, asset_name, asset_price, asset_description, asset_owner, asset_located, create_date, change_date, change_user, create_user)
	Values (@affiliate_id, @asset_types_id, @asset_name, @asset_price, @asset_description, @asset_owner, @asset_located, GETUTCDATE(), GETUTCDATE(), @create_user, @create_user)

GO
