SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-10-12
-- Description:	Updates a payment schedule
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdPaymentSchedule]
(
	@payment_schedule_id	INT,
	@frequency				VARCHAR (50),
	@amount					MONEY,
	@start_date				DATE,
	@end_date				DATE,
	@active					BIT,
	@bank_name				NVARCHAR (50),
	@routing_number			VARCHAR (50),
	@account_number			VARCHAR (50),
	@userid					NVARCHAR (100),
	@funding_id				BIGINT,
	@approved_by			NVARCHAR (100) = NULL,
	@change_id				INT = NULL,
	@comments				NVARCHAR (4000) = NULL,
	@payment_processor		VARCHAR (50) = NULL,
	--@payment_method			VARCHAR (50) = NULL,
	@change_reason_id		INT = NULL,
	@part_of_plan			BIT = NULL
)
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
DECLARE @pending_perf_status INT; SELECT @pending_perf_status = id FROM payment_plan_status_codes WHERE code = 'Pending' AND parent_id IS NULL

-- First need to check and see if this user needs approval for editing/creating payment schedules
DECLARE @needs_approval BIT; SET @needs_approval = 0
DECLARE @payment_plan_id INT
SELECT @payment_plan_id = pp.payment_plan_id
FROM payment_plans pp
INNER JOIN fundings f ON pp.merchant_id = f.merchant_id AND f.id = @funding_id
WHERE COALESCE(@part_of_plan, 0) = 1

DECLARE @payment_method VARCHAR (50); SELECT @payment_method = payment_method FROM payment_schedules WHERE payment_schedule_id = @payment_schedule_id


IF @approved_by IS NULL AND EXISTS (SELECT 1 FROM sec_payment_schedules s WHERE s.submitter_user_id = @userid)
	BEGIN
		SET @needs_approval = 1

		-- if there is already an unapproved queued record then assume update otherwise insert
		IF EXISTS(SELECT 1 FROM payment_schedule_changes WHERE payment_schedule_id = @payment_schedule_id AND approved_by IS NULL)
			-- do an update
			BEGIN
				UPDATE payment_schedule_changes
				SET frequency = @frequency, amount = @amount, start_date = @start_date, end_date = @end_date, active = @active, bank_name = @bank_name, routing_number = @routing_number,
					account_number = @account_number, userid = @userid, payment_schedule_id = @payment_schedule_id, comments = @comments, payment_processor = @payment_processor,
					--payment_method = @payment_method, 
					change_reason_id = @change_reason_id, part_of_plan = @part_of_plan
				WHERE payment_schedule_id = @payment_schedule_id AND approved_by IS NULL
			END
		ELSE 
			BEGIN
				INSERT INTO payment_schedule_changes (change_type, payment_schedule_id, frequency, amount, start_date, end_date, active, bank_name, routing_number, account_number, userid,
					funding_id, approved_by, comments, payment_processor, payment_method, change_reason_id, part_of_plan)
				VALUES ('Update', @payment_schedule_id, @frequency, @amount, @start_date, @end_date, @active, @bank_name, @routing_number, @account_number, @userid, @funding_id, @approved_by,
					@comments, @payment_processor, @payment_method, @change_reason_id, @part_of_plan)
			END

		SELECT 1 AS NeedsApproval
	END
ELSE IF @approved_by IS NULL
	BEGIN
		-- so, there is NOT a security record which means they have no need to get approved separaretly.
		SET @approved_by = @userid
	END


IF @needs_approval = 0
	BEGIN
		BEGIN TRANSACTION

		IF @payment_plan_id IS NULL AND @part_of_plan = 1
			BEGIN
				-- need to create it since it does not exist
				INSERT INTO payment_plans(merchant_id, affiliate_id, collector, current_status)
				SELECT f.merchant_id, f.affiliate_id, @userid, @pending_perf_status
				FROM fundings f WITH (NOLOCK)
				WHERE f.id = @funding_id

				SELECT @payment_plan_id = SCOPE_IDENTITY()

				INSERT INTO payment_plan_statuses (payment_plan_id, payment_plan_status_code, start_date, end_date)
				SELECT @payment_plan_id, c.id, CONVERT(DATE, GETUTCDATE()), NULL
				FROM payment_plan_status_codes c
				WHERE c.code = 'Pending'
			END

		-- if this schedule has any payments, then they cannot change it.  We'll mark it inactive and create a new one with this data
		IF EXISTS (SELECT 1 FROM payments p WHERE p.payment_schedule_id = @payment_schedule_id AND approved_flag = 1 AND p.deleted = 0 AND p.waived = 0)
			BEGIN
				-- get last payment date for this schedule so we can set it to be the end date
				DECLARE @lastDate DATETIME
				SELECT @lastDate = MAX(trans_date) FROM payments WHERE payment_schedule_id = @payment_schedule_id AND approved_flag = 1 AND deleted = 0 AND waived = 0
				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	IF @errorNum != 0 GOTO ERROR

				-- mark as inactive and set end date
				UPDATE payment_schedules SET active = 0, end_date = @lastDate WHERE payment_schedule_id = @payment_schedule_id
				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	IF @errorNum != 0 GOTO ERROR

				-- delete any non processed payments from the now inactive payment schedule
				UPDATE payments SET deleted = 1 WHERE payment_schedule_id = @payment_schedule_id AND processed_date IS NULL AND trans_date >= @lastDate
					AND deleted = 0 AND waived = 0
				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	IF @errorNum != 0 GOTO ERROR

				-- if last date is after start_date, change start_date
				IF @lastDate > @start_date SET @start_date = DATEADD(DD, 1, @lastDate)
				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	IF @errorNum != 0 GOTO ERROR

				-- create a new one
				DECLARE @newID INT
				EXEC dbo.CSP_InsPaymentSchedule @payment_method, @frequency, @amount, @start_date, NULL, @bank_name, @routing_number, @account_number, @payment_processor, @funding_id, 
					@approved_by, @end_date, @active, @approved_by, @change_id, @comments, @change_reason_id, @part_of_plan
				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(), @newID = MAX(payment_schedule_id) 
				FROM funding_payment_schedules 
				WHERE funding_id = @funding_id;	
				IF @errorNum != 0 GOTO ERROR

				-- insert payment schedule SP will create the schedule.  no need to here.
				--EXEC dbo.CSP_GeneratePaymentsForSchedule @newID, @userid
				--SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	IF @errorNum != 0 GOTO ERROR	
			END
		ELSE
			-- no approved payments on this schedule
			BEGIN		
				-- delete any unapproved payments, change the schedule, and then recreate payments
				UPDATE payments SET change_user = @userid, change_date = GETUTCDATE() WHERE payment_schedule_id = @payment_schedule_id AND approved_flag = 0
				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	IF @errorNum != 0 GOTO ERROR

				DELETE FROM payments WHERE payment_schedule_id = @payment_schedule_id AND approved_flag = 0
				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	IF @errorNum != 0 GOTO ERROR

				UPDATE payment_schedules
				SET	frequency = @frequency, amount = @amount, start_date = @start_date, end_date = @end_date, active = @active, bank_name = @bank_name, routing_number = @routing_number,
					account_number = @account_number, change_user = @userid, create_date = GETUTCDATE(), payment_schedule_change_id = @change_id, comments = @comments,
					payment_processor = @payment_processor, change_reason_id = @change_reason_id, payment_plan_id = @payment_plan_id
				WHERE payment_schedule_id = @payment_schedule_id
				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	IF @errorNum != 0 GOTO ERROR

				EXEC dbo.CSP_GeneratePaymentsForSchedule @payment_schedule_id, @userid
				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	IF @errorNum != 0 GOTO ERROR				
			END

		SELECT 0 AS NeedsApproval

		COMMIT TRANSACTION
		GOTO Done

		ERROR:	
			ROLLBACK TRANSACTION
			RAISERROR (@ErrorMessage, 16, 1)	
	END
Done:

END
GO
