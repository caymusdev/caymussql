SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-04-03
-- Description:	Returns a list of contracts for a given funding that are non-performing and still open on a different merchant under the same affiliate.
-- =============================================
CREATE PROCEDURE [CSP_GetContractsForFundingRefund]
(
	@funding_id				BIGINT
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @affiliate_id BIGINT, @merchant_id BIGINT
SELECT @affiliate_id = affiliate_id, @merchant_id = merchant_id FROM fundings f WHERE f.id = @funding_id


SELECT f2.id AS funding_id, f2.contract_number, f2.legal_name, f2.legal_name + ' - ' + f2.contract_number + ' (' + CONVERT(VARCHAR(50), f2.payoff_amount) + ')' AS funding_name
FROM fundings f2
WHERE f2.affiliate_id = @affiliate_id AND f2.merchant_id <> @merchant_id
	AND COALESCE(f2.never_redistribute, 0) = 0
	AND f2.payoff_amount > 0 
	AND (f2.contract_status = 1 OR (f2.contract_status = 4 AND f2.contract_substatus_id = 1))				
	AND f2.performance_status_id IN (2,3) -- slow pay and nonperforming
ORDER BY funding_name
END
GO
