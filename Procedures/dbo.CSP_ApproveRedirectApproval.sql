SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-08-13
-- Description: Approves or denies a Redirect Approval record
-- =============================================
CREATE PROCEDURE [dbo].[CSP_ApproveRedirectApproval]
(
	@redirect_approval_id		INT,
	@approve					BIT,
	@userid						NVARCHAR (100)
)
AS
BEGIN	
SET NOCOUNT ON;

IF @approve = 1
	BEGIN
		UPDATE redirect_approvals 
		SET change_date = GETUTCDATE(), change_user = @userid, approved_by = @userid, approved_date = GETUTCDATE(), rejected_by = NULL, rejected_date = NULL
		WHERE redirect_approval_id = @redirect_approval_id AND approved_by IS NULL
	END
ELSE 
	BEGIN
		UPDATE redirect_approvals
		SET change_date = GETUTCDATE(), change_user = @userid, rejected_by = @userid, rejected_date = GETUTCDATE(), approved_by = NULL, approved_date = NULL
		WHERE redirect_approval_id = @redirect_approval_id AND rejected_by IS NULL
	END


END
GO
