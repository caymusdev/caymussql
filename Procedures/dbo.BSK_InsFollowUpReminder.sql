SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-05-04 - May the fourth be with you
-- Description:	Used for follow up on a contact.  If user does not specify a reminder date the system might need to create one
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsFollowUpReminder]
(
	@merchant_id				INT,
	@affiliate_id				INT, 
	@contract_id				INT,
	@contact_id					INT,
	@user_email 				NVARCHAR (100),
	@last_contact_status		NVARCHAR (50) = NULL
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @reminder_date DATETIME

IF @last_contact_status IN ('LVM', 'NoAnswer')
	BEGIN
		DECLARE @user_id INT
		SELECT @user_id = user_id FROM users WHERE email = @user_email

		DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE());

		DECLARE @day_amount INT
		SELECT @day_amount = value FROM settings WHERE setting = 'LeftVMFollowup'

		IF @day_amount IS NULL SET @day_amount = 3

		DECLARE @contact_type INT
		SELECT @contact_type = contact_type_id FROM contact_types WHERE contact_type = 'phone'

		SET @reminder_date = DATEADD(DAY, @day_amount, @today)

		INSERT INTO reminders(merchant_id, affiliate_id, contract_id, contact_type_id, reminder_date, contact_id, user_id, notes, completed, [subject], 
			create_date, change_date, change_user, create_user)
		VALUES (@merchant_id, @affiliate_id, @contract_id, @contact_type, @reminder_date, @contact_id, @user_id, 'Follow up call', NULL, NULL, 
			GETUTCDATE(), GETUTCDATE(), @user_email, @user_email)

		SELECT SCOPE_IDENTITY() as reminder_id

		UPDATE c
		SET next_follow_up_date = @reminder_date, change_date = GETUTCDATE(), change_user = @user_email
		FROM cases c
		INNER JOIN affiliates a ON c.affiliate_id = a.affiliate_id AND a.affiliate_id = @affiliate_id
		WHERE c.active = 1
			AND COALESCE(c.next_follow_up_date, '2050-01-01') >= @reminder_date
	END

END

GO
