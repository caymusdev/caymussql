SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-08-03
-- Description:	Pays a funding off early with discounts
-- =============================================
CREATE PROCEDURE [dbo].[CSP_FundingEarlyPayoffWithDiscount]
(
	@funding_id				BIGINT,
	@trans_id				INT,  -- this is the lump sum payment that will have to be adjusted after we add a discount
	@user_id				NVARCHAR (100)
)
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
DECLARE @trans_amount MONEY, @transaction_id NVARCHAR (100), @trans_date DATETIME, @comments NVARCHAR (500), @batch_id INT, @record_id INT, @record_type VARCHAR (50), @previous_id INT
DECLARE @settle_date DATETIME, @settle_type NVARCHAR (50), @response_code VARCHAR (50), @trans_type_id INT, @newBatchID INT, @contract_number NVARCHAR (50)
DECLARE @discount MONEY, @contract_type VARCHAR (50), @merchant_id BIGINT
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SET @tomorrow = dbo.CF_GetTomorrow(@today)
DECLARE @expected_pp MONEY, @current_pp MONEY, @pp_remaining MONEY


SELECT @discount = f.portfolio_value - COALESCE((CASE WHEN DATEDIFF(DD, f.funded_date, @today) < 30 AND f.include_prepay_discounts = 1 THEN f.one_month_prepay_amount
											WHEN DATEDIFF(DD, f.funded_date, @today) BETWEEN 31 AND 60 AND f.include_prepay_discounts = 1 THEN f.two_month_prepay_amount
											WHEN DATEDIFF(DD, f.funded_date, @today) BETWEEN 61 AND 90 AND f.include_prepay_discounts = 1 THEN f.three_month_prepay_amount
											ELSE f.portfolio_value END), f.portfolio_value),
	@contract_number = f.contract_number
FROM fundings f
WHERE f.id = @funding_id

IF COALESCE(@discount, 0) <= 0
	BEGIN
		SELECT COALESCE(@discount, 0) AS discount
	END
ELSE
	BEGIN					
		BEGIN TRANSACTION
				
		EXEC @newBatchID = dbo.CSP_GetNewBatchNumber '', @user_id, 'EarlyPayoffWithDiscount', '', @today, 0

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		-- put in discount

		SELECT @trans_amount = @discount, @transaction_id = NULL, @trans_date = @today, @comments = 'Discount due to early payoff of ' + @contract_number, @record_id = NULL,
			@record_type = NULL, @previous_id = NULL, @settle_date = @today, @settle_type = NULL, @response_code = NULL, @trans_type_id = 8
		
		EXEC dbo.CSP_DistributeTransaction_v2 @funding_id, @trans_amount, @transaction_id, @trans_date, @comments, @newBatchID, @record_id, @record_type, @previous_id, @settle_date, 
			@settle_type, @response_code, @trans_type_id	
			
		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR	

		-- now adjust the wire or other trans for the payoff amount
		SELECT @trans_amount = trans_amount
		FROM transactions 
		WHERE trans_id = @trans_id

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		IF @trans_amount IS NOT NULL
			BEGIN
				SELECT @expected_pp = f.purchase_price FROM fundings f WHERE f.id = @funding_id

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

				SELECT @current_pp = SUM(d.trans_amount) 
				FROM transactions t
				INNER JOIN transaction_details d ON t.trans_id = d.trans_id
				WHERE t.funding_id = @funding_id AND d.trans_type_id = 7 AND COALESCE(t.redistributed, 0) = 0

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

				/*
				@expected_pp - @current_pp needs to be inserted as purchase price.  This is @pp_remaining
				@trans_amount - (@expected_pp - @current_pp) needs to be inserted as margin
				*/
				SET @pp_remaining = @expected_pp - COALESCE(@current_pp, 0)

				IF @trans_amount < @pp_remaining 
					BEGIN
						-- this is a case where underwriting put in a payoff amount less than the remaining purchase price, which should never happen
						-- if it does, we'll just create an alert
						EXEC dbo.CSP_InsAlert 'Funding', 'EarlyPayoff', @funding_id, 'Payoff amount from Solo is less than the remaining Purchase Price.' 
					END
				ELSE
					BEGIN
						-- do all purchase price first and any left goes to margin
						UPDATE transaction_details
						SET trans_amount = trans_amount + @pp_remaining, change_date = GETUTCDATE(), change_user = @user_id
						WHERE trans_id = @trans_id AND trans_type_id = 7

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

						-- now do margin
						UPDATE transaction_details
						SET trans_amount =	trans_amount - @pp_remaining, change_date = GETUTCDATE(), change_user = @user_id
						WHERE trans_id = @trans_id AND trans_type_id = 6

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

					END
			END

		-- set the batch to Closed
		UPDATE batches SET batch_status = 'Closed', change_user = @user_id, change_date = GETUTCDATE()
		WHERE batch_id = @newBatchID

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		-- update the payoff amounts for the open funding
		EXEC dbo.CSP_UpdCalculationsForFunding @funding_id, @user_id

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		-- now put funding on hold so that no more payments will come out
		UPDATE fundings SET on_hold = 1, change_user = @user_id, change_date = GETUTCDATE(), on_hold_date = GETUTCDATE(),
			prepaid_month = CASE WHEN DATEDIFF(DD, funded_date, @today) < 30 THEN 1
								 WHEN DATEDIFF(DD, funded_date, @today) BETWEEN 31 AND 60 THEN 2
								 WHEN DATEDIFF(DD, funded_date, @today) BETWEEN 61 AND 90 THEN 3
								 ELSE NULL END, 
			early_payoff_discount = @discount, 
			early_payoff_commission = CASE WHEN commission_on_funding_or_net_merchant = 1 THEN (purchase_price - @discount) * (commission_percent / 100.00)
											ELSE (net_to_merchant - @discount) * (commission_percent / 100.00) END,
			early_payoff_referral_commission = CASE WHEN commission_on_funding_or_net_merchant = 1 THEN (purchase_price - @discount) * COALESCE(referral_commission_percent / 100.00, 0)
													ELSE (net_to_merchant - @discount) * COALESCE(referral_commission_percent / 100.00, 0) END
		WHERE id = @funding_id
		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		-- deactivate any future payments on the open funding
		UPDATE pd
		SET active = 0, change_date = GETUTCDATE(), change_user = @user_id
		FROM funding_payment_dates pd
		WHERE pd.funding_id = @funding_id AND pd.active = 1 AND pd.payment_date >= @tomorrow

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR

		-- delete any payments in the queue for the open funding
		UPDATE payments 
		SET deleted = 1, change_date = GETUTCDATE(), change_user = @user_id
		WHERE funding_id = @funding_id AND processed_date IS NULL
		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

	

		-- all done, COMMIT
		COMMIT TRANSACTION
		GOTO Done

		ERROR:	
			ROLLBACK TRANSACTION
			RAISERROR (@ErrorMessage, 16, 1)	
		
		Done:

		SELECT COALESCE(@discount, 0) AS discount

	END
END

GO
