SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-06-27
-- Description:	Used to update a setting
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdSetting]
(
	@setting_id			INT,
	@value				NVARCHAR (500),
	@setting_desc		NVARCHAR (1000),
	@setting_group		NVARCHAR (50),
	@encrypt			BIT
)
AS
BEGIN	
SET NOCOUNT ON;

UPDATE settings
SET value = CASE @encrypt WHEN 1 THEN NULL ELSE @value END, setting_desc = @setting_desc, setting_group = @setting_group,
	encrypt_value = CASE @encrypt WHEN 1 THEN ENCRYPTBYPASSPHRASE('corellia', @value) ELSE NULL END, change_date = GETUTCDATE(), change_user = 'SYSTEM'
WHERE setting_id = @setting_id
END
GO
