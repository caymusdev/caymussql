SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:     Ryan Brown
-- Create Date: 2021-05-04
-- Description: this is a record of what was chosen on the contact follow up page.  just a log really.  depending on what is chosen, other records are created.  
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsContactFollowUp]
(
	@contact_log_id				INT,
	@contact_status_id			INT,
	@contact_outcome			NVARCHAR (1000),
	@follow_up_date				DATETIME,
	@follow_up_note				NVARCHAR (4000),
	@follow_up_contact_type		INT,
	@affiliate_note				NVARCHAR (MAX),
	@affiliate_id				BIGINT,
	@contact_first_name			NVARCHAR (50),
	@contact_last_name			NVARCHAR (50),
	@contact_contact_type		INT,
	@change_user				NVARCHAR (100)
)
AS

SET NOCOUNT ON;

IF EXISTS (SELECT 1 FROM contact_follow_ups WHERE contact_log_id = @contact_log_id)
	BEGIN
		UPDATE contact_follow_ups
		SET contact_status_id = @contact_status_id, contact_outcome = @contact_outcome, follow_up_date = @follow_up_date, follow_up_note = @follow_up_note, 
			follow_up_contact_type = @follow_up_contact_type, affiliate_note = @affiliate_note, affiliate_id = @affiliate_id, contact_first_name = @contact_first_name,
			contact_last_name = @contact_last_name, contact_contact_type = @contact_contact_type, change_user = @change_user, change_date = GETUTCDATE()
		WHERE contact_log_id = @contact_log_id
	END
ELSE
	BEGIN
		INSERT INTO contact_follow_ups(contact_log_id, contact_status_id, contact_outcome, follow_up_date, follow_up_note, follow_up_contact_type, affiliate_note, affiliate_id, 
			contact_first_name, contact_last_name, contact_contact_type, create_user, create_date, change_user, change_date)
		VALUES (@contact_log_id, @contact_status_id, @contact_outcome, @follow_up_date, @follow_up_note, @follow_up_contact_type, @affiliate_note, @affiliate_id, @contact_first_name,
			@contact_last_name, @contact_contact_type, @change_user, GETUTCDATE(), @change_user, GETUTCDATE())
	END

GO
