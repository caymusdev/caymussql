SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Rick Pina
-- Create date: 2021-03-22
-- Description:	Only Updates a Single Cases Contact Information 
-- =============================================
CREATE PROCEDURE [dbo].[BSK_UpdCasesContactData]
(
	@affiliate_id						INT,
	@last_contact						DATETIME = NULL,
	@last_contact_type					INT = NULL,
	@last_contact_status_id				INT = NULL,
	@next_follow_up_date				DATETIME = NULL,
	@change_user						NVARCHAR(50)
)
AS
BEGIN
SET NOCOUNT ON;

UPDATE cases
SET last_contact = CASE WHEN @last_contact IS NULL THEN last_contact ELSE @last_contact END,
	last_attempted_contact = CASE WHEN @last_contact IS NULL THEN last_attempted_contact ELSE @last_contact END,
	last_contact_type = CASE WHEN @last_contact_type IS NULL THEN last_contact_type ELSE @last_contact_type END,
	last_attempted_contact_type = CASE WHEN @last_contact_type IS NULL THEN last_attempted_contact_type ELSE @last_contact_type END,
	last_contact_status_id = CASE WHEN @last_contact_status_id IS NULL THEN last_contact_status_id ELSE @last_contact_status_id END,
	last_attempted_contact_status_id = CASE WHEN @last_contact_status_id IS NULL THEN last_attempted_contact_status_id ELSE @last_contact_status_id END,
	next_follow_up_date = CASE WHEN @next_follow_up_date IS NULL THEN next_follow_up_date ELSE @next_follow_up_date END,
	change_user = @change_user,
	change_date = GETUTCDATE()
WHERE affiliate_id = @affiliate_id AND active = 1

END

GO
