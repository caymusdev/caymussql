SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-05-22
-- Description:	Returns the settlement records from the forte_settlements for the given batch id
-- c# then loops through and creates transactions records
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetForteSettlementsForBatch]
(
	@batch_id			INT
)

AS
BEGIN
SET NOCOUNT ON;

SELECT --t.origination_date, s.settle_date, f.id, s.settle_amount, s.settle_response_code, s.settle_type, 
	s.forte_settle_id
FROM forte_settlements s
--INNER JOIN forte_trans t ON s.transaction_id = t.transaction_id
--LEFT JOIN fundings f ON s.customer_id = f.contract_number
WHERE s.batch_id = @batch_id
--ORDER BY t.origination_date 

END
GO
