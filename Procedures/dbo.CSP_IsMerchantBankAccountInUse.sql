SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-02-01
-- Description:	Checks to see if the merchange bank account is in use
-- updated 2020-02-12.  Now checks to see if any funding payment dates exist in the past.  UI will prevent user from changing routing number and account number
-- =============================================
CREATE PROCEDURE [dbo].[CSP_IsMerchantBankAccountInUse]
(
	@merchant_bank_account_id			INT
)
AS
BEGIN
SET NOCOUNT ON;
/*
SELECT COUNT(*) AS NumUsed
FROM payment_schedules ps
WHERE ps.merchant_bank_account_id = @merchant_bank_account_id AND COALESCE(ps.active, 0) = 1
*/
DECLARE @used INT; SET @used = 0
DECLARE @futureCount INT; SET @futureCount = 0
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())

IF EXISTS (
	SELECT 1 
	FROM funding_payment_dates d
	WHERE d.merchant_bank_account_id = @merchant_bank_account_id
		AND payment_date <= @today
	UNION ALL
	SELECT 1 
	FROM funding_payment_dates_work d
	WHERE d.merchant_bank_account_id = @merchant_bank_account_id
		AND payment_date <= @today
)
	BEGIN
		SET @used = 1
	END
ELSE
	BEGIN
		SELECT @futureCount = COUNT(*)
		FROM funding_payment_dates d
		WHERE d.merchant_bank_account_id = @merchant_bank_account_id
			AND d.payment_date > @today AND d.active = 1
	END
END 

SELECT @used AS IsUsedInPast, @futureCount AS futureCount
GO
