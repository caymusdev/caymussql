SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-11-06
-- Description:	Tracks Forte Transactions to make sure we get paid.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_CheckForteTransForIssues] 

AS
BEGIN
SET NOCOUNT ON;


--DECLARE @firstDate DATE
--SELECT @firstDate = CONVERT(DATE, dbo.CF_GetSetting('GoLiveDate'))

-- select transactions that do not have a matching settlement that are over 10 days old
-- and since we started this
SELECT 'Missing Settlement' AS error_type, t.transaction_id, t.customer_id, t.status, t.authorization_amount, t.received_date, 
	t.company_name, '' AS settle_response_code, NULL AS settle_amount
FROM forte_trans t
WHERE NOT EXISTS (SELECT 1 FROM forte_settlements s WHERE s.transaction_id = t.transaction_id)
	--AND t.received_date >= @firstDate
	AND DATEDIFF(D, t.received_date, GETUTCDATE()) > 10
	--AND t.received_date >= '2018-11-26' -- made a change this day that should handle declined ones going forward
	AND t.received_date >= '2020-03-06'  -- added code for the system to handles these so older ones can be ignored
	-- missing settlement is fine for U2 transactions meaning if there is a reject then the system handled it
	AND NOT EXISTS (SELECT 1 FROM transactions trans WHERE trans.transaction_id = t.transaction_id AND trans.trans_type_id = 21)
--ORDER BY t.received_date
UNION 
-- Now get settlements that settled for different than transactions that were not errors
SELECT 'Different Amounts', t.transaction_id, t.customer_id, t.status, t.authorization_amount, t.received_date, 
	t.company_name, s.settle_response_code,	s.settle_amount 
FROM forte_trans t
INNER JOIN forte_settlements s ON t.transaction_id = s.transaction_id
WHERE t.authorization_amount <> s.settle_amount
	AND s.settle_response_code NOT LIKE 'R%'
	AND s.settle_response_code NOT LIKE 'X%'

-- look for settlements that do not have an ach received transaction.  likely means the forte_trans was not downloaded.
UNION 
SELECT 'Missing Transaction', fs.transaction_id, fs.customer_id, '', fs.settle_amount, fs.settle_date, CONVERT(VARCHAR (50), fs.forte_settle_id), fs.settle_response_code,  fs.settle_amount
FROM forte_settlements fs WITH (NOLOCK)
LEFT JOIN transactions t WITH (NOLOCK) ON fs.transaction_id = t.transaction_id AND t.trans_type_id = 3
WHERE t.trans_id IS NULL AND fs.settle_date BETWEEN '2020-01-01' AND (GETUTCDATE() - 5) AND fs.settle_response_code = 'S01'


ORDER BY t.received_date
END
GO
