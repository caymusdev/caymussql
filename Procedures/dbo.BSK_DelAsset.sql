SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Rick Pina
-- Create Date: 2020-11-03
-- Description: Deletes an Asset from the Affiliates Page
-- =============================================
CREATE PROCEDURE [dbo].[BSK_DelAsset]
(
	@asset_id			INT,
	@change_user		NVARCHAR (100)
)
AS
BEGIN
    SET NOCOUNT ON
		BEGIN
			-- for audit trigger
			UPDATE assets SET change_date = GETUTCDATE(), change_user = GETUTCDATE() WHERE asset_id = @asset_id

			DELETE FROM assets WHERE asset_id = @asset_id
		END

END
GO
