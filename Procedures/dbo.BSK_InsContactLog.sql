SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-11-04
-- Description:	Inserts a record into the contact log table
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsContactLog]
(
	@affiliate_id			INT,
	@merchant_id			INT,
	@contract_id			INT,
	@contact_type			VARCHAR (50), 
	@contact_id				INT,
	@contact_outcome		NVARCHAR (1000), 
	@reminder_id			INT,
	@notes					NVARCHAR (4000),
	@change_user			NVARCHAR (100),
	@contact_status			VARCHAR (50),
	@record_id				INT
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @contact_type_id INT, @contact_status_id INT
SELECT @contact_type_id = contact_type_id FROM contact_types WHERE contact_type = @contact_type
SELECT @contact_status_id = contact_status_id FROM contact_statuses WHERE contact_status = @contact_status

INSERT INTO contact_log (affiliate_id, merchant_id, contract_id, contact_type, contact_date, contact_id, contact_outcome, reminder_id, notes, create_date, change_date,
	create_user, change_user, contact_status_id, record_id)
SELECT @affiliate_id, @merchant_id, @contract_id, @contact_type_id, GETUTCDATE(), @contact_id, @contact_outcome, @reminder_id, @notes, GETUTCDATE(), GETUTCDATE(), 
	@change_user, @change_user, @contact_status_id, @record_id


-- set the flag on the active case that there is new communication
UPDATE c
SET new_communication = 1
FROM cases c
INNER JOIN affiliates a ON c.affiliate_id = a.affiliate_id
WHERE c.active = 1 AND COALESCE(c.new_communication, 0) = 0 AND c.affiliate_id = @affiliate_id

	
END

GO
