SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 04-20-2018
-- Description:	Inserts a record into the import_log table
-- =============================================
CREATE PROCEDURE [dbo].[CSP_InsImportLog]
(
	@Message			NVARCHAR (MAX),
	@UserID				NVARCHAR (50),
	@Module				NVARCHAR (50), 
	@FileName			NVARCHAR (500)
)	
AS
BEGIN	
	SET NOCOUNT ON;

    INSERT INTO import_logs(create_date, Message, user_id, Module, file_name)
	VALUES (GETUTCDATE(), @Message, @UserID, @Module, @FileName)
END
GO
