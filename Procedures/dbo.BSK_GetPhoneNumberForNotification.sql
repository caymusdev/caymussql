SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-12-03
-- Description:	gets the phone number for a specific notification (new text message for example) so that clicking new text popup can display conversation
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetPhoneNumberForNotification]
(
	@notification_id			INT
)
AS
BEGIN
SET NOCOUNT ON;

SELECT [from] AS phone_number
FROM texts t
WHERE t.text_id = (SELECT n.record_id FROM notifications n WHERE n.notification_id = @notification_id)

END

GO
