SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-11-20
-- Description:	Given a phone number it returns the user the call should be forwarded to and what affiliate the call is from, 
-- and a message to display to user
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetAffiliateAndCollectorForCaller]
(
	@phone_number			VARCHAR (50)
)
AS
BEGIN
SET NOCOUNT ON;
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
/*
SELECT TOP 1 c.affiliate_id, u.email, a.affiliate_name, 
	'Incoming Call from ' + c.contact_first_name + ' ' + c.contact_last_name + ' (' + a.affiliate_name + ')' AS display_message, 
	up.phone_number AS users_phone_number
FROM phone_numbers n 
INNER JOIN contacts c ON n.contact_id = c.contact_id
INNER JOIN cases ca ON c.affiliate_id = ca.affiliate_id  -- find current case to know who collector is
INNER JOIN collector_cases cc ON ca.case_id = cc.case_id AND cc.start_date <= @today AND COALESCE(cc.end_date, '2050-05-09') >= @today
INNER JOIN users u ON cc.collector_user_id = u.user_id
INNER JOIN user_presence_statuses ups ON u.presence_status = ups.user_presence_status_id
INNER JOIN affiliates a ON c.affiliate_id = a.affiliate_id
INNER JOIN user_phones up ON u.user_id = up.user_id AND up.is_primary = 1
WHERE n.phone_number = @phone_number AND u.active = 1 AND ups.status_name = 'Available'
ORDER BY ca.active DESC  -- if no active case, still find the collector
*/


SELECT TOP 1 y.affiliate_id, y.email, y.affiliate_name, y.display_message, y.users_phone_number, y.OrderBy
FROM (
	SELECT x.affiliate_id, x.email, x.affiliate_name, x.display_message, x.users_phone_number, 0 AS OrderBy
	FROM (
		SELECT TOP 1 c.affiliate_id, u.email, a.affiliate_name, 
			'Incoming Call from ' + c.contact_first_name + ' ' + c.contact_last_name + ' (' + a.affiliate_name + ')' AS display_message, 
			up.phone_number AS users_phone_number
		FROM phone_numbers n 
		INNER JOIN contacts c ON n.contact_id = c.contact_id
		INNER JOIN cases ca ON c.affiliate_id = ca.affiliate_id  -- find current case to know who collector is
		INNER JOIN collector_cases cc ON ca.case_id = cc.case_id AND cc.start_date <= @today AND COALESCE(cc.end_date, '2050-05-09') >= @today
		INNER JOIN users u ON cc.collector_user_id = u.user_id
		INNER JOIN user_presence_statuses ups ON u.presence_status = ups.user_presence_status_id
		INNER JOIN affiliates a ON c.affiliate_id = a.affiliate_id
		INNER JOIN user_phones up ON u.user_id = up.user_id AND up.is_primary = 1
		WHERE n.phone_number = @phone_number AND u.active = 1 AND ups.status_name = 'Available'
		ORDER BY ca.active DESC  -- if no active case, still find the collector
	) x
	UNION 
	SELECT c.affiliate_id, NULL, a.affiliate_name, 
		'Incoming Call from ' + c.contact_first_name + ' ' + c.contact_last_name + ' (' + a.affiliate_name + ')' AS display_message, 
		NULL AS users_phone_number, 1 AS OrderBy
	FROM phone_numbers n 
	INNER JOIN contacts c ON n.contact_id = c.contact_id
	INNER JOIN cases ca ON c.affiliate_id = ca.affiliate_id  -- find current case to know who collector is
	INNER JOIN affiliates a ON c.affiliate_id = a.affiliate_id
	WHERE n.phone_number = @phone_number 
) y 
ORDER BY y.OrderBy


END

GO
