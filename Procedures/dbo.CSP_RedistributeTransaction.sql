SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-06-12
-- Description:	This will take a single transaction and redistribute it to 1 or more transactions and then each of those will need to be applied into details
-- =============================================
CREATE PROCEDURE [dbo].[CSP_RedistributeTransaction]
(
	@trans_id				INT,
	@funding_ids			NVARCHAR (1000), -- could be a | delimited list of fundings
	@amounts				NVARCHAR (3000), -- could be a | delimited list of amounts
	@comments				NVARCHAR (MAX),   -- could be a | delimited list of comments
	@userid					NVARCHAR (100) = 'SYSTEM'
)
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''

IF (SELECT COALESCE(redistributed, 0) FROM transactions WHERE trans_id = @trans_id) = 1
	BEGIN
		RAISERROR ('This Transaction has already been redistributed.', 16, 1)
	END
ELSE
	BEGIN

		BEGIN TRANSACTION

		-- first delete any transaction details if there are any
		UPDATE transaction_details SET change_date = GETUTCDATE(), change_user = @userid WHERE trans_id = @trans_id
		DELETE FROM transaction_details WHERE trans_id = @trans_id

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR

		-- create temp table to store fundings
		IF OBJECT_ID('tempdb..#tempFundings') IS NOT NULL DROP TABLE #tempFundings
		CREATE TABLE #tempFundings (id int not null, trans_date DATETIME, funding_id INT, trans_amount MONEY, comments NVARCHAR(500), batch_id INT, record_id INT)

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR

		DECLARE @trans_date DATETIME, @batch_id INT, @trans_type_id INT
		SELECT @trans_date = t.trans_date, @batch_id = t.batch_id, @trans_type_id = t.trans_type_id
		FROM transactions t
		WHERE t.trans_id = @trans_id

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR

		IF OBJECT_ID('tempdb..#tempFundingsIDs') IS NOT NULL DROP TABLE #tempFundingsIDs
		CREATE TABLE #tempFundingsIDs (id int not null identity, item NVARCHAR (100))

		IF OBJECT_ID('tempdb..#tempAmounts') IS NOT NULL DROP TABLE #tempAmounts
		CREATE TABLE #tempAmounts (id int not null identity, item NVARCHAR (100))

		IF OBJECT_ID('tempdb..#tempComments') IS NOT NULL DROP TABLE #tempComments
		CREATE TABLE #tempComments (id int not null identity, item NVARCHAR (1000))

		INSERT INTO #tempFundingsIDs(item)
		SELECT a.value FROM string_split(@funding_ids, '|') a

		INSERT INTO #tempAmounts(item)
		SELECT a.value FROM string_split(@amounts, '|') a

		INSERT INTO #tempComments(item)
		SELECT a.value FROM string_split(@comments, '|') a

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR

		INSERT INTO #tempFundings(id, trans_date, funding_id, trans_amount, comments, batch_id, record_id)
		SELECT f.id, @trans_date, f.item, a.item, c.item, @batch_id, @trans_id
		FROM #tempFundingsIDs f
		INNER JOIN #tempAmounts a ON f.id = a.id
		INNER JOIN #tempComments c ON f.id = c.id

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR


		DECLARE @maxID INT, @cnt INT; SET @cnt = 1
		SELECT @maxID = MAX(id) FROM #tempFundings

		WHILE @cnt <= @maxID
			BEGIN
				DECLARE @funding_id INT, @trans_amount MONEY, @comment NVARCHAR (500)
				SELECT @funding_id = f.funding_id, @trans_amount = f.trans_amount, @comment = f.comments
				FROM #tempFundings f
				WHERE f.id = @cnt

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR

				--  EXEC dbo.CSP_DistributeTransaction @trans_date, @funding_id, @trans_amount, @comment, @batch_id, @trans_id, NULL, NULL, @trans_id, @trans_type_id
				EXEC dbo.CSP_DistributeTransaction_v2 @funding_id, @trans_amount, NULL, @trans_date, @comment, @batch_id, @trans_id, 'transactions', 
					@trans_id, @trans_date, NULL, NULL, @trans_type_id


				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR
		
				SET @cnt = @cnt + 1
			END

		-- now update original transaction
		UPDATE transactions SET redistributed = 1, change_date = GETUTCDATE(), change_user = @userid WHERE trans_id = @trans_id

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR


		COMMIT TRANSACTION
		GOTO Done

		ERROR:	
			ROLLBACK TRANSACTION
			RAISERROR (@ErrorMessage, 16, 1)	
	END
Done:

IF OBJECT_ID('tempdb..#tempFundings') IS NOT NULL DROP TABLE #tempFundings
IF OBJECT_ID('tempdb..#tempFundingsIDs') IS NOT NULL DROP TABLE #tempFundingsIDs
IF OBJECT_ID('tempdb..#tempAmounts') IS NOT NULL DROP TABLE #tempAmounts
IF OBJECT_ID('tempdb..#tempComments') IS NOT NULL DROP TABLE #tempComments

END
GO
