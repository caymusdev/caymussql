SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2021-01-12
-- Description: Gets a list of contact types for the dropdown
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetDDLContactTypes]
AS
BEGIN
SET NOCOUNT ON

SELECT contact_person_type_id, contact_type_desc, CASE WHEN contact_type_desc = 'Other' THEN 1 ELSE 0 END AS Sorter
FROM contact_person_types 
ORDER BY Sorter, contact_type_desc ASC

END
GO
