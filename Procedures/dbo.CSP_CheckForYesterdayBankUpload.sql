SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-11-26
-- Description:	Checks for bank batches from yesterday to have been uploaded
-- =============================================
CREATE PROCEDURE CSP_CheckForYesterdayBankUpload

AS
BEGIN
SET NOCOUNT ON;

DECLARE @today DATE, @yesterday DATE; SET @today = DATEADD(HH, -5, GETUTCDATE())
SELECT @yesterday = DATEADD(DD, CASE DATEPART(WEEKDAY, @today) WHEN 1 THEN -2 WHEN 2 THEN -3 ELSE -1 END, @today)

SELECT *
FROM boa_staging 
WHERE Date = @yesterday
END
GO
