SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-11-30
-- Description:	Checks the temp_texts table for any temp_texts not tied to a real text.  temp_texts is used when a text message is too long and 
-- need to be broken into pieces.
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetIncompleteTexts]

AS
BEGIN
SET NOCOUNT ON;


SELECT *
FROM temp_texts t
WHERE t.text_id IS NULL


END

GO
