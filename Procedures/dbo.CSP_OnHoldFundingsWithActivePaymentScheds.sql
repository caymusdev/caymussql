SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-12-20
-- Description:	Checks for Fundings that are on hold but yet have an active payment schedule
-- =============================================
CREATE PROCEDURE [dbo].[CSP_OnHoldFundingsWithActivePaymentScheds] 

AS
BEGIN
SET NOCOUNT ON;

SELECT DISTINCT f.id, f.contract_number, f.legal_name
FROM fundings f
INNER JOIN funding_payment_schedules fps ON f.id = fps.funding_id
INNER JOIN payment_schedules ps ON fps.payment_schedule_id = ps.payment_schedule_id
WHERE f.on_hold = 1 AND ps.active = 1
ORDER BY f.legal_name
END
GO
