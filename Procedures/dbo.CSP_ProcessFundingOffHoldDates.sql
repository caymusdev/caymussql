SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-02-18
-- Description:	Will run periodically and will take fundings off hold based on the date
-- =============================================
CREATE PROCEDURE [dbo].[CSP_ProcessFundingOffHoldDates]

AS
BEGIN	
SET NOCOUNT ON;

DECLARE @today DATE; SET @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SELECT @tomorrow = dbo.CF_GetTomorrow(@today)

-- need to udpate both fundings and the off hold table so store in temp what we are working with
IF OBJECT_ID('tempdb..#tempIDs') IS NOT NULL DROP TABLE #tempIDs
CREATE TABLE #tempIDs (id int not null identity (1, 1), funding_id BIGINT)

INSERT INTO #tempIDs (funding_id)
SELECT f.id
FROM funding_off_hold_dates o
INNER JOIN fundings f ON o.funding_id = f.id
WHERE o.off_hold_date = @today AND f.on_hold = 1 AND o.processed_date IS NULL
-- we now have a list of fundings that are to be taken off hold today

UPDATE o
SET processed_date = GETUTCDATE(), change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM funding_off_hold_dates o
INNER JOIN #tempIDs t ON o.funding_id =t.funding_id
WHERE o.off_hold_date = @today

UPDATE f
SET on_hold = 0, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM fundings f
INNER JOIN #tempIDs t ON f.id = t.funding_id
WHERE f.on_hold = 1

-- now need to create payments for today if there are any
INSERT INTO payments (payment_schedule_id, amount, trans_date, transaction_id, approved_flag, processed_date, change_date, change_user, previous_transaction_id,
	orig_transaction_id, is_fee, retry_level, retry_sublevel, funding_id, complete, batch_id, retry_complete, processor, is_refund, is_debit, deleted, 
	waived, redirect_funding_id, redirect_approval_id, funding_payment_date_id)
SELECT NULL, pd.amount, pd.payment_date, NULL, 1, NULL, GETUTCDATE(), 'SYSTEM', NULL, NULL, 0, 0, 0, pd.funding_id, NULL, NULL, NULL, NULL, 0, 1, 0, 
	0, NULL, NULL, pd.id
FROM funding_payment_dates pd
INNER JOIN #tempIDs t ON pd.funding_id = t.funding_id
WHERE pd.payment_date = @tomorrow AND pd.active = 1 AND pd.payment_method = 'ACH'
	-- check to make sure there isn't already a payment
	AND NOT EXISTS (SELECT 1 FROM payments p WHERE p.funding_payment_date_id = pd.id AND p.deleted = 0 AND p.waived = 0 AND p.processed_date IS NULL)


IF OBJECT_ID('tempdb..#tempIDs') IS NOT NULL DROP TABLE #tempIDs

END
GO
