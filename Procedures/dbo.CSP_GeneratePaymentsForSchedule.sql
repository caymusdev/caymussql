SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-10-12
-- Description:	Creates payments into the payments table for a specific schedule. 
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GeneratePaymentsForSchedule](
	@payment_schedule_id	INT,
	@userid					NVARCHAR (100) = NULL
)	
AS
BEGIN	

SET NOCOUNT ON;
IF @userid IS NULL SET @userid = 'SYSTEM'

DECLARE @funding_id INT, @payment_method VARCHAR (50); 
SELECT @funding_id = funding_id FROM funding_payment_schedules WHERE payment_schedule_id = @payment_schedule_id
SELECT @payment_method = payment_method FROM payment_schedules WHERE payment_schedule_id = @payment_schedule_id

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SELECT @tomorrow = DATEADD(DD, CASE DATEPART(WEEKDAY, @today) WHEN 6 THEN 3 WHEN 7 THEN 2 ELSE 1 END, @today)

-- update payment schedule if the end date has passed but are still marked active
UPDATE payment_schedules
SET active = 0, change_user = @userid, change_date = GETUTCDATE()
WHERE active = 1 AND COALESCE(end_date, '2100-01-01') < @tomorrow AND payment_schedule_id = @payment_schedule_id


IF UPPER(@payment_method) = 'ACH'
	BEGIN
		-- make sure pay off amounts are accurate
		EXEC dbo.CSP_UpdCalculationsForFunding @funding_id, @userid

		INSERT INTO payments (payment_schedule_id, amount, trans_date, transaction_id, approved_flag, processed_date, change_user, change_date, funding_id)
		SELECT y.payment_schedule_id, y.amount,
			-- if it is less than today, change to today
			CASE WHEN
				-- adjust for weekends only (not holidays)
				CASE DATEPART(WEEKDAY, y.NextPayment) 
					WHEN 7 THEN DATEADD(D, 2, y.NextPayment)
					WHEN 1 THEN DATEADD(D, 1, y.NextPayment)
					ELSE y.NextPayment END < @tomorrow THEN @tomorrow 
				ELSE
					CASE DATEPART(WEEKDAY, y.NextPayment) 
					WHEN 7 THEN DATEADD(D, 2, y.NextPayment)
					WHEN 1 THEN DATEADD(D, 1, y.NextPayment)
					ELSE y.NextPayment END
				END	AS NextPayment, 
			NULL, 1, NULL, @userid, GETUTCDATE(), @funding_id
		FROM (
			SELECT CASE WHEN x.last_payment_date IS NULL THEN COALESCE(s.start_date, GETDATE())
				ELSE DATEADD(D, CASE s.frequency WHEN 'daily' THEN 1 WHEN 'weekly' THEN 7  WHEN 'monthly' THEN 30 END, CONVERT(DATE, x.last_payment_date)) END AS NextPayment, 
				s.payment_schedule_id, 
				CASE WHEN f.payoff_amount < s.amount THEN f.payoff_amount ELSE s.amount END AS amount, 
				s.routing_number, s.account_number, s.payment_processor, f.contract_number, f.legal_name
			FROM payment_schedules s
			INNER JOIN funding_payment_schedules fs ON s.payment_schedule_id = fs.payment_schedule_id
			INNER JOIN fundings f ON fs.funding_id = f.id
			LEFT JOIN (
				SELECT MAX(trans_date) AS last_payment_date, payment_schedule_id
				FROM payments p
				WHERE p.processed_date IS NOT NULL
				GROUP BY p.payment_schedule_id
			) x ON s.payment_schedule_id = x.payment_schedule_id
			WHERE f.contract_status IN (1,3,4) AND COALESCE(f.on_hold, 0) = 0 AND f.payoff_amount > 0
				AND s.active = 1 AND s.payment_schedule_id = @payment_schedule_id
		) y
		WHERE NOT EXISTS (SELECT 1 FROM payments p2 WHERE p2.trans_date = 
				CASE WHEN		
				CASE DATEPART(WEEKDAY, y.NextPayment) 
					WHEN 7 THEN DATEADD(D, 2, y.NextPayment)
					WHEN 1 THEN DATEADD(D, 1, y.NextPayment)
					ELSE y.NextPayment END < @tomorrow THEN @tomorrow 
				ELSE
					CASE DATEPART(WEEKDAY, y.NextPayment) 
					WHEN 7 THEN DATEADD(D, 2, y.NextPayment)
					WHEN 1 THEN DATEADD(D, 1, y.NextPayment)
					ELSE y.NextPayment END
				END
					AND p2.payment_schedule_id = y.payment_schedule_id
					AND p2.deleted = 0 AND p2.waived = 0)
			AND y.amount IS NOT NULL
				AND CASE WHEN
				-- adjust for weekends only (not holidays)
				CASE DATEPART(WEEKDAY, y.NextPayment) 
					WHEN 7 THEN DATEADD(D, 2, y.NextPayment)
					WHEN 1 THEN DATEADD(D, 1, y.NextPayment)
					ELSE y.NextPayment END < @tomorrow THEN @tomorrow 
				ELSE
					CASE DATEPART(WEEKDAY, y.NextPayment) 
					WHEN 7 THEN DATEADD(D, 2, y.NextPayment)
					WHEN 1 THEN DATEADD(D, 1, y.NextPayment)
					ELSE y.NextPayment END
				END	<= @tomorrow
	END
END
GO
