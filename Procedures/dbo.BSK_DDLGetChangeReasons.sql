SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2021-02-25
-- Description: Gets the list of change reasons for the dropdown on payments
-- =============================================
CREATE PROCEDURE [dbo].[BSK_DDLGetChangeReasons]
AS

SET NOCOUNT ON

SELECT x.id, x.reason_text, x.reason, x.reason_parent_id
FROM (
	SELECT r2.id, r.reason, r.reason_parent_id, r.reason + ': ' + r2.reason AS reason_text
	FROM payment_schedule_change_reasons r
	LEFT JOIN payment_schedule_change_reasons r2 ON r.id = r2.reason_parent_id
) x
WHERE x.reason_text IS NOT NULL
UNION ALL
SELECT -1, '', '', NULL
ORDER BY reason_text

GO
