SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-05-31
-- Description:	Takes a forte_staging id and will create transactions for it
-- =============================================
CREATE PROCEDURE [dbo].[CSP_ProcessForteStaging]
(
	@forte_staging_id			INT
)

AS
BEGIN
SET NOCOUNT ON;

DECLARE @funding_id INT;

DECLARE @contract_number NVARCHAR (100), @trans_date DATETIME, @batch_id INT, @record_id INT, @transaction_id NVARCHAR (100)
DECLARE @response_code NVARCHAR (50), @settle_amount MONEY, @settle_type NVARCHAR (50), @settle_date DATETIME

-- first need to get the funding for the settlement
SELECT @contract_number = s.[Consumer ID], @trans_date = s.[Origination Date], @response_code = s.[Settled Response Code], 
	@settle_amount = CASE WHEN s.Status = 'Rejected' THEN 0 ELSE s.[Total Amount] END, -- the whole dollar amount still gets put into the file.  Does not happen this way in API, just in downloaded file.
	@settle_type = s.Status, @batch_id = s.BatchID, @record_id = s.forte_staging_id, 
	@transaction_id = s.[Transaction ID], @settle_date = s.[Settle Date]
FROM forte_staging s 
WHERE s.forte_staging_id = @forte_staging_id


EXEC @funding_id = dbo.CSP_GetFundingIDForTransaction @trans_date, @contract_number, NULL, NULL
IF @funding_id = -1 SET @funding_id = NULL

DECLARE @comments NVARCHAR (500); SET @comments = @response_code + ' : ' + @settle_type
--EXEC dbo.CSP_DistributeTransaction @trans_date, @funding_id, @settle_amount, @comments, @batch_id, @record_id, @response_code, @settle_type

EXEC dbo.CSP_DistributeTransaction_v2 @funding_id, @settle_amount, @transaction_id, @trans_date, @comments, @batch_id, @record_id, 'forte_staging', 
	NULL, @settle_date, @settle_type, @response_code

END
GO
