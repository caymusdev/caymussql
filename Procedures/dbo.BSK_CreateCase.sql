SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-16
-- Description:	Checks for affiliates with no cases and creates them
-- =============================================
CREATE PROCEDURE [dbo].[BSK_CreateCase]
(
	@funding_id					BIGINT,
	@affiliate_id				BIGINT
)
AS
BEGIN
SET NOCOUNT ON;

IF @affiliate_id IS NULL
	BEGIN
		SELECT @affiliate_id = f.affiliate_id
		FROM fundings f
		WHERE f.id = @funding_id
	END

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())

IF OBJECT_ID('tempdb..#tempUsers') IS NOT NULL DROP TABLE #tempUsers
CREATE TABLE #tempUsers (id INT IDENTITY (1, 1), user_id INT, num_cases INT)


-- get a count of assigned cases for each collector that is a Customer Service
INSERT INTO #tempUsers (user_id, num_cases)
SELECT ud.user_id, SUM(CASE WHEN c.case_id IS NULL THEN 0 ELSE 1 END) AS num_cases
FROM user_departments ud
INNER JOIN departments d ON ud.department_id = d.department_id
LEFT JOIN collector_cases cc ON ud.user_id = cc.collector_user_id AND cc.start_date <= @today AND COALESCE(cc.end_date, '2050-05-09') >= @today
LEFT JOIN cases c ON cc.case_id = c.case_id AND c.active = 1
WHERE d.department = 'CustomerService' 
GROUP BY ud.user_id
ORDER BY num_cases 

DECLARE @tempUserID INT, @newCaseID INT

DECLARE @department_id INT, @case_status_id INT, @bucket_status_id INT
SELECT @department_id = department_id FROM departments WHERE department = 'CustomerService'
SELECT @case_status_id = case_status_id FROM case_statuses WHERE status = 'Active'
SELECT @bucket_status_id = bucket_status_id FROM bucket_statuses WHERE status = 'InProcess'

SELECT TOP 1 @tempUserID = user_id
FROM #tempUsers
ORDER BY num_cases

INSERT INTO cases(affiliate_id, create_date, change_date, create_user, change_user, active, department_id, case_status_id, bucket_status_id, new_case, collection_date)
SELECT @affiliate_id, GETUTCDATE(), GETUTCDATE(), 'System', 'System', 1, @department_id, @case_status_id, @bucket_status_id, 1, GETUTCDATE()

SELECT @newCaseID = SCOPE_IDENTITY()

INSERT INTO collector_cases (collector_user_id, case_id, start_date, end_date, create_date, change_date, change_user, create_user)
VALUES (@tempUserID, @newCaseID, @today, NULL, GETUTCDATE(), GETUTCDATE(), 'SystemCreateCase', 'SystemCreateCase')

INSERT INTO case_departments (case_id, department_id, start_date, end_date, create_date, change_date, change_user, create_user, do_not_move, do_not_move_by, next_move_on, next_move_to)
VALUES (@newCaseID, @department_id, @today, NULL, GETUTCDATE(), GETUTCDATE(), 'SystemCreateCase', 'SystemCreateCase', NULL, NULL, NULL, NULL)

-- create a notification for the new case
DECLARE @affiliate_name NVARCHAR (100)
SELECT @affiliate_name = 'New Case for ' + affiliate_name FROM affiliates WHERE affiliate_id = @affiliate_id

EXEC dbo.BSK_InsNotification @tempUserID, NULL, @affiliate_id, NULL, NULL, @affiliate_name, 'NewCase', 'System', @newCaseID

			
END

GO
