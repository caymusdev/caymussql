SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-09-21
-- Description:	Inserts a funding bonus from SOLO if it does not exist
-- =============================================
CREATE PROCEDURE [dbo].[CSP_SyncFundingBonus]
(
	@funding_id				[BIGINT],
	@iso_id					[BIGINT],
	@bonus_id				[BIGINT],
	@iso_bonus_id			[BIGINT] = NULL,
	@bonus_percent_level_1	[NUMERIC](16, 4) = NULL, 
	@bonus_percent_level_2	[NUMERIC](16, 4) = NULL,
	@bonus_percent_level_3	[NUMERIC](16, 4) = NULL,
	@bonus_percent_level_4	[NUMERIC](16, 4) = NULL,
	@bonus_percent_level_5	[NUMERIC](16, 4) = NULL,
	@bonus_amount_level_1	[NUMERIC](16, 4) = NULL,
	@bonus_amount_level_2	[NUMERIC](16, 4) = NULL,
	@bonus_amount_level_3	[NUMERIC](16, 4) = NULL,
	@bonus_amount_level_4	[NUMERIC](16, 4) = NULL,
	@bonus_amount_level_5	[NUMERIC](16, 4) = NULL,
	@iso_name				[NVARCHAR] (100),
	@bonus_template_id		[BIGINT] = NULL,
	@bonus_metric			[VARCHAR] (50) = NULL,
	@bonus_filter			[VARCHAR] (50) = NULL
)
AS
BEGIN
SET NOCOUNT ON;


IF NOT EXISTS(SELECT 1 FROM funding_bonuses WHERE funding_id = @funding_id AND iso_id = @iso_id)
	BEGIN
		INSERT INTO funding_bonuses(funding_bonus_id, funding_id, bonus_percent_level_1, bonus_percent_level_2, bonus_percent_level_3, bonus_percent_level_4, 
			bonus_percent_level_5, bonus_amount_level_1, bonus_amount_level_2, bonus_amount_level_3, bonus_amount_level_4, bonus_amount_level_5, iso_name, 
			iso_id, bonus_template_id, iso_bonus_id, create_date, change_date, change_user, create_user, bonus_metric, bonus_filter)
		VALUES(@bonus_id, @funding_id, @bonus_percent_level_1, @bonus_percent_level_2, @bonus_percent_level_3, @bonus_percent_level_4, @bonus_percent_level_5, 
			@bonus_amount_level_1, @bonus_amount_level_2, @bonus_amount_level_3, @bonus_amount_level_4, @bonus_amount_level_5, @iso_name, @iso_id, 
			@bonus_template_id, @iso_bonus_id, GETUTCDATE(), GETUTCDATE(), 'SYSTEM', 'SYSTEM', @bonus_metric, @bonus_filter)

		UPDATE fundings
		SET funding_bonus_id = @bonus_id
		WHERE id = @funding_id
	END
/*  Not doing now.  Should never be an update.   Once it is funded in SOLO this data should never change so once we have it in Portfolio, it stays that way.
ELSE
	BEGIN

		
	END
*/

END

GO
