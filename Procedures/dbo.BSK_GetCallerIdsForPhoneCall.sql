SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Rick Pina
-- Create date: 2021-03-16
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetCallerIdsForPhoneCall]
(
	@contact_id			INT
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @state NVARCHAR(50)
SELECT TOP 1 @state = state
FROM addresses a WITH (NOLOCK)
INNER JOIN contacts c WITH (NOLOCK) ON a.contact_id = c.contact_id
LEFT JOIN address_types t WITH (NOLOCK) ON a.address_type_id = t.address_type_id
WHERE c.contact_id = @contact_id
ORDER BY t.address_type_id  -- physcial happens to be first

SET @state = COALESCE(@state, '')

SELECT TOP 10 phone_number, phone_number_display FROM
(
	SELECT phone_number, phone_number + COALESCE(' - ' + state_abbrev, '') AS phone_number_display, 0 AS OrderBy 
	FROM caymus_phone_numbers
	WHERE state = @state OR state_abbrev = @state
	UNION 
	SELECT phone_number, phone_number + COALESCE(' - ' + state_abbrev, '') AS phone_number_display, 1 AS OrderBy 
	FROM caymus_phone_numbers 
	WHERE state != @state AND state_abbrev != @state
)
x ORDER BY x.OrderBy

END

GO
