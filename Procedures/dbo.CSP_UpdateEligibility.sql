SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2022-04-25
-- Description:	updates the eligibility of each funding record and the portfolio
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdateEligibility]
AS
BEGIN
SET NOCOUNT ON;

-- if rules change here, also update [CSP_IsFundingEligible]

DECLARE @today DATETIME; SET @today = CONVERT(DATE, DATEADD(HH, -5, GETUTCDATE()))

INSERT INTO funding_eligibility(funding_id, application_id, ineligible_date, estimated_turn, estimated_turn_max, estimated_turn_eligible, purchase_price, purchase_price_max, 
	purchase_price_eligible, first_position, first_position_requirement, first_position_eligible, pricing_ratio, pricing_ratio_min, pricing_ratio_eligible, frequency, 
	frequency_requirement, frequency_eligible, first_payment, first_payment_requirement, first_payment_eligible, last_paymemt, last_paymemt_requirement, 
	last_paymemt_eligible, collections, collections_requirement, collections_eligible, percent_collected, percent_collected_minimum, percent_collected_eligible, 
	create_date, change_date, change_user, create_user)
SELECT f.id, NULL AS application_id, NULL AS ineligible_date, f.estimated_turn, 15, CASE WHEN f.estimated_turn <= 15 THEN 1 ELSE 0 END,
	f.purchase_price, 300000, CASE WHEN f.purchase_price <= 300000 THEN 1 ELSE 0 END AS purchase_price_eligibile,
	f.first_position, 1, CASE WHEN f.first_position = 1 THEN 1 ELSE 0 END AS first_position_eligibile,
	f.pricing_ratio, 1.2, CASE WHEN f.pricing_ratio >= 1.2 THEN 1 ELSE 0 END AS pricing_ratio_eligible,
	f.frequency, 'Daily or Weekly', CASE WHEN f.frequency IN ('Daily', 'Weekly') THEN 1 ELSE 0 END AS frequency_eligible,
	f.first_received_date, COALESCE(f.funded_date, @today) + 7, CASE WHEN DATEDIFF(DD, COALESCE(f.funded_date, @today), COALESCE(f.first_received_date, @today)) <= 7 THEN 1 ELSE 0 
		END AS first_payment_eligible,
	f.last_payment_date, @today - 30, CASE WHEN DATEDIFF(DD, COALESCE(f.last_payment_date, COALESCE(f.funded_date, @today)), @today) <= 30 THEN 1 ELSE 0 END AS last_paymemt_eligible,
	CASE WHEN f.collection_type IN ('Collections') THEN 1 ELSE 0 END, 0, CASE WHEN COALESCE(f.collection_type, '') NOT IN ('Collections') THEN 1 ELSE 0 END AS collections_eligibile,	
	CASE WHEN DATEDIFF(DD, COALESCE(f.funded_date, @today), @today) > 60
		THEN CONVERT(NUMERIC(10,2), COALESCE(f.cash_to_rtr, 0) / COALESCE(COALESCE(x.original_expected, 0) + COALESCE(x2.payment_plan_expected, 0), 0.001)) * 100.00
		ELSE 0 END AS percent_collected, .50 AS percent_collected_minimum,
	CASE WHEN DATEDIFF(DD, COALESCE(f.funded_date, @today), @today) > 60
		THEN CASE 
			WHEN COALESCE(f.cash_to_rtr, 0) / COALESCE(COALESCE(x.original_expected, 0) + COALESCE(x2.payment_plan_expected, 0), 0.001) >= .50 THEN 1 ELSE 0 END
		ELSE 1 END AS percent_collected_eligible,
	GETUTCDATE(), GETUTCDATE(), 'SYSTEM', 'SYSTEM'
FROM fundings f
LEFT JOIN (
	-- this gets all expected payments using the original payments up until either the start of a payment plan or today
	SELECT funding_id, SUM(amount) AS original_expected
	FROM funding_payment_dates_original o
	WHERE payment_date <= 
		COALESCE((SELECT TOP 1 pp.start_date
		 FROM funding_payment_dates pd
		 INNER JOIN payment_plans pp ON pd.payment_plan_id = pp.payment_plan_id
		 WHERE pd.active = 1 AND pp.payment_plan_type = 'Payment Plan' AND pp.deleted = 0 AND pd.payment_date <= @today
			AND pd.funding_id = o.funding_id), @today)
	GROUP BY o.funding_id 
) x ON f.id = x.funding_id
LEFT JOIN (
	-- this gets all expected payments between the payment plan start and today
	SELECT pd.funding_id, SUM(pd.amount) AS payment_plan_expected
	FROM funding_payment_dates pd
	INNER JOIN payment_plans pp ON pd.payment_plan_id = pp.payment_plan_id
	WHERE pd.active = 1 AND pp.payment_plan_type = 'Payment Plan' AND pp.deleted = 0
		AND pd.payment_date BETWEEN pp.start_date AND @today
	GROUP BY pd.funding_id 	
	) x2 ON f.id = x2.funding_id
WHERE f.contract_status = 1 AND f.portfolio = 'spv'
	AND NOT EXISTS (SELECT * FROM funding_eligibility f2 WHERE f2.funding_id = f.id)



UPDATE fe
SET first_payment = f.first_received_date,
	first_payment_eligible = CASE WHEN DATEDIFF(DD, COALESCE(f.funded_date, @today), COALESCE(f.first_received_date, @today)) <= 7 THEN 1 ELSE 0 END,
	last_paymemt = f.last_payment_date,
	last_paymemt_eligible = CASE WHEN DATEDIFF(DD, COALESCE(f.last_payment_date, COALESCE(f.funded_date, @today)), @today) <= 30 THEN 1 ELSE 0 END,
	collections = CASE WHEN f.collection_type IN ('Collections') THEN 1 ELSE 0 END, 
	collections_eligible = CASE WHEN f.collection_type NOT IN ('Collections') THEN 1 ELSE 0 END, 
	percent_collected = CASE WHEN DATEDIFF(DD, COALESCE(f.funded_date, @today), @today) > 60
		THEN CONVERT(NUMERIC(10,2), COALESCE(f.cash_to_rtr, 0) / COALESCE(COALESCE(x.original_expected, 0) + COALESCE(x2.payment_plan_expected, 0), 0.001)) * 100.00
		ELSE 0 END, 
	percent_collected_eligible = CASE WHEN DATEDIFF(DD, COALESCE(f.funded_date, @today), @today) > 60
		THEN CASE 
			WHEN COALESCE(f.cash_to_rtr, 0) / COALESCE(COALESCE(x.original_expected, 0) + COALESCE(x2.payment_plan_expected, 0), 0.001) >= .50 THEN 1 ELSE 0 END
		ELSE 1 END,
	change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM funding_eligibility fe
INNER JOIN fundings f ON fe.funding_id = f.id
LEFT JOIN (
	-- this gets all expected payments using the original payments up until either the start of a payment plan or today
	SELECT funding_id, SUM(amount) AS original_expected
	FROM funding_payment_dates_original o
	WHERE payment_date <= 
		COALESCE((SELECT TOP 1 pp.start_date
		 FROM funding_payment_dates pd
		 INNER JOIN payment_plans pp ON pd.payment_plan_id = pp.payment_plan_id
		 WHERE pd.active = 1 AND pp.payment_plan_type = 'Payment Plan' AND pp.deleted = 0 AND pd.payment_date <= @today
			AND pd.funding_id = o.funding_id), @today)
	GROUP BY o.funding_id 
) x ON f.id = x.funding_id
LEFT JOIN (
	-- this gets all expected payments between the payment plan start and today
	SELECT pd.funding_id, SUM(pd.amount) AS payment_plan_expected
	FROM funding_payment_dates pd
	INNER JOIN payment_plans pp ON pd.payment_plan_id = pp.payment_plan_id
	WHERE pd.active = 1 AND pp.payment_plan_type = 'Payment Plan' AND pp.deleted = 0
		AND pd.payment_date BETWEEN pp.start_date AND @today
	GROUP BY pd.funding_id 	
	) x2 ON f.id = x2.funding_id
WHERE f.contract_status = 1 AND f.portfolio = 'spv'
	AND 
	(
		fe.first_payment != f.first_received_date
		OR fe.first_payment_eligible != CASE WHEN DATEDIFF(DD, COALESCE(f.funded_date, @today), COALESCE(f.first_received_date, @today)) <= 7 THEN 1 ELSE 0 END
		OR fe.last_paymemt != f.last_payment_date
		OR fe.last_paymemt_eligible != CASE WHEN DATEDIFF(DD, COALESCE(f.last_payment_date, COALESCE(f.funded_date, @today)), @today) <= 30 THEN 1 ELSE 0 END
		OR fe.collections != CASE WHEN f.collection_type IN ('Collections') THEN 1 ELSE 0 END
		OR fe.collections_eligible != CASE WHEN f.collection_type NOT IN ('Collections') THEN 1 ELSE 0 END
		OR fe.percent_collected != CASE WHEN DATEDIFF(DD, COALESCE(f.funded_date, @today), @today) > 60
			THEN CONVERT(NUMERIC(10,2), COALESCE(f.cash_to_rtr, 0) / COALESCE(COALESCE(x.original_expected, 0) + COALESCE(x2.payment_plan_expected, 0), 0.001)) * 100.00
			ELSE 0 END
		OR fe.percent_collected_eligible != CASE WHEN DATEDIFF(DD, COALESCE(f.funded_date, @today), @today) > 60
			THEN CASE 
				WHEN COALESCE(f.cash_to_rtr, 0) / COALESCE(COALESCE(x.original_expected, 0) + COALESCE(x2.payment_plan_expected, 0), 0.001) >= .50 THEN 1 ELSE 0 END
			ELSE 1 END
	)


-- now do the whole portfolio
/*
SELECT f.incorp_state, COUNT(f.incorp_state) AS total_number_in_state, 
	CONVERT(NUMERIC(10, 2), CONVERT(NUMERIC(10, 2), COUNT(f.incorp_state)) * 100.00 / CONVERT(NUMERIC(10, 2), (SELECT COUNT(*) FROM fundings WHERE contract_status = 1))) 
		AS percent_in_state
FROM fundings f
WHERE f.contract_status = 1
GROUP BY f.incorp_state
ORDER BY percent_in_state DESC
*/

DECLARE @incorp_state_percent NUMERIC (10, 2), @incorp_state VARCHAR (50)
SELECT TOP 1 @incorp_state = f.incorp_state, @incorp_state_percent = 
	CONVERT(NUMERIC(10, 2), CONVERT(NUMERIC(10, 2), COUNT(f.incorp_state)) * 100.00 / CONVERT(NUMERIC(10, 2), (SELECT COUNT(*) FROM fundings 
		WHERE contract_status = 1 AND portfolio = 'spv'))) 
FROM fundings f
WHERE f.contract_status = 1 AND f.portfolio = 'spv'
GROUP BY f.incorp_state
ORDER BY COUNT(f.incorp_state) DESC

--  SELECT @incorp_state, @incorp_state_percent


/*
SELECT f.industry, COUNT(f.industry) AS total_number_in_industry, 
	CONVERT(NUMERIC(10, 2), CONVERT(NUMERIC(10, 2), COUNT(f.industry)) * 100.00 / CONVERT(NUMERIC(10, 2), (SELECT COUNT(*) FROM fundings WHERE contract_status = 1)))
		AS percent_in_industry
FROM fundings f
WHERE f.contract_status = 1
GROUP BY f.industry
ORDER BY percent_in_industry DESC
*/


DECLARE @industry_percent NUMERIC (10, 2), @industry VARCHAR (50)
SELECT TOP 1 @industry = f.industry, @industry_percent = 
	CONVERT(NUMERIC(10, 2), CONVERT(NUMERIC(10, 2), COUNT(f.industry)) * 100.00 / CONVERT(NUMERIC(10, 2), (SELECT COUNT(*) FROM fundings 
		WHERE contract_status = 1 AND portfolio = 'spv')+.00001))
FROM fundings f
WHERE f.contract_status = 1 AND f.portfolio = 'spv'
GROUP BY f.industry
ORDER BY COUNT(f.industry) DESC

--  SELECT @industry, @industry_percent

/*
SELECT AVG(payoff_amount)
FROM fundings f
WHERE f.contract_status = 1
*/

DECLARE @avg_balance MONEY
SELECT @avg_balance = AVG(payoff_amount)
FROM fundings f
WHERE f.contract_status = 1 AND f.portfolio = 'spv'

--  SELECT @avg_balance


/*
SELECT CONVERT(NUMERIC(10, 2), (((SELECT COUNT(*) FROM fundings f WHERE f.contract_status = 1 AND f.est_months_left > 12) * 100.00) 
	/ (SELECT COUNT(*) FROM fundings WHERE contract_status = 1))) AS percent_longer_than_12_month_remaining
*/
DECLARE @remaining_term NUMERIC (10, 2)
SELECT @remaining_term = CONVERT(NUMERIC(10, 2), (((SELECT COUNT(*) FROM fundings f WHERE f.contract_status = 1 AND f.est_months_left > 12 AND f.portfolio = 'spv') * 100.00) 
	/ ((SELECT COUNT(*) FROM fundings WHERE contract_status = 1 AND portfolio = 'spv')+0.00001)))

--  SELECT @remaining_term



INSERT INTO portfolio_eligibility(incorp_state_percent, incorp_state, incorp_state_percent_max, incorp_state_eligible, industry_percent, industry, industry_percent_max, 
	industry_eligible, avg_balance, avg_balance_max, avg_balance_eligible, remaining_term, remaining_term_max, remaining_term_eligible, portfolio_date, create_date, 
	change_date, change_user, create_user)
SELECT @incorp_state_percent, @incorp_state, 20.00, CASE WHEN @incorp_state_percent <= 20.00 THEN 1 ELSE 0 END,
	@industry_percent, @industry, 35.00, CASE WHEN @industry_percent <= 35.00 THEN 1 ELSE 0 END,
	@avg_balance, 100000.00, CASE WHEN @avg_balance <= 100000.00 THEN 1 ELSE 0 END,
	@remaining_term, 50.00, CASE WHEN @remaining_term <= 50.00 THEN 1 ELSE 0 END,
	@today, GETUTCDATE(), GETUTCDATE(), 'SYSTEM', 'SYSTEM'
WHERE NOT EXISTS (SELECT * FROM portfolio_eligibility WHERE portfolio_date = @today)


UPDATE pe
SET incorp_state_percent = @incorp_state_percent, incorp_state = @incorp_state, incorp_state_percent_max = 20.00,
	incorp_state_eligible = CASE WHEN @incorp_state_percent <= 20.00 THEN 1 ELSE 0 END,
	industry_percent = @industry_percent, industry = @industry, industry_percent_max = 35.00, 
	industry_eligible = CASE WHEN @industry_percent <= 35.00 THEN 1 ELSE 0 END,
	avg_balance = @avg_balance, avg_balance_max = 100000.00, avg_balance_eligible = CASE WHEN @avg_balance <= 100000.00 THEN 1 ELSE 0 END,
	remaining_term = @remaining_term, remaining_term_max = 50.00, remaining_term_eligible = CASE WHEN @remaining_term <= 50.00 THEN 1 ELSE 0 END,
	change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM portfolio_eligibility pe
WHERE pe.portfolio_date = @today
	AND
	(	
		pe.incorp_state_percent != @incorp_state_percent
		OR pe.incorp_state != @incorp_state
		OR pe.incorp_state_percent_max != 20.00
		OR pe.incorp_state_eligible != CASE WHEN @incorp_state_percent <= 20.00 THEN 1 ELSE 0 END
		OR pe.industry_percent != @industry_percent
		OR pe.industry != @industry
		OR pe.industry_percent_max != 35.00
		OR pe.industry_eligible != CASE WHEN @industry_percent <= 35.00 THEN 1 ELSE 0 END
		OR pe.avg_balance != @avg_balance
		OR pe.avg_balance_max != 100000.00
		OR pe.avg_balance_eligible != CASE WHEN @avg_balance <= 100000.00 THEN 1 ELSE 0 END
		OR pe.remaining_term != @remaining_term
		OR pe.remaining_term_max != 50.00
		OR pe.remaining_term_eligible != CASE WHEN @remaining_term <= 50.00 THEN 1 ELSE 0 END
	)

-- now update any fundings and change portfolio to caymus if they do not qualify anymore

UPDATE f
SET portfolio = 'caymus', ineligible_date = @today, eligible = 0, change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM fundings f
INNER JOIN funding_eligibility fe ON f.id = fe.funding_id
WHERE f.portfolio = 'spv' --AND f.contract_status = 1
	AND (
		fe.estimated_turn_eligible = 0
		OR fe.purchase_price_eligible = 0
		OR fe.first_position_eligible = 0
		OR fe.pricing_ratio_eligible = 0
		OR fe.frequency_eligible = 0
		OR fe.first_payment_eligible = 0
		OR fe.last_paymemt_eligible = 0
		OR fe.collections_eligible = 0
		OR fe.percent_collected_eligible = 0		
	)
	AND (f.ineligible_date IS NULL OR COALESCE(f.eligible, 1) = 1)

-- now, return list of fundings that are no longer eligible so we can send out an email
SELECT f.legal_name, f.contract_number, CONVERT(DATE, first_payment) AS first_payment, CONVERT(DATE, first_payment_requirement) AS first_payment_requirement, first_payment_eligible, 
	CONVERT(DATE, last_paymemt) AS last_payment, CONVERT(DATE, last_paymemt_requirement) AS last_paymemt_requirement, last_paymemt_eligible, 
	collections, collections_requirement, collections_eligible, percent_collected, percent_collected_minimum, percent_collected_eligible
FROM fundings f
LEFT JOIN funding_eligibility fe ON f.id = fe.funding_id
WHERE f.ineligible_date = @today AND portfolio = 'caymus'
ORDER BY f.legal_name


END
GO
