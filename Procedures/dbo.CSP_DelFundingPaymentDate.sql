SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-02-04
-- Description:	Deletes a funding payment date from the work table
-- =============================================
CREATE PROCEDURE [dbo].[CSP_DelFundingPaymentDate]
(
	@worker_id				INT,
	--@session_id				NVARCHAR (100),
	@userid					NVARCHAR (100),
	@comments				NVARCHAR (4000),
	@change_reason_id		INT
)
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @approved BIT; SET @approved = 1

IF EXISTS (SELECT 1 FROM sec_payment_schedules s WHERE s.submitter_user_id = @userid)
	BEGIN
		SET @approved = 0
	END

UPDATE funding_payment_dates_work
SET active = 0, change_date = GETUTCDATE(), change_user = @userid, changed = 1, comments = @comments, change_reason_id = @change_reason_id
WHERE id = @worker_id --AND session_id = @session_id

IF @approved = 1
BEGIN
	EXEC dbo.CSP_ApprovePaymentDateChange @worker_id, @userid
END

END
GO
