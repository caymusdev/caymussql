SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Rick Pina
-- Create Date: 2021-03-24
-- Description: 
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsAddress]
(
	@address_1				NVARCHAR (100),
	@address_2				NVARCHAR (100),
	@city					NVARCHAR (50),
	@state					NVARCHAR (50),
	@postal_code			NVARCHAR (50),
	@contact_id					INT,
	@affiliate_id					INT,
	@address_type_id			INT,
	@create_user				NVARCHAR (100)
)
AS

SET NOCOUNT ON

	INSERT INTO addresses(affiliate_id, address_1, address_2, city, state, state_abbrev, postal_code, contact_id, address_type_id, create_date, change_date, change_user, create_user)
	Values (@affiliate_id, @address_1, @address_2, @city, @state, (SELECT state_code FROM states WHERE state_name = @state), @postal_code, @contact_id, @address_type_id, GETUTCDATE(), GETUTCDATE(), @create_user, @create_user)

GO
