SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-05-03
-- Description:	Gets the last run time for promise payments
-- =============================================
CREATE PROCEDURE CSP_GetLastRunTimeForPromisePayments

AS
BEGIN
SET NOCOUNT ON;

SELECT COALESCE(MAX(create_date), '2019-05-01') AS last_run_date 
FROM import_logs 
WHERE module = 'Collections' AND file_name = 'PromisePayments'

END
GO
