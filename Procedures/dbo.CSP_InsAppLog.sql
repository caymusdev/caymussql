SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-04-20
-- Description:	Inserts records into the app_log table
-- =============================================
CREATE PROCEDURE [dbo].[CSP_InsAppLog] 
(	
	@Message		NVARCHAR(MAX) NULL,
	@UserID			NVARCHAR(50) NULL,
	@Module			NVARCHAR(50) NULL
)
AS
BEGIN
	INSERT INTO app_logs(create_date, message, user_id, module)
	VALUES	(GETUTCDATE(), @Message, @UserID, @Module )
END
GO
