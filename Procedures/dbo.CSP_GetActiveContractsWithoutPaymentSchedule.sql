SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-06-14
-- Description:	Gets a list of Active contracts WITHOUT an Active payment schedule
-- UPDATE 2020-02-14: Will not look for active contracs with no active payments within the next week
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetActiveContractsWithoutPaymentSchedule]
	
AS
BEGIN	
SET NOCOUNT ON;

SELECT f.id AS funding_id, f.contract_number, f.legal_name, f.contract_type, f.funded_date, f.last_payment_date
FROM fundings f
WHERE f.contract_status = 1 AND COALESCE(f.on_hold, 0) = 0 AND NULLIF(f.processor, '') IS NOT NULL
	AND NOT EXISTS 		
		( SELECT 1
		FROM funding_payment_dates pd
		WHERE pd.funding_id = f.id AND COALESCE(pd.active, 0) = 1 AND DATEDIFF(DD, GETUTCDATE(),  pd.payment_date) BETWEEN 0 AND 8
			AND pd.payment_method = 'ACH'
		)
	AND f.hard_offer_type = 'ACH'
	AND COALESCE(f.payoff_amount, 1) > 0
ORDER BY f.legal_name


END
GO
