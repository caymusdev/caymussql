SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-05-07
-- Description:	Deactivates all payment schedules for a funding.  Usually used when manually editing a funding and putting it on hold.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_DeactivateFundingPaymentSchedules]
(	
	@funding_id			BIGINT
)
AS
BEGIN	
SET NOCOUNT ON;

UPDATE ps
SET active = 0
FROM payment_schedules ps 
INNER JOIN funding_payment_schedules fps ON ps.payment_schedule_id = fps.payment_schedule_id
WHERE fps.funding_id = @funding_id AND ps.active = 1

-- delete any retry payments that have not been sent for closed payment schedules
UPDATE payments
SET deleted = 1, change_user = 'SYSTEM', change_date = GETUTCDATE()
WHERE funding_id = @funding_id AND processed_date IS NULL AND deleted = 0
	AND COALESCE(retry_level, 0) <> 0

END
GO
