SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-12-03
-- Description:	returns the number of notifications the user has not looked at yet
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetNumberOfAlertsForUser]
(
	@user_email			NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @user_id INT
SELECT @user_id = user_id FROM users WHERE email = @user_email
/*
SELECT COUNT(*) AS NumAlerts
FROM notifications n
WHERE n.user_id = @user_id AND n.acted_on_time IS NULL
*/

SELECT COUNT(*) AS NumAlerts
FROM (
	SELECT * 
	FROM dbo.BF_GetAlerts(@user_email, 0)
) x

END

GO
