SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-02-27
-- Description:	Used when renewing a contract.  Pays off old one.  Used AFTER a funding has been added AND it's payoffs
-- =============================================
CREATE PROCEDURE [dbo].[CSP_RenewContract]
(
	@new_funding_id				BIGINT
)
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
DECLARE @trans_amount MONEY, @transaction_id NVARCHAR (100), @trans_date DATETIME, @comments NVARCHAR (500), @batch_id INT, @record_id INT, @record_type VARCHAR (50), @previous_id INT
DECLARE @settle_date DATETIME, @settle_type NVARCHAR (50), @response_code VARCHAR (50), @trans_type_id INT
DECLARE @open_funding_id BIGINT, @discount MONEY, @new_contract_number VARCHAR (50), @contract_type VARCHAR (50), @merchant_id BIGINT, @openCount INT, @AllCount INT, @DifferentAccount BIT
SET @openCount = 0; SET @AllCount = 0; SET @DifferentAccount = 0

SELECT @discount = f.discount, @new_contract_number = f.contract_number, @contract_type = f.contract_type, @merchant_id = f.merchant_id
FROM fundings f WHERE f.id = @new_funding_id


-- ************************************************		
-- ************************************************
IF LOWER(@contract_type) = 'renewal'
	BEGIN
		IF OBJECT_ID('tempdb..#tempFundingsRenew') IS NOT NULL DROP TABLE #tempFundingsRenew
		CREATE TABLE #tempFundingsRenew (funding_id BIGINT, contract_number NVARCHAR (50), payoff_amount MONEY)

		INSERT INTO #tempFundingsRenew (funding_id, contract_number, payoff_amount)
		SELECT f.id, f.contract_number, f.payoff_amount
		FROM fundings f
		WHERE f.contract_status = 1 AND f.merchant_id = @merchant_id AND f.payoff_amount > 0 AND f.id <> @new_funding_id

		SET @openCount = @@ROWCOUNT

		IF @openCount > 1
			BEGIN
				-- do nothing, we'll return the @openCount and code will email accounting team
				PRINT @openCount
			END
		ELSE IF @openCount = 1
			BEGIN						
				BEGIN TRANSACTION
				
				SELECT @open_funding_id = funding_id FROM #tempFundingsRenew
				
				DECLARE @today DATE, @newBatchID INT; SET @today = DATEADD(HOUR, -5, GETUTCDATE())
				DECLARE @tomorrow DATE; SET @tomorrow = dbo.CF_GetTomorrow(@today)
				EXEC @newBatchID = dbo.CSP_GetNewBatchNumber '', 'SYSTEM', 'ContractRenewal', '', @today, 0

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

				-- if there is a discount, do that first
				IF COALESCE(@discount, 0) <> 0
					BEGIN
						SELECT @trans_amount = @discount, @transaction_id = NULL, @trans_date = @today, @comments = 'Discount due to renewal of ' + @new_contract_number, @record_id = NULL,
							@record_type = NULL, @previous_id = NULL, @settle_date = @today, @settle_type = NULL, @response_code = NULL, @trans_type_id = 8
		
						EXEC dbo.CSP_DistributeTransaction_v2 @open_funding_id,	@trans_amount, @transaction_id, @trans_date, @comments, @newBatchID, @record_id, @record_type, @previous_id, @settle_date, 
							@settle_type, @response_code, @trans_type_id	
			
						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR	
					END

				-- now enter a wire for the payoff amount
				-- first find the caymus payoff amount
				SELECT TOP 1 @trans_amount = CASE WHEN COALESCE(discounted_balance, 0) <> 0 THEN discounted_balance ELSE amount END 
				FROM funding_distributions WHERE funding_id = @new_funding_id AND recipient LIKE '%Caymus%' 

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

				IF @trans_amount IS NOT NULL
					BEGIN
						SELECT @transaction_id = NULL, @trans_date = @today, @comments = 'Wire payoff due to renewal of ' + @new_contract_number, @record_id = NULL,
							@record_type = NULL, @previous_id = NULL, @settle_date = @today, @settle_type = NULL, @response_code = NULL, @trans_type_id = 5
						FROM fundings f
						WHERE f.id = @open_funding_id

						IF COALESCE(@discount, 0) = 0 
							BEGIN
								-- if no discount, then just distribute normally
								EXEC dbo.CSP_DistributeTransaction_v2 @open_funding_id,	@trans_amount, @transaction_id, @trans_date, @comments, @newBatchID, @record_id, 
									@record_type, @previous_id, @settle_date, @settle_type, @response_code, @trans_type_id
							END
						ELSE
							BEGIN
								-- if discount, need to distribute differently since margin was already reduced by discount, using the pricing ratio will not work
								DECLARE @new_trans_id INT
								INSERT INTO transactions (trans_date, funding_id, trans_amount, trans_type_id, comments, batch_id, record_id, previous_id, redistributed, 
									transaction_id, record_type, settle_date, redirect_approval_id, change_user, change_date, portfolio)
								SELECT @trans_date, @open_funding_id, @trans_amount, 5, @comments, @newBatchID, @record_id, @previous_id, 0, @transaction_id, 
									@record_type, @settle_date, NULL, 'SYSTEM', GETUTCDATE(), 'caymus'

								SELECT @new_trans_id = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
								IF @errorNum != 0 GOTO ERROR

								EXEC dbo.CSP_ApplyPPAndMarginDiscountRenewal @open_funding_id, @trans_amount, @new_trans_id, @comments
							END

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
					END

				-- set the batch to Processed
				UPDATE batches SET batch_status = 'Closed', change_user = 'SYSTEM', change_date = GETUTCDATE()
				WHERE batch_id = @newBatchID

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


				-- update the payoff amounts for the open funding
				EXEC dbo.CSP_UpdCalculationsForFunding @open_funding_id, 'SYSTEM'

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR


				-- now put funding on hold so that no more payments will come out
				UPDATE fundings SET on_hold = 1 , change_user = 'SYSTEM', change_date = GETUTCDATE(), on_hold_date = GETUTCDATE()
				WHERE id = @open_funding_id
				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

				-- RKB : 2019-05-07 -- deactivate all active payment schedules for this funding since it is now on hold
				--UPDATE ps
				--SET active = 0
				--FROM payment_schedules ps 
				--INNER JOIN funding_payment_schedules fps ON ps.payment_schedule_id = fps.payment_schedule_id
				--WHERE fps.funding_id = @open_funding_id AND ps.active = 1

				-- deactivate any future payments on the open funding
				UPDATE pd
				SET active = 0, change_date = GETUTCDATE(), change_user = 'SYSTEM'
				FROM funding_payment_dates pd
				WHERE pd.funding_id = @open_funding_id AND pd.active = 1 AND pd.payment_date >= @tomorrow

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR


				-- delete any payments in the queue for the open funding
				UPDATE payments 
				SET deleted = 1, change_date = GETUTCDATE(), change_user = 'SYSTEM'
				WHERE funding_id = @open_funding_id AND processed_date IS NULL
				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

				-- now, set the previous_funding_id on the new funding to point to the old open funding id
				UPDATE fundings SET previous_funding_id = @open_funding_id , change_user = 'SYSTEM', change_date = GETUTCDATE()
				WHERE id = @new_funding_id
				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

				-- all done, COMMIT
				COMMIT TRANSACTION
				GOTO Done

				ERROR:	
					ROLLBACK TRANSACTION
					RAISERROR (@ErrorMessage, 16, 1)	
			END
		ELSE -- @openCount = 0
			BEGIN
				-- do nothing.  no open contracts
				PRINT @openCount
			END

	END -- IF LOWER(@contract_type) = 'renewal'	
ELSE
	BEGIN
		-- check to see if it maybe should have been a renewal
		SELECT @AllCount = COUNT(*)
		FROM fundings f
		WHERE f.merchant_id = @merchant_id AND f.id <> @new_funding_id
	END

Done:

IF OBJECT_ID('tempdb..#tempFundingsRenew') IS NOT NULL DROP TABLE #tempFundingsRenew

-- now check to see if the bank info is different than what we had been pulling from
DECLARE @current_account_number NVARCHAR (100), @current_routing_number NVARCHAR (100), @most_recent_id INT
DECLARE @new_account_number NVARCHAR (100), @new_routing_number NVARCHAR (100)

SELECT TOP 1 @most_recent_id = t.trans_id
FROM transactions t WITH (NOLOCK)
WHERE t.funding_id = @open_funding_id AND t.trans_type_id = 3
ORDER BY t.trans_id DESC

IF @most_recent_id IS NOT NULL
	BEGIN
		SELECT @current_account_number = ba.account_number, @current_routing_number = ba.routing_number
		FROM transactions t WITH (NOLOCK)
		INNER JOIN payments p WITH (NOLOCK) ON t.transaction_id = p.transaction_id
		INNER JOIN funding_payment_dates pd WITH (NOLOCK) ON p.funding_payment_date_id = pd.id
		INNER JOIN merchant_bank_accounts ba WITH (NOLOCK) ON pd.merchant_bank_account_id = ba.merchant_bank_account_id
		WHERE t.trans_id = @most_recent_id

		SELECT @new_account_number = f.receivables_account_nbr, @new_routing_number = f.receivables_aba
		FROM fundings f WITH (NOLOCK)
		WHERE f.id = @new_funding_id

		IF COALESCE(@current_account_number, @new_account_number) != @new_account_number OR COALESCE(@current_routing_number, @new_routing_number) != @new_routing_number
			BEGIN
				SET @DifferentAccount = 1
			END
	END


SELECT @openCount AS openCount, @AllCount AS allCount, @DifferentAccount AS DifferentAccount

END


GO
