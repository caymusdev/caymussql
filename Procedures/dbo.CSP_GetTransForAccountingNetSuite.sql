SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:     Ryan Brown
-- Create Date: 2021-03-31
-- Description: Gets Journal Entries for NetSuite for transaction
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetTransForAccountingNetSuite]

AS

SET NOCOUNT ON


IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData
CREATE TABLE #tempData (id int not null identity, trans_id INT, main_trans_type_id INT NULL, trans_amount MONEY NULL)

IF OBJECT_ID('tempdb..#tempAllData') IS NOT NULL DROP TABLE #tempAllData
CREATE TABLE #tempAllData (id int not null identity, trans_id INT, trans_date VARCHAR (50), subsidiary VARCHAR (100), currency VARCHAR (50), postingperiod VARCHAR (50), 
	journalItemLine_memo NVARCHAR (1000), journalItemLine_account VARCHAR (100), journalItemLine_debitAmount MONEY, journalItemLine_creditAmount MONEY)

INSERT INTO #tempData (trans_id, main_trans_type_id, trans_amount)
SELECT t.trans_id, t.trans_type_id, t.trans_amount
FROM transactions t
INNER JOIN batches b ON t.batch_id = b.batch_id
WHERE t.redistributed = 0 AND t.trans_type_id NOT IN (1, 9, 23) AND b.batch_status IN ('Settled', 'Closed') AND t.sent_to_acct_2 IS NULL
	AND b.batch_type NOT IN ('ContractRenewal')
ORDER BY t.trans_date, t.trans_id

DECLARE @current_id INT, @max_id INT--, @current_trans_id INT
SELECT @current_id = MIN(id), @max_id = MAX(id) FROM #tempData


WHILE @current_id <= @max_id
	BEGIN
		DECLARE @new_trans_id INT, @new_trans_type_id INT, @new_trans_amount MONEY
		SELECT @new_trans_id = trans_id, @new_trans_type_id = main_trans_type_id, @new_trans_amount = trans_amount FROM #tempData WHERE id = @current_id

		-- if it is ACH received, we need to see if there is matching distributions (pre 2019 - but possibly could still happen)
		DECLARE @ach_rec_alone BIT; SET @ach_rec_alone = 0
		IF @new_trans_type_id = 3
			BEGIN
				IF EXISTS (SELECT 1 FROM transaction_details WHERE trans_id = @new_trans_id AND trans_type_id = 3)
					BEGIN
						SET @ach_rec_alone = 1
					END
			END
		
		IF @new_trans_type_id IN (2, 3, 4, 5, 19) AND @ach_rec_alone = 0 AND @new_trans_amount > 0
			BEGIN -- this is a regular deposit
				-- to bank
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), ''),
					CASE WHEN @new_trans_type_id IN (2, 5, 19) THEN '1000 Cash - BOA Checking - 2971'
						 WHEN @new_trans_type_id IN (4) AND b.manual_flag = 1 THEN '1007 Cash - Regions Business Checking - 1476' 
						 WHEN @new_trans_type_id IN (4) AND b.manual_flag = 0 THEN '1000 Cash - BOA Checking - 2971' 
						 ELSE '1007 Cash - Regions Business Checking - 1476'
					END, ABS(t.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				LEFT JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
				LEFT JOIN payments p2 WITH (NOLOCK) ON t.transaction_id = p2.transaction_id
				LEFT JOIN forte_settlements fs WITH (NOLOCK) ON t.record_type = 'forte_settlements' AND t.transaction_id = fs.transaction_id
					AND fs.settle_type = 'deposit' 
				WHERE t.trans_id = @new_trans_id

				-- rtr
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), ''),
					'1101 Right to Receive (RTR)', NULL, ABS(SUM(d.trans_amount))
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6,7)
				LEFT JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
				LEFT JOIN payments p2 WITH (NOLOCK) ON t.transaction_id = p2.transaction_id
				LEFT JOIN forte_settlements fs WITH (NOLOCK) ON t.record_type = 'forte_settlements' AND t.transaction_id = fs.transaction_id
					AND fs.settle_type = 'deposit' 
				WHERE t.trans_id = @new_trans_id
				GROUP BY t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), FORMAT(t.trans_date, 'MMM yyyy'), t.batch_id, 
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), '')

				-- deferred revenue margin
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), ''),
					'2011 Deferred Revenue', ABS(d.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6)
				LEFT JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
				LEFT JOIN payments p2 WITH (NOLOCK) ON t.transaction_id = p2.transaction_id
				LEFT JOIN forte_settlements fs WITH (NOLOCK) ON t.record_type = 'forte_settlements' AND t.transaction_id = fs.transaction_id
					AND fs.settle_type = 'deposit' 
				WHERE t.trans_id = @new_trans_id

				-- to margin
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), ''),
					'4003 Revenue (Margin)', NULL, ABS(d.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6)
				LEFT JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
				LEFT JOIN payments p2 WITH (NOLOCK) ON t.transaction_id = p2.transaction_id
				LEFT JOIN forte_settlements fs WITH (NOLOCK) ON t.record_type = 'forte_settlements' AND t.transaction_id = fs.transaction_id
					AND fs.settle_type = 'deposit' 
				WHERE t.trans_id = @new_trans_id

				-- to bad debt recovery
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), ''),
					'4009 Recovery on Bad Debt', NULL, ABS(d.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (15)
				LEFT JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
				LEFT JOIN payments p2 WITH (NOLOCK) ON t.transaction_id = p2.transaction_id
				LEFT JOIN forte_settlements fs WITH (NOLOCK) ON t.record_type = 'forte_settlements' AND t.transaction_id = fs.transaction_id
					AND fs.settle_type = 'deposit' 
				WHERE t.trans_id = @new_trans_id

				-- cash to fees
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), ''),
					'4001 Fee Income', NULL, ABS(d.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (1)
				LEFT JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
				LEFT JOIN payments p2 WITH (NOLOCK) ON t.transaction_id = p2.transaction_id
				LEFT JOIN forte_settlements fs WITH (NOLOCK) ON t.record_type = 'forte_settlements' AND t.transaction_id = fs.transaction_id
					AND fs.settle_type = 'deposit' 
				WHERE t.trans_id = @new_trans_id
			END	 -- regular deposit
		ELSE IF @new_trans_type_id IN (2, 3, 4, 5, 19) AND @ach_rec_alone = 0 AND @new_trans_amount < 0
			BEGIN -- REFUNDS
				-- to bank
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), ''),
					CASE WHEN @new_trans_type_id IN (2, 5, 19) THEN '1000 Cash - BOA Checking - 2971'
						 WHEN @new_trans_type_id IN (4) AND b.manual_flag = 1 THEN '1007 Cash - Regions Business Checking - 1476' 
						 WHEN @new_trans_type_id IN (4) AND b.manual_flag = 0 THEN '1000 Cash - BOA Checking - 2971' 
						 ELSE '1007 Cash - Regions Business Checking - 1476'
					END, NULL, ABS(t.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				LEFT JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
				LEFT JOIN payments p2 WITH (NOLOCK) ON t.transaction_id = p2.transaction_id				
				LEFT JOIN forte_settlements fs WITH (NOLOCK) ON t.record_type = 'forte_settlements' AND t.transaction_id = fs.transaction_id
					AND fs.settle_type = 'deposit' 
				WHERE t.trans_id = @new_trans_id

				-- rtr
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), ''),
					'1101 Right to Receive (RTR)', ABS(SUM(d.trans_amount)), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6,7)
				LEFT JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
				LEFT JOIN payments p2 WITH (NOLOCK) ON t.transaction_id = p2.transaction_id
				LEFT JOIN forte_settlements fs WITH (NOLOCK) ON t.record_type = 'forte_settlements' AND t.transaction_id = fs.transaction_id
					AND fs.settle_type = 'deposit' 
				WHERE t.trans_id = @new_trans_id
				GROUP BY t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), FORMAT(t.trans_date, 'MMM yyyy'), 
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), '')

				-- deferred revenue margin
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), ''),
					'2011 Deferred Revenue', NULL, ABS(d.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6)
				LEFT JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
				LEFT JOIN payments p2 WITH (NOLOCK) ON t.transaction_id = p2.transaction_id
				LEFT JOIN forte_settlements fs WITH (NOLOCK) ON t.record_type = 'forte_settlements' AND t.transaction_id = fs.transaction_id
					AND fs.settle_type = 'deposit' 
				WHERE t.trans_id = @new_trans_id

				-- to margin
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), ''),
					'4003 Revenue (Margin)', ABS(d.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6)
				LEFT JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
				LEFT JOIN payments p2 WITH (NOLOCK) ON t.transaction_id = p2.transaction_id
				LEFT JOIN forte_settlements fs WITH (NOLOCK) ON t.record_type = 'forte_settlements' AND t.transaction_id = fs.transaction_id
					AND fs.settle_type = 'deposit' 
				WHERE t.trans_id = @new_trans_id

				-- to bad debt recovery
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), ''),
					'4009 Recovery on Bad Debt', ABS(d.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (15)
				LEFT JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
				LEFT JOIN payments p2 WITH (NOLOCK) ON t.transaction_id = p2.transaction_id
				LEFT JOIN forte_settlements fs WITH (NOLOCK) ON t.record_type = 'forte_settlements' AND t.transaction_id = fs.transaction_id
					AND fs.settle_type = 'deposit' 
				WHERE t.trans_id = @new_trans_id

				-- cash to fees
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), ''),
					'4001 Fee Income', ABS(d.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (1)
				LEFT JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
				LEFT JOIN payments p2 WITH (NOLOCK) ON t.transaction_id = p2.transaction_id
				LEFT JOIN forte_settlements fs WITH (NOLOCK) ON t.record_type = 'forte_settlements' AND t.transaction_id = fs.transaction_id
					AND fs.settle_type = 'deposit' 
				WHERE t.trans_id = @new_trans_id
			END	 -- REFUNDS		
		ELSE IF @new_trans_type_id IN (3) AND @ach_rec_alone = 1
			BEGIN -- this is ACH received by itself
				-- to bank
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), ''),
					CASE WHEN b.batch_type LIKE '%Forte%' THEN '1000 Cash - BOA Checking - 2971' 
						WHEN b.batch_type LIKE '%Regions%' THEN '1007 Cash - Regions Business Checking - 1476' 
						ELSE '1000 Cash - BOA Checking - 2971' END, ABS(t.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				LEFT JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
				LEFT JOIN payments p2 WITH (NOLOCK) ON t.transaction_id = p2.transaction_id				
				LEFT JOIN forte_settlements fs WITH (NOLOCK) ON t.record_type = 'forte_settlements' AND t.transaction_id = fs.transaction_id
					AND fs.settle_type = 'deposit' 
				WHERE t.trans_id = @new_trans_id

				
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), ''),
					'1102 Accrued Income', NULL, ABS(t.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id		
				LEFT JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
				LEFT JOIN payments p2 WITH (NOLOCK) ON t.transaction_id = p2.transaction_id
				LEFT JOIN forte_settlements fs WITH (NOLOCK) ON t.record_type = 'forte_settlements' AND t.transaction_id = fs.transaction_id
					AND fs.settle_type = 'deposit' 
				WHERE t.trans_id = @new_trans_id
			END	 -- CC
		ELSE IF @new_trans_type_id IN (18)
			BEGIN -- ACH Draft
				-- to bank
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number,
					'1102 Accrued Income', ABS(t.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id								
				WHERE t.trans_id = @new_trans_id

				-- rtr
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number,
					'1101 Right to Receive (RTR)', NULL, ABS(SUM(d.trans_amount))
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6,7)
				WHERE t.trans_id = @new_trans_id
				GROUP BY t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), FORMAT(t.trans_date, 'MMM yyyy'), 
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number

				-- deferred revenue margin
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'2011 Deferred Revenue', ABS(d.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6)
				WHERE t.trans_id = @new_trans_id

				-- to margin
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'4003 Revenue (Margin)', NULL, ABS(d.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6)
				WHERE t.trans_id = @new_trans_id

				-- to bad debt recovery
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'4009 Recovery on Bad Debt', NULL, ABS(d.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (15)
				WHERE t.trans_id = @new_trans_id

				-- cash to fees
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'4001 Fee Income', NULL, ABS(d.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (1)
				WHERE t.trans_id = @new_trans_id
			END	 -- ACH Draft
		ELSE IF @new_trans_type_id IN (14)
		-- chargebacks
			BEGIN
				-- to bank
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), ''),
					CASE COALESCE(p.processor, p2.processor, 
						CASE WHEN b.batch_type LIKE '%Forte%' THEN 'Forte' WHEN b.batch_type LIKE '%FifthThird%' THEN '53' ELSE NULL END, f.processor) WHEN '53' 
						THEN '1000 Cash - BOA Checking - 2971' ELSE '1007 Cash - Regions Business Checking - 1476' END,
					NULL, ABS(t.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				--LEFT JOIN forte_settlements s WITH (NOLOCK) ON s.transaction_id = (SELECT transaction_id FROM forte_settlements fs WHERE t.record_id = fs.forte_settle_id) AND s.settle_type = 'deposit'
				--	AND s.settle_response_code = 'S01'
				--LEFT JOIN forte_trans ft WITH (NOLOCK) ON s.transaction_id = ft.transaction_id AND ft.status = 'funded'
				LEFT JOIN transactions t2 ON t.transaction_id = t2.transaction_id AND t2.trans_type_id = 3 AND t.funding_id = t2.funding_id
				LEFT JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
				LEFT JOIN payments p2 WITH (NOLOCK) ON t.transaction_id = p2.transaction_id				
				LEFT JOIN forte_settlements fs WITH (NOLOCK) ON t.record_type = 'forte_settlements' AND t.transaction_id = fs.transaction_id
					AND fs.settle_type = 'withdrawal' -- since it will now have 2 forte settlements we need to narrow it to just the withdrawal
				WHERE t.trans_id = @new_trans_id

				-- rtr
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), ''),
					'1101 Right to Receive (RTR)', ABS(SUM(d.trans_amount)), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6,7)
				--LEFT JOIN forte_settlements s WITH (NOLOCK) ON s.transaction_id = (SELECT transaction_id FROM forte_settlements fs WHERE t.record_id = fs.forte_settle_id) AND s.settle_type = 'deposit'
				--	AND s.settle_response_code = 'S01'
			--	LEFT JOIN forte_trans ft WITH (NOLOCK) ON s.transaction_id = ft.transaction_id AND ft.status = 'funded'
				LEFT JOIN transactions t2 ON t.transaction_id = t2.transaction_id AND t2.trans_type_id = 3 AND t.funding_id = t2.funding_id
				LEFT JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
				LEFT JOIN payments p2 WITH (NOLOCK) ON t.transaction_id = p2.transaction_id	
				LEFT JOIN forte_settlements fs WITH (NOLOCK) ON t.record_type = 'forte_settlements' AND t.transaction_id = fs.transaction_id
					AND fs.settle_type = 'withdrawal' -- since it will now have 2 forte settlements we need to narrow it to just the withdrawal
				WHERE t.trans_id = @new_trans_id
				GROUP BY t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 	FORMAT(t.trans_date, 'MMM yyyy'), 
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), '')

				-- deferred revenue margin
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), ''),
					'2011 Deferred Revenue', NULL, ABS(d.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6)
				--LEFT JOIN forte_settlements s WITH (NOLOCK) ON s.transaction_id = (SELECT transaction_id FROM forte_settlements fs WHERE t.record_id = fs.forte_settle_id) AND s.settle_type = 'deposit'
				--	AND s.settle_response_code = 'S01'
				--LEFT JOIN forte_trans ft WITH (NOLOCK) ON s.transaction_id = ft.transaction_id AND ft.status = 'funded'
				LEFT JOIN transactions t2 ON t.transaction_id = t2.transaction_id AND t2.trans_type_id = 3 AND t.funding_id = t2.funding_id
				LEFT JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
				LEFT JOIN payments p2 WITH (NOLOCK) ON t.transaction_id = p2.transaction_id	
				LEFT JOIN forte_settlements fs WITH (NOLOCK) ON t.record_type = 'forte_settlements' AND t.transaction_id = fs.transaction_id
					AND fs.settle_type = 'withdrawal' -- since it will now have 2 forte settlements we need to narrow it to just the withdrawal
				WHERE t.trans_id = @new_trans_id

				-- to margin
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), ''),
					'4003 Revenue (Margin)', ABS(d.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6)
				----LEFT JOIN forte_settlements s WITH (NOLOCK) ON s.transaction_id = (SELECT transaction_id FROM forte_settlements fs WHERE t.record_id = fs.forte_settle_id) AND s.settle_type = 'deposit'
				---	AND s.settle_response_code = 'S01'
				--LEFT JOIN forte_trans ft WITH (NOLOCK) ON s.transaction_id = ft.transaction_id AND ft.status = 'funded'
				LEFT JOIN transactions t2 ON t.transaction_id = t2.transaction_id AND t2.trans_type_id = 3 AND t.funding_id = t2.funding_id
				LEFT JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
				LEFT JOIN payments p2 WITH (NOLOCK) ON t.transaction_id = p2.transaction_id	
				LEFT JOIN forte_settlements fs WITH (NOLOCK) ON t.record_type = 'forte_settlements' AND t.transaction_id = fs.transaction_id
					AND fs.settle_type = 'withdrawal' -- since it will now have 2 forte settlements we need to narrow it to just the withdrawal
				WHERE t.trans_id = @new_trans_id

				-- to bad debt recovery
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), ''),
					'4009 Recovery on Bad Debt', ABS(d.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (15)
				--LEFT JOIN forte_settlements s WITH (NOLOCK) ON s.transaction_id = (SELECT transaction_id FROM forte_settlements fs WHERE t.record_id = fs.forte_settle_id) AND s.settle_type = 'deposit'
				--	AND s.settle_response_code = 'S01'
				--LEFT JOIN forte_trans ft WITH (NOLOCK) ON s.transaction_id = ft.transaction_id AND ft.status = 'funded'
				LEFT JOIN transactions t2 ON t.transaction_id = t2.transaction_id AND t2.trans_type_id = 3 AND t.funding_id = t2.funding_id
				LEFT JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
				LEFT JOIN payments p2 WITH (NOLOCK) ON t.transaction_id = p2.transaction_id	
				LEFT JOIN forte_settlements fs WITH (NOLOCK) ON t.record_type = 'forte_settlements' AND t.transaction_id = fs.transaction_id
					AND fs.settle_type = 'withdrawal' -- since it will now have 2 forte settlements we need to narrow it to just the withdrawal
				WHERE t.trans_id = @new_trans_id

				-- cash to fees
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number + 
						COALESCE(':' + REPLACE(fs.funding_id, 'fnd_', ''), ':' + CONVERT(VARCHAR (50), COALESCE(p.payment_id, p2.payment_id)), ''),
					'4001 Fee Income', ABS(d.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (1)
				--LEFT JOIN forte_settlements s WITH (NOLOCK) ON s.transaction_id = (SELECT transaction_id FROM forte_settlements fs WHERE t.record_id = fs.forte_settle_id) AND s.settle_type = 'deposit'
				--	AND s.settle_response_code = 'S01'
				--LEFT JOIN forte_trans ft WITH (NOLOCK) ON s.transaction_id = ft.transaction_id AND ft.status = 'funded'
				LEFT JOIN transactions t2 ON t.transaction_id = t2.transaction_id AND t2.trans_type_id = 3 AND t.funding_id = t2.funding_id
				LEFT JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
				LEFT JOIN payments p2 WITH (NOLOCK) ON t.transaction_id = p2.transaction_id	
				LEFT JOIN forte_settlements fs WITH (NOLOCK) ON t.record_type = 'forte_settlements' AND t.transaction_id = fs.transaction_id
					AND fs.settle_type = 'withdrawal' -- since it will now have 2 forte settlements we need to narrow it to just the withdrawal
				WHERE t.trans_id = @new_trans_id
			END	 -- chargeback
		ELSE IF @new_trans_type_id IN (21)
			-- rejections
			BEGIN
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'1102 Accrued Income', NULL, ABS(t.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				LEFT JOIN forte_settlements s ON s.transaction_id = (SELECT transaction_id FROM forte_settlements fs WHERE t.record_id = fs.forte_settle_id)
				LEFT JOIN forte_trans ft WITH (NOLOCK) ON ft.transaction_id = s.transaction_id AND ft.status = 'rejected'				
				WHERE t.trans_id = @new_trans_id
					AND t.trans_date >= '2019-01-01'

				-- rtr
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 						
					'1101 Right to Receive (RTR)', ABS(SUM(d.trans_amount)), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6,7)
				LEFT JOIN forte_settlements s ON s.transaction_id = (SELECT transaction_id FROM forte_settlements fs WHERE t.record_id = fs.forte_settle_id)
				LEFT JOIN forte_trans ft WITH (NOLOCK) ON ft.transaction_id = s.transaction_id AND ft.status = 'rejected'
				WHERE t.trans_id = @new_trans_id
					AND t.trans_date >= '2019-01-01'
				GROUP BY t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), FORMAT(t.trans_date, 'MMM yyyy'), 
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number

				-- deferred revenue margin
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'2011 Deferred Revenue', NULL, ABS(d.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6)
				LEFT JOIN forte_settlements s ON s.transaction_id = (SELECT transaction_id FROM forte_settlements fs WHERE t.record_id = fs.forte_settle_id)
				LEFT JOIN forte_trans ft WITH (NOLOCK) ON ft.transaction_id = s.transaction_id AND ft.status = 'rejected'
				WHERE t.trans_id = @new_trans_id
					AND t.trans_date >= '2019-01-01'

				-- to margin
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'4003 Revenue (Margin)', ABS(d.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6)
				LEFT JOIN forte_settlements s ON s.transaction_id = (SELECT transaction_id FROM forte_settlements fs WHERE t.record_id = fs.forte_settle_id)
				LEFT JOIN forte_trans ft WITH (NOLOCK) ON ft.transaction_id = s.transaction_id AND ft.status = 'rejected'
				WHERE t.trans_id = @new_trans_id
					AND t.trans_date >= '2019-01-01'

				-- to bad debt recovery
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'4009 Recovery on Bad Debt', ABS(d.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (15)
				LEFT JOIN forte_settlements s ON s.transaction_id = (SELECT transaction_id FROM forte_settlements fs WHERE t.record_id = fs.forte_settle_id)
				LEFT JOIN forte_trans ft WITH (NOLOCK) ON ft.transaction_id = s.transaction_id AND ft.status = 'rejected'
				WHERE t.trans_id = @new_trans_id
					AND t.trans_date >= '2019-01-01'

				-- cash to fees
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 						
					'4001 Fee Income', ABS(d.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (1)
				LEFT JOIN forte_settlements s ON s.transaction_id = (SELECT transaction_id FROM forte_settlements fs WHERE t.record_id = fs.forte_settle_id)
				LEFT JOIN forte_trans ft WITH (NOLOCK) ON ft.transaction_id = s.transaction_id AND ft.status = 'rejected'
				WHERE t.trans_id = @new_trans_id
					AND t.trans_date >= '2019-01-01'
			END	 -- rejection
		ELSE IF @new_trans_type_id IN (11)
			-- writeoff 
			BEGIN
				-- to bank
				/*  What Nina had given
				INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, trans_id)
				SELECT 'JE', 'Bad Debt', NULL, t.trans_date, 
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id), b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'1101', 'RTR', NULL, NULL, ABS(t.trans_amount), t.trans_id
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				WHERE t.trans_id = @new_trans_id

				-- bad debt
				INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, trans_id)
				SELECT NULL, 'Bad Debt', NULL, NULL, 'Batch ' + CONVERT(VARCHAR(50), t.batch_id), b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, '1200', 
					'Bad Debt Reserve - Purchase Price Written Off', NULL, ABS(d.trans_amount), NULL, t.trans_id
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (15)
				WHERE t.trans_id = @new_trans_id	

				-- deferred revenue margin
				INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, trans_id)
				SELECT NULL, 'Bad Debt', NULL, NULL, 'Batch ' + CONVERT(VARCHAR(50), t.batch_id), b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, '2011', 
					'Bad Debt Reserve - Purchase Price Written Off', NULL, ABS(d.trans_amount), NULL, t.trans_id
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (17)
				WHERE t.trans_id = @new_trans_id	
				*/
				-- This is from Dartanya
				-- First, find the sum of margin adjustment (trans_type_id = 17)
				DECLARE @writeoff_adjustment MONEY, @writeoff_funding_id BIGINT, @comments NVARCHAR (500), @batch_id INT
				SELECT @writeoff_funding_id = funding_id, @comments = comments, @batch_id = batch_id
				FROM transactions WITH (NOLOCK) 
				WHERE trans_id = @new_trans_id

				IF COALESCE(@comments, '') = 'Partial Writeoff'
					BEGIN
						SELECT @writeoff_adjustment = t.trans_amount
						FROM transactions t
						WHERE t.funding_id = @writeoff_funding_id AND t.trans_type_id = 17 AND t.batch_id = @batch_id						
					END
				ELSE 
					BEGIN
						SELECT @writeoff_adjustment = SUM(t.trans_amount)
						FROM transactions t
						WHERE t.funding_id = @writeoff_funding_id AND t.trans_type_id = 17
					END

				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 						
					'1101 Right to Receive (RTR)', NULL, ABS(t.trans_amount) + ABS(@writeoff_adjustment)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				WHERE t.trans_id = @new_trans_id

				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number , 
					'2011 Deferred Revenue', ABS(@writeoff_adjustment), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				WHERE t.trans_id = @new_trans_id
				/*  Email from Dartanya on 2020-01-22 says not to do account 5000 anymore
				INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, trans_id)
				SELECT NULL, 'General', NULL, t.trans_date, 
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id), b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'5000', 'Write off Bad Debt expense', NULL, NULL, ABS(t.trans_amount), t.trans_id
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				WHERE t.trans_id = @new_trans_id
				*/

				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'1200 RTR Allowance for Bad Debts', ABS(t.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				WHERE t.trans_id = @new_trans_id

				/*  Email from Dartanya on 2020-01-22 says not to do account 5000 anymore
				INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, trans_id)
				SELECT NULL, 'General', NULL, t.trans_date, 
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id), b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'5000', 'Write off of Non Performing Contract ' + f.contract_number, NULL, ABS(t.trans_amount), NULL, t.trans_id
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				WHERE t.trans_id = @new_trans_id			
				*/
			END	 -- writeoff 
		ELSE IF @new_trans_type_id IN (8)
			-- Discount 
			BEGIN
				/* changed 2020-10-07 per Dartanya email
				INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, trans_id)
				SELECT 'JE', NULL, NULL, t.trans_date, 
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id), b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'2011', 'Deferred Margin - Discount Adjustment', NULL, ABS(t.trans_amount), NULL, t.trans_id
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				WHERE t.trans_id = @new_trans_id

				INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, trans_id)
				SELECT NULL, NULL, NULL, NULL, 
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id), b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'4005', 'Discount on Portfolio', NULL, NULL, ABS(t.trans_amount), t.trans_id
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				WHERE t.trans_id = @new_trans_id
				*/

				-- Changed on 2022-04-14 because Davis said it sohuld be account 2011 and not 4005
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'2011 Deferred Revenue', ABS(t.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				WHERE t.trans_id = @new_trans_id

				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'1101 Right to Receive (RTR)', NULL, ABS(t.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				WHERE t.trans_id = @new_trans_id
			END	 -- discount 
		ELSE IF @new_trans_type_id IN (10)
			-- Settlement 
			BEGIN
				-- to bank
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'1007 Cash - Regions Business Checking - 1476', ABS(t.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				LEFT JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
				LEFT JOIN payments p2 WITH (NOLOCK) ON t.transaction_id = p2.transaction_id
				WHERE t.trans_id = @new_trans_id

				-- rtr
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'1101 Right to Receive (RTR)', NULL, ABS(SUM(d.trans_amount))
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6,7)
				WHERE t.trans_id = @new_trans_id
				GROUP BY t.trans_id, t.trans_date, 'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number

				-- deferred revenue margin
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'2011 Deferred Revenue', ABS(d.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6)
				WHERE t.trans_id = @new_trans_id

				-- to margin
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'4003 Revenue (Margin)', NULL, ABS(d.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6)
				WHERE t.trans_id = @new_trans_id

				-- to bad debt recovery
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'4009 Recovery on Bad Debt', NULL, ABS(d.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (15)
				WHERE t.trans_id = @new_trans_id

				-- cash to fees
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'4001 Fee Income', NULL, ABS(d.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (1)
				WHERE t.trans_id = @new_trans_id

				-- ?
				/*
				INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit)
				SELECT NULL, 'Bad Debt', NULL, NULL, 'Batch ' + CONVERT(VARCHAR(50), t.batch_id), b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, '1200', 'Settlement', 
					NULL, NULL, ABS(t.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (1)
				WHERE t.trans_id = @new_trans_id
				*/
			END	 -- Settlement
		ELSE IF @new_trans_type_id IN (16)
			BEGIN -- this is a REAPPLICATION
				-- only do the negative ones.  As part of the process here, we'll look up the postive ones so that they are all in one journal entry so skip the positive ones
				IF @new_trans_amount < 0
					BEGIN
						-- rtr
						INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
							journalItemLine_debitAmount, journalItemLine_creditAmount)
						SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
							'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
							'1101 Right to Receive (RTR)', ABS(SUM(d.trans_amount)), NULL
						FROM transactions t WITH (NOLOCK)
						INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
						INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
						INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6,7)
						WHERE t.trans_id = @new_trans_id
						GROUP BY t.trans_id, t.trans_date, t.batch_id, 'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number

						-- deferred revenue margin
						INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
							journalItemLine_debitAmount, journalItemLine_creditAmount)
						SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
							'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
							'2011 Deferred Revenue', NULL, ABS(d.trans_amount)
						FROM transactions t WITH (NOLOCK)
						INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
						INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
						INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6)
						WHERE t.trans_id = @new_trans_id

						-- to margin
						INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
							journalItemLine_debitAmount, journalItemLine_creditAmount)
						SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
							'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
							'4003 Revenue (Margin)', ABS(d.trans_amount), NULL
						FROM transactions t WITH (NOLOCK)
						INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
						INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
						INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6)
						WHERE t.trans_id = @new_trans_id

						-- to bad debt recovery
						INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
							journalItemLine_debitAmount, journalItemLine_creditAmount)
						SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
							'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
							'4009 Recovery on Bad Debt', ABS(d.trans_amount), NULL
						FROM transactions t WITH (NOLOCK)
						INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
						INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
						INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (15)
						WHERE t.trans_id = @new_trans_id

						-- cash to fees
						INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
							journalItemLine_debitAmount, journalItemLine_creditAmount)
						SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
							'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
							'4001 Fee Income', ABS(d.trans_amount), NULL
						FROM transactions t WITH (NOLOCK)
						INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
						INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
						INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (1)
						WHERE t.trans_id = @new_trans_id


						--- NOW DO THE MOVED TO CONTRACT
						DECLARE @temp_id INT 
						SELECT @temp_id = trans_id FROM transactions t 
						WHERE batch_id = (SELECT batch_id FROM transactions t2 WHERE t2.trans_id = @new_trans_id) AND t.trans_amount = ABS(@new_trans_amount)
							AND t.trans_id <> @new_trans_id AND t.trans_type_id = 16
							AND t.trans_date = (SELECT trans_date FROM transactions t3 WHERE t3.trans_id = @new_trans_id)

						-- for older Access where batch does not match
						IF @temp_id IS NULL
							BEGIN
								SELECT @temp_id = trans_id FROM transactions t 
								WHERE t.trans_amount = ABS(@new_trans_amount) AND t.trans_id <> @new_trans_id AND t.trans_type_id = 16
									AND t.trans_date = (SELECT trans_date FROM transactions t2 WHERE t2.trans_id = @new_trans_id)
							END
						-- rtr
						INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
							journalItemLine_debitAmount, journalItemLine_creditAmount)
						SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
							'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
							'1101 Right to Receive (RTR)', NULL, ABS(SUM(d.trans_amount))
						FROM transactions t WITH (NOLOCK)
						INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
						INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
						INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6,7)
						WHERE t.trans_id = @temp_id
						GROUP BY t.trans_id, t.trans_date, 'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number

						-- deferred revenue margin
						INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
							journalItemLine_debitAmount, journalItemLine_creditAmount)
						SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
							'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
							'2011 Deferred Revenue', ABS(d.trans_amount), NULL
						FROM transactions t WITH (NOLOCK)
						INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
						INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
						INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6)
						WHERE t.trans_id = @temp_id

						-- to margin
						INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
							journalItemLine_debitAmount, journalItemLine_creditAmount)
						SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
							'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
							'4003 Revenue (Margin)', NULL, ABS(d.trans_amount)
						FROM transactions t WITH (NOLOCK)
						INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
						INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
						INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6)
						WHERE t.trans_id = @temp_id

						-- to bad debt recovery
						INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
							journalItemLine_debitAmount, journalItemLine_creditAmount)
						SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
							'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
							'4009 Recovery on Bad Debt', NULL, ABS(d.trans_amount)
						FROM transactions t WITH (NOLOCK)
						INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
						INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
						INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (15)
						WHERE t.trans_id = @temp_id

						-- cash to fees
						INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
							journalItemLine_debitAmount, journalItemLine_creditAmount)
						SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
							'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
							'4001 Fee Income', NULL, ABS(d.trans_amount)
						FROM transactions t WITH (NOLOCK)
						INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
						INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
						INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (1)
						WHERE t.trans_id = @temp_id
					END  -- trans_amount < 0
			END	 -- reapplication
		ELSE IF @new_trans_type_id IN (22)
			BEGIN -- REFUNDS								
				-- rtr
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'1101 Right to Receive (RTR)', ABS(SUM(d.trans_amount)), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6,7)
				WHERE t.trans_id = @new_trans_id
				GROUP BY t.trans_id, t.trans_date, 'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number

				-- deferred revenue margin
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'2011 Deferred Revenue', NULL, ABS(d.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6)
				WHERE t.trans_id = @new_trans_id

				-- to margin
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'4003 Revenue (Margin)', ABS(d.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6)
				WHERE t.trans_id = @new_trans_id

				-- to bad debt recovery
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'4009 Recovery on Bad Debt', ABS(d.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (15)
				WHERE t.trans_id = @new_trans_id

				-- cash to fees
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'4001 Fee Income', ABS(d.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (1)
				WHERE t.trans_id = @new_trans_id

				-- refund account
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'2012 Refunds due to Merchants', NULL, ABS(t.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				--INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (22)
				WHERE t.trans_id = @new_trans_id
			END	 -- refunds
		ELSE IF @new_trans_type_id IN (24)
			BEGIN -- REFUNDS TOO
				-- first need to get the batch id from this funding to find the funding that the money came from
				DECLARE @refund_batch_id INT, @refund_contract_nbr VARCHAR (50)
				SELECT @refund_batch_id = batch_id FROM transactions WHERE trans_id = @new_trans_id
				SELECT @refund_contract_nbr = f.contract_number
				FROM fundings f
				WHERE f.id = (
						SELECT t2.funding_id
						FROM transactions t2
						WHERE t2.trans_id = (SELECT t3.trans_id FROM transactions t3 WHERE t3.batch_id = @refund_batch_id AND t3.trans_id <> @new_trans_id)
							)

				-- rtr
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'1101 Right to Receive (RTR)', NULL, ABS(SUM(d.trans_amount))
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6,7)
				WHERE t.trans_id = @new_trans_id
				GROUP BY t.trans_id, t.trans_date, 'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number

				-- deferred revenue margin
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'2011 Deferred Revenue', ABS(d.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6)
				WHERE t.trans_id = @new_trans_id

				-- to margin
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'4003 Revenue (Margin)', NULL, ABS(d.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (6)
				WHERE t.trans_id = @new_trans_id

				-- to bad debt recovery
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'4009 Recovery on Bad Debt', NULL, ABS(d.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (15)
				WHERE t.trans_id = @new_trans_id

				-- cash to fees
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'4001 Fee Income', NULL, ABS(d.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				INNER JOIN transaction_details d WITH (NOLOCK) ON t.trans_id = d.trans_id AND d.trans_type_id IN (1)
				WHERE t.trans_id = @new_trans_id

				-- refund account
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'2012 Refunds due to Merchants', ABS(t.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id				
				WHERE t.trans_id = @new_trans_id
			END	 -- refunds too
		ELSE IF @new_trans_type_id IN (25)
			BEGIN -- REFUNDS SENT TO MERCHANT
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'1008 Cash - Regions Bank - 1484 - (Funding)', NULL, ABS(t.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				WHERE t.trans_id = @new_trans_id

				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type + ' - ' + f.legal_name + ' ' + f.contract_number, 
					'2012 Refunds due to Merchants', ABS(t.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				INNER JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
				WHERE t.trans_id = @new_trans_id
			END	 -- refunds sent to merchant
		ELSE IF @new_trans_type_id IN (9999)
			BEGIN -- NON PORTFOLIO TRANSACTIONS
				
				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type,
					CASE WHEN b.batch_type LIKE '%Forte%' THEN '1000 Cash - BOA Checking - 2971' 
						WHEN b.batch_type LIKE '%Regions%' THEN '1007 Cash - Regions Business Checking - 1476' 
						ELSE '1000 Cash - BOA Checking - 2971' END, ABS(t.trans_amount), NULL
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				WHERE t.trans_id = @new_trans_id

				INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount)
				SELECT t.trans_id, FORMAT(t.trans_date, 'MM/dd/yyyy'), 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', FORMAT(t.trans_date, 'MMM yyyy'),
					'Batch ' + CONVERT(VARCHAR(50), t.batch_id) + '-' + b.batch_type,
					'9999 Ask My Accountant', NULL, ABS(t.trans_amount)
				FROM transactions t WITH (NOLOCK)
				INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
				WHERE t.trans_id = @new_trans_id
			END	 -- NON PORTFOLIO TRANSACTIONS
		SET @current_id = @current_id + 1
	END



UPDATE t
SET sent_to_acct_2 = GETUTCDATE(), change_date = GETUTCDATE(), change_user = 'SYSTEM'
FROM transactions t
INNER JOIN #tempAllData t2 ON t.trans_id = t2.trans_id



SELECT COALESCE(t.trans_id, '') AS [tranId], COALESCE(CONVERT(VARCHAR (50), t.trans_date), '') AS trandate, COALESCE(t.subsidiary, '') AS [subsidiary], 
	COALESCE(t.currency, '') AS [currency], t.postingperiod, t.journalItemLine_memo, t.journalItemLine_account, 
	COALESCE(CONVERT(VARCHAR (50), t.journalItemLine_debitAmount), '') AS [journalItemLine_debitAmount], 
	COALESCE(CONVERT(VARCHAR (50), t.journalItemLine_creditAmount), '') AS [journalItemLine_creditAmount]
FROM #tempAllData t
ORDER BY t.id


-- get an trans that do not balance
SELECT t.trans_id, COALESCE(SUM(t.journalItemLine_debitAmount), 0) AS debits, COALESCE(SUM(t.journalItemLine_creditAmount), 0) AS credits, 
	COALESCE(SUM(t.journalItemLine_debitAmount), 0) - COALESCE(SUM(t.journalItemLine_creditAmount), 0) AS diff
FROM #tempAllData t
LEFT JOIN transactions tr WITH (NOLOCK) ON t.trans_id = tr.trans_id
WHERE COALESCE(tr.trans_type_id, -1) <> 16
GROUP BY t.trans_id
HAVING COALESCE(SUM(t.journalItemLine_creditAmount), 0) <> COALESCE(SUM(t.journalItemLine_debitAmount), 0)


IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData
IF OBJECT_ID('tempdb..#tempAllData') IS NOT NULL DROP TABLE #tempAllData


GO
