SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-12-09
-- Description:	inserts a record into partial writeoffs table
-- =============================================
CREATE PROCEDURE [dbo].[CSP_InsPartialWriteoff]
(
	@funding_id				INT,
	@trans_date				DATE,
	@writeoff_amount		MONEY,
	@margin_adjust_amount	MONEY,
	@users_email			NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON;

INSERT INTO partial_writeoffs(funding_id, trans_date, writeoff_amount, margin_adjust_amount, processed, create_date, change_date, create_user, change_user)
VALUES (@funding_id, @trans_date, @writeoff_amount, @margin_adjust_amount, NULL, GETUTCDATE(), GETUTCDATE(), @users_email, @users_email)

	
END
GO
