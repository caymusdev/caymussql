SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-01-31
-- Description:	Called from the webpage to make changes to a funding payment date
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdFundingPaymentDates]
(
	@worker_id					INT,
	--@session_id					NVARCHAR (100), 
	@userid						NVARCHAR (100),
	@include_payment_plan		BIT,
	@merchant_bank_account_id	INT, 
	@payment_method				VARCHAR (50), 
	@payment_processor			VARCHAR (50), 
	@amount						MONEY,
	@start_date					DATE,
	@end_date					DATE,	
	@change_reason_id			INT,
	@comments					NVARCHAR (4000),
	@edit_type					VARCHAR (50),
	@frequency					VARCHAR (50), 
	@funding_id					BIGINT
)
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @tempDate DATE, @tempDate2 DATE
-- First need to check and see if this user needs approval for editing/creating payment schedules
DECLARE @approved BIT; SET @approved = 1

IF EXISTS (SELECT 1 FROM sec_payment_schedules s WHERE s.submitter_user_id = @userid)
	BEGIN
		SET @approved = 0
	END

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SELECT @tomorrow = dbo.CF_GetTomorrow(@today)
DECLARE @tempId INT, @maxId INT, @tempWorkerId INT, @payment_plan_id INT


IF (@start_date IS NULL OR @start_date < @tomorrow) AND @edit_type = 'modify'
	BEGIN
		SET @start_date = @tomorrow
	END

IF @start_date < @tomorrow
	BEGIN
		SELECT 'Payment Date cannot be in the past: ' + CONVERT(VARCHAR (50), @start_date) + '/' + CONVERT(VARCHAR (50), @worker_id) AS ErrorText
	END
ELSE
	BEGIN
		IF @include_payment_plan = 1
			BEGIN
				SELECT @payment_plan_id = pp.payment_plan_id
				FROM payment_plans pp
				INNER JOIN fundings f ON pp.merchant_id = f.merchant_id AND f.id = @funding_id				

				IF @payment_plan_id IS NULL
					BEGIN
						DECLARE @pending_perf_status INT; SELECT @pending_perf_status = id FROM payment_plan_status_codes WHERE code = 'Pending' AND parent_id IS NULL

						-- need to create it since it does not exist
						INSERT INTO payment_plans(merchant_id, affiliate_id, collector, current_status)
						SELECT f.merchant_id, f.affiliate_id, @userid, @pending_perf_status
						FROM fundings f WITH (NOLOCK)
						WHERE f.id = @funding_id

						SELECT @payment_plan_id = SCOPE_IDENTITY()

						INSERT INTO payment_plan_statuses (payment_plan_id, payment_plan_status_code, start_date, end_date, change_date, change_user)
						SELECT @payment_plan_id, c.id, CONVERT(DATE, GETUTCDATE()), NULL, GETUTCDATE(), 'SYSTEM'
						FROM payment_plan_status_codes c
						WHERE c.code = 'Pending'
					END
			END
		--------------

		IF @edit_type = 'multiEdit'
			BEGIN
				-- this is where they highlighted multiple dates and there were multiple entries within there.  
				-- Only editing the existing ones between start and end.  Will not create new entries
				UPDATE funding_payment_dates_work
				SET merchant_bank_account_id = @merchant_bank_account_id, payment_method = @payment_method, payment_processor = @payment_processor, amount = @amount, 
					change_reason_id = @change_reason_id, comments = @comments, change_date = GETUTCDATE(), change_user = @userid, changed = 1, revised_original = 0, 
					payment_plan_id = @payment_plan_id
				WHERE userid = @userid AND funding_id = @funding_id
					AND payment_date BETWEEN @start_date AND COALESCE(@end_date, '2050-01-01')

				IF @approved = 1
					BEGIN
						-- since these are all approved, we actually and unfortunately, need to loop through them and call the Approve stored procedure
						IF OBJECT_ID('tempdb..#tempWorkerIDs') IS NOT NULL DROP TABLE #tempWorkerIDs 
						CREATE TABLE #tempWorkerIDs(id int not null identity (1, 1), worker_id INT)

						INSERT INTO #tempWorkerIDs (worker_id)
						SELECT w.id
						FROM funding_payment_dates_work w
						WHERE userid = @userid AND funding_id = @funding_id AND w.payment_date BETWEEN @start_date AND COALESCE(@end_date, '2050-01-01')

						SELECT @maxId = MAX(id) FROM #tempWorkerIDs
						SET @tempId = 1
						WHILE @tempId <= @maxId
							BEGIN
								SELECT @tempWorkerId = worker_id FROM #tempWorkerIDs WHERE id = @tempId

								EXEC dbo.CSP_ApprovePaymentDateChange @tempWorkerId, @userid

								SET @tempId = @tempId + 1
							END
						
						IF OBJECT_ID('tempdb..#tempWorkerIDs') IS NOT NULL DROP TABLE #tempWorkerIDs 
					END
			END
		ELSE IF @edit_type = 'singleEdit'
			BEGIN
				-- only editing a single payment date
				UPDATE funding_payment_dates_work
				SET merchant_bank_account_id = @merchant_bank_account_id, payment_method = @payment_method, payment_processor = @payment_processor, amount = @amount, 
					change_reason_id = @change_reason_id, comments = @comments, change_date = GETUTCDATE(), change_user = @userid, changed = 1, revised_original = 0,
					payment_plan_id = @payment_plan_id, payment_date = @start_date
				WHERE userid = @userid AND funding_id = @funding_id AND id = @worker_id

				IF @approved = 1
					BEGIN
						EXEC dbo.CSP_ApprovePaymentDateChange @worker_id, @userid
					END
			END
		ELSE IF @edit_type = 'multiCreate'
			BEGIN
				-- no existing payments selected so create one for each day depending on frequency
				IF @end_date IS NULL SET @end_date = @start_date
				SET @tempDate = @start_date
		
				WHILE @tempDate <= @end_date
					BEGIN
						IF @frequency = 'Daily'
							BEGIN
								IF DATEPART(dw, @tempDate) IN (2,3,4,5,6) -- only do Monday Through Friday even though CC might have weekend amounts
									BEGIN
										INSERT INTO funding_payment_dates_work (userid, funding_payment_id, payment_date, amount, create_date, active, merchant_bank_account_id, 
											payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, change_date, change_user, approved, funding_id, changed, 
											payment_plan_id)
										VALUES (@userid, NULL, @tempDate, @amount, GETUTCDATE(), 1, @merchant_bank_account_id, @payment_method, @payment_processor, 
											NULL, @comments, @change_reason_id, GETUTCDATE(), @userid, 0, @funding_id, 1, @payment_plan_id)

										SELECT @tempWorkerId = SCOPE_IDENTITY()

										IF @approved = 1
											BEGIN
												EXEC dbo.CSP_ApprovePaymentDateChange @tempWorkerId, @userid
											END
									END
							END
						ELSE IF @frequency = 'Weekly'
							BEGIN
								IF DATEPART(dw, @tempDate) = DATEPART(dw, @start_date)
									BEGIN
										-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
										INSERT INTO funding_payment_dates_work (userid, funding_payment_id, payment_date, amount, create_date, active, merchant_bank_account_id, 
											payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, change_date, change_user, approved, funding_id, changed, 
											revised_original, payment_plan_id)
										VALUES (@userid, NULL, @tempDate, @amount, GETUTCDATE(), 1, @merchant_bank_account_id, @payment_method, @payment_processor, 
											NULL, @comments, @change_reason_id, GETUTCDATE(), @userid, 0, @funding_id, 1, 0, @payment_plan_id)

										SELECT @tempWorkerId = SCOPE_IDENTITY()

										IF @approved = 1
											BEGIN
												EXEC dbo.CSP_ApprovePaymentDateChange @tempWorkerId, @userid
											END
									END
							END
						ELSE IF @frequency = 'Monthly'
							BEGIN
								IF DAY(@tempDate) = DAY(@start_date)
									BEGIN
										-- this means we are on the same day of the week as the first payment so we need to insert as a future payment

										SET @tempDate2 = @tempDate
										-- if it is weekend, bring it back to friday
										SET @tempDate2 = DATEADD(DD, CASE DATEPART(dw, @tempDate) WHEN 1 THEN -2 WHEN 7 THEN -1 ELSE 0 END, @tempDate) 		
										
										INSERT INTO funding_payment_dates_work (userid, funding_payment_id, payment_date, amount, create_date, active, merchant_bank_account_id, 
											payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, change_date, change_user, approved, funding_id, changed, 
											revised_original, payment_plan_id)
										VALUES (@userid, NULL, @tempDate2, @amount, GETUTCDATE(), 1, @merchant_bank_account_id, @payment_method, @payment_processor, 
											NULL, @comments, @change_reason_id, GETUTCDATE(), @userid, 0, @funding_id, 1, 0, @payment_plan_id)

										SELECT @tempWorkerId = SCOPE_IDENTITY()

										IF @approved = 1
											BEGIN
												EXEC dbo.CSP_ApprovePaymentDateChange @tempWorkerId, @userid
											END									
									END
							END
						SET @tempDate = DATEADD(dd, 1, @tempDate)
					END
			END
		ELSE IF @edit_type = 'singleCreate'
			BEGIN
				-- simplest one, just create a single payment date
				INSERT INTO funding_payment_dates_work (userid, funding_payment_id, payment_date, amount, create_date, active, merchant_bank_account_id, 
					payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, change_date, change_user, approved, funding_id, changed, 
					payment_plan_id)
				VALUES (@userid, NULL, @start_date, @amount, GETUTCDATE(), 1, @merchant_bank_account_id, @payment_method, @payment_processor, 
					NULL, @comments, @change_reason_id, GETUTCDATE(), @userid, 0, @funding_id, 1, @payment_plan_id)

				SELECT @tempWorkerId = SCOPE_IDENTITY()

				IF @approved = 1
					BEGIN
						EXEC dbo.CSP_ApprovePaymentDateChange @tempWorkerId, @userid
					END
			END
		ELSE IF @edit_type = 'modify'
			BEGIN
				-- this is where the user picked to revise the original payment schedule
				-- easiest way to do this is to deactivate all dates between start and end that are from the original and then insert all
				-- new ones.
				IF @end_date IS NULL SET @end_date = '2050-01-01'

				-- RKB - 2020-03-27 - we are now turning off all future dates.  however, it's possible they have not all been loaded into the worktable
				-- so we'll load them now
				--EXEC dbo.CSP_GetFundingPaymentDates @funding_id, @start_date, @end_date, @userid
				-- can't just run the SP because it returns data but need to do the identical insert
				INSERT INTO funding_payment_dates_work (userid, funding_payment_id, payment_date, amount, create_date, active, merchant_bank_account_id, payment_method,
					payment_processor, payment_schedule_change_id, comments, change_reason_id, change_date, change_user, approved, changed, funding_id, payment_plan_id, missed)
				SELECT @userid, pd.id, pd.payment_date, pd.amount, GETUTCDATE(), pd.active, pd.merchant_bank_account_id, pd.payment_method, pd.payment_processor, 
					NULL AS payment_schedule_change_id, pd.comments, pd.change_reason_id, GETUTCDATE(), @userid, 0, 0, @funding_id, pd.payment_plan_id, COALESCE(pd.missed, 0)
				FROM funding_payment_dates pd WITH (NOLOCK)
				WHERE NOT EXISTS (
					SELECT 1
					FROM funding_payment_dates_work w
					WHERE w.payment_date = pd.payment_date AND w.funding_id = pd.funding_id AND w.userid = @userid AND w.amount = pd.amount  --AND COALESCE(w.approved, 0) = 0 AND w.reject_reason IS NULL
				) AND pd.payment_date BETWEEN @start_date AND @end_date
				AND pd.funding_id = @funding_id

				-- now we should have all future payments in the work table if they were not there before

				-- now need to go through and approve all of these, if approved = 1
				IF OBJECT_ID('tempdb..#tempWorkerIDs2') IS NOT NULL DROP TABLE #tempWorkerIDs2
				CREATE TABLE #tempWorkerIDs2(id int not null identity (1, 1), worker_id INT)

				-- this will be the list of dates we are about to deactivate
				INSERT INTO #tempWorkerIDs2 (worker_id)
				SELECT w.id
				FROM funding_payment_dates_work w
				INNER JOIN funding_payment_dates d ON w.funding_payment_id = d.id --  AND d.original = 1  --- RKB 2020-03-27 - decided to remove all future payments, not just original ones
						--- because a lot of merchants are modifying multiple times and this was leaving behind the "current" plan
				WHERE w.userid = @userid AND w.funding_id = @funding_id AND w.payment_date BETWEEN @start_date AND COALESCE(@end_date, '2050-01-01')
					AND w.active = 1

				-- deactivate all existing original payment dates
				UPDATE w
				SET active = 0, changed = 1, change_user = @userid, change_date = GETUTCDATE(), revised_original = 0
				FROM funding_payment_dates_work w
				INNER JOIN funding_payment_dates d ON w.funding_payment_id = d.id --  AND d.original = 1   --- RKB 2020-03-27 - decided to remove all future payments, not just original ones
						--- because a lot of merchants are modifying multiple times and this was leaving behind the "current" plan
				WHERE w.userid = @userid AND w.funding_id = @funding_id AND w.payment_date BETWEEN @start_date AND COALESCE(@end_date, '2050-01-01')
					AND w.active = 1

				-- if we call CSP_ApprovePaymentDateChange after inserting with revised_original = 1 then the approval is going to think it needs to approval
				-- all of the revisions because in the UI approval page only 1 shows up for all revisions.  So, above update will put revised_original = 0
				-- so that if the approval is called it will only approve that worker id.  then update revised original on the record

				IF @approved = 1
					BEGIN
						SELECT @maxId = MAX(id) FROM #tempWorkerIDs2
						SET @tempId = 1
						WHILE @tempId <= @maxId
							BEGIN
								SELECT @tempWorkerId = worker_id FROM #tempWorkerIDs2 WHERE id = @tempId

								EXEC dbo.CSP_ApprovePaymentDateChange @tempWorkerId, @userid

								SET @tempId = @tempId + 1
							END
					END

				UPDATE w
				SET revised_original = 1
				FROM funding_payment_dates_work w
				INNER JOIN #tempWorkerIDs2 t ON w.id = t.worker_id
						
				IF OBJECT_ID('tempdb..#tempWorkerIDs2') IS NOT NULL DROP TABLE #tempWorkerIDs2

				-- now insert all new ones
				DECLARE @payoff_amount MONEY, @remaining_amount MONEY, @orig_payment MONEY, @tempAmount MONEY
				SELECT @payoff_amount = f.payoff_amount
				FROM fundings f
				WHERE f.id = @funding_id

				SET @remaining_amount = @payoff_amount 
				SET @orig_payment = @amount -- @payment can be changed
				SET @tempDate = @start_date

				WHILE @remaining_amount > 0 
					BEGIN
						SET @tempAmount = @remaining_amount - @amount
						IF @tempAmount < 0
							BEGIN
								SET @amount = @remaining_amount
							END
						IF @frequency = 'Daily'
							BEGIN
								IF DATEPART(dw, @tempDate) IN (2,3,4,5,6) -- only do Monday Through Friday even though CC might have weekend amounts
									BEGIN
										INSERT INTO funding_payment_dates_work (userid, funding_payment_id, payment_date, amount, create_date, active, merchant_bank_account_id, 
											payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, change_date, change_user, approved, funding_id, changed, 
											revised_original, payment_plan_id)
										VALUES (@userid, NULL, @tempDate, @amount, GETUTCDATE(), 1, @merchant_bank_account_id, @payment_method, @payment_processor, 
											NULL, @comments, @change_reason_id, GETUTCDATE(), @userid, 0, @funding_id, 1, 0, @payment_plan_id)

										SELECT @tempWorkerId = SCOPE_IDENTITY()

										IF @approved = 1
											BEGIN
												EXEC dbo.CSP_ApprovePaymentDateChange @tempWorkerId, @userid
											END

										-- if we call CSP_ApprovePaymentDateChange after inserting with revised_original = 1 then the approval is going to think it needs to approval
										-- all of the revisions because in the UI approval page only 1 shows up for all revisions.  So, above insert will put revised_original = 0
										-- so that if the approval is called it will only approve that worker id.  then update revised original on the record

										UPDATE funding_payment_dates_work SET revised_original = 1 WHERE id = @tempWorkerId

										SET @remaining_amount = @remaining_amount - @amount
									END
							END
						ELSE IF @frequency = 'Weekly'
							BEGIN
								IF DATEPART(dw, @tempDate) = DATEPART(dw, @start_date)
									BEGIN
										-- this means we are on the same day of the week as the first payment so we need to insert as a future payment
										INSERT INTO funding_payment_dates_work (userid, funding_payment_id, payment_date, amount, create_date, active, merchant_bank_account_id, 
											payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, change_date, change_user, approved, funding_id, changed, 
											revised_original, payment_plan_id)
										VALUES (@userid, NULL, @tempDate, @amount, GETUTCDATE(), 1, @merchant_bank_account_id, @payment_method, @payment_processor, 
											NULL, @comments, @change_reason_id, GETUTCDATE(), @userid, 0, @funding_id, 1, 0, @payment_plan_id)

										SELECT @tempWorkerId = SCOPE_IDENTITY()

										IF @approved = 1
											BEGIN
												EXEC dbo.CSP_ApprovePaymentDateChange @tempWorkerId, @userid
											END

										-- if we call CSP_ApprovePaymentDateChange after inserting with revised_original = 1 then the approval is going to think it needs to approval
										-- all of the revisions because in the UI approval page only 1 shows up for all revisions.  So, above insert will put revised_original = 0
										-- so that if the approval is called it will only approve that worker id.  then update revised original on the record

										UPDATE funding_payment_dates_work SET revised_original = 1 WHERE id = @tempWorkerId

										SET @remaining_amount = @remaining_amount - @amount
									END
							END
						ELSE IF @frequency = 'Monthly'
							BEGIN
								IF DAY(@tempDate) = DAY(@start_date)
									BEGIN
										-- this means we are on the same day of the week as the first payment so we need to insert as a future payment

										SET @tempDate2 = @tempDate
										-- if it is weekend, bring it back to friday
										SET @tempDate2 = DATEADD(DD, CASE DATEPART(dw, @tempDate) WHEN 1 THEN -2 WHEN 7 THEN -1 ELSE 0 END, @tempDate) 		

										INSERT INTO funding_payment_dates_work (userid, funding_payment_id, payment_date, amount, create_date, active, merchant_bank_account_id, 
											payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, change_date, change_user, approved, funding_id, changed, 
											revised_original, payment_plan_id)
										VALUES (@userid, NULL, @tempDate2, @amount, GETUTCDATE(), 1, @merchant_bank_account_id, @payment_method, @payment_processor, 
											NULL, @comments, @change_reason_id, GETUTCDATE(), @userid, 0, @funding_id, 1, 0, @payment_plan_id)

										SELECT @tempWorkerId = SCOPE_IDENTITY()

										IF @approved = 1
											BEGIN
												EXEC dbo.CSP_ApprovePaymentDateChange @tempWorkerId, @userid
											END

										-- if we call CSP_ApprovePaymentDateChange after inserting with revised_original = 1 then the approval is going to think it needs to approval
										-- all of the revisions because in the UI approval page only 1 shows up for all revisions.  So, above insert will put revised_original = 0
										-- so that if the approval is called it will only approve that worker id.  then update revised original on the record

										UPDATE funding_payment_dates_work SET revised_original = 1 WHERE id = @tempWorkerId

										SET @remaining_amount = @remaining_amount - @amount
									END
							END
						-- increment @tempDate
						SET @tempDate = DATEADD(D, 1, @tempDate)		
						-- reset @payment variable
						SET @amount = @orig_payment						
					END	-- WHILE @remaining_amount > 0
			END
	END
END
GO
