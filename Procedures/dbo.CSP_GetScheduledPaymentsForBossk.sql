SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-26
-- Description:	Gets a list of expected payments to send to bossk
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetScheduledPaymentsForBossk]

AS
BEGIN
SET NOCOUNT ON;

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SET @tomorrow = dbo.CF_GetTomorrow(@today)
-- only going to send future payments to bossk


SELECT fpd.id AS scheduled_payment_id, f.affiliate_id, f.merchant_id, f.id AS contract_id, fpd.payment_date, fpd.amount AS payment_amount, fpd.payment_method, 
	'true' AS is_debit, mba.bank_name, mba.routing_number, mba.account_number
FROM funding_payment_dates fpd
INNER JOIN fundings f ON fpd.funding_id = f.id
INNER JOIN affiliates a ON f.affiliate_id = a.affiliate_id
LEFT JOIN merchant_bank_accounts mba ON fpd.merchant_bank_account_id = mba.merchant_bank_account_id
WHERE ((COALESCE(f.collection_type, '') <> '' AND f.payoff_amount > 0)
	OR (a.sent_to_collect IS NOT NULL AND a.closed_in_collections IS NULL))
	AND fpd.active = 1
	AND fpd.payment_date > @today

END
GO
