SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-09-13
-- Description: Changes just the text amount on a payment that needs to be approved
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdAmountOnPayment]
(	
	@payment_id			INT,
	@amount				MONEY,
	@userid				NVARCHAR (100)='SYSTEM'
)
AS
BEGIN	
SET NOCOUNT ON;

UPDATE payments 
SET amount = @amount, change_date = GETUTCDATE(), change_user = @userid
WHERE payment_id = @payment_id

END
GO
