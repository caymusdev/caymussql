SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2021-03-13
-- Description: Approves a scheduled payment and moves into the funding payment dates portfolio table
-- =============================================
CREATE PROCEDURE dbo.[BSK_ApproveSchedPayment]
(
	@scheduled_payment_id			INT,
	@users_email					NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON

DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
DECLARE @user_id INT; SELECT @user_id = user_id FROM users WHERE email = @users_email
DECLARE @newID INT, @payment_amount MONEY, @affiliate_id BIGINT, @payment_date DATE, @funding_payment_id INT, @row_count INT, @deleted BIT

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SELECT @tomorrow = dbo.BF_GetTomorrow(@today)


BEGIN TRANSACTION

SELECT @payment_amount = p.payment_amount, @affiliate_id = p.affiliate_id, @payment_date = p.payment_date, @funding_payment_id = p.funding_payment_id, 
	@deleted = p.deleted
FROM scheduled_payments p
WHERE p.scheduled_payment_id = @scheduled_payment_id

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERRORPAYMENT

-- if deleted
IF COALESCE(@deleted, 0) = 1
	BEGIN
		UPDATE funding_payment_dates
		SET active = 0, payment_schedule_change_id = @scheduled_payment_id, change_user = @users_email, change_date = GETUTCDATE()
		WHERE id = @funding_payment_id

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERRORPAYMENT

		-- update any payment that might be in the queue for tomorrow
		UPDATE p
		SET deleted = 1, change_user = @users_email, change_date = GETUTCDATE()
		FROM payments p
		WHERE p.funding_payment_date_id = @funding_payment_id AND COALESCE(p.waived, 0) = 0
			AND COALESCE(p.deleted, 0) = 0 AND p.processed_date IS NULL AND p.approved_flag = 1
			AND p.trans_date = @tomorrow

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERRORPAYMENT
	END
ELSE
	BEGIN
		-- need a temp table to store all the active fundings for this affiliate and how much of the payment each should get applied.
		IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData
		CREATE TABLE #tempData (id INT IDENTITY (1, 1), funding_id BIGINT, payment_amount MONEY)

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERRORPAYMENT

		INSERT INTO #tempData (funding_id, payment_amount)
		SELECT f.id, 
			ROUND(@payment_amount * f.payoff_amount / 
				(SELECT SUM(payoff_amount) FROM fundings f2 WHERE f2.affiliate_id = f.affiliate_id AND f2.payoff_amount > 0 AND f2.contract_status = 1), 2)
		FROM fundings f
		WHERE f.affiliate_id = @affiliate_id AND f.payoff_amount > 0 AND f.contract_status = 1

		SELECT @row_count = @@ROWCOUNT, @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERRORPAYMENT

		-- if there were no rows then there are no active accounts so we need to now check writeoffs
		IF @row_count = 0
			BEGIN
				INSERT INTO #tempData (funding_id, payment_amount)
				SELECT f.id, 
					ROUND(@payment_amount * f.payoff_amount / 
						(SELECT SUM(payoff_amount) FROM fundings f2 WHERE f2.affiliate_id = f.affiliate_id AND f2.payoff_amount > 0), 2)
				FROM fundings f
				WHERE f.affiliate_id = @affiliate_id AND f.payoff_amount > 0
			END

		-- now, in case of any rounding issues keeping us short or too much
		UPDATE #tempData
		SET payment_amount = payment_amount + (@payment_amount - (SELECT SUM(payment_amount) FROM #tempData))
		WHERE id = 1

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERRORPAYMENT

		-- now, we know how much of the payment needs to go to each funding


		-- thiS is a new payment.
		IF @funding_payment_id IS NULL
			BEGIN
				INSERT INTO funding_payment_dates (funding_id, payment_date, amount, rec_amount, payment_schedule_id, create_date, active, original, chargeback_amount, 
					merchant_bank_account_id, payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, change_date, change_user, 
					payment_plan_id, scheduled_payment_id, affiliate_id)
				SELECT t.funding_id, p.payment_date, t.payment_amount, 0, NULL, GETUTCDATE(), 1, 0, NULL, p.merchant_bank_account_id, p.payment_method, 'Forte', 
					NULL, p.comments, p.change_reason_id, GETUTCDATE(), @users_email, p.payment_plan_id, p.scheduled_payment_id, p.affiliate_id
				FROM scheduled_payments p
				INNER JOIN #tempData t ON 1 = 1 -- we want one record for each funding	
				WHERE p.scheduled_payment_id = @scheduled_payment_id
							
				SELECT @newID = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERRORPAYMENT

				UPDATE scheduled_payments 
				SET funding_payment_id = @newID, approved_user = @users_email, approved_date = GETUTCDATE(), change_date = GETUTCDATE(), change_user = @users_email
				WHERE scheduled_payment_id = @scheduled_payment_id

				SELECT  @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERRORPAYMENT

				INSERT INTO payments (payment_schedule_id, amount, trans_date, transaction_id, approved_flag, processed_date, change_date, change_user, previous_transaction_id,
					orig_transaction_id, is_fee, retry_level, retry_sublevel, funding_id, complete, batch_id, retry_complete, processor, is_refund, is_debit, deleted, 
					waived, redirect_funding_id, redirect_approval_id, funding_payment_date_id)
				SELECT NULL, pd.amount, pd.payment_date, NULL, 1, NULL, GETUTCDATE(), @users_email, NULL, NULL, 0, 0, 0, pd.funding_id, NULL, NULL, NULL, NULL, 0, 1, 0, 
					0, NULL, NULL, pd.id
				FROM funding_payment_dates pd
				WHERE pd.id = @newID AND pd.payment_date = @tomorrow

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERRORPAYMENT
							
				-- now need to set the off hold date if this funding is on hold
				INSERT INTO funding_off_hold_dates (funding_id, off_hold_date, create_date, change_date, change_user)
				SELECT f.id, dbo.CF_GetYesterday(@payment_date), GETUTCDATE(), GETUTCDATE(), @users_email
				FROM fundings f
				INNER JOIN #tempData t ON f.id = t.funding_id
				WHERE f.on_hold = 1
					AND NOT EXISTS (
						SELECT 1
						FROM funding_off_hold_dates o
						WHERE o.funding_id = f.id AND o.off_hold_date BETWEEN @today AND dbo.CF_GetYesterday(@payment_date)
					)

			END		
		ELSE -- existing payment
			BEGIN
				DECLARE @orig_date DATE
				SELECT @orig_date = d.payment_date
				FROM funding_payment_dates d
				WHERE d.id = @funding_payment_id

				UPDATE d
				SET d.amount = p.payment_amount, d.merchant_bank_account_id = p.merchant_bank_account_id, d.payment_method = p.payment_method, d.payment_processor = 'Forte', 
					d.payment_schedule_change_id = NULL, d.comments = p.comments, d.change_reason_id = p.change_reason_id, change_date = GETUTCDATE(), change_user = p.change_user,
					d.payment_plan_id = p.payment_plan_id, d.payment_date = p.payment_date,  -- in case they moved the payment
					d.scheduled_payment_id = p.scheduled_payment_id
				FROM funding_payment_dates d
				INNER JOIN scheduled_payments p ON d.id = p.funding_payment_id	
				WHERE p.scheduled_payment_id = @scheduled_payment_id

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERRORPAYMENT

				-- update any payment that might be in the queue for tomorrow
				UPDATE p
				SET amount = @payment_amount, change_date = GETUTCDATE(), change_user = @users_email
				FROM payments p
				WHERE p.funding_payment_date_id = @funding_payment_id AND COALESCE(p.waived, 0) = 0
					AND COALESCE(p.deleted, 0) = 0 AND p.processed_date IS NULL AND p.approved_flag = 1
					AND p.trans_date = @tomorrow AND @payment_date = @tomorrow  -- only update the amount on the payment if they have not moved the date

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERRORPAYMENT

				-- need to delete a payment if they have moved tomorrow's payment
				UPDATE payments
				SET deleted = 1, change_date = GETUTCDATE(), change_user = @users_email
				WHERE funding_payment_date_id = @funding_payment_id AND COALESCE(waived, 0) = 0 AND COALESCE(deleted, 0) = 0
					AND processed_date IS NULL AND approved_flag = 1 AND trans_date = @tomorrow AND @payment_date != @tomorrow

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERRORPAYMENT

				-- need to create a payment if they moved it to tomorrow
				IF @payment_date != @orig_date AND @payment_date = @tomorrow
					BEGIN
						INSERT INTO payments (payment_schedule_id, amount, trans_date, transaction_id, approved_flag, processed_date, change_date, change_user, previous_transaction_id,
							orig_transaction_id, is_fee, retry_level, retry_sublevel, funding_id, complete, batch_id, retry_complete, processor, is_refund, is_debit, deleted, 
							waived, redirect_funding_id, redirect_approval_id, funding_payment_date_id)
						SELECT NULL, pd.amount, pd.payment_date, NULL, 1, NULL, GETUTCDATE(), @users_email, NULL, NULL, 0, 0, 0, pd.funding_id, NULL, NULL, NULL, NULL, 0, 1, 0, 
							0, NULL, NULL, pd.id
						FROM funding_payment_dates pd
						WHERE pd.id = @funding_payment_id 

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERRORPAYMENT
					END

				-- now need to set the off hold date if this funding is on hold
				INSERT INTO funding_off_hold_dates (funding_id, off_hold_date, create_date, change_date, change_user)
				SELECT f.id, dbo.CF_GetYesterday(@payment_date), GETUTCDATE(), GETUTCDATE(), @users_email
				FROM fundings f
				INNER JOIN #tempData t ON f.id = t.funding_id
				WHERE f.on_hold = 1
					AND NOT EXISTS (
						SELECT 1
						FROM funding_off_hold_dates o
						WHERE o.funding_id = f.id AND o.off_hold_date BETWEEN @today AND dbo.CF_GetYesterday(@payment_date)
					)

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()	; IF @errorNum != 0 GOTO ERRORPAYMENT				
			END	

		-- need to set any active cases to inactive status because we have a payment
		UPDATE cases
		SET case_status_id = (SELECT case_status_id FROM case_statuses WHERE status = 'Inactive'), change_date = GETUTCDATE(), change_user = @users_email
		WHERE affiliate_id = @affiliate_id AND case_status_id = (SELECT case_status_id FROM case_statuses WHERE status = 'Active')

	END

UPDATE scheduled_payments 
SET approved_user = @users_email, approved_date = GETUTCDATE(), change_date = GETUTCDATE(), change_user = @users_email
WHERE scheduled_payment_id = @scheduled_payment_id

SELECT  @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERRORPAYMENT

COMMIT TRANSACTION
GOTO DonePAYMENT

ERRORPAYMENT:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)


DonePAYMENT:

END

GO
