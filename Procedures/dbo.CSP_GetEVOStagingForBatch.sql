SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-06-08
-- Description:	returns records for a given batch c# then loops through and creates transactions records
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetEVOStagingForBatch]
(
	@batch_id			INT
)

AS
BEGIN
SET NOCOUNT ON;

SELECT s.evo_staging_id
FROM evo_staging s
WHERE s.BatchID = @batch_id
ORDER BY s.[Date]


END
GO
