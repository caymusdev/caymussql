SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-07-19
-- Description:	Inserts the transactions from either an advice file or from a returns file
-- =============================================
CREATE PROCEDURE [dbo].[CSP_InsNachaTransFile]
(
	@batch_id						[INT],
	@funding_id						[BIGINT],
	@payment_id						[BIGINT],
	@reason_code					[VARCHAR] (50),
	@settle_type					[VARCHAR] (50),
	@trans_amount					[MONEY],
	@company_name					[NVARCHAR] (100),
	@settle_date					[DATETIME],
	@file_name						[VARCHAR] (100)
)
AS
BEGIN	
SET NOCOUNT ON;

INSERT INTO nacha_trans(batch_id, funding_id, payment_id, reason_code, settle_type, trans_amount, company_name, settle_date, file_name)
VALUES (@batch_id, @funding_id, @payment_id, @reason_code, @settle_type, @trans_amount, @company_name, @settle_date, @file_name)

END
GO
