SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-11-06
-- Description:	Deletes a payment
-- =============================================
CREATE PROCEDURE [dbo].[CSP_DelPayment]
(
	@payment_id			INT,
	@userid				NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON;

UPDATE payments
SET change_user = @userid, change_date = GETUTCDATE(), deleted = 1
WHERE payment_id = @payment_id

-- DELETE FROM payments WHERE payment_id = @payment_id

END
GO
