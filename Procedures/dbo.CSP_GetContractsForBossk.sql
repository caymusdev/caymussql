SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-16
-- Description:	Gets a list of fundings to send to bossk
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetContractsForBossk]

AS
BEGIN
SET NOCOUNT ON;


SELECT f.affiliate_id, a.affiliate_name, m.merchant_id, m.merchant_name, f.id AS contract_id, f.contract_number, 
	f.payoff_amount AS gross_due, f.fees_out AS fees_outstanding, f.portfolio_value AS original_rtr, f.pp_out AS pp_outstanding, CONVERT(DATE, f.writeoff_date) AS writeoff_date,
	CASE f.payoff_amount WHEN 0 THEN 'Closed' ELSE 'Active' END AS collect_status
FROM fundings f
INNER JOIN merchants m ON f.merchant_id = m.merchant_id
INNER JOIN affiliates a ON f.affiliate_id = a.affiliate_id
WHERE ((COALESCE(f.collection_type, '') <> '' AND f.payoff_amount > 0)
	OR (a.sent_to_collect IS NOT NULL AND a.closed_in_collections IS NULL))
ORDER BY a.affiliate_name, m.merchant_name, f.contract_number


END

GO
