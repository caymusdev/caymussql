SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

 -- =============================================
-- Author:      Ryan Brown
-- Create Date: 2020-09-14  -- Happy bday Mykle
-- Description: Pass in a grid and it returns all headers translated so that we don't have to have separate calls to db for each column
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetGridHeadings]
(
	@grid_name				NVARCHAR (100)	
)
AS
BEGIN
SET NOCOUNT ON

IF @grid_name = 'Roles'	
	SELECT dbo.BF_TranslateWord('RoleID') AS RoleID, dbo.BF_TranslateWord('RoleName') AS RoleName, dbo.BF_TranslateWord('RoleDesc') AS RoleDesc
ELSE IF @grid_name = 'UserRoles'
	SELECT dbo.BF_TranslateWord('RoleName') AS RoleName, dbo.BF_TranslateWord('RoleDesc') AS RoleDesc, dbo.BF_TranslateWord('HasRole') AS HasRole
ELSE IF @grid_name = 'Permissions'
	SELECT dbo.BF_TranslateWord('PermissionName') AS PermissionName, dbo.BF_TranslateWord('PermissionDesc') AS PermissionDesc
ELSE IF @grid_name = 'Labels'
    Select dbo.BF_TranslateWord('LabelName') AS LabelName, dbo.BF_TranslateWord('LabelDesc') AS LabelDesc, dbo.BF_TranslateWord('CustomLabel') AS CustomLabel
ELSE IF @grid_name = 'RolePermissions'
	SELECT dbo.BF_TranslateWord('PermissionName') AS PermissionName, dbo.BF_TranslateWord('PermissionDesc') AS PermissionDesc, dbo.BF_TranslateWord('HasPermission') AS HasPermission
ELSE IF @grid_name = 'Users'
    SELECT dbo.BF_TranslateWord('UserID') AS UserID, dbo.BF_TranslateWord('UserEmail') AS Email, dbo.BF_TranslateWord('Timezone') AS Timezone
ELSE IF @grid_name = 'Alerts'
	SELECT dbo.BF_TranslateWord('AlertType') AS AlertType, dbo.BF_TranslateWord('AlertTime') AS AlertTime, dbo.BF_TranslateWord('Affiliate') AS Affiliate, 
		dbo.BF_TranslateWord('AlertMessage') AS AlertMessage
ELSE
	SELECT '' AS NoGrid

END

GO
