SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-02-25
-- Description:	This will create funding payment dates for active fundings not on hold and don't have an approved deleted payment in the future
-- Used to make sure all active fundings have a payment for sometime in the future
-- Needs to run before GeneratePayments
-- RKB - 2020-03-10- Per Scott, turning this off for now.  The scenario is if they do an attempt on an old account that will take the funding off hold
-- and then this procedure will then create a new payment date even though the user only wanted to try once.  And if the try once rejects it won't be in time
-- before more payments go out from this normal process here.  So, for now, changing this to not do anything
-- RKB - 2020-04-03 - Have a new field on fundings, auto_payment_dates, that users can turn off so that this does not happen.  But we need it to happen 
-- for any near ending fundings that ever missed a payment.  The system needs to generate another payment since they still have a balance.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GenerateMissingFundingPaymentDates]
AS
BEGIN	
SET NOCOUNT ON;

--  RETURN --- NOT DOING THIS CURRENTLY -- 2020-03-10 (*updated on 2020-04-03 (in dev))

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SELECT @tomorrow = DATEADD(DD, CASE DATEPART(WEEKDAY, @today) WHEN 6 THEN 3 WHEN 7 THEN 2 ELSE 1 END, @today)

INSERT INTO funding_payment_dates(funding_id, payment_date, amount, payment_schedule_id, rec_amount, original, active, 
	merchant_bank_account_id, payment_method, payment_processor, payment_schedule_change_id, comments, change_reason_id, payment_plan_id, change_user, change_date)
SELECT f.id, x.NextPayment, 
	-- amount should not be more than they owe
	CASE WHEN COALESCE(ps.amount, CASE f.frequency WHEN 'Daily' THEN f.weekday_payment ELSE f.weekly_payment END) > f.payoff_amount 
		THEN f.payoff_amount ELSE COALESCE(ps.amount, CASE f.frequency WHEN 'Daily' THEN f.weekday_payment ELSE f.weekly_payment END) END, NULL, NULL AS rec_amount, 0 AS original, 1 AS active,
	COALESCE(ba.merchant_bank_account_id, ba2.merchant_bank_account_id), f.hard_offer_type, f.processor, NULL, 'System Generated', NULL, NULL, 'SYSTEM', GETUTCDATE()
FROM fundings f
LEFT JOIN merchant_bank_accounts ba ON f.merchant_id = ba.merchant_id AND f.receivables_bank_name = ba.bank_name AND f.receivables_aba = ba.routing_number
	AND f.receivables_account_nbr = ba.account_number
LEFT JOIN (
	SELECT z.funding_id, 
		CASE WHEN  
		  -- adjust for weekends only (not holidays)  
		  CASE DATEPART(WEEKDAY, z.NextPayment)   
		   WHEN 7 THEN DATEADD(D, 2, z.NextPayment)  
		   WHEN 1 THEN DATEADD(D, 1, z.NextPayment)  
		   ELSE z.NextPayment END < @tomorrow THEN @tomorrow   
		  ELSE  
		   CASE DATEPART(WEEKDAY, z.NextPayment)   
		   WHEN 7 THEN DATEADD(D, 2, z.NextPayment)  
		   WHEN 1 THEN DATEADD(D, 1, z.NextPayment)  
		   ELSE z.NextPayment END  
		  END AS NextPayment
	FROM (
		SELECT f1.id AS funding_id, 
			CASE WHEN y.last_payment_date IS NULL THEN COALESCE(f1.first_payment_date, GETDATE())  
			ELSE DATEADD(D, CASE f1.frequency WHEN 'Daily' THEN 1 WHEN 'Weekly' THEN 7 END, CONVERT(DATE, y.last_payment_date)) END AS NextPayment
		FROM fundings f1
		LEFT JOIN (
			SELECT MAX(trans_date) AS last_payment_date, p.funding_id
			FROM payments p  
			WHERE p.processed_date IS NOT NULL  
			GROUP BY p.funding_id
		) y ON f1.id = y.funding_id
	) z
) x ON f.id = x.funding_id
LEFT JOIN (
	SELECT fps.funding_id, MAX(ps.payment_schedule_id) AS latest_payment_schedule_id
	FROM funding_payment_schedules fps
	INNER JOIN payment_schedules ps ON fps.payment_schedule_id = ps.payment_schedule_id
	WHERE ps.start_date <= @today AND COALESCE(ps.end_date, '2050-01-01') >= @tomorrow AND ps.active = 1 AND ps.payment_method = 'ACH'
	GROUP BY fps.funding_id
) y ON f.id = y.funding_id
LEFT JOIN payment_schedules ps ON y.latest_payment_schedule_id = ps.payment_schedule_id
LEFT JOIN merchant_bank_accounts ba2 ON ps.merchant_bank_account_id = ba2.merchant_bank_account_id
WHERE NOT EXISTS (SELECT 1
					FROM funding_payment_dates pd
					WHERE pd.funding_id = f.id AND pd.payment_date >= @tomorrow AND pd.active = 1)
	AND NOT EXISTS (
				SELECT 1
				FROM funding_payment_dates_work w
				WHERE w.funding_id = f.id AND w.payment_date >= @tomorrow AND w.active = 0 AND w.approved = 1
					)
	AND COALESCE(f.on_hold, 0) = 0
	AND f.hard_offer_type = 'ACH'
	AND (f.auto_payment_dates = 1 OR (f.rtr_out = 0 AND COALESCE(f.fees_out, 0) > 0))-- regardless of auto payment dates setting, if funding is paid off except fees we want it to happen
	AND f.contract_status IN (1)
	AND f.payoff_amount > 0
	AND x.NextPayment = @tomorrow
	AND f.processor IS NOT NULL
ORDER BY f.legal_name



END
GO
