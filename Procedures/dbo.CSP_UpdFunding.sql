SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-05-29
-- Description:	Updates a funding.  Called from web page
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdFunding]
(
	@funding_id				BIGINT,
	@contract_status		INT,
	@bank_name				NVARCHAR (100),
	@acct_nbr				NVARCHAR (100),
	@routing_nbr			NVARCHAR (100),
	@change_user			NVARCHAR (100),
	@on_hold				BIT,
	@writeoff_date			DATETIME,
	@do_retries				BIT,
	@process_fees			BIT,
	@auto_payment_dates		BIT,
	@update_future_payments	BIT
)
AS
BEGIN	
SET NOCOUNT ON;

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())

DECLARE @original_on_hold BIT, @orig_acct_number NVARCHAR (100), @orig_routing_nbr NVARCHAR (100), @merchant_id BIGINT
SELECT @original_on_hold = COALESCE(on_hold, 0), @orig_acct_number = receivables_account_nbr, @orig_routing_nbr = receivables_aba, @merchant_id = merchant_id 
FROM fundings 
WHERE id = @funding_id

UPDATE fundings 
SET contract_status = @contract_status, receivables_bank_name = @bank_name, receivables_account_nbr = @acct_nbr, receivables_aba = @routing_nbr, change_date = GETUTCDATE(), 
	change_user = @change_user, on_hold = @on_hold, writeoff_date = @writeoff_date, do_retries = @do_retries,  process_fees = @process_fees, auto_payment_dates = @auto_payment_dates
WHERE id = @funding_id

IF @on_hold = 1 AND @original_on_hold = 0
	BEGIN
		UPDATE fundings SET on_hold_date = GETUTCDATE() WHERE id = @funding_id
	END

IF @orig_acct_number != @acct_nbr OR @orig_routing_nbr != @routing_nbr
	BEGIN
		-- need to enter this as a new merchant account
		INSERT INTO merchant_bank_accounts (merchant_id, bank_name, routing_number, account_number, account_status, create_date, change_date, change_user, nickname)
		SELECT f.merchant_id, @bank_name, @routing_nbr, @acct_nbr, 'Active', GETUTCDATE(), GETUTCDATE(), @change_user, 
			COALESCE(@bank_name, '') + ': ' + COALESCE(@routing_nbr, '') + ' / ' + COALESCE(@acct_nbr, '') AS nickname
		FROM fundings f
		WHERE f.id = @funding_id
			AND NOT EXISTS (SELECT 1 FROM merchant_bank_accounts ba
							WHERE ba.merchant_id = f.merchant_id AND ba.account_number = @acct_nbr AND ba.routing_number = @routing_nbr)
	END


IF @update_future_payments = 1
	BEGIN
		DECLARE @merchant_bank_account_id INT
		SELECT @merchant_bank_account_id = merchant_bank_account_id 
		FROM merchant_bank_accounts ba
		WHERE ba.merchant_id = @merchant_id AND ba.routing_number = @routing_nbr AND ba.account_number = @acct_nbr AND account_status = 'Active'

		UPDATE funding_payment_dates 
		SET merchant_bank_account_id = @merchant_bank_account_id, change_user = @change_user, change_date = GETUTCDATE()
		WHERE funding_id = @funding_id AND payment_date > @today AND merchant_bank_account_id != @merchant_bank_account_id
	END


END
GO
