SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-10-10
-- Description:	Gets fundings that will be finishing soon
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetFundingsFinishingSoon] 

AS
BEGIN
SET NOCOUNT ON;

SELECT f.id, f.contract_number, f.legal_name, f.payoff_amount, f.float_amount, f.estimated_turn, f.calc_turn, f.est_months_left, f.last_payment_date, s.contract_status,
	f.processor
FROM fundings f
INNER JOIN contract_statuses s ON f.contract_status = s.contract_status_id
WHERE f.est_months_left <= 1
	AND (f.contract_status IN (1,3)
		OR 
		(f.contract_status = 4 AND f.contract_substatus_id = 1)
		)
	AND f.completed_date IS NULL
	AND COALESCE(f.on_hold, 0) = 0
ORDER BY f.legal_name
END
GO
