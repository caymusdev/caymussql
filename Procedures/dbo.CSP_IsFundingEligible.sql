SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2022-04-27
-- Description:	returns eligiblity values.  Used on the underwriting side before funding
-- =============================================
CREATE PROCEDURE [dbo].[CSP_IsFundingEligible]
(
	@application_id				BIGINT,
	@estimated_turn				NUMERIC (10, 2),
	@purchase_price				MONEY,
	@first_position				BIT,
	@pricing_ratio				NUMERIC (10, 2),
	@frequency					VARCHAR (50),
	@first_payment_date			DATE,
	@users_email				NVARCHAR (200),
	@incorp_state				VARCHAR (50),
	@industry					VARCHAR (50),
	@rtr						MONEY
)
AS
BEGIN
SET NOCOUNT ON;

-- if rules change here, also update [CSP_UpdateEligibility]

DECLARE @today DATETIME; SET @today = CONVERT(DATE, DATEADD(HH, -5, GETUTCDATE()))
DECLARE @incorp_state_temp VARCHAR (50), @industry_temp VARCHAR (50)

DECLARE @incorp_state_percent NUMERIC (10, 2)
SELECT @incorp_state_temp = x.incorp_state, @incorp_state_percent = 
	CONVERT(NUMERIC(10, 2), CONVERT(NUMERIC(10, 2), SUM(x.state_count)) * 100.00 / CONVERT(NUMERIC(10, 2), (SELECT COUNT(*) FROM fundings 
		WHERE contract_status = 1 AND portfolio = 'spv')+1)) 
FROM (
	SELECT f.incorp_state, COUNT(*) AS state_count
	FROM fundings f
	WHERE f.contract_status = 1 AND f.portfolio = 'spv'
	GROUP BY f.incorp_state
	UNION ALL
	SELECT @incorp_state, 1
) x
WHERE x.incorp_state = @incorp_state
GROUP BY x.incorp_state



DECLARE @industry_percent NUMERIC (10, 2)
SELECT @industry_temp = x.industry, @industry_percent = 
	CONVERT(NUMERIC(10, 2), CONVERT(NUMERIC(10, 2), SUM(x.state_count)) * 100.00 / CONVERT(NUMERIC(10, 2), (SELECT COUNT(*) FROM fundings 
		WHERE contract_status = 1 AND portfolio = 'spv')+1)) 
FROM (
	SELECT f.industry, COUNT(*) AS state_count
	FROM fundings f
	WHERE f.contract_status = 1 AND f.portfolio = 'spv'
	GROUP BY f.industry
	UNION ALL
	SELECT @industry, 1
) x
WHERE x.industry = @industry
GROUP BY x.industry


DECLARE @avg_balance MONEY
SELECT @avg_balance = AVG(x.payoff_amount)
FROM (
	SELECT payoff_amount
	FROM fundings f
	WHERE f.contract_status = 1 AND f.portfolio = 'spv'
	UNION ALL 
	SELECT @rtr
) x


DECLARE @remaining_term NUMERIC (10, 2)
SELECT @remaining_term = CONVERT(NUMERIC(10, 2), ((((SELECT COUNT(*) FROM fundings f WHERE f.contract_status = 1 AND f.est_months_left > 12 AND f.portfolio = 'spv')
	+ CASE WHEN @estimated_turn > 12 THEN 1 ELSE 0 END) * 100.00) 
	/ ((SELECT COUNT(*) FROM fundings WHERE contract_status = 1 AND portfolio = 'spv') + 
		CASE WHEN @estimated_turn > 12 THEN 1 ELSE 0.001 END))) 


SELECT x.application_id, x.users_email, x.estimated_turn, x.estimated_turn_max, x.estimated_turn_eligible, 
	x.purchase_price, x.purchase_price_max, x.purchase_price_eligible, x.first_position, x.first_position_requirement, x.first_position_eligible,
	x.pricing_ratio, x.pricing_ratio_min, x.pricing_ratio_eligible, x.frequency, x.frequency_requirement, x.frequency_eligible,
	x.first_payment_date, x.first_payment_requirement, x.first_payment_eligible, 
	x.incorp_state_percent, x.incorp_state, x.incorp_state_percent_max, x.incorp_state_eligible,
	x.industry_percent, x.industry, x.industry_percent_max, x.industry_eligible,
	x.avg_balance, x.avg_balance_max, x.avg_balance_eligible, x.remaining_term, x.remaining_term_max, x.remaining_term_eligible,
	CASE WHEN x.estimated_turn_eligible = 0 OR x.purchase_price_eligible = 0 OR x.first_position_eligible = 0 OR x.pricing_ratio_eligible = 0
		OR x.frequency_eligible = 0 OR x.first_payment_eligible = 0 OR x.incorp_state_eligible = 0 OR x.industry_eligible = 0 or x.avg_balance_eligible = 0
		OR x.remaining_term_eligible = 0 THEN 0 ELSE 1 END AS funding_eligible
FROM (
	SELECT @application_id AS application_id, @users_email AS users_email,
		@estimated_turn AS estimated_turn, 15 AS estimated_turn_max, CASE WHEN @estimated_turn <= 15 THEN 1 ELSE 0 END AS estimated_turn_eligible,
		@purchase_price AS purchase_price, 300000 AS purchase_price_max, CASE WHEN @purchase_price <= 300000 THEN 1 ELSE 0 END AS purchase_price_eligible,
		@first_position AS first_position, 1 AS first_position_requirement, CASE WHEN @first_position = 1 THEN 1 ELSE 0 END AS first_position_eligible,
		@pricing_ratio AS pricing_ratio, 1.2 AS pricing_ratio_min, CASE WHEN @pricing_ratio >= 1.2 THEN 1 ELSE 0 END AS pricing_ratio_eligible,
		@frequency AS frequency, 'Daily or Weekly' AS frequency_requirement, CASE WHEN @frequency IN ('Daily', 'Weekly') THEN 1 ELSE 0 END AS frequency_eligible,
		@first_payment_date AS first_payment_date, @today + 7 AS first_payment_requirement, 
			CASE WHEN DATEDIFF(DD, @today, @first_payment_date) <= 7 THEN 1 ELSE 0 END AS first_payment_eligible,
		@incorp_state_percent AS incorp_state_percent, @incorp_state_temp AS incorp_state, 20.00 AS incorp_state_percent_max, 
			CASE WHEN @incorp_state_percent <= 20.00 THEN 1 ELSE 0 END AS incorp_state_eligible,
		@industry_percent AS industry_percent, @industry_temp AS industry, 35.00 AS industry_percent_max, CASE WHEN @industry_percent <= 35.00 THEN 1 ELSE 0 END AS industry_eligible,
		@avg_balance AS avg_balance, 100000.00 AS avg_balance_max, CASE WHEN @avg_balance <= 100000.00 THEN 1 ELSE 0 END AS avg_balance_eligible,
		@remaining_term AS remaining_term, 50.00 AS remaining_term_max, CASE WHEN @remaining_term <= 50.00 THEN 1 ELSE 0 END AS remaining_term_eligible
) x


END
GO
