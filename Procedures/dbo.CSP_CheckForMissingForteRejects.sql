SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2022-11-17
-- Description: Checks for forte settlements that rejected but did not create a rejection record
-- =============================================
CREATE PROCEDURE CSP_CheckForMissingForteRejects

AS
BEGIN
SET NOCOUNT ON

SELECT fs.forte_settle_id 
FROM forte_settlements fs
WHERE fs.settle_type = 'reject' AND fs.settle_response_code LIKE 'R%'
	AND NOT EXISTS(SELECT * FROM transactions WHERE trans_type_id = 21 AND transaction_id = fs.transaction_id)
	AND settle_date >= '2022-01-01'
ORDER BY settle_date DESC

END
GO
