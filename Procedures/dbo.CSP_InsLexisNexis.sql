SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:     Ryan Brown
-- Create Date: 2021-05-26
-- Description: Saves a LexisNexis Record
-- =============================================
CREATE PROCEDURE dbo.CSP_InsLexisNexis
(
	@merchant_id			BIGINT,
	@request_type			VARCHAR (50),
	@raw_request			NVARCHAR (MAX),
	@raw_response			NVARCHAR (MAX)
)
AS
BEGIN
SET NOCOUNT ON

INSERT INTO lexis_nexis (merchant_id, request_type, raw_request, raw_response ,create_date, create_user, change_date, change_user)
VALUES (@merchant_id, @request_type, @raw_request, @raw_response, GETUTCDATE(), 'SYSTEM', GETUTCDATE(), 'SYSTEM')

END
GO
