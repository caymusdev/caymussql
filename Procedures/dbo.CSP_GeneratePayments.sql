SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-06-13
-- Description:	Creates payments into the payments table.  Some one then needs to mark them as approved; whereupon a separate process will then grab them and send them to processor.
-- 2018-11-15 - We will now automatically approve payments.  Users have a view to see what WILL be sent and they can modify
-- but no need to approve anymore.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GeneratePayments](
	@userid				NVARCHAR (100) = NULL
)	
AS
BEGIN	 

SET NOCOUNT ON;
IF @userid IS NULL SET @userid = 'SYSTEM'
-- make sure pay off amounts are accurate
EXEC dbo.CSP_UpdCalculations @userid

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SELECT @tomorrow = DATEADD(DD, CASE DATEPART(WEEKDAY, @today) WHEN 6 THEN 3 WHEN 7 THEN 2 ELSE 1 END, @today)

---- update any payment schedules that the end date has passed but are still marked active
--UPDATE payment_schedules
--SET active = 0, change_user = @userid, change_date = GETUTCDATE()
--WHERE active = 1 AND COALESCE(end_date, '2100-01-01') < @tomorrow

INSERT INTO payments (funding_payment_date_id, amount, trans_date, transaction_id, approved_flag, processed_date, change_user, change_date, funding_id, redirect_funding_id)
SELECT y.funding_payment_date_id, y.amount,
	-- if it is less than today, change to today
	--CASE WHEN
	--	-- adjust for weekends only (not holidays)
	--	CASE DATEPART(WEEKDAY, y.NextPayment) 
	--		WHEN 7 THEN DATEADD(D, 2, y.NextPayment)
	--		WHEN 1 THEN DATEADD(D, 1, y.NextPayment)
	--		ELSE y.NextPayment END < @tomorrow THEN @tomorrow 
	--	ELSE
	--		CASE DATEPART(WEEKDAY, y.NextPayment) 
	--		WHEN 7 THEN DATEADD(D, 2, y.NextPayment)
	--		WHEN 1 THEN DATEADD(D, 1, y.NextPayment)
	--		ELSE y.NextPayment END
	--	END	AS NextPayment, 
	y.NextPayment,
	NULL, 1, NULL, @userid, GETUTCDATE(), y.funding_id, y.redirect_funding_id
FROM (
	SELECT --CASE WHEN x.last_payment_date IS NULL THEN COALESCE(s.start_date, GETDATE())
		--ELSE DATEADD(D, CASE s.frequency WHEN 'daily' THEN 1 WHEN 'weekly' THEN 7 END, CONVERT(DATE, x.last_payment_date)) END AS NextPayment, 
		pd.payment_date AS NextPayment,
		--s.payment_schedule_id, 
		pd.id AS funding_payment_date_id,
		--CASE WHEN (f.payoff_amount - COALESCE(f.float_amount, 0)) < s.amount 
			--THEN (f.payoff_amount - COALESCE(f.float_amount, 0)) ELSE s.amount END AS amount, 
		CASE WHEN f.payoff_amount < pd.amount AND NOT EXISTS (SELECT 1 FROM redirect_approvals ra WITH (NOLOCK)
															 WHERE ra.from_funding_id = f.id AND ra.approved_by IS NOT NULL AND COALESCE(ra.end_date, '2050-12-31') >= @tomorrow AND ra.on_hold = 0)
			THEN f.payoff_amount 
			ELSE pd.amount END AS amount, 
		ba.routing_number, ba.account_number, pd.payment_processor, f.contract_number, f.legal_name, f.id AS funding_id,
		(SELECT to_funding_id 
		FROM redirect_approvals ra WITH (NOLOCK)
		WHERE ra.from_funding_id = f.id AND ra.approved_by IS NOT NULL AND COALESCE(ra.end_date, '2050-12-31') >= @tomorrow  AND ra.on_hold = 0
			AND f.payoff_amount < pd.amount) AS redirect_funding_id
	--FROM payment_schedules s
	--INNER JOIN funding_payment_schedules fs ON s.payment_schedule_id = fs.payment_schedule_id
	FROM funding_payment_dates pd
	INNER JOIN fundings f ON pd.funding_id = f.id
	LEFT JOIN merchant_bank_accounts ba ON pd.merchant_bank_account_id = ba.merchant_bank_account_id
	--LEFT JOIN (
	--	SELECT MAX(trans_date) AS last_payment_date, payment_schedule_id
	--	FROM payments p
	--	WHERE p.processed_date IS NOT NULL
	--	GROUP BY p.payment_schedule_id
	--) x ON s.payment_schedule_id = x.payment_schedule_id
	WHERE f.contract_status IN (1,3,4) AND COALESCE(f.on_hold, 0) = 0 -- AND (f.payoff_amount - COALESCE(f.float_amount, 0)) > 0
		AND f.payoff_amount > 0
		--AND s.active = 1
		AND pd.payment_method = 'ACH'
		AND pd.payment_date = @tomorrow
		AND pd.active = 1
) y
WHERE NOT EXISTS (SELECT 1 FROM payments p2 WHERE p2.trans_date = 
		--CASE WHEN		
		--CASE DATEPART(WEEKDAY, y.NextPayment) 
		--	WHEN 7 THEN DATEADD(D, 2, y.NextPayment)
		--	WHEN 1 THEN DATEADD(D, 1, y.NextPayment)
		--	ELSE y.NextPayment END < @tomorrow THEN @tomorrow 
		--ELSE
		--	CASE DATEPART(WEEKDAY, y.NextPayment) 
		--	WHEN 7 THEN DATEADD(D, 2, y.NextPayment)
		--	WHEN 1 THEN DATEADD(D, 1, y.NextPayment)
		--	ELSE y.NextPayment END
		--END
		y.NextPayment
			--AND p2.payment_schedule_id = y.payment_schedule_id
			AND p2.funding_payment_date_id = y.funding_payment_date_id
			AND p2.deleted = 0 AND p2.waived = 0)
	AND y.amount IS NOT NULL
	--AND CASE WHEN
	--	-- adjust for weekends only (not holidays)
	--	CASE DATEPART(WEEKDAY, y.NextPayment) 
	--		WHEN 7 THEN DATEADD(D, 2, y.NextPayment)
	--		WHEN 1 THEN DATEADD(D, 1, y.NextPayment)
	--		ELSE y.NextPayment END < @tomorrow THEN @tomorrow 
	--	ELSE
	--		CASE DATEPART(WEEKDAY, y.NextPayment) 
	--		WHEN 7 THEN DATEADD(D, 2, y.NextPayment)
	--		WHEN 1 THEN DATEADD(D, 1, y.NextPayment)
	--		ELSE y.NextPayment END
	--	END	
	 AND y.NextPayment <= @tomorrow


--------------------------------------------------------
-- NOW ENTER FEES FOR CONTRACTS THAT STILL HAVE FEES Outstanding BUT NO FEE PAYMENTS IN THE QUEUE
--------------------------------------------------------
DECLARE @feeAmount MONEY; SELECT @feeAmount = COALESCE(dbo.CF_GetSetting('FeeAmount'), 72)

INSERT INTO payments (funding_payment_date_id, amount, trans_date, transaction_id, approved_flag, processed_date, change_user, change_date, funding_id, is_fee)
SELECT NULL, 
	-- 2019-04-05 RKB: Updated to not overdraw when doing fees.  Need to take in to account any payments already in the queue but not processed (for examples, the ones created above)
	CASE WHEN (f.payoff_amount - COALESCE((SELECT SUM(amount) FROM payments pa WHERE pa.funding_id = f.id AND pa.processed_date IS NULL AND pa.deleted = 0 AND pa.waived = 0), 0)) >= COALESCE(f.fees_out, 0)
			THEN CASE WHEN f.fees_out < @feeAmount THEN f.fees_out ELSE @feeAmount END
		ELSE
			CASE WHEN (f.payoff_amount - COALESCE((SELECT SUM(amount) FROM payments pa WHERE pa.funding_id = f.id AND pa.processed_date IS NULL AND pa.deleted = 0 AND pa.waived = 0), 0)) < @feeAmount
				THEN (f.payoff_amount - COALESCE((SELECT SUM(amount) FROM payments pa WHERE pa.funding_id = f.id AND pa.processed_date IS NULL AND pa.deleted = 0 AND pa.waived = 0), 0)) ELSE @feeAmount END
	END, @tomorrow AS NextPayment, NULL, 1, NULL, @userid, GETUTCDATE(), f.id, 1 AS is_fee
FROM fundings f
WHERE NOT EXISTS (SELECT 1 FROM payments p2 WHERE p2.processed_date IS NULL AND p2.funding_id = f.id AND p2.is_fee = 1 AND p2.deleted = 0 AND p2.waived = 0)
	AND COALESCE(f.fees_out, 0) > 0 AND f.contract_status = 1 AND COALESCE(f.on_hold, 0) = 0
	AND (f.payoff_amount - COALESCE((SELECT SUM(amount) FROM payments p WHERE p.funding_id = f.id AND p.processed_date IS NULL 
		AND p.deleted = 0 AND p.waived = 0), 0)) > 0 -- make sure we aren't already paying it off with the regular payment above
	AND f.process_fees = 1



--------------------------------************************************** REDIRECTS - SHOW ME THE MONEY!!! ----- **********************************
-- now look for approved redirects of fundings that have paid off but need to keep going
INSERT INTO payments (payment_schedule_id, amount, trans_date, transaction_id, approved_flag, processed_date, change_user, change_date, funding_id, is_fee, 
	redirect_funding_id, redirect_approval_id)
SELECT NULL, CASE WHEN f2.payoff_amount < CASE f.frequency WHEN 'Daily' THEN f.weekday_payment ELSE f.weekly_payment END THEN f2.payoff_amount 
	ELSE CASE f.frequency WHEN 'Daily' THEN f.weekday_payment ELSE f.weekly_payment END END AS amount, @tomorrow AS trans_date, NULL AS transaction_id,
	1 AS approved_flag, NULL AS processed_date, @userid, GETUTCDATE() AS change_date, ra.from_funding_id, 0 AS is_fee, ra.to_funding_id AS redirect_funding_id, 
	ra.redirect_approval_id
FROM redirect_approvals ra WITH (NOLOCK)
--INNER JOIN payment_schedules ps WITH (NOLOCK) ON ra.payment_schedule_id = ps.payment_schedule_id
INNER JOIN fundings f WITH (NOLOCK) ON ra.from_funding_id = f.id
INNER JOIN fundings f2 WITH (NOLOCK) ON ra.to_funding_id = f2.id
WHERE COALESCE(ra.end_date, '2050-12-31') >= @tomorrow AND ra.approved_by IS NOT NULL AND ra.rejected_by IS NULL AND f.payoff_amount <= 0 AND ra.on_hold = 0
	AND NOT EXISTS (SELECT 1 FROM payments p WHERE p.funding_id = ra.from_funding_id AND p.approved_flag = 1 AND p.processed_date IS NULL)


-- now update the start date 
UPDATE redirect_approvals
SET actual_start_date = @tomorrow, change_user = @userid, change_date = GETUTCDATE()
WHERE actual_start_date IS NULL AND redirect_approval_id IN (SELECT redirect_approval_id FROM payments p WITH (NOLOCK))



END
GO
