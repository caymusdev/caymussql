SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-05-31
-- Description:	Gets a list of alerts
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetAlerts] 
AS
BEGIN	
SET NOCOUNT ON;

SELECT 'Batch' AS AlertType, b.batch_id AS RecordID, b.status_message AS AlertMessage
FROM batches b
WHERE b.batch_status = 'Error'
UNION ALL
SELECT a.alert_type, a.record_id, a.alert_message
FROM alerts a
WHERE a.deleted = 0

END
GO
