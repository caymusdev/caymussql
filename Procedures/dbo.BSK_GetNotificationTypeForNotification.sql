SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-12-03
-- Description:	gets the type of notification for a given notification id.  used when clicking on notifications to then know what to do
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetNotificationTypeForNotification]
(
	@notification_id			INT
)
AS
BEGIN
SET NOCOUNT ON;

SELECT nt.notification_type, nt.notification_type_desc
FROM notifications n
LEFT JOIN notification_types nt ON n.notification_type_id = nt.notification_type_id
WHERE n.notification_id = @notification_id

END

GO
