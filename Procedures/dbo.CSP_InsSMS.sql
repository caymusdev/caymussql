SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-06-04
-- Description:	Records a SMS call
-- =============================================
CREATE PROCEDURE dbo.CSP_InsSMS
(
	@text_to_send			NVARCHAR (2000),
	@number_to_text			NVARCHAR (500),
	@message_id				NVARCHAR (200),
	@success				BIT,
	@change_user			NVARCHAR (100),
	@status_message			NVARCHAR (MAX)
)
AS
BEGIN	
SET NOCOUNT ON;

INSERT INTO sms_logs (message_id, number_to_text, text_to_send, change_user, success, status_message, create_date)
VALUES (@message_id, @number_to_text, @text_to_send, @change_user, @success, @status_message, GETUTCDATE())


END
GO
