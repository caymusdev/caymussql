SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-02-18
-- Description:	Inserts a record into the funding off hold date table
-- =============================================
CREATE PROCEDURE [dbo].[CSP_InsFundingOffHoldDate]
(
	@funding_id			BIGINT,
	@off_hold_date		DATETIME,
	@userid				NVARCHAR (100)
)
AS
BEGIN	
SET NOCOUNT ON;

-- this is done by the UI from a person so first need to delete any that have not been processed and have a date after todays
DECLARE @today DATE; SET @today = DATEADD(HH, -5, GETUTCDATE())

DELETE FROM funding_off_hold_dates
WHERE funding_id = @funding_id AND processed_date IS NULL AND off_hold_date >= @today

INSERT INTO funding_off_hold_dates (funding_id, off_hold_date, create_date, change_date, change_user)
SELECT @funding_id, @off_hold_date, GETUTCDATE(), GETUTCDATE(), @userid
WHERE NOT EXISTS (SELECT 1 FROM funding_off_hold_dates WHERE funding_id = @funding_id AND off_hold_date = @off_hold_date)

END
GO
