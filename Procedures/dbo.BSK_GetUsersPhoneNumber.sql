SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-11-19
-- Description:	Returns phone number for the given user.  Used when initiating a call between the user and customer
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetUsersPhoneNumber]
(
	@users_email			NVARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON;

SELECT p.phone_number
FROM user_phones p
INNER JOIN users u ON p.user_id = u.user_id
WHERE u.email = @users_email AND p.is_primary = 1


END

GO
