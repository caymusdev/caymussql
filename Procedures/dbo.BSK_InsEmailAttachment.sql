SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-14
-- Description:	Saves an email attachment into the database
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsEmailAttachment]
(
	@email_id				INT,
	@attachment				VARBINARY(MAX), 
	@attachment_name		NVARCHAR (500) NULL,
	@user_email				NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON;

INSERT INTO email_attachments (email_id, attachment, attachment_name, create_date, change_date, change_user, create_user)
VALUES (@email_id, @attachment, @attachment_name, GETUTCDATE(), GETUTCDATE(), @user_email, @user_email)

END

GO
