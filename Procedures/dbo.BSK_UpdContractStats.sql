SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-30
-- Description:	updates contract stats and some fields on affiliate
-- =============================================
CREATE PROCEDURE [dbo].[BSK_UpdContractStats]
	
AS
BEGIN
SET NOCOUNT ON;

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())

UPDATE c
SET last_payment_date = x.trans_date, last_payment_amount = x.trans_amount, change_date = GETUTCDATE(), change_user = 'System'
FROM contracts c
INNER JOIN (
	SELECT TOP 1 WITH TIES t.funding_id, t.trans_date, t.trans_amount
	FROM transactions t
	ORDER BY ROW_NUMBER() OVER (PARTITION BY t.funding_id ORDER BY t.trans_amount DESC)
) x ON c.contract_id = x.funding_id
WHERE (COALESCE(c.last_payment_date, '1900-01-01') != COALESCE(x.trans_date, '1900-01-01') OR COALESCE(c.last_payment_amount, 0) != COALESCE(x.trans_amount, 0))


UPDATE a
SET gross_due = x.gross_due
FROM affiliates a
INNER JOIN (
	SELECT f.affiliate_id, SUM(payoff_amount) AS gross_due
	FROM fundings f
	GROUP BY f.affiliate_id
) x ON a.affiliate_id = x.affiliate_id
WHERE COALESCE(a.gross_due, 0) != COALESCE(x.gross_due, 0)


-- make sure to deactivate cases where affiliates have paid off
UPDATE c
SET active = 0, change_date = GETUTCDATE(), change_user = 'System'
FROM cases c 
INNER JOIN affiliates a ON c.affiliate_id = a.affiliate_id
WHERE c.active = 1 AND a.gross_due = 0


END

GO
