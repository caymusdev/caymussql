SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-09-14 (happy birthday Mykle)
-- Description:	Checks for unapproved payments with trans date of today or before.  Used in a notification.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetUnapprovedPayments]

AS
BEGIN
SET NOCOUNT ON;

-- make sure to get the "correct" "today's" date since this runs in UTC
DECLARE @today DATE; SELECT @today = CONVERT(DATE, DATEADD(HOUR, -5, GETUTCDATE()))

SELECT p.payment_id, p.payment_schedule_id, p.amount, p.approved_flag, p.processed_date, p.trans_date, p.transaction_id
FROM payments p
WHERE COALESCE(p.approved_flag, 0) = 0 AND trans_date <= @today -- still unapproved and should have been approved by now
 AND p.deleted = 0 AND p.waived = 0
END
GO
