SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Rick Pina
-- Create Date: 2020-12-09
-- Description: Adds Entry into Business Open table
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsBusinessOpen]
(
	@merchant_id				INT,
	@contact_type				NVARCHAR (100),
	@open_closed				BIT,
	@create_user			NVARCHAR (100),
	@verified_date				DATE

)
AS

SET NOCOUNT ON
	INSERT INTO businessopen(affiliate_id, merchant_id, contact_type, open_closed, create_user, create_date, change_user, change_date, verified_date)
	SELECT m.affiliate_id, @merchant_id, @contact_type, @open_closed, @create_user, GETUTCDATE(), @create_user, GETUTCDATE(), @verified_date
	FROM merchants m
	WHERE m.merchant_id = @merchant_id
GO
