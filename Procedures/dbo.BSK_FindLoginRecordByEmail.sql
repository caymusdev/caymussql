SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Rick Pina
-- Create Date: 2020-09-02
-- Description: Finds Login Record By Email
-- =============================================
CREATE PROCEDURE dbo.BSK_FindLoginRecordByEmail
(
	@email			NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON

-- validate email and password
	
	SELECT email, hash_password, salt FROM users WHERE email = @email

END
GO
