SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Rick Pina
-- Create Date: 2020-09-15
-- Description: Updates Translated Word with a different one.
-- =============================================
CREATE PROCEDURE [dbo].[BSK_UpdTranslatedWord]
(    
    @word_id            NVARCHAR (100),
    @change_user        NVARCHAR (100),
    @new_word           NVARCHAR (100)
)
AS
BEGIN
    SET NOCOUNT ON

    UPDATE words
    SET translated_word = @new_word, change_user = @change_user, change_date = GETUTCDATE()
    WHERE word_id = @word_id

END
GO
