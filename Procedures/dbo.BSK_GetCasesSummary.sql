SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Rick Pina
-- Create Date: 2020-12-15
-- Description: 
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetCasesSummary]
(
	@collector_email NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())

-- validate email and password

DECLARE @user_id INT; SELECT @user_id = user_id FROM users WHERE email = @collector_email
	
SELECT (SELECT COUNT(*) 
        FROM collector_cases cc
        INNER JOIN cases c ON cc.case_id = c.case_id
        WHERE collector_user_id = @user_id AND c.active = 1 AND cc.start_date <= @today AND COALESCE(cc.end_date, '2050-05-09') >= @today) AS cases_assigned,

		(SELECT COUNT(*) 
        FROM collector_cases cc
        INNER JOIN cases c ON cc.case_id = c.case_id
		INNER JOIN case_statuses cs ON c.case_status_id = cs.case_status_id
        WHERE collector_user_id = @user_id AND c.active = 1 AND cc.start_date <= @today AND COALESCE(cc.end_date, '2050-05-09') >= @today AND cs.status = 'Active') AS cases_active,

		(SELECT COUNT(*) 
        FROM collector_cases cc
        INNER JOIN cases c ON cc.case_id = c.case_id
		INNER JOIN case_statuses cs ON c.case_status_id = cs.case_status_id
        WHERE collector_user_id = @user_id AND c.active = 1 AND cc.start_date <= @today AND COALESCE(cc.end_date, '2050-05-09') >= @today AND cs.status = 'Inactive') AS cases_inactive,

		(SELECT COUNT(*) 
        FROM reminders r
		INNER JOIN contact_types ct ON r.contact_type_id = ct.contact_type_id
        WHERE r.user_id = @user_id AND ct.contact_type = 'phone' AND CONVERT(DATE, r.reminder_date) = @today) AS calls_scheduled,

		(SELECT COUNT(*) 
        FROM reminders r
		INNER JOIN contact_types ct ON r.contact_type_id = ct.contact_type_id
        WHERE r.user_id = @user_id AND ct.contact_type != 'phone' AND CONVERT(DATE, r.reminder_date) = @today) AS emails_text_mail_scheduled,

		(SELECT COUNT(*) 
        FROM BF_BusinessOpenToValidate(@collector_email)) AS business_status_to_validate,

		(SELECT COUNT(*) 
		FROM payment_plans pl
		INNER JOIN affiliates a ON pl.affiliate_id = a.affiliate_id
		INNER JOIN cases c ON a.affiliate_id = c.affiliate_id
		INNER JOIN collector_cases cc ON c.case_id = cc.case_id AND cc.start_date <= GETUTCDATE() AND COALESCE(cc.end_date, '2050-05-09') >= GETUTCDATE()
		INNER JOIN payment_plan_status_codes pps ON pl.current_status = pps.id
        WHERE pl.payment_plan_type = 'Payment Plan' AND pps.code IN ('Broken', 'BrokenPayment') AND collector_user_id = @user_id AND c.active = 1) AS payment_plan_broken,

		(SELECT COUNT(*) 
        FROM cases c
		INNER JOIN collector_cases cc ON c.case_id = cc.case_id AND cc.start_date <= @today AND COALESCE(cc.end_date, '2050-05-09') >= @today
        WHERE c.new_case = 1 AND collector_user_id = @user_id AND c.active = 1) AS new_cases,

		(SELECT COUNT(*) 
        FROM payment_plans pl
		INNER JOIN affiliates a ON pl.affiliate_id = a.affiliate_id
		INNER JOIN cases c ON a.affiliate_id = c.affiliate_id
		INNER JOIN collector_cases cc ON c.case_id = cc.case_id AND cc.start_date <= @today AND COALESCE(cc.end_date, '2050-05-09') >= @today
		INNER JOIN payment_plan_status_codes pps ON pl.current_status = pps.id
        WHERE pl.payment_plan_type = 'Temporary Arrangement' AND pps.code IN ('Broken', 'BrokenPayment') AND collector_user_id = @user_id AND c.active = 1) AS temp_payment_plans_broken,

		(SELECT COUNT(*) 
        FROM payment_plans pl
		INNER JOIN affiliates a ON pl.affiliate_id = a.affiliate_id
		INNER JOIN cases c ON a.affiliate_id = c.affiliate_id
		INNER JOIN collector_cases cc ON c.case_id = cc.case_id AND cc.start_date <= @today AND COALESCE(cc.end_date, '2050-05-09') >= @today		
        WHERE collector_user_id = @user_id AND c.active = 1
			AND pl.payment_plan_type = 'Temporary Arrangement'
			-- check for last payment date in the plan and see if it is in the past
			AND (SELECT MAX(payment_date) FROM funding_payment_dates fpd WHERE fpd.payment_plan_id = pl.payment_plan_id) < GETUTCDATE()
			-- make sure a new payment plan has not been created
			AND NOT EXISTS (SELECT 1 FROM funding_payment_dates fpd2
										INNER JOIN payment_plans pp2 ON fpd2.payment_plan_id = pp2.payment_plan_id										
										WHERE pp2.affiliate_id = pl.affiliate_id AND fpd2.payment_date >= GETUTCDATE())
		) AS temp_payment_plans_expired


END
GO
