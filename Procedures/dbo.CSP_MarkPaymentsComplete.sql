SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-01-24
-- Description:	Marks payments complete and retry_complete
-- =============================================
CREATE PROCEDURE [dbo].[CSP_MarkPaymentsComplete]

AS
BEGIN
SET NOCOUNT ON;

-- payments with settles and NO payments pointing at it that have not settled
-- if they settled with R01, there will be a payment pointing at it.  
-- will not catch cases where a retry payment is deleted.  Since it was deleted, there won't be one waiting to settle and so it will be marked complete.
-- retry_complete is for cases where all retries are settled, so if one is deleted, retry_complete will never be set to 1


  -------------------------------------------------- do the complete flag -------------------------------
UPDATE p
SET complete = 1, change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM payments p
INNER JOIN forte_settlements s ON p.transaction_id = s.transaction_id
WHERE COALESCE(p.complete, 0) = 0
	AND NOT EXISTS (  -- payments that have p as their orig trans or as their prev trans but have not settled
		SELECT 1
		FROM payments p2
		LEFT JOIN forte_settlements s2 ON p2.transaction_id = s2.transaction_id
		WHERE (p.transaction_id = p2.orig_transaction_id OR p.transaction_id = p2.previous_transaction_id) AND s2.forte_settle_id IS NULL
			AND COALESCE(p2.is_fee, 0) = 0  -- don't care if there are fees waiting to be settled
	)
	AND p.trans_date >= '2019-01-01'



-- RKB : 2019-08-29 - This was timing out.  This SQL below was taking almost a minute.  
-- All it basically is doing is looking for payments that DO have a retry payment that has NOT settled
-- Changing from AND EXISTS to using JOINS sped it up tremndously (hopefully it still works.  :) )	
UPDATE p
SET complete = 0, retry_complete = 0, change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM payments p
INNER JOIN forte_settlements s ON p.transaction_id = s.transaction_id
INNER JOIN payments p2 WITH (NOLOCK) ON (p.transaction_id = p2.orig_transaction_id OR p.transaction_id = p2.previous_transaction_id)
LEFT JOIN forte_settlements s2 WITH (NOLOCK) ON p2.transaction_id = s2.transaction_id
WHERE COALESCE(p.complete, 0) = 1
/*
	AND EXISTS (  -- payments that have p as their orig trans or as their prev trans but have not settled
		SELECT 1
		FROM payments p2
		LEFT JOIN forte_settlements s2 ON p2.transaction_id = s2.transaction_id
		WHERE (p.transaction_id = p2.orig_transaction_id OR p.transaction_id = p2.previous_transaction_id) AND s2.forte_settle_id IS NULL
			AND COALESCE(p2.is_fee, 0) = 0  -- don't care if there are fees waiting to be settled
	)
*/
	AND p.trans_date >= '2019-01-01'
	AND s2.forte_settle_id IS NULL AND COALESCE(p2.is_fee, 0) = 0


UPDATE p
SET complete = 1, change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM payments p
WHERE processor IN ('53') AND transaction_id IS NOT NULL
	AND COALESCE(p.complete, 0) = 0
	AND NOT EXISTS (  -- payments that have p as their orig trans or as their prev trans but have not settled
		SELECT 1
		FROM payments p2		
		WHERE (p.transaction_id = p2.orig_transaction_id OR p.transaction_id = p2.previous_transaction_id) AND p2.transaction_id IS NULL
			AND COALESCE(p2.is_fee, 0) = 0  -- don't care if there are fees waiting to be settled
			AND p2.processor IN ('53')
	)
	AND p.trans_date >= '2019-01-01'


UPDATE p
SET complete = 0, retry_complete = 0, change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM payments p
WHERE processor IN ('53') AND transaction_id IS NOT NULL
	AND COALESCE(p.complete, 0) = 0
	AND EXISTS (  -- payments that have p as their orig trans or as their prev trans but have not settled
		SELECT 1
		FROM payments p2		
		WHERE (p.transaction_id = p2.orig_transaction_id OR p.transaction_id = p2.previous_transaction_id) AND p2.transaction_id IS NULL
			AND COALESCE(p2.is_fee, 0) = 0  -- don't care if there are fees waiting to be settled
			AND p2.processor IN ('53')
	)
	AND p.trans_date >= '2019-01-01'

----------------------------------- REGIONS ------------------------------


UPDATE p
SET complete = 1, change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM payments p
WHERE processor IN ('Regions') AND transaction_id IS NOT NULL
	AND COALESCE(p.complete, 0) = 0
	AND NOT EXISTS (  -- payments that have p as their orig trans or as their prev trans but have not settled
		SELECT 1
		FROM payments p2		
		WHERE (p.transaction_id = p2.orig_transaction_id OR p.transaction_id = p2.previous_transaction_id) AND p2.transaction_id IS NULL
			AND COALESCE(p2.is_fee, 0) = 0  -- don't care if there are fees waiting to be settled
			AND p2.processor IN ('Regions')
	)
	AND p.trans_date >= '2020-01-27'


UPDATE p
SET complete = 0, retry_complete = 0, change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM payments p
WHERE processor IN ('Regions') AND transaction_id IS NOT NULL
	AND COALESCE(p.complete, 0) = 0
	AND EXISTS (  -- payments that have p as their orig trans or as their prev trans but have not settled
		SELECT 1
		FROM payments p2		
		WHERE (p.transaction_id = p2.orig_transaction_id OR p.transaction_id = p2.previous_transaction_id) AND p2.transaction_id IS NULL
			AND COALESCE(p2.is_fee, 0) = 0  -- don't care if there are fees waiting to be settled
			AND p2.processor IN ('Regions')
	)
	AND p.trans_date >= '2020-01-27'




------------------------------------- END REGIONS ----------------------

  -------------------------------------------------- do the complete flag -------------------------------END



-- do level 2's first
-- any level 2 that is not a fee (don't care about fees in this process) that has a settlement of any kind is considered retry_complete.  
--SELECT *
UPDATE p
SET retry_complete = 1, change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM payments p
INNER JOIN forte_settlements s ON p.transaction_id = s.transaction_id
WHERE COALESCE(p.retry_complete, 0) = 0
	AND COALESCE(p.retry_level, 0) = 2
	AND COALESCE(p.is_fee, 0) = 0
	AND p.trans_date >= '2019-01-01'


-- now do level 1's
-- if a level 1 has an S01 settlement or has 2 level's 2 that are retry_complete
-- SELECT *
UPDATE p
SET retry_complete = 1, change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM payments p
INNER JOIN forte_settlements s ON p.transaction_id = s.transaction_id AND s.settle_response_code = 'S01'
WHERE COALESCE(p.retry_complete, 0) = 0
	AND COALESCE(p.retry_level, 0) = 1
	AND COALESCE(p.is_fee, 0) = 0
	AND p.trans_date >= '2019-01-01'


--SELECT *
UPDATE p
SET retry_complete = 1, change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM payments p
INNER JOIN forte_settlements s ON p.transaction_id = s.transaction_id AND s.settle_response_code <> 'S01'
WHERE COALESCE(p.retry_complete, 0) = 0
	AND COALESCE(p.retry_level, 0) = 1
	AND COALESCE(p.is_fee, 0) = 0
	AND p.trans_date >= '2019-01-01'
	AND (SELECT COUNT(*) FROM payments p2		 
		WHERE p2.previous_transaction_id = p.transaction_id AND COALESCE(p2.retry_complete, 0) = 1) = 2



-- now do level 0's
-- if a level 1 has an S01 settlement or has 2 level's 2 that are retry_complete
-- SELECT *
UPDATE p
SET retry_complete = 1, change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM payments p
INNER JOIN forte_settlements s ON p.transaction_id = s.transaction_id AND s.settle_response_code = 'S01'
WHERE COALESCE(p.retry_complete, 0) = 0
	AND COALESCE(p.retry_level, 0) = 0
	AND COALESCE(p.is_fee, 0) = 0
	AND p.trans_date >= '2019-01-01'


--  SELECT *
UPDATE p
SET retry_complete = 1, change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM payments p
INNER JOIN forte_settlements s ON p.transaction_id = s.transaction_id AND s.settle_response_code <> 'S01'
WHERE COALESCE(p.retry_complete, 0) = 0
	AND COALESCE(p.retry_level, 0) = 0
	AND COALESCE(p.is_fee, 0) = 0
	AND p.trans_date >= '2019-01-01'
	AND (SELECT COUNT(*) FROM payments p2		 
		WHERE p2.previous_transaction_id = p.transaction_id AND COALESCE(p2.retry_complete, 0) = 1) = 2


END

GO
