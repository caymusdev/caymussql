SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-12-07
-- Description:	New logic for applying or dealing with payments as they come in.  May create payments for fees, etc.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_ApplyPPAndMargin]
(
	@funding_id			BIGINT,
	@trans_amount		MONEY,
	@trans_id			INT,
	@comments			NVARCHAR (500)
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
BEGIN TRANSACTION

DECLARE @balance MONEY, @fees_out MONEY, @pp_margin MONEY, @rtr MONEY, @remaining_amount MONEY, @expected_pp MONEY, @current_pp MONEY
DECLARE @contract_status INT, @pricing_ratio NUMERIC(10, 4), @temp_amount MONEY

SELECT @fees_out = SUM(CASE d.trans_type_id WHEN 9 THEN d.trans_amount ELSE -1 * d.trans_amount END) -- positive number means they owe fees
FROM transactions t
INNER JOIN transaction_details d ON t.trans_id = d.trans_id
WHERE t.funding_id = @funding_id AND COALESCE(t.redistributed, 0) = 0 AND d.trans_type_id IN (1,9)

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR

SET @fees_out = COALESCE(@fees_out, 0)

SELECT @pp_margin = SUM(d.trans_amount)
FROM transactions t
INNER JOIN transaction_details d ON t.trans_id = d.trans_id
WHERE t.funding_id = @funding_id AND COALESCE(t.redistributed, 0) = 0 AND d.trans_type_id IN (6,7)

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR

SELECT @balance = f.portfolio_value - COALESCE(@pp_margin, 0), -- - COALESCE(f.float_amount, 0), 
	@contract_status = contract_status, @pricing_ratio = pricing_ratio, @rtr = portfolio_value
FROM fundings f
WHERE f.id = @funding_id

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR


SET @expected_pp = ROUND(@rtr / @pricing_ratio, 2) -- total expected PP for the entire funding
SELECT @current_pp = SUM(d.trans_amount) 
FROM transactions t
INNER JOIN transaction_details d ON t.trans_id = d.trans_id
WHERE t.funding_id = @funding_id AND d.trans_type_id = 7 AND COALESCE(t.redistributed, 0) = 0

SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
IF @errorNum != 0 GOTO ERROR

DECLARE @appliedPPandMargin BIT; SET @appliedPPandMargin = 0
DECLARE @last_detail_id_pp INT, @last_detail_id_marg INT

IF @balance >=  @trans_amount OR @fees_out = 0
	BEGIN
		IF @contract_status = 1
			BEGIN
				-- apply to purchase price and margin
				SET @temp_amount = ROUND(@trans_amount / @pricing_ratio, 2)

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR

				-- we have had issues where, due to rounding, too much purchase price is taken out.  For example, a payment of 120 and pricing ratio of 1.35 after 20 payments
				-- we will have overdrawn the purchase price by 2.2 cents.  So, we need to adjust at the end, essentially, if the purchse price outstanding is more less than
				-- what we have calculated it should be, then lower the pp payment this time.  Likely should only ever happen on the last payment.
				-- we do store pp_out but it may not be accurate at this moment because, for example, we could be getting 2 payments to the same funding
				-- so, add up all trans_type of 7 and then compare to a calcualted value									

				-- if we owe less PP than what we have calculated, overwrite it
				--IF @expected_pp - @current_pp < @temp_amount AND @expected_pp - @current_pp > 0 SET @temp_amount = @expected_pp - @current_pp

				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				VALUES (@trans_id, 7, @temp_amount, @comments, 'system', GETUTCDATE())

				SELECT @last_detail_id_pp = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR

				-- now do margin
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				VALUES (@trans_id, 6, @trans_amount - @temp_amount, @comments, 'system', GETUTCDATE())

				SELECT @last_detail_id_marg = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR

				SET @appliedPPandMargin = 1
			END
		ELSE IF @contract_status = 4 
			BEGIN
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				VALUES (@trans_id, 15, @trans_amount, '', 'system', GETUTCDATE())

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR
			END
		ELSE  -- contract status not in (1,4)
			BEGIN
				-- these are either not active or writeoff contracts.  Shouldn't be having money anyway so if there is, by not inserting a details
				-- an alert will be generated where these will be caught.
				PRINT 0
			END  -- end @contract_status
	END --  @balance >=  @trans_amount OR @fees_out = 0
ELSE IF (@fees_out < @trans_amount AND @balance <= 0) OR (@fees_out < (@trans_amount - @balance) AND @balance > 0)
	BEGIN
		IF @contract_status = 1
			BEGIN
				-- apply to purchase price and margin
				SET @temp_amount = ROUND((@trans_amount - @fees_out) / @pricing_ratio, 2)

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR

				-- if we owe less PP than what we have calculated, overwrite it
				--IF @expected_pp - @current_pp < @temp_amount AND @expected_pp - @current_pp > 0 SET @temp_amount = @expected_pp - @current_pp
				
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				VALUES (@trans_id, 7, @temp_amount, @comments, 'system', GETUTCDATE())

				SELECT @last_detail_id_pp = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR

				-- now do margin
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				VALUES (@trans_id, 6, @trans_amount - @fees_out - @temp_amount, @comments, 'system', GETUTCDATE())

				SELECT @last_detail_id_marg = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR

				-- now do the fees
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				VALUES (@trans_id, 1, @fees_out, @comments, 'system', GETUTCDATE())

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR

				SET @appliedPPandMargin = 1
			END
		ELSE IF @contract_status = 4 
			BEGIN
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				VALUES (@trans_id, 15, @trans_amount - @fees_out, '', 'system', GETUTCDATE())

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR

				-- now do the fees
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				VALUES (@trans_id, 1, @fees_out, @comments, 'system', GETUTCDATE())

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR
			END
		ELSE  -- contract status not in (1,4)
			BEGIN
				-- these are either not active or writeoff contracts.  Shouldn't be having money anyway so if there is, by not inserting a details
				-- an alert will be generated where these will be caught.
				PRINT 0
			END  -- end @contract_status
	END --  @fees_out < @trans_amount OR (@fees_out < (@trans_amount - @balance) AND @balance > 0)
ELSE IF @balance <= 0 AND @fees_out >= @trans_amount
	BEGIN
		-- everything goes to fees
		-- now do the fees
		INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
		VALUES (@trans_id, 1, @trans_amount, @comments, 'system', GETUTCDATE())

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR
	END
ELSE IF @balance > 0 AND @fees_out >= (@trans_amount - @balance)
	BEGIN
		IF @contract_status = 1
			BEGIN
				-- apply to purchase price and margin
				SET @temp_amount = ROUND(@balance / @pricing_ratio, 2)

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR

				-- if we owe less PP than what we have calculated, overwrite it
				--IF @expected_pp - @current_pp < @temp_amount AND @expected_pp - @current_pp > 0 SET @temp_amount = @expected_pp - @current_pp
				
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				VALUES (@trans_id, 7, @temp_amount, @comments, 'system', GETUTCDATE())

				SELECT @last_detail_id_pp = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR

				-- now do margin
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				VALUES (@trans_id, 6, @balance - @temp_amount, @comments, 'system', GETUTCDATE())

				SELECT @last_detail_id_marg = SCOPE_IDENTITY(), @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR

				-- now do the fees
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				VALUES (@trans_id, 1, (@trans_amount - @balance), @comments, 'system', GETUTCDATE())

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR

				SET @appliedPPandMargin = 1
			END
		ELSE IF @contract_status = 4 
			BEGIN
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				VALUES (@trans_id, 15, @balance - @fees_out, '', 'system', GETUTCDATE())

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR

				-- now do the fees
				INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments, change_user, change_date)
				VALUES (@trans_id, 1, @trans_amount - @balance, @comments, 'system', GETUTCDATE())

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
				IF @errorNum != 0 GOTO ERROR
			END
		ELSE  -- contract status not in (1,4)
			BEGIN
				-- these are either not active or writeoff contracts.  Shouldn't be having money anyway so if there is, by not inserting a details
				-- an alert will be generated where these will be caught.
				PRINT 0
			END  -- end @contract_status
	END  --  @balance > 0 AND @fees_out >= (@trans_amount - @balance)

-- now may need to adjust 6 and 7 due to rounding issues
IF @appliedPPandMargin = 1
	BEGIN
		DECLARE @pp_out MONEY, @margin_out MONEY

		SELECT @pp_out = f.purchase_price - COALESCE(principal.principal_paid, 0) - COALESCE(writeoff.writeoff, 0), 
			@margin_out = f.portfolio_value - f.purchase_price - COALESCE(margin.margin_paid, 0) - COALESCE(discount.discount, 0) - COALESCE(margin_adjustment.margin_adjustment, 0)
		FROM fundings f
		LEFT JOIN (
			SELECT t.funding_id, SUM(d.trans_amount) AS margin_paid
			FROM transactions t
			INNER JOIN transaction_details d ON t.trans_id = d.trans_id
			LEFT JOIN batches b ON t.batch_id = b.batch_id
			WHERE d.trans_type_id = 6 AND t.redistributed = 0
				--AND (b.batch_status IN('Settled', 'Closed') OR t.comments LIKE '%CashApplication%') 
			GROUP BY t.funding_id
			) margin ON f.id = margin.funding_id
		LEFT JOIN (
			SELECT t.funding_id, SUM(d.trans_amount) AS principal_paid
			FROM transactions t
			INNER JOIN transaction_details d ON t.trans_id = d.trans_id
			LEFT JOIN batches b ON t.batch_id = b.batch_id
			WHERE d.trans_type_id = 7 AND t.redistributed = 0
				--AND (b.batch_status IN('Settled', 'Closed') OR t.comments LIKE '%CashApplication%') 
			GROUP BY t.funding_id
			) principal ON f.id = principal.funding_id		
		LEFT JOIN (
			SELECT t.funding_id, SUM(d.trans_amount) AS writeoff
			FROM transactions t
			INNER JOIN transaction_details d ON t.trans_id = d.trans_id
			LEFT JOIN batches b ON t.batch_id = b.batch_id
			WHERE d.trans_type_id IN (11) AND t.redistributed = 0
				--AND (b.batch_status IN('Settled', 'Closed') OR t.comments LIKE '%CashApplication%') 
			GROUP BY t.funding_id
			) writeoff ON f.id = writeoff.funding_id
		LEFT JOIN (
			SELECT t.funding_id, SUM(d.trans_amount) AS discount
			FROM transactions t
			INNER JOIN transaction_details d ON t.trans_id = d.trans_id
			LEFT JOIN batches b ON t.batch_id = b.batch_id
			WHERE d.trans_type_id IN (8) AND t.redistributed = 0
			--	AND (b.batch_status IN('Settled', 'Closed') OR t.comments LIKE '%CashApplication%') 
			GROUP BY t.funding_id
			) discount ON f.id = discount.funding_id
		LEFT JOIN (
			SELECT t.funding_id, SUM(d.trans_amount) AS margin_adjustment
			FROM transactions t
			INNER JOIN transaction_details d ON t.trans_id = d.trans_id
			LEFT JOIN batches b ON t.batch_id = b.batch_id
			WHERE d.trans_type_id IN (17) AND t.redistributed = 0
				--AND (b.batch_status IN('Settled', 'Closed') OR t.comments LIKE '%CashApplication%') 
			GROUP BY t.funding_id
			) margin_adjustment ON f.id = margin_adjustment.funding_id
		WHERE f.id = @funding_id

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
		IF @errorNum != 0 GOTO ERROR

		IF COALESCE(@pp_out, 0) <> COALESCE(@margin_out, 0) AND COALESCE(@pp_out, 0) + COALESCE(@margin_out, 0) = 0
			BEGIN
				-- pp and margin are not the same but they do sum to 0 meaning they are the same number but one is positive and one is negative
				-- ajust the last ones we entered to balance them back out.							
				IF @pp_out < 0
					-- adjust the purchase price
					BEGIN												
						-- add the @margin_out to the latest purchase price transasction
						UPDATE transaction_details SET trans_amount = trans_amount - @margin_out WHERE trans_detail_id = @last_detail_id_pp

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
						IF @errorNum != 0 GOTO ERROR

						-- reduce the other one
						UPDATE transaction_details SET trans_amount = trans_amount + @margin_out WHERE trans_detail_id = @last_detail_id_marg

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
						IF @errorNum != 0 GOTO ERROR
					END
				ELSE
					BEGIN											
						-- add the @pp_out to the latest purchase price transasction
						UPDATE transaction_details SET trans_amount = trans_amount - @pp_out WHERE trans_detail_id = @last_detail_id_marg

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
						IF @errorNum != 0 GOTO ERROR

						-- reduce the other one
						UPDATE transaction_details SET trans_amount = trans_amount + @pp_out WHERE trans_detail_id = @last_detail_id_pp

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE()		
						IF @errorNum != 0 GOTO ERROR
					END
			END
	END

COMMIT TRANSACTION
GOTO Done

ERROR:	
	ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, 16, 1)

Done:
END
GO
