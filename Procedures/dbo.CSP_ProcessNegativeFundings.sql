SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-03-28
-- Description:	Processes fundings who have a negative payoff, are active.  Will move to another contract, if exists, otherwise will move to refund.
-- =============================================
CREATE PROCEDURE [dbo].[CSP_ProcessNegativeFundings]

AS
BEGIN
SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData
CREATE TABLE #tempData (id int not null identity, funding_id BIGINT, float_amount MONEY, merchant_id BIGINT, payoff_amount MONEY, contract_number VARCHAR (50))

-- Make sure numbers are up to date
EXEC dbo.CSP_UpdCalculations 'SYSTEM'
---

-- get a list of all active fundings that have a negative balance
-- if float is 0 then try to move to another account otherwise create a refund transaction.
-- if float is not 0 then just move to refund
INSERT INTO #tempData (funding_id, float_amount, merchant_id, payoff_amount, contract_number)
SELECT f.id, f.float_amount, f.merchant_id, f.payoff_amount, f.contract_number
FROM fundings f WITH (NOLOCK)
WHERE f.payoff_amount < 0 AND f.contract_status IN (1, 2, 4) --AND f.float_amount = 0

-- in case there are any complete that DO have negative balance, we need to reset them to Active 
UPDATE fundings SET contract_status = 1, completed_date = NULL, change_user = 'SYSTEM', change_date = GETUTCDATE() WHERE payoff_amount < 0 AND contract_status = 2

DECLARE @current_id INT, @max_id INT
SELECT @current_id = COALESCE(MIN(id), 0), @max_id = COALESCE(MAX(id), 0) FROM #tempData
/* vars */
DECLARE @funding_id BIGINT, @float_amount MONEY, @payoff_amount MONEY, @merchant_id BIGINT, @contract_number VARCHAR (50), @max_date DATE
DECLARE @older_funding_id BIGINT, @older_payoff_amount MONEY, @tempAmount MONEY, @older_contract_number VARCHAR (50), @tempComments VARCHAR (500), @refund MONEY
DECLARE @errorNum INT, @errorMessage NVARCHAR (4000); SET @errorMessage = ''
DECLARE @today DATE, @newBatchID INT; SET @today = DATEADD(HOUR, -5, GETUTCDATE())
DECLARE @tomorrow DATE; SELECT @tomorrow = DATEADD(DD, CASE DATEPART(WEEKDAY, @today) WHEN 6 THEN 3 WHEN 7 THEN 2 ELSE 1 END, @today)

/* end vars */

WHILE @current_id <= @max_id AND @max_id <> 0
	BEGIN
		BEGIN TRANSACTION

		SELECT @funding_id = t.funding_id, @float_amount = t.float_amount, @payoff_amount = t.payoff_amount, @merchant_id = t.merchant_id, @contract_number = t.contract_number
		FROM #tempData t
		WHERE t.id = @current_id

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		-- get a new batch id
		EXEC @newBatchID = dbo.CSP_GetNewBatchNumber '', 'SYSTEM', 'Negative Funding', '', @tomorrow, 0

		IF @float_amount = 0
			BEGIN
				SET @older_funding_id = NULL
				SELECT TOP 1 @older_funding_id = f.id, @older_payoff_amount = f.payoff_amount, @older_contract_number = f.contract_number
				FROM fundings f
				WHERE f.merchant_id = @merchant_id
					AND f.id <> @funding_id AND f.contract_status = 1
					AND COALESCE(f.never_redistribute, 0) = 0
					AND f.payoff_amount > 0
				ORDER BY f.funded_date

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

				IF @older_funding_id IS NOT NULL
					BEGIN
						-- now need to redistribute.  If the payoff amount is >= than payment amount, it can all be redistributed.  If it is less, 
						-- then we redistribute enough to payoff and the rest goes back to the original as a refund .
						DECLARE @left_over MONEY; SET @left_over = 0;
						SET @tempAmount = @payoff_amount
						IF @older_payoff_amount < ABS(@payoff_amount) 
							BEGIN
								SET @left_over =  @payoff_amount + @older_payoff_amount  -- ADD older_payoff_amount because @payoff_amount is negative (balance due)
								SET @tempAmount = -1.00 * @older_payoff_amount; 
							END
						
						SET @tempComments = 'Overage-Reapply to ' + @older_contract_number
						-- move money from newer funding
						EXEC dbo.CSP_DistributeTransaction_v2 @funding_id, @tempAmount, NULL, @today, @tempComments, @newBatchID, NULL, NULL, NULL, 
							@today, NULL, NULL, 16

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

						-- move money to other funding
						SET @tempComments = 'Overage-Reapply from ' + @contract_number; SET @tempAmount = -1.00 * @tempAmount

						EXEC dbo.CSP_DistributeTransaction_v2 @older_funding_id, @tempAmount, NULL, @today, @tempComments, @newBatchID, NULL, NULL, NULL, 
							@today, NULL, NULL, 16

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
						
						IF @left_over <> 0
							BEGIN								
								EXEC dbo.CSP_DistributeTransaction_v2 @funding_id, @left_over, NULL, @today, 'Refund', @newBatchID, NULL, NULL, NULL, 
									@today, NULL, NULL, 22
								SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
							END
					END  -- @older_funding_id IS NOT NULL
				ELSE
					BEGIN -- no other open fundings so just create a refund
						EXEC dbo.CSP_DistributeTransaction_v2 @funding_id, @payoff_amount, NULL, @today, 'Refund', @newBatchID, NULL, NULL, NULL, @today, NULL, NULL, 22

						SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
					END
			END  -- float amount = 0
		ELSE 
			BEGIN  -- float amount != 0
				EXEC dbo.CSP_DistributeTransaction_v2 @funding_id, @payoff_amount, NULL, @today, 'Refund', @newBatchID, NULL, NULL, NULL, @today, NULL, NULL, 22

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR
			END  -- float amount !=  0

		-- set the batch to Processed
		UPDATE batches SET batch_status = 'Closed', settle_date = @tomorrow WHERE batch_id = @newBatchID
		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR

		COMMIT TRANSACTION
		GOTO Done

		ERROR:	
			ROLLBACK TRANSACTION
			RAISERROR (@ErrorMessage, 16, 1)

		Done:
		SET @current_id = @current_id + 1
	END  -- end while loop
IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData

IF @max_id > 0 
	BEGIN
		EXEC dbo.CSP_UpdCalculations 'SYSTEM'
	END


-- now look for contracts that have an outstanding refund but NO float
-- we will move the money to a different merchant if available 
SELECT @current_id = 0, @max_id = 0
CREATE TABLE #tempData2 (id int not null identity, funding_id BIGINT, refund MONEY, merchant_id BIGINT, contract_number VARCHAR (50))

INSERT INTO #tempData2 (funding_id, refund, merchant_id, contract_number)
SELECT f.id, (SELECT SUM(trans_amount) FROM transactions t WHERE t.funding_id = f.id AND t.trans_type_id IN (22, 23, 25) AND t.redistributed = 0), f.merchant_id, f.contract_number
FROM fundings f WITH (NOLOCK)
WHERE f.payoff_amount = 0 AND f.contract_status IN (1,2)  AND f.float_amount = 0
	AND (SELECT SUM(trans_amount) FROM transactions t WHERE t.funding_id = f.id AND t.trans_type_id IN (22, 23, 25) AND t.redistributed = 0) < 0

SELECT @current_id = COALESCE(MIN(id), 0), @max_id = COALESCE(MAX(id), 0) FROM #tempData2

WHILE @current_id <= @max_id AND @max_id <> 0
	BEGIN
		SELECT @funding_id = t.funding_id, @refund = t.refund, @merchant_id = t.merchant_id, @contract_number = t.contract_number
		FROM #tempData2 t
		WHERE t.id = @current_id

		SET @older_funding_id = NULL -- reset from previous step

		SELECT TOP 1 @older_funding_id = f.id, @older_payoff_amount = f.payoff_amount, @older_contract_number = f.contract_number
		FROM fundings f
		WHERE f.merchant_id = @merchant_id
			AND f.id <> @funding_id AND f.contract_status = 1
			AND COALESCE(f.never_redistribute, 0) = 0
			AND f.payoff_amount > 0
		ORDER BY f.funded_date

		IF @older_funding_id IS NOT NULL
			BEGIN
				BEGIN TRANSACTION
				-- get a new batch id
				EXEC @newBatchID = dbo.CSP_GetNewBatchNumber '', 'SYSTEM', 'Negative Funding', '', @tomorrow, 0

				-- now need to redistribute.  If the payoff amount is >= than payment amount, it can all be redistributed.  If it is less, 
				-- then we redistribute enough to payoff and the rest goes back to the original as a refund .
				SET @left_over = 0;
				SET @tempAmount = ABS(@refund)
				IF @older_payoff_amount < ABS(@refund) 
					BEGIN
						SET @left_over =  @refund + @older_payoff_amount  -- ADD older_payoff_amount because @payoff_amount is negative (balance due)
						SET @tempAmount = @older_payoff_amount; 
					END
						
				SET @tempComments = 'Refund-Reapply to ' + @older_contract_number
				-- move money from newer funding
				EXEC dbo.CSP_DistributeTransaction_v2 @funding_id, @tempAmount, NULL, @today, @tempComments, @newBatchID, NULL, NULL, NULL, 
					@today, NULL, NULL, 23

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

				-- move money to other funding
				SET @tempComments = 'Refund-Reapply from ' + @contract_number; --SET @tempAmount = -1.00 * @tempAmount

				EXEC dbo.CSP_DistributeTransaction_v2 @older_funding_id, @tempAmount, NULL, @today, @tempComments, @newBatchID, NULL, NULL, NULL, 
					@today, NULL, NULL, 24

				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2
						
				IF @left_over <> 0
					BEGIN								
						PRINT 'Do nothing, leave refund here for now.'
					END

				-- set the batch to Processed
				UPDATE batches SET batch_status = 'Closed', settle_date = @tomorrow WHERE batch_id = @newBatchID
				SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR2

				COMMIT TRANSACTION
				GOTO Done2

				ERROR2:	
					ROLLBACK TRANSACTION
					RAISERROR (@ErrorMessage, 16, 1)
				END  -- @older_funding_id IS NOT NULL

		Done2:
		SET @current_id = @current_id + 1
	END  -- end while loop

IF OBJECT_ID('tempdb..#tempData2') IS NOT NULL DROP TABLE #tempData2


IF @max_id > 0 
	BEGIN
		EXEC dbo.CSP_UpdCalculations 'SYSTEM'
	END

-- now if a refund can't be moved and is less than 25, then make it a fee
SELECT @current_id = 0, @max_id = 0
CREATE TABLE #tempData3 (id int not null identity, funding_id BIGINT, refund MONEY)

INSERT INTO #tempData3 (funding_id, refund)
SELECT f.id, f.refund_out
FROM fundings f
WHERE ABS(f.refund_out) < 25 AND ABS(f.refund_out) > 0

SELECT @current_id = COALESCE(MIN(id), 0), @max_id = COALESCE(MAX(id), 0) FROM #tempData3

WHILE @current_id <= @max_id AND @max_id <> 0
	BEGIN
		SELECT @funding_id = t.funding_id, @refund = t.refund
		FROM #tempData3 t
		WHERE t.id = @current_id

		BEGIN TRANSACTION
		-- get a new batch id
		EXEC @newBatchID = dbo.CSP_GetNewBatchNumber '', 'SYSTEM', 'Negative Funding', '', @tomorrow, 0
		
		SET @tempComments = 'Refund-Fee'; SET @tempAmount = ABS(@refund)
		EXEC dbo.CSP_DistributeTransaction_v2 @funding_id, @tempAmount, NULL, @today, @tempComments, @newBatchID, NULL, NULL, NULL, 
			@today, NULL, NULL, 23

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR3

		EXEC dbo.CSP_DistributeTransaction_v2 @funding_id, @tempAmount, NULL, @today, @tempComments, @newBatchID, NULL, NULL, NULL, 
			@today, NULL, NULL, 9

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR3

		EXEC dbo.CSP_DistributeTransaction_v2 @funding_id, @tempAmount, NULL, @today, @tempComments, @newBatchID, NULL, NULL, NULL, 
			@today, NULL, NULL, 1

		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR3

		-- set the batch to Processed
		UPDATE batches SET batch_status = 'Closed', settle_date = @tomorrow WHERE batch_id = @newBatchID
		SELECT @errorNum = @@ERROR, @ErrorMessage = ERROR_MESSAGE(); IF @errorNum != 0 GOTO ERROR3

		COMMIT TRANSACTION
		GOTO Done3

		ERROR3:	
			ROLLBACK TRANSACTION
			RAISERROR (@ErrorMessage, 16, 1)

		Done3:
		SET @current_id = @current_id + 1
	END  -- end while loop

IF OBJECT_ID('tempdb..#tempData3') IS NOT NULL DROP TABLE #tempData3

IF @max_id > 0 
	BEGIN
		EXEC dbo.CSP_UpdCalculations 'SYSTEM'
	END

END
GO
