SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

 

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2020-09-11  -- never forget
-- Description: Returns 1 if user has permission otherwise returns 0
-- =============================================
CREATE PROCEDURE [dbo].[BSK_DoesUserHavePermission]
(
	@email					NVARCHAR (100),
	@permission_name		VARCHAR (50)
)
AS
BEGIN
SET NOCOUNT ON

-- first check if they are admin, if so they get access to everything
IF EXISTS (SELECT 1
			FROM users u WITH (NOLOCK)
			INNER JOIN user_roles ur WITH (NOLOCK) ON u.user_id = ur.user_id						
			INNER JOIN roles r WITH (NOLOCK) ON ur.role_id = r.role_id
			WHERE u.email = @email AND r.role_name = 'Administrator'
			)
	BEGIN
		-- has admin
		SELECT 1 AS HasPermission
	END
ELSE IF EXISTS (SELECT 1
			FROM permissions p WITH (NOLOCK)
			INNER JOIN role_permissions rp WITH (NOLOCK) ON p.permission_id = rp.permission_id
			INNER JOIN roles r WITH (NOLOCK) ON rp.role_id = r.role_id
			INNER JOIN user_roles ur WITH (NOLOCK) ON r.role_id = ur.role_id
			INNER JOIN users u WITH (NOLOCK) ON ur.user_id = u.user_id
			WHERE p.permission_name = @permission_name AND u.email = @email
		)
	BEGIN
		SELECT 1 AS HasPermission
	END
ELSE
	BEGIN
		SELECT 0 AS HasPermission
	END

END


GO
