SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:     Ryan Brown
-- Create Date: 2021-04-20
-- Description: Returns a list of all the email addresses we need to synch
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetEmailAccountsToSynch]

AS

SET NOCOUNT ON;

SELECT DISTINCT value AS email_address
FROM settings WITH (NOLOCK)
WHERE setting LIKE 'GraphSenderEmail%'
	AND setting != 'GraphSenderEmail'



GO
