SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-09-21
-- Description:	Calculates funding bonuses
-- =============================================
CREATE PROCEDURE [dbo].[CSP_CalcFundingBonuses]

AS
BEGIN
SET NOCOUNT ON;
-- vars
DECLARE @temp_count INT, @new_id INT
DECLARE @bad_records BIT; SET @bad_records = 0
-- end vars
-- setup dates
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())
DECLARE @first_day_of_month DATE; SELECT @first_day_of_month = DATEFROMPARTS(YEAR(@today),MONTH(@today),1)
DECLARE @first_day_last_month DATE; SELECT @first_day_last_month = DATEADD(MONTH, DATEDIFF(MONTH, 0, @today)-1, 0) --First day of previous month
DECLARE @last_day_last_month DATE; SELECT @last_day_last_month = DATEADD(MONTH, DATEDIFF(MONTH, -1, @today)-1, -1) --Last Day of previous month
-- end dates


IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData
CREATE TABLE #tempData (id INT IDENTITY (1, 1), iso_id BIGINT, funding_id BIGINT, funding_bonus_id INT, bonus_percent_level_1 NUMERIC(16, 4), bonus_percent_level_2 NUMERIC(16, 4), 
	bonus_percent_level_3 NUMERIC(16, 4), bonus_percent_level_4 NUMERIC(16, 4), bonus_percent_level_5 NUMERIC(16, 4), bonus_amount_level_1 NUMERIC(16, 4), 
	bonus_amount_level_2 NUMERIC(16, 4), bonus_amount_level_3 NUMERIC(16, 4), bonus_amount_level_4 NUMERIC(16, 4), bonus_amount_level_5 NUMERIC(16, 4), 
	funding_amount MONEY, iso_name NVARCHAR (100), rtr MONEY)


IF OBJECT_ID('tempdb..#tempISOs') IS NOT NULL DROP TABLE #tempISOs
CREATE TABLE #tempISOs (id INT IDENTITY (1, 1), iso_id BIGINT, iso_name NVARCHAR (100), total_volume MONEY, total_rtr MONEY, pricing NUMERIC(10, 4), widget_count INT)

	
-- first do volume
INSERT INTO #tempData(iso_id, funding_id, funding_bonus_id, bonus_percent_level_1, bonus_percent_level_2, bonus_percent_level_3, bonus_percent_level_4, bonus_percent_level_5, 
	bonus_amount_level_1, bonus_amount_level_2, bonus_amount_level_3, bonus_amount_level_4, bonus_amount_level_5, funding_amount, iso_name)
SELECT b.iso_id, b.funding_id, b.funding_bonus_id, b.bonus_percent_level_1, b.bonus_percent_level_2, b.bonus_percent_level_3, b.bonus_percent_level_4, b.bonus_percent_level_5, 
	b.bonus_amount_level_1, b.bonus_amount_level_2, b.bonus_amount_level_3, b.bonus_amount_level_4, b.bonus_amount_level_5, f.purchase_price, b.iso_name
FROM funding_bonuses b
INNER JOIN fundings f ON b.funding_id = f.id
WHERE b.bonus_metric = 'Volume' 
	AND (
		(b.bonus_filter = '_New' AND f.contract_type IN ('New', 'Renewal_Concurrent'))
		OR 
		(b.bonus_filter = 'Renewals' AND f.contract_type IN ('Renewal', 'Renewal_Concurrent'))
		OR
		(b.bonus_filter = 'Both')
	)
	AND f.funded_date BETWEEN @first_day_last_month AND @last_day_last_month
	

-- check to see if any of the iso bonus values changed during the month by grouping by the iso id and all the values and seeing if there are more than one record per iso id.
IF EXISTS (
	SELECT iso_id 
	FROM (
		SELECT iso_id, bonus_percent_level_1, bonus_percent_level_2, bonus_percent_level_3, bonus_percent_level_4, bonus_percent_level_5, 
			bonus_amount_level_1, bonus_amount_level_2, bonus_amount_level_3, bonus_amount_level_4, bonus_amount_level_5 
		FROM #tempData
		GROUP BY iso_id, bonus_percent_level_1, bonus_percent_level_2, bonus_percent_level_3, bonus_percent_level_4, bonus_percent_level_5, 
			bonus_amount_level_1, bonus_amount_level_2, bonus_amount_level_3, bonus_amount_level_4, bonus_amount_level_5
	) y
	GROUP BY iso_id
	HAVING COUNT(*) > 1
)
	BEGIN
		SET @bad_records = 1
	END
ELSE
	BEGIN
		SET @bad_records = 0
	END


IF @bad_records = 1
	BEGIN
		SELECT 'Volume failed'
	END
ELSE
	BEGIN
		INSERT INTO #tempISOs (iso_id, iso_name, total_volume)
		SELECT iso_id, iso_name, SUM(funding_amount)
		FROM #tempData
		GROUP BY iso_id, iso_name

		INSERT INTO iso_bonuses(iso_name, iso_id, bonus_amount, pay_date, create_date, change_date, change_user, create_user, approved_by, approved_date,
			sent_to_acct, bonus_month, bonus_year, funding_bonus_id, bonus_percent_level, bonus_amount_level, total_amount, total_volume)
		SELECT i.iso_name, i.iso_id, i.total_volume * (
			CASE WHEN i.total_volume >= d.bonus_amount_level_5 THEN d.bonus_percent_level_5
				 WHEN i.total_volume >= d.bonus_amount_level_4 THEN d.bonus_percent_level_4
				 WHEN i.total_volume >= d.bonus_amount_level_3 THEN d.bonus_percent_level_3
				 WHEN i.total_volume >= d.bonus_amount_level_2 THEN d.bonus_percent_level_2
				 WHEN i.total_volume >= d.bonus_amount_level_1 THEN d.bonus_percent_level_1
				 ELSE 0
			END) / 100, 
			NULL, GETUTCDATE(), GETUTCDATE(), 'SYSTEM', 'SYSTEM', NULL, NULL, NULL AS sent_to_acct, MONTH(@first_day_last_month), YEAR(@first_day_last_month),
			d.funding_bonus_id, 
			CASE WHEN i.total_volume >= d.bonus_amount_level_5 THEN d.bonus_percent_level_5
				 WHEN i.total_volume >= d.bonus_amount_level_4 THEN d.bonus_percent_level_4
				 WHEN i.total_volume >= d.bonus_amount_level_3 THEN d.bonus_percent_level_3
				 WHEN i.total_volume >= d.bonus_amount_level_2 THEN d.bonus_percent_level_2
				 WHEN i.total_volume >= d.bonus_amount_level_1 THEN d.bonus_percent_level_1
				 ELSE 0
			END, 
			CASE WHEN i.total_volume >= d.bonus_amount_level_5 THEN d.bonus_amount_level_5
				 WHEN i.total_volume >= d.bonus_amount_level_4 THEN d.bonus_amount_level_4
				 WHEN i.total_volume >= d.bonus_amount_level_3 THEN d.bonus_amount_level_3
				 WHEN i.total_volume >= d.bonus_amount_level_2 THEN d.bonus_amount_level_2
				 WHEN i.total_volume >= d.bonus_amount_level_1 THEN d.bonus_amount_level_1
				 ELSE 0
			END, i.total_volume, i.total_volume
		FROM #tempISOs i
		INNER JOIN(
			SELECT MIN(id) AS id, iso_id
			FROM #tempData 
			GROUP BY iso_id
		) x ON i.iso_id = x.iso_id
		INNER JOIN #tempData d ON x.id = d.id
		WHERE NOT EXISTS (SELECT 1 FROM iso_bonuses ib WHERE ib.iso_id = i.iso_id AND ib.bonus_month = MONTH(@first_day_last_month) AND ib.bonus_year = YEAR(@first_day_last_month))
		
		UPDATE f
		SET iso_bonus_id = b.iso_bonus_id, change_date = GETUTCDATE(), change_user = 'SYSTEM'
		FROM fundings f
		INNER JOIN #tempData t ON f.id = t.funding_id
		INNER JOIN iso_bonuses b ON t.iso_id = b.iso_id AND b.bonus_month = MONTH(@first_day_last_month) AND b.bonus_year = YEAR(@first_day_last_month)
		

		UPDATE f
		SET bonus_commission = ROUND((b.bonus_percent_level / 100.00 * f.purchase_price), 2), bonus_commission_percent = b.bonus_percent_level
		FROM fundings f
		INNER JOIN iso_bonuses b ON f.iso_bonus_id = b.iso_bonus_id
		WHERE f.id IN (SELECT funding_id FROM #tempData)			

		-- fix any rounding issues
		UPDATE f
		SET bonus_commission = bonus_commission + (b.bonus_amount - (SELECT SUM(bonus_commission) FROM fundings WHERE iso_bonus_id = f.iso_bonus_id))
		FROM fundings f
		INNER JOIN iso_bonuses b ON f.iso_bonus_id = b.iso_bonus_id
		WHERE f.id = (SELECT MIN(funding_id) FROM #tempData)			
	END

------------------------------------------------
-------------------------------------------------
---------------------------------------------
-----------------------------------------------
-- now do pricing
----------------------------------------
----------------------------------------
-- 
TRUNCATE TABLE #tempData
TRUNCATE TABLE #tempISOs

INSERT INTO #tempData(iso_id, funding_id, funding_bonus_id, bonus_percent_level_1, bonus_percent_level_2, bonus_percent_level_3, bonus_percent_level_4, bonus_percent_level_5, 
	bonus_amount_level_1, bonus_amount_level_2, bonus_amount_level_3, bonus_amount_level_4, bonus_amount_level_5, rtr, iso_name, funding_amount)
SELECT b.iso_id, b.funding_id, b.funding_bonus_id, b.bonus_percent_level_1, b.bonus_percent_level_2, b.bonus_percent_level_3, b.bonus_percent_level_4, b.bonus_percent_level_5, 
	b.bonus_amount_level_1, b.bonus_amount_level_2, b.bonus_amount_level_3, b.bonus_amount_level_4, b.bonus_amount_level_5, f.portfolio_value, b.iso_name, f.purchase_price
FROM funding_bonuses b
INNER JOIN fundings f ON b.funding_id = f.id
WHERE b.bonus_metric = 'Pricing' 
	AND (
		(b.bonus_filter = '_New' AND f.contract_type IN ('New', 'Renewal_Concurrent'))
		OR 
		(b.bonus_filter = 'Renewals' AND f.contract_type IN ('Renewal', 'Renewal_Concurrent'))
		OR
		(b.bonus_filter = 'Both')
	)
	AND f.funded_date BETWEEN @first_day_last_month AND @last_day_last_month
	
-- check to see if any of the iso bonus values changed during the month by grouping by the iso id and all the values and seeing if there are more than one record per iso id.
IF EXISTS (
	SELECT iso_id 
	FROM (
		SELECT iso_id, bonus_percent_level_1, bonus_percent_level_2, bonus_percent_level_3, bonus_percent_level_4, bonus_percent_level_5, 
			bonus_amount_level_1, bonus_amount_level_2, bonus_amount_level_3, bonus_amount_level_4, bonus_amount_level_5 
		FROM #tempData
		GROUP BY iso_id, bonus_percent_level_1, bonus_percent_level_2, bonus_percent_level_3, bonus_percent_level_4, bonus_percent_level_5, 
			bonus_amount_level_1, bonus_amount_level_2, bonus_amount_level_3, bonus_amount_level_4, bonus_amount_level_5
	) y
	GROUP BY iso_id
	HAVING COUNT(*) > 1
)
	BEGIN
		SET @bad_records = 1
	END
ELSE
	BEGIN
		SET @bad_records = 0
	END


IF @bad_records = 1
	BEGIN
		SELECT 'Pricing failed'
	END
ELSE
	BEGIN
		INSERT INTO #tempISOs (iso_id, iso_name, total_volume, total_rtr, pricing)
		SELECT iso_id, iso_name, SUM(funding_amount), SUM(rtr), SUM(rtr)/SUM(funding_amount)
		FROM #tempData
		GROUP BY iso_id, iso_name

		INSERT INTO iso_bonuses(iso_name, iso_id, bonus_amount, pay_date, create_date, change_date, change_user, create_user, approved_by, approved_date,
			sent_to_acct, bonus_month, bonus_year, funding_bonus_id, bonus_percent_level, bonus_amount_level, total_amount, total_volume)
		SELECT i.iso_name, i.iso_id, i.total_volume * (
			CASE WHEN i.pricing >= d.bonus_amount_level_5 THEN d.bonus_percent_level_5
				 WHEN i.pricing >= d.bonus_amount_level_4 THEN d.bonus_percent_level_4
				 WHEN i.pricing >= d.bonus_amount_level_3 THEN d.bonus_percent_level_3
				 WHEN i.pricing >= d.bonus_amount_level_2 THEN d.bonus_percent_level_2
				 WHEN i.pricing >= d.bonus_amount_level_1 THEN d.bonus_percent_level_1
				 ELSE 0
			END) / 100, 
			NULL, GETUTCDATE(), GETUTCDATE(), 'SYSTEM', 'SYSTEM', NULL, NULL, NULL AS sent_to_acct, MONTH(@first_day_last_month), YEAR(@first_day_last_month),
			d.funding_bonus_id, 
			CASE WHEN i.pricing >= d.bonus_amount_level_5 THEN d.bonus_percent_level_5
				 WHEN i.pricing >= d.bonus_amount_level_4 THEN d.bonus_percent_level_4
				 WHEN i.pricing >= d.bonus_amount_level_3 THEN d.bonus_percent_level_3
				 WHEN i.pricing >= d.bonus_amount_level_2 THEN d.bonus_percent_level_2
				 WHEN i.pricing >= d.bonus_amount_level_1 THEN d.bonus_percent_level_1
				 ELSE 0
			END, 
			CASE WHEN i.pricing >= d.bonus_amount_level_5 THEN d.bonus_amount_level_5
				 WHEN i.pricing >= d.bonus_amount_level_4 THEN d.bonus_amount_level_4
				 WHEN i.pricing >= d.bonus_amount_level_3 THEN d.bonus_amount_level_3
				 WHEN i.pricing >= d.bonus_amount_level_2 THEN d.bonus_amount_level_2
				 WHEN i.pricing >= d.bonus_amount_level_1 THEN d.bonus_amount_level_1
				 ELSE 0
			END, i.pricing, i.total_volume
		FROM #tempISOs i
		INNER JOIN(
			SELECT MIN(id) AS id, iso_id
			FROM #tempData 
			GROUP BY iso_id
		) x ON i.iso_id = x.iso_id
		INNER JOIN #tempData d ON x.id = d.id
		WHERE NOT EXISTS (SELECT 1 FROM iso_bonuses ib WHERE ib.iso_id = i.iso_id AND ib.bonus_month = MONTH(@first_day_last_month) AND ib.bonus_year = YEAR(@first_day_last_month))

		UPDATE f
		SET iso_bonus_id = b.iso_bonus_id, change_date = GETUTCDATE(), change_user = 'SYSTEM'
		FROM fundings f
		INNER JOIN #tempData t ON f.id = t.funding_id
		INNER JOIN iso_bonuses b ON t.iso_id = b.iso_id AND b.bonus_month = MONTH(@first_day_last_month) AND b.bonus_year = YEAR(@first_day_last_month)
	

		UPDATE f
		SET bonus_commission = ROUND((b.bonus_percent_level / 100.00 * f.purchase_price), 2), bonus_commission_percent = b.bonus_percent_level
		FROM fundings f
		INNER JOIN iso_bonuses b ON f.iso_bonus_id = b.iso_bonus_id
		WHERE f.id IN (SELECT funding_id FROM #tempData)			

		-- fix any rounding issues
		UPDATE f
		SET bonus_commission = bonus_commission + (b.bonus_amount - (SELECT SUM(bonus_commission) FROM fundings WHERE iso_bonus_id = f.iso_bonus_id))
		FROM fundings f
		INNER JOIN iso_bonuses b ON f.iso_bonus_id = b.iso_bonus_id
		WHERE f.id = (SELECT MIN(funding_id) FROM #tempData)			
	END


------------------------------------------------
-------------------------------------------------
---------------------------------------------
-----------------------------------------------
-- now do quantity
----------------------------------------
----------------------------------------
-- 
TRUNCATE TABLE #tempData
TRUNCATE TABLE #tempISOs

INSERT INTO #tempData(iso_id, funding_id, funding_bonus_id, bonus_percent_level_1, bonus_percent_level_2, bonus_percent_level_3, bonus_percent_level_4, bonus_percent_level_5, 
	bonus_amount_level_1, bonus_amount_level_2, bonus_amount_level_3, bonus_amount_level_4, bonus_amount_level_5, rtr, iso_name, funding_amount)
SELECT b.iso_id, b.funding_id, b.funding_bonus_id, b.bonus_percent_level_1, b.bonus_percent_level_2, b.bonus_percent_level_3, b.bonus_percent_level_4, b.bonus_percent_level_5, 
	b.bonus_amount_level_1, b.bonus_amount_level_2, b.bonus_amount_level_3, b.bonus_amount_level_4, b.bonus_amount_level_5, f.portfolio_value, b.iso_name, f.purchase_price
FROM funding_bonuses b
INNER JOIN fundings f ON b.funding_id = f.id
WHERE b.bonus_metric = 'Number_of_Widgets' 
	AND (
		(b.bonus_filter = '_New' AND f.contract_type IN ('New', 'Renewal_Concurrent'))
		OR 
		(b.bonus_filter = 'Renewals' AND f.contract_type IN ('Renewal', 'Renewal_Concurrent'))
		OR
		(b.bonus_filter = 'Both')
	)
	AND f.funded_date BETWEEN @first_day_last_month AND @last_day_last_month
	
-- check to see if any of the iso bonus values changed during the month by grouping by the iso id and all the values and seeing if there are more than one record per iso id.
IF EXISTS (
	SELECT iso_id 
	FROM (
		SELECT iso_id, bonus_percent_level_1, bonus_percent_level_2, bonus_percent_level_3, bonus_percent_level_4, bonus_percent_level_5, 
			bonus_amount_level_1, bonus_amount_level_2, bonus_amount_level_3, bonus_amount_level_4, bonus_amount_level_5 
		FROM #tempData
		GROUP BY iso_id, bonus_percent_level_1, bonus_percent_level_2, bonus_percent_level_3, bonus_percent_level_4, bonus_percent_level_5, 
			bonus_amount_level_1, bonus_amount_level_2, bonus_amount_level_3, bonus_amount_level_4, bonus_amount_level_5
	) y
	GROUP BY iso_id
	HAVING COUNT(*) > 1
)
	BEGIN
		SET @bad_records = 1
	END
ELSE
	BEGIN
		SET @bad_records = 0
	END


IF @bad_records = 1
	BEGIN
		SELECT 'Quantity failed'
	END
ELSE
	BEGIN
		INSERT INTO #tempISOs (iso_id, iso_name, total_volume, widget_count)
		SELECT iso_id, iso_name, SUM(funding_amount), COUNT(*)
		FROM #tempData
		GROUP BY iso_id, iso_name

		INSERT INTO iso_bonuses(iso_name, iso_id, bonus_amount, pay_date, create_date, change_date, change_user, create_user, approved_by, approved_date,
			sent_to_acct, bonus_month, bonus_year, funding_bonus_id, bonus_percent_level, bonus_amount_level, total_amount, total_volume)
		SELECT i.iso_name, i.iso_id, i.total_volume * (
			CASE WHEN i.widget_count >= d.bonus_amount_level_5 THEN d.bonus_percent_level_5
				 WHEN i.widget_count >= d.bonus_amount_level_4 THEN d.bonus_percent_level_4
				 WHEN i.widget_count >= d.bonus_amount_level_3 THEN d.bonus_percent_level_3
				 WHEN i.widget_count >= d.bonus_amount_level_2 THEN d.bonus_percent_level_2
				 WHEN i.widget_count >= d.bonus_amount_level_1 THEN d.bonus_percent_level_1
				 ELSE 0
			END) / 100, 
			NULL, GETUTCDATE(), GETUTCDATE(), 'SYSTEM', 'SYSTEM', NULL, NULL, NULL AS sent_to_acct, MONTH(@first_day_last_month), YEAR(@first_day_last_month),
			funding_bonus_id, 
			CASE WHEN i.widget_count >= d.bonus_amount_level_5 THEN d.bonus_percent_level_5
				 WHEN i.widget_count >= d.bonus_amount_level_4 THEN d.bonus_percent_level_4
				 WHEN i.widget_count >= d.bonus_amount_level_3 THEN d.bonus_percent_level_3
				 WHEN i.widget_count >= d.bonus_amount_level_2 THEN d.bonus_percent_level_2
				 WHEN i.widget_count >= d.bonus_amount_level_1 THEN d.bonus_percent_level_1
				 ELSE 0
			END, 
			CASE WHEN i.widget_count >= d.bonus_amount_level_5 THEN d.bonus_amount_level_5
				 WHEN i.widget_count >= d.bonus_amount_level_4 THEN d.bonus_amount_level_4
				 WHEN i.widget_count >= d.bonus_amount_level_3 THEN d.bonus_amount_level_3
				 WHEN i.widget_count >= d.bonus_amount_level_2 THEN d.bonus_amount_level_2
				 WHEN i.widget_count >= d.bonus_amount_level_1 THEN d.bonus_amount_level_1
				 ELSE 0
			END, i.widget_count, i.total_volume
		FROM #tempISOs i
		INNER JOIN(
			SELECT MIN(id) AS id, iso_id
			FROM #tempData 
			GROUP BY iso_id
		) x ON i.iso_id = x.iso_id
		INNER JOIN #tempData d ON x.id = d.id
		WHERE NOT EXISTS (SELECT 1 FROM iso_bonuses ib WHERE ib.iso_id = i.iso_id AND ib.bonus_month = MONTH(@first_day_last_month) AND ib.bonus_year = YEAR(@first_day_last_month))
		
		UPDATE f
		SET iso_bonus_id = b.iso_bonus_id, change_date = GETUTCDATE(), change_user = 'SYSTEM'
		FROM fundings f
		INNER JOIN #tempData t ON f.id = t.funding_id
		INNER JOIN iso_bonuses b ON t.iso_id = b.iso_id AND b.bonus_month = MONTH(@first_day_last_month) AND b.bonus_year = YEAR(@first_day_last_month)

		UPDATE f
		SET bonus_commission = ROUND((b.bonus_percent_level / 100.00 * f.purchase_price), 2), bonus_commission_percent = b.bonus_percent_level
		FROM fundings f
		INNER JOIN iso_bonuses b ON f.iso_bonus_id = b.iso_bonus_id
		WHERE f.id IN (SELECT funding_id FROM #tempData)			

		-- fix any rounding issues
		UPDATE f
		SET bonus_commission = bonus_commission + (b.bonus_amount - (SELECT SUM(bonus_commission) FROM fundings WHERE iso_bonus_id = f.iso_bonus_id))
		FROM fundings f
		INNER JOIN iso_bonuses b ON f.iso_bonus_id = b.iso_bonus_id
		WHERE f.id = (SELECT MIN(funding_id) FROM #tempData)			
	END


END


GO
