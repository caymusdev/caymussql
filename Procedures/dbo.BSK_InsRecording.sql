SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-11-18
-- Description:	Saves a phone log into the database.  Many logs can happen during a single call
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsRecording]
(
	@CallSid				[VARCHAR] (250),
	@RecordingUrl				[NVARCHAR] (200),
	@RecordingSid				[NVARCHAR] (200),
	@RecordingDuration		[NVARCHAR] (200),
	@Caller					[NVARCHAR] (100),
	@Called				[VARCHAR] (50)
)
AS
BEGIN
SET NOCOUNT ON;


INSERT INTO recordings(CallSid, RecordingUrl, RecordingSid, RecordingDuration, Caller, Called, create_date, change_date, change_user, create_user)
VALUES (@CallSid, @RecordingUrl, @RecordingSid, @RecordingDuration, @Caller, @Called, GETUTCDATE(), GETUTCDATE(), 'SYSTEM', 'SYSTEM')

END

GO
