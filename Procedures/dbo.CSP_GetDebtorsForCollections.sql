SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-03-19
-- Description:	Gets a list of fundings to send to collections as debtors
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetDebtorsForCollections]

AS
BEGIN
SET NOCOUNT ON;

--2019-06-18 - changed to pull affiliates instead of fundings


SELECT f.affiliate_id, COUNT(*) AS collect_contracts, a.affiliate_name, NULL AS frequency, MIN(f.funded_date) AS funded_date, MAX(f.last_payment_date) AS last_payment_date, 
	CASE WHEN EXISTS (SELECT 1 FROM fundings f2 WHERE f2.affiliate_id = f.affiliate_id AND f2.collection_type = 'Collections')
		THEN 'Collections' ELSE 'Customer_Service' END AS collection_type, 
	SUM(f.payoff_amount) AS payoff_amount, SUM(f.fees_out) AS fees_out, SUM(f.paid_off_amount) AS paid_off_amount, 
	SUM(f.purchase_price) AS purchase_price, 
	CONVERT(DECIMAL (18, 2),(SUM(f.paid_off_amount) / SUM(f.portfolio_value)) * 100.00, 2) AS percent_paid, SUM(COALESCE(f.pp_out, 0) + COALESCE(f.commission_amount, 0)) AS COA_out,	
	NULL AS missed_ach,
	MAX(f.estimated_turn) AS estimated_turn, MAX(f.calc_turn) AS calc_turn, MAX(f.actual_turn) AS paid_calc_turn, MIN(f.percent_performance) AS percent_performance, 
	MAX(f.actual_turn) AS actual_turn, SUM(CASE f.frequency WHEN 'Daily' THEN f.weekday_payment ELSE f.weekly_payment END) AS payment,
	MIN(f.payments_stopped_date) AS payments_stopped_date, SUM(f.portfolio_value) AS portfolio_value, 
	NULL AS bank_name, NULL AS routing_number, NULL AS account_number, NULL AS contract_status, 
	CASE SUM(f.payoff_amount) WHEN 0 THEN 'Closed' ELSE 'Active' END AS mode, 
	(SELECT COUNT(*) FROM fundings f2 WHERE f2.affiliate_id = f.affiliate_id AND 
		(contract_status = 1 OR (contract_status = 4 AND contract_substatus_id = 1))) AS open_contracts,
	CASE WHEN EXISTS (SELECT 1 FROM fundings f2 WHERE f2.affiliate_id = f.affiliate_id AND f2.collection_type = 'Collections') THEN 'VAU' ELSE 'NIC' END AS operator, 
	NULL AS contract_number, NULL AS merchant_name
FROM fundings f
INNER JOIN affiliates a ON f.affiliate_id = a.affiliate_id
WHERE (COALESCE(f.collection_type, '') <> '' AND f.payoff_amount > 0)
	OR (a.sent_to_collect IS NOT NULL AND a.closed_in_collections IS NULL)
GROUP BY f.affiliate_id, a.affiliate_name
ORDER BY a.affiliate_name


END
GO
