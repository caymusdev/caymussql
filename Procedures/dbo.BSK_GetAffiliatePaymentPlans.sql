SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2020-12-21
-- Description: Gets a list of payment plans for an affiliate
-- =============================================
CREATE PROCEDURE dbo.[BSK_GetAffiliatePaymentPlans]
(
	@affiliate_id			INT
)
AS
BEGIN
SET NOCOUNT ON

SELECT pp.payment_plan_id, pp.affiliate_id, pps.performance_status, pp.broken_payment_date, pp.payment_plan_type, 
	'Status: ' + pps.performance_status + COALESCE(' - Broken Date: ' + CONVERT(CHAR (10), pp.broken_payment_date, 126), '') AS payment_plan_description,
	COALESCE(pp.payment_plan_name, pp.payment_plan_type, CONVERT(VARCHAR (50), pp.payment_plan_id)) AS payment_plan_name
FROM payment_plans pp
LEFT JOIN payment_plan_status_codes pps ON pp.current_status = pps.id
WHERE pp.affiliate_id = @affiliate_id
ORDER BY payment_plan_description

END

GO
