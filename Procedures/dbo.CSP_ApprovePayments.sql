SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-06-13
-- Description:	Accepts a delimited list of payment ids to approve
-- =============================================
CREATE PROCEDURE [dbo].[CSP_ApprovePayments]
(	
	@payment_ids			NVARCHAR (4000), -- could be a delimited list of payment ids
	@amounts				VARCHAR (4000) -- a delimited list of amounts for each payment
)
AS
BEGIN	
SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#tempIDs') IS NOT NULL DROP TABLE #tempIDs
CREATE TABLE #tempIDs (id int not null identity, payment_id INT)

IF OBJECT_ID('tempdb..#tempAmounts') IS NOT NULL DROP TABLE #tempAmounts
CREATE TABLE #tempAmounts (id int not null identity, amount MONEY)

INSERT INTO #tempIDs(payment_id)
SELECT a.value
FROM string_split(@payment_ids, '|') a

INSERT INTO #tempAmounts(amount)
SELECT a.value
FROM string_split(@amounts, '|') a

UPDATE payments 
SET approved_flag = 1, amount = a.amount
FROM payments p
INNER JOIN #tempIDs i ON p.payment_id = i.payment_id
INNER JOIN #tempAmounts a ON i.id = a.id



IF OBJECT_ID('tempdb..#tempIDs') IS NOT NULL DROP TABLE #tempIDs
IF OBJECT_ID('tempdb..#tempAmounts') IS NOT NULL DROP TABLE #tempAmounts

END
GO
