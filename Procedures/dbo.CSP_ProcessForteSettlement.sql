SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-05-22
-- Description:	Takes a forte_settlement id and will create transactions for it
-- =============================================
CREATE PROCEDURE [dbo].[CSP_ProcessForteSettlement]
(
	@forte_settle_id			INT
)

AS
BEGIN
SET NOCOUNT ON;


DECLARE @funding_id INT, @transaction_id NVARCHAR (100), @settle_date DATETIME
DECLARE @trans_date DATE, @trans_amount MONEY, @batch_id INT, @record_id INT
DECLARE @response_code NVARCHAR (50), @settle_type NVARCHAR (50), @contract_number NVARCHAR (50), @comments NVARCHAR (500), @forte_funding_id VARCHAR(50)

SELECT @response_code = s.settle_response_code, 
	@trans_amount = CASE LOWER(s.settle_type) WHEN 'withdrawal' THEN s.settle_amount ELSE t.authorization_amount END, 
	@settle_type = s.settle_type, 
--	@trans_date = CASE WHEN LOWER(s.settle_type) IN ('reject') THEN COALESCE(p.trans_date, s.settle_date, t.origination_date)
	--	ELSE COALESCE(f.effective_date, s.settle_date, t.origination_date) END, 
	@trans_date = COALESCE(f.effective_date, s.settle_date, t.origination_date), 
	@contract_number = s.customer_id,
	@comments = COALESCE(t.company_name, '') + ' - ' + COALESCE(s.customer_id, '') + ' ' + s.settle_response_code + ' : ' + s.settle_type, @record_id = s.forte_settle_id, @batch_id = s.batch_id,
	@forte_funding_id = s.funding_id, @transaction_id = s.transaction_id, @settle_date = s.settle_date
FROM forte_settlements s
INNER JOIN forte_trans t ON s.transaction_id = t.transaction_id AND t.origination_date IS NOT NULL
LEFT JOIN forte_fundings f ON s.funding_id = f.funding_id
LEFT JOIN payments p ON s.transaction_id = p.transaction_id
WHERE s.forte_settle_id = @forte_settle_id

EXEC @funding_id = dbo.CSP_GetFundingIDForTransaction @trans_date, @contract_number, NULL, NULL
IF @funding_id = -1 SET @funding_id = NULL

-- if it is a reject of $0.00 it needs to be passed as 9 because it won't be tied to a forte funding so will be in its own batch
DECLARE @previous_id INT; SET @previous_id = NULL
DECLARE @trans_type_id INT; SET @trans_type_id = NULL

/*
IF @response_code LIKE 'R%' AND @trans_amount = 0 AND COALESCE(@forte_funding_id, '') = ''
	BEGIN
		SELECT @trans_type_id = 9, @trans_amount = COALESCE(dbo.CF_GetSetting('FeeAmount'), 72)
	END
-- end rejects in their own batch, not on forte fundings
*/

--EXEC dbo.CSP_DistributeTransaction @trans_date, @funding_id, @trans_amount, @comments, @batch_id, @record_id, @response_code, @settle_type, @previous_id, @trans_type_id
EXEC dbo.CSP_DistributeTransaction_v2 @funding_id, @trans_amount, @transaction_id, @trans_date, @comments, @batch_id, @record_id, 'forte_settlements', 
	NULL, @settle_date, @settle_type, @response_code

END
GO
