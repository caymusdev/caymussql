SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-06-07
-- Description: Checks for a batch that has the EVO file
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetEVOBatch]
(
	@fileName		NVARCHAR (50)
)
AS
BEGIN
SET NOCOUNT ON;

SELECT *
FROM batches b
WHERE b.file_name = @fileName AND b.batch_type LIKE 'EVO%'
END
GO
