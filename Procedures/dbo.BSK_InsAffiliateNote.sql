SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Rick Pina
-- Create Date: 2020-12-09
-- Description: Adds Entry into Business Open table
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsAffiliateNote]
(
	@affiliate_id				INT,
	@important_notes				BIT,
	@notes_text				NVARCHAR(MAX),
	@create_user			NVARCHAR (100)

)
AS

SET NOCOUNT ON

	INSERT INTO notes(affiliate_id, important_notes, notes_text, create_user, create_date)
	Values (@affiliate_id, @important_notes, @notes_text, @create_user, GETUTCDATE())

GO
