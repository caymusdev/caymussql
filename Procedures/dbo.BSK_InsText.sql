SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-15
-- Description:	Saves a text into the database
-- =============================================
CREATE PROCEDURE [dbo].[BSK_InsText]
(
	@affiliate_id				INT,
	@merchant_id				INT,
	@contract_id				INT, 
	@to							VARCHAR (50),
	@from						VARCHAR (50), 
	@user_email					NVARCHAR (100),
	@sent						DATETIME,
	@received					DATETIME,
	@text_message				NVARCHAR (MAX),
	@message_id					NVARCHAR (200),
	@text_type					VARCHAR (50),
	@text_data					NVARCHAR(MAX), 
	@concat						BIT,
	@contact_id					INT,
	@sent_from_bossk			BIT,
	@new_text_id				INT OUTPUT
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())

DECLARE @user_id INT
SELECT @user_id = user_id FROM users WHERE email = @user_email

INSERT INTO texts([user_id], [to], [from], affiliate_id, merchant_id, contract_id, sent, received, text_message, create_date, change_date, change_user, create_user, 
	message_id, text_type, text_data, concat, contact_id, sent_from_bossk)
SELECT @user_id, @to, @from, @affiliate_id, @merchant_id, @contract_id, @sent, @received, @text_message, GETUTCDATE(), GETUTCDATE(), @user_email, @user_email, 
	@message_id, @text_type, @text_data, @concat, @contact_id, @sent_from_bossk
WHERE NOT EXISTS (SELECT 1 FROM texts t2 WHERE t2.message_id = @message_id)

SELECT @new_text_id = SCOPE_IDENTITY()

-- create a notification if it is a received text
IF @received IS NOT NULL AND @new_text_id IS NOT NULL
	BEGIN
		DECLARE @message NVARCHAR (500)
		SELECT @message = 'New Text from ' + affiliate_name FROM affiliates WHERE affiliate_id = @affiliate_id

		-- from BSK_GetAssignedCollectorForAffiliate
		DECLARE @tempUserID INT
		SELECT @tempUserID = u.user_id
		FROM collector_cases cc
		INNER JOIN cases c ON cc.case_id = c.case_id AND c.active = 1
		INNER JOIN users u ON cc.collector_user_id = u.user_id
		WHERE cc.start_date <= @today AND COALESCE(cc.end_date, '2050-05-09') >= @today AND c.affiliate_id = @affiliate_id

		EXEC dbo.BSK_InsNotification @tempUserID, NULL, @affiliate_id, NULL, NULL, @message, 'NewText', 'System', @new_text_id
	END


-- insert a contact record
EXEC dbo.BSK_InsContactLog @affiliate_id, @merchant_id, @contract_id, 'text', @contact_id, NULL, NULL, NULL, @user_email, NULL, @new_text_id

END

GO
