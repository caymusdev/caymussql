SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-05-21
-- Description:	Will check to see if the settlement is already in the db.  If so, it will be ignored.  
-- Uniqueness is determined by settle_id
-- =============================================
CREATE PROCEDURE [dbo].[CSP_InsForteSettlement]
(
	@settle_id				NVARCHAR (100),
	@customer_token			NVARCHAR (50), 
	@transaction_id			NVARCHAR (100),
	@customer_id			NVARCHAR (50),
	@funding_id				NVARCHAR (50),
	@settle_batch_id		NVARCHAR (100),
	@settle_date			DATETIME,
	@settle_type			NVARCHAR (50),
	@settle_response_code	NVARCHAR (50),
	@settle_amount			MONEY,
	@method					NVARCHAR (50),
	@forte_funding_id		INT,
	@batch_id				INT,
	@change_user			NVARCHAR (100)
)	
AS
BEGIN
SET NOCOUNT ON;


IF NOT EXISTS(SELECT 1 FROM forte_settlements WHERE settle_id = @settle_id)
	BEGIN
		IF @forte_funding_id IS NULL
			BEGIN
				SELECT @forte_funding_id = forte_funding_id FROM forte_fundings WHERE funding_id = @funding_id
			END
		INSERT INTO forte_settlements (settle_id, customer_token, transaction_id, customer_id, funding_id, settle_batch_id, settle_date, 
			settle_type, settle_response_code, settle_amount, method, forte_funding_id, batch_id, create_date, change_date, change_user)
		VALUES (@settle_id, @customer_token, @transaction_id, @customer_id, @funding_id, @settle_batch_id, @settle_date, 
			@settle_type, @settle_response_code, @settle_amount, @method, @forte_funding_id, @batch_id, GETUTCDATE(), GETUTCDATE(), @change_user)
	END

END
GO
