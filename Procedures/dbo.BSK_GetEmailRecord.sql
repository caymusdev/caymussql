SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Rick Pina
-- Create Date: 2020-11-04
-- Description: Gets Single Email Record based on email_id
-- =============================================
CREATE PROCEDURE [dbo].[BSK_GetEmailRecord]
(
	@email_id				NVARCHAR (100)

)
AS
BEGIN
SET NOCOUNT ON

DECLARE @emailsTo VARCHAR(8000)

SELECT @emailsTo = COALESCE(@emailsTo + ';', '') + email_address
FROM email_addresses
WHERE email_id = @email_id AND address_type = 'To'

DECLARE @emailsCc VARCHAR(8000)

SELECT @emailsCc = COALESCE(@emailsCc + ';', '') + email_address
FROM email_addresses
WHERE email_id = @email_id AND address_type = 'CC'

 

SELECT e.email_id, a.affiliate_name, @emailsTo AS email_to, @emailsCc AS email_cc, COALESCE(u.email, e.from_address) AS email, e.subject, e.body, 
	e.create_date, e.message_id, ea.email_attachment_id, ea.attachment_name, e.affiliate_id
FROM emails e WITH (NOLOCK)
LEFT JOIN users u WITH (NOLOCK) ON e.sender_id = u.[user_id]
LEFT JOIN affiliates a WITH (NOLOCK) ON e.affiliate_id = a.affiliate_id
LEFT JOIN email_attachments ea WITH(NOLOCK) ON e.email_id = ea.email_id
WHERE e.email_id = @email_id

END
GO
