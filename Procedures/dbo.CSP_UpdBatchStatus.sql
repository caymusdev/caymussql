SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-05-22
-- Description:	Updates a batch status
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdBatchStatus]
(
	@batch_id			INT,
	@batch_status		NVARCHAR (50)
)

AS
BEGIN
SET NOCOUNT ON;

UPDATE batches SET batch_status = @batch_status, change_user = 'SYSTEM', change_date = GETUTCDATE() WHERE batch_id = @batch_id

END
GO
