SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-08-21
-- Description:	Inserts a funding/contract distribution (payoff) from SOLO if it does not exist
-- =============================================
CREATE PROCEDURE [dbo].[CSP_SyncFundingDistribution]
(
	@funding_id					BIGINT,
	@recipient					NVARCHAR (100),
	@address_1					NVARCHAR (100) = NULL,
	@address_2					NVARCHAR (100) = NULL,
	@city						NVARCHAR (100) = NULL,
	@state_code					NVARCHAR (50) = NULL,
	@zip_code					NVARCHAR (50) = NULL,
	@amount						MONEY,
	@bank_name					NVARCHAR (100) = NULL,
	@account_number				VARCHAR (50) = NULL,
	@routing_number				VARCHAR (50) = NULL,
	@comments					NVARCHAR (500) = NULL,
	@wire_date					DATETIME,
	@discounted_balance			MONEY = NULL,
	@payoff						BIT = NULL
)
AS
BEGIN
SET NOCOUNT ON;


IF @Wire_date IS NULL
	BEGIN
		SELECT @wire_date = funded_date 
		FROM fundings
		WHERE id = @funding_id
	END

IF NOT EXISTS(SELECT 1 FROM funding_distributions WHERE funding_id = @funding_id AND recipient = @recipient AND account_number = @account_number
	AND amount = @amount)
	BEGIN
		INSERT INTO funding_distributions(funding_id, recipient, address_1, address_2, city, state_code, zip_code, amount, bank_name, account_number, 
			routing_number, comments, wire_date, discounted_balance, change_date, change_user, payoff)
		VALUES (@funding_id, @recipient, @address_1, @address_2, @city, @state_code, @zip_code, @amount, @bank_name, @account_number, 
			@routing_number, @comments, @wire_date, @discounted_balance, GETUTCDATE(), 'SYSTEM', @payoff)

		UPDATE fundings
		SET first_position = CASE WHEN EXISTS (SELECT 1 FROM funding_distributions WHERE payoff = 0 AND funding_id = @funding_id AND recipient NOT LIKE '%Caymus%') THEN 0 ELSE 1 END
		WHERE id = @funding_id
	END
/*  Not doing now.  Should never be an update.   Once it is funded in SOLO this data should never change so once we have it in Portfolio, it stays that way.
ELSE
	BEGIN
	--	UPDATE funding_distributions
		
	END
*/

END

GO
