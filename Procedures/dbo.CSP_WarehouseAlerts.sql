SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2022-05-04 - Star Wars day
-- Description:	Runs various checks for warehouse transactions
-- =============================================
CREATE PROCEDURE [dbo].[CSP_WarehouseAlerts]

AS
BEGIN
SET NOCOUNT ON;


-- get fundings that do not have a portfolio
SELECT 'portfolio field is empty on funding' AS issue, funding_id
FROM fundings f
WHERE NULLIF(f.portfolio, '') IS NULL
ORDER BY funding_id

-- get transactions that do not have a portfolio
SELECT 'portfolio field is empty on transaction' AS issue, trans_id
FROM transactions t 
WHERE NULLIF(t.portfolio, '') IS NULL
ORDER BY t.trans_id

-- get transactions that are ACH Draft for SPV but do not have the associated warehouse transaction
SELECT 'missing warehouse transaction' AS issue, t.trans_id
FROM transactions t
LEFT JOIN transactions t2 ON t.trans_id = t2.spv_main_id
WHERE t.portfolio = 'spv' AND t.trans_type_id = 18 AND t2.trans_id IS NULL

-- get warehouse transactions that do not have a matching ach draft transaction
SELECT 'warehouse missing the ach draft' AS issue, t.trans_id
FROM transactions t
LEFT JOIN transactions t2 ON t.spv_main_id = t2.trans_id
WHERE t.portfolio = 'spv' AND t.trans_type_id = 27 AND t2.trans_id IS NULL


END

GO
