SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:     Ryan Brown
-- Create Date: 2021-09-30
-- Description: Gets Journal Entries for NetSuite Accounting for bonus commissions
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetISOBonusesForAccountingNetSuite]

AS

SET NOCOUNT ON


IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData
CREATE TABLE #tempData (id int not null identity, iso_bonus_id INT)

IF OBJECT_ID('tempdb..#tempAllData') IS NOT NULL DROP TABLE #tempAllData
CREATE TABLE #tempAllData (id int not null identity, trans_id VARCHAR (50), trans_date VARCHAR (50), subsidiary VARCHAR (100), currency VARCHAR (50), postingperiod VARCHAR (50), 
	journalItemLine_memo NVARCHAR (1000), journalItemLine_account VARCHAR (100), journalItemLine_debitAmount MONEY, journalItemLine_creditAmount MONEY, row_id INT)

INSERT INTO #tempData (iso_bonus_id)
SELECT b.iso_bonus_id
FROM iso_bonuses b
WHERE b.approved_by IS NOT NULL AND b.sent_to_acct IS NULL

IF (SELECT COUNT(*) FROM #tempData) > 0
	BEGIN
		DECLARE @trans_id NVARCHAR (50), @pay_date NVARCHAR (50), @trans_date NVARCHAR (50)
		SELECT @trans_id = CONVERT(NVARCHAR(50), MAX(b.iso_bonus_id)), @trans_date = FORMAT(MAX(b.pay_date), 'MM/dd/yyyy'), @pay_date = FORMAT(MAX(b.pay_date), 'dd-MMM')
		FROM #tempData t
		INNER JOIN iso_bonuses b ON t.iso_bonus_id = b.iso_bonus_id


		-- commissions
		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT @trans_id, @trans_date, 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', @pay_date, 
			'Monthly ISO Program', '1008 Cash - Regions Bank - 1484 - (Funding)', NULL, SUM(b.bonus_amount), 1
		FROM iso_bonuses b		
		INNER JOIN #tempData t ON b.iso_bonus_id = t.iso_bonus_id

		INSERT INTO #tempAllData(trans_id, trans_date, subsidiary, currency, postingperiod, journalItemLine_memo, journalItemLine_account, 
					journalItemLine_debitAmount, journalItemLine_creditAmount, row_id)
		SELECT @trans_id, @trans_date, 'Holding Parent Company : Caymus Funding Inc.', 'US Dollar', @pay_date, 
			'Monthly ISO Program - ' + b.iso_name, '5026 Commissions - ISO Bonus', b.bonus_amount, NULL, b.iso_bonus_id
		FROM iso_bonuses b		
		INNER JOIN #tempData t ON b.iso_bonus_id = t.iso_bonus_id

	
		UPDATE b
		SET sent_to_acct = GETUTCDATE(), change_user = 'SYSTEM', change_date = GETUTCDATE()
		FROM iso_bonuses b 
		INNER JOIN #tempData t ON b.iso_bonus_id = t.iso_bonus_id

	END


--  SELECT * FROM #tempAllData

SELECT COALESCE(t.trans_id, '') AS [tranId], COALESCE(CONVERT(VARCHAR (50), t.trans_date), '') AS trandate, COALESCE(t.subsidiary, '') AS [subsidiary], 
	COALESCE(t.currency, '') AS [currency], t.postingperiod, t.journalItemLine_memo, t.journalItemLine_account, 
	COALESCE(CONVERT(VARCHAR (50), t.journalItemLine_debitAmount), '') AS [journalItemLine_debitAmount], 
	COALESCE(CONVERT(VARCHAR (50), t.journalItemLine_creditAmount), '') AS [journalItemLine_creditAmount]
FROM #tempAllData t
--ORDER BY t.id

-- get any fundings that do not balance
SELECT t.trans_id, SUM(t.journalItemLine_debitAmount) AS debits, SUM(t.journalItemLine_creditAmount) AS credits, 
	SUM(t.journalItemLine_debitAmount) - SUM(t.journalItemLine_creditAmount) AS diff
FROM #tempAllData t
GROUP BY t.trans_id
HAVING SUM(t.journalItemLine_creditAmount) <> SUM(t.journalItemLine_debitAmount)


IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData
IF OBJECT_ID('tempdb..#tempAllData') IS NOT NULL DROP TABLE #tempAllData

GO
