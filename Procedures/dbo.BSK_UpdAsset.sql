SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Rick Pina
-- Create date: 2020-12-03
-- Description:	Updates Asset
-- =============================================
CREATE PROCEDURE [dbo].[BSK_UpdAsset]
(
	@asset_id					INT,
	@asset_types_id				INT,
	@asset_price				NVARCHAR (100),
	@asset_name					NVARCHAR (100),
	@asset_description			NVARCHAR (100),
	@asset_owner				NVARCHAR (100),
	@asset_located				BIT,
	@change_user				NVARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON;

UPDATE assets
SET asset_types_id = @asset_types_id, asset_price = @asset_price, asset_name = @asset_name,
asset_description = @asset_description, asset_owner = @asset_owner, asset_located = @asset_located,
 change_date = GETUTCDATE(), change_user = @change_user
WHERE asset_id = @asset_id

END

GO
