SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-21
-- Description:	Gets a list of transactions to send to bossk
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetTransactionsForBossk]

AS
BEGIN
SET NOCOUNT ON;

DECLARE @now DATETIME; SET @now = GETUTCDATE()

UPDATE t
SET t.sent_to_collect = @now
FROM fundings f
INNER JOIN affiliates a ON f.affiliate_id = a.affiliate_id
INNER JOIN transactions t ON f.id = t.funding_id
WHERE ((COALESCE(f.collection_type, '') <> '' AND f.payoff_amount > 0)
	OR (a.sent_to_collect IS NOT NULL AND a.closed_in_collections IS NULL))
	AND COALESCE(t.sent_to_collect, 0) = 0
	AND t.trans_date >= '2020-10-01'
	AND t.trans_type_id IN (1, 2, 3, 4, 5, 14, 19, 21) 


SELECT t.trans_id AS transaction_id, tt.trans_type AS transaction_type, t.trans_date AS transaction_date, t.trans_amount AS transaction_amount, t.payment_id, 
	t.funding_id AS contract_id, f.merchant_id, 
		CASE WHEN t.trans_type_id IN (3, 18, 21, 14) 
			THEN 
				CASE WHEN b.batch_id IS NULL THEN COALESCE(fs.settle_response_code, fsb.settle_response_code, fs2.settle_response_code, ft.status, nt.reason_code, ntb.reason_code)
					ELSE CASE WHEN NULLIF(t.comments, '') IS NULL 
						THEN COALESCE(fs.settle_response_code, fsb.settle_response_code, fs2.settle_response_code, ft.status, nt.reason_code, ntb.reason_code)
						ELSE CASE REVERSE(LEFT(REVERSE(REPLACE(t.comments, ' : withdrawal', '')), CHARINDEX(' ', REVERSE(REPLACE(t.comments, ' : withdrawal', '')))-1)) 
						WHEN 'deposit' THEN 'S01' 
						ELSE CASE CHARINDEX(' : reject', t.comments) 
							WHEN 0 THEN REVERSE(LEFT(REVERSE(REPLACE(t.comments, ' : withdrawal', '')), CHARINDEX(' ', REVERSE(REPLACE(t.comments, ' : withdrawal', '')))-1)) 
							ELSE REVERSE(LEFT(REVERSE(REPLACE(t.comments, ' : reject', '')), CHARINDEX(' ', REVERSE(REPLACE(t.comments, ' : reject', '')))-1))
							END
						END
					END
				END
			ELSE ''
		END AS settle_code
FROM fundings f
INNER JOIN affiliates a ON f.affiliate_id = a.affiliate_id
INNER JOIN transactions t ON f.id = t.funding_id
INNER JOIN transaction_types tt ON t.trans_type_id = tt.trans_type_id
LEFT JOIN batches b ON t.batch_id = b.batch_id
LEFT JOIN forte_settlements fs ON t.transaction_id = fs.transaction_id AND fs.settle_response_code LIKE 'S0%'
LEFT JOIN forte_settlements fsb ON t.transaction_id = fsb.transaction_id AND fsb.settle_response_code NOT LIKE 'S0%'
LEFT JOIN forte_trans ft ON t.transaction_id = ft.transaction_id
LEFT JOIN forte_settlements fs2 ON t.transaction_id IS NULL AND t.record_id = fs2.forte_settle_id
LEFT JOIN nacha_trans nt WITH (NOLOCK) ON t.transaction_id = CONVERT(VARCHAR (50), nt.payment_id) AND nt.reason_code LIKE 'S0%' AND t.trans_type_id NOT IN (14, 21)
LEFT JOIN nacha_trans ntb WITH (NOLOCK) ON t.transaction_id = CONVERT(VARCHAR (50), ntb.payment_id) AND ntb.reason_code NOT LIKE 'S0%' AND t.trans_type_id IN (14, 21)
WHERE ((COALESCE(f.collection_type, '') <> '' AND f.payoff_amount > 0)
	OR (a.sent_to_collect IS NOT NULL AND a.closed_in_collections IS NULL))
	AND t.sent_to_collect = @now
	AND t.trans_date >= '2020-10-01'
	AND t.trans_type_id IN (1, 2, 3, 4, 5, 14, 19, 21)  
ORDER BY t.trans_id

END

GO
