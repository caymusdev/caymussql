SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-02-11
-- Description:	Saves the batch_id on to a NACHA payment so that we can come back (after getting ACK from 5/3) and distribute the payment
-- =============================================
CREATE PROCEDURE [dbo].[CSP_UpdatePaymentBatchID] 
(
	@payment_id				INT,
	@batch_id				INT,
	@portfolio				VARCHAR (50) = 'caymus'
)
AS
BEGIN
SET NOCOUNT ON;

-- update the payment
UPDATE payments 
SET transaction_id = @payment_id, processed_date = GETUTCDATE(), batch_id = @batch_id, change_date = GETUTCDATE(), change_user = 'API', --, processor = @processor
	portfolio = @portfolio
WHERE payment_id = @payment_id 

END

GO
