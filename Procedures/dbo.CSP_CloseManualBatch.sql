SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-01-19
-- Description:	Closes a batch - Used when fixing transactions manually
-- =============================================
CREATE PROCEDURE [dbo].[CSP_CloseManualBatch]
(
	@batch_id		INT,
	@user_id		NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON;

-- make sure to get the correct "today's" date since this runs in UTC
DECLARE @today DATE; SELECT @today = CONVERT(DATE, DATEADD(HOUR, -5, GETUTCDATE()))


-- close any manual batches that are not closed that have a batch date before today
UPDATE batches
SET batch_status = 'Closed', settle_date = @today,  --batch_date
	change_user = @user_id, change_date = GETUTCDATE()
WHERE batch_status = 'Processed' AND manual_flag = 1 AND batch_id = @batch_id

END
GO
