SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

 

-- =============================================
-- Author:      Rick Pina
-- Create Date: 2020-09-09
-- Description: Updates User Password
-- =============================================
CREATE PROCEDURE [dbo].[BSK_UpdUserPassword]
(
    @email                    NVARCHAR (100),
    @hash_password            NVARCHAR (100),
	@user		      NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON

 

-- validate email and password
    
    UPDATE users
    SET hash_password = @hash_password, change_date = GETUTCDATE(), change_user = @user
    WHERE email = @email

 

END

GO
