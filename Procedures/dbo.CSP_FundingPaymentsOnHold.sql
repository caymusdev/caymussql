SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-02-18
-- Description:	This will run periodically throughout the day.  It will make sure that 
-- =============================================
CREATE PROCEDURE [dbo].[CSP_FundingPaymentsOnHold]
(
	@worker_id			INT,
	@approver			NVARCHAR (100),
	@reject_reason		NVARCHAR (500)
)
AS
BEGIN	
SET NOCOUNT ON;



SELECT f.legal_name, f.contract_number, w.payment_date, 
	CASE WHEN w.revised_original = 1 THEN 'Revise Original' ELSE 
		CASE WHEN w.funding_payment_id IS NULL THEN 'New Payment' WHEN w.active = 0 THEN 'Delete Payment' ELSE 'Change Payment' END END AS change_type, 
	w.userid
FROM funding_payment_dates_work w
INNER JOIN fundings f ON w.funding_id = f.id
WHERE w.id = @worker_id

UPDATE funding_payment_dates_work 
SET approved = 0, approver = @approver, rejected_date = GETUTCDATE(), change_user = @approver, change_date = GETUTCDATE(), ready_for_approval = 0, 
	active = 0, reject_reason = @reject_reason
WHERE id = @worker_id


END
GO
