SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-04-05
-- Description:	Gets fudings for a given day to be sent to Azure for import into Accounting system
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetFundingsForAccounting]

AS
BEGIN
SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData
CREATE TABLE #tempData (id int not null identity, funding_id INT)

IF OBJECT_ID('tempdb..#tempAllData') IS NOT NULL DROP TABLE #tempAllData
CREATE TABLE #tempAllData (id int not null identity, transaction_type VARCHAR (50) NULL, journal VARCHAR (50) NULL, bank_account VARCHAR (50) NULL, orig_date DATE NULL, reference VARCHAR (50) NULL, 
	payee_desc VARCHAR (200) NULL, gl_acct VARCHAR (50) NULL, dist_desc VARCHAR (300) NULL, total_amount MONEY NULL, debit MONEY NULL, credit MONEY NULL, funding_id INT, row_id INT)

INSERT INTO #tempData (funding_id)
SELECT f.id
FROM fundings f
WHERE COALESCE(f.funded_date, '2000-01-01') >= '2018-07-01'	AND f.sent_to_acct IS NULL
ORDER BY f.funded_date, f.id

DECLARE @current_id INT, @max_id INT
SELECT @current_id = MIN(id), @max_id = MAX(id) FROM #tempData

WHILE @current_id <= @max_id
	BEGIN
		DECLARE @new_funding_id INT
		SELECT @new_funding_id = funding_id FROM #tempData WHERE id = @current_id
	
		-- rtr
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT 'JE', 'Fundings', '', f.funded_date, f.contract_number, f.legal_name, '1101', f.legal_name + ' - Right to Receive', NULL, f.portfolio_value, NULL, f.id, 1
		FROM fundings f
		WHERE f.id = @new_funding_id

		-- deferred margin
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT '', 'Fundings', '', NULL, f.contract_number, f.legal_name, '2011', f.legal_name + ' - Deferred Margin', NULL, NULL, f.unearned_income, f.id, 2
		FROM fundings f
		WHERE f.id = @new_funding_id
		
		-- bad debt calc
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT '', 'Fundings', '', NULL, f.contract_number, f.legal_name, '5000', f.legal_name + ' - Bad Debt Expense', NULL, ROUND(0.085 * f.purchase_price, 2), NULL, f.id, 3
		FROM fundings f
		WHERE f.id = @new_funding_id

		-- bad debt calc
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT '', 'Fundings', '', NULL, f.contract_number, f.legal_name, '1200', f.legal_name + ' - Bad Debt Reserve', NULL, NULL, ROUND(0.085 * f.purchase_price, 2), f.id, 4
		FROM fundings f
		WHERE f.id = @new_funding_id

		-- net to merchant
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT '', 'Fundings', '', NULL, f.contract_number, f.legal_name, '1008', f.legal_name + ' - To Merchant', NULL, NULL, f.net_to_merchant, f.id, 5
		FROM fundings f
		WHERE f.id = @new_funding_id

		-- competitor payoffs
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT '', 'Fundings', '', NULL, f.contract_number, f.legal_name, '1008', f.legal_name + ' - Payoff To ' + d.recipient, NULL, NULL, 
			CASE WHEN COALESCE(d.discounted_balance, 0) <> 0 THEN d.discounted_balance ELSE d.amount END, f.id, 6
		FROM fundings f
		INNER JOIN funding_distributions d ON f.id = d.funding_id
		WHERE f.id = @new_funding_id
		ORDER BY CASE WHEN d.recipient LIKE '%Caymus%' THEN 1 ELSE 0 END  -- put Caymus at the bottom of the list

		-- commissions
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT '', 'Fundings', '', NULL, f.contract_number, f.legal_name, '1008', f.legal_name + ' - Commission To ' + f.commission_iso, NULL, NULL, f.commission_amount, f.id, 7
		FROM fundings f		
		WHERE f.id = @new_funding_id AND COALESCE(f.commission_iso, '') <> ''

		-- commissions
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT '', 'Fundings', '', NULL, f.contract_number, f.legal_name, '4010', f.legal_name + ' - Commission To ' + f.commission_iso, NULL, f.commission_amount, NULL, f.id, 8
		FROM fundings f		
		WHERE f.id = @new_funding_id AND COALESCE(f.commission_iso, '') <> ''

		-- referral commissions
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT '', 'Fundings', '', NULL, f.contract_number, f.legal_name, '1008', f.legal_name + ' - Commission To ' + f.referral_iso, NULL, NULL, f.referral_commission, f.id, 9
		FROM fundings f		
		WHERE f.id = @new_funding_id AND COALESCE(f.referral_iso, '') <> ''

		-- referral commissions
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT '', 'Fundings', '', NULL, f.contract_number, f.legal_name, '4010', f.legal_name + ' - Commission To ' + f.referral_iso, NULL, f.referral_commission, NULL, f.id, 10
		FROM fundings f		
		WHERE f.id = @new_funding_id AND COALESCE(f.referral_iso, '') <> ''

		-- orig fee to caymus
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT '', 'Fundings', '', NULL, f.contract_number, f.legal_name, '1008', f.legal_name + ' - To Caymus Fnding - Origination Fee', NULL, NULL, f.orig_fee, f.id, 11
		FROM fundings f		
		WHERE f.id = @new_funding_id AND COALESCE(f.orig_fee, 0) <> 0
		/*
		-- orig fee due to caymus
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id)
		SELECT '', 'Fundings', '', NULL, f.contract_number, f.legal_name, '1102', f.legal_name + ' - Origination Fee due to Caymus', NULL, f.orig_fee, NULL, f.id
		FROM fundings f		
		WHERE f.id = @new_funding_id AND COALESCE(f.orig_fee, 0) <> 0
		*/
		-- orig fee income
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT '', 'Fundings', '', NULL, f.contract_number, f.legal_name, '4004', f.legal_name + ' - Origination Fee Income', NULL, NULL, f.orig_fee, f.id, 12
		FROM fundings f		
		WHERE f.id = @new_funding_id AND COALESCE(f.orig_fee, 0) <> 0

		-- orig fee due to caymus
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT '', 'Fundings', '', NULL, f.contract_number, f.legal_name, '1007', f.legal_name + ' - Origination Fee Received', NULL, f.orig_fee, NULL, f.id, 13
		FROM fundings f		
		WHERE f.id = @new_funding_id AND COALESCE(f.orig_fee, 0) <> 0

		-- renewal payoff
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT '', 'Fundings', '', NULL, f.contract_number, f.legal_name, '1101', f.legal_name + ' - Payoff from Renewal Contract' + COALESCE(' - ' + f2.contract_number, ''), 
			NULL, NULL, 
			CASE WHEN COALESCE(d.discounted_balance, 0) <> 0 THEN d.discounted_balance ELSE d.amount END, f.id, 14
		FROM fundings f
		INNER JOIN funding_distributions d ON f.id = d.funding_id
		LEFT JOIN fundings f2 ON f.previous_funding_id = f2.id
		WHERE f.id = @new_funding_id AND d.recipient LIKE '%Caymus%'	
		
		
		-- renewal margin on original
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT '', 'Fundings', '', NULL, f.contract_number, f.legal_name, '2011', f.legal_name + ' - Payoff from Renewal Contract - ' + f2.contract_number, NULL, td.trans_amount, NULL, f.id, 17
		FROM fundings f
		INNER JOIN funding_distributions d ON f.id = d.funding_id
		INNER JOIN fundings f2 ON f.previous_funding_id = f2.id
		INNER JOIN transactions t ON t.funding_id = f2.id AND t.trans_type_id = 5 AND t.comments LIKE '%renewal%'
		INNER JOIN transaction_details td ON t.trans_id = td.trans_id AND td.trans_type_id = 6
		WHERE f.id = @new_funding_id AND d.recipient LIKE '%Caymus%'
		
		-- revenue margin
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT '', 'Fundings', '', NULL, f.contract_number, f.legal_name, '4003', f.legal_name + ' - Revenue on Original Contract - ' + f2.contract_number, NULL, NULL, td.trans_amount, f.id, 18
		FROM fundings f
		INNER JOIN funding_distributions d ON f.id = d.funding_id
		INNER JOIN fundings f2 ON f.previous_funding_id = f2.id
		INNER JOIN transactions t ON t.funding_id = f2.id AND t.trans_type_id = 5 AND t.comments LIKE '%renewal%'
		INNER JOIN transaction_details td ON t.trans_id = td.trans_id AND td.trans_type_id = 6
		WHERE f.id = @new_funding_id AND d.recipient LIKE '%Caymus%'

		-- in case renewal paid off fees  - one as credit
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT '', 'Fundings', '', NULL, f.contract_number, f.legal_name, '4001', f.legal_name + ' - Fees Paid from Renewal - ' + f2.contract_number, NULL, NULL, td.trans_amount, f.id, 19
		FROM fundings f
		INNER JOIN funding_distributions d ON f.id = d.funding_id
		INNER JOIN fundings f2 ON f.previous_funding_id = f2.id
		INNER JOIN transactions t ON t.funding_id = f2.id AND t.trans_type_id = 5 AND t.comments LIKE '%renewal%'
		INNER JOIN transaction_details td ON t.trans_id = td.trans_id AND td.trans_type_id = 1
		WHERE f.id = @new_funding_id AND d.recipient LIKE '%Caymus%'

		-- in case renewal paid off fees -- one as debit
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT '', 'Fundings', '', NULL, f.contract_number, f.legal_name, '4001', f.legal_name + ' - Fees Paid from Renewal - ' + f2.contract_number, NULL, td.trans_amount, NULL, f.id, 19
		FROM fundings f
		INNER JOIN funding_distributions d ON f.id = d.funding_id
		INNER JOIN fundings f2 ON f.previous_funding_id = f2.id
		INNER JOIN transactions t ON t.funding_id = f2.id AND t.trans_type_id = 5 AND t.comments LIKE '%renewal%'
		INNER JOIN transaction_details td ON t.trans_id = td.trans_id AND td.trans_type_id = 1
		WHERE f.id = @new_funding_id AND d.recipient LIKE '%Caymus%'
		
		-- prior contract payoff
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT '', 'Fundings', '', NULL, f.contract_number, f.legal_name, '1007', f.legal_name + ' - Prior Contract Payoff to Caymus Funding', NULL, 
			CASE WHEN COALESCE(d.discounted_balance, 0) <> 0 THEN d.discounted_balance ELSE d.amount END, NULL, f.id, 20
		FROM fundings f
		INNER JOIN funding_distributions d ON f.id = d.funding_id
		WHERE f.id = @new_funding_id AND d.recipient LIKE '%Caymus%'


		-- RKB 2020-01-17 - Dartanya says discounts need to be their own journal entry
		-- renewal discount
		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT 'JE', 'Deposits', '', NULL, f2.contract_number, f2.legal_name, '1101', f2.legal_name + ' - Discount given as part of Renewal Contract - ' + f.contract_number, 
			NULL, NULL, f.discount, f.id, 21
		FROM fundings f
		INNER JOIN fundings f2 ON f.previous_funding_id = f2.id
		WHERE f.id = @new_funding_id AND COALESCE(f.discount, 0) <> 0

		INSERT INTO #tempAllData(transaction_type, journal, bank_account, orig_date, reference, payee_desc, gl_acct, dist_desc, total_amount, debit, credit, funding_id, row_id)
		SELECT '', 'Deposits', '', NULL, f2.contract_number, f2.legal_name, '2011', f2.legal_name + ' - Discount given as part of Renewal Contract - ' + f.contract_number, 
			NULL, f.discount, NULL, f.id, 22
		FROM fundings f
		INNER JOIN fundings f2 ON f.previous_funding_id = f2.id
		WHERE f.id = @new_funding_id AND COALESCE(f.discount, 0) <> 0
	
		SET @current_id = @current_id + 1
	END

UPDATE f
SET sent_to_acct = GETUTCDATE(), change_user = 'SYSTEM', change_date = GETUTCDATE()
FROM fundings f 
INNER JOIN #tempAllData t ON f.id = t.funding_id


--  SELECT * FROM #tempAllData

SELECT COALESCE(t.transaction_type, '') AS [Transaction Type], COALESCE(t.journal, '') AS Journal, COALESCE(t.bank_account, '') AS [Bank Account], 
	COALESCE(CONVERT(VARCHAR (50), t.orig_date), '') AS [Date], t.reference AS Reference, t.payee_desc AS [Payee/Description], t.gl_acct AS [GL Account], 
	t.dist_desc AS [Distribution Description], COALESCE(CONVERT(VARCHAR (50), t.total_amount), '') AS [Total Amount], COALESCE(CONVERT(VARCHAR (50), t.debit), '') AS Debit, 
	COALESCE(CONVERT(VARCHAR (50), t.credit), '') AS Credit
FROM #tempAllData t
ORDER BY t.id

-- get any fundings that do not balance
SELECT t.funding_id, SUM(t.debit) AS debits, SUM(t.credit) AS credits, SUM(t.debit) - SUM(t.credit) AS diff
FROM #tempAllData t
GROUP BY t.funding_id
HAVING SUM(t.credit) <> SUM(t.debit)


IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData
IF OBJECT_ID('tempdb..#tempAllData') IS NOT NULL DROP TABLE #tempAllData


END
GO
