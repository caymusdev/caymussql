SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-03-05
-- Description:	Creates a transaction for failures from forte that never get accepted (U2 codes usually)
-- =============================================
CREATE PROCEDURE [dbo].[CSP_ProcessFailedTransaction]
(
	@payment_id				INT, 
	@transaction_id			NVARCHAR (100), 
	@comments				NVARCHAR (500),
	@batch_id				INT,
	@response_code			NVARCHAR (50)
)

AS
BEGIN
SET NOCOUNT ON;
DECLARE @today DATE; SELECT @today = DATEADD(HH, -5, GETUTCDATE())


DECLARE @funding_id INT, @settle_date DATETIME, @trans_date DATE, @trans_amount MONEY, @record_id INT,  @settle_type NVARCHAR (50)

SELECT @funding_id = p.funding_id, @trans_amount = p.amount
FROM payments p
WHERE p.payment_id = @payment_id


DECLARE @previous_id INT; SET @previous_id = NULL
DECLARE @trans_type_id INT; SET @trans_type_id = NULL
SELECT @trans_date = @today, @record_id = @payment_id, @settle_date = @today, @settle_type = 'reject'


EXEC dbo.CSP_DistributeTransaction_v2 @funding_id, @trans_amount, @transaction_id, @trans_date, @comments, @batch_id, @record_id, 'forte_transaction/payment', 
	NULL, @settle_date, @settle_type, @response_code, NULL

END
GO
