SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Rick Pina
-- Create date: 2021-01-13
-- Description:	Deletes Phone Number
-- =============================================
CREATE PROCEDURE [dbo].[BSK_DelPhoneNumber]
(

	@change_user							NVARCHAR(100),
	@phone_number_id					INT

)
AS
BEGIN
SET NOCOUNT ON;

		UPDATE phone_numbers SET change_date = GETUTCDATE(), change_user = @change_user WHERE phone_number_id = @phone_number_id

			
		DELETE FROM phone_numbers WHERE phone_number_id = @phone_number_id

END

GO
