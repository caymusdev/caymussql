SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-02-01
-- Description:	Checks to see if the merchange bank account is in use
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetBankAccountsForFunding]
(
	@funding_id			INT
)
AS
BEGIN
SET NOCOUNT ON;

SELECT ba.bank_name, ba.routing_number, ba.account_number, ba.account_status, ba.merchant_bank_account_id, 
	COALESCE(ba.nickname, COALESCE(ba.bank_name, '') + ': ' + COALESCE(ba.routing_number, '') + ' / ' + COALESCE(ba.account_number, '')) 
		+ CASE WHEN ba.account_number = f.receivables_account_nbr AND ba.routing_number = f.receivables_aba THEN ' (Main)' ELSE '' END AS nickname
FROM merchant_bank_accounts ba
INNER JOIN fundings f ON ba.merchant_id = f.merchant_id
WHERE f.id = @funding_id
ORDER BY ba.nickname

END
GO
