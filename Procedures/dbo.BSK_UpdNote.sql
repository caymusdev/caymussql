SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Rick Pina
-- Create date: 2021-1-4
-- Description:	Updates A Note :)
-- =============================================
CREATE PROCEDURE [dbo].[BSK_UpdNote]
(
	@note_id					INT,
	@important_notes				BIT,
	@notes_text				NVARCHAR (MAX),
	@change_user					NVARCHAR (100)
)
AS
BEGIN
SET NOCOUNT ON;

UPDATE notes
SET important_notes = @important_notes, notes_text = @notes_text,
 change_date = GETUTCDATE(), change_user = @change_user
WHERE notes_id = @note_id

END

GO
