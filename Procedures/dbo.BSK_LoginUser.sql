SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2020-08-26
-- Description: Logs in a user
-- =============================================
CREATE PROCEDURE dbo.BSK_LoginUser
(
	@email			NVARCHAR (100),
	@hash_password	NVARCHAR (200)
)
AS
BEGIN
SET NOCOUNT ON

-- validate email and password
SELECT email 
FROM users u
WHERE LOWER(u.email) = LOWER(@email) AND u.hash_password = @hash_password


END
GO
