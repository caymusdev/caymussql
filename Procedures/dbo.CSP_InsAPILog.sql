SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-04-26
-- Description:	Inserts a record in the api_log table
-- =============================================
CREATE PROCEDURE [dbo].[CSP_InsAPILog]
(
	@api_call		NVARCHAR (100),
	@api_raw_call	NVARCHAR (MAX),
	@api_response	NVARCHAR (MAX)	
)
AS
BEGIN
SET NOCOUNT ON;

INSERT INTO api_logs(api_call, api_raw_call, api_response, create_date)
VALUES (@api_call, @api_raw_call, @api_response, GETUTCDATE())

END
GO
