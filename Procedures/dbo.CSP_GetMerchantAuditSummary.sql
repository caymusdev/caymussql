SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ryan Brown
-- Create date: 2019-06-25
-- Description:	Gets summary information for a given id and type (affiliate, merchant, contract
-- =============================================
CREATE PROCEDURE [dbo].[CSP_GetMerchantAuditSummary]
(	
	@id					BIGINT,
	@summary_type		VARCHAR (50)
)
AS
BEGIN	
SET NOCOUNT ON;

IF LOWER(@summary_type) = 'affiliate'
	BEGIN
		SELECT f.affiliate_id, COUNT(*) AS total_contracts, 
			SUM(CASE WHEN f.contract_status = 1 OR (f.contract_status = 4 AND f.contract_substatus_id = 1) THEN 1 ELSE 0 END) AS open_contracts,
			SUM(CASE WHEN COALESCE(f.collection_type, '') != '' THEN 1 ELSE 0 END) AS collection_contracts,
			SUM(COALESCE(f.portfolio_value, 0)) AS original_rtr,
			SUM(COALESCE(f.payoff_amount, 0)) AS payoff_amount,
			SUM(COALESCE(f.paid_off_amount, 0)) AS amount_paid, 
			CONVERT(DATE, MAX(f.last_payment_date)) AS last_payment_date,
			(SELECT COUNT(*) FROM merchants m WHERE m.affiliate_id = f.affiliate_id) AS total_merchants,
			SUM(COALESCE(f.fees_out, 0)) AS fees_out, 
			COALESCE((SELECT SUM(trans_amount) FROM transactions t WHERE t.funding_id IN (SELECT id FROM fundings f2 WHERE f2.affiliate_id = f.affiliate_id) 
				AND t.trans_type_id = 9 AND t.redistributed = 0), 0) AS total_fees
		FROM fundings f		
		WHERE f.affiliate_id = @id
		GROUP BY f.affiliate_id
	END
ELSE IF LOWER(@summary_type) = 'merchant'
	BEGIN
		SELECT f.merchant_id, COUNT(*) AS total_contracts, 
			SUM(CASE WHEN f.contract_status = 1 OR (f.contract_status = 4 AND f.contract_substatus_id = 1) THEN 1 ELSE 0 END) AS open_contracts,
			SUM(CASE WHEN COALESCE(f.collection_type, '') != '' THEN 1 ELSE 0 END) AS collection_contracts,
			SUM(COALESCE(f.portfolio_value, 0)) AS original_rtr,
			SUM(COALESCE(f.payoff_amount, 0)) AS payoff_amount,
			SUM(COALESCE(f.paid_off_amount, 0)) AS amount_paid, 
			CONVERT(DATE, MAX(f.last_payment_date)) AS last_payment_date,
			SUM(COALESCE(f.fees_out, 0)) AS fees_out, 
			COALESCE((SELECT SUM(trans_amount) FROM transactions t WHERE t.funding_id IN (SELECT id FROM fundings f2 WHERE f2.merchant_id = f.merchant_id) 
				AND t.trans_type_id = 9 AND t.redistributed = 0), 0) AS total_fees
		FROM fundings f
		WHERE f.merchant_id = @id
		GROUP BY f.merchant_id
	END
ELSE IF LOWER(@summary_type) = 'funding'
	BEGIN
		SELECT cs.contract_status + COALESCE(' - ' + s.substatus_text, '') AS contract_status,
			f.portfolio_value AS original_rtr, f.payoff_amount, f.paid_off_amount AS amount_paid, CONVERT(DATE, f.last_payment_date) AS last_payment_date, f.percent_paid,
			f.funded_date, COALESCE(f.fees_out, 0) AS fees_out, 
			COALESCE((SELECT SUM(trans_amount) FROM transactions t WHERE t.funding_id = f.id AND t.trans_type_id = 9 AND t.redistributed = 0), 0) AS total_fees
		FROM fundings f
		INNER JOIN contract_statuses cs ON f.contract_status = cs.contract_status_id
		LEFT JOIN contract_substatuses s ON f.contract_substatus_id = s.substatus_id
		WHERE f.id = @id
	END

END
GO
