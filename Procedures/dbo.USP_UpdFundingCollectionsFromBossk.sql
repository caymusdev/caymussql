SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-09-30
-- Description:	Found that there were various places (move case, assign collector) in Bossk that changed the bossk tables but did not update the portfolio
-- funding_collections table which was causing issues with commissiomns not being accurate since commissions uses funding_collections table.
-- found that the easiest approach was to have an SP fix and create records, rather than update all places in bossk.   
-- This runs once at night and can be safely run at anytime
-- =============================================
CREATE PROCEDURE [dbo].[USP_UpdFundingCollectionsFromBossk]
AS
BEGIN
SET NOCOUNT ON;
	
IF OBJECT_ID('tempdb..#tempData') IS NOT NULL DROP TABLE #tempData
CREATE TABLE #tempData (id INT IDENTITY(1, 1), funding_collection_id INT, funding_id BIGINT, new_collection_type VARCHAR (50), new_assigned_collector NVARCHAR (100),
	new_start_date DATE)

-- this stores in a temp table funding collections that have null for end date and do not match what collections tables say should be the current status
INSERT INTO #tempData(funding_collection_id, funding_id, new_collection_type, new_assigned_collector, new_start_date)
SELECT fc.funding_collection_id, fc.funding_id, x.collection_type, x.assigned_collector, x.start_date
FROM funding_collections fc WITH (NOLOCK)
LEFT JOIN
(
-- rkb - 2022-12-07 - don't like using DISTINCT but found issue with multiple records in case_departments with null end date so for now, DISTINCT is necessary
	SELECT DISTINCT f.id AS funding_id, CASE WHEN d.department = 'CustomerService' THEN 'Customer_Service'
					  WHEN d.department LIKE '%Collections%' THEN 'Collections'
					  ELSE 'Collections'
					END AS collection_type, 
			COALESCE(cc.start_date, cd.start_date) AS start_date, u.email AS assigned_collector
	FROM collector_cases cc WITH (NOLOCK)
	INNER JOIN case_departments cd WITH (NOLOCK) ON cc.case_id = cd.case_id
	INNER JOIN cases c WITH (NOLOCK) ON cc.case_id = c.case_id
	INNER JOIN fundings f WITH (NOLOCK) ON c.affiliate_id = f.affiliate_id
	INNER JOIN departments d WITH (NOLOCK) ON cd.department_id = d.department_id
	INNER JOIN users u WITH (NOLOCK) ON cc.collector_user_id = u.user_id
	WHERE cc.end_date IS NULL AND cd.end_date IS NULL AND f.contract_status IN (1, 4)
) x ON fc.funding_id = x.funding_id
WHERE fc.end_date IS NULL 
	AND (fc.collection_type != x.collection_type OR fc.assigned_collector != x.assigned_collector)


-- set the end date of the current final record since we found that things have changed in collections and we need a new record
UPDATE fc
SET end_date = DATEADD(DD, -1, x.new_start_date), change_date = GETUTCDATE(), change_user = 'system 2'
FROM funding_collections fc
INNER JOIN #tempData x ON fc.funding_collection_id = x.funding_collection_id

-- insert in the new record
INSERT INTO funding_collections (funding_id, collection_type, start_date, end_date, assigned_collector, change_user)
SELECT x.funding_id, x.new_collection_type, x.new_start_date, NULL, x.new_assigned_collector, 'FromBossk'
FROM #tempData x

-- update the funding if necessary
UPDATE f 
SET collection_type = x.new_collection_type, change_user = 'system 2', change_date = GETUTCDATE()
FROM fundings f
INNER JOIN #tempData x ON f.id = x.funding_id
WHERE COALESCE(f.collection_type, '') <> x.new_collection_type

END
GO
