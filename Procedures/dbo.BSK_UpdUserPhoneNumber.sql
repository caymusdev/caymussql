SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Rick Pina
-- Create date: 2021-02-22
-- Description:	Updates User Phone Number
-- =============================================
CREATE PROCEDURE [dbo].[BSK_UpdUserPhoneNumber]
(
	@user_id					INT,
	@user_phone_id				INT,
	@phone_number				VARCHAR(50),
	@phone_type					VARCHAR(50),
	@is_primary					BIT,
	@change_user				NVARCHAR(100)

)
AS
BEGIN
SET NOCOUNT ON;


UPDATE user_phones
SET is_primary = 0, change_date = GETUTCDATE(), change_user = @change_user
WHERE user_id = @user_id AND @is_primary = 1 AND user_phone_id != @user_phone_id

UPDATE user_phones
SET phone_number = @phone_number, phone_type = @phone_type, is_primary = @is_primary, change_date = GETUTCDATE(), change_user = @change_user
WHERE user_phone_id = @user_phone_id

END


GO
