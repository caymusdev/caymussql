SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-05-23
-- Description:	This is used to migrate the transactions (CashApplication) from Access into the new db
-- This takes the values from CashApplication and stores out into transactions or transaction details or both, depending
-- =============================================
CREATE PROCEDURE [dbo].[CSP_MigrateCashApplication] 
(
	@CashAppID				INT,
	@caDate					DATETIME,
	@FundingId				NVARCHAR (50),
	@caValue				MONEY,
	@Batchid				INT,
	@Metricid				INT,
	@Remarks				NVARCHAR (255),
	@trans_id				INT = NULL
)
AS
BEGIN
SET NOCOUNT ON;

DECLARE @new_trans_id INT; SET @new_trans_id = -1;

DECLARE @new_funding_id INT;
SELECT @new_funding_id =  id
FROM fundings f 
WHERE f.funding_id = @FundingId

-- if no trans_id then we are creating the main record
IF @trans_id IS NULL
	BEGIN
		INSERT INTO transactions(trans_date, funding_id, trans_amount, comments, batch_id, trans_type_id, record_id, redistributed)
		VALUES (@caDate, @new_funding_id, @caValue, COALESCE(@Remarks, '') + ' - Migrate from CashApplication', @Batchid, @Metricid, @CashAppID, 0)

		SELECT @new_trans_id = SCOPE_IDENTITY()
	END
ELSE
	BEGIN
		INSERT INTO transaction_details (trans_id, trans_type_id, trans_amount, comments)
		VALUES (@trans_id, @Metricid, @caValue, COALESCE(@Remarks, '') + ' - Migrate from CashApplication')
	END


SELECT @new_trans_id AS NewTransID
END
GO
