CREATE TYPE [dbo].[IDTableType]
AS TABLE (
		[id]            [int] IDENTITY(1, 1) NOT NULL,
		[record_Id]     [int] NOT NULL,
	PRIMARY KEY CLUSTERED 
(
	[record_Id]
)
WITH (IGNORE_DUP_KEY = OFF)
)
GO
