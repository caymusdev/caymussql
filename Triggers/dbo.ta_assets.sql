SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[ta_assets] ON [dbo].[assets] FOR INSERT, UPDATE, DELETE
AS 
DECLARE @FIELD INT, @maxfield INT, @fieldname VARCHAR(128), @PKCols VARCHAR(1000), @SQL NVARCHAR(2000), @TYPE CHAR(1)

--*******************************************
-- Table specific information
DECLARE @TableName VARCHAR(128)
-- Get primary key columns for full outer join
SELECT @PKCols = ' i.asset_id = d.asset_id '
SELECT @TableName = 'assets'
-- End table specific information
--*******************************************

SELECT * INTO #ins FROM inserted
SELECT * INTO #del FROM deleted

IF EXISTS (SELECT * FROM inserted)
	IF EXISTS (SELECT * FROM deleted)
		SELECT @TYPE = 'U'
			ELSE
		SELECT @TYPE = 'I'
ELSE
	SELECT @TYPE = 'D'


IF @TYPE = 'I'
	BEGIN  -- insert		
		INSERT INTO assets_audit(asset_id, change_user, change_category, field, previous_value, changed_to_value)
		SELECT i.asset_id, i.change_user, 'I', NULL, NULL, NULL
		FROM #ins i
	END
ELSE IF @TYPE = 'D'
	BEGIN	-- delete		
		INSERT INTO assets_audit(asset_id, change_user, change_category, field, previous_value, changed_to_value)
		SELECT d.asset_id, d.change_user, 'D', NULL, NULL, NULL
		FROM #del d
	END
ELSE -- @TYPE = 'U'
	BEGIN  -- need to do dynamic SQL for updates only
		SELECT @FIELD = 0, @maxfield = MAX(ORDINAL_POSITION) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @TableName
		WHILE @FIELD < @maxfield
			BEGIN
				SELECT @FIELD = MIN(ORDINAL_POSITION) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @TableName AND ORDINAL_POSITION > @FIELD				
				SELECT @fieldname = COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @TableName AND ORDINAL_POSITION = @FIELD
				IF @fieldname <> 'change_date' AND @fieldName <> 'change_user' AND @fieldname <> 'create_date'
					BEGIN
						IF dbo.fn_IsBitSetInBitmask(COLUMNS_UPDATED(), COLUMNPROPERTY(OBJECT_ID(@TableName), @fieldName, 'ColumnID')) <> 0
							BEGIN																																						
								IF OBJECT_ID('tempdb..#tempVals') IS NOT NULL DROP TABLE #tempVals
								CREATE TABLE #tempVals (id INT, previous_text NVARCHAR (MAX), current_text NVARCHAR (MAX))
								
								DECLARE @iField NVARCHAR (50) 
								SELECT @iField = LEFT(@PKCols, CHARINDEX('=', @PKCols, 1)-1) -- stores just the insert id part: i.ext_id
								SELECT @sql = 'INSERT INTO #tempVals ' +
											' SELECT ' + @iField +  -- get the id of the table
											', d.' + @fieldName + ', i.' + @fieldName +
											' FROM #ins i full outer join #del d ON ' + @PKCols 
								EXEC (@sql)
								-- temp table now stores the indentity id of the changed table as well as the previous and current values
																																																	
								-- first need to make sure the field has changed because our Update SPs pass in all fields, not just changed ones
								SELECT @sql = 'INSERT INTO assets_audit (asset_id, change_user, change_category, field, previous_value, changed_to_value) ' +
									' SELECT i.asset_id, i.change_user, ''U'', ' +
										'''' + @fieldname + ''', ' +
										't.previous_text, t.current_text' +
									' FROM #ins i full outer join #del d ON ' + @PKCols +
									' INNER JOIN #tempVals t ON ' + @iField + ' = t.ID ' +
									' WHERE i.[' + @fieldname + '] <> d.[' + @fieldname + ']' +
									'	OR (i.[' + @fieldname + '] is null and d.[' + @fieldname + '] is not null)' + 
									'	OR (i.[' + @fieldname + '] is not null and d.[' + @fieldname + '] is null)' 

								EXEC (@sql)																
							END			
					END			
			END
	END


GO
