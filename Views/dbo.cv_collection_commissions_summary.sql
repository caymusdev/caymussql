SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[cv_collection_commissions_summary]
AS
SELECT ISNULL(CAST((row_number() OVER (ORDER BY c.collector)) AS int), 0) AS EDMXID, FORMAT(c.trans_date, 'MMMM yyyy') AS MonthDate, c.collector, SUM(c.trans_amount) AS total_recovered, 
	CONVERT(MONEY, SUM(c.trans_amount * c.commission_rate)) AS comm_amount, FORMAT(c.trans_date, 'yyyyMM') AS MonthYear,
	COUNT (DISTINCT funding_id) AS NumberContracts, MAX(c.total_assigned) AS total_assigned, MAX(c.num_with_plans) AS num_with_plans,
	(SELECT SUM(f.payoff_amount) FROM fundings f WITH (NOLOCK)
	  WHERE id IN (SELECT DISTINCT funding_id FROM commissions c2 WHERE FORMAT(c2.trans_date, 'MMMM yyyy') = FORMAT(c.trans_date, 'MMMM yyyy')
		AND c2.collector = c.collector
	  )		
	) AS current_payoff 
FROM commissions c
GROUP BY FORMAT(c.trans_date, 'MMMM yyyy'), FORMAT(c.trans_date, 'yyyyMM'), c.collector
GO
