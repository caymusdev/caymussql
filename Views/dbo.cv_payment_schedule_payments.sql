SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[cv_payment_schedule_payments]
AS
SELECT    psp.id,   f.legal_name, f.contract_number, ps.start_date, ps.end_date, ps.payment_method, ps.frequency, ps.amount, psp.payment_date, psp.expected_amount, psp.rec_amount, psp.chargeback_amount, psp.payment_id, 
                         psp.trans_id
FROM            dbo.payment_schedule_payments AS psp WITH (NOLOCK) INNER JOIN
                         dbo.payment_schedules AS ps WITH (NOLOCK) ON psp.payment_schedule_id = ps.payment_schedule_id INNER JOIN
                         dbo.funding_payment_schedules AS fps WITH (NOLOCK) ON ps.payment_schedule_id = fps.payment_schedule_id INNER JOIN
                         dbo.fundings AS f ON fps.funding_id = f.id
GO
