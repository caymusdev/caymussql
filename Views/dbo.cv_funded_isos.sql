SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[cv_funded_isos]
AS

SELECT fi.funding_iso_id, fi.funding_id, fi.iso_id, fi.is_referring, fi.legal_name, fi.dba_name, fi.name_on_check, fi.address_1, fi.address_2, fi.city, fi.state, fi.postal_code, fi.bank_name,
	fi.account_number, fi.routing_number, fi.wire_routing_number, fi.wiring_instructions
FROM funding_isos fi


GO
