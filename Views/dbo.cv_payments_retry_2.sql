SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[cv_payments_retry_2]
AS
SELECT        payment_id, payment_schedule_id, amount, trans_date, transaction_id, approved_flag, processed_date, change_date, change_user, previous_transaction_id, orig_transaction_id, is_fee, retry_level, retry_sublevel, funding_id, 
                         complete, batch_id, retry_complete
FROM            dbo.payments
WHERE        (COALESCE (retry_level, 0) = 2) AND deleted = 0 AND waived = 0
GO
