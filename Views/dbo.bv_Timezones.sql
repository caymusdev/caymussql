SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[bv_Timezones]
AS

SELECT name, current_utc_offset, is_currently_dst, name + ': ' + current_utc_offset AS display_name
FROM sys.time_zone_info

GO
