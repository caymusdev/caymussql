SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[cv_PBI_Applied]
AS
SELECT        TOP (100) PERCENT z.id, z.portfolio_value, z.legal_name, z.funded_date, z.purchase_price, z.CashToPP, z.CashToMargin, z.Discount, z.Writeoff, z.CashWire, z.CashCC, z.CashACH, z.FeesCash, z.BadDebtRecovery, 
                         z.AppMarginOut, z.AppPPOut, z.AppMarginOut + z.AppPPOut AS AppPVOut, z.AppMarginOut + z.AppPPOut + z.FeesApplied - z.FeesCash + CASE WHEN z.Writeoff > 0 AND 
                         z.contract_status <> 2 THEN z.Writeoff - z.BadDebtRecovery + z.Discount + z.MarginAdjustment ELSE 0 END AS PayoffAmount, s.contract_status, a.affiliate_name, z.contract_number, z.MarginAdjustment, z.FeesApplied, z.float_amount,
						 z.payoff_amount AS NewPayoff, z.ACHDraft, z.Checks, z.RenewalReapp, z.ACHReject
FROM            (SELECT        y.id, f.portfolio_value, f.legal_name, f.funded_date, f.purchase_price, y.CashToPP, y.CashToMargin, y.Discount, y.Writeoff, y.CashWire, y.CashCC, y.CashACH, y.FeesCash, y.BadDebtRecovery, 
                                                    f.portfolio_value - f.purchase_price - y.CashToMargin - y.Discount - y.MarginAdjustment AS AppMarginOut, f.purchase_price - y.CashToPP - y.Writeoff AS AppPPOut, y.FeesApplied, f.contract_status, f.affiliate_id,
								f.contract_number, y.MarginAdjustment, f.float_amount, f.payoff_amount, y.ACHDraft, y.Checks, y.RenewalReapp, y.ACHReject
                          FROM            (SELECT        id, SUM(CashToPP) AS CashToPP, SUM(CashToMargin) AS CashToMargin, SUM(Discount) AS Discount, SUM(Writeoff) AS Writeoff, SUM(CashWire) AS CashWire, SUM(CashCC) AS CashCC, 
                                                                              SUM(CashACH) AS CashACH, SUM(FeesCash) AS FeesCash, SUM(BadDebtRecovery) AS BadDebtRecovery, SUM(FeesApplied) AS FeesApplied,
																				SUM(MarginAdjustment) AS MarginAdjustment, SUM(ACHDraft) AS ACHDraft, SUM(Checks) AS Checks, SUM(RenewalReapp) AS RenewalReapp, SUM(ACHReject) AS ACHReject
                                                    FROM            (SELECT        f.id, SUM(CASE t .trans_type_id WHEN 7 THEN t .trans_amount ELSE 0 END) AS CashToPP, SUM(CASE t .trans_type_id WHEN 6 THEN t .trans_amount ELSE 0 END) AS CashToMargin, 
                                                                                                        SUM(CASE t .trans_type_id WHEN 8 THEN t .trans_amount ELSE 0 END) AS Discount, SUM(CASE t .trans_type_id WHEN 11 THEN t .trans_amount ELSE 0 END) AS Writeoff, 
                                                                                                        SUM(CASE t .trans_type_id WHEN 5 THEN t .trans_amount ELSE 0 END) AS CashWire, SUM(CASE t .trans_type_id WHEN 4 THEN t .trans_amount ELSE 0 END) AS CashCC, 
                                                                                                        SUM(CASE t .trans_type_id WHEN 3 THEN t .trans_amount ELSE 0 END) AS CashACH, SUM(CASE t .trans_type_id WHEN 1 THEN t .trans_amount ELSE 0 END) AS FeesCash, 
                                                                                                        SUM(CASE t .trans_type_id WHEN 15 THEN t .trans_amount ELSE 0 END) AS BadDebtRecovery, SUM(CASE t .trans_type_id WHEN 9 THEN t .trans_amount ELSE 0 END) AS FeesApplied, 
																										SUM(CASE t .trans_type_id WHEN 17 THEN t .trans_amount ELSE 0 END) AS MarginAdjustment,
																										SUM(CASE t .trans_type_id WHEN 18 THEN t .trans_amount ELSE 0 END) AS ACHDraft,
																										SUM(CASE t .trans_type_id WHEN 19 THEN t .trans_amount ELSE 0 END) AS Checks,
																										SUM(CASE t .trans_type_id WHEN 20 THEN t .trans_amount ELSE 0 END) AS RenewalReapp,
																										SUM(CASE t .trans_type_id WHEN 21 THEN t .trans_amount ELSE 0 END) AS ACHReject,
                                                                                                        t.trans_type_id
                                                                              FROM            dbo.fundings AS f LEFT JOIN
                                                                                                        dbo.cv_transactions AS t ON f.id = t.funding_id
                                                                              GROUP BY f.id, t.trans_type_id) AS x
                                                    GROUP BY id) AS y INNER JOIN
                                                    dbo.fundings AS f ON y.id = f.id) AS z INNER JOIN
                         dbo.contract_statuses AS s ON z.contract_status = s.contract_status_id
LEFT JOIN affiliates a ON z.affiliate_id = a.affiliate_id
ORDER BY z.legal_name
GO
