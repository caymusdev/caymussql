SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[cv_ach_return_codes]
AS
/*
SELECT TOP (100) PERCENT ROW_NUMBER() OVER (ORDER BY x.settle_date DESC) AS RowNumber, s.transaction_id, x.customer_id, x.funding_id, x.settle_date, x.settle_type, 
	s.settle_response_code, x.settle_amount, x.batch_id, 
	x.batch_type, x.batch_status, x.batch_date, x.status_message, x.id, x.legal_name, x.description, x.comments, s.forte_settle_id, x.origination_date
	FROM (SELECT DISTINCT s.transaction_id, s.customer_id, s.funding_id, s.settle_date, s.settle_type, 
		s.settle_response_code, COALESCE(t.authorization_amount, s.settle_amount) AS settle_amount, b.batch_id, 
		b.batch_type, b.batch_status, b.batch_date, b.status_message, f.id, f.legal_name, n.description, n.comments, t.origination_date
	FROM            dbo.forte_settlements AS s LEFT OUTER JOIN
							 dbo.batches AS b ON s.batch_id = b.batch_id LEFT OUTER JOIN
							 dbo.fundings AS f ON s.customer_id = f.contract_number LEFT OUTER JOIN
							 dbo.nacha_codes AS n ON s.settle_response_code = n.code
							 LEFT JOIN forte_trans t ON s.transaction_id = t.transaction_id
	WHERE        (s.settle_response_code LIKE 'R%') 
	AND  (COALESCE (b.batch_status, '') NOT IN ('Duplicate', 'Duplicate-old', 'Deleted'))
) x
INNER JOIN forte_settlements s ON x.transaction_id = s.transaction_id AND x.settle_response_code = s.settle_response_code
*/

SELECT TOP (100) PERCENT ROW_NUMBER() OVER (ORDER BY x.settle_date DESC) AS RowNumber, x.transaction_id, x.customer_id, x.funding_id, x.settle_date, x.settle_type, 
	x.settle_response_code, x.settle_amount, x.batch_id, x.batch_type, x.batch_status, x.batch_date, x.status_message, x.id, x.legal_name, 
	n.description, n.comments, x.forte_settle_id, x.origination_date
FROM (
	SELECT DISTINCT s.transaction_id, s.customer_id, s.funding_id, s.settle_date, s.settle_type, s.settle_response_code, 
		COALESCE(t.authorization_amount, s.settle_amount) AS settle_amount, b.batch_id, b.batch_type, b.batch_status, b.batch_date, b.status_message, f.id, 
		f.legal_name, t.origination_date, s.forte_settle_id
	FROM dbo.forte_settlements s WITH (NOLOCK)
	LEFT JOIN dbo.batches b WITH (NOLOCK) ON s.batch_id = b.batch_id 
	LEFT JOIN dbo.fundings f WITH (NOLOCK) ON s.customer_id = f.contract_number 	
	LEFT JOIN forte_trans t WITH (NOLOCK) ON s.transaction_id = t.transaction_id
	WHERE s.settle_response_code LIKE 'R%' AND  COALESCE (b.batch_status, '') NOT IN ('Duplicate', 'Duplicate-old', 'Deleted')
	UNION ALL
	SELECT CONVERT(VARCHAR (50), t.payment_id) AS transaction_id, f.contract_number AS customer_id, t.[file_name] AS funding_id, t.settle_date, t.settle_type, t.reason_code AS settle_resonse_code,
		t.trans_amount AS settle_amount, t.batch_id, b.batch_type, b.batch_status, b.batch_date, b.status_message, f.id,
		f.legal_name, p.processed_date AS origination_date, t.id AS forte_settle_id
	FROM nacha_trans t WITH (NOLOCK)
	LEFT JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
	INNER JOIN batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
	INNER JOIN payments p WITH (NOLOCK) ON t.payment_id = p.payment_id
	WHERE t.reason_code LIKE 'R%' AND  COALESCE (b.batch_status, '') NOT IN ('Duplicate', 'Duplicate-old', 'Deleted') 
) x
LEFT JOIN dbo.nacha_codes n WITH (NOLOCK) ON x.settle_response_code = n.code
GO
