SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[cv_payment_schedule_change_reasons]
AS
SELECT ISNULL(CAST((row_number() OVER (ORDER BY x.id)) AS int), 0) AS EDMXID, x.id, x.reason_text, x.reason, x.reason_parent_id
FROM (
	SELECT x.id, x.reason_text, x.reason, x.reason_parent_id
	FROM (
		SELECT r2.id, r.reason, r.reason_parent_id, r.reason + ': ' + r2.reason AS reason_text
		FROM payment_schedule_change_reasons r
		LEFT JOIN payment_schedule_change_reasons r2 ON r.id = r2.reason_parent_id
	) x
	WHERE x.reason_text IS NOT NULL
	UNION ALL
	SELECT -1, '', '', NULL
) x
GO
