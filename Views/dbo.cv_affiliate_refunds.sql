SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[cv_affiliate_refunds]
AS

-- this is a list of contracts that have refunds that also have non-performing contracts under different merchants of the same affiliate

SELECT f.id, f.contract_number, f.legal_name, f.refund_out, m.merchant_name, a.affiliate_name
FROM fundings f
INNER JOIN merchants m ON f.merchant_id = m.merchant_id
INNER JOIN affiliates a ON f.affiliate_id = a.affiliate_id
WHERE f.refund_out < 0
	AND EXISTS (
		SELECT 1
		FROM fundings f2
		WHERE f2.affiliate_id = f.affiliate_id AND f2.merchant_id <> f.merchant_id
			AND COALESCE(f2.never_redistribute, 0) = 0
			AND f2.payoff_amount > 0 
			AND (f2.contract_status = 1 OR (f2.contract_status = 4 AND f2.contract_substatus_id = 1))				
			AND f2.performance_status_id IN (2,3) -- slow pay and nonperforming
	)
AND COALESCE(f.never_redistribute, 0) = 0
GO
