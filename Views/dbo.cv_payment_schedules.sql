SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[cv_payment_schedules]
AS
SELECT        ps.payment_schedule_id, ps.payment_method, ps.frequency, ps.amount, ps.start_date, ps.cc_percentage, ps.bank_name, ps.routing_number, ps.account_number, ps.payment_processor, f.id AS funding_id, f.contract_number, 
                         f.legal_name, f.payoff_amount, f.float_amount, ps.end_date, ps.active, ps.merchant_bank_account_id, ps.comments, ps.payment_schedule_change_id,
			ps.change_reason_id, ps.performance_status_id, f.contract_status, f.merchant_id, f.affiliate_id, ps.payment_plan_id, 
			IsNull(CONVERT(BIT, CASE WHEN ps.payment_plan_id IS NULL THEN 0 ELSE 1 END), 0) AS part_of_plan
FROM            dbo.payment_schedules AS ps INNER JOIN
                         dbo.funding_payment_schedules AS fps ON ps.payment_schedule_id = fps.payment_schedule_id INNER JOIN
                         dbo.fundings AS f ON fps.funding_id = f.id
GO
