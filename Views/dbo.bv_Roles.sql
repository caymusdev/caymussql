SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW dbo.bv_Roles
AS

SELECT r.role_id, r.role_name, r.role_desc, r.system_role
FROM roles r WITH (NOLOCK)


GO
