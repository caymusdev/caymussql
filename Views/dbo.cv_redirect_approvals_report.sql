SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW dbo.cv_redirect_approvals_report
AS


SELECT t.trans_id, t.trans_date, tt.trans_type, t.trans_amount, t.batch_id, f.legal_name + ' - ' + f.contract_number AS from_funding_name,
	f2.legal_name + ' - ' + f2.contract_number AS to_funding_name
FROM transactions t WITH (NOLOCK)
INNER JOIN redirect_approvals ra WITH (NOLOCK) ON t.redirect_approval_id = ra.redirect_approval_id
INNER JOIN transaction_types tt WITH (NOLOCK) ON t.trans_type_id = tt.trans_type_id
INNER JOIN fundings f WITH (NOLOCK) ON ra.from_funding_id = f.id
INNER JOIN fundings f2 WITH (NOLOCK) ON ra.to_funding_id = f2.id
WHERE t.redirect_approval_id IS NOT NULL
	AND t.trans_type_id NOT IN (26)
GO
