SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW dbo.cv_funding_payment_dates
AS

SELECT pd.id, f.id AS funding_id, f.legal_name, f.contract_number,  pd.payment_date, pd.amount, pd.rec_amount, pd.active, pd.original, pd.chargeback_amount, 
	pd.merchant_bank_account_id, mba.bank_name, mba.routing_number, mba.account_number, pd.payment_method, pd.payment_processor, pd.comments, pd.change_reason_id, 
	cr.reason, pd.payment_plan_id
FROM funding_payment_dates pd WITH (NOLOCK)
INNER JOIN fundings f WITH (NOLOCK) ON pd.funding_id = f.id
LEFT JOIN merchant_bank_accounts mba ON pd.merchant_bank_account_id = mba.merchant_bank_account_id
LEFT JOIN payment_schedule_change_reasons cr ON pd.change_reason_id = cr.id
GO
