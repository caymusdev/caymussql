SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[cv_payments_to_process]
AS
SELECT        ROW_NUMBER() OVER (ORDER BY p.trans_date ASC) AS RowNumber, p.payment_id, p.trans_date, p.amount, p.approved_flag, ps.payment_method, ps.payment_processor, ps.frequency, ps.start_date, f.contract_number, 
f.legal_name, f.payoff_amount, f.float_amount, f.id AS funding_id
FROM            dbo.payments AS p INNER JOIN
                         dbo.payment_schedules AS ps ON p.payment_schedule_id = ps.payment_schedule_id INNER JOIN
                         dbo.funding_payment_schedules AS fs ON ps.payment_schedule_id = fs.payment_schedule_id INNER JOIN
                         dbo.fundings AS f ON fs.funding_id = f.id
WHERE        (p.processed_date IS NULL) AND (p.approved_flag = 0) AND deleted = 0 AND waived = 0
GO
