SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[bv_Cases]
AS

SELECT a.affiliate_id, a.affiliate_name, a.gross_due, c.case_id, c.last_contact, c.last_attempted_contact, ct_1.contact_type_desc AS last_contact_type, 
	ct_a.contact_type_desc AS last_attempted_contact_type, cts.contact_status_desc AS last_contact_status, c.next_follow_up_date, 
	CASE WHEN pp.payment_plan_id IS NULL THEN 'N' ELSE 'Y' END AS has_payment_plan, pps.performance_status AS payment_plan_status
FROM affiliates a WITH (NOLOCK)
INNER JOIN cases c WITH (NOLOCK) ON a.affiliate_id = c.affiliate_id AND c.active = 1
LEFT JOIN contact_types ct_1 WITH (NOLOCK) ON c.last_contact_type = ct_1.contact_type_id
LEFT JOIN contact_types ct_a WITH (NOLOCK) ON c.last_attempted_contact_type = ct_a.contact_type_id
LEFT JOIN contact_statuses cts WITH (NOLOCK) ON c.last_contact_status_id = cts.contact_status_id
LEFT JOIN payment_plans pp WITH (NOLOCK) ON c.payment_plan_id = pp.payment_plan_id
LEFT JOIN payment_plan_status_codes pps WITH (NOLOCK) ON pp.current_status = pps.id



GO
