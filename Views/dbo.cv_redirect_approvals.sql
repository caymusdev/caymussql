SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[cv_redirect_approvals]
AS
SELECT ra.redirect_approval_id, ra.from_funding_id, ra.to_funding_id, ra.approved_by, ra.approved_date, ra.anticipated_start_date, ra.actual_start_date, ra.payment_schedule_id, 
	ra.end_date, ra.create_date, ra.redirect_reason, f.legal_name, f.contract_number, f2.legal_name AS legal_name_2, f2.contract_number AS contract_number_2, f2.payoff_amount AS payoff_amount_2,
	f.legal_name + ' - ' + f.contract_number AS funding_name, f2.legal_name + ' - ' + f2.contract_number AS funding_name_2, ra.rejected_by, ra.rejected_date, 
	--ps.frequency, ps.payment_method, ps.amount, CASE ps.payment_processor WHEN '53' THEN 'Fifth Third' ELSE ps.payment_processor END AS payment_processor, 
	f.frequency, CASE f.hard_offer_type WHEN 'Split' THEN 'CC' ELSE f.hard_offer_type END AS payment_method, 
	CASE f.frequency WHEN 'Daily' THEN f.weekday_payment ELSE f.weekly_payment END AS amount, 
	CASE f.processor WHEN '53' THEN 'Fifth Third' ELSE f.processor END AS payment_processor,
	ra.on_hold
FROM redirect_approvals ra WITH (NOLOCK)
INNER JOIN fundings f WITH (NOLOCK) ON ra.from_funding_id = f.id
INNER JOIN fundings f2 WITH (NOLOCK) ON ra.to_funding_id = f2.id
-- LEFT JOIN payment_schedules ps WITH (NOLOCK) ON ra.payment_schedule_id = ps.payment_schedule_id
GO
