SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[cv_nacha_trans]
AS

SELECT n.id, n.batch_id, n.funding_id, n.payment_id, n.reason_code, n.settle_type, n.trans_amount, n.company_name, n.settle_date, 
	n.file_name, n.create_date, f.legal_name, f.contract_number
FROM nacha_trans n WITH (NOLOCK)
LEFT JOIN fundings f WITH (NOLOCK) ON n.funding_id = f.id
GO
