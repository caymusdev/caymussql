SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[cv_refunds_due]
AS
SELECT f.refund_out * -1.00 AS refund_out, -- display it as a positive number to the user
	x.last_refund_date, f.id, f.legal_name, m.merchant_name, a.affiliate_name, f.payoff_amount, f.contract_number, f.float_amount,
	DATEDIFF(DD, x.last_refund_date, DATEADD(HOUR, -5, GETUTCDATE())) AS refund_age
FROM fundings f
INNER JOIN (
	SELECT t.funding_id, MAX(t.trans_date) AS last_refund_date
	FROM transactions t
	WHERE t.trans_type_id IN (22)
	GROUP BY t.funding_id
) x ON f.id = x.funding_id
INNER JOIN merchants m ON f.merchant_id = m.merchant_id
LEFT JOIN affiliates a ON f.affiliate_id = a.affiliate_id
WHERE f.refund_out < 0 --AND DATEDIFF(DD, x.last_refund_date, DATEADD(HOUR, -5, GETUTCDATE())) > 30
	AND NOT EXISTS (SELECT 1 FROM payments p WHERE p.funding_id = f.id AND p.is_refund = 1 AND p.processed_date IS NULL AND p.deleted = 0 AND p.waived = 0)
	-- make sure it hasn't already been handled
	AND (SELECT SUM(trans_amount) FROM transactions t WHERE t.funding_id = f.id AND t.trans_type_id IN (22, 23, 25) AND t.redistributed = 0) < 0
GO
