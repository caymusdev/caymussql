SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[cv_merchant_audit]
AS

 SELECT ROW_NUMBER() OVER (ORDER BY m.merchant_id, f.contract_number) AS RowNumber, m.merchant_id, m.merchant_name, COALESCE(f.legal_name, '') AS legal_name, f.contract_number, f.id AS funding_id, 
	f.payoff_amount, f.portfolio_value, f.paid_off_amount, f.percent_paid, cs.contract_status, f.last_payment_date, a.affiliate_name, a.affiliate_id, f.funded_date,
	COALESCE(f.fees_out, 0) AS fees_out, 
	COALESCE((SELECT SUM(trans_amount) FROM transactions t WHERE t.funding_id = f.id AND t.trans_type_id = 9 AND t.redistributed = 0), 0) AS total_fees
 FROM merchants m 
 LEFT JOIN fundings f ON f.merchant_id = m.merchant_id
 LEFT JOIN contract_statuses cs ON f.contract_status = cs.contract_status_id
 LEFT JOIN affiliates a ON m.affiliate_id = a.affiliate_id
GO
