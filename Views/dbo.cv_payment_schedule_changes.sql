SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[cv_payment_schedule_changes]
AS
SELECT c.id, CASE c.change_type WHEN 'Insert' THEN 'New' WHEN 'Update' THEN 'Change' ELSE c.change_type END AS change_type,  
	f.contract_number, c.start_date, c.end_date, c.amount, c.frequency, c.bank_name, c.routing_number, c.account_number, f.legal_name, f.payoff_amount, 
	f.last_payment_date, COALESCE(ps.frequency, f.frequency) AS orig_frequency, 
	CASE COALESCE(ps.frequency, f.frequency) WHEN 'Daily' THEN COALESCE(ps.amount, f.weekday_payment) ELSE COALESCE(ps.amount, f.weekly_payment) END AS payment, c.comments,
	c.payment_processor, c.payment_method, c.change_reason_id, cr2.reason + ': ' + cr.reason AS reason_text, c.active
FROM payment_schedule_changes c
INNER JOIN fundings f ON c.funding_id = f.id
LEFT JOIN payment_schedule_change_reasons cr ON c.change_reason_id = cr.id
LEFT JOIN payment_schedule_change_reasons cr2 ON cr.reason_parent_id = cr2.id
LEFT JOIN payment_schedules ps WITH (NOLOCK) ON c.payment_schedule_id = ps.payment_schedule_id
WHERE c.approved_by IS NULL
GO
