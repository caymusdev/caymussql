SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[cv_funding_audit]
AS
/*
SELECT x.trans_id, x.trans_date, x.trans_amount, x.trans_type, x.payment_id, x.funding_id, 
	x.is_fee, f.legal_name, p.payment_id AS orig_payment_id, p2.payment_id AS previous_payment_id, f.contract_number, 
	SUM(x.trans_amount) OVER (ORDER BY x.trans_id) AS RunningTotal
FROM (
	SELECT t.trans_id, t.trans_date, 
		CASE t.trans_type_id WHEN 9 THEN -1 * t.trans_amount ELSE t.trans_amount END AS trans_amount, tt.trans_type,
		COALESCE(t.transaction_id, p.transaction_id) AS transaction_id, 
		CASE WHEN t.payment_id IS NULL THEN COALESCE(p2.orig_transaction_id, p2.transaction_id)
			WHEN p.orig_transaction_id IS NULL THEN COALESCE(t.transaction_id, p.transaction_id)
			ELSE p.orig_transaction_id END AS orig_transaction_id, 
		p.previous_transaction_id, p.payment_id, t.funding_id, 
		CASE WHEN (p.is_fee = 1 OR t.trans_type_id = 9) THEN 1 ELSE 0 END AS is_fee
	FROM transactions t
	LEFT JOIN transaction_details d ON t.trans_id = d.trans_id AND t.trans_type_id = 3 AND d.trans_type_id = 3
	INNER JOIN transaction_types tt ON t.trans_type_id = tt.trans_type_id
	LEFT JOIN payments p ON t.payment_id = p.payment_id
	LEFT JOIN payments p2 ON t.payment_id IS NULL AND t.transaction_id = p2.transaction_id
	WHERE t.funding_id = 720
		AND d.trans_detail_id IS NULL
		AND t.redistributed = 0
	--ORDER BY t.trans_date
) x
INNER JOIN fundings f ON x.funding_id = f.id
LEFT JOIN payments p ON COALESCE(x.orig_transaction_id, '-1') = COALESCE(p.transaction_id, '-2')
LEFT JOIN payments p2 ON COALESCE(x.previous_transaction_id, '-1') = COALESCE(p2.transaction_id, '-2')
--INNER JOIN affiliates a ON f.affiliate_id = a.affiliate_id
--INNER JOIN merchants m ON f.merchant_id = m.merchant_id
--WHERE x.orig_transaction_id = 'trn_075d5fb3-89b3-4c27-9e6b-139fff9d379e'
-- ORDER BY x.trans_date, x.is_fee
*/

SELECT f.id, m.trans_id, m.trans_date, m.trans_amount, m.payment_id, m.funding_id, m.is_fee, f.legal_name, m.orig_payment_id, m.previous_payment_id, 
	f.contract_number, m.balance, tt.trans_type,
	 ISNULL((ROW_NUMBER() OVER (ORDER BY m.merchant_audit_id ASC)), 0) AS 'ROWID', m.retry_level, m.retry_sublevel, m.retry_level_text, m.response_code, 
	 mm.merchant_id, mm.merchant_name, a.affiliate_id, a.affiliate_name, f.funded_date,
	 COALESCE(f.fees_out, 0) AS fees_out, 
	 COALESCE((SELECT SUM(trans_amount) FROM transactions t WHERE t.funding_id = f.id AND t.trans_type_id = 9 AND t.redistributed = 0), 0) AS total_fees,
	 m.payment_path, m.trans_path, m.result_desc, n.comments AS nacha_comments, n.description AS nacha_description, m.trans_comments
FROM merchant_audit m WITH (NOLOCK)
INNER JOIN fundings f WITH (NOLOCK) ON m.funding_id = f.id
LEFT JOIN transaction_types tt WITH (NOLOCK) ON m.trans_type = tt.trans_type_id
LEFT JOIN merchants mm WITH (NOLOCK) ON f.merchant_id = mm.merchant_id
--LEFT JOIN affiliates a WITH (NOLOCK) ON f.affiliate_id = a.affiliate_id
LEFT JOIN affiliates a WITH (NOLOCK) ON mm.affiliate_id = a.affiliate_id
LEFT JOIN nacha_codes n WITH (NOLOCK) ON m.response_code = n.code
--ORDER BY --m.merchant_audit_id
--  m.trans_date, m.trans_id, m.is_fee
GO
