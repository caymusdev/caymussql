SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[cv_distributions]
AS
SELECT        t.trans_id, t.trans_date, t.trans_amount, t.comments, t.batch_id, b.batch_type, tt.trans_type, t.record_id, f.legal_name, f.funding_id, f.contract_number, cs.contract_status
FROM            dbo.transactions AS t LEFT OUTER JOIN
                         dbo.transaction_details AS d ON t.trans_id = d.trans_id LEFT OUTER JOIN
                         dbo.fundings AS f ON t.funding_id = f.id LEFT OUTER JOIN
                         dbo.contract_statuses AS cs ON f.contract_status = cs.contract_status_id LEFT OUTER JOIN
                         dbo.transaction_types AS tt ON t.trans_type_id = tt.trans_type_id LEFT OUTER JOIN
                         dbo.batches AS b ON t.batch_id = b.batch_id
WHERE        
(d.trans_id IS NULL) --AND (t.comments NOT LIKE '%Migrate from CashApplication') 
AND (t.trans_date >= '2019-01-01')
AND (t.redistributed = 0) AND (COALESCE (b.batch_status, '') NOT IN ('Duplicate', 'Duplicate-old', 'Deleted'))
GO
