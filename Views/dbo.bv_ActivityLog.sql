SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[bv_ActivityLog]
AS


SELECT at.activity_type_desc, al.[user_id], al.comments, al.create_date, a.affiliate_name, a.affiliate_id
FROM activity_log al WITH (NOLOCK)
INNER JOIN activity_types at WITH (NOLOCK) ON al.activity_type_id = at.activity_type_id
INNER JOIN affiliates a ON al.affiliate_id = a.affiliate_id

GO
