SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[cv_processor_transactions]
WITH SCHEMABINDING
AS

-- funded transactions that do NOT have a rejection
SELECT t.customer_id, t.status, t.authorization_amount, t.origination_date, t.company_name, 
	s.settle_date, COALESCE(s.settle_response_code, '') AS settle_response_code, s.batch_id, 'Forte' AS Processor, 'ACH' AS source, t.transaction_id
FROM dbo.forte_trans t WITH (NOLOCK)
INNER JOIN dbo.forte_settlements s WITH (NOLOCK) ON t.transaction_id = s.transaction_id
WHERE t.status IN ('funded', 'unfunded')
	AND NOT EXISTS (SELECT 1 FROM dbo.forte_settlements s WHERE t.transaction_id = s.transaction_id AND s.settle_response_code NOT LIKE 'S%')	

UNION ALL

-- funded trans that later were likely unfunded (no matching S01 in settlements)  (has NON S code)
SELECT t.customer_id, t.status, t.authorization_amount, t.origination_date, t.company_name, s.settle_date, 
	COALESCE(s.settle_response_code, '') AS settle_response_code, s.batch_id, 'Forte' AS Processor, 'ACH' AS source, t.transaction_id
FROM dbo.forte_trans t WITH (NOLOCK)
LEFT JOIN dbo.forte_settlements s WITH (NOLOCK) ON t.transaction_id = s.transaction_id AND s.settle_response_code LIKE 'S%'
WHERE t.status IN ('funded')
	AND EXISTS (SELECT 1 FROM dbo.forte_settlements s WHERE t.transaction_id = s.transaction_id AND s.settle_response_code NOT LIKE 'S%')

UNION ALL

-- unfunded trans that later were likely unfunded (no matching S01 in settlements)  (has NON S code)
SELECT t.customer_id, t.status, COALESCE(s.settle_amount, t.authorization_amount) AS authorization_amount, t.origination_date, t.company_name, s.settle_date, 
	COALESCE(s.settle_response_code, '') AS settle_response_code, s.batch_id, 'Forte' AS Processor, 'ACH' AS source, t.transaction_id
FROM dbo.forte_trans t WITH (NOLOCK)
LEFT JOIN dbo.forte_settlements s WITH (NOLOCK) ON t.transaction_id = s.transaction_id AND s.settle_response_code NOT LIKE 'S%'
WHERE t.status IN ('unfunded')
	AND EXISTS (SELECT 1 FROM dbo.forte_settlements s WHERE t.transaction_id = s.transaction_id AND s.settle_response_code NOT LIKE 'S%')

UNION ALL

-- forte trans
SELECT t.customer_id, t.status, t.authorization_amount, 
	COALESCE(t.origination_date, CONVERT(DATE, p.processed_date), CONVERT(DATE, t.received_date)) AS origination_date, t.company_name, 
	COALESCE(s.settle_date, CONVERT(DATE, t.received_date)) AS settle_date, 
	COALESCE(s.settle_response_code, '') AS settle_response_code, s.batch_id, 'Forte' AS Processor, 'ACH' AS source, t.transaction_id
FROM dbo.forte_trans t WITH (NOLOCK) 
LEFT JOIN dbo.forte_settlements s WITH (NOLOCK) ON t.transaction_id = s.transaction_id
LEFT JOIN dbo.payments p ON t.transaction_id = p.transaction_id  -- some declined show NULL origination date so get the date the payment was processed
WHERE t.status NOT IN ('settling', 'funded', 'unfunded')

UNION ALL 

-- forte floats
SELECT t.customer_id, t.status, t.authorization_amount, t.origination_date, t.company_name, s.settle_date, 
	COALESCE(s.settle_response_code, '') AS settle_response_code, s.batch_id, 'Forte' AS Processor, 'ACH' AS source, t.transaction_id
FROM dbo.forte_trans t WITH (NOLOCK) 
LEFT JOIN dbo.forte_settlements s WITH (NOLOCK) ON t.transaction_id = s.transaction_id
WHERE t.status IN ('settling')
	AND NOT EXISTS (SELECT 1 FROM dbo.forte_trans t2 WHERE t.transaction_id = t2.transaction_id AND t.status <> t2.status)

UNION ALL
-- ips 
SELECT f.contract_number, 'funded' AS status, s.hold_amt, s.file_date, s.merch_name, s.file_date, '' AS settle_response_code, s.BatchID, 'IPS' AS Processor, 'CC' AS source, 
	CONVERT(VARCHAR(50), s.ips_staging_id) AS transaction_id
FROM dbo.ips_staging s WITH (NOLOCK)
LEFT JOIN dbo.merchants m WITH (NOLOCK) ON s.merch_no = m.MID
LEFT JOIN dbo.fundings f WITH (NOLOCK) ON f.id = (SELECT TOP 1 f2.id FROM dbo.fundings f2 WHERE f2.merchant_id = m.merchant_id AND f2.contract_status IN (1,4) ORDER BY f2.contract_status, f2.funded_date ASC)


UNION ALL
-- evo
SELECT f.contract_number, 'funded' AS status, s.[Split Amount], s.Date, s.DBA, s.Date, '' AS settle_response_code, s.BatchID, 'EVO/VPS' AS Processor, 'CC' AS source, 
	CONVERT(VARCHAR (50), s.evo_staging_id) AS transaction_id
FROM dbo.evo_staging s WITH (NOLOCK)
LEFT JOIN dbo.merchants m WITH (NOLOCK) ON s.MID = m.MID
LEFT JOIN dbo.fundings f WITH (NOLOCK) ON f.id = (SELECT TOP 1 f2.id FROM dbo.fundings f2 WHERE f2.merchant_id = m.merchant_id AND f2.contract_status IN (1,4) ORDER BY f2.contract_status, f2.funded_date ASC)

UNION ALL
-- FIFTH THIRD
SELECT f.contract_number, CASE t.trans_type_id WHEN 14 THEN 'unfunded' ELSE 'funded' END AS status, t.trans_amount, t.trans_date, f.legal_name, t.trans_date,
	CASE CHARINDEX(' ', t.comments) WHEN 0 THEN t.comments 
		ELSE CASE REVERSE(LEFT(REVERSE(REPLACE(NULLIF(t.comments, ''), ' : withdrawal', '')), CHARINDEX(' ', REVERSE(REPLACE(NULLIF(t.comments, ''), ' : withdrawal', '')))-1)) 
		WHEN 'deposit' THEN 'S01' 
		ELSE REVERSE(LEFT(REVERSE(REPLACE(NULLIF(t.comments, ''), ' : withdrawal', '')), CHARINDEX(' ', REVERSE(REPLACE(NULLIF(t.comments, ''), ' : withdrawal', '')))-1)) END 
		END AS settle_response_code,
	t.batch_id, 'Fifth Third' AS processor, 'ACH', t.transaction_id
FROM dbo.transactions t WITH (NOLOCK)
INNER JOIN dbo.batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
INNER JOIN dbo.fundings f WITH (NOLOCK) ON t.funding_id = f.id
WHERE b.batch_type LIKE '%FifthThird%'
	AND t.trans_type_id NOT IN (18,9)

UNION ALL
-- Regions
SELECT f.contract_number, CASE t.trans_type_id WHEN 14 THEN 'unfunded' ELSE 'funded' END AS status, t.trans_amount, t.trans_date, f.legal_name, t.trans_date,
	CASE CHARINDEX(' ', t.comments) WHEN 0 THEN t.comments 
		ELSE CASE REVERSE(LEFT(REVERSE(REPLACE(NULLIF(t.comments, ''), ' : withdrawal', '')), CHARINDEX(' ', REVERSE(REPLACE(NULLIF(t.comments, ''), ' : withdrawal', '')))-1)) 
		WHEN 'deposit' THEN 'S01' 
		ELSE REVERSE(LEFT(REVERSE(REPLACE(NULLIF(t.comments, ''), ' : withdrawal', '')), CHARINDEX(' ', REVERSE(REPLACE(NULLIF(t.comments, ''), ' : withdrawal', '')))-1)) END 
		END AS settle_response_code,
	t.batch_id, 'Regions' AS processor, 'ACH', t.transaction_id
FROM dbo.transactions t WITH (NOLOCK)
INNER JOIN dbo.batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
INNER JOIN dbo.fundings f WITH (NOLOCK) ON t.funding_id = f.id
WHERE b.batch_type LIKE '%Regions%'
	AND t.trans_type_id NOT IN (18,9)

UNION ALL
-- wires
SELECT f.contract_number, 'funded' AS status, t.trans_amount, t.trans_date, f.legal_name, t.trans_date, '' AS settle_response_code, t.batch_id, 'Wires' AS Processor, 'Wires' AS source, 
	t.transaction_id
FROM dbo.transactions t WITH (NOLOCK)
INNER JOIN dbo.fundings f WITH (NOLOCK) ON t.funding_id = f.id
WHERE t.trans_type_id = 5

UNION ALL
-- checks/cash
SELECT f.contract_number, 'funded' AS status, t.trans_amount, t.trans_date, f.legal_name, t.trans_date, '' AS settle_response_code, t.batch_id, 'Cash' AS Processor, 'Cash' AS source, 
	t.transaction_id
FROM dbo.transactions t WITH (NOLOCK)
INNER JOIN dbo.fundings f WITH (NOLOCK) ON t.funding_id = f.id
WHERE t.trans_type_id = 2


UNION ALL
-- orig fee
SELECT f.contract_number, 'funded' AS status, f.orig_fee, f.funded_date, f.legal_name, f.funded_date, '' AS settle_response_code, NULL AS batch_id, 'Orig Fee' AS Processor, 'Orig Fee' AS source, 
	NULL AS transaction_id
FROM dbo.fundings f WITH (NOLOCK)
WHERE f.orig_fee IS NOT NULL

UNION ALL
-- reapplication
SELECT f.contract_number, 'funded' AS status, t.trans_amount, t.trans_date, f.legal_name, t.trans_date, '' AS settle_response_code, t.batch_id, 'Reapply' AS Processor, 'Reapply' AS source,
	t.transaction_id
FROM dbo.transactions t WITH (NOLOCK)
INNER JOIN dbo.fundings f WITH (NOLOCK) ON t.funding_id = f.id
WHERE t.trans_type_id = 16

UNION ALL
-- settlement
SELECT f.contract_number, 'funded' AS status, t.trans_amount, t.trans_date, f.legal_name, t.trans_date, '' AS settle_response_code, t.batch_id, 'Settlement' AS Processor, 'Settlement' AS source,
	t.transaction_id
FROM dbo.transactions t WITH (NOLOCK)
INNER JOIN dbo.fundings f WITH (NOLOCK) ON t.funding_id = f.id
WHERE t.trans_type_id = 10

UNION ALL
-- discount
SELECT f.contract_number, 'funded' AS status, t.trans_amount, t.trans_date, f.legal_name, t.trans_date, '' AS settle_response_code, t.batch_id, 'Discount' AS Processor, 'Discount' AS source,
	t.transaction_id
FROM dbo.transactions t WITH (NOLOCK)
INNER JOIN dbo.fundings f WITH (NOLOCK) ON t.funding_id = f.id
WHERE t.trans_type_id = 8

UNION ALL
-- ACH Received NOT tied to a transaction and from 2019-01-01 on
SELECT f.contract_number, 'funded' AS status, t.trans_amount, t.trans_date, f.legal_name, t.trans_date, '' AS settle_response_code, t.batch_id, 'ACH' AS Processor, 'ACH' AS source, 
	t.transaction_id
FROM dbo.transactions t WITH (NOLOCK)
INNER JOIN dbo.fundings f WITH (NOLOCK) ON t.funding_id = f.id
INNER JOIN dbo.batches b WITH (NOLOCK) ON t.batch_id = b.batch_id
WHERE t.trans_type_id = 3 AND transaction_id IS NULL AND trans_date >= '2019-01-01' 	
	AND b.batch_type NOT LIKE '%FifthThird%' AND b.batch_type NOT LIKE '%Regions%'
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'cv_processor_transactions', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 1, 'SCHEMA', N'dbo', 'VIEW', N'cv_processor_transactions', NULL, NULL
GO
