SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[bv_UserRoles]
AS

SELECT ur.user_role_id, u.user_id, u.email, r.role_id, r.role_name, r.role_desc, 
	CASE WHEN u.user_id IS NULL THEN 'false' ELSE 'true' END AS has_role
FROM roles r WITH (NOLOCK)
LEFT JOIN user_roles ur WITH (NOLOCK) ON r.role_id = ur.role_id
LEFT JOIN users u WITH (NOLOCK) ON ur.user_id = u.user_id

GO
