SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[cv_transactions_model]
AS
SELECT ROW_NUMBER() OVER (ORDER BY x.funding_id, x.trans_id, x.trans_type_id) AS id, x.trans_id, x.trans_date, x.funding_id, x.trans_amount, x.comments, x.batch_id, 
	x.trans_type_id, x.trans_type, x.transaction_id, x.payment_id, 
	CASE WHEN x.trans_type_id IN (2, 3, 4, 5, 19) AND x.trans_amount > (.8 * f.payoff_amount) 
		AND DATEDIFF(DD, f.funded_date, GETUTCDATE()) < 90 AND f.include_prepay_discounts = 1 THEN 1 ELSE 0 END AS possible_early_payoff
FROM (
	SELECT DISTINCT        x.trans_id, x.trans_date, x.funding_id, x.trans_amount, x.comments, x.batch_id, x.trans_type_id, tt.trans_type, x.transaction_id, x.payment_id
	FROM            (SELECT        trans_id, trans_date, funding_id, trans_amount, comments, batch_id, trans_type_id, t.transaction_id, t.payment_id
							  FROM            dbo.transactions AS t
							  WHERE        (redistributed = 0)
							  UNION ALL
							  SELECT        d.trans_id, t.trans_date, t.funding_id, d.trans_amount, d.comments, t.batch_id, d.trans_type_id, t.transaction_id, t.payment_id
							  FROM            dbo.transaction_details AS d INNER JOIN
													   dbo.transactions AS t ON d.trans_id = t.trans_id
							  WHERE        (t.redistributed = 0)) AS x LEFT OUTER JOIN
							 dbo.transaction_types AS tt ON x.trans_type_id = tt.trans_type_id
	LEFT JOIN batches b ON x.batch_id = b.batch_id
	WHERE COALESCE(b.batch_status, '') NOT IN ('Duplicate', 'Duplicate-old', 'Deleted')		
) x
LEFT JOIN fundings f ON x.funding_id = f.id
GO
