SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[cv_iso_bonus_details]

AS

SELECT f.id AS funding_id, f.contract_number, f.legal_name, f.funded_date, f.purchase_price, f.portfolio_value, f.pricing_ratio, f.contract_type, 
	f.iso_bonus_id
FROM fundings f WITH (NOLOCK)

GO
