SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[cv_fundings_queue]
AS
SELECT        f.id, NULLIF (f.funding_id, '') AS funding_id, f.affiliate_id, f.merchant_id, f.contract_number, f.legal_name, f.contract_type, f.funded_date, f.purchase_price, f.portfolio_value, f.unearned_income, f.pricing_ratio, 
                         f.commission_percent, f.commission_amount, f.estimated_turn, f.upfront_total_commission, f.upfront_commission_percent, f.calc_turn, f.payoff_amount, f.net_to_merchant, f.apply_to_contract, f.apply_to_date, f.frequency, 
                         f.weekday_payment, f.competitor_payoff, f.receivables_bank_name, f.receivables_account_nbr, f.receivables_aba, f.weekly_payment, f.create_date, f.temp_id, f.processor, c.contract_status, f.first_payment_date,
						 f.hard_offer_type, f.cc_split, f.eligible, f.portfolio
FROM            dbo.fundings AS f LEFT OUTER JOIN
                         dbo.contract_statuses AS c ON f.contract_status = c.contract_status_id
WHERE        (f.processor IS NULL) AND (f.contract_status IN (1, 4)) AND (COALESCE (f.funded_date, dbo.CF_GetSetting(N'GoLiveDate')) >= dbo.CF_GetSetting(N'GoLiveDate'))
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "f"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 226
               Right = 282
            End
            DisplayFlags = 280
            TopColumn = 11
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 320
               Bottom = 136
               Right = 504
            End
            DisplayFlags = 280
            TopColumn = 2
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'cv_fundings_queue', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 1, 'SCHEMA', N'dbo', 'VIEW', N'cv_fundings_queue', NULL, NULL
GO
