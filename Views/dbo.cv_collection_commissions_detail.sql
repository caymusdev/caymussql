SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[cv_collection_commissions_detail]
AS
SELECT ISNULL(CAST((row_number() OVER (ORDER BY c.collector, f.legal_name)) AS int), 0) AS EDMXID, FORMAT(c.trans_date, 'MMMM yyyy') AS MonthDate, c.collector, f.legal_name, f.contract_number, 
	SUM(c.trans_amount) AS total_recovered, c.commission_rate * 100.00 AS comm_rate,
	CONVERT(MONEY, SUM(c.commission_amount)) AS comm_amount, 
	FORMAT(c.trans_date, 'yyyyMM') AS MonthYear, MAX(c.total_assigned) AS total_assigned, MAX(c.num_with_plans) AS num_with_plans, f.payoff_amount AS current_payoff,
	--cr.collection_level
	c.affiliate_bucket AS collection_level
FROM commissions c WITH (NOLOCK)
INNER JOIN fundings f WITH (NOLOCK) ON c.funding_id = f.id
--LEFT JOIN commission_rates cr WITH (NOLOCK) ON c.commission_rate_id = cr.commission_rate_id
GROUP BY FORMAT(c.trans_date, 'MMMM yyyy'), FORMAT(c.trans_date, 'yyyyMM'), c.collector, f.legal_name, f.contract_number, f.payoff_amount, c.commission_rate, c.affiliate_bucket --cr.collection_level
GO
