SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[cv_transactions]
AS
SELECT        TOP (100) PERCENT x.trans_id, x.trans_date, x.funding_id, x.trans_amount, x.comments, x.batch_id, x.trans_type_id, tt.trans_type, x.transaction_id, x.payment_id,
	COALESCE(b.settle_date, b.batch_date, x.trans_date) AS settle_date
FROM            (SELECT        trans_id, trans_date, funding_id, trans_amount, comments, batch_id, trans_type_id, t.transaction_id, t.payment_id, t.from_access
                          FROM            dbo.transactions AS t
                          WHERE        (redistributed = 0)
                          UNION 
                          SELECT        d.trans_id, t.trans_date, t.funding_id, d.trans_amount, d.comments, t.batch_id, d.trans_type_id, t.transaction_id, t.payment_id, t.from_access
                          FROM            dbo.transaction_details AS d INNER JOIN
                                                   dbo.transactions AS t ON d.trans_id = t.trans_id
                          WHERE        (t.redistributed = 0)) AS x LEFT OUTER JOIN
                         dbo.transaction_types AS tt ON x.trans_type_id = tt.trans_type_id
LEFT JOIN batches b ON x.batch_id = b.batch_id
WHERE --COALESCE(b.batch_status, '') NOT IN ('Duplicate', 'Duplicate-old', 'Deleted')
	(b.batch_status IN('Settled', 'Closed') OR x.from_access = 1) 
--ORDER BY x.trans_date
UNION ALL
SELECT NULL, f.funded_date, f.id, f.orig_fee, 'Origination Fee', NULL, NULL, 'Origination Fee', NULL AS transaction_id, NULL AS payment_id, f.funded_date AS settle_date
FROM fundings f
WHERE f.orig_fee IS NOT NULL
--ORDER BY 2
GO
