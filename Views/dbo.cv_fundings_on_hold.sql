SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO








CREATE VIEW [dbo].[cv_fundings_on_hold]
AS
/*
SELECT        f.id, f.legal_name, f.contract_number, f.payments_stopped_date, f.on_hold, f.contract_status, f.payoff_amount, f.float_amount, f.paid_off_amount, f.percent_paid, t.authorization_amount AS last_attempt_amount, 
                         t.origination_date AS last_origination_date, t.settle_date AS last_settle_date, t.status AS last_status, t.settle_response_code AS last_response_code, 
						 cs.contract_status AS contract_status_text
FROM            dbo.fundings AS f LEFT OUTER JOIN
                             (SELECT        customer_id, MAX(origination_date) AS last_orignation_date
                               FROM            dbo.cv_processor_transactions AS t
                               GROUP BY customer_id) AS x ON f.contract_number = x.customer_id LEFT OUTER JOIN
                         dbo.cv_processor_transactions AS t ON x.customer_id = t.customer_id AND x.last_orignation_date = t.origination_date
						LEFT JOIN contract_statuses cs ON f.contract_status = cs.contract_status_id
WHERE        (f.on_hold = 1)
*/
WITH last_orig_data (customer_id, last_origination_date)
AS
(
	SELECT        customer_id, MAX(origination_date) AS last_origination_date
	FROM            dbo.cv_processor_transactions AS t
	GROUP BY customer_id
)

SELECT ROW_NUMBER() OVER (ORDER BY f.payments_stopped_date DESC) AS RowNumber, 
	f.id, f.legal_name, f.contract_number, f.payments_stopped_date, f.on_hold, f.contract_status, f.payoff_amount, f.float_amount, f.paid_off_amount, 
	f.percent_paid, t.authorization_amount AS last_attempt_amount, 
	t.origination_date AS last_origination_date, t.settle_date AS last_settle_date, t.status AS last_status, t.settle_response_code AS last_response_code, 
	cs.contract_status AS contract_status_text
FROM dbo.fundings AS f WITH (NOLOCK)
INNER JOIN contract_statuses cs WITH (NOLOCK) ON f.contract_status = cs.contract_status_id
INNER JOIN last_orig_data d WITH (NOLOCK) ON f.contract_number = d.customer_id
LEFT JOIN dbo.cv_processor_transactions AS t ON d.customer_id = t.customer_id AND d.last_origination_date = t.origination_date

WHERE        (f.on_hold = 1)  
	AND f.contract_status IN (1,3, 4) AND COALESCE(f.contract_substatus_id, -1) NOT IN (2)
GO
