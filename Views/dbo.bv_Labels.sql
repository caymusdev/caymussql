SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[bv_Labels]
AS

SELECT word_id, word, word_desc, translated_word
FROM words  WITH (NOLOCK)
GO
