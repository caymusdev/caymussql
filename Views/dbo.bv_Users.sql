SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[bv_Users]
AS

 

SELECT r.user_id, r.email, r.timezone, r.active
FROM users r WITH (NOLOCK)


GO
