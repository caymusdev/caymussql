SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[cv_iso_bonuses]
AS

SELECT b.iso_bonus_id, b.iso_name, b.bonus_amount, b.bonus_month, b.bonus_year, fb.bonus_metric, fb.bonus_filter, b.bonus_percent_level, b.bonus_amount_level,
	CONVERT(VARCHAR (50), b.bonus_month) + ' ' + RIGHT('00' + CONVERT(VARCHAR (50), b.bonus_year), 2) AS month_year_old,
	CONVERT(VARCHAR (50), b.bonus_year) + RIGHT('00' + CONVERT(VARCHAR (50), b.bonus_month), 2) AS month_year, b.total_amount, b.total_volume,
	b.approved_by, b.approved_date
FROM iso_bonuses b WITH (NOLOCK)
INNER JOIN funding_bonuses fb WITh (NOLOCK) ON b.funding_bonus_id = fb.funding_bonus_id 
--WHERE b.approved_by IS NULL AND b.approved_date IS NULL

GO
