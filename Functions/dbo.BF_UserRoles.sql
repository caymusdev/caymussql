SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-09-15
-- Description:	Need a function so I can pass in the user id and return a table that can then be
-- =============================================
CREATE FUNCTION dbo.BF_UserRoles
(	
	@user_id			INT	
)
RETURNS TABLE 
AS
RETURN 
(
SELECT ur.user_role_id, u.user_id, u.email, r.role_id, r.role_name, r.role_desc, 
	CASE WHEN u.user_id IS NULL THEN 'false' ELSE 'true' END AS has_role
	--CASE WHEN EXISTS(SELECT 1 FROM user_roles ur2 WHERE ur2.role_id = r.role_id AND ur2.user_id = u.user_id) THEN 'true' ELSE 'false' END AS has_role
FROM roles r WITH (NOLOCK)
LEFT JOIN user_roles ur WITH (NOLOCK) ON r.role_id = ur.role_id AND ur.user_id = @user_id
LEFT JOIN users u WITH (NOLOCK) ON ur.user_id = u.user_id AND u.user_id = @user_id

)
GO
