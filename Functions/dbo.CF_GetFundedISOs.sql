SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-02-04
-- Description:	Selects funded isos for a given funding id
-- =============================================
CREATE FUNCTION [dbo].[CF_GetFundedISOs]
(	
	@funding_id			INT
)
RETURNS TABLE 
AS
RETURN 
(
SELECT fi.funding_id, fi.iso_id, fi.is_referring, fi.legal_name, fi.dba_name, fi.name_on_check, fi.address_1, fi.address_2, fi.city, fi.state, fi.postal_code, fi.bank_name,
	fi.account_number, fi.routing_number, fi.wire_routing_number, fi.wiring_instructions
FROM funding_isos fi
WHERE fi.funding_id = @funding_id
)
GO
