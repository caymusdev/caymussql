SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-12-02
-- Description:	Function that will return reminders and notifications to be displayed in a grid.  Can be for a specific person and also for future only records
-- =============================================
CREATE FUNCTION [dbo].[BF_GetAlerts]
(	
	@user_email			NVARCHAR (100),
	@include_past		BIT
)

RETURNS TABLE 
AS
RETURN 
(
	SELECT x.alert_id, x.user_id, x.affiliate_id, x.alert_message, x.alert_type, x.alert_type_desc, x.displayed_time, x.acted_on_time, x.record_id, a.affiliate_name, 
		u.email, CASE WHEN x.IsReminder = 1 THEN 'Reminder' ELSE 'Notification' END AS reminder_notification, x.completed
	FROM (
		SELECT 'N' + CONVERT(VARCHAR (50), n.notification_id) AS alert_id, n.user_id, n.affiliate_id, n.notification_message AS alert_message, nt.notification_type AS alert_type, 
			nt.notification_type_desc AS alert_type_desc, n.displayed_time, n.acted_on_time, n.record_id, 0 AS IsReminder, NULL AS completed
		FROM notifications n
		LEFT JOIN notification_types nt ON n.notification_type_id = nt.notification_type_id
		UNION ALL
		SELECT 'R' + CONVERT(VARCHAR (50), r.reminder_id) AS alert_id, r.user_id, r.affiliate_id, r.notes AS alert_message, ct.contact_type AS alert_type, 
			ct.contact_type_desc AS alert_type_desc, r.reminder_date AS displayed_time, r.reminder_date AS acted_on_time, NULL AS record_id, 1 AS IsReminder,
			r.completed
		FROM reminders r
		LEFT JOIN contact_types ct ON r.contact_type_id = ct.contact_type_id
	) x 
	INNER JOIN affiliates a ON x.affiliate_id = a.affiliate_id
	LEFT JOIN users u ON x.user_id = u.user_id
	WHERE (u.email = @user_email OR COALESCE(@user_email, '') = '')
		AND (
			(x.IsReminder = 0 AND (COALESCE(x.displayed_time, GETUTCDATE()) >= GETUTCDATE() OR @include_past = 1))
		 OR (x.IsReminder = 1 AND (x.completed IS NULL OR @include_past = 1))
		 )
)
GO
