SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rick Pina
-- Create date: 2021-02-04
-- Description:	Need a function so I can pass in the user id and return a table that can then be
-- =============================================
CREATE FUNCTION dbo.BF_UserDepartments
(	
	@user_id			INT	
)
RETURNS TABLE 
AS
RETURN 
(
SELECT ud.user_department_id, u.user_id, u.email, d.department_id, d.department_desc, 
	CASE WHEN u.user_id IS NULL THEN 'false' ELSE 'true' END AS has_role
	--CASE WHEN EXISTS(SELECT 1 FROM user_roles ur2 WHERE ur2.role_id = r.role_id AND ur2.user_id = u.user_id) THEN 'true' ELSE 'false' END AS has_role
FROM departments d WITH (NOLOCK)
LEFT JOIN user_departments ud WITH (NOLOCK) ON  d.department_id = ud.department_id AND ud.user_id = @user_id
LEFT JOIN users u WITH (NOLOCK) ON ud.user_id = u.user_id AND u.user_id = @user_id

)
GO
