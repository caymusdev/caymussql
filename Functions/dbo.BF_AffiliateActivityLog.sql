SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rick Pina
-- Create date: 2020-10-21
-- Description:	Need a function so I can pass in the affiliate id and return a table that can then be filtered/sorted
-- =============================================
CREATE FUNCTION [dbo].[BF_AffiliateActivityLog]
(	
	@affiliate_id			INT	
)
RETURNS TABLE 
AS
RETURN 
(
SELECT at.activity_type_desc, al.[user_id], al.comments, al.create_date
FROM activity_log al WITH (NOLOCK)
INNER JOIN activity_types at WITH (NOLOCK) ON al.activity_type_id = at.activity_type_id
WHERE al.affiliate_id = @affiliate_id

)
GO
