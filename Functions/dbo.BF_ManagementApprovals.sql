SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rick Pina
-- Create date: 2021-02-26
-- Description:	Need a function so I can pass in the user id and return a table that can then be
-- =============================================
CREATE FUNCTION dbo.BF_ManagementApprovals
(	
--	@collector_email			NVARCHAR(200)	
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT a.approval_id, a.approval_type_id, at.description AS approval_type, a.description, a.comments, a.record_id, a.new_value, a.create_user, a.create_date,
		aa.affiliate_name, aa.affiliate_id, aa.gross_due
	FROM approvals a WITH (NOLOCK)
	LEFT JOIN approval_types at WITH (NOLOCK) ON a.approval_type_id = at.approval_type_id
	LEFT JOIN affiliates aa WITH (NOLOCK) ON a.affiliate_id = aa.affiliate_id
	WHERE a.approved_by IS NULL AND a.rejected_by IS NULL
)
GO
