SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-07
-- Description:	Need a function so I can pass in the affiliate id and return a table that can then be filtered/sorted
-- =============================================
CREATE FUNCTION dbo.BF_AffiliateSummary
(	
	@affiliate_id			INT	
)
RETURNS TABLE 
AS
RETURN 
(
SELECT a.affiliate_id, a.affiliate_name, c.collection_age, c.days_assigned_to_collector, c.attempted_calls, c.contacts_made, c.last_contact, 
	COALESCE(bsp.status_desc, bs1.status_desc) AS collection_bucket, c.last_payment_date
FROM affiliates a WITH (NOLOCK)
INNER JOIN cases c WITH (NOLOCK) ON a.affiliate_id = c.affiliate_id AND c.active = 1
LEFT JOIN bucket_statuses bs1 ON c.bucket_status_id = bs1.bucket_status_id
LEFT JOIN bucket_statuses bsp ON bs1.parent_status_id = bsp.bucket_status_id
WHERE a.affiliate_id = @affiliate_id

)
GO
