SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rick Pina
-- Create date: 2020-10-16
-- Description:	Need a function so I can pass in the affiliate id and return a table that can then be filtered/sorted
-- =============================================
CREATE FUNCTION [dbo].[BF_AffiliateDocuments]
(	
	@affiliate_id			INT	
)
RETURNS TABLE 
AS
RETURN 
(
SELECT m.merchant_name, 'https://solo.mendixcloud.com/link/ViewDocs/' + CONVERT(VARCHAR(50), m.merchant_id) AS document_url
FROM merchants m WITH (NOLOCK)
WHERE m.affiliate_id = @affiliate_id

)
GO
