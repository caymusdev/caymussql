SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2021-01-04
-- Description: Returns 1 if user has management role
-- =============================================
CREATE FUNCTION BF_DoesUserHaveManagementRole
(
    @users_email			NVARCHAR (100)
)
RETURNS BIT
AS
BEGIN
    
DECLARE @has_management BIT; SET @has_management = 0

IF EXISTS (SELECT 1 
		   FROM users u
		   INNER JOIN user_roles ur ON u.user_id = ur.user_id
		   INNER JOIN roles r ON ur.role_id = r.role_id
		   WHERE r.role_name IN ('Management', 'Administrator')
			AND u.email = @users_email)
	BEGIN
		SET @has_management = 1
	END



RETURN @has_management
END
GO
