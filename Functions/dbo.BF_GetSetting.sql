SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2018-05-23
-- Description:	Gets a setting value
-- =============================================
CREATE FUNCTION [dbo].[BF_GetSetting] 
(
	@setting			NVARCHAR (50)
)
RETURNS NVARCHAR (4000)  --- needed to support special chars in passwords - 2019-09-03
AS
BEGIN
	
DECLARE @value NVARCHAR (4000)
DECLARE @encrypted VARBINARY(MAX)

SELECT @encrypted = encrypt_value FROM settings WHERE setting = @setting

IF @encrypted IS NULL
	SELECT @value = value FROM settings WHERE setting = @setting
ELSE
	SELECT @value = CONVERT(NVARCHAR(MAX), DecryptByPassPhrase('bosskcollect', @encrypted))

RETURN @value

END
GO
