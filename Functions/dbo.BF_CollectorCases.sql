SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-10-16
-- Description:	Returns a list of cases for a given collector
-- =============================================
CREATE FUNCTION [dbo].[BF_CollectorCases]
(	
	@collector_email			NVARCHAR (100),
	@filter						NVARCHAR (100)
)
RETURNS TABLE 
AS
RETURN 
(
SELECT DISTINCT a.affiliate_id, a.affiliate_name, a.gross_due, c.case_id, c.last_contact, c.last_attempted_contact, ct_1.contact_type_desc AS last_contact_type, 
	ct_a.contact_type_desc AS last_attempted_contact_type, cts.contact_status_desc AS last_contact_status, c.next_follow_up_date, 
	CASE WHEN pp.payment_plan_id IS NULL THEN 'N' ELSE 'Y' END AS has_payment_plan, pps.performance_status AS payment_plan_status, 
	CONVERT(BIT, COALESCE(c.missed_payment, 0)) AS missed_payment, 
	CONVERT(BIT, COALESCE(c.new_communication, 0)) AS new_communication,  -- may be able to remove this field since we'll have alerts showing in the menu
	CONVERT(BIT, CASE WHEN EXISTS (SELECT 1 FROM reminders r WHERE r.affiliate_id = a.affiliate_id 
		AND r.reminder_date >= GETUTCDATE() AND COALESCE(r.completed, 0) = 0) THEN 1 ELSE 0 END ) AS has_reminders,-- may be able to remove this field since we'll have alerts showing in the menu
	CONVERT(BIT, COALESCE(c.new_case, 0)) AS new_case, 
	d.department_desc AS collection_department, cs.status_desc AS collection_status, COALESCE(bsp.status_desc, bs1.status_desc) AS collection_bucket, 
		CASE WHEN bsp.bucket_status_id IS NOT NULL THEN bs1.status_desc ELSE NULL END AS collection_bucket_sub_status
FROM affiliates a WITH (NOLOCK)
INNER JOIN cases c WITH (NOLOCK) ON a.affiliate_id = c.affiliate_id
INNER JOIN collector_cases cc WITH (NOLOCK) ON c.case_id = cc.case_id 
	AND cc.start_date <= DATEADD(HH, -5, GETUTCDATE()) AND COALESCE(cc.end_date, '2050-05-09') >= DATEADD(HH, -5, GETUTCDATE())
INNER JOIN users u WITH (NOLOCK) ON cc.collector_user_id = u.user_id
LEFT JOIN contact_types ct_1 WITH (NOLOCK) ON c.last_contact_type = ct_1.contact_type_id
LEFT JOIN contact_types ct_a WITH (NOLOCK) ON c.last_attempted_contact_type = ct_a.contact_type_id
LEFT JOIN contact_statuses cts WITH (NOLOCK) ON c.last_contact_status_id = cts.contact_status_id
LEFT JOIN (
	SELECT MAX(payment_plan_id) AS most_recent_plan, affiliate_id
	FROM payment_plans 
	GROUP BY affiliate_id
) x ON a.affiliate_id = x.affiliate_id
LEFT JOIN payment_plans pp WITH (NOLOCK) ON x.most_recent_plan = pp.payment_plan_id 
LEFT JOIN payment_plan_status_codes pps WITH (NOLOCK) ON pp.current_status = pps.id
LEFT JOIN departments d WITH (NOLOCK) ON c.department_id = d.department_id
LEFT JOIN case_statuses cs WITH (NOLOCK) ON c.case_status_id = cs.case_status_id
LEFT JOIN bucket_statuses bs1 WITH (NOLOCK) ON c.bucket_status_id = bs1.bucket_status_id
LEFT JOIN bucket_statuses bsp WITH (NOLOCK) ON bs1.parent_status_id = bsp.bucket_status_id
LEFT JOIN reminders r WITH (NOLOCK) ON a.affiliate_id = r.affiliate_id AND CONVERT(DATE, r.reminder_date) = CONVERT(DATE, DATEADD(HH, -5, GETUTCDATE()))
LEFT JOIN contact_types ct WITH (NOLOCK) ON r.contact_type_id = ct.contact_type_id AND ct.contact_type = 'phone'
LEFT JOIN reminders r2 WITH (NOLOCK) ON a.affiliate_id = r2.affiliate_id AND CONVERT(DATE, r2.reminder_date) = CONVERT(DATE, DATEADD(HH, -5, GETUTCDATE()))
LEFT JOIN contact_types ct2 WITH (NOLOCK) ON r2.contact_type_id = ct2.contact_type_id AND ct2.contact_type != 'phone'
WHERE u.email = @collector_email 
	AND c.active = 1
AND (
	(@filter = 'CasesAssigned') 
	OR (@filter = 'CasesActive' AND cs.status = 'Active') 
	OR (@filter = 'CasesInactive' AND cs.status = 'Inactive') 
	OR (@filter = 'CallsScheduled' AND r.reminder_id IS NOT NULL AND ct.contact_type_id IS NOT NULL)
	OR (@filter = 'EmailTextMailScheduled' AND r2.reminder_id IS NOT NULL AND ct2.contact_type_id IS NOT NULL)
	OR (@filter = 'BusinessStatusToValidate' AND a.affiliate_id IN (SELECT affiliate_id FROM BF_BusinessOpenToValidate(@collector_email)))
	OR (@filter = 'PaymentPlanBroken' AND a.affiliate_id IN 
		(SELECT pl.affiliate_id 
        FROM payment_plans pl
		INNER JOIN affiliates a ON pl.affiliate_id = a.affiliate_id
		INNER JOIN cases c ON a.affiliate_id = c.affiliate_id
		INNER JOIN collector_cases cc ON c.case_id = cc.case_id AND cc.end_date IS NULL
		INNER JOIN payment_plan_status_codes pps ON pl.current_status = pps.id
        WHERE pl.payment_plan_type = 'Payment Plan' AND pps.code IN ('Broken', 'BrokenPayment') AND collector_user_id = u.user_id)) 
	OR (@filter = 'NewCases' and new_case = 1) 
	OR (@filter = 'TempPaymentPlansBroken' AND a.affiliate_id IN 
		(SELECT pl.affiliate_id 
        FROM payment_plans pl
		INNER JOIN affiliates a ON pl.affiliate_id = a.affiliate_id
		INNER JOIN cases c ON a.affiliate_id = c.affiliate_id
		INNER JOIN collector_cases cc ON c.case_id = cc.case_id AND cc.end_date IS NULL
		INNER JOIN payment_plan_status_codes pps ON pl.current_status = pps.id
        WHERE pl.payment_plan_type = 'Temporary Arrangement' AND pps.code IN ('Broken', 'BrokenPayment') AND collector_user_id = u.user_id))
	OR (@filter = 'TempPaymentPlansExpired' AND a.affiliate_id IN 
		(SELECT pl_a.affiliate_id
        FROM payment_plans pl_a
		INNER JOIN affiliates a_a ON pl_a.affiliate_id = a_a.affiliate_id
		INNER JOIN cases c_a ON a_a.affiliate_id = c_a.affiliate_id
		INNER JOIN collector_cases cc_a ON c_a.case_id = cc_a.case_id AND cc_a.end_date IS NULL	
        WHERE cc_a.collector_user_id = u.user_id
			AND pl_a.payment_plan_type = 'Temporary Arrangement'
			-- check for last payment date in the plan and see if it is in the past
			AND (SELECT MAX(payment_date) FROM funding_payment_dates fpd_a WHERE fpd_a.payment_plan_id = pl_a.payment_plan_id) < GETUTCDATE()
			-- make sure a new payment plan has not been created
			AND NOT EXISTS (SELECT 1 FROM funding_payment_dates fpd2_a
										INNER JOIN payment_plans pp2_a ON fpd2_a.payment_plan_id = pp2_a.payment_plan_id										
										WHERE pp2_a.affiliate_id = pl_a.affiliate_id AND fpd2_a.payment_date >= GETUTCDATE())
		)) 
	OR (@filter = '' AND cs.status = 'Active')
	)
)
GO
