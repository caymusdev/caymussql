SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rick Pina
-- Create date: 2020-10-15
-- Description:	Payments Specifically For The Affiliates Table
-- =============================================
CREATE FUNCTION [dbo].[BF_AffiliatePayments]
(	
	@affiliate_id			INT,
	@future_only			BIT,
	@payment_plan_id		INT--,
--	@users_email			NVARCHAR (100)
)
RETURNS TABLE 
AS
RETURN 
(
/*
SELECT t.trans_date AS payment_date, m.merchant_name, f.contract_number, t.trans_amount, t.settle_code, tt.trans_type AS transaction_type, 't' AS record_type,
	NULL AS payment_id, NULL AS payment_plan_id
FROM transactions t WITH (NOLOCK)
LEFT JOIN fundings f WITH (NOLOCK) ON t.funding_id = f.id
LEFT JOIN merchants m WITH (NOLOCK) ON f.merchant_id = m.merchant_id
LEFT JOIN transaction_types tt WITH (NOLOCK) ON t.trans_type_id = tt.trans_type_id
WHERE f.affiliate_id = @affiliate_id 
	AND (CONVERT(DATE, t.trans_date) > CONVERT(DATE, DATEADD(HH, -5, GETUTCDATE())) OR @future_only = 0)
	AND @payment_plan_id = -1
	AND t.trans_type_id IN (1, 2, 3, 4, 5, 14, 19, 21)
UNION ALL
SELECT pd.payment_date, m.merchant_name, f.contract_number, pd.amount AS transaction_amount, NULL AS settle_code, pd.payment_method AS transaction_type, 'p' AS record_type,
	pd.id AS payment_id, pd.payment_plan_id
FROM funding_payment_dates pd
INNER JOIN fundings f ON pd.funding_id = f.id
INNER JOIN merchants m WITH (NOLOCK) ON f.merchant_id = m.merchant_id
WHERE f.affiliate_id = @affiliate_id
	AND (CONVERT(DATE, pd.payment_date) > CONVERT(DATE, DATEADD(HH, -5, GETUTCDATE())) OR @future_only = 0)
	AND (pd.payment_plan_id = @payment_plan_id OR @payment_plan_id = -1)	
	AND pd.active = 1
*/

SELECT fpd.payment_date, m.merchant_name, f.contract_number, t.settle_code,
	fpd.amount AS transaction_amount, tt.trans_type AS transaction_type, fpd.id AS funding_payment_id, fpd.payment_plan_id, f.affiliate_id,
	CASE WHEN fpd.payment_date > DATEADD(HH, -5, GETUTCDATE()) THEN 1 ELSE 0 END AS can_edit, 'fpd' AS record_type, p.scheduled_payment_id
FROM funding_payment_dates fpd
LEFT JOIN transactions t ON COALESCE(fpd.chargeback_trans_id, fpd.reject_trans_id, fpd.rec_trans_id) = t.trans_id
INNER JOIN fundings f ON fpd.funding_id = f.id
INNER JOIN merchants m ON f.merchant_id = m.merchant_id
LEFT JOIN transaction_types tt ON t.trans_type_id = tt.trans_type_id
LEFT JOIN scheduled_payments p ON fpd.id = p.funding_payment_id
WHERE f.affiliate_id = @affiliate_id
	AND (CONVERT(DATE, fpd.payment_date) > CONVERT(DATE, DATEADD(HH, -5, GETUTCDATE())) OR @future_only = 0)
	AND (fpd.payment_plan_id = @payment_plan_id OR @payment_plan_id = -1)	
	AND fpd.active = 1
	AND NOT EXISTS (SELECT 1 FROM scheduled_payments p2 WHERE p2.funding_payment_id = fpd.id AND p2.approved_date IS NULL AND p2.approved_user IS NULL
		AND p2.rejected_date IS NULL AND p2.rejected_user IS NULL)
UNION ALL
-- bring in the working ones (unapproved)
SELECT p.payment_date, NULL AS merchant_name, NULL AS contract_number, NULL AS settle_Code, p.payment_amount, p.payment_method AS transaction_type,
	p.scheduled_payment_id, p.payment_plan_id, p.affiliate_id,
	CASE WHEN p.payment_date > DATEADD(HH, -5, GETUTCDATE()) THEN 1 ELSE 0 END AS can_edit, 'p' AS record_type, p.scheduled_payment_id
FROM scheduled_payments p
WHERE p.approved_date IS NULL AND p.approved_user IS NULL AND p.rejected_date IS NULL AND p.rejected_user IS NULL
	AND p.affiliate_id = @affiliate_id
	AND (CONVERT(DATE, p.payment_date) > CONVERT(DATE, DATEADD(HH, -5, GETUTCDATE())) OR @future_only = 0)
	AND (p.payment_plan_id = @payment_plan_id OR @payment_plan_id = -1)
	AND COALESCE(p.deleted, 0) = 0


/*

SELECT m.merchant_name, f.contract_number, COALESCE(pdw.payment_date, pd.payment_date) AS payment_date, 
	COALESCE(pdw.amount, pd.amount) AS amount, COALESCE(pdw.payment_method, pd.payment_method) AS payment_method, 
	COALESCE(pdw.payment_processor, pd.payment_processor) AS payment_processor, pdw.id AS worker_id, 
	COALESCE(pd.payment_plan_id, pdw.payment_plan_id) AS payment_plan_id,
	pd.id AS funding_payment_id, t.settle_code, tt.trans_type AS transaction_type
FROM funding_payment_dates_work pdw WITH (NOLOCK)
LEFT JOIN funding_payment_dates pd WITH (NOLOCK) ON pdw.funding_payment_id = pd.id
INNER JOIN fundings f WITH (NOLOCK) ON COALESCE(pd.funding_id, pdw.funding_id) = f.id
INNER JOIN merchants m WITH (NOLOCK) ON f.merchant_id = m.merchant_id
LEFT JOIN transactions t ON COALESCE(pd.chargeback_trans_id, pd.reject_trans_id, pd.rec_trans_id) = t.trans_id
LEFT JOIN transaction_types tt ON t.trans_type_id = tt.trans_type_id
WHERE pdw.userid = @users_email AND f.affiliate_id = @affiliate_id
	AND COALESCE(pdw.active, 1) = 1
	AND COALESCE(pdw.approved, 0) = 0 -- approved ones need to be left alone so that they can make changes to ones that have already been approved previously
	AND pdw.payment_date > DATEADD(HH, -5, GETUTCDATE())
UNION ALL
SELECT m.merchant_name, f.contract_number, pd.payment_date, pd.amount, pd.payment_method, pd.payment_processor, NULL AS worker_id, 
	pd.payment_plan_id, pd.id AS funding_payment_id, t.settle_code, tt.trans_type AS transaction_type
FROM funding_payment_dates pd WITH (NOLOCK) 
INNER JOIN fundings f WITH (NOLOCK) ON pd.funding_id = f.id
INNER JOIN merchants m WITH (NOLOCK) ON f.merchant_id = m.merchant_id
LEFT JOIN transactions t ON COALESCE(pd.chargeback_trans_id, pd.reject_trans_id, pd.rec_trans_id) = t.trans_id
LEFT JOIN transaction_types tt ON t.trans_type_id = tt.trans_type_id
WHERE f.affiliate_id = @affiliate_id
	AND pd.payment_date <= DATEADD(HH, -5, GETUTCDATE())
	*/
)



GO
