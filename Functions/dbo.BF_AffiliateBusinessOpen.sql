SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rick Pina
-- Create date: 2020-10-22
-- Description:	Need a function so I can pass in the affiliate id and return a table that can then be filtered/sorted
-- =============================================
CREATE FUNCTION [dbo].[BF_AffiliateBusinessOpen]
(	
	@affiliate_id			INT	,
	@show_all_records		BIT
)
RETURNS TABLE 
AS
RETURN 
(
SELECT m.merchant_name, b.contact_type, b.open_closed, b.create_user, b.create_date, b.verified_date	
FROM businessopen b WITH (NOLOCK)
INNER JOIN merchants m WITH (NOLOCK) ON b.merchant_id = m.merchant_id
WHERE m.affiliate_id = @affiliate_id AND @show_all_records = 1

UNION ALL
SELECT TOP 1 WITH TIES m.merchant_name, b.contact_type, b.open_closed, b.create_user, b.create_date, b.verified_date
FROM businessopen b WITH (NOLOCK)
INNER JOIN merchants m WITH (NOLOCK) ON b.merchant_id = m.merchant_id
WHERE  m.affiliate_id = @affiliate_id AND @show_all_records = 0
ORDER BY CASE WHEN ROW_NUMBER() OVER(PARTITION BY m.merchant_id ORDER BY b.verified_date DESC, b.create_date DESC) <= 1 then 0 else 1 end

)
 
GO
