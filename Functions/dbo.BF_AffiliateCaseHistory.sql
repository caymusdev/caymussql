SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rick Pina
-- Create date: 2021-03-25
-- Description:	Need a function so I can pass in the affiliate id and return a table that can then be filtered/sorted
-- =============================================
CREATE FUNCTION [dbo].[BF_AffiliateCaseHistory]
(	
	@affiliate_id			INT	
)
RETURNS TABLE 
AS
RETURN 
(
SELECT 'Collector' AS Type, u.email AS collector_department, cc.start_date, cc.end_date, cc.create_date 
FROM collector_cases cc
LEFT JOIN users u ON cc.collector_user_id = u.user_id
INNER JOIN cases c ON cc.case_id = c.case_id 
WHERE c.affiliate_id = @affiliate_id
UNION ALL
SELECT 'Department' AS Type, d.department_desc AS collector_department, cd.start_date, cd.end_date, cd.create_date  
FROM case_departments cd
INNER JOIN cases c ON cd.case_id = c.case_id
INNER JOIN departments d ON cd.department_id = d.department_id
WHERE c.affiliate_id = @affiliate_id

)
GO
