SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-01-14
-- Description:	Gets the next business tomorrow date given an input date
-- =============================================
CREATE FUNCTION [dbo].[BF_GetTomorrow] 
(
	@date			DATE
)
RETURNS DATE 
AS
BEGIN
	
DECLARE @tomorrow DATE
SELECT @tomorrow = DATEADD(DD, CASE DATEPART(WEEKDAY, @date) WHEN 6 THEN 3 WHEN 7 THEN 2 ELSE 1 END, @date)

RETURN @tomorrow

END
GO
