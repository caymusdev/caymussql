SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


   
create function [dbo].[fn_IsBitSetInBitmask]  
(@bitmask varbinary(500), @colid int)  
    returns int  
as  
begin  
    declare @word smallint  
    declare @bit  smallint  
    declare @mask binary(2)  
    declare @mval int  
    declare @oldword binary(2)    
  
    if @colid < 1 return 0  
  
    SELECT @word = 1 + FLOOR((@colid -1)/16)  
  
    SELECT @bit = (@colid -1) % 16  
  
    SELECT @mval = POWER(2, @bit)  
    SELECT @mask = convert( binary(2), unicode( substring( convert( nchar(2), convert( binary(4), @mval ) ), 2, 1 ) ) )  
      
    SELECT @oldword = convert( binary(2), SUBSTRING( convert( nvarchar(64),@bitmask), @word, 1) )  
    IF @oldword IS NULL return 0  
  
    return  convert( smallint, @oldword ) & convert( smallint, @mask )  
end
GO
