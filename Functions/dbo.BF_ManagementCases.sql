SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rick Pina
-- Create date: 2021-01-14
-- Description:	Returns a list of cases for a manager
-- =============================================
CREATE FUNCTION [dbo].[BF_ManagementCases]
(	
	@email			NVARCHAR (100)
	--@filter						NVARCHAR (100)
)
RETURNS TABLE 
AS
RETURN 
(
SELECT a.affiliate_id, a.affiliate_name, a.gross_due, cd.case_department_id, c.case_id, u.email AS assigned_collector, c.last_contact, c.last_attempted_contact, ct_1.contact_type_desc AS last_contact_type, 
	ct_a.contact_type_desc AS last_attempted_contact_type, cts.contact_status_desc AS last_contact_status, c.next_follow_up_date, 
	CASE WHEN pp.payment_plan_id IS NULL THEN 'N' ELSE 'Y' END AS has_payment_plan, pps.performance_status AS payment_plan_status,
	CASE WHEN cd.department_id = d.department_id THEN d.department_desc ELSE NULL END AS current_department, 
	cd.next_move_on, cd.do_not_move, cd.do_not_move_until, d2.department_desc AS next_department,

	d.department_desc AS collection_department, cs.status_desc AS collection_status, COALESCE(bsp.status_desc, bs1.status_desc) AS collection_bucket, 
		CASE WHEN bsp.bucket_status_id IS NOT NULL THEN bs1.status_desc ELSE NULL END AS collection_bucket_sub_status
FROM affiliates a WITH (NOLOCK)
INNER JOIN cases c WITH (NOLOCK) ON a.affiliate_id = c.affiliate_id AND c.active = 1
LEFT JOIN collector_cases cc WITH (NOLOCK) ON c.case_id = cc.case_id AND cc.start_date <= DATEADD(HH, -5, GETUTCDATE()) AND COALESCE(cc.end_date, '2050-05-09') >= DATEADD(HH, -5, GETUTCDATE())
LEFT JOIN users u WITH (NOLOCK) ON cc.collector_user_id = u.user_id
LEFT JOIN contact_types ct_1 WITH (NOLOCK) ON c.last_contact_type = ct_1.contact_type_id
LEFT JOIN contact_types ct_a WITH (NOLOCK) ON c.last_attempted_contact_type = ct_a.contact_type_id
LEFT JOIN contact_statuses cts WITH (NOLOCK) ON c.last_contact_status_id = cts.contact_status_id
LEFT JOIN payment_plans pp WITH (NOLOCK) ON c.payment_plan_id = pp.payment_plan_id
LEFT JOIN payment_plan_status_codes pps WITH (NOLOCK) ON pp.current_status = pps.id
LEFT JOIN departments d WITH (NOLOCK) ON c.department_id = d.department_id
LEFT JOIN case_statuses cs WITH (NOLOCK) ON c.case_status_id = cs.case_status_id
LEFT JOIN bucket_statuses bs1 WITH (NOLOCK) ON c.bucket_status_id = bs1.bucket_status_id
LEFT JOIN bucket_statuses bsp WITH (NOLOCK) ON bs1.parent_status_id = bsp.bucket_status_id
LEFT JOIN case_departments cd ON c.case_id = cd.case_id AND cd.start_date <= DATEADD(HH, -5, GETUTCDATE()) AND COALESCE(cd.end_date, '2050-01-01') >= DATEADD(HH, -5, GETUTCDATE())
LEFT JOIN departments d2 WITH (NOLOCK) ON cd.next_move_to = d2.department_id
--WHERE u.email = @email --AND ((@filter = 'CasesAssigned') OR(@filter = 'CasesActive') OR 
)
GO
