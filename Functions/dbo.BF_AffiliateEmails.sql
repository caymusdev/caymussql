SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rick Pina
-- Create date: 2020-11-03
-- Description:	Need a function so I can pass in the affiliate id and return a table that can then be filtered/sorted
-- =============================================
CREATE FUNCTION [dbo].[BF_AffiliateEmails]
(	
	@affiliate_id			INT	
)
RETURNS TABLE 
AS
RETURN 
(

SELECT e.email_id, COALESCE(u.email, e.from_address) AS email, e.subject, e.body, e.create_date, e.message_id
FROM emails e WITH (NOLOCK)
LEFT JOIN users u WITH (NOLOCK) ON e.sender_id = u.[user_id]
WHERE e.affiliate_id = @affiliate_id

)
GO
