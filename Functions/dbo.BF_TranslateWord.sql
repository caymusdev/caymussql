SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2020-09-14
-- Description: Takes a word as a parameter and returns the translation.  Returns the same word if no translation exists
-- =============================================
CREATE FUNCTION dbo.BF_TranslateWord
(
	@word			VARCHAR (100)	
)
RETURNS NVARCHAR (4000)
AS
BEGIN

DECLARE @translated_word NVARCHAR (MAX)

SELECT @translated_word = w.translated_word
FROM words w WITH (NOLOCK)
WHERE w.word = @word

RETURN COALESCE(@translated_word, @word) 


END
GO
