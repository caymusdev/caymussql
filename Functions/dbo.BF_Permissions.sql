SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-09-15
-- Description:	Need a function so I can pass in the role id and return a table that can then be filtered.  -1 as role id will return all permissions
-- =============================================
CREATE FUNCTION dbo.BF_Permissions
(	
	@role_id			INT	
)
RETURNS TABLE 
AS
RETURN 
(
SELECT DISTINCT p.permission_id, p.permission_name, p.permission_desc 
FROM permissions p WITH (NOLOCK)
LEFT JOIN role_permissions rp WITH (NOLOCK) ON p.permission_id = rp.permission_id
WHERE rp.role_id = @role_id OR @role_id = -1
)
GO
