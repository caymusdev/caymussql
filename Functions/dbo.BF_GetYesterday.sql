SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-01-14
-- Description:	Gets the previous business date given an input date
-- =============================================
CREATE FUNCTION [dbo].[BF_GetYesterday] 
(
	@date			DATE
)
RETURNS DATE 
AS
BEGIN
	
DECLARE @yesterday DATE
SELECT @yesterday = DATEADD(DD, CASE DATEPART(WEEKDAY, @date) WHEN 1 THEN -2 WHEN 2 THEN -3 ELSE -1 END, @date)

RETURN @yesterday

END
GO
