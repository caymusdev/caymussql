SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-11-11
-- Description:	Gets an affiliate id from the texts table 
-- =============================================
CREATE FUNCTION [dbo].[BF_GetAffiliateIDFromSMSNumber]
(
	@number			VARCHAR (50)
)
RETURNS 
@table TABLE 
(
	affiliate_id	INT
)
AS
BEGIN


DECLARE @affiliate_id INT

SELECT TOP 1 @affiliate_id = t.affiliate_id
FROM texts t
WHERE (t.[to] = @number OR t.[from] = @number) AND t.affiliate_id IS NOT NULL

-- if we don't find an existing text then look in phone numbers table
IF @affiliate_id IS NULL
	BEGIN
		SELECT TOP 1 @affiliate_id = c.affiliate_id 
		FROM phone_numbers pn
		INNER JOIN contacts c ON pn.contact_id = c.contact_id
		WHERE pn.phone_number = @number
	END

INSERT INTO @table(affiliate_id)
VALUES (@affiliate_id)

RETURN
	
END

GO
