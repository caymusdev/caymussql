SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rick Pina
-- Create date: 2020-10-13
-- Description:	Need a function so I can pass in the affiliate id and return a table that can then be filtered/sorted
-- =============================================
CREATE FUNCTION [dbo].[BF_AffiliateAffiliation]
(	
	@affiliate_id			INT	
)
RETURNS TABLE 
AS
RETURN 
(
--SELECT m.merchant_name, c.contract_number, cs.contract_status, c.gross_due, c.last_payment_date, c.last_payment_amount	
--FROM contracts c WITH (NOLOCK)
--INNER JOIN merchants m WITH (NOLOCK) ON c.merchant_id = m.merchant_id
--LEFT JOIN contract_statuses cs WITH (NOLOCK) ON c.contract_status_id = cs.contract_status_id
--WHERE m.affiliate_id = @affiliate_id

SELECT m.merchant_name, f.contract_number, cs.contract_status AS contract_status_desc, f.payoff_amount AS gross_due, f.last_payment_date, 
	f.last_trans_amount AS last_payment_amount
FROM fundings f WITH (NOLOCK)
INNER JOIN merchants m WITH (NOLOCK) ON f.merchant_id = m.merchant_id
INNER JOIN contract_statuses cs WITH (NOLOCK) ON f.contract_status = cs.contract_status_id
WHERE f.affiliate_id = @affiliate_id

)
GO
