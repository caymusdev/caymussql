SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rick Pina
-- Create date: 2020-10-16
-- Description:	Need a function so I can pass in the affiliate id and return a table that can then be filtered/sorted
-- =============================================
CREATE FUNCTION [dbo].[BF_AffiliateNotes]
(	
	@affiliate_id			INT,
	@users_email			NVARCHAR (100)
)
RETURNS TABLE 
AS
RETURN 
(


SELECT n.notes_id, n.important_notes, n.notes_text, n.create_date, n.create_user,
CASE WHEN n.create_user = @users_email THEN 1 ELSE dbo.BF_DoesUserHaveManagementRole(@users_email) END AS can_delete
FROM notes n WITH (NOLOCK)
WHERE n.affiliate_id = @affiliate_id


)

GO
