SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-03-17
-- Description:	Gets a list of incomplete reminders
-- =============================================
CREATE FUNCTION dbo.BF_GetAffiliateReminders
(	
	@affiliate_id			INT	
)
RETURNS TABLE 
AS
RETURN 
(
SELECT r.reminder_id, r.affiliate_id, r.contact_type_id, r.reminder_date, r.contact_id, r.user_id, 
	r.notes, r.subject, ct.contact_type_desc, c.contact_first_name + ' ' + c.contact_last_name AS contact_name
FROM reminders r WITH (NOLOCK)
LEFT JOIN contact_Types ct WITH (NOLOCK) ON r.contact_type_id = ct.contact_type_id
LEFT JOIN contacts c WITH (NOLOCK) ON r.contact_id = c.contact_id
WHERE COALESCE(r.completed, 0) = 0 AND r.affiliate_id = @affiliate_id

)
GO
