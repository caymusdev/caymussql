SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ryan Brown
-- Create date: 2022-09-01
-- Description:	Used for search grid on collections affiliates page
-- =============================================
CREATE FUNCTION [dbo].[BSK_AffiliatesSearch]
(	

)

RETURNS TABLE 
AS
RETURN 
(
	SELECT f.id AS funding_id, a.affiliate_name, a.affiliate_id, m.merchant_id, f.contract_number, m.merchant_name, f.collection_type, f.payoff_amount, 
		f.last_payment_date, f.last_trans_amount AS last_payment_amount, fc.assigned_collector, cs.contract_status AS contract_status
	FROM fundings f
	INNER JOIN merchants m ON f.merchant_id = m.merchant_id
	LEFT JOIN affiliates a ON m.affiliate_id = a.affiliate_id
	LEFT JOIN funding_collections fc ON f.id = fc.funding_id AND end_date IS NULL
	LEFT JOIN contract_statuses cs ON f.contract_status = cs.contract_status_id
	
)
GO
