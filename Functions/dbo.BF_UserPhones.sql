SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rick Pina
-- Create date: 2020-02-24
-- Description:	Need a function so I can pass in the affiliate id and return a table that can then be filtered/sorted
-- =============================================
CREATE FUNCTION [dbo].[BF_UserPhones]
(	
	@email			NVARCHAR(100)	
)
RETURNS TABLE 
AS
RETURN 
(
select u.user_id, up.user_phone_id, u.email, up.phone_number, up.phone_type, up.is_primary
from users u                                                          
INNER JOIN user_phones up ON u.user_id = up.user_id 
where email = @email

)
GO
