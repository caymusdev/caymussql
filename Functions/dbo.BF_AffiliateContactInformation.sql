SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rick Pina
-- Create date: 2020-10-13
-- Description:	Need a function so I can pass in the affiliate id and return a table that can then be filtered/sorted
-- =============================================
CREATE FUNCTION [dbo].[BF_AffiliateContactInformation]
(	
	@affiliate_id			INT	,
	@users_email			NVARCHAR(500)
)
RETURNS TABLE 
AS
RETURN 
(

SELECT c.contact_id, c.affiliate_id, cpt.contact_type_desc, c.active, c.best_contact, c.contact_last_name, c.business_name, c.note, c.email, pt.phone_type AS phone_type, pn.phone_number, c.success_rate, 
	pt.phone_type_desc AS phone_type_desc, c.contact_first_name,
	CASE WHEN c.create_user = @users_email THEN 1 ELSE dbo.BF_DoesUserHaveManagementRole(@users_email) END AS can_delete
FROM contacts c WITH (NOLOCK)
LEFT JOIN contact_person_types cpt WITH (NOLOCK) ON c.contact_person_type = cpt.contact_person_type_id
LEFT JOIN phone_numbers pn WITH (NOLOCK) ON c.contact_id = pn.contact_id
LEFT JOIN phone_types pt WITH (NOLOCK) ON pn.phone_type_id = pt.phone_type_id
WHERE c.affiliate_id = @affiliate_id
)

GO
