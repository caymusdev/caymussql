SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-11-11
-- Description:	If @contract_id not null it returns the merchant id and affiliate if for that contract
-- If @merchant_id is the first not null then it returns the affiliate_id for that merchant
-- =============================================
CREATE FUNCTION dbo.BF_GetAffiliateMerchantContractIDs
(	
	@contract_id			INT,
	@merchant_id			INT,
	@affiliate_id			INT
)
RETURNS 
@table TABLE 
(
	contract_id	INT,
	merchant_id	INT,
	affiliate_id	INT
)
AS
BEGIN
IF @contract_id IS NOT NULL
	BEGIN
		SELECT @merchant_id = merchant_id FROM contracts WHERE contract_id = @contract_id
		SELECT @affiliate_id = affiliate_id FROM merchants WHERE merchant_id = @merchant_id
	END
ELSE IF @merchant_id IS NOT NULL
	BEGIN
		SELECT @affiliate_id = affiliate_id FROM merchants WHERE merchant_id = @merchant_id
	END

INSERT INTO @table(contract_id, merchant_id, affiliate_id)
VALUES (@contract_id, @merchant_id, @affiliate_id)


RETURN

END

GO
