SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2020-09-15
-- Description:	Need a function so I can pass in the role id and return a table that can then be filtered
-- =============================================
CREATE FUNCTION dbo.BF_RolePermissions
(	
	@role_id			INT	
)
RETURNS TABLE 
AS
RETURN 
(
SELECT r.role_id, p.permission_id, p.permission_name, p.permission_desc,
	CASE WHEN r.role_id IS NULL THEN 'false' ELSE 'true' END AS has_permission
FROM permissions p WITH (NOLOCK)
LEFT JOIN role_permissions rp WITH (NOLOCK) ON p.permission_id = rp.permission_id AND rp.role_id = @role_id
LEFT JOIN roles r WITH (NOLOCK) ON rp.role_id = r.role_id AND r.role_id = @role_id

)
GO
