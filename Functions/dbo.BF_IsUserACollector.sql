SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:      Ryan Brown
-- Create Date: 2021-01-04
-- Description: Returns 1 if user is a collector.  Used mostly to know if we should display UI for phone availability (if they can take calls or not)
-- =============================================
CREATE FUNCTION [dbo].[BF_IsUserACollector]
(
    @users_email			NVARCHAR (100)
)
RETURNS BIT
AS
BEGIN
    
DECLARE @is_collector BIT; SET @is_collector = 0

IF EXISTS (SELECT 1 
		   FROM users u
		   INNER JOIN user_departments ud ON u.user_id = ud.user_id		   
		   WHERE u.email = @users_email)
	BEGIN
		SET @is_collector = 1
	END



RETURN @is_collector
END
GO
