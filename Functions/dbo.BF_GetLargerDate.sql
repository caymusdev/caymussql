SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:     Ryan Brown
-- Create Date: 2021-04-14
-- Description: Takes 2 dates and returns the larger of them.  Was built to simplify comparing a date that is a calculated date with dateadd and various case statements.
-- This function avoided having to retype the large calculation over and over.
-- =============================================
CREATE FUNCTION dbo.BF_GetLargerDate
(
	@date1			DATETIME,
	@date2			DATETIME
)
RETURNS DATETIME
AS
BEGIN
    DECLARE @retVal DATETIME
	SET @retVal = COALESCE(@date1, @date2)

    IF @date1 < @date2 
		SET @retVal = @date2

    -- Return the result of the function
    RETURN @retVal
END
GO
