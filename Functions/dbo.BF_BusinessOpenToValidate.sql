SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Rick Pina
-- Create date: 2020-12-15
-- Description:	Need a function so I can pass in the affiliate id and return a table that can then be filtered/sorted
-- =============================================
CREATE FUNCTION [dbo].[BF_BusinessOpenToValidate]
(	
	@collector_email			NVARCHAR (100)	
)

RETURNS TABLE 
AS
RETURN 
(


SELECT a.affiliate_id, merchant_id, merchant_name FROM merchants m
INNER JOIN affiliates a ON m.affiliate_id = a.affiliate_id
INNER JOIN cases c ON a.affiliate_id = c.affiliate_id
INNER JOIN collector_cases cc ON c.case_id = cc.case_id AND cc.start_date <= DATEADD(HH, -5, GETUTCDATE()) AND COALESCE(cc.end_date, '2050-05-09') >= DATEADD(HH, -5, GETUTCDATE())
INNER JOIN users u ON cc.collector_user_id = u.[user_id]
WHERE NOT EXISTS(SELECT * FROM businessopen bo WHERE bo.merchant_id = m.merchant_id AND DATEDIFF (D, bo.verified_date, DATEADD(HH, -5, GETUTCDATE())) < 14) AND u.email = @collector_email AND c.active = 1

)
GO
