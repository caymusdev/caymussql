SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rick Pina
-- Create date: 2020-10-16
-- Description:	Need a function so I can pass in the affiliate id and return a table that can then be filtered/sorted
-- =============================================
CREATE FUNCTION [dbo].[BF_AffiliateAssets]
(	
	@affiliate_id			INT	
)
RETURNS TABLE 
AS
RETURN 
(
SELECT a.asset_id, a.asset_name, ast.asset_type_name, a.asset_price, a.asset_description, a.asset_owner, a.asset_located	
FROM assets a WITH (NOLOCK)
INNER JOIN asset_types ast WITH (NOLOCK) ON a.asset_types_id = ast.asset_types_id
LEFT JOIN merchants m WITH (NOLOCK) ON a.merchant_id = m.merchant_id
WHERE a.affiliate_id = @affiliate_id

)
GO
