SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rick Pina
-- Create date: 2020-11-10
-- Description:	Need a function so I can pass in the affiliate id and return a table that can then be filtered/sorted
-- =============================================
CREATE FUNCTION [dbo].[BF_TextsSent]
(	
	@affiliate_id			INT	
)

RETURNS TABLE 
AS
RETURN 
(


SELECT t.text_id, t.affiliate_id, t.[to] AS text_to, t.[from], t.text_message, t.sent
FROM texts t WITH (NOLOCK)
WHERE t.sent IS NOT NULL AND (t.affiliate_id = @affiliate_id OR @affiliate_id = '')

)
GO
