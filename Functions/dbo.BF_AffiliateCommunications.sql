SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rick Pina
-- Create date: 2020-10-13
-- Description:	Need a function so I can pass in the affiliate id and return a table that can then be filtered/sorted
-- =============================================
CREATE FUNCTION [dbo].[BF_AffiliateCommunications]
(	
	@affiliate_id			INT	
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT cl.contact_date, ct.contact_type_desc, c.contact_last_name, cpt.contact_type_desc AS person_type_desc, cl.contact_outcome, r.reminder_date, r.notes,
		ct.contact_type, cl.record_id, r.create_user, 
		COALESCE(u.email, u2.email, '') AS collector, c.contact_first_name
	FROM contact_log cl WITH (NOLOCK)
	LEFT JOIN contacts c WITH (NOLOCK) ON cl.contact_id = c.contact_id
	LEFT JOIN reminders r WITH (NOLOCK) ON cl.reminder_id = r.reminder_id
	INNER JOIN contact_types ct WITH (NOLOCK) ON cl.contact_type = ct.contact_type_id
	LEFT JOIN contact_person_types cpt WITH (NOLOCK) ON c.contact_person_type = cpt.contact_person_type_id
	LEFT JOIN phone_calls p ON cl.record_id = p.phone_call_id AND cl.contact_type = (SELECT contact_type_id FROM contact_types WHERE contact_type = 'phone')
	LEFT JOIN users u ON p.user_id = u.user_id
	LEFT JOIN texts t ON cl.record_id = t.text_id AND cl.contact_type = (SELECT contact_type_id FROM contact_types WHERE contact_type = 'text')
	LEFT JOIN users u2 ON t.user_id = u2.user_id
	WHERE cl.affiliate_id = @affiliate_id
)
GO
