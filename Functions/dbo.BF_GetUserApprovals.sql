SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ryan Brown
-- Create date: 2021-04-14
-- Description:	Gets a list of user approvals not yet approved
-- =============================================
CREATE FUNCTION dbo.BF_GetUserApprovals
(	
	@users_email		NVARCHAR(100)	
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT a.approval_id, a.approval_type_id, at.description AS approval_type, a.description, a.comments, a.record_id, a.new_value, a.create_user, a.create_date,
		aa.affiliate_name, aa.affiliate_id, aa.gross_due
	FROM approvals a WITH (NOLOCK)
	LEFT JOIN approval_types at WITH (NOLOCK) ON a.approval_type_id = at.approval_type_id
	LEFT JOIN affiliates aa WITH (NOLOCK) ON a.affiliate_id = aa.affiliate_id
	WHERE a.approved_by IS NULL AND a.rejected_by IS NULL AND a.create_user = @users_email
)

GO
