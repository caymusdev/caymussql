SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rick Pina
-- Create date: 2020-12-08
-- Description:	Need a function so I can pass in the affiliate id and return a table that can then be filtered/sorted
-- =============================================
CREATE FUNCTION [dbo].[BF_EmailsReceived]
(	
	@affiliate_id			INT	
)

RETURNS 
@table TABLE 
(
	email_id	INT,
	email_to	VARCHAR(8000),
	affiliate_name	VARCHAR (1000),
	email			NVARCHAR(1000),
	subject			VARCHAR(1000),
	body			NVARCHAR(450),
	received		DATETIME,
	message_id		NVARCHAR(1000),
	has_attachments BIT
)
AS
BEGIN

INSERT INTO @table(email_id, email_to, affiliate_name, email, subject, body, received, message_id, has_attachments)
 
SELECT e.email_id, dbo.BF_LinkEmailAddresses(e.email_id, 'To'), a.affiliate_name, e.from_address, e.subject, LEFT(e.body, 450) AS body, e.received, e.message_id, 
CASE WHEN EXISTS (SELECT 1 FROM email_attachments e2 WHERE e2.email_id = e.email_id) THEN 'true' ELSE 'false' END AS has_attachments
FROM emails e WITH (NOLOCK)
LEFT JOIN users u WITH (NOLOCK) ON e.sender_id = u.[user_id]
LEFT JOIN affiliates a WITH (NOLOCK) ON e.affiliate_id = a.affiliate_id
WHERE --e.received IS NOT NULL AND 
	(e.affiliate_id = @affiliate_id OR @affiliate_id = '')
	AND e.incoming = 1

RETURN

END
GO
