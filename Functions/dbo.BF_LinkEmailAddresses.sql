SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rick Pina
-- Create date: 2020-12-08
-- Description:	Need a function so I can pass in the affiliate id and return a table that can then be filtered/sorted
-- =============================================
CREATE FUNCTION [dbo].[BF_LinkEmailAddresses]
(
   @email_id				NVARCHAR (100),
   @address_type			NVARCHAR (100)
)
RETURNS VARCHAR(8000)
AS
BEGIN

	
DECLARE @emailsTo VARCHAR(8000)

SELECT @emailsTo = COALESCE(@emailsTo + ';', '') + email_address
FROM email_addresses
WHERE email_id = @email_id AND address_type = @address_type


    RETURN @emailsTo 
END
GO
