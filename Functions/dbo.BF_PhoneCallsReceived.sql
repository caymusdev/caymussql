SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rick Pina
-- Create date: 2021-01-29
-- Description:	Need a function so I can pass in the affiliate id and return a table that can then be filtered/sorted
-- =============================================
CREATE FUNCTION [dbo].[BF_PhoneCallsReceived]
(	
	@affiliate_id			INT	
)

RETURNS 
@table TABLE 
(
	phone_call_id	INT,
	affiliate_id	INT,
	contact_id		INT,
	affiliate_name	VARCHAR (1000),
	call_to			VARCHAR(1000),
	call_from		VARCHAR(1000),
	user_id			VARCHAR(500),
	call_duration	INT,
	start_time		DATETIME,
	end_time		DATETIME,
	call_id			NVARCHAR(1000),
	conversation_id	NVARCHAR(1000),
	incoming_call BIT,
	recording_url	NVARCHAR (250)
)
AS
BEGIN

INSERT INTO @table(phone_call_id, affiliate_id, contact_id, affiliate_name, call_to, call_from, user_id, call_duration, start_time, end_time, call_id, conversation_id, incoming_call, recording_url)


SELECT pc.phone_call_id, pc.affiliate_id, c.contact_id, a.affiliate_name, pc.call_to, pc.call_from, c.contact_first_name + ' ' + c.contact_last_name AS user_id,
pc.call_duration, pc.start_time, pc.end_time, pc.call_id, pc.conversation_id, pc.incoming_call, r.RecordingUrl AS recording_url
FROM phone_calls pc
LEFT JOIN affiliates a WITH (NOLOCK) ON pc.affiliate_id = a.affiliate_id
--  LEFT JOIN phone_numbers pn WITH (NOLOCK) ON pc.call_from = pn.phone_number
LEFT JOIN (
	SELECT phone_number, contact_id, ROW_NUMBER() OVER (PARTITION BY phone_number ORDER BY phone_number) AS RowNum
	FROM phone_numbers
) phones ON pc.call_from = phones.phone_number AND phones.RowNum = 1
LEFT JOIN contacts c ON phones.contact_id = c.contact_id
LEFT JOIN recordings r WITH (NOLOCK) ON pc.call_id = r.CallSid
WHERE pc.incoming_call = 1 --AND user_id = @user_id

RETURN

END

GO
